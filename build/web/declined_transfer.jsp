<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="com.fasyl.ebanking.main.DataObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Pending Fund Transfer</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">
        <div  class="tab-content" id="tab2" align="left">

            <%
                user_id = (String) session.getAttribute("user_id");
                role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
                out.println(result + "<br/>");

                String query = "select init_account, ben_account, amount, "
                        + "trans_id from fund_transfer where ( auth1_action ='DECLINED' or "
                        + "auth2_action ='DECLINED') and (initiator_id = '" + user_id + "')";

                DataObject dob = new DataObject();
                List<String[]> list = dob.getLists(query, 4);

                int awaiting_no = list.size();
            %>

            <div class="content-box-header">

                <h3> Declined Transactions</h3>

                <div class="clear"></div>

            </div> 


            <div class="content-box-header">

                <h3> <%= awaiting_no%>  transactions was/were declined</h3>

                <div class="clear"></div>

            </div> 


            <%!
                String init_account;
                String ben_acc;
                String amt;
                String initiator_id;
                String user_id;
                String trans_id;
                String result;
                String role_id;
                List<String[]> list2;
            %>
            <br/><br/>

            <% if (awaiting_no != 0) {%>
            <table>    
                <tr>       
                    <td>Transaction Id</td>
                    <td>Account</td>
                    <td>Beneficiary Account</td>
                    <td>Amount</td>
                    <td>&nbsp;</td>
                </tr>
                <%

                    for (int i = 0; i < list.size(); i++) {
                        init_account = list.get(i)[0];
                        ben_acc = list.get(i)[1];
                        amt = list.get(i)[2];
                        trans_id = list.get(i)[3];
                %>


                <tr> 
                    <td>     <%=trans_id%></td>
                    <td>     <%=init_account%></td>
                    <td>     <%=ben_acc%></td>
                    <td>     <%=amt%></td>


                    <td>   
                        <input type="button" value="View Detail" class="button" onClick="getdetail('get_ft_details', '<%= trans_id%>')" /> 
                    </td

                    </tr>
                    <%}%>
            </table>
            <% }%>

        </div>

    </body>
</html>
