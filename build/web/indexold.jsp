<%-- 
    Document   : index
    Created on : 28-Nov-2009, 21:42:18
    Author     : 
--%>

<%@ page language="java" contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-layout.tld" prefix="layout" %>
 </html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
         
        <title>E-Banking</title>
         <link rel="shortcut icon" href="img/logo.JPG">
        <style media="all" type="text/css">@import "css/all.css";</style>
          <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
          <script language="javascript" type="text/javascript">
              $(document).ready(function(){
           $('#custom1').keyboard({
		layout: 'custom',
		customLayout: {
			'default' : [
				'C D E F',
				'8 9 A B',
				'4 5 6 7',
				'0 1 2 3',
				'{bksp} {a} {c}'
			]
		},
		maxLength : 6,
		restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		useCombos : false // don't want A+E to become a ligature
	});
              }
          </script>
    </head>
    <%@page import="org.apache.struts.action.Action"%>
    <%
   //session.setAttribute(org.apache.struts.action, request.getLocale());
   // session.setAttribute(
    //onload="showinit('./Login.jsp');"
    %>

    <body onload="showinit('./Login.jsp');">
         <div id="main" >
             
            <div id="loginheader">
                  <a href="./index.jsp" class="logo"><img src="img/fasyl_image.png" width="250" height="55" alt="" /></a>
                 <a href="" class="connection"><img src="img/globe.gif" width="36" height="36" alt="globe" /></a>
            </div>
            <div id="loginmiddle">
                <div id="left-column">
                </div>
                <div id="Login-column">
                    <div class="table">
                        <input type="button" class="buttons" onclick="showinit('./Login.jsp');" value="Login">
                        
                    </div>
                    
                    </div>
                        
                </div>
                 <div class="footermessage">
                    www.fasylgroup.com
                </div>
                <c:import url="/footer.jsp"/>
                 
            </div>
       
  </body>

    </body>
</html>

