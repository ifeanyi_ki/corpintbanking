<%-- 
    Document   : welcomepage
    Created on : 22-Jun-2015, 15:17:36
    Author     : Ayomide
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trust Bond</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css">
        <link href="screens/css/accordioin.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="hm_global">
	<div class="wrapper">
        <div class="header">
            <div class="head">
                <div class="logo"><img src="screens/images/logo.jpg" alt="Trust Bond"></div>
                <div class="clear"></div>
            </div>
            <div class="trustmenu">
              <div class="welcome"><h2>Welcome to Corporate Internet Banking</h2></div>
              <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="logscreen">
            <img src="screens/images/logscreen.jpg" alt="" />
            <div class="loglead">
                <h2>Login today for Internet Banking</h2>
                <p>Easy, safe and convenient Internet Banking is just a few clicks away. 
                    Login online in minutes.</p>
                <p class="logbtn"><a href="Login.jsp">LOGIN NOW</a></p>
            </div>
            <div class="logbullet">
                <h3>Internet Banking made easy</h3>
                <ul class="bullist">
                        <li>Secure</li> 
                    <li>24/7 Funds Access</li>
                    <li>Real-time</li>
                </ul>
            </div>

            <div class="clear"></div>
            <div class="letus">
                <h2>Let's take care of your <span>banking needs</span></h2>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="footer plus">
    	<p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
        <p>2015 © TrustBond Mortgage Bank Plc.</p>
    </div>
<div class="clear"></div>
</div>
    </body>
</html>
