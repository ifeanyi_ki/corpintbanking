<%-- 
    Document   : newHome
    Created on : 22-Jun-2015, 11:36:19
    Author     : Ayomide
--%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TrustBond Mortgage Bank Plc :: Internet Banking</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css">
        

    </head>
    <body marginheight="20" marginwidth="10" onload="toggles();
                hidediv('passwd', true);" onunload="onCloses();">
       <div class="hm_global">
	<div class="wrapper">
            <c:import url="/Header.jsp"/>
        <div class="maincontainer">
        	<div class="sidebar">
            	<c:import url="/menu.jsp"/>
                <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                    </div> <!-- End #messages -->
            </div>
            <div class="maincontent" id="main-content">
            	
                <div class="attention">
                	<span>The maximum amount withdrawable for BASE CURRENCY LIMIT is 1000000</span>
                <div class="clear"></div>
                </div>
                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                            Download From <a href="http://www.exet.tk">exet.tk</a></div>
                    </div>
                </noscript>
              <div id="scrloader">
                    <c:import url="/default.jsp"/>
              </div>
            </div>
        <div class="clear"></div>
        </div>
    <div class="clear"></div>
    <div class="adbanner"><a href="#"><img src="screensimages/adbanner03.jpg" alt="Let's take care of your banking needs"></a></div>
    </div>
    <div class="footer">
    	<p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
        <p>2015 © TrustBond Mortgage Bank Plc.</p>
        <p> Powered by <a href="http://fasylgroup.com" target="blank">FASYL Technology Group</a></p>
    </div>
<div class="clear"></div>
</div>
 
    </body>
</html>
