

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="trx_detail">
        <div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Authorise Service/Product</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">List of Services</a></li> <!-- href must be unique and match the id of target div -->
						
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
						
        <table>
							
							<thead>
								<tr>
								   <th>Affilate Name</th>
								   <th>Affiliate Code</th>
                                                                   <th>Country Code</th>
                                                                   <th>Country Name</th>
								   <th>Added By</th>
								   <th>Added Date</th>
                                                                   <th>Action</th>
								</tr>
								
							</thead>
						 
							<tfoot >
							        <tr><td>
                    <div id="Pagination" class="pagination">
                        
                    </div>
                               </td></tr>	
								
							</tfoot>
						 
							<tbody id="pageindex">
                                                               
							</tbody>
							
						</table>
                                            <input type="text" value="<%=((String)request.getAttribute("others"))%>" id="error" name="error" />             
					</div>
                                </div>
        </div>
        </div>
                                                            
                                                            <div  style="display: none">
                                                                <table >
                                                                    <tbody id="hiddenresult">
                                                               <c:set var="xml" value="${data}" />


                                                               <x:parse varDom="doc" xml="${xml}" />
                                                            <x:forEach var="row" select="$doc/ROWSET/ROW">
								<tr>
									<td><input type="hidden" value="<x:out select="$row/SERVICE_CODE" />" /><x:out select="$row/SERVICE_ACCOUNT" /></td>
									<td><x:out select="$row/SERVICE_NAME" /></td>
									<td><a href="#" title="title"></a><x:out select="$row/SERVICE_DESCRIPTION" /></td>
                                                                        <td><x:out select="$row/EMAIL" />
									<td><x:out select="$row/ADDED_BY" /></td>
									<td>
                                                                            <c:set var="cd2"  >
                                                <x:out select="$row/ADDED_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                               <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>
                                                                         
                                                                        </td>
									<td>
										<!-- Icons -->
										 <a href="#" title="Delete" onclick="delete('30052','30051')">Delete</a>
										 <a href="#"  title="edit" onclick="edit('3003','1')">edit</a>  
										 
									</td>
								</tr>
								
                                                            </x:forEach>
							</tbody>
                                                                </table>
                                                            </div>
    </body>
</html>
