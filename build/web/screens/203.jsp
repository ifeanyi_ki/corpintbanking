

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<body width="630" height="360" >

    <div  class="tab-content" id="tab2" >
        <div class="content-box-header">

            <h3>Mini <bean:message key="label.AccountStatement"/></h3>

            <div class="clear"></div>
            <input type="hidden" id="screenId" value="203"/>
            <input type="hidden" id="taskId" value="4800"/>
            <input type="hidden" id="txnId" value="AC"/>
            <input type="hidden" name="check" id="check" value="S"/>
        </div>
        <form Name="TxnFrm">
            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <!-- End .content-box-header -->
            <p>

                <label><bean:message key="label.AccountCode"/></label>
                <select name="acct_no" id ="acct_no" onchange="showTrx(4800)" class="small-input">
                    <option value="" selected style="display:none">Select an account</option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                    </x:forEach>
                </select>
            </p>


            <div align="right"> <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"></div>

        </form>

        <Div id="trx_detail1"></Div>
        <Div id="trx_detail"></Div>


    </div>
</body>

<%

    {
    }
%>
