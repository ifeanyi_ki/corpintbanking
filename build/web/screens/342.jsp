

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    <script language="JavaScript" src="scripts/screens/common.js"/>
        <script language="JavaScript">


                    //-----------------------------------------------------------------------------

                    //-----------------------------------------------------------------------------
        </script>
    </head>
    <body>
        <c:set var="xml" value="${data}" />


        <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3><bean:message key="label.SITittle"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">
             <c:set var="chsourcecount">
                    <x:out select="$doc/ROWSET/ROW/ACCOUNT"/>
             </c:set>
                    <c:choose>

                <c:when test="($chsourcecount == '') or ($chsourcecount == null)">
                    <table width="90%" align="center" class="TableBorder">
                        <tr>
                            <td class="Warning">
				<bean:message key="label.noaccount"/>
                            </td>
                        </tr>
                    </table>
                </c:when>
                <c:otherwise>
                    <script language="JavaScript">
                        l_boolean = true;
                    </script>
               
                    <p>
                        <label><bean:message key="label.SIType"/></label>
                         <select name="fldSiType" id="fldSiType" class="small-input" >
                                    <x:forEach var="row" select="$doc/ROWSET/SITYPE">
                                        <option value= "<x:out select="PRODUCTCODE"/>" ><x:out select="PRODUCTDESCRIPTION"/> </option>
                                    </x:forEach>

                                </select>
                                <input type="hidden" name="SiTypeDesc" value=""/>
                    </p>
                    <p>
                        <label><bean:message key="label.SISourceAcct"/></label>
                        <select name="dSrcAcctNo" id="dSrcAcctNo" class="text-input small-input" onchange="updateSICCY(this.value,'SiTrfCurr1');">
                            <option  style="display: none;">Select an account</option>
                                 <x:forEach var="row" select="$doc/ROWSET/ROW">
                                     <option data-ccys="<x:out select="CCY"/>" value= "<x:out select="ACCOUNT"/>" ><x:out select="ACCOUNT"/> </option>
                                    </x:forEach>


                                </select>
                                <input type="hidden" name="accountBranch" id="accountBranch" value="<x:out select="$doc/ROWSET/ROW/BRANCH_CODE"/>"/>
                    </p>
                    <p>
                        <label><bean:message key="label.Destinationaccount"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo"  value="" onchange="siAcctDetails('001','342');"/>
                    <div id="crbranchcode"></div>
                        <input type="hidden" value="" id="crbrcode" name="crbrcode"/>
                    </p>
                      
                    <p>
                        <label><bean:message key="label.TransferAmount"/></label>
                        <input class="text-input small-input" type="text" name ="SiTrfAmt" id="SiTrfAmt"  value =""  />
                    </p>
                    <p>
                        <label><bean:message key="label.TransferCurrency"/></label>
                        <select name="SiTrfCurr1" id="SiTrfCurr1" class="text-input small-input">
                            <option value="-1" style="display: none;" >Select SI Currency</option>

                                     <x:forEach var="row" select="$doc/ROWSET/ROW">
                                        <option value= "<x:out select="CCY"/>" > <x:out select="CCY"/></option>
                                    </x:forEach>

                                </select>
                    </p>

                    <p>
                        <label><bean:message key="label.SIFrequency"/></label>
                        <select name="SiFreqDesc"id="SiFreqDesc" class="text-input small-input">
                                    <option value ='1'>Daily </option>
                                    <option value ='2'>Weekly</option>
                                    <option value ='3'>Monthly</option>
                                    <option value ='4'>Quarterly</option>
                                    <option value ='5'>Semi Annually</option>
                                    <option value ='6'>Yearly</option>

                                </select>
                                <input type="hidden" name="SiFreqDesc" value=""/>
                    </p>
                    
                   <!-- <p>
                        <input type = 'checkbox' name ='fldSiPeriodFlg' value="N" onClick="return checkFreq ();"/>                    </p>
                    <p>-->
                   <p>
                        <label><bean:message key="label.SIPriority"/></label>
                        <!--<input class="text-input small-input" type="text" name ="SiPriority" id="SiPriority"  value = '' onfocus="spiner('SiPriority');"-->
                    <select name="SiPriority"id="SiPriority" class="text-input small-input">
                                    <option value ='1'>1 </option>
                                    <option value ='2'>2</option>
                                    <option value ='3'>3</option>
                                    <option value ='4'>4</option>
                                    <option value ='5'>5</option>
                                    <option value ='6'>6</option>
                                    <option value ='7'>7</option>
                                    <option value ='8'>8</option>
                                    <option value ='9'>9</option>   
                                    <option value ='10'>10</option>
                                    
                                </select>
                   </p>
                     <p>
                        <label><bean:message key="label.FirstExecDate"/></label>
                        <input class="text-input small-input" type="text"  name ="FirstExecDate" id="FirstExecDate" value = '' onmousedown="pickDate('FirstExecDate');" onblur="pickDate('FirstExecDate');" onfocus="pickDate('FirstExecDate');"/>
                    </p>
                    <p>
                        <label><bean:message key="label.sitenure"/></label>
                         <input class="text-input small-input" type="text"  name ="sitenure" id="sitenure" value = '' onchange="addMonthsToDate();"/><small>month(s)</small>
                    </p>
                    <!--<Div id="trx_detail"></Div>-->
                     <p>
                        <label><bean:message key="label.SIFinalDate"/></label>
                        <input class="text-input small-input" disabled="disabled" type="text"  name ="SiFinalDate" id="SiFinalDate" value = ''/>
                    </p>
                    <p>
                        <label><bean:message key="label.Narration"/></label>
                        <textarea class="text-input textarea wysiwyg" id="SiNarration" name="SiNarration" cols="20" rows="10"></textarea>

                    </p>
                    <p>
                        <input type="button"  class="button" value="Submit" onClick="getSI(3421,342)" >
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                    </p>

               
                </c:otherwise>
                    </c:choose>



            </form>
        </div>

                <div id="ajaxload"></div>
    </body>
</html>
