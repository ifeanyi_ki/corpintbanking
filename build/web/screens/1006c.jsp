
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="application/msword" %>
<% response.setHeader("Content-Disposition", "attachment; filename=\"statement.doc\""); %>
<%@page import="com.fasyl.ebanking.logic.CustomerTrx" %>


<%              System.out.println("=============== inside 1006c ==================");
            java.sql.Connection connection = null;
            java.sql.CallableStatement callable = null;
            String data = null;
            String scn = request.getParameter("taskId") + ".jsp";
            //String acct =(String) session.getAttribute("acct_no");
            String acct=request.getParameter("acct_no");
            //acct = "008-810002-10-00-02";
                System.out.println(acct+"This is d acct");
            String startDate = request.getParameter("fldFromDate");
            String endDate = request.getParameter("fldToDate");
            String from = request.getParameter("fldFromAmount");
            String to = request.getParameter("fldToAmount");
            String drcr = request.getParameter("fldDrCr");
            String order = request.getParameter("order");
             System.out.println(acct+"This is d acct"+ " startdate " + startDate);
            CustomerTrx custtrx = new CustomerTrx();
            data = custtrx.getTrxXml(acct, startDate, endDate, from, to, drcr,order); 
            /*try {
                connection = com.fasyl.ebanking.db.DataBase.getConnection();
                //System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
                callable = connection.prepareCall("{?=call FN_GET_TRX(?,?,?,?,?,?)}");
                callable.registerOutParameter(1, oracle.jdbc.OracleTypes.CLOB);
                callable.setString(2, acct);
                callable.setString(3, startDate == null ? "N" : startDate);
                callable.setString(4, endDate == null ? "N" : endDate);
                callable.setString(5, from == null ? "0" : from);
                callable.setString(6, to == null ? "0" : to);
                callable.setString(7, drcr == null ? "N" : drcr);
                callable.executeQuery();
                java.sql.Clob datas = callable.getClob(1);
                data=datas.getSubString(1, (int)datas.length());*/
                pageContext.setAttribute("data", data);
                System.out.println("*** this is data **** " + data);
                //out.print(data);
                //out.print(data);



%>




<html>
     <head>
       <META HTTP-EQUIV="expires"CONTENT="0">
        <meta http-equiv="Content-Type" content="application/vnd.ms-excel; charset=UTF-8">

        <title>JSP Page</title>
         <script type="text/javascript">

/*function loadPrintableVersion()

{

	window.open('1006c.jsp', 'newWin', 'scrollbars=yes,status=no,menubar=yes,width=1000,height=635');

	//document.submit();



}
 //End "opennewsletter" function*/

</script>

    </head>
     <body>
         <form  method="POST" action="/MainController">

             <c:if test="${data!=null}">



<!--<div class="table">-->




   <table class="listing" cellpadding="0" cellspacing="0">
        
            <th width="10%">Trx Date</th>
            <th width="30%" >Ref No</th>
            <th width="50%" >Description</th>
            <th width="5%" >Debit</th>
            <th width="5%" >Credit </th>
            <th width="10%" >Running Balance</th>

            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
 <x:forEach var="row" select="$doc/ROWSET/ROW">

            <tr style="border:1px solid black">
                    <td style="border:1px solid black;padding-left: 2px">
                    <x:set var="c" select="$row/TXN_INIT_DATE"/>
                    <c:set var="cd2"  >
                                                <x:out select="$row/TXN_INIT_DATE"/>
                                            </c:set>
                    <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                    <label><fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/></label>
                    <input type="hidden" name="acno" id="acno" value="<x:out select="AC_NO" />"/>
                           <input type="hidden" name="taskId" id="taskId" value="1006d"/>
                           <input type="hidden" name="acctno" id="acctno" value=<%=acct%>/>
                </td>

                <td style="border:1px solid black;padding-left: 2px">
                    <x:set var="c" select="$row/REFNO"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select=""/>
                            </x:otherwise>
                        </x:choose>
                </td>
                
                
                 <td style="border:1px solid black;padding-left: 2px">
                    <x:set var="c" select="$row/TRN_DESC"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select=""/>
                            </x:otherwise>
                        </x:choose>
                </td>

                  <td style="border:1px solid black;padding-left: 2px">
                        <x:set var="c" select="$row/DRCR_IND"/>
                         <x:set var="cy" select="$row/AC_CCY"/>
                        <c:set var="check">
                            <x:out select="$c"/>
                        </c:set>
                          <c:set var="currency">
                            <x:out select="$cy"/>
                        </c:set>

                        <c:choose>
                            <c:when test="${check=='D'}">
                                <c:if test="${currency=='NGN'}" >
                                       <c:set var="fomt">
                                    <x:out select="$row/LCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol=""  />
                                </c:if>
                                  <c:if test="${currency!='NGN'}" >
                                     <c:set var="fomt">
                                    <x:out select="$row/FCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                </c:if>

                            </c:when>
                            <c:otherwise>
                                <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                     <td style="border:1px solid black;padding-left: 2px">
                        <x:set var="c" select="$row/DRCR_IND"/>
                        <x:set var="cy" select="$row/AC_CCY"/>
                        <c:set var="check">
                            <x:out select="$c"/>
                        </c:set>
                        
                         <c:set var="currency">
                            <x:out select="$cy"/>
                        </c:set>
                        <c:choose>
                            <c:when test="${check=='C'}">
                                <c:if test="${currency=='NGN'}" >
                                    
                                      <c:set var="fomt">
                                    <x:out select="$row/LCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                    
                                </c:if>
                                  <c:if test="${currency!='NGN'}" >
                                  
                                      <c:set var="fomt">
                                    <x:out select="$row/FCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                </c:if>

                            </c:when>
                            <c:otherwise>
                                <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                    </td>

                    <!-- added by K.I as requested by TB -->
                    <!--running balance column begins here -->
                    <td style="border:1px solid black;padding-left: 2px">
                    <x:set var="rb" select="$row/RUNNING_BALANCE"/>
                    <c:set var="check">
                        <x:out select="$rb"/>
                    </c:set>

                        <x:choose >
                        <x:when select="$rb">
                            <x:out select="$rb"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select=""/>
                            </x:otherwise>
                        </x:choose>
                </td>     
                    <!--running balance column ends here -->
            </tr>
            </x:forEach>
           
    <!--<input type="button"  class="buttons" value="Print"  name="Print"  onClick="window.print();return false; ">-->
    </table>

            </c:if>


         </form>
</body>

</html>

<%
           /* } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    callable.close();
                    connection.close();
                } catch (Exception e) {
                }
            }*/
%>