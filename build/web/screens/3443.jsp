<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <fieldset>
             <c:set var="xml" value="${data}"/>
        <x:parse varDom="doc" xml="${xml}" />
            <x:forEach var="row" select="$doc/RESULT">
                    <c:set var="cc" >
     <x:out select="$doc/RESULT/ERRORMESSAGE" />
    </c:set>
            <c:if test="${cc==null || cc==''}">
        <table>

            <tr>

                        <td width="500" class="LblIPMand" height="20" nowrap><bean:message key="label.siclosesuccess"/></td>

                    </tr>
                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
                    
        </table>
                      </c:if>
            <c:if test="${cc!=null||cc!=''}">
                <table>
                    <tr>
                       <td width="200" class="LblIPMand" height="20" nowrap> <c:out value="${cc}"/></td>
                    </tr>
                </table>
            </c:if>
             </x:forEach>
        </fieldset>

    </body>
</html>
