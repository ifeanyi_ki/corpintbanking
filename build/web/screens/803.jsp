<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
                <div align="center" >
        <div class="content-box">
       
            <div class="content-box-header">
  
                <h3><h3><bean:message key="label.servicesetup"/></h3></h3>

					<ul class="content-box-tabs" >
						<li><a onclick="return showAuth(803,'1');"  class="default-tab">Create Services</a></li> <!-- href must be unique and match the id of target div -->
						<li><a  onclick="return ManageServiceSetup(8034,'803');">Manage Services</a></li>
					</ul>
                <div class="clear"></div>

            </div >
            <div align="left" style="margin-left: 10px;height: 500px" id="trx_detail">
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">


                    <p>
                        <label><bean:message key="label.choosecategory"/><font color="red">*</font></label>
                       <select name="categoryid" id ="categoryid" onchange="GetServForm(8031,'803')" class="small-input">
                       <option value="" selected style="display:none"></option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CATEGORY_ID" />"><x:out select="CATEGORY_NAME" /></option>
                    </x:forEach>
                </select>
                    </p>

              
                    



            </form>
                 <div id="trx_detail2">

                            </div>
                        <div id="searchresult">

                            </div>        </div>
        </div>
                </div>
    </body>
</html>
