

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<body width="630" height="360" >
    <script>
        function setFormAction(fileType) {
            if (fileType == 'pdf') {
                alert('why1');
                document.frmStmtInq.action = "screens/1006b.jsp?";

            } else if (fileType == 'excel') {
                document.frmStmtInq.action = "screens/1006d.jsp?"
                        ;
            } else if (fileType == 'rtf') {
                document.frmStmtInq.action = "screens/1006c.jsp?"

            }
        }

    </script>

    <c:set var="acct" value="${acct}" />

    <%
        session.setAttribute("acctno", request.getAttribute("acct"));
    %>
    <div  class="tab-content" id="tab2" >
        <div class="content-box-header">

            <h3><bean:message key="label.TxnHistory"/></h3>

            <div class="clear"></div>

            <input type="hidden" id="screenId" value="204"/>
            <input type="hidden" id="taskId" value="1006"/>
            <input type="hidden" name="check" id="check" value="S"/>
        </div>

        <form method="POST"  Name="frmStmtInq">
            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <!-- End .content-box-header -->

            <input type="hidden" name="acctno" id="acctno" value="<c:out value="${acct}"/>">
            <input type="hidden" name="checks" id="checks" value="${drcr}">
            <input type="hidden" name="startDate" id="startDate" value="${startDate}">
            <input type="hidden" name="endDate" id="endDate" value="${endDate}">
            <input type="hidden" name="from" id="from" value="${from}">
            <input type="hidden" name="to" id="to" value="${to}">
            <p>

                <label><bean:message key="label.AccountCode"/></label>
                <select name="acct_no" id ="acct_no"  class="small-input column-left">
                    <option value="" selected style="display:none">Select an account</option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                    </x:forEach>
                </select>

            </p>
            <p>
                <br />
                <label>
                    Ordering
                </label>
                <select id="order" name="order" class="small-input column-left">
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                </select>
            </p>
            <div class="clear"></div>
            <p class="column-right"></p>
            <div class="clear"></div>
            <p>&nbsp;&nbsp;</p>


            <div id="dettrxinfo">

                <%--<p >
                    <bean:message key="label.CurrentPeriod"/><input  type="radio" name="fldPeriod" value="0" CHECKED = "true" onClick="Inits()">
                </p>
               <p>&nbsp;</p>
               <hr />--%>

                <p>
                    <bean:message key="label.SpecifiedPeriod"/> <input type="checkbox" name="fldPeriod" value="1"  onClick="selectDates()">
                    &nbsp;&nbsp
                    <bean:message key="label.StartDate"/>&nbsp;&nbsp;&nbsp;<input class="text-input small-input" type="text" name="fldFromDate" id="fldFromDate" maxlength="12" onfocus="pickDate('fldFromDate'), disabledatebox(this);" onmouseover="pickDate('fldFromDate')"/>
                    &nbsp;&nbsp
                    <bean:message key="label.EndDate"/>&nbsp;&nbsp;<input class="text-input small-input" type="text" name="fldToDate" id="fldToDate" maxlength="12" onfocus=" pickDate('fldToDate'), disabledatebox(this);" onmouseover="pickDate('fldToDate')"/>
                </p>
                <p>&nbsp;</p>
                <hr />

                <p>
                    <bean:message key="label.Amount"/> <input type="checkbox" name="fldPeriod" value="2"  onClick="selectAmounts()">
                    &nbsp;&nbsp
                    <bean:message key="label.FromAmount"/>&nbsp;&nbsp; <input class="text-input small-input" type="text" name="fldFromAmount" maxlength="10" onfocus="return disableAmountbox(this)"/>
                    &nbsp;&nbsp
                    <bean:message key="label.ToAmount"/>&nbsp;&nbsp;<input class="text-input small-input" type="text" name="fldToAmount" maxlength="10" onfocus=" return disableAmountbox(this)"/>
                </p>
                <p>&nbsp;</p>
                <p>
                    <bean:message key="label.both"/> <input  type="radio" name="fldDrCr" value="B"   onClick="return trxradiovalid(), disableAmountbox(this)"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <bean:message key="label.Debit"/> <input  type="radio" name="fldDrCr" value="D"  onClick="return trxradiovalid(), disableAmountbox(this)" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <bean:message key="label.Credit"/><input  type="radio" name="fldDrCr" value="C" onClick=" return trxradiovalid(), disableAmountbox(this)"/>
                </p>
                <hr />
                <input type="button"  class="button" value="Submit" onClick="showTrx(1006)" />


            </div>
            <Div id="trx_detail"></Div>

        </form>

    </div>
</body>


