


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<p>&nbsp;&nbsp;</p>

<div  class="tab-content" id="tab1">
  <div class="content-box-header">

                <h3><bean:message key="label.rate"/></h3></div>

                <div class="clear"></div>
    <div class="clear"></div>
    <fieldset>
        <legend></legend>


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                            <th class="first"><bean:message key="label.TxnDate"/></th>
                            <th><bean:message key="label.Description"/></th>
                            <th><bean:message key="label.DebitAmount"/></th>
                            <th><bean:message key="label.CreditAmount"/></th>

                        </tr>

                    </thead>

                    <tfoot>
                        <tr>
                            <td colspan="6">


                                <div class="pagination">
                                    <a href="#" title="First Page">&laquo; First</a><a href="#" title="Previous Page">&laquo; Previous</a>
                                    <a href="#" class="number" title="1">1</a>
                                    <a href="#" class="number" title="2">2</a>
                                    <a href="#" class="number current" title="3">3</a>
                                    <a href="#" class="number" title="4">4</a>
                                    <a href="#" title="Next Page">Next &raquo;</a><a href="#" title="Last Page">Last &raquo;</a>
                                </div> <!-- End .pagination -->
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <c:set var="xml" value="${data}" />

                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <c:set var="count" value="${count+1}"/>
                            <c:if  test="${count<=20}">
                                <tr>
                                    <td>
                                        <x:set var="c" select="$row/VALUE_DT"/>
                    <label><input name="refno" id="refno" type="radio" value="<x:out select="REFNO" />"  onclick="getTrxDetail(this.value,'1006a');return false"/><x:out select="$c" /></label>
                    <input type="hidden" name="acno" id="acno" value="<x:out select="AC_NO" />"/>
                           <input type="hidden" name="taskId" id="taskId" value="1006a"/>
                           <input type="hidden" name="taskId" id="screenId" value="1"/>
                                    </td>
                                    <td>
                                    <x:set var="c" select="$row/TRN_DESC"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select=""/>
                            </x:otherwise>
                        </x:choose>
                                    </td>
                                    <td>
                                               <x:set var="c" select="$row/DRCR_IND"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <c:choose>
                            <c:when test="${check=='D'}">
                            <x:out select="$row/LCY_AMOUNT"/>

                        </c:when>
                            <c:otherwise>
                            <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                                            </td>
                                            <td>
                                            <x:set var="c" select="$row/DRCR_IND"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <c:choose>
                            <c:when test="${check=='C'}">
                            <x:out select="$row/LCY_AMOUNT"/>

                        </c:when>
                            <c:otherwise>
                            <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                                            </td>
                                </tr>
                            </tbody>
                        </c:if>
                    </x:forEach>

                </table>

            </fieldset>
            <table>
                <tr>

                    <c:if test="${lowcount>0}">
                        <td>
                            <div align="left">

                                <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                            </div>
                        </td>
                    </c:if>
            </table>
            <c:if test="${size>increment && highcount<size}">


                <div align="center">

                    <input type="submit" name="Submit" value="next" onClick="getNextValue(1011,'101');" class="button">

                </div>


            </c:if>


            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>




