
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
            <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>


        <div  class="tab-content" id="tab2" align="left">
            <input type="hidden" id="checkamount" value=""/>
             <input type="hidden" id="checkno" value=""/>
            <div class="content-box-header">

                <h3><bean:message key="label.ChequeBkrequest"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST" name="chequebookreq">

                <p>
                    <label><bean:message key="label.AccountCode"/></label>
                    <select name="acct_id" id ="acct_id" class="small-input">
                        <option value="" selected style="display:none">Select an account</option>
                        <c:set var="xml" value="${data}" />
                        <x:parse varDom="doc" xml="${xml}" />
                        <x:forEach var="row" select="$doc/ROWSET/ROW" >
                            <option value="<x:out select="$row/ACCOUNT" />"><x:out select="$row/ACCOUNT" /></option>
                        </x:forEach>
                    </select>
                </p>

                <p>
                    <label><bean:message key="label.BookletSize"/></label>
                    <select name="leave" id ="leave" class="small-input">


                        <option value="" selected style="display:none">Select number of leaves</option>
                        <x:forEach var="row" select="$doc/ROWSET/LEAVESNO">
                            <option value="<x:out select="$row/LEAVES_NO"/>"  ><x:out select="$row/LEAVES_NO"/></option>
                        </x:forEach>
                    </select>
                </p>
                <p>
                    <label>Number of Booklet</label>
                    <input type="text" value="1" class="text-input small-input"  id="bookletno" name="bookletno" width="1px"/>
                </p>
                 <p>
                      <input type="hidden" id="checkno2" class="text-input small-input" name="checkno2"   size="30" value=""/>
                    <label style="display: inline-block;width: 22%;">Personal</label>
                    <input type="checkbox" value="" class="text-input" id="personal" name="personal" onclick="validateCheckbox(true);"/>
                 </p>
                 <p>
                    <label style="display: inline-block;width: 22%;">Corporate</label> 
                    <input type="checkbox" value="" class="text-input" id="corporate" name="corporate" onclick="validateCheckbox(false);"/>
                 </p>
                 <br /><br />
                 <small>Kindly note that this operation will attract charges.</small>
                <p>
                    <input type="button"  class="button" value="Submit" onClick="showDivs(3221,'322')" >
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>







            </form>
            <!--<Div id="trx_detail"></Div>-->
        </div>