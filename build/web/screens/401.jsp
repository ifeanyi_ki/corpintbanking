
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html >
    <head>


    </head>

    <body>
        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3><bean:message key="label.searchapperror"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">

                <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                       Enter Date range and click <b>SEARCH</b> to view the application log.
                    </div>
                </div>


                <p>
                    <label><bean:message key="label.from"/></label>
                    <input class="text-input small-input" type="text" id="from" onfocus="pickDate('from');" onmouseover="pickDate('from');" />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>

                <p>
                    <label><bean:message key="label.to"/></label>
                    <input class="text-input small-input" type="text" id="to" name="to" onfocus="pickDate('to');" onmouseover="pickDate('to');" />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>
                <p>
                    <input type="button" onclick="getAppError('401','4011');" value="<bean:message key="label.search"/>" class="button">
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>

                <p>

                </p>

            </form>
            <div id="trx_detail"  >

            </div>
            <div id="searchresult">

            </div>


        </div>


    </body>
    <html>