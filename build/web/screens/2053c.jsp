


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ page contentType="application/vnd.ms-excel" %>
<%@ page import="com.fasyl.ebanking.logic.GetLoanInfo"%>
<% response.setHeader("Content-Disposition", "attachment; filename=\"loan.doc\""); %>

<%
java.sql.Connection connection = null;
            java.sql.CallableStatement callable = null;
            String data = null;
            String ref = request.getParameter("ref");
            System.out.println(" This is the ref for the loan "+ref);
            data = (new GetLoanInfo()).getLoanRepayment(ref);
            pageContext.setAttribute("data", data);
            System.out.println("*** this is data **** " + data);
    
%>
<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<p>&nbsp;&nbsp;</p>

<div  class="tab-content" id="tab1">
  <div class="content-box-header">

              <!-- <h3><bean:message key="label.loanschedule"/></h3></div>-->

                <div class="clear"></div>
    <div class="clear"></div>

      


    <table>
        
                    <thead>
                        <tr>
                            <th class="first">Component Name</th>
                             <th>Due Date</th>
                            <th>Amount Due</th>
                            <th>Amount Settled</th>
                            <th>Amount Overdue</th>
                           
                          

                        </tr>

                    </thead>
        <tbody id="hiddenresult">
            <c:set var="xml" value="${data}" />

                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            
                                <tr style="border:1px solid black">
                                    <td style="border:1px solid black;padding-left: 2px">
                                        <x:out select="$row/COMPONENT_NAME"/>


                                    </td>
                                    
                                      <td style="border:1px solid black;padding-left: 2px">
                                          <c:set var="cd2"  >
                                                <x:out select="$row/SCHEDULE_DUE_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd" var="formatedDate"/>
                                             
                            
                                                <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy" />
                                        

                                    </td>
                                    
                                    <td style="border:1px solid black;padding-left: 2px">

                            <x:out select="$row/AMOUNT_DUE"/>

                                            </td>
                                            <td style="border:1px solid black;padding-left: 2px">
                            <x:out select="$row/AMOUNT_SETTLED"/>
                                            </td>
                                            <td style="border:1px solid black;padding-left: 2px">

                            <x:out select="$row/AMOUNT_OVERDUE"/>

                                            </td>
                                            
               
                                            
                                </tr>
                        <input type="hidden" value="<x:out select="$row/ACCOUNT_NUMBER"/>" name="ref" id="ref"/>      
                    </x:forEach>
                          </tbody> 
    </table>           
      

</div>




