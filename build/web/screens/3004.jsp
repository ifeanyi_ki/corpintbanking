

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="trx_detail">
            <div class="content-box"><!-- Start Content Box -->

                <div class="content-box-header">

                    <h3>Authorise Account Added</h3>

                    <ul class="content-box-tabs">
                        <li><a href="#tab1" class="default-tab">List of Accounts</a></li> <!-- href must be unique and match the id of target div -->

                    </ul>

                    <div class="clear"></div>

                </div> <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                        <div class="notification attention png_bg">
                            <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                            <div>
                                Kindly approve or  reject each transaction by clicking the link "Approve" or "Reject"
                            </div>
                        </div>
                        <table>

                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Request ID</th>
                                    <th>Acct Number</th>
                                    <th>Action</th>
                                </tr>

                            </thead>

                            <tfoot >
                                <tr><td>

                                    </td></tr>
                            </tfoot>

                            <tbody id="pageindex">

                            </tbody>

                        </table>
                        <div id="Pagination" >

                        </div>              
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
            <table>

                <tbody id="hiddenresult">
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />

                    <x:choose>
                        <x:when select="$doc/ROWSET/ROW/NO_DATA">
                            <tr>
                                <td colspan="4"><x:out select="$doc/ROWSET/ROW/NO_DATA" /></td>
                            </tr>
                        </x:when>
                        <x:otherwise>
                            <x:forEach var="row" select="$doc/ROWSET/ROW">
                                <tr>
                                    <td><x:out select="$row/DAT_TXN" /></td>
                                    <td><x:out select="$row/COD_RETRIEVAL_REF" /></td>
                                    <td><a href="#" title="title"></a><x:out select="$row/ACTION_VALUE" /></td>
                                    <input type="hidden" id="<x:out select="$row/COD_RETRIEVAL_REF" />ID" value="<x:out select="$row/ACTION_VALUE" />" />
                                    
                                    <td class="<x:out select="$row/COD_RETRIEVAL_REF" />action">
                                        <!-- Icons -->
                                        <a href="#" title="Approve" onclick="authorise('3004','1','<x:out select="$row/REQUEST_DESC" />','<x:out select="$row/COD_RETRIEVAL_REF" />','<x:out select="$row/REQUEST_PAR" />')">Approve</a> 
                                        | <a href="#" title="Reject" onclick="showRejectReasonDiv(<x:out select="$row/COD_RETRIEVAL_REF" />)">Reject</a> 

                                        <%-- Hidden field below added by Ayo for approval requester_user_email--%>
                                        <input type="hidden" id="<x:out select="$row/COD_RETRIEVAL_REF" />requester" value="<x:out select="$row/COD_REQUESTER_EMAIL" />" />
                                    </td>
                                </tr>
                                <tr id="<x:out select="$row/COD_RETRIEVAL_REF" />rejectRow" style="display: none">
                                    <td colspan="3"><input class="text-input small-input" placeholder="Type reason for rejection here" type ="text" id="<x:out select="$row/COD_RETRIEVAL_REF" />rejectComment" style="box-sizing: border-box;width: 100%" /></td>
                                    <td>
                                        <a href="#" title="Submit" onclick="reject('3004','1','<x:out select="$row/REQUEST_DESC" />','<x:out select="$row/COD_RETRIEVAL_REF" />','<x:out select="$row/REQUEST_PAR" />')">Submit</a> 
                                        | <a href="#" title="Cancel" onclick="cancelReject('<x:out select="$row/COD_RETRIEVAL_REF" />')">Cancel</a>
                                    </td>
                                </tr>

                            </x:forEach>
                        </x:otherwise>
                    </x:choose>
                </tbody>
            </table>

        </div>
    </body>
</html>
