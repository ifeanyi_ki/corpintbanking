


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<form name="viewInboxMail" action="MainController" method="POST" style="border: none; display: none;">

    <input type="hidden" name="reqType" value="I"/>
    <input type="hidden" name="screenId" value="5142"/>
    <input type="hidden" name="taskId" value="51421"/>
    <input type="hidden" name="flg_box" value="I"/>               
    <input type="hidden" id="sessId" name="sessId" value="<%=request.getSession().getId()%>">
    <input type="hidden" id="projectPath" name="projectPath" value="<%= request.getRealPath("")%>"/>

</form>





<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />
<fieldset>

    <form name="frmcontrol" action="MainController" method="post">
        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>

                            <%--<th><input class="check-all" type="checkbox" /></th>--%>
                            <th></th>
                            <th><bean:message key="label.subject"/></th>
                            <th>Active Date</th>
                            <th>Expired Date</th>

                        </tr>

                    </thead>

                    <tfoot id="Pagination">

                    </tfoot>
                    <tbody id="pageindex">



                    </tbody>
                </table>
            </form>
        </fieldset>


        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">


    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>


<div style="display: none">
    <table>
        <tbody id="hiddenresult">
            <x:forEach var="row" select="$doc/ROWSET/ROW">

                <c:set var="count" value="${count+1}"/>
                <c:if  test="${count<=20}">

                    <tr>
                        <td><input type="radio" name="bltnRadio" onclick="viewDetailMessageWithImage('40221','4022',this.value)" value="<x:out  select="MESSAGEID"/>" /><x:out  select="MESSAGEID"/></td>
                        <td>
                            <a onClick=""><x:out  select="TITTLE"/> </a>
                        </td>
                        <td>
                            <c:set var="cd2"  >
                                <x:out select="ACTIVE_DATE"/>
                            </c:set>
                            <fmt:parseDate value="${cd2}" type="DATE" pattern="yy-MM-dd"                var="formatedDate"/>   

                            <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>        
                        </td>
                        <td>
                            <c:set var="cd2"  >
                                <x:out select="EXPIRE_DATE"/>
                            </c:set>
                            <fmt:parseDate value="${cd2}" type="DATE" pattern="yy-MM-dd"                var="formatedDate"/>   

                            <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>  

                        </td>
                    </tr>


                </c:if>
            </x:forEach>
        </tbody>
    </table>
    <div id="ajaxload"></div>
</div>

