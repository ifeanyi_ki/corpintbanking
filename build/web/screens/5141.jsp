<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set var="xml" value="${data}"/>

        <x:parse varDom="doc" xml="${xml}" />
        <x:forEach var="row" select="$doc/ROWSET/ROW">
    <c:set var="cc" >
     <x:out select="$doc/ROWSET/ROW/SUCCESS" />
    </c:set>

    <c:if test="${cc!=null || cc!='' }">

        <fieldset>
        <p>

            <b><x:out select="$doc/ROWSET/ROW/SUCCESS"/></b>

                    </p>
                    <p>
                        &nbsp;&nbsp;
                    </p>

        </fieldset>
</c:if>            <c:if test="${cc==''||cc==null}">
                         <fieldset>


                <tr>

                     <p>
                         <label class="small-input formcolumn-left"><bean:message key="label.error"/></label>
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/ERROR"/>" >
                    </p>
                    <p>
                        &nbsp;&nbsp;
                    </p>
                         </fieldset>
    </c:if>
</x:forEach>

    </body>
</html>

