


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>






    <c:set var="xml" value="${data}" />
    <x:parse varDom="doc" xml="${xml}" />
    <c:set var="error">
        <x:out select="$doc/ROWSET/ROW/NODATA"/>
    </c:set>
    <c:set var="highcount">
        <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
    </c:set>
    <c:set var="lowcount">
        <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
    </c:set>

    <c:set var="size">
        <x:out select="$doc/ROWSET/ROW/SIZE"/>
    </c:set>
    <c:set var="increment" value="20" />

    <div  class="tab-content" id="tab1">
        

            <p class="column-left">
                <b><bean:message key="label.accttypedesc"/>:</b>
            </p>
            <p class="column-right">
                <x:out select="$doc/ROWSET/ROW/CUST_AC_NO"/>
            </p>
            <!--<div class="clear"></div>-->
            <p class="column-left">
                <b><bean:message key="label.AcctHolder"/>:</b>
            </p>
            <p class="column-right">
                <x:out select="$doc/ROWSET/ROW/CUSTOMER_NAME1"/>
            </p>
            <c:set var="block">
               <x:out select="$doc/ROWSET/ROW/AC_STAT_BLOCK"/>
            </c:set>
              <c:set var="dormant">
               <x:out select="$doc/ROWSET/ROW/AC_STAT_DORMANT"/>
            </c:set>
            <c:if test="${block=='Y'}">
              <p class="column-left">
                <b><bean:message key="label.acctstatus"/>:</b>
            </p>
            <p class="column-right">
                Blocked
            </p>
            </c:if>
            <c:if test="${dormant=='Y'}">
              <p class="column-left">
                <b><bean:message key="label.acctstatus"/>:</b>
            </p>
            <p class="column-right">
               <bean:message key="label.acctblock"/>
            </p>
            </c:if>
             <c:if test="${(dormant=='N')&& (block=='N')}">
              <p class="column-left">
                <b><bean:message key="label.acctstatus"/>:</b>
            </p>
            <p class="column-right">
               <bean:message key="label.acctactive"/>
            </p>
            </c:if>

            <div class="clear"></div>
           
            <fieldset>




                <c:choose>
                    <c:when test="${error==null || error!='[]'|| error==' '}">
                        <table>

                            <thead>
                                <tr>
                                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                                    <th><bean:message key="label.Description"/></th>
                                     <th><bean:message key="label.Values"/></th>

                                </tr>

                            </thead>

                            <tfoot>
                               

                                   
                            </tfoot>
                            <tbody>
                                <c:set var="xml" value="${data}" />

                                <c:set var="count" value="0"/>

                                <x:forEach var="row" select="$doc/ROWSET/ROW">

                                    <c:set var="count" value="${count+1}"/>
                                    <c:if  test="${count<=20}">
                                        <tr>
                                            <td>
                                      <bean:message key="label.CurrentBalance"/>
                                            </td>
                                            <td>
                                            <x:set var="c" select="$row/ACY_AVILABLE"/>
                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose></td>
                                        </tr>
                                        <tr>



                    <td>
                        <bean:message key="label.UnclearedFund" />
                    </td >

                    <td align="left">

                <x:set var="c" select="$row/ACY_UNCOLLECTED"/>

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.OverdraftLimit"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/LIMIT_AMOUNT" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>

                </td>


                </tr>
                
                
           
                <tr>



                    <td align="left">
                        <bean:message key="label.MinimumBal"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/MIN_REQD_BAL" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.NetBalWithdrawal"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/NET_AVILABLE_BAL" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                
               
                                    </tbody>
                                </c:if>
                            </x:forEach>
                                    <tr>
                                        <td colspan="2"><input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"></td>
                                    </tr>
                        </table>
 
                    </fieldset>
                    <table>
                        <tr>

                            <c:if test="${lowcount>0}">
                                <td>
                                    <div align="left">

                                        <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                                    </div>
                                </td>
                            </c:if>
                    </table>
                                
                    <c:if test="${size>increment && highcount<size}">


                        <div align="center">

                            <input type="submit" name="Submit" value="next" onClick="getNextValue(1011,'101');" class="button">

                        </div>


                    </c:if>


                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
                </div>
            </c:when>
            <c:otherwise>
                <table>
                    <tr>
                        <td>There  is no data</td>
                    </tr>
                </table>
            </c:otherwise>

        </c:choose>




