

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<%

    int timeout = session.getMaxInactiveInterval();
    response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
%>
<div  >
    <div class="content-box-header">

        <h3><bean:message key="label.beneficiarytype"/></h3>

        <div class="clear"></div>

    </div>
    <form Name="TxnFrm">
        <c:set var="xml" value="${data}" />


        <x:parse varDom="doc" xml="${xml}" />
        <!-- End .content-box-header -->
        <p>
            <label><bean:message key="label.selectbengrp"/><font color="red">*</font></label>
            <select name="selectbengrp" id ="selectbengrp" onchange="listbeneficiaries('6041', 604);" class="small-input">
                <option value="" selected style="display:none">Select beneficiary type / group</option>
                <x:forEach var="row" select="$doc/ROWSET/ROW">
                    <option value="<x:out select="$row/BENEFTGRPNAME"/>"><x:out select="$row/BENEFTGRPNAME"/></option>
                </x:forEach>
            </select>
        </p>
        <input type="hidden" name="bentype" id="bentype" value="GRP">



    </form>

    <div id="trx_detail">

    </div>

</div>


