
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html >

    <head>


    </head>

    <body>
        <input type="hidden" name="trxtypes" id="trxtypes" value="${trxtype}"/>
        <input type="hidden" name="froms" id="froms" value="${from}"/>
        <input type="hidden" name="tos" id="tos" value="${to}"/>

        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3>Report Generator</h3>

                <div class="clear"></div>
                `
            </div> <!-- End .content-box-header -->
            <form action="screens/1006d.jsp?" target="secretIFrame" method="POST" name="frmStmtInq">

                <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        Choose date range, Select report type and click <b>SEARCH</b> to view Report.
                    </div>
                </div>

                <p>
                    <label><bean:message key="label.from"/></label>
                    <input class="text-input small-input" type="text" id="from" name="from" onfocus="pickDate('from');" onmousedown="pickDate('from');" />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>

                <p>
                    <label><bean:message key="label.to"/></label>
                    <input class="text-input small-input" type="text" id="to" name="to" onfocus="pickDate('to');" onmouseover="pickDate('to');"  />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>
                <p>
                    <label>Transaction Type</label>
                    <select id="trxtype" name="trxtype" class="text-input small-input">
                        <option  style="display: none;">Select transaction type</option>
                        <option value="AUMCUR">Newly Created users</option>
                        <option value="AAU">All Users</option>
                        <option value="AUTHAUM">Modified Users</option>
                        <option value="AUTHAAA">Added Account</option>
                        <option value="AUTHAAR">Removed Account</option>
                        <option value="CFLOG">Consecutive Failed Login</option>
                    </select>

                </p>
                <p>
                    <input type="button" onclick="getReport('9011', '901');" value="<bean:message key="label.search"/>" class="button">
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
                </p>
            </form>


            <div id="trx_detail"  >

            </div>


        </div>


    </body>
    <html>