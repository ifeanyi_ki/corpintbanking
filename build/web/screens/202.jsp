


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>







<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<div  class="tab-content" id="tab1">
    <input type="hidden" id="taskId" value="4600"/>
     <input type="hidden" id="screenId" value="201"/>
    <div class="content-box-header">

        <h3><bean:message key="label.AccountSummary"/></h3>
        <div class="clear"></div>
    </div>
        <fieldset style="padding-top: 20px;">
            <!--<img src="./img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
            <img src="./img/bg-th-right.gif" width="7" height="7" alt="" class="right" />-->


            <c:choose>
                <c:when test="${error==null || error!='[]'|| error==' '}">
                    <table>

                        <thead>
                            <tr>
                                <!--<th><input class="check-all" type="checkbox" /></th>-->
                                <th class="first"><bean:message key="label.acctno"/></th>
                                <th><bean:message key="label.Description"/></th>
                                <th><bean:message key="label.Currency"/></th>
                                <th>Balance</th>
                               

                            </tr>

                        </thead>

                        <tfoot>
                            <tr>
                                <td colspan="6">


                                    
                                    <div class="clear"></div>
                                </td>
                            </tr>
                        </tfoot>
                        <tbody>
                            <c:set var="xml" value="${data}" />

                            <c:set var="count" value="0"/>

                            <x:forEach var="row" select="$doc/ROWSET/ROW">

                                <c:set var="count" value="${count+1}"/>
                                <c:if  test="${count<=20}">
                                    <tr>
                                        <td>
                    <x:set var="c" select="CUST_AC_NO"/>

                    <label><input name="refno" id="refno" type="radio" value="<x:out select="CUST_AC_NO" />"  onclick="showDivSumm(4600,'<x:out select="CUST_AC_NO" />');"/><x:out select="$c" /></label>
                    <input type="hidden" name="acct_no" id="acct_no" value="<x:out select="CUST_AC_NO" />">
                </td>
                <td>
                    <x:out select="DESCRIPTION"/>
                </td>

                <td>
                    <x:out select="CCY"/>
                </td>

                <td>
                    <x:out select="ACY_CURR_BALANCE" />
                </td>
            
                                </tbody>
                            </c:if>
                        </x:forEach>

                    </table>

                </fieldset>
                <table>
                    <tr>

                        <c:if test="${lowcount>0}">
                            <td>
                                <div align="left">

                                    <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                                </div>
                            </td>
                        </c:if>
                </table>
                <c:if test="${size>increment && highcount<size}">


                    <div align="center">

                        <input type="submit" name="Submit" value="next" onClick="getNextValue(1011,'101');" class="button">

                    </div>


                </c:if>


                <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
                <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
                <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
                <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
    
        </c:when>
        <c:otherwise>
            <table>
                <tr>
                    <td>There  is no data</td>
                </tr>

            </table>

        </c:otherwise>

    </c:choose>
        <!--<div align="right"> <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"></div>-->
        <Div  id="trx_detail"></Div>




