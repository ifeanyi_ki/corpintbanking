<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>General Payment</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">
                
                     <label><bean:message key="label.AccountCode"/></label>
                          <select name="acct_no" id ="acct_no" class="small-input">
                    <option value="" selected style="display:none"></option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                    </x:forEach>
                </select>
                         <p>
                        <label><bean:message key="label.choosecategory"/><font color="red">*</font></label>
                       <select name="categoryid" id ="categoryid" class="text-input small-input"  onchange="GetServCat('000','805')">
                               <option value="" selected style="display:none"></option>
                               
                              <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CATEGORY_ID" />"><x:out select="CATEGORY_NAME" /></option>
                    </x:forEach>
                            </select>

                    </p>
                       <p>
                        <label><bean:message key="label.chooseservice"/><font color="red">*</font></label>
                       <select name="serviceid" id ="serviceid" class="text-input small-input" onchange="GetProdServ('000','7010')" >
                               <option value=""></option>
                               
                              
                            </select>
                       <input type="hidden" value="" id="serviceid2" name="serviceid2"/>
                       <input type="hidden" value="<x:out select="SERVICE_ACCOUNT" />" name="serviceacct" id="serviceacct"/>
                       <input type="hidden" value="<x:out select="EMAIL" />" name="email" id="email"/>
                       </p>
                
                 
                    <p>
                        <label><bean:message key="label.serviceoption"/><b><font color="red">*</font></b></label>
                        <select name="productid" id ="productid" class="text-input small-input" onchange="populateprodamt(this.value);"  >
                                <option value=""></option>


                            </select>
                   <input type="hidden" value="" id="productid2" name="productid2"/>
                    </p>

                    <p>
                        <label><bean:message key="label.smartcardno"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="smartcardno" id="smartcardno" maxLength="20"  value="" onblur="getAccountDetail('001','332');"/>

                    </p>
                      <p>
                        <label><bean:message key="label.amount"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="amt" id="amt" maxLength="20"  value="" onblur="getAccountDetail('001','332');"/>

                    </p>
                    <p>
                        <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="enterphonenumber" id="enterphonenumber" maxLength="20"  value="" onblur="getAccountDetail('001','332');"/>
                    <input id="token" name="token" type="button" onclick="getPin('70000','1');"  value="<bean:message key="label.getpin"/>" class="button"><div id="trx_detail"></div>
                    </p>

                    <p>
                           <input id="testButton" name="testButton" type="button" onclick="protxn('70100',7010);"  value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                    </p>



       <div id="trx_detail">

                </div>



            </form>
                    <div id="trx_details">

                            </div>
                        <div id="search_result">

                            </div>        </div>
    </body>
</html>
