
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html >
    <head>

      <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>

    <body>
        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3>Search For Unauthorize Service/Product</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">

                <p>
                    <label>Choose Product/Services<b><font color="red">*</font></b></label>
                    <select name="criteria" id ="prdsrv" class="text-input small-input" onchange="listactions(30051,3005);"  >
                        <option value="" selected style="display:none">Select a product / service</option>
                        <option value="AUTHSERV">Services</option>
                        <option value="AUTHPROD">Product</option>

                    </select>
                </p>
                <div class="notification attention png_bg">
                    <a href="#" class="close" onclick="addNoclass();"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        Please select Product or Service.
                    </div>
                </div>
                <div id="trx_detail"></div>           

                </body>
                <html>