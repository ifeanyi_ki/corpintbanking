


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>






    <c:set var="xml" value="${data}" />
    <x:parse varDom="doc" xml="${xml}" />
    <c:set var="error">
        <x:out select="$doc/ROWSET/ROW/NODATA"/>
    </c:set>
    <c:set var="highcount">
        <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
    </c:set>
    <c:set var="lowcount">
        <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
    </c:set>

    <c:set var="size">
        <x:out select="$doc/ROWSET/ROW/SIZE"/>
    </c:set>
    <c:set var="increment" value="20" />

    <p>&nbsp;&nbsp;</p>

    <div  class="tab-content" id="tab1">



            <fieldset>




                <c:choose>
                    <c:when test="${error==null || error!='[]'|| error==' '}">
                        <table>

                            <thead>
                                <tr>
                                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                                    <th><bean:message key="label.Description"/></th>
                                     <th><bean:message key="label.Values"/></th>

                                </tr>

                            </thead>

                            <tfoot>
                                        <tr><td>
                   
                               </td></tr>
                            </tfoot>
                            <tbody>
                                <c:set var="xml" value="${data}" />

                                <c:set var="count" value="0"/>

                                <x:forEach var="row" select="$doc/ROWSET/ROW">

                                    <c:set var="count" value="${count+1}"/>
                                    <c:if  test="${count<=20}">
                                        <tr>
                                            <td>
                                      <bean:message key="label.Account"/>
                                            </td>
                                            <td>
                                            <x:set var="c" select="$row/DFLT_SETTLE_AC"/>
                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose></td>
                                        </tr>
                                        <tr>



                    <td>
                        <bean:message key="label.intrate" />
                    </td >

                    <td align="left">

                <x:set var="c" select="$row/RATE"/>

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.deposittenure"/>
                    </td>

                    <td>
                        <x:set  var="c" select="$row/TENOR" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.maturityamt"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/TOTAL_AMOUNT_DUE" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>

                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.principal"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/AMOUNT" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.currency"/>
                    </td>

                    <td>
                 <x:set  var="c" select="$row/CURRENCY" />

                        <x:choose >
                        <x:when select="$c">
                            <x:out select="$c"/>

                        </x:when>
                            <x:otherwise>
                            <x:out select="0.00"/>
                            </x:otherwise>
                        </x:choose>
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.maturitydate"/>
                    </td>

                    <td>
                 
                  <c:set var="cd2"  >
                                                <x:out select="$row/MATURITY_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                               <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>
                        
                </td>


                </tr>
                <tr>



                    <td align="left">
                        <bean:message key="label.startdate"/>
                    </td>

                    <td>
                 
                 <c:set var="cd2"  >
                                                <x:out select="$row/VALUE_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                               <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>


                </td>


                </tr>
                
                                    </tbody>
                                </c:if>
                            </x:forEach>

                        </table>

                    </fieldset>
                    <table>
                        <tr>

                            <c:if test="${lowcount>0}">
                                <td>
                                    <div align="left">

                                        <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                                    </div>
                                </td>
                            </c:if>
                    </table>
                    <c:if test="${size>increment && highcount<size}">


                        <div align="center">

                            <input type="submit" name="Submit" value="next" onClick="getNextValue(1011,'101');" class="button">

                        </div>


                    </c:if>


                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
                </div>
            </c:when>
            <c:otherwise>
                <table>
                    <tr>
                        <td>There  is no data</td>
                    </tr>
                </table>
            </c:otherwise>

        </c:choose>




