<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Inter Bank Transfer</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">

                <c:set var="xml" value="${data}" />
                <x:parse varDom="doc" xml="${xml}" />



                <input type="hidden" id="screenId" value="7015"/>
                <input type="hidden" id="taskId" value="4600"/>
                <p>
                    <label><bean:message key="label.accounttodebit"/><font color="red">*</font></label>

                    <select name="acct_no" id ="acct_no" onchange=updateCCY(this.value, 'ccy', 'branch', 'availbal', 'maxamt') class="small-input">
                        <c:set var="acc_no">
                            <x:out select="$doc/ROWSET/ROW/ACCOUNT_NUMBER" />
                        </c:set>

                        <c:if test="${acc_no==null || acc_no=='' }">
                            <option selected style="display: none;" value=<x:out  select="$doc/ROWSET/ROW/ACCOUNT_NUMBER" />  > <x:out  select="$doc/ROWSET/ROW/ACCOUNT_NUMBER" /> Select an account</option>
                        </c:if>


                        <x:forEach var="row" select="$doc/ROWSET/ROW">
                            <c:set var="curr_acc_no">
                                <x:out select="CUST_AC_NO" />
                            </c:set>
                            <c:choose>
                                <c:when test="${(acc_no != null && acc_no !='')&& acc_no == curr_acc_no}">
                                    <option selected data-maxamt="<x:out select="MAX_AMOUNT"/>" data-availbal="<x:out select="BALANCE"/>" data-branch="<x:out select="BRANCH_CODE"/>" data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                                </c:when>
                                <c:when test="${(curr_acc_no != null && curr_acc_no !='')}">
                                    <option data-maxamt="<x:out select="MAX_AMOUNT"/>" data-availbal="<x:out select="BALANCE"/>" data-branch="<x:out select="BRANCH_CODE"/>" data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                                </c:when>
                            </c:choose>

                        </x:forEach>
                    </select>

                </p>

                <p>
                    <label>One Time Transfer</label>
                    <input type="checkbox" name="ott" id="ott" value="OTP" onclick="enableOTT()"/>
                </p>
                <!-- block for one time fund transfer -->
                <div id="onetimetransfer" style="display: none">
                    <p>
                        <label>Beneficiary's Account<font color="red">*</font></label>
                        <input class="text-input small-input" type="text"  id="bene_acc_no" name="bene_acc_no" value=<x:out  select="$doc/ROWSET/ROW/BENE_ACCOUNT_NUMBER" />  >
                    </p>
                    <p>
                        <label>Beneficiary's Bank<font color="red">*</font></label>
                            <c:set var="ben_bank">
                                <x:out select="$doc/ROWSET/ROW/BENEFICIARY_BANK" />
                            </c:set>
                        <select name="bank" id="bank" onchange=getAccountName(7015)>
                            <c:if test="${ben_bank ==null || ben_bank=='' }">
                                <option selected style="display: none;" value=<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_BANK_CODE" /> > <x:out  select="$doc/ROWSET/ROW/BENEFICIARY_BANK" /> Select beneficiary bank</option>
                            </c:if>
                            <c:set var="xml" value="${data}" />
                            <x:parse varDom="doc" xml="${xml}" />
                            <x:forEach var="row" select="$doc/ROWSET/BANKS" >
                                <c:set var="curr_ben_bank">
                                    <x:out select="BANK_NAME" />
                                </c:set>
                                <c:choose>
                                    <c:when test="${(ben_bank != null && ben_bank !='')&& ben_bank == curr_ben_bank}">
                                        <option selected value="<x:out select="BANK_CODE" />"><x:out select="BANK_NAME" /></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="<x:out select="BANK_CODE" />"><x:out select="BANK_NAME" /></option>
                                    </c:otherwise>
                                </c:choose>
                            </x:forEach>
                        </select>
                </div>
                <!-- end of one time fund transfer -->
                </p>
                <c:set var="cc" >
                    <x:out  select="$doc/ROWSET/ROW/FLAG " />
                </c:set>
                <c:set var="c" >
                    <x:out  select="$doc/ROWSET/ROW/ACCNAME " />
                </c:set>
                <p>
                    <c:if test="${c =='NotFound' || c=='NotFound2'}">

                        <label >Beneficiary's Account Name<font color="red">*</font></label>
                        <input class="text-input small-input" type="text"  id="bene_acc_name" name="bene_acc_name"  >
                        <label id="account_null_message"><font color="red">Unable to fetch Beneficiary's name from selected Bank</font></label>
                            <%-- <input class="text-input small-input" type="text"  id="bene_acc_name" name="bene_acc_name" value="<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_BANK_CODE " />"  > --%>

                    </c:if>





                    <c:if test="${cc == 'flag'}">
                        <label >Beneficiary's Account Name<font color="red">*</font></label>
                        <input class="text-input small-input" type="text"  id="bene_acc_name" name="bene_acc_name" value="<x:out  select="$doc/ROWSET/ROW/ACCNAME " />"  >
                    </c:if>                                                                    



                </p>
                <%-- <label><bean:message key="branch""label.amount"/><b><font color="red">*</font></b></label>--%>

                <label>Amount<font color="red">*</font></b></label>
                <input class="text-input small-input" type="amt"  id="amt" name="amt" value="" /> <span id="balance"></span>
                <input type="hidden" class="text-input small-input" type="text"  id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>" disabled  /><!---it was disabled because this filed is only used to allow customer to know the currncy of the account they are tasfering from-->
                <input type="hidden" id="branch" name="branch" value=""/>
                <input type="hidden" id="availbal" name="availbal" value=""/>
                <input type="hidden" id="maxamt" name="maxamt" value=""/>
                <input type="hidden" id="ccy2" name="ccy2" value=""/>

                </p>







                <%--     <p>
                         <label><bean:message key="label.ecgmobileNumber"/><b><font color="red">*</font></b></label>
                         <input class="text-input small-input" type="text" name ="mobileNumber" id="mobileNumber" maxLength="20"  />

                    </p>
                --%>    
                <p>
                    <label><bean:message key="label.ecgremarks"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text" name ="ecgremarks" id="ecgremarks" maxlength="150"  />

                </p>
                <p>

                    <input id="testButton" name="testButton" type="button" onclick="temporaryDisable(this);
                            getPin('70000', '1');"  value="<bean:message key="label.getpin"/>" class="button"> 
                </p>
                <div id="trx_details">
                    <br /><br />
                </div>
                <!--<div id="trx_detail"></div>-->
                <p>
                    <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text" name ="enterpin" id="enterpin" maxLength="20"  value="" onblur="getAccountDetail('001', '332');"/>

                </p>
                <div id="trx_details"></div>
                <p>
                    <%--   <input id="testButton" name="testButton" type="button" onClick="recharge()"  value="<bean:message key="label.ecgSubmit"/>" class="button">--%>
                    <input type="button"  class="button" value="Continue" onclick="InterbankFT(7016, '7015');" /> 
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                </p>







            </form>
            <div id="search_result">

            </div>
        </div>
    </body>
</html>
