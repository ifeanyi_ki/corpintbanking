

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-layout.tld" prefix="layout" %>

<html>
    <head>
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title><bean:message key="label.SITittle"/></title>
        <script language="JavaScript" src="scripts/screens/common.js"/>
        <script language="JavaScript">


            //-----------------------------------------------------------------------------

            //-----------------------------------------------------------------------------
        </script>
        <!--<script type="text/javascript" src="screens/scripts/modal.js"></script>-->

        <script type="text/javascript">


        </script>
    </head>

    <body >
        <!--  <form name="frmmain" method="POST" action="/EBanking/MainController">-->
        <div>


            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <c:set var="chsourcecount">
                <x:out select="$doc/ROWSET/ROW/INSTRUCTION_NO"/>
            </c:set>

            <c:choose>

                <c:when test="($chsourcecount == '') or ($chsourcecount == null)">
                    <table width="90%" align="center" class="TableBorder">
                        <tr>
                            <td class="Warning">
                                <bean:message key="label.noaccount"/>
                            </td>
                        </tr>
                    </table>
                </c:when>
                <c:otherwise>
                    <script language="JavaScript">
                        l_boolean = true;
                    </script>

                    <table width="90%" align="center" class="TableBorder" cellspacing="3" cellpadding="3">
                        <tr>
                            <td colspan="2" style="text-align: center" class="LblIPMand">
                                <bean:message key="label.SIDetail"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="40%" class="LblIPMand"><bean:message key="label.SIType"/></td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" name="SiTypeDesc" value="<x:out select="$doc/ROWSET/ROW/PRODUCT_DESCRIPTION"/>"/>
                                <input type="hidden" name="instno" id="instnos" value="<x:out select="$doc/ROWSET/ROW/INSTRUCTION_NO" />"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="LblIPMand" ><bean:message key="label.SISourceAcct"/></td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" name="SiTypeDesc" value="<x:out select="$doc/ROWSET/ROW/DR_ACCOUNT"/>"/>
                                <input type="hidden" name="accountBranch" id="accountBranch" value="<x:out select="$doc/ROWSET/ROW/BRANCH_CODE"/>"/>
                            </td>
                        </tr>
                        <!-- <tr>
                             <td class="LblIPMand" ><bean:message key="label.ToAccount"/></td>
                             <td class="LblIPMand" colspan="2">
                                 <select name="SelectDestAcct" onChange="return populateDestnAcct (document.frmmain.fldSelectDestAcct.options[document.frmmain.fldSelectDestAcct.selectedIndex].value,'destnacct');"
                                         onFocus = "return disableBenef ();">
                                     <option value=""></option>
 
                                 </select>
                             </td>
                         </tr>
                         <tr>
                             <td class="LblIPMand" ><bean:message key="label.BenAccount"/></td>
                             <td class="LblIPMand" colspan="2">
                                 <select name="fldBenefAcct" onChange="return populateDestnAcct (document.frmmain.fldBenefAcct.options[document.frmmain.fldBenefAcct.selectedIndex].value,'benef');"
                                         onFocus = "return disableBenef ();">
                                     <option value=""></option>
 
                                         <option value= "" ></option>
                                     >
                                 </select>
                             </td>
                         </tr>-->

                        <tr>
                            <td class="LblIPMand"><bean:message key="label.Destinationaccount"/></td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" name ="DestAcctNo" id="DestAcctNo" maxLength="20"  value="<x:out select="$doc/ROWSET/ROW/CR_ACCOUNT"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="LblIPMand" ><bean:message key="label.TransferAmount"/> </td>
                            <td class="LblIPMand" >
                                <input type="text" disabled="disabled" name ="SiTrfAmt" id="SiTrfAmt"  value ="<x:out select="$doc/ROWSET/ROW//SI_AMT"/>" maxlength="15" size="18"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="LblIPMand" ><bean:message key="label.TransferCurrency"/></td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" name ="SiTrfAmt" id="SiTrfAmt"  value ="<x:out select="$doc/ROWSET/ROW/SI_AMT_CCY"/>" maxlength="15" size="18"/>
                            </td>
                        </tr>


                        <tr>
                            <td class="LblIPMand"><bean:message key="label.FirstExecDate"/></td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" maxlength="10" name ="FirstExecDate" id="FirstExecDate" value = '<x:out select="$doc/ROWSET/ROW/FIRST_EXEC_DATE"/>'/>
                            </td>
                        </tr>
                        <tr>
                            <td class="LblIPMand"><bean:message key="label.SIFinalDate"/> </td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" maxlength="10" name ="SiFinalDate" id="SiFinalDate" value = '<x:out select="$doc/ROWSET/ROW/SI_EXPIRY_DATE"/>'/>
                            </td>
                        </tr>
                        <c:set var="exec_days">
                            <x:out select="$doc/ROWSET/ROW/EXEC_DAYS" />
                        </c:set>
                        <c:set var="exec_mths">
                            <x:out select="$doc/ROWSET/ROW/EXEC_MTHS" />
                        </c:set>
                        <c:set var="exec_yrs">
                            <x:out select="$doc/ROWSET/ROW/EXEC_YRS" />
                        </c:set>
                        <c:choose>
                            <c:when test="${exec_days != null && exec_days == 1}">
                                <c:set var="freq" value="Daily" />
                            </c:when>
                            <c:when test="${exec_days != null && exec_days == 7}">
                                <c:set var="freq" value="Weekly" />
                            </c:when>
                            <c:when test="${exec_mths != null && exec_mths == 1}">
                                <c:set var="freq" value="Monthly" />
                            </c:when>
                            <c:when test="${exec_mths != null && exec_mths == 3}">
                                <c:set var="freq" value="Quarterly" />
                            </c:when>
                            <c:when test="${exec_mths != null && exec_mths == 6}">
                                <c:set var="freq" value="Semi-Annually" />
                            </c:when>
                            <c:when test="${exec_yrs != null && exec_yrs == 1}">
                                <c:set var="freq" value="Yealy" />
                            </c:when>
                        </c:choose>
                        <tr>
                            <td class="LblIPMand">Frequency</td>
                            <td class="LblIPMand" colspan="2">
                                <input type="text" disabled="disabled" maxlength="10" name ="SiFinalDate" id="SiFinalDate" value = '<c:out value="${freq}" />'/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                </c:otherwise>
            </c:choose>

            <!--</form>-->
        </div>

        <br/>


    </body>
</html>




