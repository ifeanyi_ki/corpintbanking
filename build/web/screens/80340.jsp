


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>



   <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    
<x:set var="row" select="$doc/ROWSET/ROW"/>
<html>
    <head>
        <link rel="stylesheet" href="scripts/resources/css/style.css" type="text/css" media="screen" />
    </head>
    <body>
        <form>
<p>
    <label><bean:message key="label.servicename"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="servicename" name="servicename" value="<x:out select="$row/SERVICE_NAME" />" disabled/><!-- <span class="input-notification error png_bg">Error message</span>-->
    <input type="hidden" value="<x:out select="$row/SERVICE_CODE" />" id="serviceid" name="serviceid"/>
</p>
<p>
    <label><bean:message key="label.servicedesc"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="servicedesc" name="servicedesc" value="<x:out select="$row/SERVICE_DESCRIPTION" />" disabled /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->

</p>
<p>
    <label><bean:message key="label.serviceacct"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="serviceacct" name="serviceacct" value="<x:out select="$row/SERVICE_ACCOUNT" />"  />

</p>
<p>
    <label><bean:message key="label.email"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="email" name="email" value="<x:out select="$row/EMAIL" />"  />

</p>


<p>
    <input type="button" onclick="editServicesSetup(80341,'8034','E');" value="<bean:message key="label.Submit"/>" class="button"/>
    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
</p>
        </form>
<div id="ajaxload"></div>
    </body>
</html>