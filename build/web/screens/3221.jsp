<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <p>&nbsp;&nbsp;</p>
        <fieldset>
            <c:set var="error" value="${error}"/>
            <c:if test="${error==null}">
        <table>

            <tr>

                        <td width="200" class="LblIPMand" height="20" nowrap><bean:message key="label.chequebkreqsuccess"/></td>
                        
                    </tr>
                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
                    <!--<tr>
                        <td width="400" class="LblIPMand" height="20" nowrap><bean:message key="label.confirmationno"/> </td>
                        <td width="400" class="LblIPMand" height="20" nowrap> <c:out value="${cheqseq}"/></td>
                    </tr>-->

        </table>
                      </c:if>
            <c:if test="${error!=null}">
                <table>
                    <tr>
                       <td width="200" class="LblIPMand" height="20" nowrap> <c:out value="${cheqseq}"/></td>
                    </tr>
                </table>
            </c:if>
        </fieldset>
             
     
    </body>
</html>
