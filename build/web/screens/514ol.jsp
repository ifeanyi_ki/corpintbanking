
    <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script language="JavaScript" type="text/javascript">
var l_nos_msg   = '';


</script>
<script type="text/javascript" language='JavaScript' src='jsdir/mailalert.js'/>

<title>Customer Messages and Mailing</title>


<script type="text/javascript" language="JavaScript">
//------------------------------------------------------------------------------
var addlistWindow;
var l_alter = "false";
var	reqId = "";
var forward = false;
var	reply = false;
var subject, from, to, cc, date, loginuserid;
loginuserid = '';

reply = true;

subject = "<xsl:value-of select='faml/subject'/>";
from = "<xsl:value-of select='faml/fromid'/>";
to = "<xsl:value-of select='faml/touserid'/>";
cc = "<xsl:value-of select='faml/ccuserid'/>";
date = "<xsl:value-of select='faml/date'/>";
var a = "";
var b = "";
var mode = "";
<xsl:if test="(faml/mode='Save') or (faml/mode='Send')">
	mode = "no_textbox"
</xsl:if>

var reqflg = new Array();
var cntflg = 0;
<xsl:for-each select="faml/message">
	reqflg[cntflg] = "<xsl:value-of select="@reqflg"/>";
	cntflg++;
</xsl:for-each>
//------------------------------------------------------------------------------
function inbox ()
{
	document.frmComMain.fldScrnSeqNbr.value = "05";
	document.frmComMain.fldFlgBox.value = "I";
	document.frmComMain.submit ();
	return false;
}
//-----------------------------------------------------------------------------
function draft()
{
	document.frmComMain.fldScrnSeqNbr.value = "03";
	document.frmComMain.fldFlgBox.value = "D";
	document.frmComMain.submit ();
	return false;
}
//-----------------------------------------------------------------------------
function validateMailId(
	p_control
){
	var lUser;
	var	userArray	= new Array ();

	lUser = p_control.value;
	if (lUser != "") {
		userArray = lUser.split(";");
	}
	if (! checkValue (userArray)) {
		p_control.focus ();
		return false;
	}

	if (lUser != "") {
		for (l_i = 0; l_i <(userArray.length); l_i++) {
			if(userArray[l_i] == " "){
				alert ("Space between mail id's is not allowed.");
				p_control.focus ();
				return false;
			}
			if (userArray[l_i] == loginuserid) {
				alert ("User cannot send mail to himself.");
				p_control.focus ();
				return false;
			}
			if (userArray [l_i].length > 50) {
				alert ("The length of a single user id cannot be more than 50.");
				p_control.focus ();
				return false;
			}
		}
	}

	return true;
}
//-----------------------------------------------------------------------------
function sent()
{
	document.frmComMain.fldScrnSeqNbr.value = "04";
	document.frmComMain.fldFlgBox.value = "S";
	document.frmComMain.submit ();

	return false;
}
//-----------------------------------------------------------------------------
// just  for  testing of Address List Bean
function compose()
{
   	document.frmComMain.fldScrnSeqNbr.value = "08";
	document.frmComMain.fldReqType.value = "COM";
	document.frmComMain.submit ();

	return false;
}
//------------------------------------------------------------------------------
function checkInput (p_obj)
{
	for (var l_i = 0; l_i < p_obj.value.length; l_i++) {
		if (p_obj.value.charAt(l_i) == '"') {
			alert("Please do not enter double quotes");
			p_obj.focus ();
			return false;
		}
	}
	return true;
}
//------------------------------------------------------------------------------
function checkValue (
	p_arr
) {
	for (var l_i = 0; l_i < p_arr.length; l_i++) {
		if (p_arr [l_i] == "") {
			if (l_i != (p_arr.length - 1)) {
				alert ("Please enter correct mail id.");
				return false;
			}
		}
	}
	return true;
}
//------------------------------------------------------------------------------
function send ()
{
	setRequestId ();
	var l_i	= 0;
	var	l_user_to_arr	= new Array ();
	var	l_user_cc_arr	= new Array ();
	var	l_user_to_list	= "";
	var	l_user_cc_list	= "";

	if(!(validateMailId(document.frmComMain.fldToMsg))){
		return false;
	}
	if(!(validateMailId(document.frmComMain.fldCcMsg))){
		return false;
	}

	if (trim (document.frmComMain.fldToMsg.value) == "")
	{
		alert ("There are no mail ids in the To list.");
		document.frmComMain.fldToMsg.focus ();
		return false;
	}

	if (trim (document.frmComMain.fldSubject.value) == "")
	{
		alert ("Please enter the subject.");
		document.frmComMain.fldSubject.focus ();
		return false;
	}

	if (trim (document.frmComMain.fldTxt.value) == "")
	{
		alert ("please enter message");
		document.frmComMain.fldTxt.focus ();
		return false;
	}

	if (!checkInput (document.frmComMain.fldToMsg)) {
		return false;
	}
	if (document.frmComMain.fldCcMsg.value != "") {
		if (!checkInput (document.frmComMain.fldCcMsg)) {
			return false;
		}
	}
	if (!checkInput (document.frmComMain.fldSubject)) {
		return false;
	}
	if (!checkInput (document.frmComMain.fldTxt)) {
		return false;
	}

	l_user_to_list = document.frmComMain.fldToMsg.value;
	l_user_cc_list = document.frmComMain.fldCcMsg.value;
	l_user_to_arr = l_user_to_list.split(";");
	if (l_user_cc_list != "") {
		l_user_cc_arr = l_user_cc_list.split(";");
	}
/*
	if (l_user_cc_arr.length &gt; 0) {
		for (l_i = 0; l_i &lt; l_user_to_arr.length; l_i++) {
			var l_j	= 0;
			for (l_j = 0; l_j &lt; l_user_cc_arr.length; l_j++) {
				if (l_user_cc_arr [l_j] == l_user_to_arr [l_i])
				{
					alert ("The user " + l_user_cc_arr [l_j]
						+ " cannot exist in both the To list and the CC list");
					document.frmComMain.fldCcMsg.focus ();
					return false;
				}
			}
		}
	}
*/
//	document.frmhidden.fldScrnSeqNbr.value = "13";
	document.frmhidden.fldScrnSeqNbr.value = "06";
	document.frmhidden.fldToMsg.value =
				 (document.frmComMain.fldToMsg.value);
	document.frmhidden.fldCcMsg.value =
				(document.frmComMain.fldCcMsg.value);
	document.frmhidden.fldSubject.value =
				(trim (document.frmComMain.fldSubject.value));
	document.frmhidden.fldTxt.value =  (trim (document.frmComMain.fldTxt.value.substring(0,2000)));
	document.frmhidden.fldMode.value = "1";
//	document.frmhidden.fldType.value = "V";
	document.frmhidden.fldType.value = "S";

	document.frmhidden.target="_self";

	if (addlistWindow != null) {
		if (! addlistWindow.closed) {
			addlistWindow.close ();
		}
	}
	document.frmhidden.submit ();
	return false;
}
//-----------------------------------------------------------------------------
function AddressBook()
{
	if(!(validateMailId(document.frmComMain.fldToMsg))){
		return false;
	}
	if(!(validateMailId(document.frmComMain.fldCcMsg))){
		return false;
	}
	document.frmhidden.fldScrnSeqNbr.value 	= "10";
	document.frmhidden.fldReqType.value 	= "COM";

	var features 	=	"directories=no,location=no,menubar=no,status=no,"
					+	"toolbar=no,dependant=yes,scrollbars=yes,top=10,left=30,resizable=no,width=600,height=600";

    addlistWindow = window.open ("", "AddressBook", features);
    document.frmhidden.target = "AddressBook";
    document.frmhidden.submit();

    return false;
}
//------------------------------------------------------------------------------
function fireApp ()
{
	if (document.frmChoose.fldIdUserSrch.value != "") {
		if (! validateInp (document.frmChoose.fldIdUserSrch.value, "id")) {
			alert ("Invalid characters in User Id field, please enter again.");
			document.frmChoose.fldIdUserSrch.focus ();
			return false;
		}
	}

	if (document.frmChoose.fldEmail.value != "") {
		if (! validateEmail ()) {
			return false;
		}
	}

	if (document.frmChoose.fldFirstName.value != "") {
		if (! validateInp (document.frmChoose.fldFirstName.value, "desc")) {
			alert ("Invalid characters in First Name field, please enter again.");
			document.frmChoose.fldFirstName.focus ();
			return false;
		}
	}

	if (document.frmChoose.fldLastName.value != "") {
		if (! validateInp (document.frmChoose.fldLastName.value, "desc")) {
			alert ("Invalid characters in Last Name field, please enter again.");
			document.frmChoose.fldLastName.focus ();
			return false;
		}
	}

	if (document.frmChoose.fldNickName.value != "") {
		if (! validateInp (document.frmChoose.fldNickName.value, "desc")) {
			alert ("Invalid characters in Nick Name field, please enter again.");
			document.frmChoose.fldNickName.focus();
			return false;
		}
	}

	document.frmSubmit.fldFirstName.value =  (trim (document.frmChoose.fldFirstName.value));
	document.frmSubmit.fldLastName.value = (trim (document.frmChoose.fldLastName.value));
	document.frmSubmit.fldNickName.value = (trim (document.frmChoose.fldNickName.value));
	document.frmSubmit.fldIdUserSrch.value =  (trim (document.frmChoose.fldIdUserSrch.value));
	document.frmSubmit.fldEmail.value =  (trim (document.frmChoose.fldEmail.value));
	document.frmSubmit.fldScrnSeqNbr.value = "02";
	document.frmSubmit.submit();
	return false;
}
//------------------------------------------------------------------------------
function save ()
{
	var l_i	= 0;
	var	l_user_to_arr	= new Array ();
	var	l_user_cc_arr	= new Array ();
	var	l_user_to_list	= "";
	var	l_user_cc_list	= "";

	setRequestId ();

	if(!(validateMailId(document.frmComMain.fldToMsg))){
		return false;
	}

	if(!(validateMailId(document.frmComMain.fldCcMsg))){
		return false;
	}
	if (trim (document.frmComMain.fldToMsg.value) == "")
	{
		alert ("There are no mail ids in the To list.");
	//	document.frmComMain.fldToMsg.focus ();
		return false;
	}
	if (trim (document.frmComMain.fldSubject.value) == "")
	{
		alert ("Please enter the subject.");
		document.frmComMain.fldSubject.focus ();
		return false;
	}

	if (trim (document.frmComMain.fldTxt.value) == "")
	{
		alert ("Please enter the message.");
		document.frmComMain.fldTxt.focus ();
		return false;
	}

	l_user_to_list = document.frmComMain.fldToMsg.value;
	l_user_cc_list = document.frmComMain.fldCcMsg.value;
	l_user_to_arr = l_user_to_list.split(";");
	if (l_user_cc_list != "") {
		l_user_cc_arr = l_user_cc_list.split(";");
	}

	if (l_user_cc_arr.length > 0) {
		for (l_i = 0; l_i < l_user_to_arr.length; l_i++) {
			var l_j	= 0;
			for (l_j = 0; l_j < l_user_cc_arr.length; l_j++) {
				if (l_user_cc_arr [l_j] == l_user_to_arr [l_i])
				{
					alert ("The user " + l_user_cc_arr [l_j]
						+ " cannot exist in both the To list and the CC list");
					document.frmComMain.fldCcMsg.focus ();
					return false;
				}
			}
		}
	}

//	document.frmhidden.fldScrnSeqNbr.value = "13";
	document.frmhidden.fldScrnSeqNbr.value = "06";
	document.frmhidden.fldToMsg.value =
				 (document.frmComMain.fldToMsg.value);
	document.frmhidden.fldCcMsg.value =
				 (document.frmComMain.fldCcMsg.value);
	document.frmhidden.fldSubject.value =
				 (document.frmComMain.fldSubject.value);
	document.frmhidden.fldTxt.value =  (trim (document.frmComMain.fldTxt.value));
	document.frmhidden.fldMode.value = "2";
//	document.frmhidden.fldType.value = "V";
	document.frmhidden.fldType.value = "S";

	document.frmhidden.target="_self";

	if (addlistWindow != null) {
		if (! addlistWindow.closed) {
			addlistWindow.close ();
		}
	}
	document.frmhidden.submit ();
	return false ;
}
//------------------------------------------------------------------------------
function Close ()
{
	document.frmhidden.target="_self";

	if(window.confirm("This message will not be saved. Do you still want to close it ??")) {
		document.frmhidden.fldScrnSeqNbr.value = "01";
		document.frmhidden.submit ();
	}

		if (addlistWindow != null) {
			if (! addlistWindow.closed) {
				addlistWindow.close ();
			}
		}
		return false ;
}
//------------------------------------------------------------------------------
function fireApp()
{
	setRequestId ();
	document.frmhidden.submit();
	return false;
}
//------------------------------------------------------------------------------
function setRequestId ()
{
	top.frames ["frame_menu"].document.forms ["frmMenu"].fldRequestId.value =
					document.frmhidden.fldRequestId.value;
}
//------------------------------------------------------------------------------
</script>
</head>
<body dir='LTR'  class="WorkArea">
<table width="90%">
<tr>
	<td width="40%" class="formHeading">Mail Message</td>
</tr>
</table>

<form name="frmhidden" method="post" action="entry">
	<xsl:apply-templates select="faml/mci"/>
	<input type="hidden" name="fldToMsg" value=""/>
	<input type="hidden" name="fldCcMsg" value=""/>
	<input type="hidden" name="fldSubject" value=""/>
	<input type="hidden" name="fldMsgId" value="{faml/messageid}"/>
	<input type="hidden" name="fldTxt" value=""/>
	<input type="hidden" name="fldMode" value=""/>
	<input type="hidden" name="fldType" value=""/>
	<input type="hidden" name="fldReqType" value="COM"/>
	<input type="hidden" name="fldSeqNo" value="02"/>
</form>
<form name="frmSubmit" action="entry" method="post">
	<input type="hidden" name="fldIdUser"/>
	<input type="hidden" name="fldIdUserSrch"/>
	<input type="hidden" name="fldFirstName"/>
	<input type="hidden" name="fldLastName"/>
	<input type="hidden" name="fldNickName"/>
	<input type="hidden" name="fldEmail"/>
	<xsl:apply-templates select="faml/mci"/>
</form>
<form name="frmComMain" action="entry" method="post">
<xsl:apply-templates select="faml/mci"/>
<input type="hidden" name="DummyReqId" value=""/>
<input type="hidden" name="fldFlgBox" value=""/>
<input type="hidden" name="fldReqType" value="COM"/>
<br/>
<xsl:if test = "faml/mci/@scrnseqnbr != '06'">
	<table width="90%" align="center">

        	<tr>
        		<td class="buttons" width='10%'>
       				<a href="JavaScript:void(0)" onClick="return inbox()">Inbox</a>
        		</td>

        		<td class="buttons" width='10%'>
        			<xsl:if test="faml/flgbox != 'D'">
           				<a href="JavaScript:void(0)" onClick="return draft()">Draft</a>
        			</xsl:if>
        			<!--<xsl:if test="faml/flgbox = 'D'">
	        			Draft
    	    		</xsl:if>-->
        		</td>

          		<td class="buttons" width='10%'>
          			<xsl:if test="faml/flgbox != 'C'">
           				<a href="JavaScript:void(0)" onClick="return compose()">Compose</a>
        			</xsl:if>
        		<!--	<xsl:if test="faml/flgbox = 'C'">
	        			Compose
    	    		</xsl:if>-->
       			</td>

        		<td class="buttons" width='10%'>
        			<xsl:if test="faml/flgbox != 'S'">
	       				<a href="JavaScript:void(0)" onClick="return sent()">Sent Items</a>
    	    		</xsl:if>
        			<!--<xsl:if test="faml/flgbox = 'S'">
        				Sent Items
        			</xsl:if>-->
       			</td>

        	</tr>


	</table>
</xsl:if>
<xsl:if test="faml/mode='1'">
	<!--<xsl:if test="faml/successful/@number != 0">
		<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
			<tr>
				<td class="ColHeadingLeftAlignedBold" colspan="2">Your message has been sent.</td>
			</tr>

			<xsl:if test="faml/subject != ''">
				<tr>
					<td width="30%" class="DataLeftAlignedBold">subject</td>
					<td class="DataLeftAligned"><xsl:value-of select="faml/subject"/>
					</td>
				</tr>
			</xsl:if>
		</table>
		<br/>
		<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
			<tr>
				<td class="ColHeadingLeftAlignedBold">email to/Cc</td>
				<td class="ColHeadingLeftAlignedBold">username </td>
				<td class="ColHeadingLeftAlignedBold">status </td>
			</tr>
			<script language="JavaScript">
				var l_tmp = 0;
				var l_user_arr = new Array ();
				var l_type_arr = new Array ();
				var l_status_arr = new Array ();
				<xsl:for-each select="faml/user">
						l_user_arr[l_tmp]='<xsl:value-of select="@name"/>';
						l_type_arr[l_tmp]='<xsl:value-of select="@type"/>';
						l_status_arr[l_tmp]='<xsl:value-of select="@status"/>';
						l_tmp++;
				</xsl:for-each>
				var l_i	= 0;
				if (l_user_arr.length >0) {
					for (l_i = 1; l_i <= l_user_arr.length; l_i++) {
						document.write ("&lt;tr&gt;");
						document.write ("&lt;td class='DataLeftAligned' &gt;");
						if(l_type_arr[l_i - 1] =='T')
							document.write ('to');
						else
							document.write ('Cc');
						document.write ("&lt;/td&gt;");
						document.write ("&lt;td class='DataLeftAligned' &gt;");
						document.write (l_user_arr [l_i - 1]);
						document.write ("&lt;/td&gt;");
						document.write ("&lt;td class='DataLeftAligned' &gt;");
						if(l_status_arr[l_i - 1] == 'S') {
							document.write ("successful");
						} if(l_status_arr[l_i - 1] == 'M') {
							document.write ("missing user");
						} else if(l_status_arr[l_i - 1] == 'A') {
							document.write ("user not authorized to receive");
						}
						document.write ("&lt;/td&gt;");
						document.write ("&lt;/tr&gt;");
					}
				}
			</script>
		</table>
	</xsl:if>

	<xsl:if test="faml/successful/@number = '0'">
		<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
			<tr>
				<td class='LblIPMand'>
                                    <b>Your mail has not been sent but it has been saved in the draft folder.</b>
				</td>
			</tr>
		</table>
		<xsl:if test="faml/user">
			<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
				<tr>
					<td width="40%" class="ColHeadingLeftAlignedBold">invalid users</td>
					<td class="ColHeadingLeftAlignedBold" width="60%">reason</td>
				</tr>
				<xsl:for-each select="faml/user">
				<script language="JavaScript">
				if (l_alter == "true") {
					l_alter = "false";
					document.write ("&lt;tr class='AlterRow2' &gt;");
				} else {
					l_alter = "true";
					document.write ("&lt;tr class='AlterRow1' &gt;");
				}
				</script>
						<td class="DataLeftAlignedBold" width="40%">
							<xsl:value-of select="@name"/>
						</td>
						<td class="DataLeftAligned">
							<xsl:if test="@status='M'">
								missing user
							</xsl:if>
							<xsl:if test="@status='A'">
								user not authorized to receive
							</xsl:if>
						</td>
				<script language="JavaScript">
					document.write ("&lt;/tr&gt;");
				</script>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:if>
</xsl:if>
<xsl:if test="/faml/mode='2'">
	<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
		<tr>
			<td class="ColHeadingLeftAlignedBold" colspan="2">Your message has been saved.</td>
		</tr>
		<xsl:if test="faml/subject != ''">
			<tr class="DataLeftAligned">
				<td width="30%" class="DataLeftAlignedBold">subject</td>
				<td class="DataLeftAligned"><xsl:value-of select="faml/subject"/></td>
			</tr>
		</xsl:if>
	</table>
	<br/>
	<xsl:if test="faml/toidusr != ''">
		<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
			<tr>
				<td class="ColHeadingLeftAlignedBold">list of users in to list</td>
			</tr>
			<tr>
				<td class="DataLeftAligned"><xsl:value-of select="faml/toname"/></td>
			</tr>
			</table>
	</xsl:if>
	<br/>
	<xsl:if test="faml/ccidusr != ''">
		<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
			<tr>
				<td class="ColHeadingLeftAlignedBold">list of users in cc list</td>
			</tr>
			<tr>
				<td class="DataLeftAligned"><xsl:value-of select="faml/ccname"/></td>
			</tr>
			</table>
	</xsl:if>
</xsl:if>
<xsl:choose>
<xsl:when test="(faml/mci/@scrnseqnbr='08')"/>
<xsl:when test="(faml/mci/@scrnseqnbr='09')"/>
<xsl:when test="(faml/mci/@scrnseqnbr='14')"/>
<xsl:when test="(faml/mci/@scrnseqnbr='06')
						and ((faml/unsuccessvectorlist/unsuccessvector)
								or (faml/notaccesslist/notaccessvector))"/>
<xsl:otherwise>
</xsl:otherwise>
</xsl:choose>
-->
<xsl:choose>

	<xsl:when test="(faml/mci/@scrnseqnbr='06') and (faml/mode='2')"/>
	<xsl:when test="(faml/mci/@scrnseqnbr='06') and
					not ((faml/unsuccessvectorlist/unsuccessvector)
							or (faml/notaccesslist/notaccessvector))"/>
	<xsl:otherwise>
		<input type="hidden" name="messageid" value="{/faml/messageid}"></input>
		<br/>
    	<table class="tableBorder" cellspacing="2" cellpadding="2" align="center" width="90%">
		<tr>
			<td width="25%" class="LblIPMand">
			<a href="JavaScript:void(0)" onclick="return AddressBook()" class="LblIPMand"><b>to</b></a>
			</td>
			<xsl:if test="/faml/reqtype = 'RPY'">
				<td width="75%" class="LblIPMand">
					<script language="Javascript">
					if (navigator.appName == 'Microsoft Internet Explorer') {
						document.write ('&lt;input type="text" name="fldToMsg" disabled="true" maxLength="2000" size="53"/&gt;');
					}
					if (navigator.appName == 'Netscape') {
						document.write ('&lt;input type="text" name="fldToMsg" disabled="true" maxLength="2000" size="35"/&gt;');
					}
					</script>
					<script language="javascript">
						document.frmComMain.fldToMsg.value =
							"<xsl:value-of select='faml/fromid'/>";
					</script>
                                    <input type="text" name="fldToMsg" maxLength="2000" size="35" value=""/>
				</td>
			</xsl:if>
			<!--<xsl:if test="/faml/reqtype != 'RPY'">-->
				<td width="75%" class="LblIPMand">
					<script language="Javascript">
					var l_sub = "<xsl:value-of select='faml/toidusr'/>";
					if (navigator.appName == 'Microsoft Internet Explorer') {
						document.write ('&lt;input type="text" name="fldToMsg" disabled="true" maxLength="2000" size="53" value="'
									+ l_sub + '"/&gt;');
					}
					if (navigator.appName == 'Netscape') {
						document.write ('&lt;input type="text" name="fldToMsg" disabled="true" maxLength="2000" size="35" value="'
									+ l_sub + '"/&gt;');
					}
					</script>

				</td>
			</xsl:if>
		</tr>
		<tr>
			<td width="25%" class="LblIPMand">
                            <a href="JavaScript:void(0)" onclick="return AddressBook()" class="LblIPMand" ><b>cc</b></a>
			</td>
			<!--<xsl:if test="/faml/reqtype = 'RPY'">-->
			<td width="75%" class="LblIPMand">
				<script language="Javascript">
				if (navigator.appName == 'Microsoft Internet Explorer') {
					document.write ('&lt;input type="text" name="fldCcMsg" disabled="true" maxLength="2000" size="53"/&gt;');
				}
				if (navigator.appName == 'Netscape') {
					document.write ('&lt;input type="text" name="fldCcMsg" disabled="true" maxLength="2000" size="35"/&gt;');
				}
				</script>

				<!--<xsl:if test="faml/loginusertype='O'">-->
					<!--<xsl:if test="faml/fromusertype='O'">
						<script language="javascript">
							document.frmComMain.fldCcMsg.value =
								"<xsl:value-of select='faml/fromid'/>";
						</script>
					</xsl:if>-->
				<!--</xsl:if>-->
                                 <input type="text" name="fldCcMsg" maxLength="2000" size="35" value=""/>
			</td>
			</xsl:if>
			<!--<xsl:if test="/faml/reqtype != 'RPY'">-->
			<td width="75%" class="LblIPMand">
				<script language="Javascript">
				var l_sub = "<xsl:value-of select='faml/ccidusr'/>";
				if (navigator.appName == 'Microsoft Internet Explorer') {
					document.write ('&lt;input type="text" name="fldCcMsg" disabled="true" maxLength="2000" size="53" value="'
								+ l_sub + '"/&gt;');
				}
				if (navigator.appName == 'Netscape') {
					document.write ('&lt;input type="text" name="fldCcMsg" disabled="true" maxLength="2000" size="35" value="'
								+ l_sub + '"/&gt;');
				}
				</script>

			</td>
			<!--</xsl:if>-->
		</tr>
		<tr>
                    <td width="25%" class="LblIPMand"><b>subject *</b>
			</td>
			<td class="LblIPMand">
				<script language="Javascript">
				var l_sub = "<xsl:value-of select='faml/subject'/>";
				if (navigator.appName == 'Microsoft Internet Explorer') {
					document.write ('input type="text" name="fldSubject" maxLength="100" size="53" value="'
								+ l_sub + '"/&gt;');
				}
				if (navigator.appName == 'Netscape') {
					document.write ('input type="text" name="fldSubject" maxLength="100" size="35" value="'
								+ l_sub + '"');
				}
				</script>
                            <input type="text" name="fldSubject" maxLength="100" size="35" value=""/>
			</td>
		</tr>
		<tr>
			<td width="25%" class="LblIPMand"><b>
			message *</b>
			</td>
			<td class="LblIPMand">
				<textarea rows="10" cols="45" name="fldTxt"></textarea>
			</td>
		</tr>
		<tr>
		<td class="LblIPMand" colspan="2">
		*any message text which is more than 2000 characters will be truncated.
		</td>
		</tr>
		</table>
		<br/>
		<table width="90%" align="center">
			<tr>
				<td class="buttons">
		      		<a href="JavaScript:void(0)" onClick="return send()">Send</a>
				</td>
				<td class="buttons">
					<a href="JavaScript:void(0)" onClick="return save()">Save</a>
				</td>
				<td class="buttons">
				<a href="JavaScript:void(0)" onClick="window.print ()">print</a>
				</td>
				<td class="buttons">
					<a href="JavaScript:void(0)" onClick="return Close()">Close</a>
				</td>
			</tr>
		</table>
	</xsl:otherwise>
</xsl:choose>
		<br/>

</form>
</body>
</html>
</xsl:template>
<xsl:template match="faml/mci">
	<input type="hidden" name="fldAppId" value="{@appid}"></input>
	<input type="hidden" name="fldTxnId" value="{@txnid}"></input>
	<input type="hidden" name="fldScrnSeqNbr" value="01"></input>
	<input type="hidden" name="fldSessionId" value="{@sessionid}"></input>
	<input type="hidden" name="fldRequestId" value="{@requestid}"/>
</xsl:template>
</xsl:stylesheet>
