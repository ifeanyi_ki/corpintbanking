<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<form name="frmComMain" id="frmComMain" method="get" action="MainController" style="border: none; padding: 0; margin: 0;">
    <input type="hidden" name="DummyReqId" value=""/>
    <input type="hidden" name="fldFlgBox" value=""/>
    <input type="hidden" name="fldReqType" value="COM"/>
    <input type="hidden" name="screenId" value="514"/>
    <input type="hidden" name="taskId" value="5141"/>
    <input type="hidden" name="mode" value="1"/>
    <input type="hidden" name="flg_box" value=""/>
    <input type="hidden" name="verify" value="S"/>
    <input type="hidden" name="userid" value="<%=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getUserid()%>"/>

    <p>
        <label class="formcolumn-left"><bean:message key="label.to"/><font color="red">*</font></label>
        <input class="text-input small-input" type="text" id="fldToMsg1" name="fldToMsg1" value="" onclick="getAddress('to');" disabled><small><Span onclick="clearfield('to')"><a>clear</a></span></small><small >&nbsp;&nbsp;<Span onclick="getAddress('to');"><a>add</a></span></small>
        <input class="text-input small-input" type="hidden" id="fldToMsg" name="fldToMsg" value="" >

    </p>
    <p>
        <label class="formcolumn-left"><bean:message key="label.from"/><font color="red">*</font></label>
        <input class="text-input small-input" type="text" id="fldfromMsg1" name="fldfromMsg1" value="<%=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getUsername()%>"  >
        <input class="text-input small-input" type="hidden" id="fldfromMsg" name="fldfromMsg" value="<%=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getEmail()%>"  >
    </p>
    <div id="addressbook"></div>  

    <p>

        <label class="formcolumn-left"><bean:message key="label.subject"/></label>
        <input class="text-input small-input" type="text" id="fldSubject" name="fldSubject" value="" >

    </p>
    <p>
        <label class="formcolumn-left"><bean:message key="label.body"/></label>
        <textarea class="textarea wysiwyg" id="fldTxt" name="fldTxt" cols="80" rows="5"></textarea>

    </p>
    <p>
        <label class="formcolumn-left"></label>
        <input type="button"  onclick="send(5141,'514');" value="<bean:message key="label.send"/>&nbsp;" class="button">

        <input type="button" onclick="return save(5141,'514');" value="<bean:message key="label.save"/>" class="button">
        <!--<input type="button" onclick=" return print();" value="<bean:message key="label.print"/>" class="button">-->
        <input type="button" onclick=" return resetMail('trx_detail');" value="<bean:message key="label.close"/>" class="button">
    </p>
</form>

