


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


            <html>
                <head>

                </head>
                <body>


 <div  class="tab-content" id="tab2" align="center">
    <div class="content-box-header">

        <h3><bean:message key="label.managebenficiary"/></h3>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->
    <form action="" method="POST">


<c:set var="xml" value="${data}" />



 <x:parse varDom="doc" xml="${xml}" />

 <x:forEach var="row" select="$doc/ROWSET/ROW">
     
                      <p>
                <label><bean:message key="label.beneficiaryacct"/></label>
                <input class="text-input small-input" type="text" id="beneficiaryacct" name="beneficiaryacct" value="<x:out select="$row/DESTINATION_ACCT"/>" onblur="getBenAccountDetail('001','60122');"  />
                 <input type="hidden" value="<x:out select="$row/DESTINATION_ACCT"/>" id="oldbeneficiaryacct">
            <div id="crbranchcode"></div>
                        <input type="hidden" value="" id="crbrcode" name="crbrcode"/>
                        <input type="hidden" value="" id="ccy" name="ccy"/>
            </p>
            <p>
                <label>Beneficiary Name</label>
                <input class="text-input small-input" type="text" id="beneficiaryname" name="beneficiaryname" value=""  />
                <input type="hidden" value="<x:out select="$row/BENEFICIARY_ID"/>" id="beneficiaryid" name="beneficiaryid">
            </p>
               <p>
                <label><bean:message key="label.txnamount"/></label>
                <input class="text-input small-input" type="text" id="txnamount" name="txnamount" value=""  />
                 
            </p>
     


            <p>
                <label><bean:message key="label.institution"/></label>
                <input class="text-input small-input" disabled="disabled" type="text" id="institution" name="institution" value="<x:out select="$row/INSTITUTION_ID"/>"   />
            </p>
             <p>
                <label><bean:message key="label.description"/></label>
                <input class="text-input small-input" disabled="disabled" type="text" id="description" name="description" value="<x:out select="$row/DESCRIPTION"/>"  />
            </p>
              <p>
                <label><bean:message key="label.beneficiarygrp"/></label>
                <input class="text-input small-input" disabled="disabled" type="text" id="beneficiarygrp" name="beneficiarygrp" value="<x:out select="$row/BENEFTGRPNAME"/>"    z     />
                <input type="hidden"  name="oldbeneficiarygrp" id="oldbeneficiarygrp" value="<x:out select="$row/BENEFTGRPNAME"/>"/>
                <input type="hidden" value="GRP" name="bentype" id="bentype"/>
            </p>
            <p>
                <input type="button" onclick="addBeneficiaries('601221','60122');" rel="modal" class="button" value="<bean:message key="label.addbeneficiary"/>">
                
            </p>


 </x:forEach>

            <div id="ajaxload"></div>
    </form>
</div>


</body>
        </html>
