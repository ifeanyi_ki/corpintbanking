

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>

    </head>
    <body>


        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.CreateUser"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">

                <c:set var="xml" value="${data}" />


                <x:parse varDom="doc" xml="${xml}" />
                <p>
                    <label><bean:message key="label.userid"/><font color="red">*</font></label>
                    <input class="text-input small-input" type="text" id="user" name="user" value="" onblur="validateUser(this.value,'102c')"/><!-- <span class="input-notification error png_bg">Error message</span>-->
                <div id="crbranchcode"></div>
                <input type="hidden" name="user_password"  id="user_password"  value="password"  >

                </p>
                <p>
                    <label>Full Name<font color="red">*</font></label>
                    <input class="text-input small-input" type="text" id="user_name" name="user_name" value=""  /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->

                </p>

                <p>
                    <label><bean:message key="label.lang"/></label>

                    <select name="user_lang" id ="user_lang"  class="small-input">
                        <option value="" selected style="display:none">Select preferred language</option>
                        <x:forEach var="row" select="$doc/ROWSET/ROW">
                            <option value="<x:out select="$row/LANG_ID"/>"><x:out select="$row/LANG_DESC"/></option>
                        </x:forEach>
                    </select>
                </p>
                <p>
                <p>
                    <label ><bean:message key="label.roleid"/><font color="red">*</font></label>
                    <select id="cod_role_id" name="cod_role_id">
                        <option></option>
                        <x:forEach var="role" select="$doc/ROWSET/USERTYPES">
                            <option value="<x:out select="$role/USERTYPE"/>"><x:out select="$role/DESCRIPTION"/></option>
                        </x:forEach>
                    </select>
                    <input type="hidden" value="<x:out select="$doc/ROWSET/USERTYPES/ROLES"/>" name="role_type"  id="role_type"/>
                </p>
                <p>
                    <label><bean:message key="label.email"/><font color="red">*</font></label>
                    <input class="text-input small-input" type="text" id="email" name="email" value="" onblur=" return echeck(this.value);">
                </p>
                <p>
                    <label>Branch Code<font color="red">*</font></label>
                    <input class="text-input small-input" type="text"  id="branchcode" name="branchcode" value="" onchange=" return checkmandatory(this.id,this.name,this.value);"  />

                </p>

                <p>
                    <input type="button" onclick="createUser(1022,'102b');" value="<bean:message key="label.Submit"/>" class="button"/>
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button" />
                </p>





            </form>
            <input type="hidden" id="address"  name="address"   value="">
            <input type="hidden" id="custno"  name="custno"   value="" />
            <input type="hidden" id="phone"  name="phone"   value="" />
            <input type="hidden" name="screenId" id="screenId" value="102b"/>
            <input type="hidden" name="taskId" id="taskId" value="1021"/>
            <input type="hidden" id="user_id"  name="username"  value="<%=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getUserid()%>" />
        </div>
        <div id="ajaxload"></div>

    </body>
</html>
