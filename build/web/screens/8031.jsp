<%-- 
    Document   : 8031
    Created on : 11-Nov-2011, 11:01:04
    Author     : baby
--%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<form>
<p>
    <label><bean:message key="label.servicename"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="servicename" name="servicename" value="" /><!-- <span class="input-notification error png_bg">Error message</span>-->

</p>
<p>
    <label><bean:message key="label.servicedesc"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="servicedesc" name="servicedesc" value=""  /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->

</p>
<p>
    <label><bean:message key="label.serviceacct"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="serviceacct" name="serviceacct" value=""  />

</p>
<p>
    <label><bean:message key="label.email"/><font color="red">*</font></label>
    <input class="text-input small-input" type="text" id="email" name="email" value=""  />

</p>

<p>
    <input type="button" onclick="createservice(8032,'8031');" value="<bean:message key="label.Submit"/>" class="button"/>
    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
</p>
</form>