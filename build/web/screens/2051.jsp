

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>


<html>
    <head>

    </head>
    <body>
        <c:set var="xml" value="${data}" />


        <%System.out.println(pageContext.findAttribute("xml") + "nawao");%>
        <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2" >
            <div class="content-box-header">

                <h3><bean:message key="label.detailloan"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">


                <x:forEach var="row" select="$doc/ROWSET/ROW">


                    <p>
                        <label><bean:message key="label.settlementacc"/></label>
                        <input class="text-input small-input" type="text" id="user" name="user" value="<x:out select="$row/DR_SETL_AC"/>" disabled />
                        <input type="hidden" id="acct"  name="acct" value="<x:out select="$row/DR_SETL_AC"/>"  >
                    </p>
                    <p>
                        <label><bean:message key="label.currency"/></label>
                        <input class="text-input small-input" type="text" id="user_name" name="user_name" value="<x:out select="$row/CURRENCY"/>"  disabled/>
                        <input type="hidden" id="currency"  name="currency"  value="<x:out select="$row/CURRENCY"/>"  >
                    </p>

                    <p>
                        <label><bean:message key="label.principal"/></label>
                        <input class="text-input small-input" type="text" id="principal" name="principal" value="<x:out select="$row/LCY_AMOUNT"/>" disabled />
                    </p>
                    <p>
                        <label><bean:message key="label.amountdue"/></label>
                        <input class="text-input small-input" type="text" id="amountdue" name="amountdue" value="<x:out select="$row/AMOUNT_DUE"/>" disabled />
                    </p>
                    <p>
                        <label><bean:message key="label.duedate"/></label>

                        <c:set var="cd2"  >
                            <x:out select="$row/DUE_DATE"/>
                        </c:set>
                        <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd" var="formatedDate"/>


                        <input class="text-input small-input" type="text" id="duedate" name="duedate" value='<fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy" />' disabled />
                    </p>
                    <p>
                        <label><bean:message key="label.totalsettled"/></label>
                        <input class="text-input small-input" type="text" id="totalsettled" name="totalsettled" value="<x:out select="$row/AMOUNT_SETTLED"/>" disabled />
                    </p>
                    <p>
                        <label><bean:message key="label.outstandingprincipal"/></label>
                        <input class="text-input small-input" type="text" id="outstandingprincipal" name="outstandingprincipal" value="<x:out select="$row/PRINCIPAL_OUTSTANDING_BAL"/>" disabled />
                    </p>
                    <p>
                        <a   href="javascript:void(0)" onclick="listRepayment('<x:out select="$row/CONTRACT_REF_NO"/>', '2052', '2051');" rel="modal" class="button"><bean:message key="label.loanschedule"/></a>

                    </p>

                </x:forEach>
                <input type="hidden" name="screenId" id="screenId" value="101"/>
                <input type="hidden" name="taskId" id="taskId" value="1011"/>

            </form>
        </div>


    </body>
</html>
