
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<div  class="tab-content" id="tab1">
    <div class="content-box-header">

        <h3><bean:message key="label.filter"/></h3>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->
    <div class="notification attention png_bg">
            <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
            <div>
                Kindly enter customer name or number and click the search button
            </div>
        </div>
    <form action="" method="POST">

        <fieldset>

            <p>
                <label><bean:message key="label.CustomerName"/></label>
                <input class="text-input small-input" type="text" id="customername" name="customername" />
                <br /><small>Enter the customer name</small>
            </p>

            <p>
                <label><bean:message key="label.Customerno"/></label>
                <input class="text-input small-input" type="text" id="customerno" name="customerno" />
            </p>

            <p>
                <input type="button" onclick="validateSearch(document.getElementById('customerno').value,document.getElementById('customername').value,'102e','102d');" value="<bean:message key="label.search"/>" class="button">
                <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"></td>
            </p>
        </fieldset>

        


            <input type="hidden" name="screenId" id="screenId" value="102d"/>
            <input type="hidden" name="taskId" id="taskId" value="102e"/>
            <div id="searchresult" >

            </div>

    </form>
</div>

<%



    {
    }
%>
