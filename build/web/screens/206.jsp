


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<p>&nbsp;&nbsp;</p>

<div  class="tab-content" id="tab1">
  <div class="content-box-header">

                <h3><bean:message key="label.fixeddeposit"/></h3></div>

                <div class="clear"></div>
    <div class="clear"></div>
    <fieldset>
        <legend></legend>


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                            <th class="first"><bean:message key="label.contractrefno"/></th>
                            
                            <th><bean:message key="label.principal"/></th>
                            <th><bean:message key="label.maturitydate"/></th>
                            <th><bean:message key="label.Account"/></th>
                            <th><bean:message key="label.startdate"/></th>

                        </tr>

                    </thead>

                    <tfoot>
                        <tr>
                            <td colspan="6">


                                <div class="pagination">
                                                                    </div> <!-- End .pagination -->
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <c:set var="xml" value="${data}" />

                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <c:set var="count" value="${count+1}"/>
                            <c:if  test="${count<=20}">
                                <tr>
                                    <td>
                                        <x:set var="c" select="$row/CONTRACT_REF_NO"/>
                    <label><input name="refno" id="refno" type="radio" value="<x:out select="CONTRACT_REF_NO" />"  onclick="getLoanDetail(this.value,'2061','206');return false"/><x:out select="$c" /></label>
                    <input type="hidden" name="acno" id="acno" value="<x:out select="AC_NO" />"/>

                                    </td>
                                    
                                    <td>

                            <x:out select="$row/LCY_AMOUNT"/>

                                            </td>
                                            <td>
                                                 <c:set var="cd2"  >
                                                <x:out select="$row/MATURITY_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                               <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>
                            
                            
                           
                                            </td>
                                            <td>

                            <x:out select="$row/DFLT_SETTLE_AC"/>

                                            </td>
                                            <td>
                                                
                                            <c:set var="cd2"  >
                                                <x:out select="$row/VALUE_DATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                               <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>
                            
                            
                                            </td>
                                </tr>
                            </tbody>
                        </c:if>
                    </x:forEach>

                </table>

            </fieldset>
            <table>
                <tr>

                    <c:if test="${lowcount>0}">
                        <td>
                            <div align="left">

                                <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                            </div>
                        </td>
                    </c:if>
            </table>
            <c:if test="${size>increment && highcount<size}">


                <div align="center">

                    <input type="submit" name="Submit" value="next" onClick="getNextValue(1011,'101');" class="button">

                </div>


            </c:if>


            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>




