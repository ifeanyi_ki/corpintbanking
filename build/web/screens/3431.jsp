


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<head> <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp"></head>

<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<p>&nbsp;&nbsp;</p>

<div  class="tab-content" id="tab1">

    <div class="clear"></div>
    <fieldset>
        <!--<img src="./img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
        <img src="./img/bg-th-right.gif" width="7" height="7" alt="" class="right" />-->


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                            <th class="first"><bean:message key="label.siInstructionno"/></th>
            <th><bean:message key="label.sidebitaccount"/></th>
            <th><bean:message key="label.sicreditaccount"/></th>
            <th><bean:message key="label.siAmt"/></th>

                        </tr>

                    </thead>

                    <tfoot>
                        <tr>
                            <td colspan="6">


                                <div class="pagination">
                                    <a href="#" title="First Page">&laquo; First</a><a href="#" title="Previous Page">&laquo; Previous</a>
                                    <a href="#" class="number" title="1">1</a>
                                    <a href="#" class="number" title="2">2</a>
                                    <a href="#" class="number current" title="3">3</a>
                                    <a href="#" class="number" title="4">4</a>
                                    <a href="#" title="Next Page">Next &raquo;</a><a href="#" title="Last Page">Last &raquo;</a>
                                </div> <!-- End .pagination -->
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <c:set var="xml" value="${data}" />

                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <c:set var="count" value="${count+1}"/>
                            <c:if  test="${count<=20}">

                                <tr>
                                    <td>
                                       <x:set var="c" select="$row/INSTRUCTION_NO"/>
                    <input id="instnos" name="instnos" type="radio" value="<x:out select="INSTRUCTION_NO" />" onclick="showSiToClose(3432,3431);"/><x:out select="$c" /></label>
                     <input type="hidden" name="acct_branch" id="acct_branch" value="<x:out select="$row/DR_ACC_BR" />"/>

                                    </td>
                                    <td>
                                    <x:set var="c" select="$row/DR_ACCOUNT"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <c:choose>
                            <c:when test="${check!=null}">
                            <x:out select="$row/DR_ACCOUNT"/>

                        </c:when>
                            <c:otherwise>
                            <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                    <input type="hidden" value="<x:out select="$row/DR_ACCOUNT"/>" id="acct_no" name="acct_no">
                                    </td>
                                   <td>
                    <x:set var="c" select="$row/CR_ACCOUNT"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <c:choose>
                            <c:when test="${check!=null}">
                            <x:out select="$row/CR_ACCOUNT"/>

                        </c:when>
                            <c:otherwise>
                            <c:out value=""/>
                            </c:otherwise>
                        </c:choose>

                </td>
                <td>
                   <x:set var="c" select="$row/SI_AMT"/>
                    <c:set var="check">
                        <x:out select="$c"/>
                    </c:set>

                        <c:choose>
                            <c:when test="${check!=null}">
                            <x:out select="$row/SI_AMT"/>

                        </c:when>
                            <c:otherwise>
                            <c:out value=""/>
                            </c:otherwise>
                        </c:choose>

                </td>


            </tr>
                            </tbody>
                        </c:if>
                    </x:forEach>

                </table>

            </fieldset>
            <table>
                <tr>

                    <c:if test="${lowcount>0}">
                        <td>
                            <div align="left">

                                <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                            </div>
                        </td>
                    </c:if>
            </table>
            <c:if test="${size>increment && highcount<size}">


                <div align="center">

                    <input type="submit" name="Submit" value="next" onClick="getNextValue(3431,'343');" class="button">

                </div>


            </c:if>


            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
            <input type="hidden" name="instnos" id="instnos" value="<x:out select="$doc/ROWSET/ROW/INSTRUCTION_NO" />"/>

            <c:set var="acct" value="${acct}" />
     <input type="hidden" name="acctno" id="acctno" value="<c:out value="${acct}"/>">
     <input type="hidden" name="check" id="check" value="${drcr}">
     <input type="hidden" name="startDate" id="startDate" value="${startDate}">
     <input type="hidden" name="endDate" id="endDate" value="${endDate}">
     <input type="hidden" name="from" id="from" value="${from}">
     <input type="hidden" name="to" id="to" value="${to}">

     <input type="hidden" name="taskId" id="taskId" value="3411"/>
                           <input type="hidden" name="detail" id="detail" value="true"/>
                           <input type="hidden" id="screen_id" value=3411""/>
                            <input type="hidden" name="instno" id="instno" value=" "/>
                     <input type="hidden" name="detail" id="detail" value="false"/>
                     
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>




