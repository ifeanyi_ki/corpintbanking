<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style media="all" type="text/css">@import "css/wbsFormElements.css";</style>
<html:html lang="true" >
   
    <head>

    <meta name="robots" content="noindex, nofollow">
               <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="Pragma" content="no-cache">
            <meta http-equiv="Cache-Control" content="no-cache">
	<title>E Banking</title>
	<script language=JavaScript src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
         <link rel="stylesheet" href="screens/scripts/keyboard.css"  type="text/css" rel="stylesheet"/>
        <script language=JavaScript src="screens/scripts/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/ui-lightness/jquery-ui-1.8.12.custom.css" type="text/css" rel="stylesheet"/>

         <!-- <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
         <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.12.custom.css" type="text/css" rel="stylesheet"/>-->
	<SCRIPT language=JavaScript src="screens/scripts/jquery.keyboard.js" type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/jquery.mousewheel.js"  type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
        <style type="text/css">
		body, div, p, blockquote, ol, ul, dl, li, dt, dd, table, tbody, td, th, caption { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; }
		tr, td { vertical-align:top; }
		h1, h2, h3, h4 { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; font-weight:bold; line-height:1em; }
		h2 { font-size:110%; font-style:italic; }
		.mywbs_formlabel { font-style:italic;font-weight:bold }
		#trAjax { display:none; }
		.error, .important { color:#990033; font-weight:bold; }
	</style>

		<script type="text/javascript" src="./Sign in to my.wbs_files/jquery-1.4.2.min.js"></script>			
	
		<script type="text/javascript">
		
		</script>
    </head>

    <body style="margin:100px;padding:0" onload="keyboards();">
	<table style="width:100%;height:100%;border:none" cellpadding="20" cellspacing="0" summary="Introduction to my.wbs">
		<tbody><tr>
			<td style="background:#E2ECF3;min-width:270px">
                               <img src="screens/scripts/resources/images/slcb_lion.gif">
				<img src="screens/scripts/resources/images/slcb_banner.gif">
                                
				<p>&nbsp;</p>

				<div style="background:#92B6CF;padding:0.5em">
				  <p>&nbsp;</p>
			</div></td>
			<td style="width:300px;padding-top:32px;">
				<div>
					<h2><img src="screens/scripts/resources/images/signIn.gif"> Sign In</h2>
					
					
					<noscript>
						&lt;br /&gt;
						&lt;div style="background:#ffc;color:red;border:1px solid red;padding:10px"&gt;
							&lt;h2&gt;Javascript is disabled&lt;/h2&gt;
							
							&lt;p&gt;Please note that some areas of this site use Javascript.
							We recommend that you enable Javascript on your browser before proceeding.&lt;/p&gt;
						&lt;/div&gt;							
					</noscript>							
					
					<div class="wbsFormElements">
						<html:form action="Main.do"  >
							<fieldset>
								
			
		      <div class="label">
                              




                                 <div class="spacer"></div></div>
			
			<div id="userCode_field" class="row" >
                            <div class="label" nowrap>
						
                                <p><label for="userCode" id="userCode_label"><bean:message key="label.userid"  /></label></p>
                                        
                            </div>

                            <div class="formElement ">

                                <p> <html:text property="userid" ></html:text></p>
                           
                                <p><html:messages id="errors"><font color="red"><Strong><div class="label"><bean:write name="errors" /></div></Strong> </font> </html:messages></p>

			
		</div><div class="spacer"></div></div>
	
			
			<div id="password_field" class="row "><div class="label" nowrap>
                                <p><label for="password" id="password_label">
                                        <bean:message key="label.password"/>
			</label></p>
		</div>
                        <div class="formElement ">
		
                            <p><html:password property="password"></html:password></p>
                            <p>

		</div><div class="spacer"></div></div>
	
			
			<div id="signIn_field" class="row "> <div class="formElement ">
		
				<span title="">
			
		</span>
			
			<!--<button id="signIn" title="" type="submit" onclick="" onkeyup="" name="login" value="" tabindex="20">-->
                            <input type="button" class="buttons" onclick="showinit();" value="<bean:message key="label.Submit"/>">
                                <input type="reset" class="buttons"  onclick="javascript:window.close();" value=Reset>
			<!--</button-->
			
		</div><div class="spacer"></div></div>
	

								
								<div style="margin-top:2.5em;border-top:solid 1px #666;font-size:0.8em">
									<div id="trAjax" style="padding-top: 5px; display: block; ">
										<!--<input type="checkbox" id="cbAjax" name="ajax" value="T">Enable progressive page build?-->
									</div>
									<p>
										By signing in, I accept the <a href="http://slcb.com" target="_blank">Terms of Use</a>.
										Authorised access only.<a href="http://slcb.com" target="_blank">Problems signing in?</a>
									</p>
								</div>
							</fieldset>
						</html:form>
					</div>
				</div>
			</td>
		</tr>
	</tbody></table>
	<script src="./Sign in to my.wbs_files/urchin.js" type="text/javascript">
	</script>
	<script type="text/javascript">
	_uacct = "UA-1894511-1";
	urchinTracker();
	</script>


</body>
 
</html:html>