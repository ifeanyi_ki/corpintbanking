
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.InterBankBeneficiaryBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.InterBankBeneficiaryBo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.fasyl.corpIntBankingadmin.vo.BankVo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <%!
        String bankCode;
        List<BankVo> bankList;
        InterBankBeneficiaryBo interBankBeneficiaryBo;
        String userId;
        String beneficiaryName;
    %>
    <%
        bankList = new ArrayList<BankVo>();
        interBankBeneficiaryBo = new InterBankBeneficiaryBoImpl();
        bankList = interBankBeneficiaryBo.getBankList();
        HttpSession session1 = request.getSession(false);
        userId = (String) session1.getAttribute("user_id");
        String benBank = (String) request.getAttribute("benBank");
        String benBankName = "";
        for (BankVo bankVo : bankList){
            if(bankVo.getBankCode().equals(benBank)){
                benBankName = bankVo.getBankName();
            }
        }
        String benAcctNo = (String) request.getAttribute("benAccNo");
        beneficiaryName = (String) request.getAttribute("benName");
        System.out.println("userid: " + userId + ", benName: " + beneficiaryName);
    %>

    <body>

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Add Inter Bank Beneficiary</h3>

                <div class="clear"></div>

            </div>

            <form>
                <p>
                    <input  type="text"  value="<%=userId%>" id="interbankuserid" class="text-input small-input" hidden="true"/>
                </p> 
                <p>
                    <label>Beneficiary's Account<font color="red">*</font></label> 
                        <% if (benAcctNo != null ) {%>
                    <input  type="text"  value="<%= benAcctNo%>" id="interBenAccNo" class="text-input small-input" required/>
                    <% } else { %>
                    <input  type="text"  value="" id="interBenAccNo" class="text-input small-input" required/>
                    <%  }%>
                </p> 
                <br/>
                <p>
                    <label>Beneficiary's Bank<font color="red">*</font></label>
                    <select type="text"  value="<%=bankCode%>" id="interBenBank" onchange=showAuthBen('doNameEnquiry')>
                        <% if (benBank != null ) {%>
                        <option value=<%=benBank%>><%=benBankName%></option>
                        <% } else { %>
                        <option value="">-- Select Bank --</option>
                        <%  } %>

                        <% for (BankVo bankVo : bankList) {%>
                        <% bankCode = bankVo.getBankCode();%>
                        <option value=<%=bankVo.getBankCode()%>><%= bankVo.getBankName()%></option>
                        <% }%>
                    </select>
                </p>
                <br/>
                <p>
                    <% if (beneficiaryName == "failed" || beneficiaryName == null || beneficiaryName.equalsIgnoreCase("")) {%>
                <p>
                    <label >Beneficiary's Account Name<font color="red">*</font></label>
                    <input type="text"  value="" id="interbankAccName" class="text-input small-input" required/>
                    <label id="account_null_message"><font color="red">Unable to fetch Beneficiary's name from selected Bank</font></label>
                </p>
                <%  } else {%>

                <p>
                    <label >Beneficiary's Account Name<font color="red">*</font></label>
                        <% if (beneficiaryName.equalsIgnoreCase("first")) {
                                beneficiaryName = "";
                            }%>
                    <input type="text"  value="<%=beneficiaryName%>" id="interbankAccName" class="text-input small-input" required/>
                </p>

                <% }%>
                </p>
                <br/>

                <p>
                    <label >Amount<font color="red">*</font></label>
                    <input type="text"  value="" id="interbankAmt" class="text-input small-input" required/>
                </p>
                <br/>

                <p>
                    <input type="button"  class="button" value="Submit" onClick="showAuthBen('createBen')" >
                </p>


            </form>
        </div>
    </body>
</html>
