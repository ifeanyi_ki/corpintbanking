
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>

    <body>
        <%!
            String user_id = "";
            String email = "";
            String mobile_number = "";
            String first_name = "";
            String last_name = "";
            String client = "";
            String role_id = "";
            String created_by = "";
            String active = "";

        %>


        <%
            UserVo userVo = (UserVo) request.getAttribute("userVo");

            role_id = (String) userVo.getRoleId() == null ? "" : (String) userVo.getRoleId();
            user_id = (String) userVo.getId() == null ? "" : (String) userVo.getId();
            email = (String) userVo.getEmail() == null ? "" : (String) userVo.getEmail();
            mobile_number = (String) userVo.getPhoneNumber() == null ? "" : (String) userVo.getPhoneNumber();
            first_name = (String) userVo.getFirstName() == null ? "" : (String) userVo.getFirstName();
            last_name = (String) userVo.getLastName() == null ? "" : (String) userVo.getLastName();
            client = (String) userVo.getClient() == null ? "" : (String) userVo.getClient();
            created_by = (String) userVo.getCreated_by() == null ? "" : (String) userVo.getCreated_by();
            active = (String) userVo.getActive() == null ? "" : (String) userVo.getActive();


        %>

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Edit User</h3>

                <div class="clear"></div>

            </div>
            <form>
                <p>
                    <label>Client</label>
                    <input type="text" readonly="true" value="<%= client%>" class="text-input small-input" />
                    <input type ="hidden" value ="<%= client%>" id="client" >
                </p>
                <br/>
                <p>
                    <label>User Id</label>
                    <input name="user_id" type="text" readonly="true" value=" <%= user_id%>" class="text-input small-input" />
                    <input type ="hidden" value ="<%= user_id%>" id="user_id" >
                </p>
                <br/>
                <p>
                    <label>Email Address</label>
                    <input class="text-input small-input" type ="text" value ="<%= email%>" id="email" />
                </p>
                <br/>
                <p>
                    <label>Mobile Number</label>
                    <input class="text-input small-input" type ="text" value ="<%= mobile_number%>" id="mobile_number" />
                </p>
                <br/>
                <p>
                    <label>First Name</label>
                    <input class="text-input small-input" type ="text" value ="<%= first_name%>" readonly="true"/>
                    <input type ="hidden" value ="<%= first_name%>" id="first_name" >
                </p>
                <br/>
                <p>
                    <label>Last Name</label>
                    <input class="text-input small-input" type ="text" value ="<%= last_name%>" readonly="true"/>
                    <input type ="hidden" value ="<%= last_name%>" id="last_name" >
                </p>
                <br/>
                <p>
                    <label>Role</label>  

                    <select type="text"  value="<%= role_id%>" id="role_id" required>
                        <option value="<%= role_id%>"><%=role_id%></option>
                        <option value="INITIATOR" >INITIATOR</option>
                        <option value="Authorizer">AUTHORIZER</option>
                    </select>
                </p>
                <br/>
                <p>
                    <label>Created By</label>
                    <input class="text-input small-input" type ="text" value ="<%= created_by%>" readonly="true"/>
                    <input type ="hidden" value ="<%= created_by%>" id="created_by" >
                </p>
                <br/>
                <p>
                    <label>Active</label>
                    <select type="text"  value="<%= active%>" id="active" required>
                        <option value="<%= active%>"><%=active%></option>
                        <option value="Y" >Y</option>
                        <option value="N">N</option>
                    </select>
                </p>
                <br/>
                <input type="button"  class="button" value="Submit" onClick="movetopage('movetopage', 'edit_user_verify.jsp')" />  &nbsp; &nbsp;
                <input type="button"  class="button" value="Reset Password" onClick="resetpwd('reset_password')" />      


            </form>
        </div>
    </body>
</html>
