<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="com.fasyl.corpIntBankingadmin.vo.ftVo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.FundTransferBo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Pending Fund Transfer</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Post Approved Transfers</h3>

                <div class="clear"></div>

            </div> 

            <%
                FundTransferBo fundTransferBo = new FundTransferBoImpl();
                user_id = (String) session.getAttribute("user_id");
                role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
                out.println(result + "<br/>");

                List<ftVo> ftVOORList = fundTransferBo.getAuthorizedFundTrxOfOROperator(user_id);
                List<ftVo> ftVOOnlyList = fundTransferBo.getAuthorizedFundTrxOfONLYOperator(user_id);
                List<ftVo> ftVOANDList = fundTransferBo.getAuthorizedFundTrxOfAndOperator(user_id);

                int size = ftVOORList.size() + ftVOOnlyList.size() + ftVOANDList.size();
            %>

            <%  if (size == 0) {%>

            <div class="content-box-header">

                <h3><%= size%>  transactions is/are pending for posting</h3>

                <div class="clear"></div>

            </div> 
            <% } else { %>

            <%!
                String init_account;
                String ben_acc;
                String benAccName;
                String benBank;
                String amt;
                String initiator_id;
                String user_id;
                String trans_id;
                String result;
                String role_id;
                String trx_type;
                String narration;
                String benCode;

            %>
            <br/>
            <table>    
                <tr>       
                    <!--<td>Transaction Id</td>-->
                    <td>Account</td>
                    <td>Beneficiary Account</td>
                    <td>Amount</td>
                    <td></td>
                    <td></td>
                </tr>
                <%

                    for (ftVo ftVO : ftVOOnlyList) {
                        init_account = ftVO.getAcc();
                        ben_acc = ftVO.getBen_acc();
                        amt = String.valueOf(ftVO.getAmt());
                        trans_id = ftVO.getTrans_id();
                        trx_type = ftVO.getTrxType();
                        narration = ftVO.getNarration();
                        benAccName = ftVO.getBene_acc_name();
                        benBank = ftVO.getBene_bank();
                        benCode = ftVO.getBene_bank_code();
                %>


                <tr> 
    <!--                <td>     <%=trans_id%></td>-->
                    <td>     <%=init_account%></td>
                    <td>     <%=ben_acc%> </td>
                    <td>     <%=amt%> </td>


                    <td>
                        <form>
                            <input value="View Detail" class="button" type="button" 
                                   onClick="getdetail('get_ft_details', '<%= trans_id%>')" /> 
                        </form>
                    </td>

                    <td>
                        <form>
                            <% if (trx_type.equalsIgnoreCase("Third Party Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <% } else if (trx_type.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="makepayment(70151, '7016', '<%=init_account%>',
                                                   '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>');" /> 
                            <%}else if(trx_type.equalsIgnoreCase("Local Transfer")){%>
                                <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33310,'3331', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <%}%>
                        </form>
                    </td>
                </tr>
                <%}%>

                <%
                    if (ftVOORList.size() != 0) {

                        for (ftVo ftVO : ftVOORList) {
                            init_account = ftVO.getAcc();
                            ben_acc = ftVO.getBen_acc();
                            amt = String.valueOf(ftVO.getAmt());
                            trans_id = ftVO.getTrans_id();
                            trx_type = ftVO.getTrxType();
                            narration = ftVO.getNarration();
                            benAccName = ftVO.getBene_acc_name();
                            benBank = ftVO.getBene_bank();
                            benCode = ftVO.getBene_bank_code();
                %>


                <tr> 
    <!--                <td>     <%=trans_id%></td>-->
                    <td>     <%=init_account%></td>
                    <td>     <%=ben_acc%> </td>
                    <td>     <%=amt%> </td>


                    <td>
                        <form>
                            <input value="View Detail" class="button" type="button" onClick="getdetail('get_ft_details', '<%= trans_id%>')" /> 
                        </form>
                    </td>

                    <td>
                        <form>
                            <% if (trx_type.equalsIgnoreCase("Third Party Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <% } else if (trx_type.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="makepayment(70151, '7016', '<%=init_account%>',
                                                   '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>');" /> 
                          
                             <%}else if(trx_type.equalsIgnoreCase("Local Transfer")){%>
                                <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33310,'3331', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <%}%>
                        </form>
                    </td>
                </tr>
                <%}%>
                <% } %>

                <%
                    if (ftVOANDList.size() != 0) {

                        for (ftVo ftVO : ftVOANDList) {
                            init_account = ftVO.getAcc();
                            ben_acc = ftVO.getBen_acc();
                            amt = String.valueOf(ftVO.getAmt());
                            trans_id = ftVO.getTrans_id();
                            trx_type = ftVO.getTrxType();
                            narration = ftVO.getNarration();
                            benAccName = ftVO.getBene_acc_name();
                            benBank = ftVO.getBene_bank();
                            benCode = ftVO.getBene_bank_code();
                %>


                <tr> 
    <!--                <td>     <%=trans_id%></td>-->
                    <td>     <%=init_account%></td>
                    <td>     <%=ben_acc%> </td>
                    <td>     <%=amt%> </td>


                    <td>
                        <form>
                            <input value="View Detail" class="button" type="button" onClick="getdetail('get_ft_details', '<%= trans_id%>')" /> 
                        </form>
                    </td>

                    <td>
                        <form>
                            <% if (trx_type.equalsIgnoreCase("Third Party Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <% } else if (trx_type.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                            <input value="Submit" class="button" type="button" 
                                   onClick="makepayment(70151, '7016', '<%=init_account%>',
                                                   '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>');" /> 
                             <%}else if(trx_type.equalsIgnoreCase("Local Transfer")){%>
                                <input value="Submit" class="button" type="button" 
                                   onClick="Transfer(33310,'3331', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                                   '<%=narration%>', '<%=trans_id%>')" />
                            <%}%>
                        </form>
                    </td>
                </tr>
                <%}%>
                <% } %>
            </table><br/>
            <% }%>
        </div>         

    </body>
</html>
