
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="com.fasyl.ebanking.dto.InterBeneficiaryDto"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.InterBankBeneficiaryBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.InterBankBeneficiaryBo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.PasswordChangeBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.PasswordChangeBo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <%
            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
                <h3>View Inter Bank Beneficiary</h3>
                <div class="clear"></div>
            </div> 
            <%!
                List<InterBeneficiaryDto> interBenList;
                InterBankBeneficiaryBo interBenBo;
                String result;
            %>
            <%
                String benId = "";
                String benAcc = "";
                String benName = "";
                String benBank = "";
                String amt = "";
                String custNo = "";
                interBenBo = new InterBankBeneficiaryBoImpl();
                interBenList = interBenBo.getInterBenList(((UserVo) session.getAttribute("userVo")).getClient());
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");

                if (result != "") {
            %>
            <div class="content-box-header">

                <h3><%=result%></h3>

                <div class="clear"></div>

            </div> 
            <% }%>
            <% if (interBenList.isEmpty()) {%>
            <div class="content-box-header">
                <h3>Inter Beneficiary List is empty</h3>
                <div class="clear"></div>
            </div> 

            <% } else { %>

            <table>
                <th>Beneficiary Account</th>
                <th>Beneficiary Bank</th>
                <th>Beneficiary Name</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                    <%
                        for (InterBeneficiaryDto iBenDto : interBenList) {
                            benId = iBenDto.getBeneficiary_id();
                            benAcc = iBenDto.getDestination_acct();
                            benBank = iBenDto.getBenBank();
                            amt = iBenDto.getAmount();
                            custNo = iBenDto.getCustomer_no();
                            benName = iBenDto.getBeneficiary_name();
                    %>

                <br/><br/>
                <tr>
                <form >
                    <td>
                        <%=benAcc%>  
                        <input type="hidden" id ="benAcct"  value ="<%=benAcc%>" /> 
                    </td>
                    <td>
                        <%=benBank%>
                        <input type="hidden" id ="benBank"  value ="<%=benBank%>" /> 
                    </td>
                    <td>
                        <%=benName%> 
                        <input type="hidden" id ="benName"  value ="<%=benName%>" /> 
                    </td>
                    <td>
                        <input type="button" class="button" onclick="showAuthBen('modBen')" value="Update" />
                        <input type="hidden" id = "<%=benId%>"  value ="<%=benId%>" />       
                    </td>
                    <td>
                        <input type="button" class="button" onclick="showAuthBen('delBen')" value="DELETE" />
                        <input type="hidden" id = "benId"  value ="<%= benId%> " />       
                    </td>
                    <input type="hidden" id ="amt"  value ="<%=amt%>" />       
                </form>

                </tr>
                <% }%>

            </table>
            <% }%>
            <br/>

        </div>         

    </body>
</html>
