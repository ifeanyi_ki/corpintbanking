
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.PasswordChangeBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.PasswordChangeBo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Password Change Request List</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Password Change Request</h3>

                <div class="clear"></div>

            </div> 
            

            <%!
                String user_id;
                String result;
                List<String> useridList;
                PasswordChangeBo passwordChangeBo;
            %>


            <%
                passwordChangeBo = new PasswordChangeBoImpl();
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
               
                useridList = passwordChangeBo.getListofPendingPasswordChangeRequest((String)session.getAttribute("user_id"));
                if (result != "") {
            %>

            <div class="content-box-header">

                <h3><%=result%></h3>

                <div class="clear"></div>

            </div> 
            <% }%>
            

            <% if (useridList.isEmpty()) {%>
            <div class="content-box-header">

                <h3>Number of pending password change request is ${fn:length(useridList)}</h3>

                <div class="clear"></div>

            </div> 

            <% } else { %>
           
            <table>
                <%
                    for (int i = 0; i < useridList.size(); i++) {
                        user_id = useridList.get(i);
                %>

                <br/><br/>
                <tr>
                <form >
                    <td>
                        <%= user_id%>  
                    </td>

                    <!--                    <td>
                                            <input type="button" class="button" onclick="viewUsers('user_details', '<%= user_id%>')" value="View User" />
                                            <input type="hidden" id = "<%= user_id%> "  value ="<%= user_id%> " />       
                                        </td>-->

                    <td>
                        <input type="button" class="button" onclick="auth('approve_pwd_change', '<%= user_id%>')" value="APPROVE" />
                        <input type="hidden" id = "<%= user_id%> "  value ="<%= user_id%> " />       
                    </td>

                    <td>
                        <input type="button" class="button" onclick="auth('decline_pwd_change', '<%= user_id%>')" value="DECLINE" />
                        <input type="hidden" id = "<%= user_id%> "  value ="<%= user_id%> " />       
                    </td>

                    <!--<p id="loghc" style="height: 10px;"> </p> i don't know waht is doing here-->
                </form>

                </tr>
                <% }%>

            </table>
            <% }%>
            <br/>

        </div>         

    </body>
</html>
