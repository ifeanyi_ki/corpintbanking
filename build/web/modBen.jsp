

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <%
            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
            String benAcct = request.getParameter("benAcct");
            String benBank = request.getParameter("benBank");
            String benName = request.getParameter("benName");
            String amt = request.getParameter("amt");
            String benId = request.getParameter("benId");
        %>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Modify Inter Bank Beneficiary</h3>

                <div class="clear"></div>

            </div> 

            <form>
                <input class="text-input small-input" type="hidden"  value="<%=benId%>" id="benId"  />
                <p>
                    <label>Beneficiary Account Number</label>
                    <input class="text-input small-input" type="text"  value="<%=benAcct%>" id="benAcct"  disabled="true"/>
                </p>
                
                <p>
                    <label>Beneficiary Bank</label>
                    <input class="text-input small-input" type="text"  value="<%=benBank%>" id="benBank"  disabled="true"/>
                </p>

                <p>
                    <label>Beneficiary Name</label>
                    <input class="text-input small-input" type="text"  value="<%=benName%>" id="benName"  disabled="true"/>
                </p>
                
                <p>
                    <label>Amount</label>
                    <input class="text-input small-input" type="text"  value="<%=amt%>" id="amt"  />
                </p>


                <input type="button"  class="button" value="Submit" onClick="showAuthBen('updateBen')" />

            </form>
        </div>
    </body>
</html>
