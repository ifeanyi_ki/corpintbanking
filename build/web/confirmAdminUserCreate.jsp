
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
                <h3>Please Confirm Entry</h3>
                <div class="clear"></div>
            </div>
            <form>
<!--                <p>
                    <label>Client</label>
                    <input type="text" value="<%= (String) request.getParameter("client")%>" readonly="true" class="text-input small-input"/> 
                    <input type="hidden"  value="<%= (String) request.getParameter("client")%>"  class="input_summary" id ="client"  readonly>
                </p> 
                <br/>-->
                <p>
                    <label>User Id</label>
                    <input type="text" value="<%=(String) request.getParameter("user_id")%> " readonly="true" class="text-input small-input"/> 
                    <input type="hidden" value="<%= (String) request.getParameter("user_id")%>"  class="input_summary" id ="user_id" readonly>
                </p>
                <br/>
                <p>
                    <label>Email Address</label>
                    <input type="text" value="<%= (String) request.getParameter("email")%>" readonly="true" class="text-input small-input"/> 
                    <input type="hidden" value="<%= (String) request.getParameter("email")%>" class="input_summary"   id ="email"  readonly>
                </p>
                <br/>
                <p>
                    <label>Mobile Number</label>
                    <input type="text" value="<%= (String) request.getParameter("mobile_number")%>" readonly="true" class="text-input small-input"/> 
                    <input  type="hidden" value="<%= (String) request.getParameter("mobile_number")%>"  class="input_summary" id ="mobile_number" readonly>
                </p>
                <br/>
                <p>
                    <label>First Name</label>
                    <input type="text" value="<%= (String) request.getParameter("first_name")%>" readonly="true" class="text-input small-input"/> 
                    <input  type="hidden" value="<%= (String) request.getParameter("first_name")%>" class="input_summary"  id ="first_name"   readonly>
                </p>
                <br/>
                <p>
                    <label>Last Name</label>
                    <input type="text" value="<%= (String) request.getParameter("last_name")%>" readonly="true" class="text-input small-input"/> 
                    <input type="hidden"  value="<%= (String) request.getParameter("last_name")%>"  class="input_summary" id ="last_name"  readonly>
                </p>
                <br/>
                <p>
                    <label>Role</label>
                    <input type="text" value="<%= (String) request.getParameter("role_id")%>" readonly="true" class="text-input small-input"/> 
                    <input type="hidden"  value="<%= (String) request.getParameter("role_id")%>"  class="input_summary" id ="role_id"  readonly>
                </p>
                <br/>
                <input type="button"  class="button" value="Edit" onClick="confirmAdminUserCreation('edit_admin_create')" >  &nbsp; 
                <input type="button"  class="button" value="Submit" onClick="confirmAdminUserCreation('Submit_admin_create')" >


            </form>
    </body>
</html>
