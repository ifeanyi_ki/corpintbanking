<%@page import="com.fasyl.ebanking.main.DataObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Corporate List</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">

            <div class="content-box-header">

                <h3>Mandate List</h3>

                <div class="clear"></div>

            </div>
            <%!
                String customer_name;
                String result;
            %>

            <%
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
            %>
            <br/><br/>
            <h3<% out.println(result);%></h3>

            <%
                DataObject dob = new DataObject();
                String account = (String) request.getAttribute("account");
                List<String[]> list = dob.getLists("select  lower_limit, upper_limit, auth1, operator, auth2 from account_mandate where account = '" + account + "'", 5);
            %>

            <%if (list.size() == 0 || list == null) { %>

            <h3><% out.println("Mandate list is empty.");%></h3>
            <%  } else {  %>

            <table>

                <thead> 
                    <tr>
                        <td>LOWER LIMIT</td> 
                        <td>UPPER LIMIT</td> 
                        <!--<td>AUTHORIZER A</td>-->
                        <td>OPERATOR</td> 
<!--                        <td>AUTHORIZER B</td> -->
                        <td>&nbsp;</td> 
                        <td>&nbsp;</td>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="7">


                            <div class="pagination">



                            </div> <!-- End .pagination -->
                            <div class="clear"></div>
                        </td>
                    </tr>
                </tfoot>
                <% for (int i = 0; i < list.size(); i++) {
                        String lo_limit = list.get(i)[0];
                        String up_limit = list.get(i)[1];
                %>
                <form>
                    <tbody>
                        
                            <td><%= lo_limit%></td>
                            <td><%= list.get(i)[1]%></td>
                            <!--<td> <%= list.get(i)[2]%></td>-->
                            <td><%= list.get(i)[3]%></td>
                            <!--<td>-->
                                <input type="hidden" id = "account"  value ="<%= account%>" /> 
                            <!--</td>-->
                            <td width="50">   <input style="width: 30px" href="#" class="button" value="EDIT" onClick="editmandate('edit_mandate1', '<%= list.get(i)[2]%>', '<%= list.get(i)[3]%>', '<%= list.get(i)[4]%>')" /> </td>

                            <td width="50">   <input href="#" style="width: 60px" class="button" value="REMOVE" onClick="editmandate('remove_mandate', '<%= list.get(i)[2]%>', '<%= list.get(i)[3]%>', '<%= list.get(i)[4]%>')" /></td> 
                        
                    </tbody>
                </form>
                <% }%>
            </table>

            <% }%>

            <!--<br/>-->
            <input type="button" class="button" value="Back" onclick="goBack('back', '<%= account%>')" />

        </div>         

    </body>
</html>
                               
                               
