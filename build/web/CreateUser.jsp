<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    
    
    <body>

        <%!
            String user_id = "";
            String email = "";
            String mobile_number = "";
            String first_name = "";
            String last_name = "";
        %>


        <%
            String user_id = (String) session.getAttribute("user_id");

            user_id = (String) request.getParameter("user_id") == null ? "" : (String) request.getParameter("user_id");
            email = (String) request.getParameter("email") == null ? "" : (String) request.getParameter("email");
            mobile_number = (String) request.getParameter("mobile_number") == null ? "" : (String) request.getParameter("mobile_number");
            first_name = (String) request.getParameter("first_name") == null ? "" : (String) request.getParameter("first_name");
            last_name = (String) request.getParameter("last_name") == null ? "" : (String) request.getParameter("last_name");
        %>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Create User</h3>

                <div class="clear"></div>

            </div>

            <form>
                <p>
                    <label>User Id</label> 
                    <input  type="text"  value="<%= user_id%>" id="user_id" class="text-input small-input" required/>
                </p> 
                <br/>
                <p>
                    <label>Email Address</label>
                    <input type="text"  value="<%= email%>" id="email"  class="text-input small-input" required/>
                </p>
                <br/>
                <p>
                <label>Mobile Number</label>
                <input type="text"  value="<%= mobile_number%>" id="mobile_number" class="text-input small-input" required/><span> Format: 08091110001</span>
                </p>
                <br/>
                <p>
                    <label>First Name</label>
                    <input type="text"  value="<%= first_name%>" id="first_name" class="text-input small-input" required/>
                </p>
                <br/>
                <p>
                    <label>Last Name</label>
                    <input type="text"  value="<%= last_name%>" id="last_name" class="text-input small-input" required/>
                </p>
                <br/>
                <p>
                    <label>Role</label>
                    <select type="text"  value="" id="role_id">
                        <option value="">-- Select Role --</option>
                        <option value="INITIATOR" >INITIATOR</option>
                        <option value="Low Level Authorizer">Low Level Authorizer</option>
                        <option value="High Level Authorizer" >High Level Authorizer</option>
                    </select>
                </p>
<!--                <br/>
                <p>
                    <label>Transaction Limit</label>
                    <input type="text"  value="" id="trans_limit" class="text-input small-input"/>
                </p>-->
                <br/>
                <p>
                <input type="button"  class="button" value="Submit" onClick="confirmAdminUserCreation('create_admin_user')" >
                </p>


            </form>
        </div>
    </body>
</html>
