<%@page import="com.fasyl.corpIntBankingadmin.bo.FundTransferBo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Account List</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Please Choose Account</h3>

                <div class="clear"></div>

            </div>

            <br/><br/>
            <%!
                String account;
            %>
            <%
                FundTransferBo fundTransferBo = new FundTransferBoImpl();
                String user_id = (String) session.getAttribute("user_id");
                List<String> accList = fundTransferBo.getAccountListForAnInitiator(user_id);

            %>
            <form>
                <p>
                    <label>Account number</label>
                    <select type="text"  value="" id="<%=account%>">
                        <option value="">-- Select Account Number --</option>
                        <% for (int i = 0; i < accList.size(); i++) {
                                    account = accList.get(i);%>
                        <option value="<%=account%>"><%=account%></option>
                        <% }%>
                    </select>

                </p>
                <br/>
                <input type="button" class="button" value="Submit" 
                       onClick="transfer_fund('transfer_fund', '<%=account%>')"/>

                    <!--<input type="hidden" id="<%=account%>"  value ="<%=account%>" />-->

            </form>

        </table>
    </div>
</div>         
</body>
</html>


