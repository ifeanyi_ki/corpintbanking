

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Change Password</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Password Change Request</h3>

                <div class="clear"></div>

            </div> 

            <form>
                <p>
                    <label>Old Password</label>
                    <input class="text-input small-input" type="password"  value="" id="oldpasswd"  required/>
                </p>
                
                <p>
                    <label>New Password</label>
                    <input class="text-input small-input" type="password"  value="" id="newpasswd"  required/>
                </p>

                <p>
                    <label>Confirm New Password</label>
                    <input class="text-input small-input" type="password"  value="" id="confirmpasswd"  required/>
                </p>

                <input type="button"  class="button" value="Submit" onClick="change_password('change_pass')" />

            </form>
        </div>
    </body>
</html>
