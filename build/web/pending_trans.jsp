<%@page import="com.fasyl.corpIntBankingadmin.vo.FundTransferVo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.FundTransferBo"%>
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Pending Fund Transfer</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
                <h3>Pending Fund Transfers</h3>
                <div class="clear"></div>
            </div>
            <%
                FundTransferBo fundTransferBo = new FundTransferBoImpl();
                String user_id = (String) session.getAttribute("user_id");
                String role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");

                List<FundTransferVo> fundTransferList2 = new ArrayList<FundTransferVo>(); //list2
                List<FundTransferVo> fundTransferList1 = new ArrayList<FundTransferVo>();
                
                if (role_id.equalsIgnoreCase("High Level Authorizer")) {
                    fundTransferList2 = fundTransferBo.getPendingFundTransfers(user_id);
                    fundTransferList1 = fundTransferBo.getPendingFundTransfersByAuth2(user_id);
                } else {
                    fundTransferList1 = fundTransferBo.getPendingFundTransfersByAuth1(user_id);
                }
                int awaiting_no = fundTransferList1.size() + fundTransferList2.size();
            %>

            <div  class="tab-content" id="tab2" align="left">
                <div class="content-box-header">
                    <h3><%=awaiting_no%>  transactions is/are pending for authorization.</h3>
                    <div class="clear"></div>
                </div>


                <br/>
                <% out.println(result + "<br/>"); %>
                <%!
                    String init_account;
                    String ben_acc;
                    String amt;
                    String initiator_id;
                    String user_id;
                    String trans_id;
                    String result;
                    String role_id;
                    List<String[]> list2;
                %>
                <% if (awaiting_no != 0) { %>
                <table>    
                    <tr>       
                        <!--<td>Transaction Id</td>-->
                        <td>Initiator</td>
                        <td>Account</td>
                        <td>Beneficiary Account</td>
                        <td>Amount</td>
                        <td></td>
                    </tr>
                    <%
                        for (FundTransferVo fundTransferVo : fundTransferList1) {
                            initiator_id = fundTransferVo.getInitiatorId();
                            init_account = fundTransferVo.getInitAccount();
                            ben_acc = fundTransferVo.getBenAccount();
                            amt = fundTransferVo.getAmount();
                            trans_id = fundTransferVo.getTransactionId();
                    %>
                    <tr> 
                        <!--<td>trans_id</td>-->
                        <td><%=initiator_id%></td>
                        <td><%=init_account%></td>
                        <td><%=ben_acc%></td>
                        <td><%=amt%></td>
                        <td>   
                            <form>   
                                <input type="button" value="process"  class="button" 
                                       onClick="getdetail('get_ft_details', '<%= trans_id%>')"> 
                            </form>
                        </td>
                    </tr>
                    <%}%>  
                    <%
                        if (role_id.equalsIgnoreCase("High Level Authorizer")) {
                            for (FundTransferVo fundTransferVo : fundTransferList2) {
                                initiator_id = fundTransferVo.getInitiatorId();
                                init_account = fundTransferVo.getInitAccount();
                                ben_acc = fundTransferVo.getBenAccount();
                                amt = fundTransferVo.getAmount();
                                trans_id = fundTransferVo.getTransactionId();
                    %>
                    <tr> 
                        <!--<td>trans_id</td>-->
                        <td><%=initiator_id%></td>
                        <td><%=init_account%></td>
                        <td><%=ben_acc%></td>
                        <td><%=amt%></td>
                        <td>   
                            <form>   
                                <input type="button" class="button" value="process" onClick="getdetail('get_ft_details', '<%=trans_id%>')">
                            </form>
                        </td>
                    </tr>
                    <%}%>
                    <%}%>
                    <%}%>
                </table><br/>
            </div>         
    </body>
</html>
