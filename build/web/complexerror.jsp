

<%@page import="com.fasyl.ebanking.main.*"%>

<%@page import="java.util.*" %>



<html>

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trust Bond</title>
        <link rel="shortcut icon" href="screens/images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css">

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <link href="styles.css" rel="stylesheet" type="text/css">
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
        <%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
        <%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
        <%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
        <style>
            .mainBodyBG {

                background-color:#E2ECF3; /*#FFFFFF;*/
                margin-top: 150px;
                margin-left:400px;
                height: 50%;
                width: 50%;

            }
            .errMsgFont { font-family: Tahoma; font-size: 13px; line-height: 25px; color: #333333 }
        </style>
    </head>



    <%
        System.out.println("complexerror.jsp");
        String sMsg = (String) request.getAttribute("msg_text");

        String sModel = (String) request.getAttribute("msg_model_type");
        String actualmsg = (String) request.getAttribute("actual_msg");

        String sOkTarget = "";

        String sYesTarget = "";

        String sNoTarget = "";

        String sFontColor = "";

        if ((sModel.equalsIgnoreCase("error")) || (sModel.equalsIgnoreCase("success")) || (sModel.equalsIgnoreCase("message"))) {

            sOkTarget = (String) request.getAttribute("msg_ok_target");

            if (sModel.equals("success")) {
                sFontColor = "#009933";
            } else {
                sFontColor = "#CC0000";
            }

        } else {

            sYesTarget = (String) request.getAttribute("msg_yes_target");

            sNoTarget = (String) request.getAttribute("msg_no_target");

        }


    %>



    <body >

        <%--<div class="mainBodyBG" align="center">
            <div align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>

    <td height="100">&nbsp;</td>

  </tr>

  <tr>

    <td>

      <table width="450" border="0" cellspacing="0" cellpadding="0" align="center" class="TableH">

        <tr>

          <td height="20" class="formFont">

            <div align="center" class="mainCaption"><font color=<%=sFontColor%> class="errHeader"><%=actualmsg==null?"":actualmsg%>

              : </font></div>

          </td>

        </tr>

        <tr>

          <td class="formFont">

            <div align="center" class="errMsgFont"><%=sMsg%></div>

          </td>

        </tr>

                <%

                            if( (sModel.equals("error")) || (sModel.equals("success")) || (sModel.equals("message")) )

                                {

                %>

        <tr>

          <td height="25" class="formFont">

            <div align="center" class="normal2"> <b class="errMsgFont"><a href="<%=sOkTarget%>" class="errMsgFont">OK</a></b></div>

          </td>

        </tr>

                <%

                                }

                                else

                                {

                %>

                <tr>

          <td height="25" class="formFont">

            <div align="center" class="normal2"> <b class="errMsgFont"><a href="<%=sYesTarget%>" class="errMsgFont">Yes</a></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="errMsgFont"><a href="<%=sNoTarget%>" class="errMsgFont">No</a></b></div>

          </td>

        </tr>

                <%

                                }

                %>

      </table>

    </td>

  </tr>

</table>
                </div>
                </div> --%>

        <div class="hm_global">
            <div class="wrapper">
                <div class="logoutwrap" style="height:450px;">
                    <div class="logcontent">
                        <div class="loglogo">
                            <img src="screens/images/logo.jpg"  alt="Trust Bond">
                            <div class="clear"></div>
                        </div>
                        <div class="thanks" style="text-align:center;">
                            <p><%=sMsg%></p>
                            <%

                                if ((sModel.equals("error")) || (sModel.equals("success")) || (sModel.equals("message"))) {

                            %>
                            <p><a href="<%=sOkTarget%>">OK >></a></p>
                            <%                     } else {

                            %>
                            <p><a href="<%=sYesTarget%>">Yes</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="<%=sNoTarget%>">No</a></p>
                            <%                    }

                            %>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="enjoy">
                        <h3>Enjoy our instant account transfer </h3>
                        <h4>The fast way to transfer money to other bank accounts </h4>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="social">
                    <span>
                        <a href="#"><img src="screens/images/mail.jpg" alt="mail"> </a>
                    </span>
                    <span>
                        <a href="#"><img src="screens/images/fb.jpg" alt="Facebook"> </a>
                    </span>
                    <span>
                        <a href="#"><img src="screens/images/tw.jpg" alt="Twitter"> </a>
                    </span>
                    <span>
                        <a href="#"><img src="screens/images/gplus.jpg" alt="Google+"></a> 
                    </span>
                    <span>
                        <a href="#"><img src="screens/images/ig.jpg" alt="LinkedIn"> </a>
                    </span>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="footer">
                <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
                <p>2015 � TrustBond Mortgage Bank Plc.</p>
            </div>
            <div class="clear"></div>
        </div>
        <script src="screens/js/jquery.min.js"></script>

    </body>

</html>

