/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.db;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

 

public class DataBase {
    static Context initContext;
    private static DataSource datasource;
    private static DataSource datasource2;
    protected static   Connection con=null;
    protected static   Connection con2=null;
    static{
        try{

            datasource = (DataSource)new InitialContext().lookup("jdbc/OraclePool");
             con=datasource.getConnection();
            //con= com.fasyl.ebanking.db.ConnectionClass.getConn(null);
            System.out.println(con);
       }catch(Exception e){
           e.printStackTrace();
       }
    }

    static{
        try{

            datasource2 = (DataSource)new InitialContext().lookup("jdbc/OraclePoolFCC");
             con2=datasource2.getConnection();
            //con2= com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:fcchost/fcchost@10.100.60.30:1521:fcclive");
            System.out.println(con);
       }catch(Exception e){
           e.printStackTrace();
       }
    }
       public static Connection getConnection(){
        try {
            if (con==null || con.isClosed()) {
                System.out.println();
                //con= com.fasyl.ebanking.db.ConnectionClass.getConn(null);
                try {

                    con = datasource.getConnection();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            } else {
                System.out.println("returning con");
                return con;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
         return con;
        }
   public static Connection getConnectionToFCC(){
        try {
            if (con2==null || con2.isClosed()) {
                try {
                    System.out.println("inside etconnectionfcc");
                    con2 = datasource2.getConnection();
                    System.out.println("about to leave etconnectionfcc");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                    //con2= com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:fcchost/fcchost@10.100.60.30:1521:fcclive");
            } else {
                System.out.println(" I am returning con2");
                return con2;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
         return con2;
        }


}
