package test.util;

import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class TestConnection {

    final static Logger logger = Logger.getLogger(TestConnection.class);
    static BasicDataSource bdsTBMB = null;

    public static void main(String args[]) {
        try {
            bdsTBMB = (BasicDataSource) ConnectionManager.getInstance().getTBMBDatasource();
            System.out.println("connection was successfull!!!" + bdsTBMB.getConnection().toString());
            if (bdsTBMB.getConnection() != null) {
                System.out.println("connection was successfull!!!" + bdsTBMB.getConnection().toString());
                logger.info("connection was successfull!!!");
            } else {
                System.err.println("connection was not successfull!!!");
                logger.debug("connection was not successfull!!!");
            }
            System.out.println("connection was successfull!!!" + bdsTBMB.getConnection().toString());
        } catch (IOException ex) {
            System.err.println("error:" + ex.getMessage());
            logger.error("IOException error happened here: " + ex.getMessage());
        } catch (PropertyVetoException ex) {
            System.err.println("error:" + ex.getMessage());
            logger.error("PropertyVetoException error happened here: " + ex.getMessage());
        } catch (SQLException ex) {
            System.err.println("error:" + ex.getMessage());
            logger.error("SQLException error happened here: " + ex.getMessage());
        }
    }
}
