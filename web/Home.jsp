<%@page import="com.fasyl.corpIntBankingadmin.bo.FundTransferBo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%

    response.setDateHeader("Expires",
            System.currentTimeMillis() + 0);

    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");

    // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");

    // Set standard HTTP/1.0 no-cache header.
    response.setHeader("Pragma", "no-cache");

%>
<html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="HandheldFriendly" content="True">
                <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>TrustBond Mortgage Bank Plc :: Internet Banking</title>
                    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
                    <!--[if lt IE 9]>
                                            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
                    <![endif]-->

                    <!-- STYLE SHEET -->
                    <link rel="stylesheet" type="text/css" href="screens/css/style.css" />
                    <link rel="stylesheet" type="text/css" href="screens/css/accordioin.css" />

                    <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
                    <script language="JavaScript" src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
                    <script language=JavaScript src="screens/scripts/resources/scripts/jquery.form.js" type="text/javascript"></script>
                    <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" />
                    <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
                    <script language=JavaScript src="screens/scripts/resources/scripts/jquery.pagination.js" type="text/javascript"></script>
                    <script language=JavaScript src="screens/scripts/session.js" type="text/javascript"></script>
                    <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
                    <script language=JavaScript src="screens/scripts/transaction.js" type="text/javascript"></script>

                    <link href="screens/scripts/SyntaxHighlighter.css" rel="stylesheet" type="text/css" />
                    <!--                       CSS                       -->

                    <!-- Reset Stylesheet -->

                    <link rel="stylesheet" href="screens/scripts/resources/css/pagination.css" type="text/css" media="screen" />


                    <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />
                    <!-- Main Stylesheet -->
                    <link rel="stylesheet" href="screens/scripts/resources/css/style.css" type="text/css" media="screen" />

                    <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
                    <link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />

                    <!-- Colour Schemes
            
                    Default colour scheme is green. Uncomment prefered stylesheet to use it.
            
                    <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
            
                    <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />
            
                    -->

                    <!-- Internet Explorer Fixes Stylesheet -->

                    <!--[if lte IE 7]>
                            <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
                    <![endif]-->

                    <!--                       Javascripts                       -->

                    <!-- jQuery -->
                    <script type="text/javascript" src="screens/scripts/resources/scripts/jquery-1.3.2.min.js"></script>

                    <!-- jQuery Configuration -->
                    <script type="text/javascript" src="screens/scripts/resources/scripts/simpla.jquery.configuration.js"></script>

                    <!-- Facebox jQuery Plugin -->
                    <script type="text/javascript" src="screens/scripts/resources/scripts/facebox.js"></script>

                    <!-- jQuery WYSIWYG Plugin -->
                    <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.wysiwyg.js"></script>

                    <!-- jQuery Datepicker Plugin -->
                    <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.datePicker.js"></script>
                    <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.date.js"></script>
                    <script type="text/javascript" src="screens/scripts/resources/scripts/datepickercontrol.js"></script>
                    <link type="text/css" rel="stylesheet" href="screens/scripts/resources/scripts/datepickercontrol.css"/>
                    <!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->


                    <!-- Internet Explorer .png-fix -->

                    <!--[if IE 6]>
                            <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                            <script type="text/javascript">
                                    DD_belatedPNG.fix('.png_bg, img, li');
                            </script>
                    <![endif]-->
                    <style type="text/css">
                        #main {
                            width:992px;
                            margin:0 auto;
                            background-image: url(screens/scripts/resources/images/bg-middle.gif);
                        }
                    </style>
                    </head>
                    <%  
                        String user_id = (String) session.getAttribute("user_id");
                        String role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();
                        FundTransferBo fundTransferBo = new FundTransferBoImpl();
                        String size = "0";
                        size = String.valueOf(fundTransferBo.getPendingFundTransferCount(user_id));
                    %>
                    <body onload="toggles();" onunload="onCloses();"><!-- Wrapper for the radial gradient background -->

                        <script type="text/javascript" src="js/ddaccordion.js"></script>
                        <script type="text/javascript" src="js/accordion2.js"></script>
                        <div class="hm_global">
                            <div class="wrapper">
                                <c:import url="/Header.jsp"/>
                                <div class="maincontainer">
                                    <div class="sidebar">
                                        <c:import url="/menu.jsp"/>
                                        <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                                        </div> <!-- End #messages -->
                                    </div>
                                    <div class="maincontent" id="main-content">

                                        <noscript> <!-- Show a notification if the user has disabled javascript -->
                                            <div class="notification error png_bg">
                                                <div>
                                                    Javascript is disabled or is not supported by your browser. Please <a href="" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                                                    Download From <a href="http://www.exet.tk">exet.tk</a></div>
                                            </div>
                                        </noscript>
                                        <div id="scrloader">

                                              <%if (role_id.equalsIgnoreCase("LOW LEVEL AUTHORIZER") || role_id.equalsIgnoreCase("HIGH LEVEL AUTHORIZER")) {%>
                                            <c:import url="/pending_trans.jsp"/>

                                            <%} else if (role_id.equalsIgnoreCase("client_admin")) {%> 

                                            <c:import url="/passwd_change_request_list.jsp"/>

                                            <%} else if (role_id.equalsIgnoreCase("Initiator")) {%> 
                                            <c:import url="/default.jsp"/>
                                            <% } %>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                                <div class="adbanner"><a href="#"><img src="screens/images/adbanner03.jpg" alt="Let's take care of your banking needs"></a></div>
                            </div>
                            <div class="footer">
                                <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
                                <p>2015 © TrustBond Mortgage Bank Plc.</p>
                                <p> Powered by <a href="http://fasylgroup.com" target="blank">FASYL Technology Group</a></p>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </body>


                    <!-- Download From www.exet.tk-->
                    </html>
