<%-- 
    Document   : menu
    Created on : 26-Nov-2009, 15:47:52
    Author     : 
--%>

<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu</title>
        <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="screens/scripts/resources/css/style.css" type="text/css" media="screen" />
        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />
        <style media="all" type="text/css">@import "css/cssverticalmenu.css";</style>
        <script type="text/javascript" src="screens/scripts/verticalMenu.js"></script>
        <script type="text/javascript" src="screens/js/jquery-1.3.2.min.js"></script>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
    </head>
    <body>
        <div>
            <ul id="main-nav">
                <%if (((UserVo) session.getAttribute("userVo")).getRoleId().equalsIgnoreCase("client_admin")) {%>
                <li>
                    <a href="#" class="nav-top-item">Client Admin Operations</a>
                    <ul>
                        <li>
                            <a href="#" onclick="showManageAcc('accounts_list')">
                                Manage Accounts
                            </a>
                        </li>
<!--                        <li>
                            <a  href="#" onclick="showAuthPassword('authorize_passwd_change')">
                                Authorize Password Change
                            </a>
                        </li>
                        <li>
                            <a  href="#" onclick="showAuthUserAccess('edit_user')">
                                Edit User
                            </a>
                        </li>-->
                    </ul>
                </li>
                <% } %>
                <%-- Account Info --%>
                <%if (((UserVo) session.getAttribute("userVo")).getRoleId().equalsIgnoreCase("INITIATOR")) {%>
                <li>
                    <a href="#" class="nav-top-item">Account Info</a>
                    <ul>
                        <li>
                            <a href="#" onClick="showAuth(201)">Account Position</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(202)">Account Summary</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(203)">Mini Statement</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(204)">Account History</a>
                        </li>
                    </ul>
                </li>
                
                <%-- Services --%>
                <li>
                    <a href="#" class="nav-top-item">Services</a>
                    <ul>
                        <li>
                            <a href="#" class="nav-2nd-item a.secondlevel">Cheque Book</a>
                            <ul>
                                <li>
                                    <a href="#" onClick="showAuth(321)">Status Enquiry</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(322)">Cheque Request</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(323)">Stop Cheque</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(324)">Confirm Cheque </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="nav-2nd-item a.secondlevel">Funds Transfer</a>
                            <ul>
                                <li>
                                    <a href="#" onClick="showAuth(333)">Local Transfer</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(332)">Intra Bank Transfer</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(7015)">Inter Bank Transfer</a>
                                </li>
                                <li>
                                    <a href="#" onClick="showAuth(7021)">Bulk Transfer</a>
                                </li>
                                <li>
                                    <a  href="#" onclick="showDeclinedFt('declined_transfer')">
                                        Declined Transactions  
                                    </a>                       
                                </li>
                            </ul>
                        </li>
                                <li>
                                    <a href="#" class="nav-2nd-item a.secondlevel">Standing Instructions</a>
                                    <ul>
                                        <li>
                                            <a href="#" onClick="showAuth(341)">SI Enquiry</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="showAuth(342)">SI Setup</a>
                                        </li>
                                        <li>
                                            <a href="#" onClick="showAuth(344)">Close SI</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    
                </li>
                
                <%-- Manage Intra Bank Beneficiary --%>
                <li>
                    <a href="#" class="nav-top-item">Manage Intra Bank Beneficiary</a>
                    <ul>
                        <li>
                            <a href="#" onClick="showAuth(601)">Add Beneficiary</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(602)">Modify Beneficiary</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(603)">Delete Beneficiary</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(604)">View Beneficiary</a>
                        </li>
                    </ul>
                </li>

                <%-- Manage Inter Bank Beneficiary --%>
                <li>
                    <a href="#" class="nav-top-item">Manage Inter Bank Beneficiary</a>
                    <ul>
                        <li>
                            <a href="#" onClick="showAuthBen('addBen')">Add Beneficiary</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuthBen('viewBen')">View Beneficiary</a>
                        </li>
                    </ul>
                </li>
                <% } %>

                <%if (((UserVo) session.getAttribute("userVo")).getRoleId().equalsIgnoreCase("Low Level Authorizer")) {%>
                <li>
                    <a href="#" class="nav-top-item">Low Level Authorizer Operations</a>
                    <ul>
                        <li>
                            <a href="#" onclick="showAuthPendingTransfer('pending_trans')">
                                Authorize Fund Transfers
                            </a>
                        </li>
                    </ul>
                </li>
                <% } %>
                <%-- High Level Authorizer Operations --%>
                <%if (((UserVo) session.getAttribute("userVo")).getRoleId().equalsIgnoreCase("High Level Authorizer")) {%>
                <li>
                    <a href="#" class="nav-top-item">High Level Authorizer Operations</a>
                    <ul>
                        <li>
                            <a href="#" onclick="showAuthPendingTransfer('pending_trans')">
                                Authorize Fund Transfers
                            </a>
                        </li>
                    </ul>
                </li>
                <% }%>
                <%-- Password Manager --%>
                <li>
                    <a href="#" class="nav-top-item">Password Manager</a>
                    <ul>
                        <li>
                            <a href="#" onclick="showAuthPass('change_password')">
                                Change Password
                            </a>
                        </li>
                    </ul>
                </li>

                <%-- Report --%>
                <li>
                    <a href="#" class="nav-top-item">Report</a>
                    <ul>
                        <li>
                            <a href="#" onClick="showAuth(5001)">Funds Transfer Report</a>
                        </li>
                        <li>
                            <a href="#" onClick="showAuth(5008)">System Activity Report</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </body>
</html>
