

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"  
[<!ATTLIST TAG ccys CDATA #IMPLIED]>
<html>
    <head>
       <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>


        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
            
                <h3><bean:message key="label.fundtransfertittle"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <c:set var="xml" value="${data}" />


                                  <x:parse varDom="doc" xml="${xml}" />
                   
						<div class="notification attention png_bg">
							<a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<x:forEach var="row" select="$doc/ROWSET/CCY_SETUP">
                                                        <div>
								The maximum amount withdrwable for <x:out select="CCY_TYPE" /> is <x:out select="MAX_AMOUNT" />
							</div>
                                                        </x:forEach>
						</div>
                    <p>
                        <label><bean:message key="label.sidebitaccount"/><b><font color="red">*</font></b></label>
                        <select  id ="ftdebitacc" class="small-input" onchange="updateCCY(this.value,'ccy','branch','availbal','maxamt');;">
                                <option value=""></option>
                             
                                <x:forEach var="row" select="$doc/ROWSET/ROW">
                                
                               <option data-maxamt="<x:out select="MAX_AMOUNT"/>" data-accdesc data-availbal="<x:out select="BALANCE"/>" data-branch="<x:out select="BRANCH_CODE"/>" data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                                </x:forEach>
                            </select>
                    </p>
                    <p>
                        <label><bean:message key="label.choosebeneficiary"/></label>
                        <select name="benacct" id ="benacct" onchange="updateDest(this.value);" class="small-input">
                    <option value=""></option>
                    <c:set var="xml" value="${data}" />
                    <x:parse varDom="docs" xml="${xml}" />
                    <x:forEach var="ben" select="$docs/ROWSET/BENEFICIARY" >
                        <option data-ccy2="<x:out select="CCY" />" value="<x:out select="DESTINATION_ACCT" />"><x:out select="DESTINATION_ACCT" />--<x:out select="BENEFICIARY_NAME" /></option>
                    </x:forEach>
                </select></p>
                    <input type="hidden" value="" id="ccy2"/>
                    <p>
                        <label><bean:message key="label.sicreditaccount"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo" maxLength="20"  value="" onblur="getAccountDetail('001','332');" />

                    </p>
                    <div  id="crbranchcode"></div>
                    <p>
                        <label><bean:message key="label.ftcurrency"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text"  id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>"  />
                               <input type="hidden" id="branch" name="branch" value=""/>
                        <input type="hidden" id="availbal" name="availbal" value=""/>
                        <input type="hidden" id="maxamt" name="maxamt" value=""/>
                    </p>
                        <p>
                        <label><bean:message key="label.ftamount"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="amt"  id="amt" name="ccy" value=""  /><span id="balance"></span>

                    </p>
                    <p><input id="testButton" name="testButton" type="button" onclick="getPin('70000','1');"  value="<bean:message key="label.getpin"/>" class="button">
            </p>
            <div id="trx_detail"></div>
                   <p>
                        <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="enterpin" id="enterpin" maxLength="20"  value="" />
                    
                    </p>
                    <p>
                           <input id="testButton" name="testButton" type="button" onclick="otherfundtransferft(3321,'332');"  value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>

                    </p>


         




            </form>
                    <div id="trx_details">

                            </div>
                            </div>


    </body>
</html>
