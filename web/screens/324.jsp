
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
     <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>


        <div  class="tab-content" id="tab2" align="left">

             <input type="hidden" id="leave" value=""/>
            <div class="content-box-header">

                <h3><bean:message key="label.confirmcheque"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">

                <p>
                    <label><bean:message key="label.AccountCode"/></label>
                    <select id="acct_id"  name="acct_id" class="text-input small-input">
                        <option value="" selected style="display:none">Select an account</option>
                        <c:set var="xml" value="${data}" />
                        <x:parse varDom="doc" xml="${xml}" />
                        <x:forEach var="row" select="$doc/ROWSET/ROW" >
                            <option value="<x:out select="$row/ACCOUNT" />"><x:out select="$row/ACCOUNT" /></option>
                        </x:forEach>
                    </select>
                        <input type="hidden" id="checkno2" class="text-input small-input" name="checkno2"   size="30" value=""/>
                </p>

                <p>
                    <label><bean:message key="label.enterCheckBKNo"/></label>
                  <input type="text" id="checkno" class="text-input small-input" name="checkno"   size="30" value="">
                </p>
                 <p>
                    <label><bean:message key="label.enterChecAmount"/></label>
                  <input type="text" id="amt"class="text-input small-input" name="amt"   size="30" value="">
                </p>
                                 <p>
                    <label>Beneficiary Name </label>
                  <input type="text" id="benname"class="text-input small-input" name="benname"   size="30" value="">
                </p>
                <p>
                    <input type="button"  class="button" value="Submit" onClick="confirmCheque(3241,'324')" >
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>







            </form>
            <!--<Div id="trx_detail"></Div>-->
        </div>