

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
     <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>
        <div class="content-box">


            <div class="content-box-header">

                <h3><bean:message key="label.bulktransfer"/></h3>
                <%--<ul class="content-box-tabs" >
                    <!--<li><a class="default-tab"  href="#trx_detail" onclick="return bulkTransfer(70002,'7021');"><bean:message key="label.bulkTransfer"/></a></li>
                    <!--<li><a  href="#trx_detail" onclick="return bentab(60120,'601');"><bean:message key="label.addbeneficiary"/></a></li>
                    <li><a  href="#trx_detail" onclick="return bentab(604,'1');"><bean:message key="label.viewbeneficiary"/></a></li>
                    <li><a  href="#trx_detail" onclick="return bentab(602,'1');"><bean:message key="label.ModifyBeneficiary"/></a></li>-->
                </ul>--%>
                <div class="clear"></div>
            </div>
            <div class="content-box-content"style="padding: 0;">

                <form name="TxnFrmz" style="display: none;">
                    <input type="hidden" value="GRP" name="selectbeneficiary">
                </form>

                <div id="trx_detail" class="tab-content default-tab" style="display: none;border: none;padding: 0; margin: 0px;">
                    <form Name="TxnFrms" id="TxnFrms" action="/MainController" method="Post">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <c:set var="xml" value="${data}" />


                        <x:parse varDom="doc" xml="${xml}" />

                        <div class="notification attention png_bg">
                            <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                                <x:forEach var="row" select="$doc/ROWSET/CCY_SETUP">
                                <div>
                                    The maximum amount withdrawable for <x:out select="CCY_TYPE" /> is <x:out select="MAX_AMOUNT" />
                                </div>
                            </x:forEach>
                        </div>

                        <p>
                            <label><bean:message key="label.sidebitaccount"/><b><font color="red">*</font></b></label>
                            <select name="ftdebitacc" id="ftdebitacc" class="text-input small-input" onchange="updateCCY(this.value,'ccy','branch','availbal','maxamt')" >
                                <option value="" selected style="display:none">Select an account</option>
                                <x:forEach var="row" select="$doc/ROWSET/ROW">
                                    <option data-maxamt="<x:out select="MAX_AMOUNT"/>" data-availbal="<x:out select="BALANCE"/>" data-branch="<x:out select="BRANCH_CODE"/>" data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                                </x:forEach>

                            </select>

                        </p>

                        <p>
                            <label><bean:message key="label.selectbengrp"/><b><font color="red">*</font></b></label>
                            <select name="benacct" id ="benacct" onchange="getTotalamount(this.value)" class="small-input">
                                <option value="" selected style="display:none">Select beneficiary group</option>

                                <c:set var="xml" value="${data}" />


                                <x:parse varDom="docs" xml="${xml}" />
                                <x:forEach var="ben" select="$docs/ROWSET/BENEFICIARY" >
                                    <option value="<x:out select="BENEFTGRPNAME" />"><x:out select="BENEFTGRPNAME" /></option>
                                </x:forEach>
                            </select></p>
                        <!-- <p>
                             <label><bean:message key="label.sicreditaccount"/><b><font color="red">*</font></b></label>
                             <input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo" maxLength="20"  value="" onblur="getAccountDetail('001','332');"/>
     
                         </p>-->

                        <label><bean:message key="label.ftcurrency"/><b><font color="red">*</font></b></label>
                        <input disabled class="text-input small-input" type="text"  id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>"  />

                        </p>
                        <div  id="ajaxload" style="display: none"></div>
                        <p>

                        <p>
                            <label>Total <bean:message key="label.ftamount"/><b><font color="red">*</font></b></label>
                            <input class="text-input small-input" disabled="disabled" type="amt"  id="amt" name="amt" value="" /> <span id="balance"></span>
                            <input type="hidden" id="branch" name="branch" value=""/>
                            <input type="hidden" id="availbal" name="availbal" value=""/>
                            <input type="hidden" id="maxamt" name="maxamt" value=""/>

                        </p>


                        <p>
                            <label><bean:message key="label.narration"/><b><font color="red">*</font></b></label>
                            <input class="text-input small-input" maxlength="150" type="narration"  id="narration" name="narration" value=""  /><span id="narration"></span>
                        </p>

                        <p>

                                   <input id="testButton" name="testButton" type="button" onclick="temporaryDisable(this);
                        getPin('70000', '1');"  value="<bean:message key="label.getpin"/>" class="button"> 
                        </p>
                        <div id="trx_details">
                            <br /><br />
                        </div>
                        <!--<div id="trx_detail"></div>-->
                        <p>
                            <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                            <input class="text-input small-input" type="text" name ="enterpin" id="enterpin" maxLength="20"  value="" />

                        </p>
                        <p>
                            <input id="testButton" name="testButton" type="button" onclick="fundtransfer('70211','7021');"  value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                        </p>



                        <div id="trx_details">

                        </div>



                    </form>
                </div>

            </div>

        </div> 


    </body>
</html>
