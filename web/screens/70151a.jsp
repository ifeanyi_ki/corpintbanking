
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="application/pdf" %>       
<%@
page import="
     java.io.*,
     java.util.*,
     java.text.*,
     com.itextpdf.text.pdf.*,
     com.itextpdf.text.*,com.fasyl.ebanking.util.*,org.w3c.dom.*,org.xml.sax.*,com.fasyl.ebanking.logic.CustomerTrx"
     %>
<% response.setHeader("Content-Disposition", "attachment; filename=\"IBFTReceipt.pdf\"");%> 
<%
    //page creted by K.I to handle IB ft receipt generation
    response.setContentType("application/pdf");
    com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4); //new Document();

    System.out.println("=============== inside 70151a ==================");
    String custAcc = request.getParameter("custacc");
    String benAcc = request.getParameter("benacc");
    String benBank = request.getParameter("benbank");
    String benAccName = request.getParameter("benaccname");
    //double amt = Double.parseDouble(request.getParameter("amount"));
    String remarks = request.getParameter("remarks");

    String formattedNumber="";
    Double amount;
    
    CustomerTrx customerTrx = new CustomerTrx();

    String amt = request.getParameter("amount");
        if (!"".equalsIgnoreCase(amt)) {           
            if (amt == null) {
                amt = "";
            } else {
                 amount = Double.parseDouble(amt);
                 formattedNumber = String.format("%,.2f", amount);
                 System.out.println("formatted number: " + formattedNumber);
            }

        }
    
    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    ServletContext context = getServletContext();

    String custAccName = "";

    if (custAcc != null || "".equalsIgnoreCase(custAcc)) {
        custAccName = customerTrx.getAccName(custAcc);
    }

    try {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        PdfPTable table = new PdfPTable(2);
        Paragraph coyNoParagraph;
        Paragraph coyStringParagraph;
        Paragraph headerParagraph;
        Paragraph dateParagraph;
        Paragraph lineParagraph;
        Paragraph disclaimerParagraph;
        Paragraph disclaimerHeaderParagraph;
        PdfWriter writer;
        PdfContentByte canvas;
        Rectangle rectangle;
        Font headerFont;
        Font coyStringFont;
        Font receiptContentFont;
        Font disclaimerFont;
        Font disclaimerHedearFont;
        Font dateFont;
        String headerString = "TRUSTBOND ONLINE TRANSFER ADVICE";
//        String coyString = "TrustBond Mortgage Bank plc";
//        String coyRCNo = "RC: 196748";
        String disclaimerText = "Your transfer has been successful and the beneficiary's account will be credited. "
                + "However, this does not serve as confirmation of credit into the beneficiary's account. "
                + "Due to the nature of the internet, transctions may be subject to interruption, transmission blockout, "
                + "delayed transmission and incorrect data transmission. The bank is not liable for malfunction in "
                + "communications fascilities not within its control that may affect the accuracy or timeliness of messages "
                + "and transactions you send. All transactions are subject to verifiction and our normal fraud checks.";


        NumberFormat formatter = new DecimalFormat("0.00");


        String path = context.getRealPath("screens/scripts/resources/images/logo.png");
        
        writer = PdfWriter.getInstance(document, buffer); //for web
        document.open();

        System.out.println("This is the contextpath : " + document.right(document.rightMargin()) + " y coord " + document.top());
        Image image = Image.getInstance(path);

        String date = sdf.format(new Date());

        com.itextpdf.text.pdf.draw.LineSeparator underline = new com.itextpdf.text.pdf.draw.LineSeparator(1, 100, null, com.itextpdf.text.Element.ALIGN_CENTER, -2);

        headerParagraph = new Paragraph();
        coyStringParagraph = new Paragraph();
        coyNoParagraph = new Paragraph();
        dateParagraph = new Paragraph();
        lineParagraph = new Paragraph();

        //align header and coy name accordingly
        headerParagraph.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
        headerParagraph.setSpacingBefore(200f); //new
        headerParagraph.setIndentationRight(70f); //new
        coyStringParagraph.setAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
        coyNoParagraph.setAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);

        //design font
        headerFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
        coyStringFont = new Font(Font.FontFamily.COURIER, 8);
        receiptContentFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
        disclaimerFont = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
        disclaimerHedearFont = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD | Font.UNDERLINE);
        dateFont = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL | Font.UNDERLINE);
        //add logo and scale accordingly
        //image = Image.getInstance("logo.jpg"); //for non version
        image.scaleAbsolute(140f, 50f); //102, 50
        image.setAbsolutePosition(420f, 780f);

        //build the header
        headerParagraph.add(new Chunk(headerString, headerFont));
//        coyStringParagraph.add(new Chunk(coyString, coyStringFont));
//        coyNoParagraph.add(new Chunk(coyRCNo, coyStringFont));
        document.add(headerParagraph);
        document.add(image);

        //display coy name below the logo
        document.add(Chunk.NEWLINE);
//        document.add(coyStringParagraph);
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
//        document.add(coyNoParagraph);
//        document.add(Chunk.NEWLINE);

        //draw a rectangle on the document
        canvas = writer.getDirectContent();
        rectangle = new Rectangle(36, 360, 559, 740); //increase the 2nd cordinate to pull the rectangle up
        rectangle.setBorder(Rectangle.BOX);
        rectangle.setBorderWidth(1);
        canvas.rectangle(rectangle);

        //display timestamp
        dateParagraph.setIndentationRight(50f);  //indentation not working yet
        dateParagraph.add(new Chunk("Transaction Date:" + date, dateFont));
        document.add(dateParagraph);

        PdfPCell cell0 = new PdfPCell(new Paragraph(new Chunk("Transaction Type: TrustBond-Inter Bank Fund Transfer", receiptContentFont)));
        PdfPCell celldummy = new PdfPCell(new Paragraph(new Chunk("")));
        PdfPCell cell1 = new PdfPCell(new Paragraph(new Chunk("Sender: " + custAccName, receiptContentFont)));
        PdfPCell cell2 = new PdfPCell(new Paragraph(new Chunk("Receiver: " + benAccName, receiptContentFont)));
        PdfPCell cell3 = new PdfPCell(new Paragraph(new Chunk("Account Number: " + custAcc, receiptContentFont)));
        PdfPCell cell4 = new PdfPCell(new Paragraph(new Chunk("Account Number: " + benAcc, receiptContentFont)));
        PdfPCell cell5 = new PdfPCell(new Paragraph(new Chunk("Transaction Amount: NGN"+ formattedNumber , receiptContentFont)));
        PdfPCell cell6 = new PdfPCell(new Paragraph(new Chunk("Receiving Bank: " + benBank, receiptContentFont)));
        PdfPCell cell7 = new PdfPCell(new Paragraph(new Chunk("Remarks: " + remarks, receiptContentFont)));

        celldummy.setBorder(Rectangle.NO_BORDER);
        cell0.setBorder(Rectangle.NO_BORDER);
        celldummy.setPadding(15f);
        cell0.setPadding(15f);
        //cell0.setNoWrap(true);
        cell1.setBorder(Rectangle.NO_BORDER);
        cell2.setBorder(Rectangle.NO_BORDER);
        cell1.setPadding(15f);
        cell2.setPadding(15f);
        cell3.setBorder(Rectangle.NO_BORDER);
        cell4.setBorder(Rectangle.NO_BORDER);
        cell3.setPadding(15f);
        cell4.setPadding(15f);
        cell5.setBorder(Rectangle.NO_BORDER);
        cell6.setBorder(Rectangle.NO_BORDER);
        cell5.setPadding(15f);
        cell6.setPadding(15f);
        cell7.setBorder(Rectangle.NO_BORDER);
        cell7.setPadding(15f);
        cell7.setColspan(2);

        table.addCell(cell0);
        table.addCell(celldummy);
        table.addCell(cell1);
        table.addCell(cell2);
        table.addCell(cell3);
        table.addCell(cell4);
        table.addCell(cell5);
        table.addCell(cell6);
        table.addCell(cell7);

        document.add(Chunk.NEWLINE);

        document.add(table);

        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);

        lineParagraph.add(underline);

        document.add(lineParagraph);

        document.add(Chunk.NEWLINE);

        //add disclaimer
        disclaimerHeaderParagraph = new Paragraph(new Chunk("Disclaimer", disclaimerHedearFont));
        disclaimerParagraph = new Paragraph(new Chunk(disclaimerText, disclaimerFont));
        disclaimerParagraph.setLeading(10f);

        document.add(disclaimerHeaderParagraph);
        document.add(disclaimerParagraph);

        document.close();
        addWaterColor(response, buffer, context); //add water color effect to the tempFile.pdf
    } catch (DocumentException e) {
        e.printStackTrace();
    }

%>

<%!
    public void addWaterColor(HttpServletResponse response, ByteArrayOutputStream buffer, ServletContext context) {
//        String tempPdfPath = context.getRealPath("screens/scripts/resources/temp" + txnCode.toString() + ".pdf");
//        String PdfFilePath = context.getRealPath("screens/scripts/resources/TFTReceipt.pdf");
        try {
            PdfReader Read_PDF_To_Watermark = new PdfReader(buffer.toByteArray()); //read the temp pdf to apply water color effect on

            int number_of_pages = Read_PDF_To_Watermark.getNumberOfPages();
            //PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, new FileOutputStream(PdfFilePath));
            PdfStamper stamp = new PdfStamper(Read_PDF_To_Watermark, buffer);

            int i = 0;
//            context = getServletContext();
          String path = context.getRealPath("screens/scripts/resources/images/online transfer1.gif");

            Image watermark_image = Image.getInstance(path);
//            Image watermark_image1 = Image.getInstance(path);
//            Image watermark_image2 = Image.getInstance(path);
//            Image watermark_image3 = Image.getInstance(path);

//            watermark_image1.setAbsolutePosition(100, 650);
            watermark_image.setAbsolutePosition(50, 400);
//            watermark_image2.setAbsolutePosition(350, 650);
//            watermark_image3.setAbsolutePosition(350, 500);
            PdfContentByte add_watermark;
            while (i < number_of_pages) {
                i++;
                add_watermark = stamp.getUnderContent(i);
                add_watermark.addImage(watermark_image);
//                add_watermark.addImage(watermark_image1);
//                add_watermark.addImage(watermark_image2);
//                add_watermark.addImage(watermark_image3);
            }


            stamp.close();

            DataOutput dataOutput = new DataOutputStream(response.getOutputStream());

            byte[] bytes = buffer.toByteArray();
            response.setContentLength(bytes.length);

            for (int j = 0; j < bytes.length; j++) {
                dataOutput.writeByte(bytes[j]);
            }

            response.flushBuffer();

            //response.getOutputStream().flush();

        } catch (Exception i1) {

            i1.printStackTrace();
        }
    }

%>