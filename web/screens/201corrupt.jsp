

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<body width="630" height="360" >

    <div  class="tab-content" id="tab2" >
        <div class="content-box-header">

            <h3><bean:message key="label.AccountPosition"/></h3>

            <div class="clear"></div>

        </div>
        <form Name="TxnFrm">
            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <!-- End .content-box-header -->
            <p>
                <input type="hidden" id="screenId" value="201"/>
                <input type="hidden" id="taskId" value="4600"/>
                <label><bean:message key="label.AccountCode"/></label>
                <select name="acct_no" id ="acct_no" onchange="showDiv(4600)" class="small-input">
                    <option value=""></option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var=""  select=""  >
                        <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                    </x:forEach>
                </select>
            </p>

            <input type="hidden" name="check" id="check" value="S"/>


        </form>
        <Div id="trx_detail"></Div>
    </div>

</body>


