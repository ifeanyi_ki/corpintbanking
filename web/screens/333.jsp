

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
      <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
                <c:set var="xml" value="${data}" />


                <x:parse varDom="doc" xml="${xml}" />
                <h3>Local <bean:message key="label.fundtransfertittle"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">
                <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                        <x:forEach var="row" select="$doc/ROWSET/CCY_SETUP">
                        <div>
                            The maximum amount withdrawable for <x:out select="CCY_TYPE" /> is <x:out select="MAX_AMOUNT" />
                        </div>
                    </x:forEach>
                </div>

                <p>
                    <label><bean:message key="label.sidebitaccount"/><b><font color="red">*</font></b></label>
                    <select  id ="ftdebitacc" class="small-input" onchange="updateCCY(this.value,'ccy','branch','availbal','maxamt');">
                        <option value="" selected style="display:none">Select an account</option>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <option data-maxamt="<x:out select="MAX_AMOUNT"/>" data-availbal="<x:out select="BALANCE"/>" data-branch="<x:out select="BRANCH_CODE"/>" data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                        </x:forEach>
                    </select>

                </p>

                <p>
                    <label><bean:message key="label.sicreditaccount"/><b><font color="red">*</font></b></label>
                    <!--<input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo" maxLength="20"  value="" onchange="getAccountDetail('001','332');"/>-->
                    <select  id ="DestAcctNo" class="small-input" onchange="updateccy2(this.value,'ccy2')">
                        <option value="" selected style="display:none">Select beneficiary account</option>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <option data-ccys="<x:out select="CCY" />" value="<x:out select="CUST_AC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                        </x:forEach>
                    </select>
                </p>
                <div  id="crbranchcode"></div>
                <p>
                    <label><bean:message key="label.ftcurrency"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text"  id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>" disabled  /><!---it was disabled because this filed is only used to allow customer to know the currncy of the account they are tasfering from-->
                    <input type="hidden" id="branch" name="branch" value=""/>
                    <input type="hidden" id="availbal" name="availbal" value=""/>
                    <input type="hidden" id="maxamt" name="maxamt" value=""/>
                    <input type="hidden" id="ccy2" name="ccy2" value=""/>

                </p>
                <p>
                    <label><bean:message key="label.ftamount"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="amt"  id="amt" name="ccy" value="" /> <span id="balance"></span>

                </p>

                <p>
                    <label><bean:message key="label.narration"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" maxlength="150" type="narration"  id="narration" name="narration" value=""  /><span id="narration"></span>
                </p>                    

               <p>

                    <input id="testButton" name="testButton" type="button" onclick="temporaryDisable(this);
                            getPin('70000', '1');"  value="<bean:message key="label.getpin"/>" class="button"> 
                </p>
                <div id="trx_details">
                    <br /><br />
                </div>
                <!--<div id="trx_detail"></div>-->
                <p>
                    <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text" name ="enterpin" id="enterpin" maxLength="20"  value="" />

                </p>
                <p>
                    <input id="testButton" name="testButton" type="button" onclick="fundtransferft(3331,'333');"  value="<bean:message key="label.Submit"/>" class="button">
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                </p>







            </form>
            <!--<div id="trx_details">

            </div>-->
            <div id="search_result">

            </div>
            <div id="searchresult">

            </div>        </div>


    </body>
</html>
