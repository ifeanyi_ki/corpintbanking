



<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<head><meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp"></head>

             <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />


                <div class="clear"></div>

<div id="trx_details" class="tab-content default-tab">
            <form Name="TxnFrms" id="TxnFrms" action="/MainController" method="Post">

                    <p>
                        <label><bean:message key="label.ftlimit"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="ftlimt" name="ftlimt" value="100000" /><!-- <span class="input-notification error png_bg">Error message</span>-->

                    </p>
                    <p>
                        <label><bean:message key="label.sidebitaccount"/><b><font color="red">*</font></b></label>
                        <select name="ftdebitacc" id ="ftdebitacc" class="text-input small-input"  >
                                <option value="" selected style="display:none">Select an account</option>
                                <x:forEach var="row" select="$doc/ROWSET/ROW">
                               <option value="<x:out select="CUST_ACC_NO"/>"><x:out select="CUST_AC_NO"/></option>
                                </x:forEach>

                            </select>

                    </p>

                    <p>
                        <label><bean:message key="label.selectbengrp"/><b><font color="red">*</font></b></label>
                        <select name="benacct" id ="benacct"  class="small-input"><!--onblur="updateDestAcct()"-->
                             <x:forEach var="ben" select="$docs/ROWSET/BENEFICIARY" >
                    <option value="<x:out select="BENEFTGRPNAME" />"><x:out select="BENEFTGRPNAME" /></option>
                    </x:forEach>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="docs" xml="${xml}" />
                    <x:forEach var="ben" select="$docs/ROWSET/BENEFICIARY" >
                        <option value="<x:out select="DESTINATION_ACCT" />"><x:out select="DESTINATION_ACCT" /></option>
                    </x:forEach>
                </select></p>
                    <p>
                        <label><bean:message key="label.sicreditaccount"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo" maxLength="20"  value="" onblur="getAccountDetail('001','332');"/>

                    </p>
                    <div  id="crbranchcode"></div>
                    <p>
                        <label><bean:message key="label.ftcurrency"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text"  id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>"  />
                          <input type="hidden" name="user_password"  id="user_password"  value="password"  >
                    </p>
                        <p>
                        <label><bean:message key="label.ftamount"/><b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="amt"  id="amt" name="ccy" value="" />

                    </p>

                    <p>
                           <input id="testButton" name="testButton" type="button" onclick="fundTransfer();"  value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                    </p>







            </form>
                            </div>