


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


            <html>
                <head>
 <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
                </head>
                <body>


 <div class="content-box">
     <div class="content-box-header">

                    <h3><bean:message key="label.setupft"/></h3>


                </div>
                        <div id="trx_detail" class="tab-content default-tab">

    <form action="" method="POST" name="setup">



          <p>
                <label><bean:message key="label.institutionId"/></label>
                <input class="text-input small-input" type="text" id="institutionId" name="institutionId" value="<x:out select="$row/INSTITUTION_ID"/>"  />

            </p>

              <p>
                <label><bean:message key="label.institution"/></label>
                <input class="text-input small-input" type="text" id="institution" name="institution" value="<x:out select="$row/INSTITUTION_NAME"/>"  />

            </p>


            <p>
                <label><bean:message key="label.ftminimumamount"/></label>
                <input class="text-input small-input" type="text" id="ftminimumamount" name="ftminimumamount" value="<x:out select="$row/MIN_AMOUNT"/>"  />

            </p>
            <p>
                <label><bean:message key="label.ftmaxamount"/></label>
                <input class="text-input small-input" type="text" id="ftmaxamount" name="ftmaxamount" value="<x:out select="$row/MAX_AMOUNT"/>"  />

            </p>

            <p>
                <label><bean:message key="label.usebeneficiaryciaryonly"/></label>
                <input  type="checkbox" id="usebeneficiaryciaryonly" name="usebeneficiaryciaryonly" value="<x:out select="$row/USE_BENEFICIARY"/>"  />

            </p>


            <p>
                <a   href="javascript:void(0)" onclick="setupft(60111,'331');" rel="modal" class="button"><bean:message key="label.ftsetup"/></a>
                <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
            </p>





    </form>
                        </div>
</div>


</body>
        </html>
