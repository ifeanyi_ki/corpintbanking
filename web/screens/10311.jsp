

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>


            <html>
                <head>

                </head>
                <body>
<c:set var="xml" value="${data}" />


<%System.out.println(pageContext.findAttribute("xml") + "nawao");%>
 <x:parse varDom="doc" xml="${xml}" />

 <div  class="tab-content" id="tab2" align="left">
    <div class="content-box-header">

        <h3><bean:message key="label.viewuser"/></h3>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->
    <form action="" method="POST">


 <x:forEach var="row" select="$doc/ROWSET/ROW">
            <c:set var="userType">
                <x:out select="$row/COD_ROLE_ID"/>
            </c:set>
            
            <c:choose>
                <c:when test="${userType=='EXT_USER' || userType=='CUS_ENQUI'}">
                    <p>
                        <label><bean:message key="label.custno"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="custno" name="custno" value="<x:out select="$row/CUSTNO"/>" disabled/><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->

                         <input type="hidden" id="custno"  name="custno"   value="<x:out select="$row/CUSTNO"/>" >
                    </p>
                </c:when>
            </c:choose>
            

            <p>
                <label><bean:message key="label.userid"/></label>
                <input class="text-input small-input" type="text" id="user" name="user" value="<x:out select="$row/USERID"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
                 <input type="hidden" id="user"  name="user" value="<x:out select="$row/USERID"/>"  >
            </p>
                <p>
                <label><bean:message key="label.email"/></label>
                <input class="text-input small-input" type="text" id="user" name="user" value="<x:out select="$row/EMAIL"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
                 <input type="hidden" id="email"  name="email" value="<x:out select="$row/USERID"/>"  >
            </p>
             <p>
                <label><bean:message key="label.phone"/></label>
                <input class="text-input small-input" type="text" id="phone" name="lang" value="<x:out select="$row/PHONE"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
                <input type="hidden" id="phone"  name="phone" value="<x:out select="$row/PHONE"/>"  >
            </p>
            
            <p>
                <label>Name</label>
                <input class="text-input small-input" type="text" id="user_name" name="user_name" value="<x:out select="$row/USERNAME"/>"  disabled/><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
               <input type="hidden" id="user_name"  name="user_name"  value="<x:out select="$row/USERNAME"/>"  >
            </p>
            <c:choose>
                <c:when test="${userType=='EXT_USER' || userType=='CUS_ENQUI'}">
                    <p>
                        <label>Account Numbers Linked</label>
                        <textarea class="textarea wysiwyg" wrap="hard" disabled=""><x:out select="$row/ACCT_NO"/></textarea>
                        <input type="hidden" id="acct_no"  name="acct_no"  value="<x:out select="$row/ACCT_NO"/>"  >
                    </p>
                </c:when>
                
            </c:choose>
            

            <p>
                <label><bean:message key="label.lang"/></label>
                <input class="text-input small-input" type="text" id="lang" name="lang" value="<x:out select="$row/LANG"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
            </p>
             <p>
                <label><bean:message key="label.branchcode"/></label>
                <input class="text-input small-input" type="text" id="lang" name="lang" value="<x:out select="$row/USERBRANCH"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
            </p>
            <p>
                <label><bean:message key="label.lastlogin"/></label>
                <c:set var="cd2"  >
                                                <x:out select="$row/LASTLOGINDATE"/>
                                            </c:set>
                                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd" var="formatedDate"/>
                                             
                <input class="text-input small-input" type="text" id="lastlogin" name="lastlogin" value='<fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy" />' disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
            </p>
            <p>
                
                <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
            </p>

 </x:forEach>




        <input type="hidden" name="screenId" id="screenId" value="1031"/>
        <input type="hidden" name="taskId" id="taskId" value="10311"/>

    </form>
</div>


</body>
        </html>
