<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.productsetup"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">

                <p>
                        <select name="categoryid" id ="categoryid" onchange="GetServCat('000','805')" class="small-input">
                       <option value="" selected style="display:none"></option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CATEGORY_ID" />"><x:out select="CATEGORY_NAME" /></option>
                    </x:forEach>
                </select>
                </p>

                <p>
                    <label><bean:message key="label.chooseservice"/><font color="red">*</font></label>
                    <select name="serviceid" id ="serviceid" onchange="GetProdForm(8051,'805')" class="small-input">
                        <option value="" selected style="display:none"></option>
                       
                    </select>
                </p>


                <div id="trx_detail">

                </div>



            </form>
            <div id="search_result">

            </div>
            <div id="searchresult">

            </div>        </div>
    </body>
</html>

