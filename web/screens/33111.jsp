


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


            <html>
                <head>
                 <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
                </head>
                <body>


 <div class="content-box">
     <div class="content-box-header">

                    <h3><bean:message key="label.setupft"/></h3>
                    

                </div>
                        <div id="trx_detail" class="tab-content default-tab">

    <form action="" method="POST" name="setup">
<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<x:set var="row" select="$doc/ROWSET/ROW"/>


          <p>
                <label><bean:message key="label.institutionId"/></label>
                <input class="text-input small-input" type="text" id="institutionIdold" name="institutionIdold" value="<x:out select="$row/INSTITUTION_ID"/>" disabled />
                <input  type="hidden" id="institutionId" name="institutionId" value="<x:out select="$row/INSTITUTION_ID"/>"  />
            </p>

              <p>
                <label><bean:message key="label.institution"/></label>
                <input class="text-input small-input" type="text" id="institutionold" name="institutionold" value="<x:out select="$row/INSTITUTION_NAME"/>" disabled />
                 <input  type="hidden" id="institution" name="institution" value="<x:out select="$row/INSTITUTION_NAME"/>"  />
            </p>


            <p>
                <label><bean:message key="label.ftminimumamount"/></label>
                <input class="text-input small-input" type="text" id="ftminimumamount" name="ftminimumamount" value="<x:out select="$row/MIN_AMOUNT"/>"  />
                <input type="hidden" value="<x:out select="$row/SEQ_NO"/>" id="refnos" name="refno"/>
            </p>
            <p>
                <label><bean:message key="label.ftmaxamount"/></label>
                <input class="text-input small-input" type="text" id="ftmaxamount" name="ftmaxamount" value="<x:out select="$row/MAX_AMOUNT"/>"  />

            </p>

           <p>
                <label><bean:message key="label.usebeneficiaryciaryonly"/></label>
                <input  type="checkbox" id="usebeneficiaryciaryonly" name="usebeneficiaryciaryonly" value="<x:out select="$row/USE_BENEFICIARY"/>"  />

            </p>


            <p>
                <a   onclick="setupfts(33112,'33111',document.getElementById('refnos').value);" rel="modal" class="button"><bean:message key="label.ftsetup"/></a>
                
            </p>





    </form>
                        </div>
</div>


</body>
        </html>
