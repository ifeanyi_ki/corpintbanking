<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>    
        <c:set var="xml" value="${data}" />
        <x:parse varDom="doc" xml="${xml}" />
        <x:forEach var="row" select="$doc/ROWSET/ROW">
            <c:set var="cc" >
                <x:out select="$doc/ROWSET/ROW/SUCCESS" />
            </c:set>
            <c:if test="${cc!=null || cc!='' }">
                <!--                       <fieldset> //commented out by K.I, put the logic to generate receipt for the transfer
                                        <table>
                                
                                                    <tr>
                                                        <td> &nbsp;&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                
                <td width="500" class="LblIPMand" height="20" nowrap> <x:out select="$doc/ROWSET/ROW/SUCCESS"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td> &nbsp;&nbsp;</td>
                                                    </tr>
                                        </table>
                                        </fieldset>
                                put the logic for the receipt by K.I -->
                <!--                <fieldset>-->

                <x:out select="$doc/ROWSET/ROW/SUCCESS"/>
                <form method="post" id="third_party_ft_receipt">
                    <br/>
                    <br/>
                    <table cellspacing='6' >
                        <!--                        <tr>
                                                    <td>Transaction Code </td> 
                                                    <td>
                        <c:set var="txnCode">
                            <x:out select="$doc/ROWSET/ROW/TRANSACTION_CODE"/>
                        </c:set>
                        <x:out select="$doc/ROWSET/ROW/TRANSACTION_CODE"/>
                        <input type="text" hidden="true" value="${txnCode}" name="txncode"/>
                    </td>
                </tr>-->
                        <tr>
                            <td>Customer Account </td>
                            <td>
                                <c:set var="accno">
                                    <x:out select="$doc/ROWSET/ROW/FTDEBITACC"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/FTDEBITACC"/>
                                <input type="text" hidden="true" value="${accno}" name="accno"/><!--ftdebitacc-->
                            </td>
                        </tr>
                        <tr>
                            <td>Transfered Amount</td>
                            <td>
                                <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                                <c:set var="amt">
                                    <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                                </c:set>
                                <input type="text" hidden="true" value="${amt}" name="amount"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Beneficiary Account </td>
                            <td>
                                <c:set var="benacc">
                                    <x:out select="$doc/ROWSET/ROW/BENACC"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/BENACC"/>
                                <input type="text" hidden="true" value="${benacc}" name="benacc"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Narration</td>
                            <td>
                                <c:set var="narration">
                                    <x:out select="$doc/ROWSET/ROW/NARRATION"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/NARRATION"/>
                                <input type="text" hidden="true" value="${narration}" name="nar"/>
                            </td>
                        </tr>
                    </table>
                    <c:set var="refno">
                        <x:out select="$doc/ROWSET/ROW/REFNO"/>
                    </c:set>
                    <input type="text" hidden="true" value="${refno}" name="refno"/>
                    <br/>
                    <br/>
                    <div align="center">
                        <input type="submit"  class="button" value="Download Receipt" name="receipt"  onclick="setFormActionFTReceipt('pdf');">
                    </div>
                </form>

                <!--                </fieldset>-->
            </c:if>            <c:if test="${cc==''||cc==null}">
                <fieldset>

                    <table width="500" height="20">
                        <tr>
                            <td width="122" class="LblIPMand" height="20"><bean:message key="label.error"/><font color="red">*</font></td>
                            <td width="" height="20"<td width="" height="20"><b><bean:message key="label.paswdeerrorgen"/></b></td>
                            <td></td>
                        </tr>
                        </tr>

                    </table>
                </fieldset>
            </c:if>
        </x:forEach>
    </body>
</html>

