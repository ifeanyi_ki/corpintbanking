<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <div align="center" >
        <div class="content-box">
       
            <div class="content-box-header">
  
               
 <h3><bean:message key="label.affiliatesetup"/></h3>

					<ul class="content-box-tabs" >
						<li><a onclick="return showAuth(801,'1');"  class="default-tab">Setup</a></li> <!-- href must be unique and match the id of target div -->
						<li><a  onclick="return ManageAfiliate(8013,'801');">Manage Setup</a></li>
					</ul>
                <div class="clear"></div>

            </div >
            <div align="left" style="margin-left: 10px;height: 600px">
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">

                    <p>
                        <label><bean:message key="label.affiliatename"/><font color="red">*</font></label>
                       <input class="text-input small-input" type="text" name ="affname" id="affname" maxLength="20"  value="" />

                    </p>
                      
                       <p>
                        <label>Affiliate Code<b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="affcode" id="affcode" maxLength="20"  value="" />

                    </p>

                       <p>
                        <label>Country Name<b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="contname" id="contname" maxLength="20"  value="" />

                    </p>
                     
                      <p>
                        <label>Country Code<b><font color="red">*</font></b></label>
                        <input class="text-input small-input" type="text" name ="contcode" id="contcode" maxLength="20"  value="" />

                    </p>


                    <p>
                           <input id="testButton" name="testButton" type="button" onclick="maintaff(8012,801);"  value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                    </p>




            </form>
                            <div id="trx_detail">
                                
                            </div>
                    <div id="ajaxload">

                            </div>
                        <div id="searchresult">

                        </div>        </div></div></div>
    </body>
</html>
