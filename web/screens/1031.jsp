


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>







    <c:set var="xml" value="${data}" />
    <x:parse varDom="doc" xml="${xml}" />
    <c:set var="error">
        <x:out select="$doc/ROWSET/ROW/NODATA"/>
    </c:set>
    <c:set var="highcount">
        <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
    </c:set>
    <c:set var="lowcount">
        <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
    </c:set>

    <c:set var="size">
        <x:out select="$doc/ROWSET/ROW/SIZE"/>
    </c:set>
    <c:set var="increment" value="20" />

    <div  class="tab-content" id="tab1">
        <div class="content-box-header">

            <h3><bean:message key="label.ListUser"/></h3>

            <div class="clear"></div></div>
            <fieldset style="padding-top: 20px;">
                <!--<img src="./img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
                <img src="./img/bg-th-right.gif" width="7" height="7" alt="" class="right" />-->


                <c:choose>
                    <c:when test="${error==null || error!='[]'|| error==' '}">
                        <table>

                            <thead>
                                <tr>
                                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                                    <th><bean:message key="label.userid"/></th>
                                    <th><bean:message key="label.email"/></th>

                                </tr>

                            </thead>

                            <tfoot >
                                        <tr><td>
                    <div id="Pagination" class="pagination">
                        
                    </div>
                               </td></tr>
                            </tfoot>
                            <tbody id="pageindex">
                                
                          </tbody>
                        </table>

                    </fieldset>
                    


                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
                    <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
                
            </c:when>
            <c:otherwise>
                <table>
                    <tr>
                        <td>There  is no data</td>
                    </tr>
                </table>
            </c:otherwise>

        </c:choose>
            <div style="display: none">
                <table>
                    <tbody id="hiddenresult">
                                <c:set var="xml" value="${data}" />

                                <c:set var="count" value="0"/>

                                <x:forEach var="row" select="$doc/ROWSET/ROW">

                                    <c:set var="count" value="${count+1}"/>
                                    <c:if  test="${count<=20}">
                                        <tr>
                                            <td>
                                                <x:set var="c" select="USERID"/>

                                                <input name="refno" id="refno" type="radio" value="<x:out select="$c" />"  onclick="getUserForm(this.value,'10311','1031');"/><x:out select="$c" />

                                            </td>
                                            <td>
                                            <x:out select="EMAIL"/></td>
                                        </tr>
                                    
                                </c:if>
                            </x:forEach>
                          </tbody>
                </table>
            </div>
                                </div>



