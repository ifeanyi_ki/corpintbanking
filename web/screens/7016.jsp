<%--
    Document   : 701
    Created on : 27-Apr-2011, 10:13:19
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%--

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.electricitybill"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form Name="TxnFrm" id="TxnFrm" action="/MainController" method="Post">
                     
            <c:set var="xml" value="${data}" />
            <x:parse varDom="doc" xml="${xml}" />
       
                    <p>
                        
                      <!--  <input type="hidden" id="screenId" value="7015"/>-->
                        <input type="hidden" id="taskId" value="7015"/>
                        
             <div  class="tab-content" id="tab1">
        

            <p class="column-left">
                <b><bean:message key="label.accounttodebit"/></b>
            </p>
            <p class="column-right">
                <x:out  select="$doc/ROWSET/ACCOUNT_NUMBER" /> 
                <input type="hidden" id="acct_no" value=<x:out  select="$doc/ROWSET/ACCOUNT_NUMBER" /> name="accttodebt"/>
            </p>
               
             <p class="column-left">
                <b><bean:message key="label.packagename"/></b>
            </p>
            <p class="column-right">
                <x:out  select="$doc/ROWSET/PACKAGE_NAME" /> 
                <input type="hidden" id="packagename" value=<x:out  select="$doc/ROWSET/PACKAGE_NAME" /> name="packge="/>
            </p>
            
            <p class="column-left">
                <b><bean:message key="label.amount"/></b>
            </p>
            <p class="column-right">
                 <x:out  select="$doc/ROWSET/AMOUNT" />
                 <input type="hidden" id="amount" value=<x:out  select="$doc/ROWSET/AMOUNT" /> name="amount"/>
            </p>
            <p class="column-left">
                <b><bean:message key="label.charge"/></b>
            </p>
            <p class="column-right">
                <x:out  select="$doc/ROWSET/CHARGE" />
                <input type="hidden" id="charge" value=<x:out  select="$doc/ROWSET/CHARGE" /> name="charge"/>
            </p>
            <p class="column-left">
                <b><bean:message key="label.ecgcardnumber"/></b>
            </p>
            <p class="column-right">
                <x:out  select="$doc/ROWSET/CARDNUMBER" />
                <input type="hidden" id="cardnumber" value=<x:out  select="$doc/ROWSET/CARDNUMBER" /> name="cardnumber"/>
            </p>
            <p class="column-left">
                <b><bean:message key="label.ecgemail"/></b>
            </p>
            <p class="column-right">
                 <x:out  select="$doc/ROWSET/EMAIL" />
                 <input type="hidden" id="ecgemail" value=<x:out  select="$doc/ROWSET/EMAIL" /> name="ecgemail"/>
            </p>
            
            <p class="column-left">
                <b><bean:message key="label.ecgmobileNumber"/></b>
            </p>
            <p class="column-right">
                 <x:out  select="$doc/ROWSET/MOBILE_NUMBER" />
                 <input type="hidden" id="mobileNumber" value=<x:out  select="$doc/ROWSET/MOBILE_NUMBER" /> name="mobileNumber"/>
            </p>
            <p class="column-left">
                <b><bean:message key="label.ecgremarks"/></b>
            </p>
            <p class="column-right">
                 <x:out  select="$doc/ROWSET/REMARKS" />
                 <input type="hidden" id="ecgremarks" value=<x:out  select="$doc/ROWSET/REMARKS" /> name="ecgremarks"/>
            </p>
            <p class="column-left">
                <b><bean:message key="label.enterpin"/></b>
            </p>
            <p class="column-right">
                <x:out  select="$doc/ROWSET/TOKEN" />
                <input type="hidden" id="enterpin" value=<x:out  select="$doc/ROWSET/TOKEN" /> name="token"/>
            </p>
            <input type="hidden" id="flag"  name="flag" value="null"/>
          
        
                    
                    <p>
                    
                         
<%-- <input type="button"  class="button" value="Continue" onClick="recharge(70151,'7015')">
 <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">

                    </p>







            </form>
                    <div id="search_result">
<input id="testButton" name="testButton" type="button" onClick="recharge(70151,'7016')"  value="<bean:message key="label.ecgSubmit"/>" class="button">
                            </div>
                               </div>
    </body>
    
</html>


--%>








<html>
    <head>
              <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <%

        int timeout = session.getMaxInactiveInterval();
        response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
    %>
    <body>


        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <c:set var="xml" value="${data}" />


                <x:parse varDom="doc" xml="${xml}" />



                <h3>Confirm entries</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">


                <p>
                    <label><bean:message key="label.accounttodebit"/></label>
                    <input class="text-input small-input" type="text" id="acct_no"  name="acct_no" value="<x:out  select="$doc/ROWSET/ROW/ACCOUNT_NUMBER" /> " disabled />
                    <input type="hidden" id="acct_no" value=<x:out  select="$doc/ROWSET/ROW/ACCOUNT_NUMBER" /> name="accttodebt"/>

                </p>
                <p>

                    <label>Beneficiary's Account<font color="red">*</font></label>
                    <input class="text-input small-input" type="text"  id="bene_acc_no" name="bene_acc_no" value="<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_ACCOUNT" />" disabled/>
                    <input type="hidden"  id="bene_acc_no" value=<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_ACCOUNT" /> name="bene_acc_no" > 
                </p>


                <p>

                    <label>Beneficiary's Account Name<font color="red">*</font></label>
                    <input class="text-input small-input" type="text"  id="bene_acc_name" name="bene_acc_name" value="<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_ACCOUNT_NAME" />" disabled/>
                    <input type="hidden"  id="bene_acc_name"  value=<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_ACCOUNT_NAME" /> name="bene_acc_name" > 
                </p>

                <p>

                    <label>Beneficiary's Bank<font color="red">*</font></label>
                    <input class="text-input small-input" type="text"  id="bank" name="bank" value="<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_BANK" />" disabled/>
                    <input type="hidden"  id="bank"  value=<x:out  select="$doc/ROWSET/ROW/BENEFICIARY_BANK" /> name="bank" > 
                </p>


                <p>
                    <label><bean:message key="label.amount"/></label>
                    <c:set var="realAmt">
                        <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                    </c:set>
                    <fmt:formatNumber type="number" value="${realAmt}" var="formattedAmt" />
                    <input class="text-input small-input" type="text" id="amt1"  name="amt1" value="${realAmt}" disabled />
                    <input type="hidden" id="amt" value=<x:out  select="$doc/ROWSET/ROW/AMOUNT" /> name="amt"/>

                </p>


                <p>
                    <label><bean:message key="label.enterpin"/><b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text" id="enterpin"  name="enterpin" value="<x:out  select="$doc/ROWSET/ROW/TOKEN" />" disabled />
                    <input type="hidden" id="enterpin" value=<x:out  select="$doc/ROWSET/ROW/TOKEN" /> name="enterpin"/>

                </p> 





                <p>
                    <label><bean:message key="label.ecgremarks"/></label>
                    <input class="text-input small-input" type="text" id="ecgremarks"  name="ecgremarks" value="<x:out  select="$doc/ROWSET/ROW/REMARKS" />" diabled />
                    <input type="hidden" id="ecgremarks" value=<x:out  select="$doc/ROWSET/ROW/REMARKS" /> name="ecgremarks"/>

                </p>



                <p>
                    <input type="button" onclick="transferIB('IBFT', '7016');" value="Confirm" class="button"/>
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>





            </form>
        </div>
        <div id="ajaxload"></div>



    </body>
</html>
