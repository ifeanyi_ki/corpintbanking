/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var period_specific	= false;
var period_amount	= false;
var l_accts = true;
var	acctDisplay	= new Array ();
var count = 0;
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function disabledatebox(a)  {
	if (period_specific)  {
                   return(true);
	}else  {
		a.blur();
	}
	return(false);
}
//------------------------------------------------------------------------------

function disableAmountbox(a)  {
	if (period_amount)  {
		return(true);
	}else  {
		a.blur();
	}
	return(false);
}

//------------------------------------------------------------------------------
function selectDates ()
{
    if(document.frmStmtInq.fldPeriod[0].checked == true) {
        period_specific	= true;
        document.frmStmtInq.fldFromDate.value = "";
        document.frmStmtInq.fldToDate.value	= "";

        document.frmStmtInq.fldFromDate.disabled 	= false;
        document.frmStmtInq.fldToDate.disabled 		= false;
        //document.frmStmtInq.fldFromAmount.disabled 	= true;
        //document.frmStmtInq.fldToAmount.disabled      = true;
    }else {
        period_specific	= false;
        document.frmStmtInq.fldFromDate.value = "";
        document.frmStmtInq.fldToDate.value	= "";

        document.frmStmtInq.fldFromDate.disabled 	= true;
        document.frmStmtInq.fldToDate.disabled 		= true;
    }
        
    //document.frmStmtInq.fldFromDate.focus();

		//deselectAmount();

}
//------------------------------------------------------------------------------

function selectAmounts ()
{
     //alert('hmm2w');
     
    if(document.frmStmtInq.fldPeriod[1].checked == true) {
        
        period_amount	= true;
        document.frmStmtInq.fldFromAmount.value = "";
        document.frmStmtInq.fldToAmount.value	= "";


        //document.frmStmtInq.fldFromDate.disabled 	= true;
        //document.frmStmtInq.fldToDate.disabled 		= true;
        document.frmStmtInq.fldFromAmount.disabled 	= false;
        document.frmStmtInq.fldToAmount.disabled      = false;

        document.frmStmtInq.fldFromAmount.focus();
        document.frmStmtInq.fldDrCr[0].checked = true;
    } else {
        
        period_amount	= false;
        document.frmStmtInq.fldFromAmount.value = "";
        document.frmStmtInq.fldToAmount.value	= "";

        document.frmStmtInq.fldFromAmount.disabled 	= true;
        document.frmStmtInq.fldToAmount.disabled      = true;
        
        for(var i = 0; i <= document.frmStmtInq.fldDrCr.length; i++){
            document.frmStmtInq.fldDrCr[i].checked=false   ;
        }
    }

		//deselectDates();
}

//------------------------------------------------------------------------------
function Inits()
{
  
	//if (l_accts) {
		//if (document.frmStmtInq.fldPeriod[0].checked == true) {
		document.frmStmtInq.fldFromDate.disabled 	= true;
		document.frmStmtInq.fldToDate.disabled 		= true;
                document.frmStmtInq.fldFromAmount.disabled 	= true;
		document.frmStmtInq.fldFromAmount.disabled      = true;
		deselect();
		//}
	//}
}
//------------------------------------------------------------------------------
function deselect ()
{
		deselectDates ();
		
			deselectAmount ();
		
}

//------------------------------------------------------------------------------
function deselectDates ()
{
		document.frmStmtInq.fldFromDate.value		= ""    ;
		document.frmStmtInq.fldToDate.value 		= ""   ;
		period_specific	= false;
}
//------------------------------------------------------------------------------

function deselectAmount ()
{
		document.frmStmtInq.fldFromAmount.value		= ""    ;
		document.frmStmtInq.fldToAmount.value 		= ""   ;
		for(var i = 0; i <= document.frmStmtInq.fldDrCr.length; i++){
			document.frmStmtInq.fldDrCr[i].checked=false   ;
		}
		period_amount	= false;
}

//-----------------------------------------------------------------------------
function click_go () {

	var l_acctdisp	= document.frmStmtInq.fldAcctNo.options[document.frmStmtInq.fldAcctNo.selectedIndex].text;

	if (document.frmStmtInq.fldPeriod[0].checked == true) {
		document.frmStmtInq.fldPeriod.value = "0";
		document.frmSubmit.fldPeriod.value = "0";
	}
	if (document.frmStmtInq.fldPeriod[1].checked == true) {
		document.frmStmtInq.fldPeriod.value 	= "1";
		document.frmSubmit.fldPeriod.value = "1";
		document.frmSubmit.fldFromDate.value	= document.frmStmtInq.fldFromDate.value;
		document.frmSubmit.fldToDate.value	= document.frmStmtInq.fldToDate.value;

		var l_date1 = document.frmStmtInq.fldFromDate.value;
		var l_date2 = document.frmStmtInq.fldToDate.value;

		if (l_date1 == "") {
			alert ("%%K_ENTR_ST_DT%%");
			document.frmStmtInq.fldFromDate.focus ();
			return false;
		}

		if (l_date2 == "") {
			alert ("%%K_ENTR_END_DT%%");
			document.frmStmtInq.fldToDate.focus ();
			return false;
		}

		if (!date_val (l_date1)) {
			document.frmStmtInq.fldFromDate.focus ();
			return false;
		}
		if (!date_val (l_date2)) {
			document.frmStmtInq.fldToDate.focus ();
			return false;
		}

		if ((whichDateGreater (l_date1, l_date2)) == 1) {
			alert ("%%K_TO_DT_GRTR%%");
			return false;
		}
	}

	if (document.frmStmtInq.fldPeriod[2].checked == true) {
		document.frmStmtInq.fldPeriod.value 	= "2";
		document.frmSubmit.fldPeriod.value 		= "2";
		document.frmSubmit.fldFromAmount.value	= trim(document.frmStmtInq.fldFromAmount.value);
		document.frmSubmit.fldToAmount.value	= trim(document.frmStmtInq.fldToAmount.value);
		var i =0;
		document.frmSubmit.fldDrCr.value = "";
		for (i=0; i<document.frmStmtInq.fldDrCr.length; i++){
			if(document.frmStmtInq.fldDrCr[i].checked == true){
				document.frmStmtInq.fldDrCr.value = document.frmStmtInq.fldDrCr[i].value;
			}
		}
		var l_amount1 = document.frmStmtInq.fldFromAmount.value;
		var l_amount2 = document.frmStmtInq.fldToAmount.value;

		if (validateAmount (l_amount1)) {
			setAmount (document.frmSubmit.fldFromAmount);
		}else{
			document.frmStmtInq.fldFromAmount.focus ();
			return false;
		}

		if (validateAmount (l_amount2)) {
			setAmount (document.frmSubmit.fldToAmount);
		}else{
			document.frmStmtInq.fldToAmount.focus ();
			return false;
		}

		if(l_amount1 > l_amount2){
			alert("The initial amount is greater than final");
			document.frmStmtInq.fldFromAmount.focus ();
			return false;
		}
	}

	document.frmSubmit.fldAcctNo.value
		= document.frmStmtInq.fldAcctNo.options[document.frmStmtInq.fldAcctNo.selectedIndex].value;
	document.frmSubmit.fldAcctDisplay.value	= l_acctdisp;
	document.frmSubmit.fldClosingBal.value	= "0";
	document.frmSubmit.fldOpeningBal.value	= "0";
	document.frmSubmit.fldScrnSeqNbr.value	= "02";

	if (document.frmSubmit.fldTxnId.value=="LNA") {
		document.frmSubmit.fldCodModule.value = "LN";
	} else{
		document.frmSubmit.fldCodModule.value = "CH";
	}

	document.frmSubmit.fldSort.value = document.frmStmtInq.fldSort.options[document.frmStmtInq.fldSort.selectedIndex].value;
	document.frmSubmit.submit ();
	return false;
}



