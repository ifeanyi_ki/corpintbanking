

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <div id="trx_detail">
            <div class="content-box"><!-- Start Content Box -->
                <%if (!(((String) request.getAttribute("others")) == null)) {%>
                <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification"  alt="close" onclick="addNoclass();"/></a>
                    <div>
                        <%=((String) request.getAttribute("others"))%>
                    </div>
                </div>


                <%}%>

                <div class="content-box-header">

                    <h3>Authorise Other Setup</h3>

                    <ul class="content-box-tabs">
                        <li><a href="#tab1" class="default-tab">List of Unathorise Setup</a></li> <!-- href must be unique and match the id of target div -->

                    </ul>

                    <div class="clear"></div>

                </div> <!-- End .content-box-header -->

                <div class="content-box-content">

                    <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->


                        <table>

                            <thead>
                                <tr>
                                    <th>Parameter Description</th>
                                    <th> Parameter Value</th>
                                    <th>Modified By</th>
                                    <th>Modified Date</th>
                                    <th>Action</th>
                                </tr>

                            </thead>

                            <tfoot >


                            </tfoot>

                            <tbody id="pageindex">

                            </tbody>

                        </table>
                        <input type="hidden" value="<%=((String) request.getAttribute("others"))%>" id="error" name="error" />  
                        <div id="Pagination" class="pagination">

                        </div>         
                    </div>
                </div>
            </div>
        </div>

        <div  style="display: none">
            <table >
                <tbody id="hiddenresult">
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW">
                        <tr>
                            <td><input type="hidden" value="<x:out select="$row/KEYS" />" /><x:out select="$row/KEYS" /></td>
                            <td><x:out select="$row/VALUE" /></td>
                            <td><a href="#" title="title"></a><x:out select="$row/MODIFIED_BY" /></td>

                            <td>
                                <c:set var="cd2"  >
                                    <x:out select="$row/MODIFIED_DATE"/>
                                </c:set>
                                <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd"                var="formatedDate"/>
                                <fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/>

                            </td>
                            <td>
                                <!-- Icons -->
                                <a href="#" title="Approve" onclick="ApproveOtherSetup('300611', '30061', 'A', '<x:out select="$row/MODIFIED_BY" />', '<x:out select="$row/KEYS" />', 'AUTH')">Approve</a>
                                <a href="#" title="Reject" <a href="#" title="Reject" onclick="ApproveOtherSetup('300611', '30061', 'R', '<x:out select="$row/MODIFIED_BY" />', '<x:out select="$row/KEYS" />', 'AUTH')">Reject</a>  

                            </td>
                        </tr>

                    </x:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
