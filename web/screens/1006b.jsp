
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="application/pdf" %>       
<%@
page import="
java.io.*,
java.util.*,
java.text.*,
com.itextpdf.text.pdf.*,
com.itextpdf.text.*,com.fasyl.ebanking.util.*,org.w3c.dom.*,org.xml.sax.*,com.fasyl.ebanking.logic.CustomerTrx"
%>
<% response.setHeader("Content-Disposition", "attachment; filename=\"Statement.pdf\""); %> 
<%
 
//response.setContentType("application/pdf");
com.itextpdf.text.Document document = new com.itextpdf.text.Document();

 System.out.println("=============== inside 1006b ==================");
           java.sql.Connection connection = null;
            java.sql.CallableStatement callable = null;
            String data = null;
            String scn = request.getParameter("taskId") + ".jsp";
            //String acct =(String) session.getAttribute("acct_no");
            String acct=request.getParameter("acct_no");
            //acct = "008-810002-10-00-02";
           System.out.println(acct+"This is d acct");
            String startDate = request.getParameter("fldFromDate");
            String endDate = request.getParameter("fldToDate");
            String from = request.getParameter("fldFromAmount");
            String to = request.getParameter("fldToAmount");
            String drcr = request.getParameter("fldDrCr");
            String order = request.getParameter("order");
            System.out.println(acct+"This is d acct"+ " startdate " + startDate);
            CustomerTrx custtrx = new CustomerTrx();
            data = custtrx.getTrxXml(acct, startDate, endDate, from, to, drcr,order); 
    
try{
    NumberFormat     formatter = new DecimalFormat("0.00");
 Hashtable<String, String> list   = new Hashtable<String, String>();
        org.w3c.dom.Document                  doc    = XMLUtilty.getStringAsdocument(data);
        org.w3c.dom.Element                   docEle = doc.getDocumentElement();
        NodeList                  nl     = docEle.getElementsByTagName("ROW");
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
PdfWriter.getInstance(document, buffer); 
document.open();
ServletContext context = getServletContext(); //commented out by K.I, edited to load TB logo
String path  = context.getRealPath("screens/scripts/resources/images/logo.png");
System.out.println("This is the contextpath : "+document.right(document.rightMargin()) + " y coord "+document.top());
    Image image = Image.getInstance(path);
          //image.setAbsolutePosition(505,800);
          image.setAbsolutePosition(290f,800f);
            //document.add(new Paragraph());
            document.add(image) ; //end of comment
document.add(new Paragraph());                         
document.add(Chunk.NEWLINE);
document.add(Chunk.NEWLINE);
//document.addTitle("Loan Repayment");              
PdfPTable table = new PdfPTable(6);
PdfPCell cell = new PdfPCell (new Paragraph ("Transaction History"));
cell.setColspan(6);
cell.setRowspan(2);
cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
cell.setVerticalAlignment(Chunk.ALIGN_CENTER);
table.addCell(cell);

table.addCell("Transaction Date");
table.addCell("Ref No");
table.addCell("Description");
table.addCell("Debit");
table.addCell("Credit");
table.addCell("Running Balance"); //added by K.I as requested by TB

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
                
org.w3c.dom.Element el = (org.w3c.dom.Element) nl.item(i); 
//String desc = com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "TRN_DESC");
String test =   com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "DRCR_IND") ; 
String accy =  com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AC_CCY") ;       
//System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "RUNNING_BALANCE"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "TXN_INIT_DATE"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "REFNO"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "TRN_DESC"));

if(test.equals("D")){
    if(accy.equals("NGN")){
    
table.addCell( formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "LCY_AMOUNT"))));
    }else{
 table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "FCY_AMOUNT"))));   
    }
}else{
  table.addCell("");  
}
if(test.equals("C")){
    if(accy.equals("NGN")){
        System.out.println("About to format");
table.addCell(formatter.format(Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "LCY_AMOUNT"))));
    }else{
 table.addCell(formatter.format(Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "FCY_AMOUNT"))));   
    }
}else{
  table.addCell("");  
}
System.out.println("About to format2");
table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "RUNNING_BALANCE")))); //added by K.I,as requested by tb, suspected to be previously commented out


               
                
            }
        }






document.add(table); 
document.close(); 

DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
byte[] bytes = buffer.toByteArray();
response.setContentLength(bytes.length);
for(int i = 0; i < bytes.length; i++)
{
dataOutput.writeByte(bytes[i]);
}
response.getOutputStream().flush();
}catch(DocumentException e){
e.printStackTrace();
}


  /*          } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    callable.close();
                    connection.close();
                } catch (Exception e) {
                }
            }*/
%>