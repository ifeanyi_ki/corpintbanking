
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<% response.setHeader("Content-Disposition", "attachment; filename=\"Statement.pdf\""); %> 
<%@ page contentType="application/pdf" %>       
<%@page import="java.io.*,java.util.*,
java.text.*,
com.itextpdf.text.pdf.*,
com.itextpdf.text.*,com.fasyl.ebanking.util.*,org.w3c.dom.*,org.xml.sax.*"
%>
<%
//response.setContentType("application/pdf");
response.setHeader("Content-Disposition", "attachment; filename=\"Statement.pdf\"");
com.itextpdf.text.Document document = new com.itextpdf.text.Document();

 System.out.println("=============== inside 1008a ==================");
            java.sql.Connection connection = null;
            java.sql.CallableStatement callable = null;
            String data = null;
            String scn = request.getParameter("taskId") + ".jsp";
            //String acct =(String) session.getAttribute("acct_no");
            String acct=request.getParameter("acct_no");
            //acct = "008-810002-10-00-02";
           System.out.println(acct+"This is d acct");
            String startDate = request.getParameter("fldFromDate");
            String endDate = request.getParameter("fldToDate");
            String from = request.getParameter("fldFromAmount");
            String to = request.getParameter("fldToAmount");
            String drcr = request.getParameter("fldDrCr");
            System.out.println(acct+"This is d acct"+ " startdate " + startDate);
            try {
                connection = com.fasyl.ebanking.db.DataBase.getConnection();
                //System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
                callable = connection.prepareCall("{?=call FN_GET_TRX(?,?,?,?,?,?)}");
                callable.registerOutParameter(1, oracle.jdbc.OracleTypes.CLOB);
                callable.setString(2, acct);
                callable.setString(3, startDate == null ? "N" : startDate);
                callable.setString(4, endDate == null ? "N" : endDate);
                callable.setString(5, from == null ? "0" : from);
                callable.setString(6, to == null ? "0" : to);
                callable.setString(7, "S");
                callable.executeQuery();
                java.sql.Clob datas = callable.getClob(1);
                data=datas.getSubString(1, (int)datas.length());
                pageContext.setAttribute("data", data);
try{
    NumberFormat     formatter = new DecimalFormat("0.00");
 Hashtable<String, String> list   = new Hashtable<String, String>();
        org.w3c.dom.Document                  doc    = XMLUtilty.getStringAsdocument(data);
        org.w3c.dom.Element                   docEle = doc.getDocumentElement();
        NodeList                  nl     = docEle.getElementsByTagName("ROW");
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
PdfWriter.getInstance(document, buffer); 
document.open();
/*ServletContext context = getServletContext();
String path  = context.getRealPath("screens/scripts/resources/images/slcb_lion.GIF");
System.out.println("This is the contextpath : "+document.right(document.rightMargin()) + " y coord "+document.top());
    Image image = Image.getInstance(path);
          image.setAbsolutePosition(505,800);
            
            document.add(image) ; */   
document.add(new Paragraph());                  
document.add(Chunk.NEWLINE);
document.add(Chunk.NEWLINE);
//document.addTitle("Loan Repayment");              
PdfPTable table = new PdfPTable(5);
PdfPCell cell = new PdfPCell (new Paragraph ("Mini Statement"));
cell.setColspan(5);
cell.setRowspan(2);
cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
cell.setVerticalAlignment(Chunk.ALIGN_CENTER);
table.addCell(cell);

table.addCell("Transaction Date");
table.addCell("Ref No");
table.addCell("Description");
table.addCell("Debit");
table.addCell("Credit");
//table.addCell("Running Balance");

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
org.w3c.dom.Element el = (org.w3c.dom.Element) nl.item(i);  
String test =   com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "DRCR_IND") ; 
String accy =  com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AC_CCY") ;       
System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "RUNNING_BALANCE"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "TXN_INIT_DATE"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "REFNO"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "TRN_DESC"));
if(test.equals("D")){
    if(accy.equals("NGN")){
    
table.addCell( formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "LCY_AMOUNT"))));
    }else{
 table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "FCY_AMOUNT"))));   
    }
}else{
  table.addCell("");  
}
if(test.equals("C")){
    if(accy.equals("NGN")){
table.addCell(formatter.format(Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "LCY_AMOUNT"))));
    }else{
 table.addCell(formatter.format(Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "FCY_AMOUNT"))));
    }
}else{
  table.addCell("");  
}
//table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "RUNNING_BALANCE"))));


               
                
            }
        }






document.add(table); 
document.close(); 
System.out.println("before get outputstream");
DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
System.out.println("after get outputstream");
byte[] bytes = buffer.toByteArray();
response.setContentLength(bytes.length);
for(int i = 0; i < bytes.length; i++)
{
dataOutput.writeByte(bytes[i]);
}
response.getOutputStream().flush(); 
System.out.println("All done");
}catch(DocumentException e){
e.printStackTrace();
}


            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    callable.close();
                } catch (Exception e) {
                }
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
%>