


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<p>&nbsp;&nbsp;</p>

<div  class="tab-content" id="tab1">

    <div class="clear"></div>
    <fieldset>
        <!--<img src="./img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
        <img src="./img/bg-th-right.gif" width="7" height="7" alt="" class="right" />-->


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                          
                            <th class="first" style="width: auto"><bean:message key="label.TxnDate"/></th>
                            <th><bean:message key="label.Description"/></th>
                            <th><bean:message key="label.DebitAmount"/></th>
                            <th><bean:message key="label.CreditAmount"/></th>
                            <th>Running Balance</th>
                            
                        </tr>

                    </thead>

                    <tbody id="pageindex">
                       
   </tbody>
                </table>

            </fieldset>
       
 <div id="Pagination" class="pagination">
                        
                    </div>

            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>
<iframe id="secretIFrame" src="" style="display:none;visibility: hidden" ></iframe>

<div align="center">
 <input type="submit"  class="button" value="Export to pdf" name="Excel"  onClick="setFormStatementAction('pdf');">
    <input type="submit"  class="button" value="Export to Excel" name="submit" onclick="setFormStatementAction('excel');" >
    <input type="submit"  class="button" value="Export to RTF" onClick="setFormStatementAction('rtf');">

</div>
<div style="display: none">
     <table>
        <tbody id="hiddenresult">
            <c:set var="xml" value="${data}" />
            <x:parse varDom="doc" xml="${xml}" />

            <c:set var="count" value="0"/>

            <x:forEach var="row" select="$doc/ROWSET/ROW">

                <tr>
                    <td style="width: auto; white-space:nowrap;">
                        <x:set var="c" select="$row/TXN_INIT_DATE"/>
                          <c:set var="cd2"  >
                                <x:out select="$row/TXN_INIT_DATE"/>
                            </c:set>
                          <c:set var="vDate"  >
                               <x:out select="$row/VALUE_DT"/>
                           </c:set>
                       
                            <fmt:parseDate value="${vDate}" type="DATE" pattern="yyyy-MM-dd" var="fmtPDate"/>
                            <fmt:formatDate value="${fmtPDate}" var="fmtVDate" type="DATE" pattern="dd-MMM-yyyy"/>
                            
                            <fmt:parseDate value="${cd2}" type="DATE" pattern="yyyy-MM-dd" var="formatedDate"/>
                                               
   
                        <label><input name="refno" id="refno" type="radio" value="<x:out select="REFNO" />"  onclick="getTrxDetail(this.value,'1006a','<fmt:formatDate value="${fmtPDate}" type="DATE" pattern="dd-MMM-yyyy"/>');return false"/><fmt:formatDate value="${formatedDate}" type="DATE" pattern="dd-MMM-yyyy"/></label>
                        <input type="hidden" name="acno" id="acno" value="<x:out select="AC_NO" />"/>
                        <input type="hidden" name="taskId" id="taskId" value="1006a"/>
                        <input type="hidden" name="taskId" id="screenId" value="1"/>
                    </td>
              
                    <%--<td>
                    Ayomide commented this out as TBMB requested
                        <x:out select="$row/REFNO"/>
                    </td> --%>
                    <td>
                        <x:set var="c" select="$row/TRN_DESC"/>
                        <c:set var="check">
                            <x:out select="$c"/>
                        </c:set>

                        <x:choose >
                            <x:when select="$c">
                                <x:out select="$c"/>

                            </x:when>
                            <x:otherwise>
                                <x:out select=""/>
                            </x:otherwise>
                        </x:choose>
                    </td>
                    <td>
                        <x:set var="c" select="$row/DRCR_IND"/>
                         <x:set var="cy" select="$row/AC_CCY"/>
                        <c:set var="check">
                            <x:out select="$c"/>
                        </c:set>
                          <c:set var="currency">
                            <x:out select="$cy"/>
                        </c:set>

                        <c:choose>
                            <c:when test="${check=='D'}">
                                <c:if test="${currency=='NGN'}" >
                                       <c:set var="fomt">
                                    <x:out select="$row/LCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                </c:if>
                                  <c:if test="${currency!='NGN'}" >
                                     <c:set var="fomt">
                                    <x:out select="$row/FCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                </c:if>

                            </c:when>
                            <c:otherwise>
                                <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <x:set var="c" select="$row/DRCR_IND"/>
                        <x:set var="cy" select="$row/AC_CCY"/>
                        <c:set var="check">
                            <x:out select="$c"/>
                        </c:set>
                        
                         <c:set var="currency">
                            <x:out select="$cy"/>
                        </c:set>
                        <c:choose>
                            <c:when test="${check=='C'}">
                                <c:if test="${currency=='NGN'}" >
                                    
                                      <c:set var="fomt">
                                    <x:out select="$row/LCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                    
                                </c:if>
                                  <c:if test="${currency!='NGN'}" >
                                  
                                      <c:set var="fomt">
                                    <x:out select="$row/FCY_AMOUNT"/> 
                                    </c:set>
                                    <fmt:formatNumber value="${fomt}" type="currency" currencySymbol="" />
                                </c:if>

                            </c:when>
                            <c:otherwise>
                                <c:out value=""/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    
                    <td>
                        <x:out select="$row/RUNNING_BALANCE"/>
                    </td>
                    <%--<td> Ayomide Commented out value date
                         <c:out value="${fmtVDate}" />
                    </td>--%>
                       
                </tr>


            </x:forEach>
        </tbody>
    </table>
</div>


