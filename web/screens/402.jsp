

<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItemFactory"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<script type="text/javascript" src="js/datepicker.js"> </script>
<link href="./css/demo.css"       rel="stylesheet" type="text/css" />

<!-- Add the datepicker's stylesheet -->
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="screens/scripts/resources/scripts/simpla.jquery.configuration.js"></script>

<html xml:lang="en" lang="en">
    <head>
        <title></title>
        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="scripts/resources/css/reset.css" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="scripts/resources/css/style.css" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="scripts/resources/css/invalid.css" type="text/css" media="screen" />
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <link rel="stylesheet" type="text/css" href="../css/all.css">
    </head>
    <body dir='LTR'  class="WorkArea">

        <div align="center">
            <div class="content-box">

                <div class="content-box-header">

                    <h3><bean:message key="label.createbulletin"/></h3>

                    <ul class="content-box-tabs" >
                        <li><a onclick="return showAuth(402,'1');"  class="default-tab"><bean:message key="label.createbulletin"/></a></li> <!-- href must be unique and match the id of target div -->
                        <li><a  onclick="return viewmessage(4022,'402');"><bean:message key="label.viewbulletin"/></a></li>
                    </ul>
                    <div class="clear"></div>

                </div >
                <div class="content-box-content">
                    <div id="trx_detail" class="tab-content default-tab" style="border: none; padding: 0; margin: 0;">



                        <form id="frmmain" name="frmmain" enctype="multipart/form-data" action="402.jsp" method="post"style="border: none; padding: 0; margin: 0;">

                            <script type="text/javascript" src="scripts/supportsession.js"></script>
                            <script type="text/javascript" src="screens/scripts/jquery.form.js"></script>
                            <input type="hidden" name="fldMsgLangId"/>
                            <input type="hidden" name="fldMsgLangText"/>
                            <input type="hidden" name="fldMsgFromDate"/>
                            <input type="hidden" name="fldMsgToDate"/>
                            <input type="hidden" name="fldMsgIsCust"/>
                            <input type="hidden" name="fldMsg"/>
                            <input type="hidden" name="fldMsgSubj"/>
                            <input type="hidden" name="fldMsgUser"/>
                            <input type="hidden" name="fldsendTo" />

                            <p>
                                <label class="formcolumn-left"><bean:message key="label.lang"/><font color="red">*</font></label>
                                <select name="languageid" id="languageid" size="1" class="small-input">
                                    <option>ENG</option>
                                </select>

                            </p>
                            <p>
                                <label class="formcolumn-left " for="subject">Subject<font color="red">*</font></label>
                                <input class="text-input small-input" type="text" id="subject" name="subject" value="" />


                            </p>
                            <p>
                                <label class="formcolumn-left " for="msgvalidfrom"><bean:message key="label.bulletinactivedate"/><font color="red">*</font></label>
                                <input class="text-input small-input" type="text" id="msgvalidfrom" name="msgvalidfrom" value="" onfocus="pickDate('msgvalidfrom')" onmouseover="pickDate('msgvalidfrom');"  >


                            </p>
                            <p>
                                <label style="clear: both;" class="formcolumn-left " for="msgvalidto">Expired Date<font color="red">*</font></label>
                                <input class="text-input small-input" type="text" id="msgvalidto" name="msgvalidto" value="" onfocus="pickDate('msgvalidto')" onmouseover="pickDate('msgvalidto');"  >

                            </p>

                            <p>
                                <label class="formcolumn-left"><bean:message key="label.body"/><font color="red">*</font></label>
                                <textarea class="textarea wysiwyg" id="message" name="message" cols="80" rows="5"></textarea>

                            </p>

                            <p>
                                <label class="formcolumn-left " for="subject">Attachment<font color="red"></font></label>
                                <input class="text-input small-input" type="file" onchange="uploadTriggered('attach')" id="attach" name="attach"  size="50" />
                                <input type="hidden" id="sessId" name="sessId" value="<%=request.getSession().getId()%>">
                            </p>

                            <p>
                                <label class="formcolumn-left " for="subject">Attachment2<font color="red"></font></label>
                                <input class="text-input small-input" type="file" onchange="uploadTriggered('attach2')" id="attach2" name="attach2"  size="50" />

                            </p>

                            <p>
                                <label class="formcolumn-left " for="subject">Attachment3<font color="red"></font></label>
                                <input class="text-input small-input" type="file" onchange="uploadTriggered('attach3')" id="attach3" name="attach3"  size="50" />

                            </p>
                            <p>
                                <input type="button"  onclick="createMessage(4021,'402');" value="<bean:message key="label.createbuletin"/>" class="button">
                                <label class="formcolumn-left"></label>
                              <!-- <input type="button" onclick="return viewmessage(4022,'402');" value="<bean:message key="label.viewbulletin"/>" class="button ">
                               <label class="formcolumn-left"></label>-->
                                <input type="reset" onclick=" return clearForm();" value="<bean:message key="label.clearform"/>" class="button ">
                                <input type="hidden" id="custspecific" name="custspecific" value="N">
                            </p>
                            <!--<input type="hidden" id="custspecific" name="custspecific" value="N"/>-->

                            <input type="hidden" id="operationType" name="operationType" value="UPLOAD">
                        </form>
                        <iframe height="0" width="0" id="target_iframe" name="target_iframe">




                    </div>
                    <div  id="trx_detail"></div>
                </div>

            </div>
        </div>

    </body>


    <!-- Scriplet for Bulletin Attachment Upload Added By Rebecca see below --> 

    <%
        String operationType = request.getParameter("operationType");
        String fieldUploading = request.getParameter("fieldUploading");
        System.out.println("operationType: " + operationType);
        if (operationType == null); else if (operationType.equals("UPLOAD")) {
            boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
            if (!isMultipartContent) {
                System.out.println("You are not trying to upload");
                return;
            }
            System.out.println("You are trying to upload<br/>");

            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                List<FileItem> fields = upload.parseRequest(request);
                System.out.println("Number of fields: " + fields.size());
                Iterator<FileItem> it = fields.iterator();
                if (!it.hasNext()) {
                    System.out.println("No fields found");
                    return;
                }
                while (it.hasNext()) {
                    FileItem fileItem = it.next();
                    String fieldName = fileItem.getFieldName();
                    boolean isFormField = fileItem.isFormField();
                    if (isFormField); else if (fieldName.equalsIgnoreCase(fieldUploading)) {
                        System.out.println("UPLOAD FIELD FOUND >>>>>>>>");
                        String ext = fileItem.getName().substring(fileItem.getName().lastIndexOf(".") + 1);

                        try {
                            String project_path = request.getContextPath();//getRealPath("");
                            String Ripped_Folder = "attachmentFolder";
                            String folder = request.getSession().getId();
                            String filePath = project_path + "/" + Ripped_Folder + "/" + folder;
                            File f = new File(filePath);
                            if (!f.exists()) {
                                f.mkdirs();
                                System.out.println("folder created at : " + f.getAbsolutePath());
                            }
                            System.out.println("file: /" + fieldName + "." + ext);
                            java.io.FileOutputStream fout = new java.io.FileOutputStream(filePath + "/" + fieldName + "." + ext);
                            fout.write(fileItem.get());
                            fout.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }

            } catch (FileUploadException e) {
                e.printStackTrace();
            }

        }
    %>  
</html>
