
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html >
   
    <head>


    </head>

<body>
       <input type="hidden" name="trxtypes" id="trxtypes" value="${trxtype}"/>
     <input type="hidden" name="froms" id="froms" value="${from}"/>
     <input type="hidden" name="tos" id="tos" value="${to}"/>
  
<div  class="tab-content" id="tab2">
    <div class="content-box-header">

        <h3>Report Generator</h3>

        <div class="clear"></div>
`
    </div> <!-- End .content-box-header -->
    <form action="screens/1006d.jsp?" target="secretIFrame" method="POST" name="frmStmtInq">
    <br />
    <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        Select account, date range, transaction status and transfer type and click <b>SEARCH</b> to view report.
                    </div>
                </div>
    <br />
             <label><bean:message key="label.AccountCode"/></label>
                <select name="acct_no" id ="acct_no"  class="small-input">
                    <option value="" selected style="display:none">Select an account</option>
                    <c:set var="xml" value="${data}" />


                    <x:parse varDom="doc" xml="${xml}" />
                    <x:forEach var="row" select="$doc/ROWSET/ROW" >
                        <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                    </x:forEach>
                </select>
            </p>
            <p>
                <label><bean:message key="label.from"/></label>
                <input class="text-input small-input" type="text" id="from" name="from" onfocus="pickDate('from');" onmousedown="pickDate('from');" />
                <br /><small>Enter date(dd-mon-yyyy)</small>
            </p>

            <p>
                <label><bean:message key="label.to"/></label>
                <input class="text-input small-input" type="text" id="to" name="to" onfocus="pickDate('to');" onmouseover="pickDate('to');"  />
                 <br /><small>Enter date(dd-mon-yyyy)</small>
            </p>
                <p>
                <label>Transaction Status</label>
                <select id="trxtype" name="trxtype" class="text-input small-input">
                    <option selected style="display:none">Select transaction status</option>
                    <option value="PSDFT">Passed Fund Transfer</option>
                    <option value="FLDFT">Failed Fund Transfer</option>
                   
                </select>
               
            </p>
            <p>
                 <label>Transfer Type</label>
                  <select id="trftype" name="trftype" class="text-input small-input">
                  <option style="display:none">Select transfer type</option>
                   <option value="INTFT">Inter Bank Transfers</option>
                    <option value="LOCFT">Intra Bank Transfers</option>
                  </select>
            </p>
            <p>                              
                <input type="button" onclick="getFTReport('5003','5002');" value="<bean:message key="label.search"/>" class="button">
                <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
            </p>
            
            </form>
      
     
             <div id="trx_detail"  >

        </div>


</div>


</body>
<html>