

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%

    int timeout = session.getMaxInactiveInterval();
    response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
%>
<body width="630" height="360" >

    <div  class="tab-content" id="tab2" >
        <div class="content-box-header">

            <h3><bean:message key="label.beneficiarytype"/></h3>

            <div class="clear"></div>

        </div>
        <form Name="TxnFrm">
            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <!-- End .content-box-header -->
            <p>
                <label><bean:message key="label.selectbeneficiary"/><font color="red">*</font></label>
                <select name="selectbeneficiary" id ="selectbeneficiary" onchange="selBenType('601');" class="small-input">
                    <option value="" selected style="display:none">Select beneficiary type</option>
                    <x:forEach var="row" select="$doc/ROWSET/ROW">
                        <option value="<x:out select="$row/BEN_TYPE"/>"><x:out select="$row/BEN_DESC"/></option>
                    </x:forEach>
                </select>
            </p>




        </form>
        <div id="search_result">

        </div>
        <div id="searchresult">

        </div>
    </div>
</body>

<%
    {
    }
%>
