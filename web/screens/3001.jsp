<%-- 
    Document   : 3001
    Created on : 03-Sep-2011, 14:59:56
    Author     : baby
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Authorise Created User</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">List of Users</a></li> <!-- href must be unique and match the id of target div -->
						
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
						<div class="notification attention png_bg">
							<a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								Kindly approve or  reject each transaction by clicking the link "Approve" or "Reject"
							</div>
						</div>
        <table>
							
							<thead>
								<tr>
								   <th><input class="check-all" type="checkbox" /></th>
								   <th>Customer ID</th>
								   <th>Customer Name</th>
								   <th>Email</th>
								   <th>Date</th>
								   <th>Action</th>
								</tr>
								
							</thead>
						 
							<tfoot>
								<tr>
									<td colspan="6">
									
										
										<div class="pagination">
											
										</div> <!-- End .pagination -->
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>
						 
							<tbody>
								<tr>
									<td><input type="checkbox" /></td>
									<td></td>
									<td><a href="#" title="title"></a></td>
									<td></td>
									<td></td>
									<td>
										<!-- Icons -->
										 <a href="#" title="Approve">Approve</a>
										 <a href="#" title="Rejct">Reject</a> 
										 
									</td>
								</tr>
								
								
							</tbody>
							
						</table>
						
					</div>
    </body>
</html>
