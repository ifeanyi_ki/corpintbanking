<form name="frmmain" id="frmmain" method="post" action="/MainController" >

                <script type="text/javascript" src="scripts/supportsession.js"></script>
                <script type="text/javascript" src="screens/scripts/jquery.form.js"></script>
                <input type="hidden" name="fldMsgLangId"/>
                <input type="hidden" name="fldMsgLangText"/>
                <input type="hidden" name="fldMsgFromDate"/>
                <input type="hidden" name="fldMsgToDate"/>
                <input type="hidden" name="fldMsgIsCust"/>
                <input type="hidden" name="fldMsg"/>
                <input type="hidden" name="fldMsgSubj"/>
                <input type="hidden" name="fldMsgUser"/>
                <input type="hidden" name="fldsendTo" />

                    <p>
                        <label class="formcolumn-left"><bean:message key="label.lang"/><font color="red">*</font></label>
                        <select name="languageid" id="languageid" size="1" class="small-input">
                        <option>ENG</option>
                        </select>
                    </p>
                    <p>
                        <label class="formcolumn-left" for="subject">Subject<font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="subject" name="subject" value="" onfocus="pickDate('msgvalidfrom')" >
                    </p>
                    <p>
                        <label class="formcolumn-left" for="msgvalidfrom"><bean:message key="label.bulletinactivedate"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="msgvalidfrom" name="msgvalidfrom" value="" onfocus="pickDate('msgvalidfrom')" >
                    </p>
                        <p>
                        <label class="formcolumn-left" for="msgvalidto"><bean:message key="label.bulletinexpiredate"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="msgvalidto" name="msgvalidto" value="" onfocus="pickDate('msgvalidto')" >
                    </p>
           
                     <p>
                        <label class="formcolumn-left"><bean:message key="label.body"/><font color="red">*</font></label>
                        <textarea class="textarea wysiwyg" id="message" name="message" cols="80" rows="5"></textarea>
                    </p>
                    <p>
                        <input type="button"  onclick="return createMessage(4021,'402');" value="<bean:message key="label.createbuletin"/>" class="button">
                         <label class="formcolumn-left"></label>
                        <input type="button" onclick="return viewmessage(4022,'402');" value="<bean:message key="label.viewbulletin"/>" class="button ">
                        <label class="formcolumn-left"></label>
                        <input type="reset" onclick=" return clearForm();" value="<bean:message key="label.clearform"/>" class="button ">
                        <input type="hidden" id="custspecific" name="custspecific" value="N">
                    </p>
            </form>
