


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<form name="viewInboxMail" action="MainController" method="POST" style="border: none; padding: 0; margin: 0;">

    <input type="hidden" name="reqType" value="I"/>
    <input type="hidden" name="screenId" value="5142"/>
    <input type="hidden" name="taskId" value="51421"/>
    <input type="hidden" name="flg_box" value="I"/>

</form>





<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<fieldset>

    <form name="frmcontrol" action="MainController" method="post">
        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>

                            <th></th>
                            <th><bean:message key="label.subject"/></th>
                            <th><bean:message key="label.from"/></th>
                            <th><bean:message key="label.receive"/></th>
                            <th>Action</th>

                        </tr>

                    </thead>


                    <tbody id="pageindex">



                    </tbody>

                </table>
                <div id="Pagination">

                </div>
            </form>
        </fieldset>


        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">


    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>


<div style="display: none">
    <table>
        <tbody id="hiddenresult">
            <x:forEach var="row" select="$doc/ROWSET/ROW">

                <c:set var="count" value="${count+1}"/>
                <c:if  test="${count<=20}">




                    <tr>
                        <td><input type="radio" name="inboxRadio" onclick="viewMail(this.value)" value="<x:out  select="MSG_ID"/>"/></td>
                        <td>
                            <a onClick=""><x:out  select="TEXT_SUBJECT"/> </a>
                        </td>
                        <td>
                            <x:out select="FROM_NAME"/>
                        </td>
                        <td>
                            <x:out select="MSG_DATE"/>
                        </td>
                        <td>
                            <!-- Icons -->
                            <a href="#" title="delete" onclick="deletes('<x:out  select="MSG_ID"/>', '5141', '514')">delete</a>


                        </td>
                    </tr>


                </c:if>
            </x:forEach>
        </tbody>
    </table>
</div>

