<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

 <%

response.setDateHeader("Expires",
   System.currentTimeMillis(  ) + 0);

             response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");

  // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
  response.addHeader("Cache-Control", "post-check=0, pre-check=0");

  // Set standard HTTP/1.0 no-cache header.
  response.setHeader("Pragma", "no-cache");

  %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
    </head>
    <body>
      <c:set var="xml" value="${data}" />
    <x:parse varDom="doc" xml="${xml}" />
        <x:forEach var="row" select="$doc/ROWSET/ROW">
    <c:set var="cc" >
     <x:out select="$doc/ROWSET/ROW/SUCCESS" />
    </c:set>
    <c:if test="${cc!=null || cc!='' }">
        <fieldset>
        <table>

                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
                    <tr>

                        <td width="500" class="LblIPMand" height="20" nowrap> <x:out select="$doc/ROWSET/ROW/SUCCESS"/></td>
                    </tr>
                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
        </table>
        </fieldset>
</c:if>            <c:if test="${cc==''||cc==null}">
                         <fieldset>

      <table width="500" height="20">
                <tr>
                    <td width="122" class="LblIPMand" height="20"><bean:message key="label.error"/><font color="red">*</font></td>
                    <td width="" height="20"<td width="" height="20"><b><bean:message key="label.paswdeerrorgen"/></b></td>
                    <td></td>
                </tr>
                </tr>

            </table>
                         </fieldset>
    </c:if>
</x:forEach>

    </body>
</html>

