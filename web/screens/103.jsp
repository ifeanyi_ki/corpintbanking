
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<div  class="tab-content" id="tab2">
    <div class="content-box-header">

        <h3><bean:message key="label.searchUser"/></h3>

        <div class="clear"></div>

    </div> <!-- End .content-box-header -->
    <form action="" method="POST">


        <div class="notification attention png_bg">
            <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
            <div>
                Kindly enter Email OR User ID of customer and click search button
            </div>
        </div>

        <fieldset>

            <p>
                <label><bean:message key="label.email"/></label>
                <input class="text-input small-input" type="text" id="customerno" name="customerno" />
                <br /><small>Enter the customer email</small>
            </p>

            <p>
                <label><bean:message key="label.userid"/></label>
                <input class="text-input small-input" type="text" id="customername" name="customername" />
            </p>
            <p>
                <input type="button" onclick="validateSearch(document.getElementById('customerno').value,document.getElementById('customername').value,'1031','103');" value="<bean:message key="label.search"/>" class="button">
                <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
            </p>
        </fieldset>


        <input type="hidden" name="screenId" id="screenId" value="103"/>
        <input type="hidden" name="taskId" id="taskId" value="1031"/>
        <div id="searchresult">

        </div>
    </form>
</div>

<%



    {
    }
%>
