<%--
    Document   : 401
    Created on : Feb 7, 2011, 4:38:16 AM
    Author     : Administrator
--%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Application Error</title>
    </head>
    <body>
        <form method="POST" action="">
            <c:set var="xml" value="${data}" />
            <x:parse varDom="doc" xml="${xml}" />
            <c:set var="error">
                <x:out select="$doc/ROWSET/ROW/NODATA"/>
            </c:set>
            <c:set var="highcount">
                <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
            </c:set>
            <c:set var="lowcount">
                <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
            </c:set>

            <c:set var="size">
                <x:out select="$doc/ROWSET/ROW/SIZE"/>
            </c:set>
            <c:set var="increment" value="20" />

            <div  class="tab-content" id="tab1">
                <div class="content-box-header">

                    <h3>Fraud Report</h3>
                    <input type="hidden" id="screen_id" value="4031"/>
                    <div class="clear"></div>

                </div>
                <div class="clear"></div>
                <fieldset style="padding-top: 20px;">



                    <c:choose>
                        <c:when test="${error==null || error!='[]'|| error==' '}">
                            <table>

                                <thead>
                                    <tr>
                                        <th class="first"><bean:message key="label.trxid"/></th>
                                        <th><bean:message key="label.userid"/></th>
                                        <th><bean:message key="label.uname"/></th>
                                        <th><bean:message key="label.date"/></th>                                       
                                    </tr>

                                </thead>

                                <tfoot>
                                    <tr>
                                        <td colspan="6">


                                            <div class="pagination">
                                                <a href="#" title="First Page">&laquo; First</a><a href="#" title="Previous Page">&laquo; Previous</a>
                                            </div> <!-- End .pagination -->
                                            <div class="clear"></div>
                                        </td>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <c:set var="xml" value="${data}" />

                                    <c:set var="count" value="0"/>

                                    <x:forEach var="row" select="$doc/ROWSET/ROW">

                                        <c:set var="count" value="${count+1}"/>
                                        <c:if  test="${count<=20}">

                                            <tr>
                                                <td>
                                                    <x:set var="c" select="$row/TXN_SEQ"/>
                                                    <input id="txnid" name="txnid" type="radio" value="<x:out select="TXN_SEQ" />" onclick="showSiDetail(3412,3411);"/><x:out select="$c" /></label>

                                                </td>

                                                <td>
                                                    <x:set var="check" select="$row/COD_USR_ID"/>
                                                    <c:set var="check">
                                                        <x:out select="$c"/>
                                                    </c:set>

                                                    <c:choose>
                                                        <c:when test="${check!=null}">
                                                            <x:out select="$row/COD_USR_ID"/>

                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:out value=""/>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </td>
                                                <td>
                                                    <x:set var="c" select="$row/COD_USR_NAME"/>
                                                    <c:set var="check">
                                                        <x:out select="$c"/>
                                                    </c:set>

                                                    <c:choose>
                                                        <c:when test="${check!=null}">
                                                            <x:out select="$row/COD_USR_NAME"/>

                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:out value=""/>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>
                                                
                                                
                                                <td>
                                                    <x:set var="c" select="$row/LAST_FAILED_DATE"/>
                                                    <c:set var="check">
                                                        <x:out select="$c"/>
                                                    </c:set>

                                                    <c:choose>
                                                        <c:when test="${check!=null}">
                                                            <x:out select="$row/LAST_FAILED_DATE"/>

                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:out value=""/>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </td>                                                

                                            </tr>
                                        </tbody>
                                    </c:if>
                                </x:forEach>

                            </table>

                        </fieldset>
                        <table>
                            <tr>

                                <c:if test="${lowcount>0}">
                                    <td>
                                        <div align="left">

                                            <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                                        </div>
                                    </td>
                                </c:if>
                        </table>
                        <c:if test="${size>increment && highcount<size}">


                            <div align="center">

                                <input type="submit" name="Submit" value="next" onClick="getNextValue(3411,'341');" class="button">

                            </div>


                        </c:if>


                        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
                        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
                        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
                        <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
                        <input type="hidden" name="instnos" id="instnos" value="<x:out select="$doc/ROWSET/ROW/INSTRUCTION_NO" />"/>

                        <c:set var="acct" value="${acct}" />
                        <input type="hidden" name="acctno" id="acctno" value="<c:out value="${acct}"/>">
                        <input type="hidden" name="check" id="check" value="${drcr}">
                        <input type="hidden" name="startDate" id="startDate" value="${startDate}">
                        <input type="hidden" name="endDate" id="endDate" value="${endDate}">
                        <input type="hidden" name="from" id="from" value="${from}">
                        <input type="hidden" name="to" id="to" value="${to}">

                        <input type="hidden" name="taskId" id="taskId" value="3411"/>
                        <input type="hidden" name="detail" id="detail" value="true"/>
                        <input type="hidden" id="screen_id" value=3411""/>
                    </div>
                </c:when>
                <c:otherwise>
                    <table>
                        <tr>
                            <td>There  is no data</td>
                        </tr>
                    </table>
                </c:otherwise>

            </c:choose>
        </form>
    </body>
</html>
