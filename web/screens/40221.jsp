
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta Http-Equiv="Cache-Control" Content="no-cache">
<meta Http-Equiv="Pragma" Content="no-cache">
<meta Http-Equiv="Expires" Content="0">
<meta Http-Equiv="Pragma-directive: no-cache">
<meta Http-Equiv="Cache-directive: no-cache">
        
    </head>
<form name="frmmain" id="frmmain" method="post" action="/MainController" >


    <script type="text/javascript" src="scripts/supportsession.js"></script>
    <script type="text/javascript" src="screens/scripts/jquery.form.js"></script>
    <input type="hidden" name="fldMsgLangId"/>
    <input type="hidden" name="fldMsgLangText"/>
    <input type="hidden" name="fldMsgFromDate"/>
    <input type="hidden" name="fldMsgToDate"/>
    <input type="hidden" name="fldMsgIsCust"/>
    <input type="hidden" name="fldMsg"/>
    <input type="hidden" name="fldMsgSubj"/>
    <input type="hidden" name="fldMsgUser"/>
    <input type="hidden" name="fldsendTo" />
    <c:set var="xml" value="${data}" />
    <x:parse varDom="doc" xml="${xml}" />
    <x:forEach var="row" select="$doc/ROWSET/ROW">
        <p>
            <label class="formcolumn-left"><bean:message key="label.lang"/><font color="red">*</font></label>
            <select name="languageid" id="languageid" size="1" class="small-input">
                <option>ENG</option>
            </select>

        </p>
        <p>
            <label class="formcolumn-left " for="subject">Subject<font color="red">*</font></label>
            <input class="text-input small-input" type="text" id="subject" name="subject" value=" <x:out select="TITTLE"/>" disabled =""/>


        </p>
        <p>
            <label class="formcolumn-left " for="msgvalidfrom">Active Date</label>
            <c:set var="cd2"  >
                <x:out select="ACTIVE_DATE"/>
            </c:set>
            <fmt:parseDate value="${cd2}" type="DATE" pattern="yy-MM-dd"                var="formatedDate"/>
            <input class="text-input small-input" type="text" id="msgvalidfrom" name="msgvalidfrom" value="<fmt:formatDate value='${formatedDate}' type='DATE' pattern='dd-MMM-yyyy'/>" disabled =""/>


        </p>
        <p>
            <label class="formcolumn-left " for="msgvalidto">Exp. Date<font color="red">*</font></label>
                <c:set var="cd2"  >
                    <x:out select="EXPIRE_DATE"/>
                </c:set>
                <fmt:parseDate value="${cd2}" type="DATE" pattern="yy-MM-dd"                var="formatedDate"/>

            <input class="text-input small-input" type="text" id="msgvalidto" name="msgvalidto" value="<fmt:formatDate value='${formatedDate}' type='DATE' pattern='dd-MMM-yyyy'/> " disabled =""/>


        </p>

        <p>
            <label class="formcolumn-left"><bean:message key="label.body"/><font color="red">*</font></label>
            <textarea class="textarea wysiwyg" id="message" name="message" cols="80" rows="5" value="<x:out select="BODY"/>" disabled=""><x:out select="BODY"/></textarea>

        </p>
        <p>
          
            <a href="#" onclick="window.open('screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT.<x:out select="IMGEXT"/>?dummy=<%= new java.util.Date().getTime() %>',
                'ATTACHMENT','location=1,status=1,scrollbars=1,width=800,height=800')">
                <img src="screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT.<x:out select="IMGEXT"/>?dummy=<%= new java.util.Date().getTime() %>" class="logo" width="38" height="38" border="0" /> 
            </a>
            <a href="#" onclick="window.open('screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT2.<x:out select="IMGEXT2"/>?dummy=<%= new java.util.Date().getTime() %>',
                'ATTACHMENT2','location=1,status=1,scrollbars=1,width=800,height=800')">
                <img src="screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT2.<x:out select="IMGEXT2"/>?dummy=<%= new java.util.Date().getTime() %>" class="logo"  width="38" height="38" border="0" /> 
            </a>
              <a href="#" onclick="window.open('screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT3.<x:out select="IMGEXT3"/>?dummy=<%= new java.util.Date().getTime() %>',
                'ATTACHMENT3','location=1,status=1,scrollbars=1,width=800,height=800')">
                <img src="screens/attachmentFolder/<%= request.getSession().getId() %>/ATTACHMENT3.<x:out select="IMGEXT3"/>?dummy=<%= new java.util.Date().getTime() %>" class="logo" width="38" height="38" border="0" />
            </a>
        </p>
       
       

    </x:forEach>

  <!-- <input type="button"  onclick="return createMessage(4021,'402');" value="<bean:message key="label.createbuletin"/>" class="button">
    <label class="formcolumn-left"></label>
   <input type="button" onclick="return viewmessage(4022,'402');" value="<bean:message key="label.viewbulletin"/>" class="button ">
   <label class="formcolumn-left"></label>
   <input type="reset" onclick=" return clearForm();" value="<bean:message key="label.clearform"/>" class="button ">
   <input type="hidden" id="custspecific" name="custspecific" value="N">-->
</form>

</html>