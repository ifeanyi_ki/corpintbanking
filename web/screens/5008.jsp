
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<html >

    <head>


    </head>

    <body>
        <input type="hidden" name="acct_nos" id="acct_nos" value="${acct_no}"/>
        <input type="hidden" name="trxtypes" id="trxtypes" value="${trxtype}"/>
        <input type="hidden" name="froms" id="froms" value="${from}"/>
        <input type="hidden" name="tos" id="tos" value="${to}"/>

        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3>Report Generator</h3>

                <div class="clear"></div>
                `
            </div> <!-- End .content-box-header -->

            <form action="screens/1006d.jsp?" target="secretIFrame" method="POST" name="frmStmtInq">
                <div class="notification attention png_bg">
                    <a href="#" class="close"><img src="screens/scripts/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
                    <div>
                        Enter user ID, select date range and click <b>SEARCH</b> to view report.
                    </div>
                </div>
                <br /><br /><label>User ID<font color="red">*</font></label>
                <input type="text" id="customer_id" class="text-input small-input" name="customer_id"   size="30" value="">

                </p>
                <p>
                    <label><bean:message key="label.from"/></label>
                    <input class="text-input small-input" type="text" id="from" name="from" onfocus="pickDate('from');" onmousedown="pickDate('from');" />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>

                <p>
                    <label><bean:message key="label.to"/></label>
                    <input class="text-input small-input" type="text" id="to" name="to" onfocus="pickDate('to');" onmouseover="pickDate('to');"  />
                    <br /><small>Enter date(dd-mon-yyyy)</small>
                </p>





                <p>
                    <input type="button" onclick="getGeneralReport('50081', '5008');" value="<bean:message key="label.search"/>" class="button">
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
                </p>

            </form>


            <div id="trx_detail"  >

            </div>


        </div>


    </body>
    <html>