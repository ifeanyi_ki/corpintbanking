
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="application/pdf" %>       
<%@
page import="
java.io.*,
java.util.*,
java.text.*,
com.itextpdf.text.pdf.*,
com.itextpdf.text.*,com.fasyl.ebanking.util.*,org.w3c.dom.*,org.xml.sax.*,com.fasyl.ebanking.logic.GetLoanInfo"
%>
<% response.setHeader("Content-Disposition", "attachment; filename=\"loan.pdf\""); %> 
<%
 
//response.setContentType("application/pdf");
com.itextpdf.text.Document document = new com.itextpdf.text.Document();

 System.out.println("=============== inside 2053 ==================");
           java.sql.Connection connection = null;
            java.sql.CallableStatement callable = null;
            String data = null;
            String ref = request.getParameter("ref");
            System.out.println(" This is the ref for the loan "+ref);
           data = (new GetLoanInfo()).getLoanRepayment(ref);
    
try{
    NumberFormat     formatter = new DecimalFormat("0.00");
 Hashtable<String, String> list   = new Hashtable<String, String>();
        org.w3c.dom.Document                  doc    = XMLUtilty.getStringAsdocument(data);
        org.w3c.dom.Element                   docEle = doc.getDocumentElement();
        NodeList                  nl     = docEle.getElementsByTagName("ROW");
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
PdfWriter.getInstance(document, buffer); 
document.open();
ServletContext context = getServletContext();
String path  = context.getRealPath("screens/scripts/resources/images/logo.PNG");
System.out.println("This is the contextpath : "+document.right(document.rightMargin()) + " y coord "+document.top());
    Image image = Image.getInstance(path);
          image.setAbsolutePosition(505,800);
            //document.add(new Paragraph());
            document.add(image) ;           
document.add(Chunk.NEWLINE);
document.add(Chunk.NEWLINE);
//document.addTitle("Loan Repayment");              
PdfPTable table = new PdfPTable(5);
PdfPCell cell = new PdfPCell (new Paragraph ("Loan Repayment"));
cell.setColspan(5);
cell.setRowspan(2);
cell.setHorizontalAlignment(Chunk.ALIGN_CENTER);
cell.setVerticalAlignment(Chunk.ALIGN_CENTER);
table.addCell(cell);
table.addCell("Component Name");
table.addCell("Due Date");
table.addCell("Amount Due");
table.addCell("Amount Settled");
table.addCell("Amount Overdue");

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
                
org.w3c.dom.Element el = (org.w3c.dom.Element) nl.item(i); 
      
System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "COMPONENT_NAME"));
table.addCell(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "COMPONENT_NAME"));   
table.addCell( com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "SCHEDULE_DUE_DATE"));
System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "SCHEDULE_DUE_DATE"));
table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AMOUNT_DUE"))));
System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AMOUNT_DUE"));
table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AMOUNT_SETTLED"))));
table.addCell(formatter.format( Double.parseDouble(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, "AMOUNT_OVERDUE"))));
         
                
            }
        }






document.add(table); 
document.close(); 

DataOutput dataOutput = new DataOutputStream(response.getOutputStream());
byte[] bytes = buffer.toByteArray();
response.setContentLength(bytes.length);
for(int i = 0; i < bytes.length; i++)
{
dataOutput.writeByte(bytes[i]);
}

}catch(DocumentException e){
e.printStackTrace();
}


  /*          } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    callable.close();
                    connection.close();
                } catch (Exception e) {
                }
            }*/
%>