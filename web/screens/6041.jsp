


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%

    int timeout = session.getMaxInactiveInterval();
    response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
%>
<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<div  class="tab-content" id="tab1">
    <div class="content-box-header">

        <h3><bean:message key="label.listben"/></h3>

        <div class="clear"></div></div>
    <fieldset>
        <legend></legend>


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                            <th class="first"><bean:message key="label.beneficiaryid"/></th>
                            <th><bean:message key="label.beneficiaryname"/></th>
                            <th><bean:message key="label.beneficiaryacct"/></th>
                            <th><bean:message key="label.beneficiarytype"/></th>
                            <th><bean:message key="label.amount"/></th>
                        </tr>

                    </thead>

                    <tfoot>
                        <tr><td>
                                <div id="Pagination" class="pagination">

                                </div>
                            </td></tr>
                    </tfoot>
                    <tbody>


                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <c:set var="count" value="${count+1}"/>
                            <c:if  test="${count<=20}">
                                <tr>
                                    <td>
                                        <x:set var="c" select="$row/BENEFICIARY_ID"/>
                                        <label><input name="refno" id="beneficiaryids" type="radio" value="<x:out select="$row/DESTINATION_ACCT"/>"  onclick="getBeneficiaryDetail2(this.value, '60411', '6041', '<x:out select="$row/BENEFICIARY_ID" />');
                                                return false"/><x:out select="$c" /></label>
                                        <input type="hidden" name="beneficiaryid" id="beneficiaryid" value="<x:out select="$row/BENEFICIARY_ID" />"/>


                                    </td>
                                    <td>
                                        <x:set var="c" select="$row/BENEFICIARY_NAME"/>
                                        <c:set var="check">
                                            <x:out select="$c"/>
                                        </c:set>

                                        <x:choose >
                                            <x:when select="$c">
                                                <x:out select="$c"/>

                                            </x:when>
                                            <x:otherwise>
                                                <x:out select=""/>
                                            </x:otherwise>
                                        </x:choose>
                                    </td>
                                    <td>
                                        <input type="hidden" name="beneficiaryacct" id="beneficiaryacct" value="<x:out select="$row/DESTINATION_ACCT"/>"/>
                                        <x:out select="$row/DESTINATION_ACCT"/>

                                    </td>
                                    <td>
                                        <x:out select="$row/BENEFICIARY_TYPE"/>
                                    </td>

                                    <td>
                                        <x:out select="$row/AMOUNT"/>
                                    </td>
                                </tr>
                            </tbody>
                        </c:if>
                    </x:forEach>

                </table>

            </fieldset>
            <table>
                <tr>

                    <c:if test="${lowcount>0}">
                        <td>
                            <div align="left">

                                <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                            </div>
                        </td>
                    </c:if>
            </table>
            <c:if test="${size>increment && highcount<size}">


                <div align="center">

                    <input type="submit" name="Submit" value="next" onClick="getNextValue(1011, '101');" class="button">

                </div>


            </c:if>


            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>




