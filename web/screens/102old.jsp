

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<form Name="TxnFrm">

    <body width="630" height="360" >
        <table width="592" scroll="No" bgcolor="transparent">
            <td width="592">
                <table width="592">
                    <tr>
                        <td width="592" class="formHeading" height="20"><bean:message key="label.CreateUser"/></td>
                    <input type="hidden" id="screen_id" value="2001"/>
                    </tr>
                </table>
                <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.userid"/><font color="red">*</font></td>
                        <td width="" height="20"><input type="text" id="user"  name="user" keyf="Y"   value="" onblur=" return checkmandatory(this.id,this.name,this.value);"></td>
                        <td></td>
                    </tr>
                </table>

                <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.password"/><font color="red">*</font></td>
                        <td width="" height="20"><input type="password" id="user_password" class="DataLeftAligned" name="user_password" valid="Y"  value="" onblur=" return checkmandatory2(this.id,this.name,this.value);"></td>
                    </tr>
                </table>
                <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.uname"/><font color="red">*</font></td>
                        <td width="" height="20"><input type="text" id="user_name"  name="username" keyf="Y"   value="" onblur=" return checkmandatory(this.id,this.name,this.value);"></td>
                        <td></td>
                    </tr>
                </table>
                <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.lang"/><font color="red">*</font></td>
                        <td width="" height="20">
                            <select id="user_lang"  name="language">
                                <option value="ENG">ENG</option>
                                <option value="FRA">FRA</option>
                                <option value="POR">POR</option>
                            </select>
                        <td></td>
                    </tr>
                </table>
                <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.roleid"/><font color="red">*</font> </td>
                        <td width="" height="20">
                            <select name="cod_chrg_type" id ="cod_role_id">
                               <option value=""></option>
                                <%
                                java.util.ArrayList<String> list = new java.util.ArrayList<String>();

              list = (java.util.ArrayList<String>)request.getAttribute("rolelist");
              String sel = null;
                        for (int i = 0; i < list.size(); i++) {
                             %>
                            <%="<option value=" + (list.get(i)) + " " + sel + ">" + list.get(i) + "</option>"%>
                <%
                     }
                %>
                            </select>

                        </td>
                    </tr>
                </table>
                            <table width="500" height="20">
                    <tr>
                        <td width="122" class="LblIPMand" height="20"><bean:message key="label.email"/><font color="red">*</font></td>
                        <td width="" height="20"><input type="text" id="email"  name="email" keyf="Y"   value="" onblur=" return checkmandatory(this.id,this.name,this.value);"></td>
                        <td></td>
                    </tr>
                </table>
                <table width="100%" height="20">
                    <tr>
                        <td align="center" height="20">
                            <input type="button" onclick="createUser();" value="<bean:message key="label.Submit"/>" class="buttons">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="buttons">

                    </tr>
                </table>

            </td>
        </table>
    </body>
</form>
<%


               
            {
                
            }
%>
