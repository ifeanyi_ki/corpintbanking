<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
      
    </head>
    <body>
        <c:set var="xml" value="${data}" />
        <x:parse varDom="doc" xml="${xml}" />
        <x:forEach var="row" select="$doc/ROWSET/ROW">
            <c:set var="cc" >
                <x:out select="$doc/ROWSET/ROW/SUCCESS" />
            </c:set>
            <c:if test="${cc!=null || cc!='' }">
                <!-- <fieldset> //commented out by K.I to display IBFT receipt -->
                <!--<table>-->

                <!--<tr>-->
                <!--<td> &nbsp;&nbsp;</td>-->
                <!--</tr>-->
                <!--<tr>-->

                        <!--<td width="500" class="LblIPMand" height="20" nowrap> <x:out select="$doc/ROWSET/ROW/SUCCESS"/></td>-->
                <!--</tr>-->
                <!--<tr>-->
                <!--<td> &nbsp;&nbsp;</td>-->
                <!--</tr>-->
                <!--</table>-->
                <!--</fieldset>-->
                <x:out select="$doc/ROWSET/ROW/SUCCESS" />
                <form method="post" id='IBFT_receipt'>
                    <br/><br/>
                    <table cellspacing='6'>
                        <tr>
                            <td>Customer Account </td> 
                            <td>
                                <c:set var="accno">
                                    <x:out select="$doc/ROWSET/ROW/CUSTACC"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/CUSTACC"/>
                                <input type="text" hidden="true" value="${accno}" name="accno"/>  <!--custacc-->
                            </td>
                        </tr>
                        <tr>
                            <td>Beneficiary Account </td> 
                            <td>
                                <c:set var="benAcc">
                                    <x:out select="$doc/ROWSET/ROW/BENACC"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/BENACC"/>
                                <input type="text" hidden="true" value="${benAcc}" name="benacc"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Beneficiary Bank </td> 
                            <td>
                                <c:set var="benBank">
                                    <x:out select="$doc/ROWSET/ROW/BENBANK"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/BENBANK"/>
                                <input type="text" hidden="true" value="${benBank}" name="benbank"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Beneficiary Account Name </td> 
                            <td>
                                <c:set var="benAccName">
                                    <x:out select="$doc/ROWSET/ROW/BENACCNAME"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/BENACCNAME"/>
                                <input type="text" hidden="true" value="${benAccName}" name="benaccname"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Amount</td> 
                            <td>
                                <c:set var="amount">
                                    <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                                <input type="text" hidden="true" value="${amount}" name="amount"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Narration</td> 
                            <td>
                                <c:set var="remarks">
                                    <x:out select="$doc/ROWSET/ROW/REMARKS"/>
                                </c:set>
                                <x:out select="$doc/ROWSET/ROW/REMARKS"/>
                                <input type="text" hidden="true" value="${remarks}" name="remarks"/>
                            </td>
                        </tr>
                    </table>

                    <c:set var="refno">
                        <x:out select="$doc/ROWSET/ROW/REFNO"/>
                    </c:set>
                    <input type="text" hidden="true" value="${refno}" name="refno"/>

                    <br/>
                    <br/>
                    <div align="center">
                        <input type="submit"  class="button" value="Download Receipt" name="IBFTreceipt"  onclick="setFormActionIBFTReceipt('pdf');">
                    </div>
                </form>
            </c:if>           







            <c:set var="xml" value="${data}" />
            <x:parse varDom="doc" xml="${xml}" />
            <x:forEach var="row" select="$doc/ROWSET" >
                <c:set var="cm" >
                    <x:out select="$doc/ROWSET/ACCOUNT_NUMBER" />
                </c:set>
                <c:if test="${cm!=null || cm!='' }">
                    <fieldset>
                        <table>

                            <tr>
                                <td> &nbsp;&nbsp;</td>
                            </tr>
                            <tr>

                                <td width="500" class="LblIPMand" height="20" nowrap> <x:out select="$doc/ROWSET/ACCOUNT_NUMBER"/></td>
                            </tr>
                            <tr>
                                <td> &nbsp;&nbsp;</td>
                            </tr>
                        </table>
                    </fieldset>
                </c:if>           

            </x:forEach> 






            <%--<c:if test="${cc==''||cc==null}">
                         <fieldset>
                             
                      <x:forEach var="row" select="$doc/ROWSET/">
                      <c:set var="cc" >
                     <x:out select="$doc/ROWSET/ROW/SUCCESS" />
                     </c:set>       
                             
                             
                              <c:if test="${cc!=null || cc!='' }">
        <fieldset>
        <table>

                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
                    <tr>

                        <td width="500" class="LblIPMand" height="20" nowrap> <x:out select="$doc/ROWSET/ROW/SUCCESS"/></td>
                    </tr>
                    <tr>
                        <td> &nbsp;&nbsp;</td>
                    </tr>
        </table>
        </fieldset>
</c:if>            <c:if test="${cc==''||cc==null}">
                         <fieldset>
            <%--
                  <table width="500" height="20">
                            <tr>
                                <td width="122" class="LblIPMand" height="20"><bean:message key="label.error"/><font color="red">*</font></td>
                                <td width="" height="20"<td width="" height="20"><b><bean:message key="label.paswdeerrorgen"/></b></td>
                                <td></td>
                            </tr>
                            </tr>

            </table>

                         </fieldset>
    </c:if>--%>
        </x:forEach>

    </body>
</html>

