

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>

    </head>

    <body>
        <c:set var="xml" value="${data}" />



        <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.modifieruser"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">
                
                <c:set var="custType">
                    <x:out select="$doc/ROWSET/ROW/COD_ROLE_ID" />
                </c:set>
                <x:forEach var="row" select="$doc/ROWSET/ROW">
                    <c:choose>
                        <c:when test="${custType == 'EXT_USER' || custType == 'CUS_ENQUI'}">
                            <p>
                                <label><bean:message key="label.custno"/><font color="red">*</font></label>
                                <input class="text-input small-input" type="text" id="custno" name="custno" value="<x:out select="$row/CUSTNO"/>" disabled/><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->

                                <input type="hidden" id="custno"  name="custno"   value="<x:out select="$row/CUSTNO"/>" >
                            </p>
                        </c:when>
                    </c:choose>
                    

                    <p>
                        <label><bean:message key="label.userid"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text"  value="<x:out select="$row/USERID"/>" maxlength="10" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input class="text-input small-input" type="hidden" id="user" name="user" value="<x:out select="$row/USERID"/>" maxlength="10"  /><!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input type="hidden" id="userold"  name="userold" keyf="Y"   value="<x:out select="$row/USERID"/>"  >
                    </p>
                    <p>
                        <label><bean:message key="label.uname"/></label>
                        <input class="text-input small-input" type="text" id="user_name" name="user_name" value="<x:out select="$row/USERNAME"/>"  /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                        <input type="hidden" id="user_name_old"  name="user_name_old"  value="<x:out select="$row/USERNAME"/>"  >
                    </p>


                    <p>
                        <label><bean:message key="label.lang"/></label>
                        <input class="text-input small-input" type="text" id="lang" name="lang" value="<x:out select="$row/LANG"/>" disabled /><!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input type="hidden" id="lang"  name="lang"   value="<x:out select="$row/LANG"/>"
                    </p>
                    <p>
                        <label><bean:message key="label.roleid"/><font color="red">*</font> </label>
                        <input  type="hidden" id="cod_role_idold" name="cod_role_idold" value="<x:out select="$row/COD_ROLE_ID"  />" disabled>
                        <input class="text-input small-input" type="text" id="cod_role_idd" name="cod_role_idd" value="<x:out select="$row/COD_ROLE_ID"  />" disabled><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                    </p>
                    <!-- End .content-box-header -->
                    <p>
                        <c:set var="xml2" value="${others}" />


                        <x:parse varDom="doc2" xml="${xml2}" />
                        <label>Choose New Role<font color="red">*</font></label>
                        <select name="cod_role_id" id ="cod_role_id"  class="small-input">
                            <x:forEach var="rows" select="$doc2/ROWSET/ROW">
                                <c:set var="userType">
                                    <x:out select="$rows/USERTYPE"/>
                                </c:set>
                                <c:choose>
                                    <c:when test="${(userType == 'EXT_USER' || userType == 'CUS_ENQUI')
                                            && (custType == 'EXT_USER' || custType == 'CUS_ENQUI')}">
                                        <c:choose>
                                            <c:when test="${custType == userType}">
                                                <option selected="selected" value="<x:out select="$rows/USERTYPE"/>"><x:out select="$rows/DESCRIPTION"/></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="<x:out select="$rows/USERTYPE"/>"><x:out select="$rows/DESCRIPTION"/></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:when test ="${(userType == 'SYS_ADMIN' || userType == 'USER_ADMIN')
                                            && (custType == 'SYS_ADMIN' || custType == 'USER_ADMIN')}">
                                        <c:choose>
                                            <c:when test="${custType == userType}">
                                                <option selected="selected" value="<x:out select="$rows/USERTYPE"/>"><x:out select="$rows/DESCRIPTION"/></option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="<x:out select="$rows/USERTYPE"/>"><x:out select="$rows/DESCRIPTION"/></option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                </c:choose>
                            </x:forEach>
                        </select>
                    </p>



                    <p>
                        <label><bean:message key="label.email"/></label>
                        <!--<input class="text-input small-input" type="text"  name="email" value="<x:out select="$row/EMAIL"/>" />--> <!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input class="text-input small-input" type="text" id="email" name="email" value="<x:out select="$row/EMAIL"/>"  /><!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input type="hidden" id="emailold"  name="emailold" value="<x:out select="$row/EMAIL"/>" >
                    </p>
                    
                    <c:choose>
                        <c:when test="${custType == 'EXT_USER' || custType == 'CUS_ENQUI'}">
                            <p>
                                <label><bean:message key="label.phone"/></label>
                                <!--<input class="text-input small-input" type="text"  name="phone" value="<x:out select="$row/PHONE"/>"/> --> <!-- <span class="input-notification error png_bg">Error message</span>-->
                                <input class="text-input small-input" type="text" id="phone" name="phone" value="<x:out select="$row/PHONE"/>"  /><!-- <span class="input-notification error png_bg">Error message</span>-->
                                <input type="hidden" id="phoneold"  name="phoneold" value="<x:out select="$row/PHONE"/>" >
                            </p>
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" id="phone"  name="phone" value="" >
                        </c:otherwise>
                    </c:choose>
                    
                    <p>
                        <label><bean:message key="label.branchcode"/></label>
                        <input class="text-input small-input" type="text" id="branchname" name="branchname" value="<x:out select="$row/USERBRANCH"/>"/><!-- <span class="input-notification error png_bg">Error message</span>-->
                        <input type="hidden" id="branchcodeold"  name="branchcodeold"   value="<x:out select="$row/USERBRANCH"/>">
                    </p>


                    <p>
                        <c:set var = "flg_status">
                            <x:out select="$row/FLG_STATUS" />
                        </c:set>
                        <c:choose>
                            <c:when test="${flg_status == 'D'}" >
                                <input type="button" onclick="modifyUser('104111','10411','E');" value="Enable" class="button"/>
                            </c:when>
                            <c:when test="${flg_status == 'A'}" >
                                <input type="button" onclick="modifyUser('104111','10411','D');" value="Disable" class="button"/>
                                <input type="button" onclick="modifyUser('104111','10411','M');" value="Modify" class="button"/> 
                            </c:when>
                            <c:otherwise>
                                <input type="button" onclick="modifyUser('104111','10411','E');" value="Enable" class="button"/>
                                <input type="button" onclick="modifyUser('104111','10411','D');" value="Disable" class="button"/>
                                <input type="button" onclick="modifyUser('104111','10411','M');" value="Modify" class="button"/> 
                            </c:otherwise>
                        </c:choose>
                        
                        <input type="button" onclick="modifyUser('104111','10411','DE');" value="Delete" class="button"/> 
                        <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button"/>
                    </p>

                </x:forEach>

                <input type="hidden" name="actionenable" id="actionenable" value="Enable a user"/>
                <input type="hidden" name="actiondisable" id="actiondisable" value="Disable a User"/>
                <input type="hidden" name="actionmodify" id="actionmodify" value="Modify a User"/>
                <input type="hidden" name="actionmodify" id="actiondelete" value="Delete a User"/>


            </form>
        </div>


    </body>
</html>
