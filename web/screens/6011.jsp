


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>

    </head>
    <%

        int timeout = session.getMaxInactiveInterval();
        response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
    %>
    <body>


        <div  class="tab-content" id="tab2" align="">
            <div class="content-box-header">

                <h3>Individual Beneficiary</h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">

                <p>
                    <label><bean:message key="label.beneficiaryacct"/></label>
                    <input class="text-input small-input" type="text" id="beneficiaryacct" name="beneficiaryacct" value="" onblur="getBenAccountDetail('001', '60122');"  />

                <div id="crbranchcode"></div>
                <input type="hidden" value="" id="crbrcode" name="crbrcode"/>
                <input type="hidden" value="" id="ccy" name="ccy"/></p>

                <!-- <p>
                     <label><bean:message key="label.SourceAccount"/></label>
                     <input class="text-input small-input" type="text" id="SourceAccount" name="SourceAccount" value=""  />-->



                <p>
                    <label><bean:message key="label.beneficiaryname"/></label>
                    <input class="text-input small-input" type="text" id="beneficiaryname" name="beneficiaryname" value=""  />

                </p>


                <!-- <p>
                  <label><bean:message key="label.txnamount"/></label>-->
                <input class="text-input small-input" type="hidden" id="txnamount" name="txnamount" value="0"  />


                <p>
                    <label><bean:message key="label.institution"/></label>
                    <input class="text-input small-input" type="text" maxlength="100" id="institution" name="institution" value=""  />
                </p>
                <p>
                    <label><bean:message key="label.description"/></label>
                    <input class="text-input small-input" type="text" maxlength="100" id="description" name="description" value=""  />
                    <input class="text-input small-input" type="hidden" id="bentype" name="bentype" value="IND"  />
                    <input class="text-input small-input" type="hidden" id="beneficiarygrp" name="bentype" value="Individual"  />
                </p>
                <p>
                    <input type="button" onclick="addBeneficiaries(60111, '6011');" class="button" value="<bean:message key="label.addbeneficiary"/>">
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>


                <div id="ajaxload"></div>

            </form>
        </div>


    </body>
</html>
