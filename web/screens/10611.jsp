

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>

    </head>
    <body>
        <c:set var="xml" value="${data}"/>

        <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.removeaccount"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->

            <x:choose>
                <x:when select="$doc/ROWSET/ROW/NO_DATA_FOUND">
                    <p><x:out select="$doc/ROWSET/ROW/NO_DATA_FOUND" /></p>
                </x:when>
                <x:otherwise>
                    <form action="" method="POST">

                        <p>
                            <label><bean:message key="label.custno"/><font color="red">*</font></label>
                            <input class="text-input small-input" type="text" id="customernos" name="customernos" value="<x:out select="$doc/ROWSET/ROW/CUSTNO"/>" disabled /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                            <input class="text-input small-input" type="hidden" id="customerno" name="customerno" value="<x:out select="$doc/ROWSET/ROW/CUSTNO"/>"  />

                        </p>

                        <p>
                            <label><bean:message key="label.Account"/></label>
                            <select id="acct_no"  name="acct_no" >
                                <x:forEach select="$doc/ROWSET/ROW">
                                    <option value="<x:out select="CUST_ACCT"/>"><x:out select="CUST_ACCT"/></option>
                                </x:forEach>
                            </select>
                            <input type="hidden" name="userid" value="<x:out select="COD_USR_ID"/>" id="userid"/>
                            <input type="hidden" name="actionendesc" value="Remove an account from customer" id="actionendesc"/>
                        </p>

                        <p>
                            <input type="button" onclick="addaccount(106111,'10611','false','RA');" value="<bean:message key="label.Submit"/>" class="button">

                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                        </p>
                        
                    </form>
                    <input type="hidden" id="address"  name="address"   value="">
                    <input type="hidden" id="custno"  name="custno"   value="" >
                    <input type="hidden" name="screenId" id="screenId" value="102b"/>
                    <input type="hidden" name="taskId" id="taskId" value="1021"/>
                    <input type="hidden" id="user_id"  name="username"  value="<%=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getUserid()%>">
                </x:otherwise>
            </x:choose>

        </div>


    </body>
</html>
