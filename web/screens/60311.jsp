


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>

    </head>
    <%

        int timeout = session.getMaxInactiveInterval();
        response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
    %>
    <body>


        <div  class="tab-content" id="tab2" align="center">
            <div class="content-box-header">

                <h3><bean:message key="label.managebenficiary"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">


                <c:set var="xml" value="${data}" />



                <x:parse varDom="doc" xml="${xml}" />

                <x:forEach var="row" select="$doc/ROWSET/ROW">

                    <p>
                        <label><bean:message key="label.beneficiaryname"/></label>
                        <input class="text-input small-input" type="text" id="beneficiaryname" name="beneficiaryname" value="<x:out select="$row/BENEFICIARY_NAME"/>"  />
                        <input type="hidden" value="<x:out select="$row/BENEFICIARY_ID"/>" id="beneficiaryid" name="beneficiaryid">
                    </p>

                    <p>
                        <label><bean:message key="label.beneficiaryacct"/></label>
                        <input class="text-input small-input" type="text" id="beneficiaryacct" name="beneficiaryacct" value="<x:out select="$row/DESTINATION_ACCT"/>"  />
                        <input type="hidden" value="<x:out select="$row/DESTINATION_ACCT"/>" id="oldbeneficiaryacct">
                    </p>

                    <p>
                        <label><bean:message key="label.txnamount"/></label>
                        <input class="text-input small-input" type="text" id="txnamount" name="txnamount" value="<x:out select="$row/AMOUNT"/>"  />

                    </p>
                    <p>
                        <label><bean:message key="label.institution"/></label>
                        <input class="text-input small-input" type="text" id="institution" name="institution" value="<x:out select="$row/INSTITUTION_ID"/>"  />
                    </p>
                    <p>
                        <label><bean:message key="label.description"/></label>
                        <input class="text-input small-input" type="text" id="description" name="description" value="<x:out select="$row/DESCRIPTION"/>"  />
                    </p>
                    <p>
                        <label><bean:message key="label.beneficiarygrp"/></label>
                        <input class="text-input small-input" type="text" id="beneficiarygrp" name="beneficiarygrp" value="<x:out select="$row/BENEFTGRPNAME"/>"disabled  />
                        <input type="hidden"  name="oldbeneficiarygrp" id="oldbeneficiarygrp" value="<x:out select="$row/BENEFTGRPNAME"/>"/>
                        <input type="hidden" value="GRP" name="bentype" id="bentype"/>
                    </p>
                    <p>
                        <input    onclick="modifyBeneficiary('603111', '60311');" type="button"  class="button" name="Delete" value="delete"/>

                    </p>


                </x:forEach>


            </form>
        </div>


    </body>
</html>
