

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<head> <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp"></head>

<body width="630" height="360" >

    <div  class="tab-content" id="tab2" >
        <div class="content-box-header">

            <h3><bean:message key="label.siEnquiry"/></h3>
            <input type="hidden" id="screen_id" value=341""/>
            <div class="clear"></div>

        </div>
        <form Name="TxnFrm">
            <c:set var="xml" value="${data}" />


            <x:parse varDom="doc" xml="${xml}" />
            <!-- End .content-box-header -->
            <p>

                <label><bean:message key="label.AccountCode"/></label>
                <select name="acct_no" id ="acct_no" onchange="showSI(3411,341)" class="small-input">
                    <option value="" selected style="display:none">Select an account</option>
                    <c:set var="xml" value="${data}" />


                                <x:parse varDom="doc" xml="${xml}" />
                                <x:forEach var="row" select="$doc/ROWSET/ROW" >
                                    <option value="<x:out select="CUST_AC_NO" />"><x:out select="CUST_AC_NO" /></option>
                                </x:forEach>
                </select>
            </p>
                      <input type="hidden" name="instno" id="instno" value=" "/>
                     <input type="hidden" name="detail" id="detail" value="false"/>


        </form>
                 <Div id="trx_detail"></Div>
    </div>
</body>


