

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<html>
    <head>
            <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    </head>
    <body>


        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <c:set var="xml" value="${data}" />


                <x:parse varDom="doc" xml="${xml}" />



                <h3><bean:message key="label.fundtransfertittle"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">


                <p>
                    <label><bean:message key="label.sidebitaccount"/></label>
                    <input class="text-input small-input" type="text" id="ftdebitacc1" name="ftdebitacc1" value="<x:out select="$doc/ROWSET/ROW/DRACCT"/>" disabled/><!-- <span class="input-notification error png_bg">Error message</span>-->

                    <input type="hidden" name="ftdebitacc"  id="ftdebitacc"  value="<x:out select="$doc/ROWSET/ROW/DRACCT"/>"  >

                </p>
                <p>
                    <label><bean:message key="label.sicreditaccount"/></label>
                    <input class="text-input small-input" type="text" id="DestAcctNo1" name="DestAcctNo1" value="<x:out select="$doc/ROWSET/ROW/DESTACCTNO"/>"  disabled/><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                    <input type="hidden" name="benacct"  id="benacct" name="benacct" value="<x:out select="$doc/ROWSET/ROW/DESTACCTNO"/>" />
                </p>

                <p>
                    <label><bean:message key="label.ftcurrency"/></label>
                    <input class="text-input small-input" type="text" id="ccy" name="ccy" value="<x:out select="$doc/ROWSET/ROW/CCY"/>" disabled/><!-- <span class="input-notification error png_bg">Error message</span>-->

                    <input type="hidden" name="ccy"  id="ccy"  value="<x:out select="$doc/ROWSET/ROW/CCY"/>"  />

                </p>
                <p>
                    <label><bean:message key="label.ftamount"/></label>
                    <c:set var="realAmt">
                        <x:out select="$doc/ROWSET/ROW/AMOUNT"/>
                    </c:set>
                    <fmt:formatNumber type="number" value="${realAmt}" var="formattedAmt" />
                    <input class="text-input small-input" type="text" id="amt1" name="amt1" value="${formattedAmt}" disabled /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                    <input type="hidden" name="amt"  id="amt"  value="<x:out select="$doc/ROWSET/ROW/AMOUNT"/>" />
                    <input type="hidden" name="enterpin"  id="enterpin"  value="<x:out select="$doc/ROWSET/ROW/ACTION"/>" />
                </p>


                <p>
                    <label><bean:message key="label.narration"/></label>
                    <input class="text-input small-input" type="text" id="narration1" name="narration1" value="<x:out select="$doc/ROWSET/ROW/NARRATION"/>" disabled /><!-- <span class="input-notification success png_bg">Successful message</span> <!-- Classes for input-notification: success, error, information, attention -->
                    <input type="hidden" name="narration"  id="narration"  value="<x:out select="$doc/ROWSET/ROW/NARRATION"/>" />                         
                </p>


                <p>
                    <input type="button" onclick="bulktransfer(702110,'70211');" value="<bean:message key="label.Submit"/>" class="button"/>
                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                </p>





            </form>
        </div>
        <div id="ajaxload"></div>



    </body>
</html>
