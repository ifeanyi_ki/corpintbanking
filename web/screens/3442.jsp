

<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>


<html>
    <head>
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <script language="JavaScript" src="scripts/screens/common.js"/>
        <script language="JavaScript">


            //-----------------------------------------------------------------------------

            //-----------------------------------------------------------------------------
        </script>
    </head>
    <body>
        <c:set var="xml" value="${data}" />


        <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2">
            <div class="content-box-header">

                <h3><bean:message key="label.closesi"/></h3>

                <div class="clear"></div>

            </div> <!-- End .content-box-header -->
            <form action="" method="POST">
                <c:set var="chsourcecount">
                    <x:out select="$doc/ROWSET/ROW/ACCOUNT"/>
                </c:set>
                <c:choose>

                    <c:when test="($chsourcecount == '') or ($chsourcecount == null)">
                        <table width="90%" align="center" class="TableBorder">
                            <tr>
                                <td class="Warning">
                                    <bean:message key="label.noaccount"/>
                                </td>
                            </tr>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <script language="JavaScript">
                            l_boolean = true;
                        </script>
                        <x:forEach var="row" select="$doc/ROWSET/ROW">
                            <p>
                                <label><bean:message key="label.SIType"/></label>

                                <input class="text-input small-input" type="text" name="SiTypeDesc" value="PRODUCT_DESCRIPTION" disabled/>
                            </p>
                            <p>
                                <label><bean:message key="label.SISourceAcct"/></label>
                                <input class="text-input small-input" type="text" name="SiTypeDesc" value="<x:out select="$doc/ROWSET/ROW/DR_ACCOUNT"/>" disabled/>
                                <input type="hidden" name="acct_branch" id="acct_branch" value="<x:out select="$doc/ROWSET/ROW/DR_ACC_BR"/>"/>
                            </p>
                            <p>
                                <label><bean:message key="label.Destinationaccount"/><font color="red">*</font></label>
                                <input class="text-input small-input" type="text" name ="DestAcctNo" id="DestAcctNo"  value="<x:out select="$doc/ROWSET/ROW/CR_ACCOUNT"/>" disabled>

                            </p>

                            <p>
                                <label><bean:message key="label.TransferAmount"/></label>
                                <input class="text-input small-input" type="text" name ="SiTrfAmt" id="SiTrfAmt"  value ="<x:out select="$doc/ROWSET/ROW//SI_AMT"/>" disabled />
                            </p>
                            <p>
                                <label><bean:message key="label.TransferCurrency"/></label>
                                <input  class="text-input small-input" type="text" name ="SiTrfAmt" id="SiTrfAmt"  value ="<x:out select="$doc/ROWSET/ROW/SI_AMT_CCY"/>" disabled/>
                            </p>


                            <p>
                                <label><bean:message key="label.FirstExecDate"/></label>
                                <input class="text-input small-input" type="text"  name ="FirstExecDate" id="FirstExecDate" value = '<x:out select="$doc/ROWSET/ROW/FIRST_EXEC_DATE"/>' disabled/>
                            </p>
                            <p>
                                <label><bean:message key="label.SIFinalDate"/></label>
                                <input class="text-input small-input" type="text"  name ="SiFinalDate" id="SiFinalDate" value = '<x:out select="$doc/ROWSET/ROW/SI_EXPIRY_DATE"/>' disabled/>
                            </p>

                            <p>
                                <input type="button"  class="button" value="CloseSI" onClick="closeSI(3443,'3442')" >
                                    <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button" >
                            <input type="hidden" name="instno" id="instnos" value="<x:out select="$doc/ROWSET/ROW/INSTRUCTION_NO" />"/>
                            </p>

                        </x:forEach>
                    </c:otherwise>
                </c:choose>



            </form>
        </div>


    </body>
</html>
