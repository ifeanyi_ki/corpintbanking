


<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<head><meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp"> </head>

<c:set var="xml" value="${data}" />
<x:parse varDom="doc" xml="${xml}" />
<c:set var="error">
    <x:out select="$doc/ROWSET/ROW/NODATA"/>
</c:set>
<c:set var="highcount">
    <x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>
</c:set>
<c:set var="lowcount">
    <x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>
</c:set>

<c:set var="size">
    <x:out select="$doc/ROWSET/ROW/SIZE"/>
</c:set>
<c:set var="increment" value="20" />

<div  class="tab-content" id="tab1">

    <div class="clear"></div>
    <fieldset>
        <!--<img src="./img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
        <img src="./img/bg-th-right.gif" width="7" height="7" alt="" class="right" />-->


        <c:choose>
            <c:when test="${error==null || error!='[]'|| error==' '}">
                <table>

                    <thead>
                        <tr>
                            <th class="first"><bean:message key="label.siInstructionno"/></th>
                            <th><bean:message key="label.sidebitaccount"/></th>
                            <th><bean:message key="label.sicreditaccount"/></th>
                            <th><bean:message key="label.siAmt"/></th>
                            <th>Frequency</th>

                        </tr>

                    </thead>

                    <tfoot>
                        <tr>
                            <td colspan="6">


                                <div class="Pagination" id="Pagination">
                                </div> <!-- End .pagination -->
                                <div class="clear"></div>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <c:set var="xml" value="${data}" />

                        <c:set var="count" value="0"/>

                        <x:forEach var="row" select="$doc/ROWSET/ROW">

                            <c:set var="count" value="${count+1}"/>
                            <c:if  test="${count<=20}">

                                <tr>
                                    <td>
                                        <x:set var="c" select="$row/INSTRUCTION_NO"/>
                                        <input id="instnos1" name="instnos1" type="radio" value="<x:out select="$row/INSTRUCTION_NO" />" onclick="showSiDetail2(this.value,3412,3411);"/><label><x:out select="$c" /></label>
                                        <input type="hidden" name="instnos" id="instnos" value="<x:out select="$row/INSTRUCTION_NO" />"/>
                                    </td>
                                    <td>
                                        <x:set var="c" select="$row/DR_ACCOUNT"/>
                                        <c:set var="check">
                                            <x:out select="$c"/>
                                        </c:set>

                                        <c:choose>
                                            <c:when test="${check!=null}">
                                                <x:out select="$row/DR_ACCOUNT"/>

                                            </c:when>
                                            <c:otherwise>
                                                <c:out value=""/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <x:set var="c" select="$row/CR_ACCOUNT"/>
                                        <c:set var="check">
                                            <x:out select="$c"/>
                                        </c:set>

                                        <c:choose>
                                            <c:when test="${check!=null}">
                                                <x:out select="$row/CR_ACCOUNT"/>

                                            </c:when>
                                            <c:otherwise>
                                                <c:out value=""/>
                                            </c:otherwise>
                                        </c:choose>

                                    </td>
                                    <td>
                                        <x:set var="c" select="$row/SI_AMT"/>
                                        <c:set var="check">
                                            <x:out select="$c"/>
                                        </c:set>

                                        <c:choose>
                                            <c:when test="${check!=null}">
                                                <x:out select="$row/SI_AMT"/>

                                            </c:when>
                                            <c:otherwise>
                                                <c:out value=""/>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <c:set var="exec_days">
                                        <x:out select="$row/EXEC_DAYS" />
                                    </c:set>
                                    <c:set var="exec_mths">
                                        <x:out select="$row/EXEC_MTHS" />
                                    </c:set>
                                    <c:set var="exec_yrs">
                                        <x:out select="$row/EXEC_YRS" />
                                    </c:set>
                                    <c:choose>
                                        <c:when test="${exec_days != null && exec_days == 1}">
                                            <c:set var="freq" value="Daily" />
                                        </c:when>
                                        <c:when test="${exec_days != null && exec_days == 7}">
                                            <c:set var="freq" value="Weekly" />
                                        </c:when>
                                        <c:when test="${exec_mths != null && exec_mths == 1}">
                                            <c:set var="freq" value="Monthly" />
                                        </c:when>
                                        <c:when test="${exec_mths != null && exec_mths == 3}">
                                            <c:set var="freq" value="Quarterly" />
                                        </c:when>
                                        <c:when test="${exec_mths != null && exec_mths == 6}">
                                            <c:set var="freq" value="Semi-Annually" />
                                        </c:when>
                                        <c:when test="${exec_yrs != null && exec_yrs == 1}">
                                            <c:set var="freq" value="Yealy" />
                                        </c:when>
                                    </c:choose>
                                    <td>
                                        <c:out value="${freq}" />
                                    </td>

                                </tr>
                            </tbody>
                        </c:if>
                    </x:forEach>

                </table>

            </fieldset>
            <table>
                <tr>

                    <c:if test="${lowcount>0}">
                        <td>
                            <div align="left">

                                <!--<input type="submit" name="Submit" value="&lt; Previous" onClick="javascript:history.go(-1)" class="buttons">-->

                            </div>
                        </td>
                    </c:if>
            </table>
            <c:if test="${size>increment && highcount<size}">


                <div align="center">

                    <input type="submit" name="Submit" value="next" onClick="getNextValue(3411,'341');" class="button">

                </div>


            </c:if>


            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/HIGHCOUNT"/>" id="highcount" name="highcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/LOWCOUNT"/>" id="lowcount" name="lowcount">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHEMAIL"/>" id="customernonext" name="customernonext">
            <input type="hidden" value="<x:out select="$doc/ROWSET/ROW/SEARCHNAME"/>" id="customername" name="customernamenext">
            

            <c:set var="acct" value="${acct}" />
            <input type="hidden" name="acctno" id="acctno" value="<c:out value="${acct}"/>">
            <input type="hidden" name="check" id="check" value="${drcr}">
            <input type="hidden" name="startDate" id="startDate" value="${startDate}">
            <input type="hidden" name="endDate" id="endDate" value="${endDate}">
            <input type="hidden" name="from" id="from" value="${from}">
            <input type="hidden" name="to" id="to" value="${to}">

            <input type="hidden" name="taskId" id="taskId" value="3411"/>
            <input type="hidden" name="detail" id="detail" value="true"/>
            <input type="hidden" id="screen_id" value=3411""/>
        </div>
    </c:when>
    <c:otherwise>
        <table>
            <tr>
                <td>There  is no data</td>
            </tr>
        </table>
    </c:otherwise>

</c:choose>




