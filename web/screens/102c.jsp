<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<html>
    <head>
        <title></title>
    </head>
    <body dir='LTR'  class="WorkArea" onload="Inits()">
     <c:set var="xml" value="${data}" />
 <x:parse varDom="doc" xml="${xml}" />

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3><bean:message key="label.CreateUser"/></h3>

                <div class="clear"></div>

            </div>
                <fieldset >
                    <legend></legend>
                     <x:forEach var="row" select="$doc/ROWSET/ROW">
            <form name="frmmain" id="frmmain" method="post" action="/MainController" >
              <input type="hidden" name="screenId" id="screenId" value="102c"/>
                    <input type="hidden" name="taskId" id="taskId" value="1021"/>
                    <div id="sub-content"><p>
                   <!-- <label class="formcolumn-left">Select Account</label>-->
                    <table>
                        
                        <thead>
                                <tr>
                                    <!--<th><input class="check-all" type="checkbox" /></th>-->
                                    <th>Select Account</th>
                                    <th>All Account</th>
                                    <th></th>
                                    <th>Selected Account</th>

                                </tr>

                            </thead>
                            <tbody>
                        <tr>
                            <td ></td>
            <td align=""><select id="removedaccountlist" name="removedaccountlist" size="10" multiple >
             <x:forEach var="rows" select="$doc/ROWSET/ACCOUNTS">
                        <option value="<x:out select="$rows/CUST_AC_NO"/>"><x:out select="$rows/CUST_AC_NO"/></option>
                    </x:forEach>
            </select></td>
            <td ><input type="button" name="Add" value="&gt;&gt;" onclick="addAccount()"/> <input type="button" name="Remove" value="&lt;&lt;" onclick="removeaccount()"></td>
            <td ><select id="addedaccountlist" name="addedaccountlist" size="10" multiple >

            </select></td>
            
          </tr>
                            </tbody>         </table></p>
                    </div>
                      <p>
                        <label ><bean:message key="label.custno"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="custno" name="custno" value="<x:out select="$row/CUSTOMER_NO"/>" disabled>
                         <input type="hidden" id="custno"  name="custno"   value="<x:out select="$row/CUSTOMER_NO"/>" >
                         <input type="hidden" class="DataLeftAligned" name="user_password" id="user_password"  value="password"  >
                    </p>
                           <p>
                        <label><bean:message key="label.userid"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="user" name="user" value="<x:out select="$doc/ROWSET/USER/USERID"/>" onblur="validateUser(this.value,'102c')" disabled />
                        <div id="crbranchcode"></div>
                    </p>

                           <p>
                        <label ><bean:message key="label.uname"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="username" name="user" value="<x:out select="$row/CUSTOMER_NAME1"/>" disabled >
                        <input type="hidden" id="user_name"  name="user_name" value="<x:out select="$row/CUSTOMER_NAME1"/>"  >
                        <input type="hidden" id="user_id"  name="username"  value="<%=((com.fasyl.ebanking.main.User)session.getAttribute("user")).getUserid()%>">
                    </p>

                <p>
                    <label><bean:message key="label.lang"/><font color="red">*</font></label>
                    <select name="languageid" id="user_lang"   size="1" class="small-input">
                        <option value="eng">English</option>
                               
                                
                    </select>

                </p>
                <p>
                        <label>Contact Email<font color="red">*</font></label>
                        <input class="text-input small-input" type="text" value="" id="contact" name="contact" onchange=" return echeck(this.value);"/>
                        <input type="hidden" value="<x:out select="$doc/ROWSET/ROLES/ROLE_TYPE"/>" name="role_type"  id="role_type"/>   
                    </p>
                 <p>
                        <label ><bean:message key="label.roleid"/><font color="red">*</font></label>
                        <select id="cod_role_id" name="cod_role_id">
                        <option></option>
                       <x:forEach var="role" select="$doc/ROWSET/ROLES">
                        <option value="<x:out select="$role/USERTYPE"/>"><x:out select="$role/DESCRIPTION"/></option>
                    </x:forEach>
                        </select
                    </p>
                      <p>
                        <label ><bean:message key="label.email"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="email" name="email" value="" onchange=" return echeck(this.value);" >

                    </p>
                    
                     <p>
                        <label ><bean:message key="label.phone"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" id="phone" name="phone number" value="">
                        <input type="hidden" id="phone"  name="phone"   value="<x:out select="$row/PHONE"/>">
                    </p>
                  

                     <p>
                        <label ><bean:message key="label.branchcode"/><font color="red">*</font></label>
                        <input class="text-input small-input" type="text" name="<x:out select="$row/BRANCH_NAME"/>" value="<x:out select="$row/BRANCH_NAME"/>" >
                        <input type="hidden" id="branchcode"  name="branchcode"   value="<x:out select="$row/BRANCH_NAME"/>">

                    </p>
                    
                     <p>
                        <label ><bean:message key="label.address"/><font color="red">*</font></label>
                        <textarea class="textarea wysiwyg" id="address" name="address" cols="80" rows="5" disabled>
                        <x:out select="$row/ADDRESS_LINE1"/><x:out select="$row/ADDRESS"/>

                        </textarea>
                     <input type="hidden" id="address"  name="address"   value="<x:out select="$row/ADDRESS_LINE1"/><x:out select="$row/ADDRESS_LINE2"/>">
                    </p>
                     </x:forEach>
                        <c:set var="xml" value="${data}" />
                      <x:parse varDom="doc" xml="${xml}" />
                    <p>
                    
                    </p>
                    <p>&nbsp;&nbsp;</p>
                    <p>
                        <label class="formcolumn-left"></label>
                        <input type="button" onclick="createUser('1021','102c');" value="<bean:message key="label.Submit"/>" class="button">
                            <input type="reset" onclick="resetme();" value="<bean:message key="label.cancel"/>" class="button">
                    </p>
            </form>

                   
                    </fieldset >
        </div>
    </body>
</html>