<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE htmlz
    <html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
    <title>Corporate List</title>
    <%

        int timeout = session.getMaxInactiveInterval();
        response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
    %>
</head>
<body id="background">
    <div  class="tab-content" id="tab2" align="left">
        <div class="content-box-header">

            <h3>Edit Mandate</h3>

            <div class="clear"></div>

        </div>
        <br/><br/>
        <%!
            String lo_limit;
            String up_limit;
            String auth1;
            String operator;
            String auth2;
            String account;

        %>

        <%
            lo_limit = (String) request.getAttribute("lo_limit");
            up_limit = (String) request.getAttribute("up_limit");
            auth1 = (String) request.getAttribute("auth1");
            operator = (String) request.getAttribute("operator");
            auth2 = (String) request.getAttribute("auth2");
            account = (String) request.getAttribute("account");
        %>

        <table>
            <col width="30">
            <col width="30">
            <col width="30">
            <col width="30">
            <col width="30">
            <col width="30">
            <col width="30">
            <tr> 
                <td>LOWER LIMIT</td> 
                <td>UPPER LIMIT </td>
                <!--<td>AUTHORIZER A</td>--> 
                <td>OPERATOR </td>
                <!--<td>AUTHORIZER B</td>-->
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <%

            %>

            <form >

                <tr>
                    <td><input style="width: 70px" type = "text" value ="<%= lo_limit%>" id = "lo_limit">
                    <td><input style="width: 70px" type = "text" value ="<%= up_limit%>" id = "up_limit">
<!--                    <td> <%= auth1%>
                        <input type = "hidden" value ="<%= auth1%>" id = "auth1">-->
                    <td> <%= operator%>   
                        <input type = "hidden" value ="<%= operator%>" id = "operator">
<!--                    <td> <%= auth2%>   
                        <input type = "hidden" value ="<%= auth2%>" id = "auth2">-->

                        <input type="hidden" id = "account"  value ="<%= account%>" id ="account">       
                    <td><input class="button" type="button" value="Submit" href="#" onClick="setupmandate('edit_mandate')" />  

                    <td><input style="width: 80px" class="button" type="button" value="REMOVE" href="#" onClick="setupmandate('remove_mandate')" />  
                </tr>
            </form>

        </table><br/>
    </div>
</div>         

</body>
</html>
