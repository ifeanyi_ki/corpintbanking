<%-- 
    Document   : newHome
    Created on : 22-Jun-2015, 11:36:19
    Author     : Ayomide
--%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style media="all" type="text/css">@import "css/wbsFormElements.css";</style>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TrustBond Mortgage Bank Plc :: Internet Banking</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css">
        <link href="screens/css/accordioin.css" rel="stylesheet" type="text/css" />
        <meta name="robots" content="noindex, nofollow">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/keyboard.css"  type="text/css" rel="stylesheet"/>
        <script language=JavaScript src="screens/scripts/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/ui-lightness/jquery-ui-1.8.12.custom.css" type="text/css" rel="stylesheet"/>

        <!-- <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.12.custom.css" type="text/css" rel="stylesheet"/>-->
        <SCRIPT language=JavaScript src="screens/scripts/jquery.keyboard.js" type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/jquery.mousewheel.js"  type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
        <style type="text/css">
            body, div, p, blockquote, ol, ul, dl, li, dt, dd, table, tbody, td, th, caption { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; }
            tr, td { vertical-align:top; }
            h1, h2, h3, h4 { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; font-weight:bold; line-height:1em; }
            h2 { font-size:110%; font-style:italic; }
            .mywbs_formlabel { font-style:italic;font-weight:bold }
            #trAjax { display:none; }
            .error, .important { color:#990033; font-weight:bold; }
            a           { color: #000; text-decoration: underline; }
            a:hover     { color: #005300; text-decoration: none; }
            a:active     { color: #000; }
            a:focus     { outline: #000; }
        </style>

        <script type="text/javascript" src="./Sign in to my.wbs_files/jquery-1.4.2.min.js"></script>			
        

    </head>
    <%-- jsp scriptlet to get the mac address for device detection 13/11/15 by K.I--%>
    <%
        java.lang.Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
        java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
        String line = in.readLine();
        String[] result = line.split(",");
        String macAddress = result[0].replace('"', ' ').trim();
        System.out.println("The machine id of the user is: " + macAddress);
    %>
    <body onload="keyboards();">
        <div class="hm_global">
            <div class="wrapper">
                <div class="header">
                    <div class="head">
                        <div class="logo"><img src="screens/images/logo.jpg" alt="Trust Bond"></div>

                        <div class="clear"></div>
                    </div>
                    <div class="trustmenu">
                        <div class="welcome"><h2>Welcome to Corporate Internet Banking</h2></div>
                        <!--<div class="logstatus">
                            <ul>
                                <li><a href="#">Logout</a></li>
                                <li><a href="#">Password</a></li>
                            </ul>
                        </div>-->
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <noscript>
                &lt;br /&gt;
                &lt;div style="background:#ffc;color:red;border:1px solid red;padding:10px"&gt;
                &lt;h2&gt;Javascript is disabled&lt;/h2&gt;

                &lt;p&gt;Please note that some areas of this site use Javascript.
                We recommend that you enable Javascript on your browser before proceeding.&lt;/p&gt;
                &lt;/div&gt;
                </noscript>
                <div class="logscreen">
                    <img src="screens/images/logscreen2.jpg" alt="" />
                    <div class="loglead logplus">
                        <h2>Login today for Internet Banking</h2>
                        <p>Easy, safe and convenient Internet Banking is just a few clicks away. 
                            Login online in minutes.</p>
                        <ul class="bullist">
                            <li>Secure</li> 
                            <li>24/7 Funds Access</li>
                            <li>Real-time</li>
                        </ul>
                    </div>
                    <div class="logbox">
                        <% if (request.getAttribute("message") != null) { %>
                        <span style="color: red"> <%=request.getAttribute("message")%> </span>
                        <% } %>
                        <html:form action="Main.do"  >
                            <html:hidden value="<%=macAddress%>" property="macAddress" ></html:hidden>
                            <div class="signy">
                                <div id="userCode_field" class="row" >
                                    <div class="label" nowrap>
                                        <p><label for="userCode" id="userCode_label"><bean:message key="label.userid"  /></label></p>
                                    </div>
                                    <div class="formElement ">
                                        <p> <html:text property="userid" ></html:text></p>
                                        <p><html:messages id="errors"><font color="red"><Strong><div class="label"><bean:write name="errors" /></div></Strong> </font> </html:messages></p>
                                        </div>

                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="signy">
                                    <div id="password_field" class="row "><div class="label" nowrap>
                                            <p>
                                                <label for="password" id="password_label">
                                                <bean:message key="label.password"/>
                                            </label>
                                        </p>
                                    </div>
                                    <div class="formElement ">
                                        <p><html:password property="password"></html:password></p>
                                        </div>

                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="rmb">
                                    <input name="" type="checkbox" value=""> Remember my user ID [?]
                                    <div class="clear"></div>
                                </div>
                                <hr/>
                                <ul class="forgetton">
                                    <!--<li><a href="#">Forgetton you Passwords?</a></li>
                                    <li><a href="#">Forgetton your ID?</a></li>
                                    <li><a href="#">Having problems loggin in?</a></li>-->
                                    <li>By signing in, I accept the Terms & Conditions indicated on the Internet Banking form.</li>
                                </ul>
                                <div class="rmb">
                                    <div id="signIn_field" class="row "><div class="formElement ">

                                            <!--<button id="signIn" title="" type="submit" onclick="" onkeyup="" name="login" value="" tabindex="20">-->
                                            <input type="button" class="buttons" onclick="showinit();" value="<bean:message key="label.Submit"/>">
                                        <input type="reset" class="buttons"  onclick="javascript:window.close();" value=Reset>
                                        <!--</button-->

                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="clear"></div>
                        </html:form>
                    </div>
                    <div class="clear"></div>
                    <div class="letus">
                        <h2>Let's take care of your <span>banking needs</span></h2>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="footer plus">
                <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
                <p> 012772890 - 3 | 08157955511</p>
                <p>2015 © TrustBond Mortgage Bank Plc.</p>
            </div>
            <div class="clear"></div>
        </div>

    </body>
</html>
