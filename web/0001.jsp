<%--
    Document   : 0001
    Created on : Jul 13, 2011, 1:42:32 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml_rt" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

    int timeout = session.getMaxInactiveInterval();
    response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
%>

<%!
    java.util.ArrayList<String[]> questionList = null;
%>

<%!
    // private java.lang.String data;
    //Added by Rebecca to fecth Security Questions
    public void jspInit() {
        com.fasyl.ebanking.logic.SecurityQuestion securityQuestion = new com.fasyl.ebanking.logic.SecurityQuestion();

        try {
            questionList = securityQuestion.getSecurityQuestion();
        } catch (java.sql.SQLException ex) {
        }

        System.out.println("size in jsp: " + questionList.size());
    }


%>


<%
    String newpassword = request.getParameter("newpasswd");
    String confirmpassword = request.getParameter("confnewpasswd");
    String passcode = request.getParameter("passcode");
    String users = request.getParameter("user");
    com.fasyl.ebanking.main.User user = (com.fasyl.ebanking.main.User) session.getAttribute("user");
//request.getSession();
    String updPasswd = "update SM_USER_ACCESS set cod_usr_pwd=?,flg_status='A',login_failiure_count=0,maint_date=sysdate where cod_usr_id=? ";
    System.out.println(" " + newpassword + confirmpassword + " ff " + user.getUserid() + " testing " + passcode);
    if (newpassword != null && users != null) {
        if (!(newpassword.equals(confirmpassword))) {
            request.setAttribute("msg_text", "The user password  does not match");
            request.setAttribute("msg_model_type", "error");
            request.setAttribute("msg_ok_target", "0001.jsp");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
            dispatcher.forward(request, response);
            return;
        }
        String password = com.fasyl.ebanking.util.Utility.genaratePassword();
        java.sql.Connection con = null;
        java.sql.PreparedStatement pstmt = null;

        System.out.println("Before encryption " + password);
        newpassword = com.fasyl.ebanking.util.Encryption.encrypt(newpassword);
        int result = 0;
        boolean ispasscode = false;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement("select 1 from sm_user_access where passcode=?");
            pstmt.setString(1, passcode);
            java.sql.ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {

                ispasscode = true;
            }
            if (ispasscode) {
                pstmt = con.prepareStatement(updPasswd);
                pstmt.setString(1, newpassword);
                pstmt.setString(2, user.getUserid());
                result = pstmt.executeUpdate();
            } else {
                request.setAttribute("msg_text", "You supplied wrong passcode");
                request.setAttribute("msg_model_type", "message");
                request.setAttribute("msg_ok_target", "Login.jsp");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);

            }
            if (result > 0) {
                request.setAttribute("msg_text", "You have successfully changed your password");
                request.setAttribute("msg_model_type", "message");
                request.setAttribute("msg_ok_target", "Login.jsp");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
                dispatcher.forward(request, response);
                return;
            } else {
                request.setAttribute("msg_text", "error while changing password");
                request.setAttribute("msg_model_type", "error");
                request.setAttribute("msg_ok_target", "Login.jsp");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
                dispatcher.forward(request, response);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            try {
                con.rollback();
            } catch (Exception e) {
                e.getMessage();
            }

            try {
                pstmt.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
    }
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language=JavaScript src="screens/scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/keyboard.css"  type="text/css" rel="stylesheet"/>
        <script language=JavaScript src="screens/scripts/jquery-ui-1.8.12.custom.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/ui-lightness/jquery-ui-1.8.12.custom.css" type="text/css" rel="stylesheet"/>
        <SCRIPT language=JavaScript src="screens/scripts/jquery.keyboard.js" type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/jquery.mousewheel.js"  type="text/javascript"></SCRIPT>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
        <link rel="stylesheet" href="screens/scripts/resources/css/styles.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="scripts/resources/css/" type="text/css" media="screen" />


        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css" />
        <link rel="stylesheet" type="text/css" href="screens/css/accordioin.css" />

        <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
        <script language="JavaScript" src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery.form.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" />
        <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery.pagination.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/session.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/transaction.js" type="text/javascript"></script>

        <link href="screens/scripts/SyntaxHighlighter.css" rel="stylesheet" type="text/css" />
        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->

        <link rel="stylesheet" href="screens/scripts/resources/css/pagination.css" type="text/css" media="screen" />


        <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="screens/scripts/resources/css/style.css" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />

        <!-- Colour Schemes

        Default colour scheme is green. Uncomment prefered stylesheet to use it.

        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />

        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />

        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery-1.3.2.min.js"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/simpla.jquery.configuration.js"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/facebox.js"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.wysiwyg.js"></script>

        <!-- jQuery Datepicker Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.datePicker.js"></script>
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.date.js"></script>
        <script type="text/javascript" src="screens/scripts/resources/scripts/datepickercontrol.js"></script>
        <link type="text/css" rel="stylesheet" href="screens/scripts/resources/scripts/datepickercontrol.css"/>
        <!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->



        <style>
            h1, h2, h3, h4, h5, h6 {
                font-family: Helvetica, Arial, sans-serif;
                color: #222;
                font-weight: bold;
            }
            h3            { font-size: 17px; padding: 0 0 10px 0; }

            p {
                padding: 5px 0 10px 0;
                line-height: 1.6em;
            }

            .button {
                font-family: Verdana, Arial, sans-serif;
                display: inline-block;
                background: #92B6CF /*url('../images/bg-button-greenib.gif')*/ top left repeat-x !important;
                /*background: #459300 url('../images/bg-button-greenib.gif') top left repeat-x !important;*/
                /*border: 1px solid #459300 !important;*/
                border: 1px solid #990000 !important;
                padding: 4px 7px 4px 7px !important;
                color: #fff !important;
                font-size: 11px !important;
                cursor: pointer;
            }

            form input.text-inputs {
                border: 1px #abadb3 solid;
                width: 220px;
                background: #fff;
                padding: 3px 5px;
            }

            form p {
                margin-left: 20px;
            }

            .content-box-header {
                background: #92B6CF url('../images/bg-content-box.gif') top left repeat-x;

            }

            #header form label{
                display: inline;
                padding: 0 0 10px;
                font-weight: bold;
            }
            form label {

                display: inline-block;
                padding: 0 0 10px;
                font-size: 12px;
                font-weight: bold;
                width: 100px;
            }
            .mainBodyBG {

                /* background-color:#E2ECF3;*/ /*#FFFFFF;*/
                margin-top: 50px;
                margin-left:300px;
                height: auto;
                width: 50%;
            }

            .buttons {
                font-size: 12px;
                text-transform: uppercase;
                background: url(screens/images/logbtn_bg.jpg) repeat-x;
                font-weight: 600;
                padding: 7px 20px;
                color: #533401;
                border: 1px #fff solid;
                border-color: transparent;
                cursor: pointer;
            }

            form {
                border: 1px solid #abadb3;
                border-radius: 5px;
                margin: 10px;
            }
        </style>
        <title>Change Password</title>
    </head>
    <body style="background-color: white;">
        <div class="logo"><img src="screens/images/logo.jpg" alt="Trust Bond"></div>
        <div class="trustmenu">
            <div class="welcome" style="float: none;"><h2>Welcome to Personal Internet Banking</h2></div>
        </div>
        <div class="mainBodyBG">
            <div align="left" id="body-wrappers">
                <div class="content-box-header" style="margin: 10px;">
                    <h3>Change Password</h3>

                    <div class="clear"></div>
                </div>

                <form action="003.jsp" name="passwordform" method="POST" onsubmit="return validateFirstTimeLoginForm()">




                    <%-- <p>
                   <div align="right" id="passwdbutton">
        
                <a   href="javascript:void(0)" onclick="hidediv('passwd',false);" rel="modal" type="button" class="button">Password</a>
                    </div>
                        <label ><bean:message key="label.oldpasswd"/></label><br>
                        <input class="" type="password" id="oldpasswd" name="oldpasswd" value="" size="50"  />
                      </p>--%>
                    <p style="margin-top: 20px;">
                        <label><bean:message key="label.newpasswd"/></label>
                        <input class="text-inputs" type="password" id="newpasswd" name="newpasswd" value=""  />


                    </p>
                    <p>
                        <label>Confirm Password</label>
                        <input class="text-inputs" type="password" id="confnewpasswd" name="confnewpasswd" value=""   />
                        <input type="hidden" value="<%=(user.getUserid())%>" name="user"/>

                    </p>



                    <p>
                        <label>Choose Security Question<font color="red">*</font></label>
                        <select name="question" id ="question"  class="small-input" style="width: auto !important;">
                            <option value="" style="display:none" selected>Select security question</option>
                            <%
                                for (int i = 0; i < questionList.size(); i++) {
                                    String[] question = questionList.get(i);
                                    out.println("<option value=\"" + question[0] + "\" >" + question[1] + "</option>");
                                }
                            %>
                        </select>


                    </p>   

                    <p>
                        <label><bean:message key="label.answer"/></label>
                        <input class="text-inputs" type="answer" id="answer" name="answer" value="" />
                        <input type="hidden" value="answ" name="answ"/>

                    </p>




                    <p>
                        <label>Passcode</label>
                        <input class="text-inputs" type="password" id="passcode" name="passcode" value="" />
                        <input type="hidden" value="<%=(user.getUserid())%>" name="user"/>

                    </p>
                    <div align="center">
                        <p>
                            <input type="submit" value="Submit" name="Submit" class="buttons">
                        </p>
                    </div>

                </form>
            </div>
        </div>

        <div class="adbanner"><a href="#"><img src="screens/images/adbanner03.jpg" alt="Let's take care of your banking needs"></a></div>
        <div class="footer">
            <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
            <p>2015 © TrustBond Mortgage Bank Plc.</p>
            <p> Powered by <a href="http://fasylgroup.com" target="blank">FASYL Technology Group</a></p>
        </div>
        <div class="clear"></div>

    </body>
</html>
