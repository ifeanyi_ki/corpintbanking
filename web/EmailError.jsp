<%--
    Document   : Home
    Created on : 15-Sep-2010, 14:46:27
    Author     : Tope
--%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="Pragma" content="no-cache">
          <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
            <meta http-equiv="Cache-Control" content="no-cache">
        <link rel="stylesheet" type="text/css" href="css/menu/menu.css">
        <link rel="stylesheet" type="text/css" href="css/all.css">
        <link href="css/alert.css" rel="stylesheet" type="text/css" > </link>
	<link href="css/lighting.css" rel="stylesheet" type="text/css" > </link>

        <title>E-Banking</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body>
        <p>
            <%=request.getParameter("secpasswd")%>
        </p>
    </body>