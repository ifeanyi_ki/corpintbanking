<%@page import="com.fasyl.ebanking.main.DataObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Initiators List</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Initiator List</h3>

                <div class="clear"></div>

            </div> 


            <%!
                String customer_name;
                String result;
            %>

            <%
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
            %>
            <br/>
            <h3><% out.println(result); %></h3>

            <br/>


            <%
                DataObject dob = new DataObject();
                String account = (String) request.getAttribute("account");
                List<String[]> list = dob.getLists("select initiator_id, trans_limit from acc_init_mandate where account = '" + account + "'", 2);

            %>
            <% if (list.size() == 0) {%>
            <p>Yet to assign Initiators to account no: <%= account%></p>
            <% } else {%>
            <table>
                <tr> 
                    <td> INITIATOR </td>   
                    <td> TRANSACTION LIMIT</td>   
                    <td>&nbsp;</td>   
                    <td>&nbsp;</td>
                </tr>
                <%
                    for (int i = 0; i < list.size(); i++) {
                %>

                <tr>
                <form>
                    <td> <%= list.get(i)[0]%></td>
                    <td><%= list.get(i)[1]%> </td>
                    <input type="hidden" value="<%= list.get(i)[1]%>" id="trans_limit" />  
                    <td><input type="button"  href="#" value="EDIT" class="button" 
                               onClick="editinitiator('edit_initiator', '<%= list.get(i)[0]%>', '<%= account%>', '<%= list.get(i)[1]%>')" /> </td>
                    <td><input type="button"  href="#" value="REMOVE" class="button" 
                               onClick="editinitiator('remove_initiator', '<%= list.get(i)[0]%>', '<%= account%>', '<%= list.get(i)[1]%>')" /> </td> 
                </form>
                </tr>

                <% }%>
                <% }%>
            </table>   

            <br/>

            <input type="button" class="button" value="Back" onclick="goBack('back', '<%= account%>')" />

        </div>
    </div>         

</body>
</html>
