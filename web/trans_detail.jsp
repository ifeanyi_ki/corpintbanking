<%@page import="com.fasyl.corpIntBankingadmin.vo.ftVo"%>
<%@page import="com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl"%>
<%@page import="com.fasyl.corpIntBankingadmin.bo.FundTransferBo"%>
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Pending Fund Transfer</title>
        <%

            int timeout = session.getMaxInactiveInterval();
            response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">
                <h3>Transaction Details</h3>
                <div class="clear"></div>
            </div> 

            <%!
                String init_account;
                String ben_acc;
                String amt;
                String initiator_id;
                String user_id;
                String trans_id;
                String narration;
                String role_id;
                String auth1;
                String auth2;
                String auth1_action;
                String auth2_action;
                String query;
                String auth1_comment;
                String auth2_comment;
                boolean post;
                String operator;
                String benAccName;
                String benBank;
                String trxType;
            %>
            <%
                ftVo fTVo = null;
                FundTransferBo fundTransferBo = new FundTransferBoImpl();
                user_id = (String) session.getAttribute("user_id");
                trans_id = (String) request.getAttribute("trans_id");
                role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();
                if (role_id.equalsIgnoreCase("Initiator")) {
                    fTVo = fundTransferBo.getFundTransferByTrxId(trans_id);
                }

                if (role_id.equalsIgnoreCase("Low Level Authorizer")) {
                    fTVo = fundTransferBo.getFundTransferByTrxIdAndAuth1(trans_id);
                }

                if (role_id.equalsIgnoreCase("High Level Authorizer")) {
                    fTVo = fundTransferBo.getFundTransferByTrxIdAndAuth2(trans_id);
                }

                if (fTVo == null && role_id.equalsIgnoreCase("High Level Authorizer")) {
                    fTVo = fundTransferBo.getFundTransferByTrxIdAndAuth2withOR(trans_id);
                }

            %>
            <%  initiator_id = fTVo.getInt_id();
                init_account = fTVo.getInit_acc();
                ben_acc = fTVo.getBen_acc();
                amt = String.valueOf(fTVo.getAmt());
                narration = fTVo.getNarration();
                auth1 = fTVo.getAuth1() == null ? user_id : fTVo.getAuth1();
                auth1_action = fTVo.getAuth1_action();
                auth2 = fTVo.getAuth2();
                auth2_action = fTVo.getAuth2_action();
                auth1_comment = fTVo.getAuth1_comment() == null ? "" : fTVo.getAuth1_comment();
                auth2_comment = fTVo.getAuth2_comment() == null ? "" : fTVo.getAuth2_comment();
                operator = fTVo.getOperator();
                benAccName = fTVo.getBene_acc_name();
                benBank = fTVo.getBene_bank();
                trxType = fTVo.getTrxType();
            %>
            <br/><br/>
            <form>
                <p>
                    <label>Initiator</label>
                    <input class="text-input small-input" type="text" value="<%=initiator_id%>" disabled />
                </p>
                <br/>
                <p>
                    <label>Account</label>
                    <input class="text-input small-input" type="text" value="<%=init_account%>" disabled />
                </p>
                <br/>
                <p>
                    <label>Beneficiary Account</label>
                    <input class="text-input small-input" type="text" value="<%=ben_acc%>" disabled />
                </p>
                <br/>
                <p>
                    <label>Amount</label>
                    <input class="text-input small-input" type="text" value="<%=amt%>" disabled />
                </p>
                <br/>
                <p>
                    <label>Narration</label>
                    <input class="text-input small-input" type="text" value="<%=narration%>" disabled  /> 
                </p>
                <br/>
                <p>
                    <label>Transaction Type</label>
                    <input class="text-input small-input" type="text" value="<%=trxType%>" disabled  /> 
                </p>
                <%-- start of soft token --%>
                <br/>
                <p>

                    <input  class="button" id="testButton" name="testButton" type="button" onclick="temporaryDisable(this);
                            getPin('70000', '1');"  value="Get Token" /> 
                </p>
                <div id="trx_details">
                    <br /><br />
                </div>
                <p>
                    <label>Enter Token<b><font color="red">*</font></b></label>
                    <input class="text-input small-input" type="text" name ="enterpin" id="enterpin" maxLength="20"  value="" onblur="getAccountDetail('001', '332');"/>

                </p>
                <div id="trx_details"></div>
                <%-- start of soft token --%>
                <br/>
                <% if ((!role_id.equalsIgnoreCase("High Level Authorizer") && operator.equalsIgnoreCase("OR"))
                            || (role_id.equalsIgnoreCase("High Level Authorizer") && operator.equalsIgnoreCase("AND"))
                            || (role_id.equalsIgnoreCase("Low Level Authorizer") && operator.equalsIgnoreCase("ONLY"))
                            || (role_id.equalsIgnoreCase("Low Level Authorizer") && operator.equalsIgnoreCase("AND"))) { %>
                <h3>Level One Authorization</h3>

                <br/>
                <p>
                    <label>User Id</label>
                    <% if (auth1 == "null" || auth1.equalsIgnoreCase("") || auth1.equalsIgnoreCase("null")) {%>   
                    <input id="auth1" class="text-input small-input" type="text" value="<%=user_id%>" disabled /> 
                    <%} else {%>
                    <input  id="auth_1" class="text-input small-input" type="text" value="<%=auth1%>" disabled /> 
                    <% }%>

                </p>
                <br/>
                <p>
                    <label>Decision</label>
                    <input class="text-input small-input" type="text" value="<%=auth1_action%>" disabled />  
                </p> 
                <br/>
                <p>
                    <label>Comment</label>
                    <input type="text" value = "<%=auth1_comment%>" id = "auth1_comment" class="text-input small-input" <% if (!role_id.equalsIgnoreCase("LOW LEVEL AUTHORIZER")) { %> disabled  <%}%>  />      
                </p>
                <% } %>
                <% if (role_id.equalsIgnoreCase("High Level Authorizer")) { %>
                <% if ((auth2_action != null)) {%>    
                <br/>
                <h3> Level Two Authorization </h3>

                <br/>
                <p>
                    <label>User Id</label>
                    <input  id="auth2" class="text-input small-input" type="text" value="<%=user_id%>" disabled /> 

                </p>
                <br/>
                <p>
                    <label>Decision</label>
                    <input class="text-input small-input" type="text" value="<%=auth2_action%>" disabled />
                </p>
                <br/>
                <p>
                    <label>Comment</label>
                    <input class="text-input small-input" type="text" value = "<%=auth2_comment%>" id = "auth2_comment" <% if (!role_id.equalsIgnoreCase("HIGH LEVEL AUTHORIZER")) { %> readonly  <%}%>>      

                </p>
                <%}%>
                <%}%>
                <br/>
                <p>
                    <% if (role_id.contains("Authorizer")) {%>     
                    <input type="hidden" value ="<%= trans_id%>"  id="trans_id">
                    <label>Action</label>
                    <select type="text" value="" id ="action" required>
                        <option value="" >-- Select Action --</option>
                        <option value="DECLINED">DECLINE</option>
                        <option value="APPROVED">APPROVE</option>
                    </select>  
                </p>	
                <%--  Enable this button only for LLA when the operator is AND --%>
                <% if (role_id.equalsIgnoreCase("Low Level Authorizer") && operator.equalsIgnoreCase("AND")) {%>
                <p>
                    <input class="button" type="button" value="Submit"  onClick="processFT('authFT', '<%=user_id%>')" />
                </p>
                <% }%>
                <%}%>        


                <%
//                    if (operator.equalsIgnoreCase("ONLY")) {
//                        if (auth1_action.equalsIgnoreCase("APPROVED")) {
//                            post = true;
//                        }
//                    } else if (operator.equalsIgnoreCase("AND")) {
//                        if (auth1_action.equalsIgnoreCase("APPROVED") && auth2_action.equalsIgnoreCase("APPROVED")) {
//                            post = true;
//                        }
//                    } else if (operator.equalsIgnoreCase("OR")) {
//                        if (auth1_action != null) {
//                            if (auth1_action.equalsIgnoreCase("APPROVED")) {
//                                post = true;
//                            }
//                        }
//                        if (auth2_action != null) {
//                            if (auth2_action.equalsIgnoreCase("APPROVED")) {
//                                post = true;
//                            }
//                        }
//                    }
                %>
                <%  if (role_id.equalsIgnoreCase("Low Level Authorizer") && operator.equalsIgnoreCase("ONLY")) {%>  
                <br/>
                <% if (trxType.equalsIgnoreCase("Third Party Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>','<%=trans_id%>', '<%=user_id%>', '<%=role_id%>')" />
                <% } else if (trxType.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="makepayment(70151, '7016', '<%=init_account%>',
                                       '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                       '<%=narration%>', '<%=trans_id%>', '<%=user_id%>', '<%=role_id%>');" /> 
                <%}else if (trxType.equalsIgnoreCase("Local Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onclick="Transfer(33310,'3331','<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>','<%=trans_id%>', '<%=user_id%>', '<%=role_id%>')" />
                <%}%>
                <%}%> 
                
                <%  if (role_id.equalsIgnoreCase("Low Level Authorizer") && operator.equalsIgnoreCase("OR")) {%>  
                <br/>
                <% if (trxType.equalsIgnoreCase("Third Party Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>')" />
                <% } else if (trxType.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="makepayment(70151, '7016', '<%=init_account%>',
                                       '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                       '<%=narration%>');" /> 
                <%}else if (trxType.equalsIgnoreCase("Local Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onclick="Transfer(33310,'3331','<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>','<%=trans_id%>', '<%=user_id%>', '<%=role_id%>')" />
                <%}%>
                <%}%> 
                
                  <%  if (role_id.equalsIgnoreCase("High Level Authorizer") && operator.equalsIgnoreCase("OR")) {%>  
                <br/>
                <% if (trxType.equalsIgnoreCase("Third Party Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>')" />
                <% } else if (trxType.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="makepayment(70151, '7016', '<%=init_account%>',
                                       '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                       '<%=narration%>');" /> 
                <%}else if (trxType.equalsIgnoreCase("Local Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onclick="Transfer(33310,'3331','<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>','<%=trans_id%>', '<%=user_id%>', '<%=role_id%>')" />
                <%}%>
                <%}%> 
                
                  <%  if (role_id.equalsIgnoreCase("High Level Authorizer") && operator.equalsIgnoreCase("AND")) {%>  
                <br/>
                <% if (trxType.equalsIgnoreCase("Third Party Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="Transfer(33210, '3321', '<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>')" />
                <% } else if (trxType.equalsIgnoreCase("Inter Bank Fund Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onClick="makepayment(70151, '7016', '<%=init_account%>',
                                       '<%=ben_acc%>', '<%=benAccName%>', '<%=benBank%>', '<%=amt%>',
                                       '<%=narration%>');" /> 
                <%}else if (trxType.equalsIgnoreCase("Local Transfer")) {%>
                <input value="Submit" class="button" type="button" 
                       onclick="Transfer(33310,'3331','<%=init_account%>', '<%=ben_acc%>', '<%=amt%>',
                                       '<%=narration%>','<%=trans_id%>', '<%=user_id%>', '<%=role_id%>')" />
                <%}%>
                <%}%> 

                <input type="button" value="Back" class="button" 
                       onClick="backToDeclined('back')" /> 
            </form>


        </div>         
    </body>
</html>
