<%-- 
    Document   : Home
    Created on : 15-Sep-2010, 14:46:27
    Author     : Tope
--%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
 <%


try {

    com.fasyl.ebanking.main.User user= (com.fasyl.ebanking.main.User)session.getAttribute("user");
     //out.println(user.getUsername());
     request.setAttribute("user", user);
  }catch(Exception e){
      e.printStackTrace();
  }
  %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="Pragma" content="no-cache">
            <meta http-equiv="Cache-Control" content="no-cache">
        <link rel="stylesheet" type="text/css" href="css/menu/menu.css">
        <link rel="stylesheet" type="text/css" href="css/all.css">
        <link href="css/alert.css" rel="stylesheet" type="text/css" > </link>
	<link href="css/lighting.css" rel="stylesheet" type="text/css" > </link>

        <title>E-Banking</title>
        <script type="text/javascript">
           

function logoff(){
    
    var src="./screens/logout.jsp?";
    req.open("POST",src , true);
    req.send(null);

        }




</script>


        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>

        <link rel="stylesheet" href="screens/scripts/windowfiles/dhtmlwindow.css" type="text/css" />
        <script type="text/javascript" src="screens/scripts/windowfiles/dhtmlwindow.js"/>
        <script type="text/javascript" src="screens/scripts/Prototype.js"/>
        <script type="text/javascript" src="screens/scripts/tabber.js"/>
        <link rel="stylesheet" href="screens/scripts/tab.css" type="text/css" />
        <link rel="stylesheet" href="screens/scripts/modal.css" type="text/css" />
       <script type="text/javascript" src="screens/scripts/modal.js"></script>
       <script type="text/javascript" src="screens/scripts/supportsession.js"></script>
       <script type="text/javascript" src="screens/scripts/jquery-1.2.1"></script>
       <script type="text/javascript" src="screens/scripts/jquery.form.js"></script>
        <script type="text/javascript" src="screens/scripts/transaction.js"></script>
        <script type="text/javascript" src="screens/scripts/cal2.js"></script>
        <script type="text/javascript" src="screens/scripts/cal_conf2.js"></script>
        <LINK href='screens/scripts/CalenderControl.css' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src='screens/scripts/CalendarControl.js' language='javascript'></script>
    <link href="screens/scripts/SyntaxHighlighter.css" rel="stylesheet" type="text/css" />
 </head>
 <script type="text/javascript">

 //End "opennewsletter" function

</script>
    </head>
   

    

    <body onload="init();">

        <div id="main" >

            <c:import url="/Header.jsp"/>
           
             
            <div id="middle">
                <div id="left-column" >
                    <c:import url="/menu.jsp"/>
                </div>
		   
                <div id="center-column">
                   <!-- <div class="table">-->
               <div id="scrloader" value="true">
               </div>
                    
                <!--  </div>-->
               
              </div>
                 <div id="right-column">
                     <strong class="h"><bean:message key="label.fastpath"/></strong>
                        <div class="box">
                             <table>
                     <tbody>
                     <tr>
                      <form id=frm_fastPath action="javascript:" name=frm_fastPath>
                          <td><input  type=textfield name=fastPath size="6" id="faspath" />
                         </td>

                         <td><html:submit onclick="showScreen(this.form.fastPath,this.form.fastPath.value)" styleClass="buttons" ><bean:message key="label.Go"/></html:submit></td>
                      </form>
                    </tr>
                     </tbody>
                </table>
                        </div>
                    </div>
                       <div id="right-column">
                           <div class="buttom">
                           <form id=frm_logout action="./screens/logout.jsp" name=frm_logout>
                               <input type="submit"  class="buttons" value="Logout"  >
                               <input type="button"  class="buttons" value="Reset" onclick="resetme();"  >
                               <div id="logout"><input type="hidden" value="false" id="login"  ></div>

                      </form>
                           </div>
                      </div>
              </div>
                      
              <div class="footermessage">
                     www.fasylgroup.com
        </div>
                     
             <c:import url="/footer.jsp"/>
        </div>
        
    </body>
</html>
