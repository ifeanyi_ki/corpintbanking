<%-- 
    Document   : Header
    Created on : 26-Nov-2009, 14:47:37
    Author     : Nisar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
 <%


try {
    com.fasyl.xnett.main.User user= (com.fasyl.xnett.main.User)session.getAttribute("user");
         

  %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style media="all" type="text/css">@import "css/all.css";</style>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>
        <title>JSP Page</title>
    </head>
    
    <body>
    <style>
        
    </style>
        <div id="header">
            <a href="./index.jsp" class="logo"><img src="img/xnettlogo.png" width="250" height="55" alt=""  /></a>
            <div  id="details">
                <table class="table" width="100%">
 

                        <tr >
                            <td class="tableclass"><bean:message key="label.userid"/></td>
                            <td   class="style2"><label id="userid" title="<%=user.getUserid()%>"><%=user.getUserid()%></label></td>
                            <td class="tableclass" ><bean:message key="label.lastlogin"/></td>
                            <td   class="style2"><%=user.getLastlogindate()%></td>
                           
                        </tr>
                        <tr>
                            <td class="tableclass"><bean:message key="label.Branch"/></td>
                            <td  class="style2"><%=user.getBranch()%></td>
                            <td  class="tableclass"><bean:message key="label.posting"/></td>
                            <td   class="style2"><label id="sysdate" title=<%=user.getLastlogindate()%>><%=user.getLastlogindate()%></label></td>
                        </tr>
                        <tr>
                            <td class="tableclass">Affiliate</td>
                            <td   class="style2"><%=user.getAffiliate()%></td>
                        </tr>
                   
                </table>

            </div>
            
                  
            <a href="" class="connection"><img src="img/globe.gif" width="36" height="36" alt="globe" /></a>
        </div>

    </body>
</html>
<%
}catch(Exception e){
      e.printStackTrace();
  }
%>
