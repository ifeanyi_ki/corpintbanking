<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Users List</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

        <div >
            <div > 
                <%!
                    String user_id;
                %>

                <table>
                    <%
                        List<String> useridList = (List<String>) request.getAttribute("userList");

                    %>
                    <% if (useridList == null) {%>
                    <div class="content-box-header">

                        <h3>User does not exist.</h3>

                        <div class="clear"></div>

                    </div>
                    <% } else { %>
                    <%
                        for (int i = 0; i < useridList.size(); i++) {
                            user_id = useridList.get(i);

                    %>

                    <tr>
                    <form>
                        <td> <%= user_id%>  </td>
                        <td>
                            <input href="#" type="submit" class="button" value="View User" onClick="getuserdetails('get_user_details', '<%= user_id%>')" />
                            <input type="hidden" id = "<%= user_id%> "  value ="<%= user_id%> " />       
                        </td>
                    </form>
                    </tr>
                    <% }%>
                    <% }%>
                </table><br/>
            </div>
        </div>         

    </body>
</html>
