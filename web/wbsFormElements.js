window.onunload = function() {
    if (typeof(disabledForm) != "undefined") {
        for (var x=0; x<disabledForm.elements.length; x++) {
            var el = disabledForm.elements[x];
            if (el.type == 'submit' || el.type == 'reset')
                el.readonly = false;
            if (typeof el.name == "string" && el.name.indexOf('count_FE') != -1)
                el.disabled = false;
        }
    }
}

String.prototype.trim = function() {
    var temp = this;
    while (escape(temp).indexOf('%u2018') > -1)
       { 
       temp = escape(temp).replace('%u2018', '%27').replace('%u2019', '%27').replace('%u0060', '%27').replace('%u00b4', '%27').replace('%u201c', '%22').replace('%u201d', '%22').replace('%u2022', '[*]').replace('%u2219', '[*]').replace('%u00b7', '[*]');
       temp = unescape(temp);
       }
    return temp.replace(/^\s*/,"").replace(/\s*$/,"")
}

function choosefield(fieldid,thisval) {
	var field = document.getElementById(fieldid);
	for (var i=0; i<field.length; i++) {
		if (field.options[i].text == thisval) {
      		field.selectedIndex = i;
      		return;
       	}
	}
}

function addLabelProperties(aForm) {
	//	Collect all label elements in form, init vars		
	if (typeof aForm.getElementsByTagName == 'undefined') return;
	var labels = aForm.getElementsByTagName("label");
	var label, i = j = 0;
	var el;

	//	Loop through labels retrieved
	while (label = labels[i++])
	{
		//	For Opera 6
		if (typeof label.htmlFor == 'undefined') return;
		
		//	Retrieve element
		el = aForm.elements[label.htmlFor];
		if (typeof el == 'undefined' || typeof el.label != 'undefined')
			//	no associated element, or label property already added
			continue;
		else if (typeof el.length != 'undefined' && el.length > 1 && el.nodeName != 'SELECT') {
		    //	For arrayed elements
			for ( j = 0; j < el.length; j++ )
				el.item( j ).label = label;
		}
		//	Regular label
		el.label = label;
	}
}		

function getfieldname(thisfield) {
    var txt;
    
	if (thisfield.label) {
		if (thisfield.label.title.length != 0 && !thisfield.label.title.substr('required') && !thisfield.label.title.substr('undefined'))
		    return '\'' + thisfield.label.title.trim() + '\'';
		else if (typeof thisfield.label.innerHTML != 'undefined')
		    return '\'' + thisfield.label.innerHTML.trim() + '\'';
		else
			return 'A field';
	}
	else
		return 'A field';
}

function insertAtCaret(fieldid,string) {
	var field = document.getElementById(fieldid);

    //IE support
    if (document.selection) {
        field.focus();
        var sel = document.selection.createRange();
        sel.text = string;
        field.focus();
    }
    //MOZILLA/NETSCAPE support
    else if (field.selectionStart || field.selectionStart == '0') {
        var startPos = field.selectionStart;
        var endPos = field.selectionEnd;
        field.value = field.value.substring(0, startPos)
        + string
        + field.value.substring(endPos, field.value.length);
        field.focus();
        field.selectionStart = startPos + string.length;
        field.selectionEnd = startPos + string.length;
    } else {
        field.value += string;
        field.focus();
    }
}

function fieldLengthCounter(field,maxlimit,unit,enforce) {
	if (document.getElementById && document.createTextNode) {
    	var inputs = field.parentNode.getElementsByTagName('input');
		
		for (var i=0;i<inputs.length;i++) {
		   	if (inputs[i] && inputs[i].id.indexOf('count_') != -1) {
				counter = inputs[i];
	        	if (unit == 'word') {
	            	var wc = wordcounter(field.value);
	            	if (enforce && wc > maxlimit) {
	            	    var source = field.value;
	            	    var words = source.split(/\s+/g);
	            	    var pos = 0;
	            	    for (var i=0; i < maxlimit; i++)
	                        pos = source.indexOf(words[i], pos) + words[i].length; 
	                    field.value = source.substr(0,pos);
	            		if (field.setSelectionRange) {
	            		    field.focus();
	               		    field.setSelectionRange(field.value.length,field.value.length);
	               		    field.scrollTop = field.scrollHeight;
	                   	}
	                    var wc = wordcounter(field.value);
	            	}
	        		var remaining = maxlimit - wc;
	         	} else {
	        	    // default to 'character'
	        	    var unit = 'character';
	            	if (enforce && field.value.length > maxlimit) {
	            		field.value = field.value.substring(0, maxlimit);
	            		if (field.setSelectionRange) {
	            		    field.focus();
	               		    field.setSelectionRange(maxlimit,maxlimit);
	               		    field.scrollTop = field.scrollHeight;
	            		}
	            	}
	            	var wc = wordcounter(field.value);
	        		var remaining = maxlimit - field.value.length;
	        	}
	        	
	        	var wctext = '';
	        	if (wc == 1)
	        		wctext = ' (' + wc + ' word written)';
	        	else if (wc != 0)
	        		wctext = ' (' + wc + ' words written)';
	        	
	        	if (remaining == 1)
	        		counter.value = '1 ' + unit + ' left' + wctext;
	        	else if (remaining >= 0)
	        		counter.value = remaining + ' ' + unit + 's left' + wctext;
	        	else if (remaining == -1)
	        		counter.value = '1 ' + unit + ' too many' + wctext;
	        	else
	        		counter.value = -remaining + ' ' + unit + 's too many' + wctext;
	    	}
		}
    }
}

function wordcounter(string) {
	var a = string.split(/\s+/g); // split the sentence into an array of words
	if (a.length > 1)
		return a.length;
	else if (a[0].length == 0)
		return 0;
	else
		return 1;
}

function setFocus() {
	var form = document.forms[0];
	for (x = 0; x < form.elements.length; x++) {
		if (form.elements[x].className == 'mandatory' && (form.elements[x].value == '' || form.elements[x].selectedIndex == 0)) {
			form.elements[x].focus();
			break;
		}
	}
}

function updateCheckLink(srcfield,thisLink) {
    if (urlcheck(srcfield)) {
        thisLink.href = srcfield.value;
        thisLink.disabled = false;
        thisLink.onclick = '';
    } else {
        thisLink.href = 'about:blank';
        thisLink.disabled = true;
        thisLink.onclick = 'return false';
    }
}

function highlightRed(el) {
    if (el.style)
		el.style.backgroundColor = '#FF6666';
	else if (document.getElementById)
	    document.getElementById(el).style.backgroundColor = '#FF6666';
}

function highlightActive(el) {
    if (el.style)
	    el.style.backgroundColor = '#D1DFD3';
	else if (document.getElementById)
	    document.getElementById(el).style.backgroundColor = '#D1DFD3';
}

function unhighlight(el) {
	if (el.style)
	    el.style.backgroundColor = '#F6F6F6';
	else if (document.getElementById)
	    document.getElementById(el).style.backgroundColor = '#F6F6F6';
}

function unhighlightgroup(el) {
	if (el.type == 'radio' || el.type == 'checkbox') {
		var elg = document.getElementsByName(el.name);
		for (var i=0; i<elg.length; i++) {
			elg[i].style.backgroundColor = 'white';
		}
	}
}

function addOptionButtonText(el) {
	if (document.getElementById(el).value.length)
		document.getElementById(el+'-AddButton').value='Add option';
	else
		document.getElementById(el+'-AddButton').value='Cancel';
}

function addOptionPrepare(el) {
	if (document.getElementById(el).type == 'select-one' && document.getElementById(el).value == 'addANewOption') {
		document.getElementById(el+'-AddSection').style.display = 'block';
		document.getElementById(el).style.display = 'none';
	}
}

function addOption(el) {
	var selectEl = document.getElementById(el);
	var newValue = document.getElementById(el+'-Add').value;
	if (newValue.length) {
		selectEl.options[selectEl.length] = new Option(newValue, newValue);
		selectEl.selectedIndex = selectEl.length-1;
		document.getElementById(el+'-Add').value = '';
		addOptionButtonText(el);
	} else {
		selectEl.selectedIndex = 0;
	}
	document.getElementById(el+'-AddSection').style.display = 'none';
	document.getElementById(el).style.display = 'block';
}

function parseFunc(func) {
	 var str = func.toString();
	 // remove comments to EOL
	 str = str.replace(/\/\/.*\n/g,"\n");
	 // remove inline comments
	 str = str.replace(/\/\*[^*]*\*+([^/*][^*]*\*+)*\//g,"");
	
	 var arity = func.length;
	 var match = /function\s*([a-zA-Z_$][\w$]*)?\s*\(([^)]*)\)/.exec(str);
	 var vars = match[2].split(/\s*,\s*/);
	 return {name:match[1],arity:arity,args:vars};
}

function validate(aForm, extraValidationFn) {
	var canSend = true;
	var problem = '';
	var lists_handled = '';
	if (aForm == null)
		var form = document.forms[1];
	else
		var form = aForm;
		
    addLabelProperties(form);
    
	for (var x=0; x<form.elements.length; x++) {
		var el = form.elements[x];
				
        // trim + strip word utf
        if (el.type == 'text' || el.type == 'textarea' || el.type == 'password') { 
            el.value = el.value.trim();
        }		
        if (el.type && el.type.search(/text|file|textarea|password|radio|checkbox|select-one|select-multiple/) != -1 && (typeof extraValidationFn == 'function')) {
			
			FnDetails = parseFunc(extraValidationFn);
			var extraResult = extraValidationFn(el);
			if (extraResult != '') {
				canSend = false;				
				highlightRed(el);			
				
					problem = problem + getfieldname(el) + ' ' + extraResult + '\n';
												
			}
		}
        if (el.className.indexOf('bbcode') != -1) {
            // start by removing [[escaped brackets]] and [*] list items
			var bb = el.value.replace(/\[{2}.*\]{2}/g, '').replace(/\[\*\]/g, '').replace(/\[\/?enclosure\]/g, '').match(/\[\/?[a-z]+/gi);
			if (bb == null) {
				unhighlight(el);
			} else {
				var stack = new Array();
				var l = bb.length;
				for (var i=0;i<l;i++) {
					thisTag = bb[i].replace(/\[/, '');
					if (thisTag.indexOf('/') != -1) {
						// it's a closer
						if (!stack.length) {
							stack.push(thisTag);
							break;
						} else {
							var test = stack.pop();
							if (test != thisTag.replace(/\//, '')) {
								stack.push(test);
								break;
							}
						}
					} else {
						// it's an opener
						stack.push(thisTag);
					}
				}
				
				if (stack.length){
					canSend = false;
					highlightRed(el);
					problem =  'I found this text, "[' + stack.pop() + '", which looks like BBcode.\nIf you want to use square brackets, [[double them up]], unless you are writing BBcode.\nApart from [*] for list items, all BBcode needs both [starting] and [/ending] codes to wrap in pairs.\n';
				} else {
					unhighlight(el);
				}					
			}
        }

		if (el.className.indexOf('mandatory') != -1) {
			if (((el.type == 'text' || el.type == 'file' || el.type == 'textarea' || el.type == 'password') && el.value == '')
			 || ((el.type == 'select-one' || el.type == 'select-multiple') && el.selectedIndex <= 0)) {
				canSend = false;
				highlightRed(el);
				problem = problem + getfieldname(el) + ' is required, but was left blank.\n';
			} else {
				unhighlight(el);
			}
			if (el.type == 'radio' || el.type == 'checkbox') {
				var elg = document.getElementsByName(el.name);
				checkedList = false;
				for (var i=0; i<elg.length; i++) {
					if (elg[i].checked) {
						checkedList = true;
						break;
					}
				}
				if (!checkedList) {
					highlightRed(el);
					canSend = false;
					if (lists_handled.indexOf(el.name) == -1) {
						lists_handled = lists_handled + el.name + ' ';
						if (elg.length == 1)
						    problem = problem + getfieldname(el) + ' is required, but was not ticked.\n';
						else
						    problem = problem + getfieldname(el) + ' is required, but no item was selected from the list.\n';
					}
				} else {
					unhighlight(el);
				}
			}
		}
		

		
		if ((el.className.indexOf('integer') != -1) && (!integercheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' can only contain whole numbers, using the digits 0-9 without punctuation.\n';
		}
		
		if ((el.className.indexOf('float') != -1) && (!floatcheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' can only contain a numeric value, using the digits 0-9 and decimal point (.)\n';
		}
		
		if ((el.className.indexOf('email') != -1) && (!emailcheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a valid email address.\n';
		}

		
		if ((el.className.indexOf('url') != -1) && (!urlcheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a valid URL. Please check it begins with http:// or similar and ends with a slash or appropriate filename.\n';
		}
		
		if ((el.className.indexOf('telephone') != -1) && (!telephonecheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a valid telephone number (only the digits 0-9, spaces, + for international prefix and brackets are allowed).\n';
		}
		
		if ((el.className.indexOf('ukpostcode') != -1) && (!ukpostcodecheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a valid UK postcode.\n';
		}
		
		if ((el.className.indexOf('time') != -1) && (!timecheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a recognisable time (24 hour format is required - hh:mm).\n';
		}
		
		if ((el.className.indexOf('date') != -1) && (!datecheck(el))) {
			canSend = false;
			highlightRed(el);
			problem = problem + getfieldname(el) + ' does not contain a recognisable date (english format is preferred - d month yyyy).\n';
		}
		
		
		
		var start = el.className.indexOf('minimum_');
		if (start != -1) {
			var end = el.className.indexOf(' ', start);
			if (end == -1)
				var end = el.className.length;
			var limit = parseInt(el.className.substring(start+8, end));
			if ((el.type == 'select-multiple' || el.type == 'checkbox') && lists_handled.indexOf(el.name) == -1) {
				lists_handled = lists_handled + el.name + ' ';
				if (countChecked(el, 0) < limit) {
					canSend = false;
					highlightRed(el);
					problem = problem + 'You must select at least ' + limit + ' options for ' + getfieldname(el) + '.\n';
				}		
			} else if (el.type == 'text' || el.type == 'textarea' || el.type == 'password') {
				if (!minimumcheck(el, limit)) {
					canSend = false;
					highlightRed(el);
					problem = problem + 'The value of ' + getfieldname(el) + ' must be at least ' + limit + '.\n';
				}
			}
		}

		var start = el.className.indexOf('maximum_');
		if (start != -1) {
			var end = el.className.indexOf(' ', start);
			if (end == -1)
				var end = el.className.length;
			var limit = parseInt(el.className.substring(start+8, end));
			if ((el.type == 'select-multiple' || el.type == 'checkbox') && lists_handled.indexOf(el.name) == -1) {
				lists_handled = lists_handled + el.name + ' ';
				if (countChecked(el, 0) > limit) {
					canSend = false;
					highlightRed(el);
					problem = problem + 'You must select no more than ' + limit + ' options for ' + getfieldname(el) + '.\n';
				}		
			} else if (el.type == 'text' || el.type == 'textarea' || el.type == 'password') {
				if (!maximumcheck(el, limit)) {
					canSend = false;
					highlightRed(el);
					problem = problem + 'The value of ' + getfieldname(el) + ' must be no more than ' + limit + '.\n';
				}
			}
		}
		
	}

	if (canSend) {
	    for (var x=0; x<form.elements.length; x++) {
	        el = form.elements[x];
	        if (el.type == 'submit' || el.type == 'reset') {
	            el.style.color = '#afafaf';
	            el.readonly = true;
	            disabledForm = form;
	        }
            if (typeof el.name == "string" && el.name.indexOf('count_FE') != -1) {
                el.disabled = true;
	            disabledForm = form;
            }
	    }
		return true;
	} else {

		if (countstrings(problem, '\n') == 1) {
			problem = 'Please check the highlighted box:\n' + problem;
		}
		else {
			problem = 'Please check the highlighted boxes:\n' + problem;		
		}
		alert(problem);

		setFocus();

		return false;
	}
}

function countstrings(str1, str2) {
	var count = 0;
	for (var i=0;i<str1.length;i++) {
		if (str2 == str1.substr(i,str2.length))
			count++;
	}
	return count;
}

function StrReplace(str1, str2, str3) {
	str1 = str1.split(str2).join(str3);
	return str1;
}

function countChecked(el) {
	var siblings = el.form[el.name];
	var count = 0;
	
	for(var x=0; x<siblings.length; x++ ) {
		if(siblings[x].checked == true) count++;
	}

	return count;
}


function minimumcheck(thisfield, limit) {
	with (thisfield) {
		if (value == null || value == '' || parseInt(value, 10) >= parseInt(limit, 10)) {
			return true;
		} else {
			return false;
		}
	}
}

function maximumcheck(thisfield, limit) {
	with (thisfield) {
		if (value == null || value == '' || parseInt(value, 10) <= parseInt(limit, 10)) {
			return true;
		} else {
			return false;
		}
	}
}

function integercheck(thisfield) {
	with (thisfield) {
		if (value == null || value == '' || parseInt(value, 10) == value) {
			return true;
		} else {
			return false;
		}
	}
}

function floatcheck(thisfield) {
	with (thisfield) {
		if (value == null || value == '' || !isNaN(parseFloat(value)) || !isNaN(parseFloat('0' + value))) {
			return true;
		} else {
			return false;
		}
	}
}

function emailcheck(thisfield) {
	var regexp = /^[A-Za-z0-9&'-=_.]+@([A-Za-z0-9&'=_.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
	with (thisfield) {
		if (value == null || value == '' || value.match(regexp)) {
			return true;
		} else {
			return false;
		}
	}
}

function urlcheck(thisfield) {
	var regexp = /^(http:\/\/|https:\/\/|ftp:\/)([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}.*$/;
	with (thisfield) {
		if (value == null || value == '' || value.match(regexp)) {
			return true;
		} else {
			return false;
		}
	}
}

function telephonecheck(thisfield) {
	var regexp = /^[0-9+() ]{5,20}$/;
	with (thisfield) {
		if (value == null || value == '' || value.match(regexp)) {
			return true;
		} else {
			return false;
		}
	}
}

function ukpostcodecheck(thisfield) {
	var regexp = /^[A-Z]{1,2}[0-9][A-Z0-9]? [0-9][ABD-HJLNP-UW-Z]{2}$/;
	with (thisfield) {
		if (value == null || value == '' || value.match(regexp)) {
			return true;
		} else {
			return false;
		}
	}
}

function timecheck(thisfield) {
	var regexp = /^[012]?[0-9][:.][0-9]{2}$/;
	with (thisfield) {
		if (value == null || value == '' || value.match(regexp)) {
			return true;
		} else {
			return false;
		}
	}
}

function datecheck(thisfield) {
  var MMM = ' JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC',
  Suf = '(st|nd|rd|th)', Mon, x, Rex, B, Y, ND=0, F=2;
  var Months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

  Q = thisfield.value;
  
  if (Q == null || Q == '')
  	return true;
  
  if (F==0) { Rex = /(\d+)(\d{2})(\d{2})$/     // D5+ as Y+MMDD
    Q = Q.search(Rex)==-1 ? '' : Q.replace(Rex, '$1 $2 $3') // split
   } // optional paragraph

  Rex = new RegExp(Suf, 'i') // Remove suffix, see * below
  Q = Q.replace(Rex, ' ')  // optional paragraph

  Rex = /([^A-Z]+)([IVX]{1,4})(.*)/i // Seek Roman (month) : viii IX
  if (Rex.test(Q)) {
    Mon = Q.replace(Rex, '$2').toUpperCase() // 1-4 Chars of month
    x =
     ' I    II   III  IV   V    VI   VII  VIII IX   X    XI   XII '.
      indexOf(' '+Mon)
    Q = Q.replace(Rex, '$1 '+(1+x/5)+' $3') // make numeric
   } // optional paragraph

  Rex = /([^A-Z]*)([A-Z]{1,3})(.*)/i
  // Seek month letters : Au / Aug. Or {3}.
  if (Rex.test(Q)) {
    Mon = Q.replace(Rex, '$2').toUpperCase() // 1-3 Letters of month
    x = MMM.indexOf(' '+Mon); // or next for English only, *
    // x = ' JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC'.
    //   indexOf(' '+Mon)
    Q = Q.replace(Rex, '$1 '+(1+x/4)+' $3') // to numeric
    } // optional paragraph

  Rex = /^(\d+)\D+(\d+)\D+(\d+)$/ // three digit fields
  // if (F==1) Q = Q                       // ISO
  //if (F==2) Q = Q.replace(Rex, '$3 $2 $1') // EU
  //if (F==3) Q = Q.replace(Rex, '$3 $1 $2') // NA
  Q = Q.replace(Rex, '$3 $2 $1');
  B = Rex.test(Q) // Split into $1 $2 $3
  if (B) with (RegExp) { Y = +$1
    if (Y<100) Y += (Y<30?2000:1900)      // optional century line
    with (ND = new Date(Y, $2-1, $3))
      B = ((getMonth()==$2-1) && (getDate()==$3))  } // YMD valid ?
  // For true years 00..99, enter as >2 digits, check $1.length;
  // then increase year by 100 & decrease month by 1200.
  if (B) {
  	var nd = Q.match(Rex);
	nd[2] = Months[nd[2]-1]; // month to text
	nd[1] = parseInt(nd[1]); // year to integer
	if (nd[1]<100) nd[1] += (nd[1]<30?2000:1900);
	thisfield.value = nd[3] + ' ' + nd[2] + ' ' + nd[1];
  }
  return B;
  //return [B, ND] // [Valid, DateObject]
  // To ban leading zeros in M, D, and Y,
  // alter all \\d+ in last Rex to [1-9]\\d?  untested.
  /* end datecheck */ }

function limitcheck(thisfield, question, count)
{
	var checkcount = 0;
	for (var idx=0; idx < thisfield.form.elements.length; idx++)
	{
		// checkboxes
		if ((thisfield.form.elements[idx].type == 'checkbox') && (thisfield.form.elements[idx].name == question) && (thisfield.form.elements[idx].checked))
			checkcount++;
		// integer fields
		else if ((thisfield.form.elements[idx].type == 'text') && (thisfield.form.elements[idx].name.indexOf(question && '_answer') != -1) && (thisfield.form.elements[idx].value.length != 0))
			checkcount++;
	}
	if (checkcount > count)
	{
		alert('You may choose a maximum of #Attributes.Limit# from the list in ' + question + '.\n\nPlease de-select another option first if you wish to choose this one.');
		return false;
	}
	else
		return true;
}

function rankcheck(thisfield, question, limit)
{
	var arrAnswer = new Array();
	var arrQuestion = new Array();
	var duplicated = -1;
	
	if ((thisfield.value != '') && ((parseInt(thisfield.value) != thisfield.value) || (thisfield.value > limit) || (thisfield.value < 1)))
	{
		thisfield.style.backgroundColor = '#FF6666';
		alert('Sorry, you can only rank this option between 1 and ' + limit + '.');
		thisfield.value = '';
		thisfield.focus();
		return false;
	}
	
	for (var idx=0; idx < thisfield.form.elements.length; idx++)
	{
		if ((thisfield.form.elements[idx].type == 'text') && (thisfield.form.elements[idx].name.indexOf(question + '_answer') != -1))
		{
			arrQuestion[arrQuestion.length] = thisfield.form.elements[idx];
			if (thisfield.form.elements[idx].value.length != 0)
				arrAnswer[arrAnswer.length] = parseInt(thisfield.form.elements[idx].value);
		}
	}
	
	arrAnswer.sort();
	
	for (var idx=1; idx<arrAnswer.length; idx++)
		if (arrAnswer[idx] == arrAnswer[idx-1])
		{
			duplicated = arrAnswer[idx];
			break;
		}
	
	if (duplicated != -1)
	{
		for (var idx=0; idxarrQuestion.length; idx++)
		{
			if (arrQuestion[idx].value == duplicated)
				arrQuestion[idx].style.backgroundColor = '#FF6666';
			else
				arrQuestion[idx].style.backgroundColor = '#FFFFFF';
		}
		
		alert('Sorry, you cannot rank two options equally in ' + question + '.\nYou have already ranked one of the options with value ' + parseInt(thisfield.value) + '.\nPlease re-rank that option first, if you wish to rank this latest option ' + parseInt(thisfield.value) + '.');
		thisfield.value = '';
		thisfield.focus();
		return false;
	}
	else
	{
		for (var idx=0; idxarrQuestion.length; idx++)
		{
			arrQuestion[idx].style.backgroundColor = '#FFFFFF';
		}
		return true;
	}
}

// BBCode functions

var bbcOpenTags = new Array;

function bbcNextTextarea(el) {
    var theElements = el.form.elements;
    var ready = false;
    for (var i=0; i < theElements.length ;i++) {
        if (theElements[i].name == el.name)
            ready = true;
        if (theElements[i].type == "textarea" && ready)
            return theElements[i];
    }
    return false;
}

function bbcAddTag(el, tag) {
	bbcOpenTags[bbcOpenTags.length] = tag;
	el.innerHTML = '/' + el.innerHTML;
}

function bbcRemoveTag(el, tag) {
	for (i = 0; i < bbcOpenTags.length; i++) {
		if (bbcOpenTags[i] == tag) {
			bbcOpenTags.splice(i, 1);
			el.innerHTML = el.innerHTML.replace('/', '');
		}
	}
}

function bbcCheckOpenTags(tag) {
	for (i = 0; i < bbcOpenTags.length; i++)
		if (bbcOpenTags[i] == tag)
			return true;
	return false;
}
		
function bbcInsert(el, tag, hasEnd) {
    tagStart = '[' + tag + ']';
    tag = tag.split('=')[0];
    tagEnd = hasEnd ? '[/' + tag + ']' : '';

	textbox = bbcNextTextarea(el);
	textbox.focus();

	if (document.selection) {
	    // IE
		sel = document.selection.createRange();
		if (sel.text.length > 0) {
			sel.text = tagStart + sel.text + tagEnd;
		}
		else {
			if (!bbcCheckOpenTags(tag) || !hasEnd) {
				sel.text = tagStart;
				if (hasEnd)
	                bbcAddTag(el, tag);
			}
			else {
				sel.text = tagEnd;
				bbcRemoveTag(el, tag);
			}
		}
		textbox.focus();
	}
	else if (textbox.selectionStart || textbox.selectionStart == '0') {
    	// gecko
		var startPos = textbox.selectionStart;
		var endPos = textbox.selectionEnd;
		var cursorPos = endPos;
		if (startPos != endPos) {
			textbox.value = textbox.value.substring(0, startPos)
										+ tagStart
										+ textbox.value.substring(startPos, endPos)
										+ tagEnd
										+ textbox.value.substring(endPos, textbox.value.length);
			cursorPos += tagStart.length + tagEnd.length;
		}
		else {
			if (!bbcCheckOpenTags(tag) || !hasEnd) {
				textbox.value = textbox.value.substring(0, startPos)
											+ tagStart
											+ textbox.value.substring(endPos, textbox.value.length);
				if (hasEnd)
	                bbcAddTag(el, tag);
				cursorPos = startPos + tagStart.length;
			}
			else {
				textbox.value = textbox.value.substring(0, startPos)
											+ tagEnd
											+ textbox.value.substring(endPos, textbox.value.length);
				bbcRemoveTag(el, tag);
				cursorPos = startPos + tagEnd.length;
			}
		}
		textbox.focus();
		textbox.selectionStart = cursorPos;
		textbox.selectionEnd = cursorPos;
	}
	else {
		if (!bbcCheckOpenTags(tag) || !hasEnd) {
			textbox.value += tagStart;
			if (hasEnd)
			    bbcAddTag(el, tag);
		}
		else {
			textbox.value += tagEnd;
			bbcRemoveTag(el, tag);
		}
		textbox.focus();
	}
}

function BBCurl(el) {
	textbox = bbcNextTextarea(el);
	
	if (!bbcCheckOpenTags('url')) {
    	var URL = prompt("Enter the URL/address:", "http://");
    	if (URL) {
    	    if (URL.indexOf('www.') == 0)
    	        URL = 'http://' + URL;
    	    bbcInsert(el, 'url=' + URL, true);
    	}
    } else {
        bbcInsert(el, 'url', true);
    }
}

function aerValidate(thisField){
	if ((thisField.name == 'frm_length')&&(thisField.value == '0')){					
		problem=' cannot be 0!';
		return problem;
	} else {
		return '';
	}
}