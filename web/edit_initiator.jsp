<%@page import="com.fasyl.ebanking.main.DataObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Corporate List</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

          <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Edit initiator Mandate</h3>

                <div class="clear"></div>

            </div>


                <%!
                    String customer_name;
                    String result;
                    String initiator_id;
                    String account;
                    String trans_limit;
                %>

                <%
                    result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
                    out.println(result);
                    initiator_id = (String) request.getAttribute("initiator_id");
                    account = (String) request.getAttribute("account");
                    trans_limit = (String) request.getAttribute("trans_limit");
                %>
                <br><br><br>

                <table>

                    <tr> 
                        <td> Initiator Id </td>
                        <td> Account </td>
                        <td> Transaction Limit</td>
                        <td>&nbsp;</td>
                    </tr>

                    <form >
                        <tr>
                            <td> <%= initiator_id%></td>
                            <td><%= account%>   </td>
                            <td>
                                <input class="text-input small-input" type = "text" value = "<%= trans_limit%>"  id="trans_limit"/>
                            </td>
                            <td>   
                                <input value="Submit" type="button" class="button" href="#" 
                                       onClick="editinitiatorlimit('edit_initiator_limit', '<%=initiator_id%>', '<%= account%>')">  
                            </td>

                        </tr>



                    </form>






                </table><br/>    

            </div>
        </div>         

    </body>
</html>
