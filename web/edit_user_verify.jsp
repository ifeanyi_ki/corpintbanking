
<%@page import="com.fasyl.corpIntBankingadmin.vo.UserVo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
        <%
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>

    <body>
        <%!
            String user_id = "";
            String email = "";
            String mobile_number = "";
            String first_name = "";
            String last_name = "";
            String client = "";
            String role_id = "";
            String created_by = "";
            String active = "";

        %>
        <%
            UserVo userVo = (UserVo) request.getAttribute("userVo");

            role_id = (String) userVo.getRoleId() == null ? "" : (String) userVo.getRoleId();
            user_id = (String) request.getParameter("user_id") == null ? "" : (String) request.getParameter("user_id");
            email = (String) userVo.getEmail() == null ? "" : (String) userVo.getEmail();
            mobile_number = (String) userVo.getPhoneNumber() == null ? "" : (String) userVo.getPhoneNumber();
            first_name = (String) userVo.getFirstName() == null ? "" : (String) userVo.getFirstName();
            last_name = (String) userVo.getLastName() == null ? "" : (String) userVo.getLastName();
            client = (String) userVo.getClient() == null ? "" : (String) userVo.getClient();
            created_by = (String) userVo.getCreated_by() == null ? "" : (String) userVo.getCreated_by();
            active = (String) userVo.getActive() == null ? "" : (String) userVo.getActive();
//            role_id = (String) request.getAttribute("role_id") == null ? "" : (String) request.getAttribute("role_id");
//            user_id = (String) request.getAttribute("user_id") == null ? "" : (String) request.getAttribute("user_id");
//            email = (String) request.getAttribute("email") == null ? "" : (String) request.getAttribute("email");
//            mobile_number = (String) request.getAttribute("mobile_number") == null ? "" : (String) request.getAttribute("mobile_number");
//            first_name = (String) request.getAttribute("first_name") == null ? "" : (String) request.getAttribute("first_name");
//            last_name = (String) request.getAttribute("last_name") == null ? "" : (String) request.getAttribute("last_name");
//            client = (String) request.getAttribute("client") == null ? "" : (String) request.getAttribute("client");
//            created_by = (String) request.getAttribute("created_by") == null ? "" : (String) request.getAttribute("created_by");
//            active = (String) request.getAttribute("active") == null ? "" : (String) request.getAttribute("active");
        %>

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Please confirm changes</h3>

                <div class="clear"></div>

            </div>
            <form>
                <p>
                    <label>Client</label>
                    <input type="text" readonly="true" value="<%= client%>" class="text-input small-input" />
                    <input type ="hidden" value ="<%= client%>" id="client" >
                </p>
                <br/>
                <p>
                    <label>User Id</label>
                    <input type="text" readonly="true" value=" <%= user_id%>" class="text-input small-input" />
                    <input type ="hidden" value ="<%= user_id%>" id="user_id" >
                </p>
                <br/>
                <p>
                    <label>Email Address</label>
                    <input class="text-input small-input" type ="text" value ="<%= email%>" id="email" readonly="true"/>
                </p>
                <br/>
                <p>
                    <label>Mobile Number</label>
                    <input class="text-input small-input" type ="text" value ="<%= mobile_number%>" id="mobile_number" readonly="true"/>
                </p>
                <br/>
                <p>
                    <label>First Name</label>
                    <input class="text-input small-input" type ="text" value ="<%= first_name%>" readonly="true"/>
                    <input type ="hidden" value ="<%= first_name%>" id="first_name" >
                </p>
                <br/>
                <p>
                    <label>Last Name</label>
                    <input class="text-input small-input" type ="text" value ="<%= last_name%>" readonly="true"/>
                    <input type ="hidden" value ="<%= last_name%>" id="last_name" >
                </p>
                <br/>
                <p>
                    <label>Role</label>   
                    <input type="text" value="<%= (String) request.getParameter("role_id")%>" readonly="true" class="text-input small-input"/>
                    <input type="hidden"  value="<%= (String) request.getParameter("role_id")%>"  class="input_summary" id ="role_id"  readonly>
                </p>
                <br/>
                <p>
                    <label>Created By</label>
                    <input class="text-input small-input" type ="text" value ="<%= created_by%>" readonly="true"/>
                    <input type ="hidden" value ="<%= created_by%>" id="created_by" >
                </p>
                <br/>
                <p>
                    <label>Active</label>
                    <input type="text" value="<%= active%>" class="text-input small-input" readonly="true"/>
                    <input type ="hidden" value ="<%= active%>" id="active" >
                </p>
                <br/>
                <p>
                    <input type="button"  class="button" value="Edit" onClick="movetopage('movetopage', 'edit_user.jsp')" >  &nbsp; &nbsp; 
                    <input type="button"  class="button" value="Submit" onClick="submit_edit_user('submit_edit_user')" > 
                </p>

            </form>

    </body>
</html>
