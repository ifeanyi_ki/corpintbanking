<%-- 
    Document   : Logout.jsp
    Created on : 22-Jun-2015, 15:08:43
    Author     : Ayomide
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Trust Bond</title>
        <link rel="shortcut icon" href="screens/images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- STYLE SHEET -->
        <link rel="stylesheet" type="text/css" href="screens/css/style.css">

    </head>
    <body>
        <div class="hm_global">
            <div class="wrapper">
                <div class="logoutwrap">
                    <div class="logcontent">
                        <div class="loglogo">
                            <img src="screens/images/logo.jpg"  alt="Trust Bond">
                            <div class="clear"></div>
                        </div>
                        <div class="thanks">
                            <p>Thank you for using our Internet Banking Service </p>
                            <p><a href="ReLogin.jsp">Click here to re-login to your account >></a></p>
                            <div class="clear"></div>
                        </div>
                        <div class="lets">
                            <h2>Let's take care of your</h2>
                            <h1>banking needs</h1>
                            <h3><img src="screens/images/slogan.png" alt="… your trust, our bond" /></h3>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="phone_app">
                        <img src="screens/images/phone_app.png" alt="Trust Bond Phone App"></div>
                    <div class="enjoy">
                        <h3>Enjoy our instant Fund Transfer, </h3>
                        <h4>A fast way to transfer money. </h4>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="social">
                    <%--<span>
                        <a href="#"><img src="screens/images/mail.jpg" alt="mail"> </a>
                    </span>--%>
                    <span>
                        <a href="https://www.facebook.com/pages/TrustBond-Mortgage-Bank-Plc/418070921639823"><img src="screens/images/fb.jpg" alt="Facebook"> </a>
                    </span>
                    <span>
                        <a href="https://twitter.com/trustbondmbplc"><img src="screens/images/tw.jpg" alt="Twitter"> </a>
                    </span>
                    <span>
                        <a href="https://plus.google.com/u/0/107044154222867026486/posts"><img src="screens/images/gplus.jpg" alt="Google+"></a> 
                    </span>
                    <%--<span>
                        <a href="#"><img src="screens/images/ig.jpg" alt="LinkedIn"> </a>
                    </span>--%>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="footer">
                <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
                <p> 012772890 - 3 | 08157955511</p>
                <p>2015 © TrustBond Mortgage Bank Plc.</p>
            </div>
            <div class="clear"></div>
        </div>
        <script src="screens/js/jquery.min.js"></script>
    </body>
</html>
