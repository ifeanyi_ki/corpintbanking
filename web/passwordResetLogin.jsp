<%@page import="com.fasyl.corpIntBankingadmin.daoImpl.UserAccessDAOImpl"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="HandheldFriendly" content="True">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TrustBond Mortgage Bank Plc :: Internet Banking</title>
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
        <!--[if lt IE 9]>
                                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <meta name="robots" content="noindex, nofollow">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Pragma" content="no-cache">
        <meta http-equiv="Cache-Control" content="no-cache">

        <link rel="stylesheet" type="text/css" href="screens/css/style.css" />
        <link rel="stylesheet" type="text/css" href="screens/css/accordioin.css" />

        <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
        <script language="JavaScript" src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery.form.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" />
        <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery.pagination.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/session.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/transaction.js" type="text/javascript"></script>

        <link href="screens/scripts/SyntaxHighlighter.css" rel="stylesheet" type="text/css" />
        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->

        <link rel="stylesheet" href="screens/scripts/resources/css/pagination.css" type="text/css" media="screen" />


        <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />
        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="screens/scripts/resources/css/style.css" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />

        <!-- Colour Schemes

        Default colour scheme is green. Uncomment prefered stylesheet to use it.

        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />

        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />

        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery-1.3.2.min.js"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/simpla.jquery.configuration.js"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/facebox.js"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.wysiwyg.js"></script>

        <!-- jQuery Datepicker Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.datePicker.js"></script>
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.date.js"></script>
        <script type="text/javascript" src="screens/scripts/resources/scripts/datepickercontrol.js"></script>
        <link type="text/css" rel="stylesheet" href="screens/scripts/resources/scripts/datepickercontrol.css"/>
        <!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->

        <style type="text/css">
            body, div, p, blockquote, ol, ul, dl, li, dt, dd, table, tbody, td, th, caption { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; }
            tr, td { vertical-align:top; }
            h1, h2, h3, h4 { font-family:"Corbel","Helvetica Neue","Nimbus Sans",arial,helvetica,freesans,sans-serif; font-weight:bold; line-height:1em; }
            h2 { font-size:110%; font-style:italic; }
            .mywbs_formlabel { font-style:italic;font-weight:bold }
            #trAjax { display:none; }
            .error, .important { color:#990033; font-weight:bold; }
            a           { color: #000; text-decoration: underline; }
            a:hover     { color: #005300; text-decoration: none; }
            a:active     { color: #000; }
            a:focus     { outline: #000; }
        </style>

        <script type="text/javascript" src="./Sign in to my.wbs_files/jquery-1.4.2.min.js"></script>			


    </head>
    <%-- jsp scriptlet to get the mac address for device detection 13/11/15 by K.I--%>
    <%
        java.lang.Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
        java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
        String line = in.readLine();
        String[] result = line.split(",");
        String macAddress = result[0].replace('"', ' ').trim();
        System.out.println("The machine id of the user is: " + macAddress);
        UserAccessDAOImpl userAccessDaoImpl = new UserAccessDAOImpl();
        String question = userAccessDaoImpl.getQuestion((String) session.getAttribute("user_id"));
    %>
    <body onload="keyboards();">
        <!--<div class="hm_global">-->
        <div class="wrapper">
            <div class="header">
                <div class="head">
                    <div class="logo"><img src="screens/images/logo.jpg" alt="Trust Bond"></div>

                    <div class="clear"></div>
                </div>
                <div class="trustmenu">
                    <div class="welcome"><h2>Welcome to Corporate Internet Banking</h2></div>

                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>

            <noscript>
            &lt;br /&gt;
            &lt;div style="background:#ffc;color:red;border:1px solid red;padding:10px"&gt;
            &lt;h2&gt;Javascript is disabled&lt;/h2&gt;

            &lt;p&gt;Please note that some areas of this site use Javascript.
            We recommend that you enable Javascript on your browser before proceeding.&lt;/p&gt;
            &lt;/div&gt;
            </noscript>


            <div  class="tab-content" id="tab2" align="left">
                <br/><br/>

                <span style="font: red">You must change your password on password reset</span>

                <html:form action="first.do">
                    <c:if test="${request.getAttribute('message') != null }">
                        <span style="color: red">${requestScope.message}</span>
                    </c:if>
                    <table cellspacing="5px" cellpadding="2px">
                        <tr>
                            <td>&nbsp;</td>
                            <td><html:messages id="errors"><font color="red"><Strong><div class="label"><bean:write name="errors" /></div></Strong> </font> </html:messages></td>
                            </tr>
                            <tr>
                                <td>Old Password</td>
                                <td><html:password property="oldPassword" name="FirstLoginForm" ></html:password></td>
                            </tr>
                            <tr>
                                <td>New Password</td>
                                <td><html:password property="newPassword" name="FirstLoginForm" ></html:password></td>
                            </tr>

                            <tr>
                                <td>Confirm New Password</td>
                                <td>
                                <html:password property="confirmPassword" name="FirstLoginForm" ></html:password>
                                </td>
                            </tr>

                            <tr>
                                <td>Question</td>
                                <td>
                                <html:select property="questionId">
                                    <html:option value='<%=question%>'><%=question%></html:option>
                                </html:select>
                                <%--<html:text property="question" name="FirstLoginForm" value='<%=question%>' disabled="true" size="40"></html:text>--%>
                            </td>
                        </tr>


                        <tr>
                            <td>Security Answer</td>
                            <td>
                                <html:text property="answer" name="FirstLoginForm" ></html:text>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                <html:submit value="Submit" />
                            </td>
                        </tr>

                    </table>

                </html:form>
            </div>
            <div class="clear"></div>

            <div class="footer plus">
                <p>This site ensures that all information sent to us via the World Wide Web are encrypted. You can confirm the information provided by our certificate issuer by clicking on the padlock icon on your address bar.</p>
                <p> 012772890 - 3 | 08157955511</p>
                <p>2015 © TrustBond Mortgage Bank Plc.</p>
            </div>
            <div class="clear"></div>


    </body>
</html>

