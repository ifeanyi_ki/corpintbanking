<%--
    Document   : menu
    Created on : 26-Nov-2009, 15:47:52
    Author     :
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu</title>

        <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />

		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="screens/scripts/resources/css/stylenews.css" type="text/css" media="screen" />

		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />

        <style media="all" type="text/css">@import "css/cssverticalmenu.css";</style>
        <script type="text/javascript" src="screens/scripts/verticalMenu.js"></script>
        <script type="text/javascript" src="screens/js/jquery-1.3.2.min.js"></script>
        <SCRIPT language=JavaScript src="screens/scripts/session.js" type="text/javascript"></SCRIPT>

       <link rel="stylesheet" type="text/css" href="screens/superfish-1.4.8/css/superfish.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="screens/superfish-1.4.8/css/superfish-navbar.css" media="screen"/>
		<script type="text/javascript" src="screens/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="screens/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript">

		// initialise plugins
		jQuery(function(){
			jQuery('ul.sf-menu').superfish();

		});

		</script>

 <style type="text/css">
  
            #header {
	position:absolute;
	width:100%;
	height:100px;
	/*background:url(screens/scripts/resources/images/header.PNG) no-repeat top;*/
	margin-top: 0px;
        margin-left: 0px;
        background-color:#ffffff;


}
            body{
                margin: 0 10px 0 10px;
}
div ul.sf-menu{
  margin-top:60px;
   margin-bottom: 0;
   font-size: smaller;

}
    
    </style>

    </head>
    <body>



        <div id="header">
            <ul class="sf-menu">
  <li>
    <a href="#" >System Admin</a>
    <ul>
      <li>
        <a href="#" onClick="showAuth(101)">Generate Password</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(102)">New User</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(103)">View User</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(104)">Modify User</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(105)">Add Account</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(106)">Remove Account</a>
      </li>
    </ul>
  </li>

  <li>
    <a href="#" class="nav-top-item">Account Info</a>
    <ul>
      <li>
        <a href="#" onClick="showAuth(201)">Acct Position</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(202)">Acct Summary</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(203)">Mini Statement</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(204)">Account History</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(205)">Loan Enquiry</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(206)">Deposit Enquiry</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="#" class="nav-top-item">Services</a>
    <ul>
      <li>
        <a href="#" class="nav-2nd-item a.secondlevel">Cheque Book</a>
        <ul>
          <li>
            <a href="#" onClick="showAuth(321)">Status Enquiry</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(322)">Cheque Req.</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(323)">Stop Cheque</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(324)">Confirm Cheque </a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#" class="nav-2nd-item a.secondlevel">Fund Transfer</a>
        <ul>
          <li>
            <a href="#" onClick="showAuth(331)">Setup</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(332)">Local Transfer</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(333)">Others</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(334)">Affiliate Transfer</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#" class="nav-2nd-item a.secondlevel">Standing Instruct..</a>
        <ul>
          <li>
            <a href="#" onClick="showAuth(341)">SI Enquiry</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(342)">SI Setup</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(344)">Close SI</a>
          </li>
        </ul>
      </li>
    </ul>
  </li>
  <li>
    <a href="#" class="nav-top-item">Security</a>
    <ul>
      <li>
        <a href="#" onClick="showAuth(401)">App Error</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(402)">Create Bulleting</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(403)">Fraud Report</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="#" class="nav-top-item">Info Board</a>
    <ul>
      <li>
        <a href="#" onClick="showAuth(511)">Rate</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(513)">Bulletin</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(514)">Message</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="#" class="nav-top-item">Manage Beneficiary</a>
    <ul>
      <li>
        <a href="#" onClick="showAuth(601)">Add Beneficiary</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(602)">Modify Beneficiary</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(603)">Delete Beneficiary</a>
      </li>
      <li>
        <a href="#" onClick="showAuth(604)">View Beneficiary</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="#" class="nav-top-item">Payment</a>
    <ul>
      <li>
        <a href="#" class="nav-2nd-item a.secondlevel">Bill/Utility Payment</a>
        <ul>
          <li>
            <a href="#" onClick="showAuth(7011)">Merchant Payment</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(7012)">DSTV Payment</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(7013)">Top Up</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(7014)">E-Cash</a>
          </li>
          <li>
            <a href="#" onClick="showAuth(7015)">Electricity Bill</a>
          </li>
        </ul>
      </li>
      <li>
        <a href="#" class="nav-2nd-item a.secondlevel">Bulk Payment</a>
        <ul>
          <li>
            <a href="#" onClick="showAuth(7021)">Transfer</a>
          </li>
        </ul>
      </li>
    </ul>
  </li>
</ul>
            <img src="" height="" width="" class="header" alt="Testing">
</div>

        
    </body>

</html>
