<%--
    Document   : ReLogin
    Created on : Apr 26, 2010, 11:37:48 AM
    Author     : Henry
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html:html lang="true">


    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="Pragma" content="no-cache">
            <meta http-equiv="Cache-Control" content="no-cache">

        <title>E-banking</title>
         <link rel="shortcut icon" href="img/logo.JPG">
        <style media="all" type="text/css">@import "css/all.css";</style>
        <script type="text/javascript" src="screens/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="screens/scripts/dhtmlwindow.css" type="text/css" />
        <script type="text/javascript" src="screens/scripts/dhtmlwindow.js"/>
        <link rel="stylesheet" href="screens/scripts/modal.css" type="text/css" />
       <script type="text/javascript" src="screens/scripts/modal.js"></script>
    </head>
    <body>
        <div id="main" >

            <div id="loginheader">
                  <a href="./index.jsp" class="logo"><img src="img/fasyl_image.png" width="250" height="55" alt="" /></a>
                 <a href="" class="connection"><img src="img/globe.gif" width="36" height="36" alt="globe" /></a>

            </div>
            <div id="loginmiddle">
                <div id="left-column">
                </div>
                <div id="Login-column">
                    <div class="table">
                    <html:form action="ReLogin.do" >

                        <table class="table" width="60%">
                            <thead>
                              <tr>
                                  <th colspan="3" class="formHeading"><bean:message key="label.relogin" /></th>

                             </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td class="LblIPMand" ><bean:message key="label.userid"  /><font color="red">*</font></td>
                                <td  ><html:text property="userid" ></html:text>
                                </td>
                                <td width="10">

                                   <div class="loginbox">
                               <html:messages id="errors">
                             <font color="red"><Strong><bean:write name="errors" /></Strong> </font> </html:messages>




                                 </div>
                                  </td>
                            </tr>
                            <tr>
                                <td class="LblIPMand"  ><bean:message key="label.password"/><font color="red">*</font></td>
                                <td ><html:password property="password"></html:password></td>
                            </tr>
                            <tr >
                                <td colspan="2" align="center"> <html:submit styleClass="buttons"><bean:message key="label.Submit"/></html:submit>
                                    <html:cancel styleClass="buttons" ><bean:message key="label.cancel"/></html:cancel>
                                </td>
                            </tr>

                            </tbody>
                        </table>

                    </html:form>

                    </div>

                    </div>

                </div>
                 <div class="footermessage">
         www.fasylgroup.com
     </div>
                <c:import url="/footer.jsp"/>

            </div>

  </body>

</html:html>
 <%
                             String failure=(String)session.getAttribute("failure");


                              if(failure!=null){
                              out.print(" <script type='text/javascript'>" +

                                      "dhtmlmodal.open('EmailBox', 'iframe', './error.jsp', 'Processing', 'width=290px,left=100px,top=200px,center=1,resize=1,scrolling=0');"+
                                      "</script>");
                              }
                             session.invalidate();
                              %>

