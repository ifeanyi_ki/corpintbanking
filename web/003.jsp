<%-- 
    Document   : 003
    Created on : Jul 13, 2011, 5:34:34 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%

    int timeout = session.getMaxInactiveInterval();
    response.setHeader("Refresh", timeout + "; URL =" + request.getContextPath() + "/Login.jsp");
%>

<%
    String newpassword = request.getParameter("newpasswd");
    String confirmpassword = request.getParameter("confnewpasswd");
    String passcodes = request.getParameter("passcode");
    String question = request.getParameter("question");
    String answer = request.getParameter("answer");

    System.out.print("Ayo: Got here");
    com.fasyl.ebanking.main.User user = (com.fasyl.ebanking.main.User) session.getAttribute("user");
    System.out.print("Ayo: Got here2");
    String syspasscode = "";
//request.getSession();
    String updPasswd = "update SM_USER_ACCESS set cod_usr_pwd=?,"
            + " question_id='" + question + "',"
            + " answer='" + answer + "',"
            + "flg_status='A',login_failiure_count=0,"
            + "modified_date=sysdate,modified_by='" + user.getUserid() + "' where cod_usr_id=? ";

    try {
        java.sql.Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
        java.sql.PreparedStatement pstmt = con.prepareStatement("select passcode from SM_USER_ACCESS where cod_usr_id=?");

        pstmt.setString(1, user.getUserid());
        System.out.print("Ayo: Got here3: " + user.getUserid());
        java.sql.ResultSet rs = pstmt.executeQuery();
        System.out.print("Ayo: Got here4");

        if (rs.next()) {
            syspasscode = rs.getString("PASSCODE");
            System.out.print("Ayo: Got here5: " + syspasscode);
            System.out.println(com.fasyl.ebanking.util.Encryption.decrypt(syspasscode));

            System.out.print("Ayo: Got here6");
        }
        rs.close();
        pstmt.close();
        con.close();
    } catch (Exception ex) {
        ex.printStackTrace();
    } finally {

    }
    System.out.println(" " + newpassword + confirmpassword + " ff " + user.getUserid() + " testing " + passcodes);
    if (newpassword != null && passcodes != null) {
        System.out.println("<<<<<<<<<<<<<< hmmmmmmmmm>>>>>>>>>>> ");
        if (!(newpassword.equals(confirmpassword))) {
            System.out.println("<<<<<<<<<<<<<< inside paassword error.jsp>>>>>>>>>>> ");
            request.setAttribute("msg_text", "The user password  does not match");
            request.setAttribute("msg_model_type", "error");
            request.setAttribute("msg_ok_target", "0001.jsp");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
            dispatcher.forward(request, response);
            return;
        } else if (!(passcodes.equals(com.fasyl.ebanking.util.Encryption.decrypt(syspasscode)))) {
            System.out.println("<<<<<<<<<<<<<< passcode error.jsp>>>>>>>>>>> ");
            request.setAttribute("msg_text", "The passcode is not correct");
            request.setAttribute("msg_model_type", "error");
            request.setAttribute("msg_ok_target", "0001.jsp");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
            dispatcher.forward(request, response);
            return;
        } else if (passcodes == null || newpassword == null || confirmpassword == null) {
            request.setAttribute("msg_text", "One of the field is empty");
            request.setAttribute("msg_model_type", "error");
            request.setAttribute("msg_ok_target", "0001.jsp");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
            dispatcher.forward(request, response);
            return;
        }

        String password = com.fasyl.ebanking.util.Utility.genaratePassword();
        java.sql.Connection con = null;
        java.sql.PreparedStatement pstmt = null;

        System.out.println("Before encryption " + password);
        newpassword = com.fasyl.ebanking.util.Encryption.encrypt(newpassword);
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement(updPasswd);
            pstmt.setString(1, newpassword);
            pstmt.setString(2, user.getUserid());
            int result = pstmt.executeUpdate();
            if (result > 0) {
                request.setAttribute("msg_text", "You have successfully changed your password");
                request.setAttribute("msg_model_type", "message");
                request.setAttribute("msg_ok_target", "Login.jsp");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
                dispatcher.forward(request, response);
                return;
            } else {
                request.setAttribute("msg_text", "error while changing password");
                request.setAttribute("msg_model_type", "error");
                request.setAttribute("msg_ok_target", "Login.jsp");
                RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
                dispatcher.forward(request, response);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            try {
                pstmt.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
    } else {
        request.setAttribute("msg_text", "error while validating your credentials");
        request.setAttribute("msg_model_type", "error");
        request.setAttribute("msg_ok_target", "Login.jsp");
        RequestDispatcher dispatcher = request.getRequestDispatcher("/complexerror.jsp");//context.getRequestDispatcher(sTarget);
        dispatcher.forward(request, response);
        return;
    }
%>