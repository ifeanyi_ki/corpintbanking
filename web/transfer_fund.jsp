<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Transfer Fund</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <div class="content-box-header">

                <h3>Third Party Fund Transfer</h3>

                <div class="clear"></div>

            </div> 

            <%!
                String account;
                String result;
                String trans_limit;
            %>

            <%
                account = (String) request.getAttribute("account");
                trans_limit = (String) request.getAttribute("trans_limit");
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");
                out.println(result);

            %>
            <br/>
            <h3>
                FUND TRANSFER FROM ACCOUNT <%=account%>  
                YOUR TRANSACTION LIMIT FOR THIS ACCOUNT IS N<%=trans_limit%></h3>
            <br/>

            <form>
                <p>
                    <label>Beneficiary Account Name</label>
                    <input class="text-input small-input" type="text" value ="" id ="ben_acct_name" >
                </p>
                <p>
                    <label>Beneficiary Account</label>
                    <input class="text-input small-input" type="text" value ="" id ="ben_acct">
                </p>
                <p>
                    <label>Amount</label>                 
                    <input class="text-input small-input" type = "text" value ="" id = "amount">
                </p>

                <p>
                    <label>Narration</label>
                    <input class="text-input small-input" type = "text" value ="" id = "narration">
                    <input type = "hidden" value ="" id = "token">
                </p>


                <input type="hidden"   value ="<%= account%>" id ="account" />   
                <input type="hidden"   value ="<%= trans_limit%>" id = "trans_limit" />  
                <input type="button" class="button" value="SUBMIT" onClick="submit_transfer('submit_transfer')"   />  

            </form>
        </table><br/>
    </div>
</div>         

</body>
</html>
