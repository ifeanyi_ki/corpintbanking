

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>JSP Page</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body>

        <form>              
            <p>
                <span style="font-weight: bold;">Enter User Id or User's First name:  </span>
                <input class="text-input small-input" type="text"  value="" id="id_name"  required/>
                
            </p>
            <br/>
            <center>
                <input  type="button"  class="button" value="Search" onClick="search('search_user_details')" >
            </center>              
        </form>


    </body>
</html>
