<%@page import="com.fasyl.ebanking.main.DataObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <meta http-equiv="refresh" content="<%= session.getMaxInactiveInterval()%>; /CorpIntBanking/Login.jsp">
        <title>Manage Account</title>
        <%
        
           int timeout = session.getMaxInactiveInterval();
           response.setHeader("Refresh", timeout + "; URL ="+request.getContextPath()+"/Login.jsp");
        %>
    </head>
    <body id="background">

        <div  class="tab-content" id="tab2" align="left">
            <%!
                String user_id;
                String result;
                String account;
            %>
            <%
                result = (String) request.getAttribute("result") == null ? "" : (String) request.getAttribute("result");

                account = request.getParameter("account");
                if (account == null) {
                    account = (String) request.getAttribute("account");
                }
                user_id = (String) session.getAttribute("user_id");
                List<String> initiators = new DataObject().getList("select userid from tb_user_access where created_by  = '" + user_id + "' and role_id = 'INITIATOR'");
                List<String> lowLevAuths = new DataObject().getList("select userid from tb_user_access where created_by  = '" + user_id + "' and role_id = 'Low Level Authorizer'");
                List<String> hiLevAuths = new DataObject().getList("select userid from tb_user_access where created_by  = '" + user_id + "' and role_id = 'High Level Authorizer'");
            %>

            <div class="content-box-header">

                <h3>Management Panel For Account Number: <%= account%></h3>

                <div class="clear"></div>

            </div>

            <div class="content-box-header">

                <h3>Add Initiator</h3>

                <div class="clear"></div>

            </div>
                <h3><% out.println(result);%></h3>
            <form>
                <p>
                    <label>Initiator:</label>
                    <select type="text" value="<%=initiators%>" id ="initiator" required>
                        <option value="" >-- Select Initiator --</option>
                        <%
                            for (int i = 0; i < initiators.size(); i++) {
                        %>
                        <option value="<%= initiators.get(i)%>"><%=initiators.get(i)%></option>
                        <%}%>
                        <select/>
                </p>
                <br/>
                <p>
                    <label>Transaction Limit: </label>
                    <input type="text" class="text-input small-input" value="" id="trans_limit"  required/>
                    <input type="hidden"  value="<%= account%>" id="account"  required/>
                </p>
                <br/>
                <p>
                    <input type="button"  class="button" value="Submit" onClick="addtoinitmandate('init_mandate')" />
                </p>
            </form>
            <br/>
            <input href="#" type="button" class="button" value="View Existing Initiators" onClick="viewinitiators('view_initiators', '<%= account%>')" />
            <br/><br/>

            <div class="content-box-header">

                <h3>Set Up Mandate</h3>

                <div class="clear"></div>

            </div>

            <form>
                <p>
                    <label>Lower Limit:</label> 
                    <input class="text-input small-input" type="text"  value="" id="lo_limit"  required/>
                </p>
                <br/>
                <p>
                    <label>Upper Limit:</label>
                    <input class="text-input small-input" type="text"  value="" id="up_limit"  required/> 
                    <input class="text-input small-input" type="hidden"  value="<%= account%>" id="account"  required/>
                </p>

                <br/>
                <p>
                    <label>Operator</label>
                    <select type="text" value="" id ="operator" required>
                        <option value="" >-- Select Operator --</option>
                        <option value="ONLY">ONLY</option>
                        <option value="AND">AND</option>
                        <option value="OR">OR</option>
                    </select>
                </p>

                <br/>
                <p>
                    <input type="button"  class="button" value="Submit" onClick="setupmandate('setupmandate')" /> 
                </p>

            </form>
                
            <br/><br/>
            <input href="#" class="button" value="View Existing Mandates" onClick="getAcctDetails('view_mandates', '<%= account%>')" />


        </div>

    </body>
</html>
