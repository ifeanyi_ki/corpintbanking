<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tile" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%
%>
<html xmlns="http://www.w3.org/1999/xhtml"  >
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Internet Banking</title>
        <style type="text/css">
            body{
                margin: 5px 5px 5px 5px;
}
        </style>
        <script type="text/javascript">


        </script>


        <script language=JavaScript src="screens/scripts/mail.js" type="text/javascript"></script>
        <script language="JavaScript" src="screens/scripts/resources/scripts/jquery-1.5.1.js" type="text/javascript"></script>
        <script language=JavaScript src="screens/scripts/resources/scripts/jquery.form.js" type="text/javascript"></script>
        <link rel="stylesheet" href="screens/scripts/jquery-ui-1.8.13.custom/css/smoothness/jquery-ui-1.8.13.custom.css" type="text/css" />
        <script language=JavaScript src="screens/scripts/js/jquery-ui-1.8.13.custom/jquery-ui-1.8.13.custom.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="screens/scripts/transaction.js"></script>

        <link href="screens/scripts/SyntaxHighlighter.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="screens/scripts/resources/css/reset.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="screens/scripts/resources/css/stylenews.css" type="text/css" media="screen" />
        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="screens/scripts/resources/css/invalid.css" type="text/css" media="screen" />

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/simpla.jquery.configuration.js"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/facebox.js"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="screens/scripts/resources/scripts/jquery.wysiwyg.js"></script>


        <!--[if IE]><script type="text/javascript" src="resources/scripts/jquery.bgiframe.js"></script><![endif]-->


    </head>

    <body ><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
            <div >
                <c:import url="/menu2.jsp"/>
            </div>


            <div id="main-content">
                <div id="scrloader">
                    <c:import url="/default.jsp"/>
                </div>

                <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">

                </noscript>



                <!-- End Notifications -->

                <div id="footer">

                </div><!-- End #footer -->

            </div> <!-- End #main-content -->


        </div></body>


    <!-- Download From www.exet.tk-->
</html>
