/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//start of global declaration
//---------------------------------------------------------------------------
var req = createRequestObject();
var txndata='<?xml version="1.0"?><ROWSET><DATA>';
var xmlDoc=null;
var seq;
var callBack;
var win;
var canpost=false;
var ajaxwin;
var statuses=true;
var divId ="";
var callbackAftersuccess = false;
var popup = new Array();
popup[0]="101111";
popup[1]="1021";
popup[2]="104111";
popup[3]="3211";
popup[4]="3221";
popup[5]="3231";
popup[6]="3241";
popup[7]="3443";
popup[8]="3421";
popup[9]="4021";
popup[10]="5141";
var addlistWindow;
//--------------------------------------//end of global variable declaration
//function account(acctno,ccy){
//    this.acct=acctno;
//    this.ccy=ccy;
//}

function showinit() {
    //alert(URL);
    window.open('',document.LoginForm.userid.value, 'toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1200,height=800');
    document.LoginForm.target=document.LoginForm.userid.value;
    document.LoginForm.submit();
    document.LoginForm.userid.value="";
    document.LoginForm.password.value="";
// window.openDialog(url, name, features, arg1, arg2)
}


function handlepagination(){
    divId='trx_detail'//divs;
    var taskId='102e'
   var data = document.getElementById("data").value;
    var src =  '/IntBanking/main.jsp?taskId='+taskId+'&data='+data;
     prepareAjaxRequest(divId,true,src);
}
function paginate(total_sizez,whatToCall){
   
    $("#Pagination").pagination(20, {
        items_per_page:5, 
        callback:handlepagination//whatToCall
    });
}
function authorise(taskId,screenId,action,refno,par){

    divId='trx_detail';
     var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&action='+action+'&refno='+refno+'&par='+par;
     prepareAjaxRequest(divId,true,src);
}
function validateSearch(custno,valuex,taskid,screenid){
          var test = false;
          if(custno.length==0 && valuex.length==0){
              jQuery.facebox('<p>You need to enter value into one of the fields</p> ');
              test = false;
          }else{
              test=true
          }
        if(test){
            getUserInfo(taskid,screenid);
        }
       }
function validatePassword(valuex){
    if (valuex.length==0 ){
        jQuery.facebox('You need to enter value in the password field');
        return false;
    }else if (valuex.length<8){
       jQuery.facebox('The entered value length must be between 8 and 35');
       return false;
    }else if (valuex.length>35){
       jQuery.facebox('The entered value length must be between 8 and 35');
       return false;
    }else{
        return true
    }
}

function confirmPassword(newpasswd,confpasswd){
    if (newpasswd!=confpasswd){
        jQuery.facebox("The password did not match");
        return false;
    }else if (!validatePassword(newpasswd)){
        return false;
    }
    else{
        document.forms['passwords'].submit();
        return true; 
    }

}
function addMonthsToDate() {
    / //alert('test2');
    divId = 'trx_detail'
    callBack = 'partialCallback()';
    var startDates=document.getElementById("FirstExecDate").value;
    var tenure=document.getElementById("sitenure").value;
    var src =  '/IntBanking/MainController?screenId='+'3420'+'&taskId='+'001'+'&startDates='+startDates+'&tenure='+tenure+'&branch='+'04'
    preparePartialAjaxRequest(divId,true,src,callBack);
/*var startDate = new Date(startDates);
    alert('test1'+startDate);
    var numMonths=document.getElementById("sitenure").value;
        alert('test2'+numMonths);
    var addYears = Math.floor(numMonths/12);
        alert('test3'+addYears);
    var addMonths = numMonths - (addYears * 12);
        alert('test4'+addMonths);
        alert(startDate.getMonth());
    var newMonth = startDate.getMonth() + addMonths;

    alert(newMonth);
    if (startDate.getMonth() + addMonths > 11) {
      ++addYears;
      newMonth = startDate.getMonth() + addMonths - 12;
    }
    var newDate = new Date(startDate.getYear()+addYears,newMonth,startDate.getDate(),startDate.getHours(),startDate.getMinutes(),startDate.getSeconds());
   alert(newDate);
    // adjust to correct month
    while (newDate.getMonth() != newMonth) {
      newDate = addDaysToDate(newDate, -1);
    }
     alert(newDate);
     var actualDate = newDate.getDate().toString()+'/'+(newDate.getMonth()+1).toString()+'/'+newDate.getFullYear().toString();
    alert(actualDate);
    document.getElementById('SiFinalDate').value=actualDate;
    $('SiFinalDate').attr('disabled','disabled');
    return actualDate;*/
}
function spiners(ids){
    alert('spinner');
    $("#"+ids).spinner({
        min: 1,
        max: 100,
        increment: 1
    });
    $("#"+ids).spinner({
        showOn: 'both'
    });
}
function addDaysToDate(myDate,days) {
    return new Date(myDate.getTime() + days*24*60*60*1000);
}




Array.prototype.contains = function (value,caseSensitive)
// Returns true if the passed value is found in the
// array. Returns false if it is not.
{
    var i;
    for (i=0; i < this.length; i++) {
        // use === to check for Matches. ie., identical (===),
        if(caseSensitive){	//performs match even the string is case sensitive
            if (this[i].toLowerCase() == value.toLowerCase()) {
                return true;
            }
        }else{
            if (this[i] == value) {
                return true;
            }
        }
    }
    return false;
};
//--------------------------------------//end of contains
function pickDate(ids){
    // alert('change')
    //var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
    $( "#"+ids ).datepicker({
        dateFormat:'dd-M-yy'
    } );
	
}
function keyboards(){
    //alert('inside');
    $('input[type=password]').keyboard({
        layout:"qwerty",
        customLayout:
        [["q w e r t y {bksp}","Q W E R T Y {bksp}"],
        ["s a m p l e {shift}","S A M P L E {shift}"],
        ["{accept} {space} {cancel}","{accept} {space} {cancel}"]]
    });
}
function CheckRange(){
    $('#range').show();
    $('#norange').hide();
//document.getElementById('test').style="visibility:visible";
     
}
function changePassword(taskId,screenId){
    var oldpasswd=document.getElementById("oldpasswd").value;
    var newpasswd=document.getElementById("newpasswd").value;
    document.getElementById("oldpasswd").value="";
    document.getElementById("newpasswd").value="";
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&oldpasswd='+oldpasswd+'&newpasswd='+newpasswd ;
    prepareAjaxRequest('trx_detail',false,src);
//$('#passwd').hide();

}
//============================ Start Beneficiary Scripts
function fundTransfer(taskId,screenId){
    //alert()
    var ftlimt=document.getElementById("ftlimt").value;
    var ftdebitacc=document.getElementById("ftdebitacc").value;
    var benacct=document.getElementById("benacct").value;
    var DestAcctNo=document.getElementById("DestAcctNo").value;
    var ccy=document.getElementById("ccy").value;
    var amt=document.getElementById("amt").value;
    var token=document.getElementById("token").value;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&ftlimt='+ftlimt+'&ftdebitacc='+ftdebitacc+'&DestAcctNo='+DestAcctNo+'&ccy='+ccy+'&amt='+amt;
    prepareAjaxRequest('scrloader',false,src);

}
function hidediv(divid,hide){
    if (hide){
        $('#'+divid).hide();
    }
    else{
        $('#'+divid).show();
        $('#passwdbutton').hide();
    }
    keyboards();
}



function updateCCY(acct,ids){
     
    var ccys = $('option[value='+acct+']').data("ccys");
    document.getElementById(ids).value=ccys;
}
function bentab(taskId,screenId){
    
    //$('#trx_details').hide();
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId

    prepareAjaxRequest('trx_detail',true,src);

 
}
function shownew(taskId,screenId){
    var result = true;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId ;
    prepareAjaxRequest('scrloader',result,src);
}
function bulkTransfer(taskId,screenId){
    //$('#trx_details').show();
    //alert('inside');
    var result = true;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId;

    prepareAjaxRequest('trx_detail',result,src);
}
function selBenType(screenId){
    var result = true;
    var benType=document.TxnFrm.selectbeneficiary.value;
    var taskId;
    if (benType=='IND'){
        taskId = 6011;
    }else{
        taskId=60120
    }
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId;
    prepareAjaxRequest('scrloader',result,src);
}
function getBeneficiaryDetail(valuex,taskId,screenId){
    var result = false;
    var beneficiaryid=document.getElementById('beneficiaryid').value;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&beneficiaryid='+beneficiaryid+"&beneficiaryacct="+valuex;
    prepareAjaxRequest('trx_detail',result,src);
}
function modifyBeneficiary(taskId,screenId){
    var beneficiaryname = document.getElementById("beneficiaryname").value;
    var beneficiaryacct = document.getElementById("beneficiaryacct").value;
    var institution = document.getElementById("institution").value;
    var description = document.getElementById("description").value;
    var beneficiarygrp = document.getElementById("beneficiarygrp").value;
    var txnamount = document.getElementById("txnamount").value;
    var bentype = document.getElementById("bentype").value;
    var oldbeneficiarygrp = document.getElementById("oldbeneficiarygrp").value;
    var oldbeneficiaryacct = document.getElementById("oldbeneficiaryacct").value;
    var beneficiaryid = document.getElementById("beneficiaryid").value;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+
    '&beneficiaryname='+beneficiaryname+'&beneficiaryacct='+beneficiaryacct+
    '&beneficiaryid='+beneficiaryid+'&institution='+institution+'&description='
    +description+'&beneficiarygrp='+beneficiarygrp+"&bentype="+bentype
    +"&oldbeneficiaryacct="+oldbeneficiaryacct+"&oldbeneficiarygrp="+oldbeneficiarygrp+'&txnamount='+txnamount;
    prepareAjaxRequest('trx_detail',false,src);
}
function listbeneficiaries(taskId,screenId){
    divId='trx_detail';
    var result = true;
    var selectbengrp=document.TxnFrm.selectbengrp.value;
    var bentype=document.TxnFrm.bentype.value;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&beneficiarygrp='+selectbengrp+'&bentype='+bentype;
    if(taskId==60120){
        divId="scrloader";
    }else if(taskId ==60211){
        result=false;
    }
    prepareAjaxRequest(divId,result,src);
}
function addBeneficiaries(taskId,screenId){
    //alert('enter');
    divId = 'scrloader';
    var beneficiaryid = null;
    var txnamount = null;
    var beneficiaryname = document.getElementById("beneficiaryname").value;
    var beneficiaryacct = document.getElementById("beneficiaryacct").value;
    //var SourceAccount = document.getElementById("SourceAccount").value;
    var institution = document.getElementById("institution").value;
    var description = document.getElementById("description").value;
    var bentype = document.getElementById("bentype").value;
    //var description = document.getElementById("description").value;
    var result = false;
    var beneficiarygrp=null;
    //alert('out');
    if (taskId=="60111" ||taskId=="601221" ){
        //divId='trx_detail';
        if (taskId=="601221"){
            divId='trx_detail';
            beneficiaryid = document.getElementById("beneficiaryid").value;
            txnamount = document.getElementById("txnamount").value;
        }
        beneficiarygrp = document.getElementById("beneficiarygrp").value;
    }
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&beneficiaryname='+beneficiaryname+'&beneficiaryacct='+beneficiaryacct+'&institution='+institution+'&description='+description+'&beneficiarygrp='+beneficiarygrp+"&bentype="+bentype+"&beneficiaryid="+beneficiaryid+'&txnamount='+txnamount;
    prepareAjaxRequest(divId,result,src);
}
//====================================End Beneficiary Scripts
//
//
//
//

// ========================start fundtransfer scripts
function getPin(taskId,screenId){

    //prompt('You require token for this transaction,click generate to PROCEED');
    //var htmls = "";
    // jQuery.facebox='<b>You require token for this transaction,click generate to PROCEED</b>'
    var result = false;
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId;
    prepareAjaxRequest('trx_details',result,src);

}
function softKeyboard(ids){
    // alert('inside softKeyboard');
    Keyboard(ids, {
        showOn:'focus',
        showAnim:'fadeIn',
        keypadOnly:false,
        layoutName:'alphabetic'
    });
    PrimeFaces.widget.Keyboard(ids, {
        showOn:'focus',
        showAnim:'fadeIn',
        keypadOnly:false,
        layoutName:'alphabetic'
    });
}
function escapeClientId(a){

    // alert('inside primefaces');
    return"#"+a.replace(/:/g,"\\:")
}
/*function Keyboard(b,a){
    alert('here enter');
    this.id=b;
    this.cfg=a;
    this.jqId=escapeClientId(b);
    this.jq=jQuery(this.jqId);
    alert('here1');
    if(this.cfg.layoutTemplate){
        alert('here2');
        this.cfg.layout=PrimeFaces.widget.KeyboardUtils.createLayoutFromTemplate(this.cfg.layoutTemplate)
    }else{
        this.cfg.layout=PrimeFaces.widget.KeyboardUtils.getPresetLayout(this.cfg.layoutName)
    }
    this.jq.keypad(this.cfg);
    if(this.cfg.behaviors){
        alert('here3');
        PrimeFaces.attachBehaviors(this.jq,this.cfg.behaviors)
    }
    if(this.cfg.theme!=false){
        PrimeFaces.skinInput(this.jq)
    }
};*/
function setFormAction(fileType){
    if (fileType=='pdf'){
        //alert('why1');
        document.frmStmtInq.action="screens/1006b.jsp?";
        //alert('why2');
        document.frmStmtInq.submit();
    }else if (fileType=='excel'){
        document.frmStmtInq.action="screens/1006d.jsp?"
        document.frmStmtInq.submit();
    }else if (fileType=='rtf'){
        document.frmStmtInq.action="screens/1006c.jsp?"
        document.frmStmtInq.submit();
    }
}
function setupfts(taskId,screenId){
    //alert('inside');
    var result = true;
    var ftminimumamount;
    var institutionId;
    var institution;
    var ftmaxamount;
    var usebeneficiaryciaryonly;
    var refno;
    if (taskId=='33112'){
   divId='trx_detail1';
        result = false
        ftminimumamount = document.getElementById('ftminimumamount').value;
        institutionId = document.getElementById('institutionId').value;
        institution = document.getElementById('institution').value;
        ftmaxamount = document.getElementById('ftmaxamount').value;
        refno = document.getElementById('refno').value;
        if (document.setup.usebeneficiaryciaryonly.checked){
            usebeneficiaryciaryonly = 'Y';
        }else{
            usebeneficiaryciaryonly = 'N';
        }
        alert('done');
    }
    
    if(taskId=='33111'){
        
        divId='trx_detail1';
        result=false;
    }
    //alert('outside');
    //alert(usebeneficiaryciaryonly);
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&ftminimumamount='+ftminimumamount+'&ftmaxamount='+ftmaxamount+'&institutionId='+institutionId+'&usebeneficiaryciaryonly='+usebeneficiaryciaryonly+'&institution='+institution+"&refno="+refno;
    prepareAjaxRequest(divId,result,src);
}

function updateDestAcct(){
    var beneficiary = document.getElementById("benacct").value;
    var ftdebitacc = document.getElementById('ftdebitacc').value;
    if (beneficiary!='undefined'||beneficiary!=''){
        if(ftdebitacc==''){
            alert('You need to enter value into FROM field ');
            document.getElementById("ftdebitacc").focus();
            document.getElementById("benacct").value='';
            return ;
        }
        if(ftdebitacc==beneficiary){
            alert('From field can not be equal to beneficiary field');
            document.getElementById("ftdebitacc").focus();
            document.getElementById("benacct").value='';
            return;
        }

        document.getElementById("DestAcctNo").focus();
        document.getElementById("DestAcctNo").value=beneficiary;
        document.getElementById("amt").focus();
    }
//return true;
}

//=========================end fundtransfer scripts
function prepareAjaxRequest(div,stat,src){
    statuses=stat;
    divId=div;
    document.getElementById(divId).innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}
function preparePartialAjaxRequest(div,stat,src,callBack){
    // alert('inside prepare aajax request');
    statuses=stat;
    divId=div;
    document.getElementById(divId).innerHTML='Processing User Request';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxPartialRequest
}
//-----------------start of loan scripts-----------------------
function getLoanDetail(refno,taskId,screenId){
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&refno='+refno;
    statuses=true;
    divId='scrloader';
    document.getElementById('scrloader').innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}

function listRepayment(refno,taskId,screenId){
    var src =  '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&refno='+refno;
    statuses=true;
    divId='scrloader';
    document.getElementById('scrloader').innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}
//----------------------------end of loan scripts--------------
function addaccount(taskId,screenId,isAddAccount,action){
    var customerno=document.getElementById('customerno').value;
    var acct_no=document.getElementById('acct_no').value;
    var userid = document.getElementById("userid").value;
    var actiondesc;
    var tablename = 'cust_acct';
    var tablecolumn='status';
    actiondesc  = document.getElementById("actionendesc").value; 
    alert(actiondesc||' '||tablecolumn||' ' ||tablename );
    var src = '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&customerno='+customerno+'&acct_no='+acct_no+"&isAddAccount="+isAddAccount+"&action="+action+"&actiondesc="+actiondesc+"&tablename="+tablename+'&tablecolumn='+tablecolumn+"&userid="+userid;
    statuses=false;
    divId='scrloader';
    document.getElementById('scrloader').innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}
function getAccountToAddForm(value,taskId,screenId){
    //alert('inside getAccountToAddForm ');
    var customerno=value;
    var src = '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&customerno='+customerno;
    statuses=true;
    divId='scrloader';
    document.getElementById('scrloader').innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}
function addAccount(){
    var result = '';
    // alert(document.frmmain.removedaccountlist.length);
    for (var i = 0; i <= document.frmmain.removedaccountlist.length; i++) {
        if (document.frmmain.removedaccountlist.options[i].selected) {
            var li = document.createElement("option");
            var li2 = document.createTextNode(document.frmmain.removedaccountlist.options[i].text);
            li.appendChild(li2);
            li.setAttribute("value",document.frmmain.removedaccountlist.options[i].text );
            var addedaccountlist = document.getElementById("addedaccountlist");
            addedaccountlist.appendChild(li);
            var removedaccountlist = document.getElementById("removedaccountlist");
            removedaccountlist.removeChild(document.frmmain.removedaccountlist.options[i]);
        //alert('done');

        }

    }

    return result;
}

function getAccounts(){
    alert('inside getAccounts');
    var result = '';
    var size = document.frmmain.addedaccountlist.length;
    alert(size);
    for (var i = 0; i <size; i++) {
        alert('generating result');
        if (i<size-1)
            result += document.frmmain.addedaccountlist.options[i].text+';';
        else
            result += document.frmmain.addedaccountlist.options[i].text;
    }
    if(result.length==0 || result==''){
        jQuery('You have to select at least one account number');
        canpost=false;
    }
    alert(result);
    return result;
}

function removeaccount(){
    var result = '';
    for (var i = 0; i < document.frmmain.addedaccountlist.length; i++) {
        if (document.frmmain.addedaccountlist.options[i].selected) {
            var li = document.createElement("option");
            var li2 = document.createTextNode(document.frmmain.addedaccountlist.options[i].text);
            li.appendChild(li2);
            li.setAttribute("value",document.frmmain.addedaccountlist.options[i].text );
            var removedaccountlist = document.getElementById("removedaccountlist");
            removedaccountlist.appendChild(li);
            var addedaccountlist = document.getElementById("addedaccountlist");
            addedaccountlist.removeChild(document.frmmain.addedaccountlist.options[i]);
        //alert('done');

        }

    }

    return result;
}
function viewMail(messageId){

    //alert('inside viewMail');
    divId='trx_detail';
    var  flg_box = document.viewInboxMail.flg_box.value;
    //alert(flg_box);
    var  screenId = document.viewInboxMail.screenId.value;
    var  reqType = document.viewInboxMail.reqType.value;

    var  taskId = document.viewInboxMail.taskId.value;
    //alert(taskId);
    var src = '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&flg_box='+flg_box+'&messageId='+messageId+'&reqType='+reqType;
    statuses=true;
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest
}
function hidebutton(){
    $('#submitbutton').hide();
}

function forward(){
    $('#forwardbutton').hide();
    $('#submitbutton').show();
    document.frmComMain.fldToMsg.value='';
    document.frmComMain.fldCcMsg.value='';

}
function inbox(){
    divId = 'trx_detail';
    var src = '/IntBanking/MainController?screenId=514&taskId=5142&flg_box=I'
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    //alert('after clearing');

    //alert('about to post');
    statuses=true;
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest

}
function sent(){

    divId = 'trx_detail';
    var src = '/IntBanking/MainController?screenId=514&taskId=5144&flg_box=S'
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    //alert('after clearing');

    //alert('about to post');
    statuses=true;
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest

}
function draft(){

    divId = 'trx_detail';
    var src = '/IntBanking/MainController?screenId=514&taskId=5143&flg_box=D'
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    //alert('after clearing');
    statuses=true;
    //alert('about to post');
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest

}
function compose(){
    divId = 'trx_detail';
    var src = '/IntBanking/MainController?screenId=514&taskId=5140'
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    //alert('after clearing');
    statuses=true;
    //alert('about to post');
    req.open("POST",src , true);
    req.send(null);
    //alert()
    req.onreadystatechange = sendAjaxRequest

}
function save(taskId,screenId){

    //alert('inside save');
    canpost=sends();
    //alert(canpost+' inside function send');
    if(!canpost)return false;

    var fldToMsg=document.frmComMain.fldToMsg.value;
    // var fldFlgBox=document.frmComMain.fldFlgBox.value;
    //var fldReqType=document.frmComMain.fldReqType.value;
    var mode='2';
    var flg_box='D';
    var verify=document.frmComMain.verify.value;
    var userid=document.frmComMain.userid.value;
    //var fldfromMsg=document.frmComMain.fldfromMsg.value;
    var fldCcMsg=document.frmComMain.fldCcMsg.value;
    var fldSubject=document.frmComMain.fldSubject.value;
    var fldTxt=document.frmComMain.fldTxt.value;
    if(popup.contains(taskId,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    //alert('about to set trx_detail');
    divId = 'trx_detail';
    document.getElementById(divId).innerHTML='<b>Loading -----</b>';
    var src  = '/IntBanking/MainController?screenId='+screenId+"&taskId="+taskId+"&fldToMsg="+fldToMsg+"&fldCcMsg="+fldCcMsg+"&mode="+mode+"&flg_box="+flg_box+"&verify="+verify+"&fldSubject="+fldSubject+"&fldTxt="+fldTxt+"&userid="+userid;
    //alert(src)
    jQuery.facebox='<b>Processing User Request------</b>'
    //document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    //alert('after clearing');
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = sendAjaxRequest
    return true;
}
function validateMailId(
    p_control
    ){

    // alert('inside validatemailid');
    var lUser;
    var	userArray	= new Array ();

    lUser = p_control.value;
    //alert(lUser)
    if (lUser != "") {
        //alert('field is not null')
        userArray = lUser.split(";");
    //alert (userArray);
    }
    if (! checkValue (userArray)) {
        alert('checkvalue failed');
        p_control.focus ();
        return false;
    }
    //alert('about to continue');
    if (lUser != "") {
        for (l_i = 0; l_i <(userArray.length); l_i++) {
            //alert('checking luser array 4 validation');
            if(userArray[l_i] == " "){
                alert ("Space between mail id's is not allowed.");
                p_control.focus ();
                return false;
            }
            //alert('why');
            //alert(document.frmComMain.userid.value);
            var loginuserid = document.frmComMain.userid.value;
            if (userArray[l_i] == loginuserid) {
                alert ("User cannot send mail to himself.");
                p_control.focus ();
                return false;
            }
            //alert('why2');
            if (userArray [l_i].length > 50) {
                alert ("The length of a single user id cannot be more than 50.");
                p_control.focus ();
                return false;
            }
        }
    }
    //alert('alert about to return');
    return true;
}
function checkValue (
    p_arr
    ) {
    //alert('Send message to Administrator?');
    for (var l_i = 0; l_i < p_arr.length; l_i++) {
        if (p_arr [l_i] == "") {
            if (l_i != (p_arr.length - 1)) {
                //alert ("Please enter correct mail id.");
                jQuery.facebox("Please enter correct mail id.");
                return false;
            }
        }
    }
    return true;
}

function sends ()
{        //alert('start');

    //setRequestId ();
    var l_i	= 0;
    var	l_user_to_arr	= new Array ();
    var	l_user_cc_arr	= new Array ();
    var	l_user_to_list	= "";
    var	l_user_cc_list	= "";

    if(!(validateMailId(document.frmComMain.fldToMsg))){
        jQuery.facebox('Field \'To\' can not be empty');
        return false;
    }
    if(!(validateMailId(document.frmComMain.fldCcMsg))){
        jQuery.facebox('Field \'CC\' can not be empty');
        return false;
    }
    //alert(document.frmComMain.fldToMsg.value + ' without trim');
    //alert(trim (document.frmComMain.fldToMsg.value) + ' with trim');
    if (document.frmComMain.fldToMsg.value == "")
    {
        //alert ("There are no mail ids in the To list.");
        jQuery.facebox("<b>There are no mail ids in the To list.</b>");
        document.frmComMain.fldToMsg.focus ();
        return false;
    }
    if (document.frmComMain.fldCcMsg.value == "")
    {
        //alert ("There are no mail ids in the To list.");
        jQuery.facebox("<b>There are no mail ids in the CC list.</b>");
        document.frmComMain.fldToMsg.focus ();
        return false;
    }
    if (document.frmComMain.fldSubject.value == "")
    {
        //alert ("Please enter the subject.");
        jQuery.facebox("<b>Please enter the subject.</b>");
        document.frmComMain.fldSubject.focus ();
        return false;
    }
    //alert('oga ooooo'+document.frmComMain.fldTxt.value);
    if (document.frmComMain.fldTxt.value == "")
    {
        //alert ("please enter message");
        jQuery.facebox("<b>please enter message</b>");
        document.frmComMain.fldTxt.focus ();
        return false;
    }
    //alert('o ti fe sumi');
    if (!checkInput (document.frmComMain.fldToMsg)) {
        return false;
    }
    if (document.frmComMain.fldCcMsg.value != "") {
        if (!checkInput (document.frmComMain.fldCcMsg)) {
            return false;
        }
    }
    if (!checkInput (document.frmComMain.fldSubject)) {
        return false;
    }
    if (!checkInput (document.frmComMain.fldTxt)) {
        return false;
    }

    l_user_to_list = document.frmComMain.fldToMsg.value;
    l_user_cc_list = document.frmComMain.fldCcMsg.value;
    l_user_to_arr = l_user_to_list.split(";");
    if (l_user_cc_list != "") {
        l_user_cc_arr = l_user_cc_list.split(";");
    }
    /*
	if (l_user_cc_arr.length &gt; 0) {
		for (l_i = 0; l_i &lt; l_user_to_arr.length; l_i++) {
			var l_j	= 0;
			for (l_j = 0; l_j &lt; l_user_cc_arr.length; l_j++) {
				if (l_user_cc_arr [l_j] == l_user_to_arr [l_i])
				{
					alert ("The user " + l_user_cc_arr [l_j]
						+ " cannot exist in both the To list and the CC list");
					document.frmComMain.fldCcMsg.focus ();
					return false;
				}
			}
		}
	}
*/
    //	document.frmhidden.fldScrnSeqNbr.value = "13";
    //document.frmhidden.fldScrnSeqNbr.value = "06";
    /*document.frmhidden.fldToMsg.value =  //commented by Tope
				 (document.frmComMain.fldToMsg.value);
	document.frmhidden.fldCcMsg.value =
				(document.frmComMain.fldCcMsg.value);
	document.frmhidden.fldSubject.value =
				(trim (document.frmComMain.fldSubject.value));
	document.frmhidden.fldTxt.value =  (trim (document.frmComMain.fldTxt.value.substring(0,2000)));
	document.frmhidden.fldMode.value = "1";
//	document.frmhidden.fldType.value = "V";
	document.frmhidden.fldType.value = "S";*/

    //document.frmhidden.target="_self";
    //alert('about to close any open addlistwindow');
    if (addlistWindow != null) {
        if (! addlistWindow.closed) {
            addlistWindow.close ();
        }
    }
    //alert('about to return');
    return true;
}
function checkInput (p_obj)
{    //alert('inside check input');
    for (var l_i = 0; l_i < p_obj.value.length; l_i++) {
        if (p_obj.value.charAt(l_i) == '"') {
            alert("Please do not enter double quotes");
            p_obj.focus ();
            return false;
        }
    }
    //alert('about to return');
    return true;
}
function send(taskId,screenId){
    //alert('inside send');
    canpost=sends();
    //alert(canpost+' inside function send');
    if(!canpost)return false;

    var fldToMsg=document.frmComMain.fldToMsg.value;
    // var fldFlgBox=document.frmComMain.fldFlgBox.value;
    //var fldReqType=document.frmComMain.fldReqType.value;
    var mode=document.frmComMain.mode.value;
    var flg_box=document.frmComMain.flg_box.value;
    var verify=document.frmComMain.verify.value;
    var userid=document.frmComMain.userid.value;
    //var fldfromMsg=document.frmComMain.fldfromMsg.value;
    var fldCcMsg=document.frmComMain.fldCcMsg.value;
    var fldSubject=document.frmComMain.fldSubject.value;
    var fldTxt=document.frmComMain.fldTxt.value;
    if(popup.contains(taskId,false)){
        statuses=false;
    }
    else{
        statuses=true;
    }
    //alert('about to set trx_detail');
    divId = 'trx_detail';
    var src  = '/IntBanking/MainController?screenId='+screenId+"&taskId="+taskId+"&fldToMsg="+fldToMsg+"&fldCcMsg="+fldCcMsg+"&mode="+mode+"&flg_box="+flg_box+"&verify="+verify+"&fldSubject="+fldSubject+"&fldTxt="+fldTxt+"&userid="+userid;
    //alert(src)
    //jQuery.facebox='<b>Loading------</b>'
    //document.getElementById('trx_detail').innerHTML='Loading - - - -';
    //alert('after clearing');
    document.getElementById(divId).innerHTML='<b>Processing User Request -----</b>';
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = sendAjaxRequest
    return true;
}
function initz(){
    //alert('inside initz');
    $('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
    $('ul.content-box-tabs li a.default-tab').addClass('current'); // Add the class "current" to the default tab
    $('.content-box-content div.default-tab').show(); // Show the div with class "default-tab"

    $('.content-box ul.content-box-tabs li a').click( // When a tab is clicked...
        function() {
            $(this).parent().siblings().find("a").removeClass('current'); // Remove "current" class from all tabs
            $(this).addClass('current'); // Add class "current" to clicked tab
            var currentTab = $(this).attr('href'); // Set variable "currentTab" to the value of href of clicked tab
            $(currentTab).siblings().hide(); // Hide all content divs
            $(currentTab).show(); // Show the content div with the id equal to the id of clicked tab
            return false;
        }
        );
}

function getAppError(screenId,taskId){
    var to = document.getElementById("to").value;
    var from = document.getElementById("from").value;
    var src  = '/IntBanking/MainController?screenId='+screenId+"&taskId="+taskId+"&to="+to+"&from="+from;
    document.getElementById("trx_detail").innerHTML='Processing User Request .........'
    //alert(src);
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange2;
}

function validateDate(){

}

function createMessage(taskId,screenId){
    var languageid = document.getElementById('languageid').value;
    var msgvalidfrom = document.getElementById('msgvalidfrom').value;
    var msgvalidto = document.getElementById('msgvalidto').value;
    var subject = document.getElementById('subject').value;
    var message = unescape(document.getElementById('message').value.trim());
    var custspecific = document.getElementById('custspecific').value;
    alert(custspecific);
    var src = '/IntBanking/MainController?screenId='+screenId+'&taskId='+taskId+'&languageid='+languageid+'&msgvalidfrom='+msgvalidfrom+'&msgvalidto='+msgvalidto+'&subject='+subject+'&message='+message+'&custspecific='+custspecific;
    // alert(src);
    if(popup.contains(taskId,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    //alert('about to set trx_detail');
    divId = 'trx_detail';
    document.getElementById('trx_detail').innerHTML='Processing User Request - - - -';
    // alert('after clearing');
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = sendAjaxRequest;
}

function sendAjaxPartialRequest() {
    alert('inside send ajax partial');
    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else if(req.responseText=='0002'){
                alert(req.responseText);
                document.getElementById(divId).innerHTML='';
                var src='./screens/SessionTimeout.jsp';
                //alert('about to call ajaxwin');
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = sendAjaxRequest;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info:', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else if(isnumeric(req.responseText)){
                //alert(req.responseText);
                document.getElementById(divId).innerHTML='';
                src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = sendAjaxRequest;
            //alert('about to call ajaxwin');
            // ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else{
                //document.getElementById('trx_detail').innerHTML='';
                //callBack;
                //alert(req.responseText);
                partialCallback();
            //return req.responseText;
            }





        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}

function partialCallback(){
    alert('inside partialCallback')
    if (statuses){
        if (req.responseText=='bad'){
            document.getElementById(divId).innerHTML='';
            document.getElementById('trx_detail').innerHTML = '<p>Choose a new starting date<p>';
        //hidebutton();
        }else{
            document.getElementById('trx_detail').innerHTML = '';
            document.getElementById('SiFinalDate').value= req.responseText;
        }


    }else{
        //alert(req.responseText);
        document.getElementById(divId).innerHTML='';
        jQuery.facebox(req.responseText);

    }
}
function sendAjaxRequest() {

    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else if(req.responseText=='0002'){
                //alert(req.responseText);
                document.getElementById(divId).innerHTML='';
                var src='./screens/SessionTimeout.jsp';
                //alert('about to call ajaxwin');
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = sendAjaxRequest;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info:', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else if(isnumeric(req.responseText)){
                //alert(req.responseText);
                document.getElementById(divId).innerHTML='';
                src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = sendAjaxRequest;
            //alert('about to call ajaxwin');
            // ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else{
                //document.getElementById('trx_detail').innerHTML='';
                if (statuses){
                    //alert('statuses');
                    //alert(divId);
                    document.getElementById(divId).innerHTML='';
                    document.getElementById(divId).innerHTML = req.responseText;
                    hidebutton();



                }else{
                    //alert(req.responseText);
                    document.getElementById(divId).innerHTML='';
                    jQuery.facebox(req.responseText);

                }
            //return req.responseText;
            }





        }
        else    {
            alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}
function closeAjaxWin(){
    //alert('inside closeAjaxwin');
    ajaxwin.close();
}
function isnumeric(value){

    var checkOK = "0123456789";
    var checkStr = value;
    //var field=document.getElementById(id);
    //field.tabIndex=null;
    if (checkStr==null||checkStr.length==0)
    {
        //alert("Value is required");
        //field.focus();
        return (false);
    }
    var allValid = true;
    var allNum = "";
    for (i = 0;  i < checkStr.length;  i++)
    {
        ch = checkStr.charAt(i);
        for (j = 0;  j < checkOK.length;  j++)
            if (ch == checkOK.charAt(j))
                break;
        if (j == checkOK.length)
        {
            allValid = false;
            break;
        //  document.getElementById(id).focus();
        // return (false);

        }
        if (ch != ",")
            allNum += ch;
    }
    if (!allValid)
    {
        // alert("Please enter only digit characters in the \"numbers\" field.");

        //field.focus();


        return (false);
    }else{
        //canpost=true;
        return (true);
    }
}
function resetme(){

    document.getElementById('scrloader').innerHTML = '';
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function ReqInit(requestxml){
    parse(requestxml);
    req.selectSingleNode("//GLOBALS").appendChild('Nisar');
}




function getTrxDetail(value,taskId,dates){

    var acno=document.getElementById("acno").value;

    //var refno=document.getElementById("refno").value;

    var screenId = document.getElementById("screenId").value;
    //var dates = document.getElementById("dates").value;
    var src ='/IntBanking/MainController?refno='+value+'&acno='+acno+'&taskId='+taskId+"&screenId="+screenId+"&dates="+dates;
    prepareAjaxRequest("trx_detail1",false,src)


}
function onClose(){
    var evt=true;
    var message = 'Are you sure you want to leave?';
    if (typeof evt == 'undefined') {
        evt = window.event;
    }
    if (evt) {
        evt.returnValue = message;
        var src="/IntBanking/MainController?logoff=Y";
        //alert('about to call logoff');
        req.open("POST",src , true);
        req.send(null);

        req.onreadystatechange = function() {

            //alert('onreadychange');
            if (req.readyState == 4)  {

                // only if "OK"
                if (req.status == 200) {
                    if(req.responseText==401){
                        jQuery.facebox("Please validate Session:\n" +req.statusText );
                        init();
                    }else{
                        jQuery.facebox('Done');
                    }
                }





            }

        }

    }
    return message;
}
function userNotFoundError(){
    //alert('inside usernot found error');
    var src='DisplayUserError.jsp'
    statuses=false;
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
//ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'User Not Found', 'width=650px,height=400px,left=300px,top=100px,resize=0,scrolling=1');
//dhtmlwindow.
}
function getUserInfo(valuex,screenId){//This is used to get list of customers based on searh criteria during user creation
    // alert('inside get user info');
    var customerno=document.getElementById("customerno").value;
    //alert(customerno);
    var customername=document.getElementById("customername").value;
    //alert('inside get user info');

    src="/IntBanking/MainController?taskId="+valuex+"&customerno="+customerno+"&customername="+customername+"&screenId="+screenId;
    //alert(src);
    document.getElementById('searchresult').innerHTML='Processing User Request...........';
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = function getInfo(){
        if (req.readyState == 4)  {
            // only if "OK"
            if (req.status == 200) {

                if(req.responseText==401){
                    alert("Please validate Session:\n" +req.statusText );
                    init();
                }else if(req.responseText=='0002'){
                    //alert(req.responseText);
                    document.getElementById('searchresult').innerHTML='';
                    var src='./screens/SessionTimeout.jsp';
                    //alert('about to call ajaxwin');
                    statuses=false;
                    req.open("POST",src , true);
                    req.send(null);
                    req.onreadystatechange = processReqChange;
                //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'User Not Found', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
                }else if(isnumeric(req.responseText)){
                    //alert(req.responseText);
                    document.getElementById('searchresult').innerHTML='';
                    src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                    //alert('about to call ajaxwin');
                    statuses=false;
                    req.open("POST",src , true);
                    req.send(null);
                    req.onreadystatechange = processReqChange;
                //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info:', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
                }else{
               
                    document.getElementById('searchresult').innerHTML='';
                    document.getElementById('searchresult').innerHTML = req.responseText;
            
                    paginate(20,handlepagination);
                }

            }

        }
    }

}

function getNextValue(valuex,screenId){
    var   highcounts = document.getElementById("highcount").value;
    // alert(highcounts);
    var   highcount  = parseInt(highcounts)+20;
    // alert(highcount);
    var   lowcounts = document.getElementById("lowcount").value;
    var lowcount  = parseInt(highcounts)+1;
    //alert(lowcount);
    var customerno=document.getElementById("customernonext").value;
    //alert(customerno);
    var customername=document.getElementById("customernamenext").value;
    // alert('inside get next value');

    src="/IntBanking/MainController?taskId="+valuex+"&customerno="+customerno+"&customername="+customername+"&screenId="+screenId+"&highcount="+highcount+"&lowcount="+lowcount;
    //alert(src);
    document.getElementById('searchresult').innerHTML='Processing User Request...........';
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = function getInfo(){
        if (req.readyState == 4)  {
            // only if "OK"
            if (req.status == 200) {
                if(req.responseText==401){
                    alert("Please validate Session:\n" +req.statusText );
                    init();
                }else{
                    //document.getElementById('trx_detail').innerHTML='';
                    //alert('inside search result');
                    // alert(req.responseText);
                    //alert(req.responseText);
                    document.getElementById('searchresult').innerHTML='';
                    document.getElementById('searchresult').innerHTML = req.responseText;
                //dhtmlwindow.;

                }

            }

        }
    }
}

function getUserForm(value,taskId,screenId){//This function directs the request to Maincontroller servlet to get the form for external user and populate the required fileds from flexcube
   divId='scrloader';
   var result=true;
    var customerno=value;//document.getElementById("refno").value;
    var src="/IntBanking/MainController?taskId="+taskId+"&customerno="+customerno+"&screenId="+screenId;

prepareAjaxRequest(divId,result,src);
}
function getExtUserField(valuex,screenId){// This is used based on user type to determine the next page during user creation
    var usertype=document.getElementById("cod_role_id").value;
    var src;
    if (usertype=='CUST'){
        src="/IntBanking/MainController?taskId="+valuex+"&screenId="+screenId;
        document.getElementById('scrloader').innerHTML='Processing User Request...........';
        req.open("POST",src , true);
        req.send(null);
        req.onreadystatechange = processUserCreation;
    }else{
        var taskId = document.getElementById("taskId").value;
        //alert(taskId);
        src="/IntBanking/MainController?taskId="+taskId+"&screenId="+screenId+"&usertype="+usertype;

        document.getElementById('scrloader').innerHTML='Processing User Request...........';
        req.open("POST",src , true);
        req.send(null);
        req.onreadystatechange = processReqChange;
    }
}

/*function fundTransfer(){

    //alert('inside fundTransfer');

    //$('#testButton').click(function(){

    var options = {
        target: '#search_result'

    };
    $('#TxnFrm').ajaxSubmit(options);

// });
//alert(good);

}*/
function processUserCreation(){


    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else{
                //alert(req.responseText);
                //document.getElementById('trx_detail').innerHTML='';
                document.getElementById('scrloader').innerHTML='';
                //alert(req.responseText);
                document.getElementById('scrloader').innerHTML = req.responseText;

            }

        }

    }
}
function getTrxDetail2(value){
    //Define arbitrary function to run desired DHTML Window widget codes
    //alert("Inside getTrxDetail")
    acno=document.getElementById("acctno").value;
    //taskId=value;
    startDate=document.getElementById("startDate").value;
    endDate=document.getElementById("endDate").value;
    from=document.getElementById("from").value;
    to=document.getElementById("to").value;
    check=document.getElementById("check").value;
    //alert(check+'This is d check');
    //var src ='./MainController?refno='+value+'&acno='+acno+'&taskId='+taskId;
    var src = './screens/1006c.jsp?taskId='+value+'&acno='+acno+'&startDate='+startDate+'&endDate='+endDate+'&from='+from+'&to='+to+'&check='+check;
    //alert(src);
    //modified on february 27,2011
    prepareAjaxRequest("trx_detail2",true,src)
/*req.open("POST",src , true);
    req.send(null);
    statuses=false;
    prepareAjaxRequest("scrloader",true,src)
    req.onreadystatechange = processReqChange;
// win=dhtmlmodal.open("ajaxbox", "ajax", src, "Trnansaction View", "width=618px,height=240px,left=320px,top=250px,resize=1,scrolling=1")
//ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Transaction Detail', 'width=650px,height=400px,left=300px,top=100px,resize=0,scrolling=1')
*/
}
function close(){
    //th.close();
    document.getElementById('close_win').innerHTML='';
}
function processTrxRequest() {
    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else{

                document.getElementById('view_detail').innerHTML='';
                document.getElementById('view_detail').innerHTML = req.responseText;

            }





        }
        else    {
            alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}

function parse(xml) {
    var dom;
    try {
        dom = new ActiveXObject("Microsoft.XMLDOM");
        dom.async = false;
        dom.loadXML(xml);
    } catch (error) {
        try {
            var parser = new DOMParser();
            dom = parser.parseFromString(xml, "text/xml");
            delete parser;
        } catch (error2) {
            if (debug)
                alert("XML parsing is not supported.");
        }
    }
    return dom;
}



function createRequestObject() {
    var ro;
    try {
        // Try to create object for Firefox, Safari, IE7, etc.
        ro = new XMLHttpRequest();
    }
    catch (e) {
        try {
            // Try to create object for later versions of IE.
            ro = new ActiveXObject('MSXML2.XMLHTTP');
        }
        catch (e) {
            try {
                // Try to create object for early versions of IE.
                ro = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (e) {
                // Could not create an XMLHttpRequest object.
                return false;
            }
        }
    }
    return ro;
}
function startSession(valuex) {


    //alert(valuex);

    req.open("post", "./Processor?key="+valuex, true);
    req.send(null);
    req.onreadystatechange = sessionResponse;
// branch for IE/Windows ActiveX version


}
function validatecustomer() {

    if(!canpost){
        alert('All fields are mandatory');
        document.getElementById('cod_ccy').focus();

    }else{
        var src="./screens/Q200.jsp?answer="+document.getElementById('answer').value;
        ;
        req.open("post",src , true);
        req.send(null);
        req.onreadystatechange = function validationres(){
            if (req.readyState == 4)  {
                // only if "OK"
                if (req.status == 200) {
                    var isvalid=req.responseText;
                    alert(isvalid.trim());
                    document.getElementById('scrloader').innerHTML='';
                }
                else    {
                    alert(req.status);
                    alert("There was a problem retrieving the XML data:\n" + req.statusText);
                }
            }
        };
    }
}






function reset(){

    document.getElementById('scrloader').innerHTML='';
}
function resetdiv(){

    document.getElementById('view_detail').innerHTML='';
}

function showScreen(fastPath,valuex) {

    fastPath.value='';
    var src="/IntBanking/MainController?taskId="+valuex;
    req.open("GET",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
// branch for IE/Windows ActiveX version
}
function showAuth(valuex) {
    // var txnId=document.getElementById('txnId').value;

    var src="/IntBanking/MainController?taskId="+valuex+"&screenId=1";
    document.getElementById('scrloader').innerHTML='Processing User Request...........';
    if (valuex=='402'||valuex=='514'||valuex=='331'||valuex=='7021' ||valuex=='323')callbackAftersuccess=true;
    req.open("POST",src , true);
    req.send(null);
    statuses=true
    req.onreadystatechange = processReqChange;

// branch for IE/Windows ActiveX version
}

function download(valuex) {
    acctno = document.getElementById('acctno').value;
    var src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno;

    //document.getElementById('scrloader').innerHTML='Processing User Request...........';
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange2;

// branch for IE/Windows ActiveX version
}
function showTrx(valuex) {
    //alert('entered')
    var acctno = document.getElementById('acct_no').value;
    var screenId = document.getElementById("screenId").value;
    var src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&screenId="+screenId;
    if (valuex=='4800'){
        check = document.getElementById("check").value;
        src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&check="+check+"&screenId="+screenId;

    }

    else if (document.frmStmtInq.fldPeriod[0].checked == true) {
        //alert('here 0');
        src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&screenId="+screenId;
    }
    else if (document.frmStmtInq.fldPeriod[1].checked == true) {
        var fldFromDate = document.frmStmtInq.fldFromDate.value;
        var fldToDate = document.frmStmtInq.fldToDate.value;

        src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&startDate="+fldFromDate+"&endDate="+fldToDate+"&screenId="+screenId;
    }
    else if (document.frmStmtInq.fldPeriod[2].checked == true) {
        var l_amount1 = document.frmStmtInq.fldFromAmount.value;
        var l_amount2 = document.frmStmtInq.fldToAmount.value;
        var i=0;
        var check=0;
        for (i=0; i<document.frmStmtInq.fldDrCr.length; i++){
            if(document.frmStmtInq.fldDrCr[i].checked == true){
                //document.frmStmtInq.fldDrCr.value = document.frmStmtInq.fldDrCr[i].value;
                check = document.frmStmtInq.fldDrCr[i].value;
            }
        }

        if(l_amount1 > l_amount2){
            alert("The initial amount is greater than final");
            document.frmStmtInq.fldFromAmount.focus ();
            return ;
        }
        alert('here');
        //src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&startDate="+l_amount1+"&endDate="+l_amount2+"&check="+check;
        src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&from="+l_amount1+"&to="+l_amount2+"&check="+check+"&screenId="+screenId;
    }
  //alert('here2');
    document.getElementById('trx_detail').innerHTML='Processing User Request...........';

    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange2;

// branch for IE/Windows ActiveX version
}

function showDiv(valuex) {
    //alert('inside showdiv');
    var screenId=document.getElementById("screenId").value;
    var acct=document.getElementById("acct_no").value;
    var src="./MainController?taskId="+valuex+"&screenId="+screenId+"&acct="+acct;
    // document.getElementById('trx_detail').innerHTML='Loading...........';
    document.getElementById('trx_detail').innerHTML='Processing User Request...........';

    //$.ajax({url:src,type:'POST',datatype: 'string',success: function(data){document.getElementById('trx_detail').innerHTML=' ';
    //document.getElementById('trx_detail').innerHTML=data}});
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange2;
}
function loadPrintableVersion()

{

    window.open('./screens/1006d.jsp', 'newWin', 'scrollbars=yes,status=no,menubar=yes,width=1000,height=635');

    document.submit();



}
//End "opennewsletter" function

function Logout(){
    document.getElementById('Logout').innerHTML='<input type="hidden" value="true" id="login"  >'
}
function logoutbt(){
    var src="./screens/logout.jsp?";
    req.open("POST",src , true);
    req.send(null);
}
function shows(value){

}
function showDivs(valuex,screenId) {
    //alert('inside showdivs');
    var src;

    var acct=document.getElementById("acct_id").value;
    //alert(acct);
    var leaves=document.getElementById("leave").value;
    /* alert(document.getElementById("leave").value);
    alert(document.getElementById("checkno").value);
    alert(document.getElementById("checkamount").value);*/
    var checkno=document.getElementById("checkno").value;
    //alert(checkno);
    var checkamount=document.getElementById("checkamount").value;
    //alert(document.getElementById("checkamount").value);
    /* if (document.getElementById("leave").value!=null || document.getElementById("leave").value!=" " ){
        leaves=document.getElementById("leave").value;
      alert(leaves);
    }

    if (document.getElementById("checkno").value!=null || document.getElementById("checkno").value!=" " ){
        checkno=document.getElementById("checkno").value;
      alert(checkno);
    }

    if (document.getElementById("checkamount").value!=null || document.getElementById("checkamount").value!=" " ){
        checkamount=document.getElementById("checkamount").value;
        alert(checkamount);
    }*/


    src="./MainController?taskId="+valuex+"&acno="+acct+"&leaves="+leaves+"&checkno="+checkno+"&checkamount="+checkamount+"&screenId="+screenId;
    //alert('whatup!' + src);
    document.getElementById('scrloader').innerHTML='';
    document.getElementById('scrloader').innerHTML='Processing User Request...........';

    //alert('about to call ajaxwin');
    //var popup=["101111","1021","104111","3211","3221","3231","3241"];

    if(popup.contains(valuex,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
}


// branch for IE/Windows ActiveX version
function processReqChange() {


    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else if(req.responseText=='0002'){
                //alert(req.responseText);
                document.getElementById('scrloader').innerHTML='';
                var src='./screens/SessionTimeout.jsp';
                //alert('about to call ajaxwin');
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info:', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else if(isnumeric(req.responseText)){
                //alert(req.responseText);
                document.getElementById('scrloader').innerHTML='';
                src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                //modified on february 27,2011
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //alert('about to call ajaxwin');
            // ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else{
                //document.getElementById('trx_detail').innerHTML='';
                if (statuses){
                    document.getElementById('scrloader').innerHTML='';
                    document.getElementById('scrloader').innerHTML = req.responseText;
                    
                    if (callbackAftersuccess)$('#range').hide();
                    initz();
                    //softKeyboard();
                    DatePickerControl.init();
                    if(document.getElementById('screen_id').value=='1401'){
                        document.getElementById('TCY_Code').onchange();
                    }
                }else{
                    document.getElementById('scrloader').innerHTML='';
                    jQuery.facebox(req.responseText);

                }
            //return req.responseText;
            }





        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}

function processReqChangeFacebox() {

    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else if(req.responseText=='0002'){
                //alert(req.responseText);
                document.getElementById('scrloader').innerHTML='';
                var src='./screens/SessionTimeout.jsp';
                //alert('about to call ajaxwin');
                //statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info:', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else if(isnumeric(req.responseText)){
                //alert(req.responseText);
                document.getElementById('scrloader').innerHTML='';
                src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                //alert('about to call ajaxwin');
                //statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else{
                //document.getElementById('trx_detail').innerHTML='';
                /* document.getElementById('scrloader').innerHTML='';
                document.getElementById('scrloader').innerHTML = req.responseText;
                if(document.getElementById('screen_id').value=='1401'){
                    document.getElementById('TCY_Code').onchange();
                }*/
                document.getElementById('scrloader').innerHTML='';
                jQuery.facebox(req.responseText);
            //return req.responseText;
            }





        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}
function validateUser(valuex,screenId){
    //alert('inside validate user');
    //alert(valuex);
    

    if ((valuex.length==0||valuex.length==null||valuex==null||valuex=='')){
        canpost = false;
        jQuery.facebox("You  need to enter userid");
        
    }else if(!removeSpecialChar(valuex)){
       //document.getElementById('user').value="";
       document.getElementById('user').focus();
       canpost=false;
       return;
    }

    else if(canpost){
        canpost = true;
        var src="/IntBanking/MainController?user="+valuex+"&taskId=001&screenId="+screenId;
        // alert(src);
        if (window.XMLHttpRequest) {

            req = new XMLHttpRequest( );
        }
        else if (window.ActiveXObject) {
            //msPopulate( );
            req = new ActiveXObject("Microsoft.XMLHTTP");
        }
        req.open("POST",src , true);
        req.send(null);
        req.onreadystatechange = processAcctDetChange;
    }
    
}


function getAccountDetail(taskId,screenId){
    var  acct = document.getElementById("DestAcctNo").value;
    var src="/IntBanking/MainController?acct="+acct+"&taskId="+taskId+"&screenId="+screenId;
    // alert(src);
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest( );
    }
    else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processAcctDetChange2;

}
function processAcctDetChange2(){

    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else{
                //parse(req)
                if (window.ActiveXObject) {
                    //alert('inside callback window 200');
                    msPopulate2( );
                }
                else if (window.XMLHttpRequest) {
                    //alert('inside callback non window 200');
                    nonMSPopulate2( );
                }

            }

        }
    }
}
function loginPopup(){
    //alert('inside loginpopup');
    var secpasswd=document.getElementById("secpasswd").value;
    //alert(secpasswd);
    if(secpasswd!=null||secpasswd!=''){
        // alert('about to call LoginPopup');
        var src ='LoginPopup.jsp?secpasswd='+secpasswd;
        // alert(src);
        statuses=false;
        req.open("POST",src , true);
        req.send(null);
        req.onreadystatechange = processReqChange;
    //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'SI Detail', 'width=650px,height=400px,left=300px,top=100px,resize=0,scrolling=1');
    }
}
function processAcctDetChange() {
    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else{

                if (window.ActiveXObject) {
                    // alert('inside callback window 200');
                    msPopulate( );
                }
                else if (window.XMLHttpRequest) {
                    //alert('inside callback non window 200');
                    nonMSPopulate( );
                }

            }

        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}

function currencyPopulate(){

}
function msPopulate( ){

    //alert('inside mspopulate');
    var resp = req.responseText;
    //alert(resp);
    var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async="false";
    xmlDoc.loadXML(resp);
    //alert(xmlDoc);

    if (resp=='no data'){
        //alert('The user id has been used');
        var   detailDiv = document.getElementById("user");
        document.getElementById('crbranchcode').innerHTML='';

        document.getElementById('crbranchcode').innerHTML = 'The user id has been used';

        detailDiv.value="";

        detailDiv.focus( );

    }else{
        //alert('why in this');
        document.getElementById('crbranchcode').innerHTML='';
    /*document.getElementById('crbranchcode').innerHTML='';
        urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
        //var codes = urlElementsBr[0].childNodes;
        var codes = urlElementsBr[0].firstChild.data;
        alert(codes);
        //var code = codes[0].firstChild.data;
        //alert(code);
        var li = document.createElement("input");
        //alert('why1');
        li.setAttribute("type","hidden" );
        //alert('why2');
        li.setAttribute("id","crbrcode" );
        //alert('why4');
        li.setAttribute("value",codes );
        //alert(li);
        alert("the account number is ok ");
        var branchcode = document.getElementById("crbranchcode");
        branchcode.appendChild(li);*/
    }
}

function nonMSPopulate( ){
    //alert('inside non mspopulate');
    xmlDoc = document.implementation.createDocument("","", null);
    var resp = req.responseText;
    var parser = new DOMParser( );
    var dom = parser.parseFromString(resp,"text/xml");
    urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
    if (resp=='no data'){
        //alert('The user id has been used');
        var   detailDiv = document.getElementById("user");
        document.getElementById('crbranchcode').innerHTML='';

        document.getElementById('crbranchcode').innerHTML = 'The user id has been used';

        detailDiv.value="";

        detailDiv.focus( );

    }else{
        document.getElementById('crbranchcode').innerHTML='';
    /*document.getElementById('crbranchcode').innerHTML='';
        urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
        //var codes = urlElementsBr[0].childNodes;
        var codes = urlElementsBr[0].firstChild.data;
        alert(codes);
        //var code = codes[0].firstChild.data;
        //alert(code);
        var li = document.createElement("input");
        //alert('why1');
        li.setAttribute("type","hidden" );
        //alert('why2');
        li.setAttribute("id","crbrcode" );
        //alert('why4');
        li.setAttribute("value",codes );
        //alert(li);
        alert("the account number is ok ");
        var branchcode = document.getElementById("crbranchcode");
        branchcode.appendChild(li);*/
    }
}

function msPopulate2( ){

    //alert('inside mspopulate');
    var resp = req.responseText;
    //alert(resp);
    var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async="false";
    xmlDoc.loadXML(resp);
    //alert(xmlDoc);

    if (resp=='no data'){
        //alert('account no is not correct');
        var detailDiv = document.getElementById("DestAcctNo");
        document.getElementById('crbranchcode').innerHTML='';
        document.getElementById('crbranchcode').innerHTML = 'The account number is not correct';
        detailDiv.value="";
        detailDiv.focus( );


    }else{
        document.getElementById('crbranchcode').innerHTML='';
        urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
        //var codes = urlElementsBr[0].childNodes;
        var codes = urlElementsBr[0].firstChild.data;

        //var code = codes[0].firstChild.data;
        //alert(code);
        var li = document.createElement("input");
        //alert('why1');
        li.setAttribute("type","hidden" );
        //alert('why2');
        li.setAttribute("id","crbrcode" );
        //alert('why4');
        li.setAttribute("value",codes );
        //alert(li.toString());

        //alert("the account number is ok ");
        alert(codes);
        var branchcode = document.getElementById("crbranchcode");
        //document.getElementById("crbranchcode")='';
        //document.getElementById("crbranchcode")='<input type="hidden" value="'+codes+'" id="crbrcode"  >';
        //alert('about to append');
        //alert(branchcode.appendChild(li));//this is the main one working need to use it for other browser
        branchcode.appendChild(li);
    }
}

function nonMSPopulate2( ){
    //alert('inside non mspopulate');
    xmlDoc = document.implementation.createDocument("","", null);
    var resp = req.responseText;
    var parser = new DOMParser( );
    var dom = parser.parseFromString(resp,"text/xml");
    // urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
    if (resp=='no data'){
        //alert('account no is not correct');
        var detailDiv = document.getElementById("DestAcctNo");
        document.getElementById('crbranchcode').innerHTML='';
        document.getElementById('crbranchcode').innerHTML = 'The account number is not correct';
        detailDiv.value="";
        detailDiv.focus( );


    }else{
        alert("why ok");
        document.getElementById('crbranchcode').innerHTML='';
        //alert("why ok2");
        var urlElementsBr = xmlDoc.getElementsByTagName('BRANCH_CODE');
        //var codes = urlElementsBr[0].childNodes;
        var codes = urlElementsBr[0].firstChild.data;

        //var code = codes[0].firstChild.data;
        //alert(codes);
        var li = document.createElement("input");
        //alert('why1');
        li.setAttribute("type","hidden" );
        //alert('why2');
        li.setAttribute("id","crbrcode" );
        //alert('why4');
        li.setAttribute("value",codes );
        //alert(li.toString());

        / //alert("the account number is ok ");
        //alert(codes);
        var branchcode = document.getElementById("crbranchcode");
        //document.getElementById("crbranchcode")='';
        //document.getElementById("crbranchcode")='<input type="hidden" value="'+codes+'" id="crbrcode"  >';
        //alert('about to append');
        //alert(branchcode.appendChild(li));
        branchcode.appendChild(li);
    }
}

function showSiDetail(valuex,screenId){
    var acctno = document.getElementById('acct_no').value;
    var detail = document.getElementById('detail').value;
    var instno = document.getElementById('instnos').value;
    var src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&detail="+detail+"&instno="+instno+"&screenId="+screenId;
    //alert(src);
    statuses=false;
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
//ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'SI Detail', 'width=650px,height=400px,left=300px,top=100px,resize=0,scrolling=1');
//ajaxwin.onclose=function(){
/*dhtmlwindow.close(ajaxwin);
        return true;
        return window.confirm("Do you want to Close");
    }*/


}

function showSiToClose(valuex,screenId,instno){
    //alert('inside showSiToClose ')
    var acctno = document.getElementById('acct_no').value;
    var detail = document.getElementById('detail').value;
    var acct_branch = document.getElementById('acct_branch').value;
    // alert(acct_branch);
    // var instno = document.getElementById('instnos').value;
    //alert(instno);
    var src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&detail="+detail+"&instno="+instno+"&acct_branch="+acct_branch+"&screenId="+screenId;
    // alert(src);
    document.getElementById('scrloader').innerHTML='Processing User Request...........';

    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;



}

function modifyUser(valuex,screenId,action){
    var userid = document.getElementById("user").value;
    var email  = document.getElementById("email").value;
    var emailold = document.getElementById("emailold").value;
    var userold = document.getElementById("userold").value
    var roleid=document.getElementById('cod_role_id').value;
    var actiondesc;
    var tablename = 'sm_user_access';
    var tablecolumn
    if(action=='E'){
        actiondesc  = document.getElementById("actionenable").value; 
         roleid='A';
          tablecolumn='flg_status';
    }else if (action=='D' ){
       actiondesc  = document.getElementById("actiondisable").value;
       tablecolumn='flg_status';
       roleid = 'D';
     
    }else if (action=='M'){
       actiondesc  = document.getElementById("actionmodify").value; 
       tablecolumn = 'cod_role_id';
       
    }
    //alert(userold);
    var src="/IntBanking/MainController?taskId="+valuex+"&screenId="+screenId+"&customername="+userid+
        "&customerno="+email+"&userold="+userold+"&action="+action+"&actiondesc="+actiondesc+
        "&roleid="+roleid+"&tablename="+tablename+'&tablecolumn='+tablecolumn;
    document.getElementById('scrloader').innerHTML='Processing User Request...........';
    if(popup.contains(valuex,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
}
function genPasswordJQ(valuex,screenId){
    //alert('inside JQ');
    jQuery.facebox(function($){
        genPassword(valuex,screenId)
    });
}
function genPassword(valuex,screenId){
    var userid = document.getElementById("user").value;
    var ispasscode = 'false';
    var src="/IntBanking/MainController?taskId="+valuex+"&screenId="+screenId+"&customername="+userid+"&ispasscode="+ispasscode;
    prepareAjaxRequest("scrloader",false,src);
}
function genPasscodeJQ(valuex,screenId){

    var userid = document.getElementById("user").value;
    var ispasscode = 'true';
    var src="/IntBanking/MainController?taskId="+valuex+"&screenId="+screenId+"&customername="+userid+"&ispasscode="+ispasscode;
    prepareAjaxRequest("scrloader",false,src);

}
function closeSI(valuex,screenId){

    var acct_branch = document.getElementById('acct_branch').value;
    //alert(acct_branch);
    var instno = document.getElementById('instnos').value;
    //alert(instno);
    var src="/IntBanking/MainController?taskId="+valuex+"&instno="+instno+"&acct_branch="+acct_branch+"&screenId="+screenId;
    //alert(src);
    document.getElementById('scrloader').innerHTML='Processing User Request...........';
    if(popup.contains(valuex,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;



}
function showSI(valuex,screenId){
    acctno = document.getElementById('acct_no').value;
    var detail = document.getElementById('detail').value;
    var instno = document.getElementById('instno').value;
    var src="/IntBanking/MainController?taskId="+valuex+"&acctno="+acctno+"&detail="+detail+"&instno="+instno+"&screenId="+screenId;
    document.getElementById('trx_detail').innerHTML='Processing User Request...........';

    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange2;
}
function processReqChange2() {
    if (req.readyState == 4)  {
        // only if "OK"
        if (req.status == 200) {
            if(req.responseText==401){
                alert("Please validate Session:\n" +req.statusText );
                init();
            }else if(req.responseText=='0002'){
                //alert(req.responseText);
                document.getElementById('trx_detail').innerHTML='';
                var src='./screens/SessionTimeout.jsp';
                //alert('about to call ajaxwin');
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'User Not Found', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else if(isnumeric(req.responseText)){
                //alert(req.responseText);
                document.getElementById('searchresult').innerHTML='';
                src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
                statuses=false;
                req.open("POST",src , true);
                req.send(null);
                req.onreadystatechange = processReqChange;
            //alert('about to call ajaxwin');
            //ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
            }else{

                //document.getElementById('trx_detail').innerHTML='';
                document.getElementById('trx_detail').innerHTML = req.responseText;
                if(document.getElementById('screen_id').value=='1401'){
                    document.getElementById('TCY_Code').onchange();
                }
                initPagination();
            }

            function initPagination(){
                //alert('inside initpagination');
                $('#datatabs').dataTable();
            //alert('end');
            }




        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }

}
function init(){

    //  req.onreadystatechange = processReqChange;
    var src="./main.jsp?taskId=blank";

    req.open("GET",src , true);
    req.send(null);
    req.onreadystatechange= function processinit(){
        if (req.readyState == 4)  {
            // only if "OK"
            if (req.status == 200) {
                document.getElementById('scrloader').innerHTML='';
                document.getElementById('scrloader').innerHTML = req.responseText;



            }
            else    {
                //alert(req.status);
                alert("There was a problem retrieving the XML data:\n" + req.statusText);
            }
        }
    }
}
function postRoles(){
    var role_id=document.getElementById('role_id').value;
    var role_description= document.getElementById('role_description').value;
    //var parameters='functionid=9999&role_id='+role_id.value+'&role_description='+role_description.value;

    txndata=txndata+'<role_id>';
    txndata=txndata+role_id;
    txndata=txndata+'</role_id>';
    txndata=txndata+'<role_description>';
    txndata=txndata+role_description;
    txndata=txndata+'</role_description>';
    txndata=txndata+'</DATA></ROWSET>'
    var parameter="txndata="+txndata;
    //alert(parameter);
    req.onreadystatechange = responsepost;
    var src="./screens/RoleDef.jsp?"+parameter;
    req.open("post",src , true);
    req.send(null);


    txndata='';
    txndata='<?xml version="1.0"?><ROWSET><DATA>';
}


function responsepost(){
    if (req.readyState == 4)  {
        if (req.status == 200) {
            //document.getElementById('faspath').focus();
            document.getElementById('scrloader').innerHTML = '';
            var response =req.responseText;
            jQuery.facebox(req.responseText);
        //alert(response.trim());
        }else if(isnumeric(req.responseText)){
            //alert(req.responseText);
            document.getElementById('searchresult').innerHTML='';
            src='./screens/DisplayUserErrors.jsp?msg='+req.responseText;
            statuses=false;
            req.open("POST",src , true);
            req.send(null);
            req.onreadystatechange = processReqChange;
        //alert('about to call ajaxwin');
        // ajaxwin=dhtmlwindow.open('ajaxbox', 'ajax', src, 'Info', 'width=330px,height=200px,left=450px,top=50px,resize=0,scrolling=1');
        }
        else    {
            //alert(req.status);
            alert("There was a problem retrieving the XML data:\n" + req.statusText);
        }
    }
}
function mapprofile(){
    var cod_role_id=document.getElementById('cod_role_id').value;
    var cod_task_id= document.getElementById('cod_task_id').value;
    var userid=document.getElementById('userid').title;
    txndata=txndata+'<cod_role_id>';
    txndata=txndata+cod_role_id;
    txndata=txndata+'</cod_role_id>';
    txndata=txndata+'<cod_task_id>';
    txndata=txndata+cod_task_id;
    txndata=txndata+'</cod_task_id>';
    txndata=txndata+'<userid>';
    txndata=txndata+userid;
    txndata=txndata+'</userid>';
    txndata=txndata+'</DATA></ROWSET>'

    var parameters='txndata='+txndata;
    // alert(parameters);
    var src="./screens/2002.jsp?"+parameters;
    req.open("post",src , true);
    req.send(null);
    req.onreadystatechange=responsepost;
    txndata='';
    txndata='<?xml version="1.0"?><ROWSET><DATA>';

}
function removeSpecialChar(valuex){

    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?%";
    var test = true;
    for (var i = 0; i < valuex.length; i++) {
        
        if(!test){
          return false;
        }
        if (iChars.indexOf(valuex.charAt(i)) != -1 ) {
            test=false;
            jQuery.facebox("Your username has special characters like %,$,&,etc. \nThese are not allowed.\n Please remove them and try again.\n ");
            return false;
        }
     
    }
    return true;
}
function createUser(taskId,screenId){


    var custno = document.getElementById('custno').value;
    var user=encodeURIComponent(document.getElementById('user').value);
    //alert(user);
    var user_password= document.getElementById('user_password').value;
    var user_name= unescape(document.getElementById('user_name').value);
    //alert(user_name);
    var user_lang= document.getElementById('user_lang').value;
    // alert(user_lang);
    var cod_role_id= document.getElementById('cod_role_id').value;
    // alert(cod_role_id);
    var screen_id=document.getElementById('screenId').value;
    //alert(screen_id);
    var userid=document.getElementById('user_id').value;
    //alert(userid);
    var branchCode=document.getElementById("branchcode").value;
    // alert(branchCode);
    var email=document.getElementById('email').value;
    var role_type = document.getElementById("role_type").value;
    var contact='';
    alert(role_type);
    var accounts='';
    if(taskId=='1021'){
        
    contact = document.getElementById("contact");
    accounts = getAccounts();
    alert(accounts);
}
    if(!canpost){
        alert('All fields are mandatory');
        document.getElementById('user').focus();
    }else{
        txndata=txndata+'<screen>';
        txndata=txndata+screen_id;
        txndata=txndata+'</screen>';
        txndata=txndata+'<branchCode>';
        txndata=txndata+branchCode;
        txndata=txndata+'</branchCode>';
        txndata=txndata+'<custno>';
        txndata=txndata+custno;
        txndata=txndata+'</custno>';
        txndata=txndata+'<user_id>';
        txndata=txndata+user;
        txndata=txndata+'</user_id>';
        txndata=txndata+'<user_password>';
        txndata=txndata+user_password;
        txndata=txndata+'</user_password>';
        txndata=txndata+'<user_name>';
        txndata=txndata+unescape(user_name);
        txndata=txndata+'</user_name>';
        txndata=txndata+'<user_lang>';
        txndata=txndata+user_lang;
        txndata=txndata+'</user_lang>';
        txndata=txndata+'<cod_role_id>';
        txndata=txndata+cod_role_id;
        txndata=txndata+'</cod_role_id>';
        txndata=txndata+'<userid>';
        txndata=txndata+userid;
        txndata=txndata+'</userid>';
        txndata=txndata+'<email>';
        txndata=txndata+email;
        txndata=txndata+'</email>';
        txndata=txndata+'<roletype>';
        txndata=txndata+role_type;
        txndata=txndata+'</roletype>';
        txndata=txndata+'<contact>';
        txndata=txndata+contact;
        txndata=txndata+'</contact>';
        txndata=txndata+'</DATA></ROWSET>'

        var parameters='txndata='+txndata;
        //alert(parameters);
        var src="/IntBanking/MainController?"+parameters+"&taskId="+taskId+"&screenId="+screenId+'&accounts='+accounts+'&custno='+custno+'&role_type='+role_type;
        document.getElementById('scrloader').innerHTML='Processing User Request...........';
        req.open("post",src , true);
        req.send(null);
        req.onreadystatechange=responsepost;
        txndata='';
        txndata='<?xml version="1.0"?><ROWSET><DATA>';
    }
}

function urlDecode(value){
    return unescape(value);
}

function registercust(){
    var account=document.getElementById('account').value;
    var cust= document.getElementById('cust_name').value;
    var branch= document.getElementById('branch').value;
    var currency= document.getElementById('currency').value;
    var limit=document.getElementById('limit').value;
    var mail=document.getElementById('mail').value;
    var question=document.getElementById('question').value;
    var answer=document.getElementById('answer').value;
    //   Question();
    if(!canpost){
        alert('All fields are mandatory');
        document.getElementById('mail').focus();

    }else{
        var parameters='account='+account+'&cust='+cust+'&branch='+branch+'&currency='+currency+'&limit='+limit+'&mail='+mail+'&question='+question+'&answer='+answer;

        var src="./screens/C1000.jsp?"+parameters;

        req.open("post",src , true);
        req.send(parameters);
        win=dhtmlmodal.open('EmailBox', 'iframe', "./screens/registerprocess.jsp", 'Processing', 'width=150px,left=100px,top=200px,center=1,resize=1,scrolling=0');
        // transactionwindow=dhtmlmodal.ajax_connect();
        win.onclose=function(){ //Define custom code to run when window is closed
            dhtmlmodal.closeveil();
            return true;
        }
        req.onreadystatechange = function processcust() {

            if (req.readyState == 4)  {
                if (req.status == 200) {
                    var response =req.responseText;
                    //prompt( response.trim());
                    alert(response.trim())

                    win.close();
                    document.getElementById('scrloader').innerHTML='';
                }
            }
        }
    }
}

function getRate(){

    var ccy=document.getElementById('TCY_Code').value;
    var settlement=document.getElementById('ACY_Code').value;
    var date=document.getElementById('sysdate').title;

    var src="./screens/Rate.jsp?var_txn_ccy="+ccy+'&var_sttlment_ccy='+settlement+'&sysdate='+date;
    req.open("GET",src , true);
    req.send(null);
    req.onreadystatechange = function processRate() {
        if (req.readyState == 4)  {
            if (req.status == 200) {
                var rate =req.responseText;
                document.getElementById('TCLCY_Rate').value=rate.trim();
            }
        }
    }
}

function getTransactions(screen){
    var userid=document.getElementById('acct_id').value;
    //alert(screen);
    //alert(userid);
    // var settlement=document.getElementById('ACY_Code').value;
    // alert('inside gettransaction');
    var src="./screens/" + screen +" ?acctid="+userid;
    req.open("POST",src , true);
    req.send(null);
    //  req.onreadystatechange =processReqChange;

    req.onreadystatechange = function execute() {

        if (req.readyState == 4)  {
            if (req.status == 200) {
                var response =req.responseText;

                alert(response.trim());
                document.getElementById('scrloader').innerHTML=response;

            }
        }
    }
//showScreen(document.getElementById('faspath'),9999);
}
function getSI(valuex,screenId){
    var fldSiType=document.getElementById('fldSiType').value;
    var dSrcAcctNo = document.getElementById("dSrcAcctNo").value;
    var DestAcctNo=document.getElementById('DestAcctNo').value;
    var SiTrfAmt = document.getElementById('SiTrfAmt').value;
    var SiTrfCurr1 = document.getElementById("SiTrfCurr1").value;
    var SiFreqDesc=  document.getElementById("SiFreqDesc").value;
    var SiPriority = document.getElementById("SiPriority").value;
    var FirstExecDate = document.getElementById("FirstExecDate").value;
    var SiFinalDate = document.getElementById("SiFinalDate").value;
    //alert(SiFinalDate);
    var SiNarration = document.getElementById("SiNarration").value;
    var accountBranch = document.getElementById("accountBranch").value;
    //alert(accountBranch);
    //alert('hurray'+document.getElementById("crbrcode").value);
    var crbrcode = document.getElementById("crbrcode").value;
    // alert('hurray'+crbrcode);

    var parameter = "&fldSiType="+fldSiType+"&dSrcAcctNo="+dSrcAcctNo+"&DestAcctNo="+DestAcctNo+"&SiTrfAmt="+SiTrfAmt+"&SiTrfCurr1="+SiTrfCurr1+"&SiFreqDesc="+SiFreqDesc+"&SiPriority="+SiPriority+"&FirstExecDate="+FirstExecDate+"&SiFinalDate="+SiFinalDate+"&SiNarration="+SiNarration+"&accountBranch="+accountBranch+"&crbrcode="+crbrcode+"&screenId="+screenId;
    //alert(parameter);
    var src="/IntBanking/MainController?taskId="+valuex+parameter;
    document.getElementById('scrloader').innerHTML='Processing User Request...........';
    if(popup.contains(valuex,false)){
        statuses=false;
    }else{
        statuses=true;
    }
    req.open("POST",src , true);
    req.send(null);
    req.onreadystatechange = processReqChange;
}


function showOverride(valuex){
    req.onreadystatechange = processReqChange;
    var src="./screens/Override.jsp?taskId="+valuex;
    req.open("GET",src , true);
    req.send(null);
//
//alert(valuex);
// document.getElementById('ovdmessage').innerHTML='OK';

//alert('parameters set');
//document.getElementById('scrloader').innerHTML=tab;
//createTxn();
}

function Question(){
    var tab1;
    tab1="<html>"
    tab1 =  tab1 + "<head>"

    tab1 =  tab1 + "<form Name=\"question\">"
    tab1 =  tab1 + "<body>"
    tab1 =  tab1 + "<table scroll=\"No\" bgcolor=\"transparent\">"
    tab1 =  tab1 + "<tr><td width=\"300\" class=\"formHeading\" height=\"20\">Phone number</td></tr>"
    tab1= tab1 + "</table><table scroll=\"No\" bgcolor=\"transparent\">"
    tab1= tab1 + "<tr><td class=\"LblIPMand\" width=\"30%\"> Number "
    tab1 = tab1 +  "</td><td><input id=\"phno\" name=\"phno\" size=\"10\" style=\"TEXT-ALIGN: left ; TEXT-TRANSFORM:uppercase;\"/>";
    tab1=tab1 + "</td></tr>"

    tab1= tab1+ "<tr><td align=\"center\" height=\"20\"><input type=\"Button\" name=\"btnOk\" id=\"btnOk\" style=\"FONT-WEIGHT: bold\" size=\"20\" class=\"ButtonField\" valid=\"Y\" value=\"Send Text\" onClick=\"javascript:DoLocalAuth(UserID.value.toUpperCase(),Passwd.value.toUpperCase());\"/>"

    tab1 =tab1 + "<br/><td><input type=\"Button\" name=\"btnCancel\" id=\"btnCancel\" style=\"FONT-WEIGHT: bold\" size=\"20\" class=\"ButtonField\" valid=\"Y\" value=\"Cancel\"onClick=\"javascript:top.TPUtl.CancelAuth(UserID.value);\"/></td>"

    //63_Retros -- END --Baiduri Caser ref # BNDBAD0105 ENDS SITE Ref # BRT/22

    tab1 =  tab1 + "</tr></table>"
    tab1 =  tab1 + "</body>"
    tab1 =  tab1 + "</form>"
    tab1 =  tab1 + "</html>"

    document.getElementById('scrloader').innerHTML=tab1;
}

function postTxn(){

    txndata=txndata+'</DATA></ROWSET>'

    var parameters='txndata='+txndata;
    // alert(parameters);
    req.onreadystatechange = function responsepost(){
        if (req.readyState == 4)  {
            if (req.status == 200) {
                // document.getElementById('faspath').focus();
                document.getElementById('scrloader').innerHTML = '';
                var response =req.responseText;
                alert(response.trim());
            }
            else    {
                alert(req.status);
                alert("There was a problem retrieving the data:\n" + req.statusText);
            }
        }
    };
    var src="./PostTxn?"+parameters;
    req.open("post",src , true);

    //data
    req.send(null);
    txndata='';
    txndata='<?xml version="1.0"?><ROWSET><DATA>';
}

function sendtoHost(value){
    seq=value;
    // alert(seq);

    var parameter="./screens/process.jsp"
    var src="./Processor?sequence="+seq;
    req.open("get",src , true);
    req.send(null);
    //window.open(parameter, "Process", "width=618px,height=240px,left=320px,top=250px,resize=1,scrolling=1"); //Run
    win=dhtmlmodal.open('EmailBox', 'iframe', parameter, 'Processing', 'width=290px,left=100px,top=200px,center=1,resize=1,scrolling=0');
    // transactionwindow=dhtmlmodal.ajax_connect();
    win.onclose=function(){ //Define custom code to run when window is closed
        dhtmlmodal.closeveil();
        return true;
    }
    req.onreadystatechange = function proceed() {

        if (req.readyState == 4)  {
            if (req.status == 200) {
                var response =req.responseText;
                win.close();
                alert(response.trim());

                document.getElementById('scrloader').innerHTML='';
                showScreen(document.getElementById('faspath'),200)
            }
        }
    }
}

function echeck(str) {

    var at="@"
    var dot="."
    var lat=str.indexOf(at)
    var lstr=str.length
    var ldot=str.indexOf(dot)
    if (str.indexOf(at)==-1){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;

    }

    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }

    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }

    if (str.indexOf(at,(lat+1))!=-1){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }

    if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }

    if (str.indexOf(dot,(lat+2))==-1){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }

    if (str.indexOf(" ")!=-1){
        alert("Invalid E-mail ID");
        document.getElementById("mail").focus();
        canpost=false;
        return false;
    }
    canpost=true;
    return true;
}
function checkmandatory(fieldid,fieldname,fieldvalue){
    if (fieldvalue==null ||fieldvalue.length==0){
        alert(fieldname +" cannot be null")
        canpost=false;
        document.getElementById(fieldid).focus();
        // document.getElementById("submit").disabled=true;
        //document.getElementById("submit").valid='N';
        return false;
    }
    canpost=true;
    //   document.getElementById("submit").disabled=false;
    // document.getElementById("submit").valid='Y';
    return true;
}

function checkmandatory2(fieldid,fieldname,fieldvalue){
    if (fieldvalue==null | fieldvalue.length==8){
        alert(fieldname +" cannot be null and cannot be less than 8 characters")
        canpost=false;
        document.getElementById(fieldid).focus();
        // document.getElementById("submit").disabled=true;
        //document.getElementById("submit").valid='N';
        return false;
    }
    canpost=true;
    //   document.getElementById("submit").disabled=false;
    // document.getElementById("submit").valid='Y';
    return true;
}



var dhtmlwindow={
    imagefiles:['windowfiles/min.gif', 'img/hr.gif', 'windowfiles/restore.gif', 'windowfiles/resize.gif'], //Path to 4 images used by script, in that order
    ajaxbustcache: true, //Bust caching when fetching a file via Ajax?
    ajaxloadinghtml: '<b>Loading Page. Please wait...</b>', //HTML to show while window fetches Ajax Content?
    minimizeorder: 0,
    zIndexvalue:100,
    tobjects: [], //object to contain references to dhtml window divs, for cleanup purposes
    lastactivet: {}, //reference to last active DHTML window
    init:function(t){
        var domwindow=document.createElement("div") //create dhtml window div
        domwindow.id=t
        domwindow.className="dhtmlwindow"
        var domwindowdata=''
        domwindowdata='<div class="drag-handle">'
        domwindowdata+='DHTML Window <div class="drag-controls"><img src="'+this.imagefiles[0]+'" title="Minimize" /><img src="'+this.imagefiles[1]+'" title="Close" /></div>'
        domwindowdata+='</div>'
        domwindowdata+='<div class="drag-contentarea"></div>'
        domwindowdata+='<div class="drag-statusarea"><div class="drag-resizearea" style="background: transparent url('+this.imagefiles[3]+') top right no-repeat;">&nbsp;</div></div>'
        domwindowdata+='</div>'
        domwindow.innerHTML=domwindowdata
        document.getElementById("dhtmlwindowholder").appendChild(domwindow)
        //this.zIndexvalue=(this.zIndexvalue)? this.zIndexvalue+1 : 100 //z-index value for DHTML window: starts at 0, increments whenever a window has focus
        var t=document.getElementById(t)
        var divs=t.getElementsByTagName("div")
        for (var i=0; i<divs.length; i++){ //go through divs inside dhtml window and extract all those with class="drag-" prefix
            if (/drag-/.test(divs[i].className))
                t[divs[i].className.replace(/drag-/, "")]=divs[i] //take out the "drag-" prefix for shorter access by name
        }
        //t.style.zIndex=this.zIndexvalue //set z-index of this dhtml window
        t.handle._parent=t //store back reference to dhtml window
        t.resizearea._parent=t //same
        t.controls._parent=t //same
        t.onclose=function(){
            return true
        } //custom event handler "onclose"
        t.onmousedown=function(){
            dhtmlwindow.setfocus(this)
        } //Increase z-index of window when focus is on it
        t.handle.onmousedown=dhtmlwindow.setupdrag //set up drag behavior when mouse down on handle div
        t.resizearea.onmousedown=dhtmlwindow.setupdrag //set up drag behavior when mouse down on resize div
        t.controls.onclick=dhtmlwindow.enablecontrols
        t.show=function(){
            dhtmlwindow.show(this)
        } //public function for showing dhtml window
        t.hide=function(){
            dhtmlwindow.hide(this)
        } //public function for hiding dhtml window
        t.close=function(){
            dhtmlwindow.close(this)
        } //public function for closing dhtml window (also empties DHTML window content)
        t.setSize=function(w, h){
            dhtmlwindow.setSize(this, w, h)
        } //public function for setting window dimensions
        t.moveTo=function(x, y){
            dhtmlwindow.moveTo(this, x, y)
        } //public function for moving dhtml window (relative to viewpoint)
        t.isResize=function(bol){
            dhtmlwindow.isResize(this, bol)
        } //public function for specifying if window is resizable
        t.isScrolling=function(bol){
            dhtmlwindow.isScrolling(this, bol)
        } //public function for specifying if window content contains scrollbars
        t.load=function(contenttype, contentsource, title){
            dhtmlwindow.load(this, contenttype, contentsource, title)
        } //public function for loading content into window
        this.tobjects[this.tobjects.length]=t
        return t //return reference to dhtml window div
    },
    open:function(t, contenttype, contentsource, title, attr, recalonload){

        var d=dhtmlwindow //reference dhtml window object
        function getValue(Name){
            var config=new RegExp(Name+"=([^,]+)", "i") //get name/value config pair (ie: width=400px,)
            return (config.test(attr))? parseInt(RegExp.$1) : 0 //return value portion (int), or 0 (false) if none found
        }
        if (document.getElementById(t)==null) //if window doesn't exist yet, create it
            t=this.init(t) //return reference to dhtml window div
        else
            t=document.getElementById(t)
        this.setfocus(t)
        t.setSize(getValue(("width")), (getValue("height"))) //Set dimensions of window
        var xpos=getValue("center")? "middle" : getValue("left") //Get x coord of window
        var ypos=getValue("center")? "middle" : getValue("top") //Get y coord of window
        //t.moveTo(xpos, ypos) //Position window
        if (typeof recalonload!="undefined" && recalonload=="recal" && this.scroll_top==0){ //reposition window when page fully loads with updated window viewpoints?
            if (window.attachEvent && !window.opera) //In IE, add another 400 milisecs on page load (viewpoint properties may return 0 b4 then)
                this.addEvent(window, function(){
                    setTimeout(function(){
                        t.moveTo(xpos, ypos)
                    }, 400)
                }, "load")
            else
                this.addEvent(window, function(){
                    t.moveTo(xpos, ypos)
                }, "load")
        }
        t.isResize(getValue("resize")) //Set whether window is resizable
        t.isScrolling(getValue("scrolling")) //Set whether window should contain scrollbars
        t.style.visibility="visible"
        t.style.display="block"
        t.contentarea.style.display="block"
        t.moveTo(xpos, ypos) //Position window
        t.load(contenttype, contentsource, title)
        if (t.state=="minimized" && t.controls.firstChild.title=="Restore"){ //If window exists and is currently minimized?
            t.controls.firstChild.setAttribute("src", dhtmlwindow.imagefiles[0]) //Change "restore" icon within window interface to "minimize" icon
            t.controls.firstChild.setAttribute("title", "Minimize")
            t.state="fullview" //indicate the state of the window as being "fullview"
        }
        return t
    },
    setSize:function(t, w, h){ //set window size (min is 150px wide by 100px tall)
        t.style.width=Math.max(parseInt(w), 150)+"px"
        t.contentarea.style.height=Math.max(parseInt(h), 100)+"px"
    },
    moveTo:function(t, x, y){ //move window. Position includes current viewpoint of document
        this.getviewpoint() //Get current viewpoint numbers
        t.style.left=(x=="middle")? this.scroll_left+(this.docwidth-t.offsetWidth)/2+"px" : this.scroll_left+parseInt(x)+"px"
        t.style.top=(y=="middle")? this.scroll_top+(this.docheight-t.offsetHeight)/2+"px" : this.scroll_top+parseInt(y)+"px"
    },
    isResize:function(t, bol){ //show or hide resize inteface (part of the status bar)
        t.statusarea.style.display=(bol)? "block" : "none"
        t.resizeBool=(bol)? 1 : 0
    },
    isScrolling:function(t, bol){ //set whether loaded content contains scrollbars
        t.contentarea.style.overflow=(bol)? "auto" : "hidden"
    },
    load:function(t, contenttype, contentsource, title){ //loads content into window plus set its title (3 content types: "inline", "iframe", or "ajax")
        if (t.isClosed){
            alert("DHTML Window has been closed, so no window to load contents into. Open/Create the window again.")
            return
        }
        var contenttype=contenttype.toLowerCase() //convert string to lower case
        if (typeof title!="undefined")
            t.handle.firstChild.nodeValue=title
        if (contenttype=="inline")
            t.contentarea.innerHTML=contentsource
        else if (contenttype=="div"){
            var inlinedivref=document.getElementById(contentsource)
            t.contentarea.innerHTML=(inlinedivref.defaultHTML || inlinedivref.innerHTML) //Populate window with contents of inline div on page
            if (!inlinedivref.defaultHTML)
                inlinedivref.defaultHTML=inlinedivref.innerHTML //save HTML within inline DIV
            inlinedivref.innerHTML="" //then, remove HTML within inline DIV (to prevent duplicate IDs, NAME attributes etc in contents of DHTML window
            inlinedivref.style.display="none" //hide that div
        }
        else if (contenttype=="iframe"){
            t.contentarea.style.overflow="hidden" //disable window scrollbars, as iframe already contains scrollbars
            if (!t.contentarea.firstChild || t.contentarea.firstChild.tagName!="IFRAME") //If iframe tag doesn't exist already, create it first
                t.contentarea.innerHTML='<iframe src="" style="margin:0; padding:0; width:100%; height: 100%" name="_iframe-'+t.id+'"></iframe>'
            window.frames["_iframe-"+t.id].location.replace(contentsource) //set location of iframe window to specified URL
        // this.ajax_connect(contentsource, t)
        }
        else if (contenttype=="ajax"){
            t.style.backgroundColor="#F8F8F8";
            this.ajax_connect(contentsource, t) //populate window with external contents fetched via Ajax
            //colorize and hide content div (while window is being dragged)
            //t.contentarea.style.visibility="hidden"
            t.style.backgroundColor="#BBD9EE"

        }
        t.contentarea.datatype=contenttype //store contenttype of current window for future reference
    },
    setupdrag:function(e){
        var d=dhtmlwindow //reference dhtml window object
        var t=this._parent //reference dhtml window div
        d.etarget=this //remember div mouse is currently held down on ("handle" or "resize" div)
        var e=window.event || e
        d.initmousex=e.clientX //store x position of mouse onmousedown
        d.initmousey=e.clientY
        d.initx=parseInt(t.offsetLeft) //store offset x of window div onmousedown
        d.inity=parseInt(t.offsetTop)
        d.width=parseInt(t.offsetWidth) //store width of window div
        d.contentheight=parseInt(t.contentarea.offsetHeight) //store height of window div's content div
        if (t.contentarea.datatype=="iframe"){ //if content of this window div is "iframe"
            t.style.backgroundColor="#F8F8F8" //colorize and hide content div (while window is being dragged)
            t.contentarea.style.visibility="hidden"
        }
        document.onmousemove=d.getdistance //get distance travelled by mouse as it moves
        document.onmouseup=function(){
            if (t.contentarea.datatype=="iframe"){ //restore color and visibility of content div onmouseup
                t.contentarea.style.backgroundColor="white"
                t.contentarea.style.visibility="visible"
            }
            d.stop()
        }
        return false
    },
    getdistance:function(e){
        var d=dhtmlwindow
        var etarget=d.etarget
        var e=window.event || e
        d.distancex=e.clientX-d.initmousex //horizontal distance travelled relative to starting point
        d.distancey=e.clientY-d.initmousey
        if (etarget.className=="drag-handle") //if target element is "handle" div
            d.move(etarget._parent, e)
        else if (etarget.className=="drag-resizearea") //if target element is "resize" div
            d.resize(etarget._parent, e)
        return false //cancel default dragging behavior
    },
    getviewpoint:function(){ //get window viewpoint numbers
        var ie=document.all && !window.opera
        var domclientWidth=document.documentElement && parseInt(document.documentElement.clientWidth) || 100000 //Preliminary doc width in non IE browsers
        this.standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body //create reference to common "body" across doctypes
        this.scroll_top=(ie)? this.standardbody.scrollTop : window.pageYOffset
        this.scroll_left=(ie)? this.standardbody.scrollLeft : window.pageXOffset
        this.docwidth=(ie)? this.standardbody.clientWidth : (/Safari/i.test(navigator.userAgent))? window.innerWidth : Math.min(domclientWidth, window.innerWidth-16)
        this.docheight=(ie)? this.standardbody.clientHeight: window.innerHeight
    },
    rememberattrs:function(t){ //remember certain attributes of the window when it's minimized or closed, such as dimensions, position on page
        this.getviewpoint() //Get current window viewpoint numbers
        t.lastx=parseInt((t.style.left || t.offsetLeft))-dhtmlwindow.scroll_left //store last known x coord of window just before minimizing
        t.lasty=parseInt((t.style.top || t.offsetTop))-dhtmlwindow.scroll_top
        t.lastwidth=parseInt(t.style.width) //store last known width of window just before minimizing/ closing
    },
    move:function(t, e){
        t.style.left=dhtmlwindow.distancex+dhtmlwindow.initx+"px"
        t.style.top=dhtmlwindow.distancey+dhtmlwindow.inity+"px"
    },
    resize:function(t, e){
        t.style.width=Math.max(dhtmlwindow.width+dhtmlwindow.distancex, 150)+"px"
        t.contentarea.style.height=Math.max(dhtmlwindow.contentheight+dhtmlwindow.distancey, 100)+"px"
    },
    enablecontrols:function(e){
        var d=dhtmlwindow
        var sourceobj=window.event? window.event.srcElement : e.target //Get element within "handle" div mouse is currently on (the controls)
        if (/Minimize/i.test(sourceobj.getAttribute("title"))) //if this is the "minimize" control
            d.minimize(sourceobj, this._parent)
        else if (/Restore/i.test(sourceobj.getAttribute("title"))) //if this is the "restore" control
            d.restore(sourceobj, this._parent)
        else if (/Close/i.test(sourceobj.getAttribute("title"))) //if this is the "close" control
            d.close(this._parent)
        return false
    },
    minimize:function(button, t){
        dhtmlwindow.rememberattrs(t)
        button.setAttribute("src", dhtmlwindow.imagefiles[2])
        button.setAttribute("title", "Restore")
        t.state="minimized" //indicate the state of the window as being "minimized"
        t.contentarea.style.display="none"
        t.statusarea.style.display="none"
        if (typeof t.minimizeorder=="undefined"){ //stack order of minmized window on screen relative to any other minimized windows
            dhtmlwindow.minimizeorder++ //increment order
            t.minimizeorder=dhtmlwindow.minimizeorder
        }
        t.style.left="10px" //left coord of minmized window
        t.style.width="200px"
        var windowspacing=t.minimizeorder*10 //spacing (gap) between each minmized window(s)
        t.style.top=dhtmlwindow.scroll_top+dhtmlwindow.docheight-(t.handle.offsetHeight*t.minimizeorder)-windowspacing+"px"
    },
    restore:function(button, t){
        dhtmlwindow.getviewpoint()
        button.setAttribute("src", dhtmlwindow.imagefiles[0])
        button.setAttribute("title", "Minimize")
        t.state="fullview" //indicate the state of the window as being "fullview"
        t.style.display="block"
        t.contentarea.style.display="block"
        if (t.resizeBool) //if this window is resizable, enable the resize icon
            t.statusarea.style.display="block"
        t.style.left=parseInt(t.lastx)+dhtmlwindow.scroll_left+"px" //position window to last known x coord just before minimizing
        t.style.top=parseInt(t.lasty)+dhtmlwindow.scroll_top+"px"
        t.style.width=parseInt(t.lastwidth)+"px"
    },
    close:function(t){
        try{
            var closewinbol=t.onclose()
        }
        catch(err){ //In non IE browsers, all errors are caught, so just run the below
            var closewinbol=true
        }
        finally{ //In IE, not all errors are caught, so check if variable isn't defined in IE in those cases
            if (typeof closewinbol=="undefined"){
                alert("An error has occured somwhere inside your \"onclose\" event handler")
                var closewinbol=true
            }
        }
        if (closewinbol){ //if custom event handler function returns true
            if (t.state!="minimized") //if this window isn't currently minimized
                dhtmlwindow.rememberattrs(t) //remember window's dimensions/position on the page before closing
            if (window.frames["_iframe-"+t.id]) //if this is an IFRAME DHTML window
                window.frames["_iframe-"+t.id].location.replace("about:blank")
            else
                t.contentarea.innerHTML=""
            t.style.display="none"
            t.isClosed=true //tell script this window is closed (for detection in t.show())
        }
        return closewinbol
    },
    setopacity:function(targetobject, value){ //Sets the opacity of targetobject based on the passed in value setting (0 to 1 and in between)
        if (!targetobject)
            return
        if (targetobject.filters && targetobject.filters[0]){ //IE syntax
            if (typeof targetobject.filters[0].opacity=="number") //IE6
                targetobject.filters[0].opacity=value*100
            else //IE 5.5
                targetobject.style.filter="alpha(opacity="+value*100+")"
        }
        else if (typeof targetobject.style.MozOpacity!="undefined") //Old Mozilla syntax
            targetobject.style.MozOpacity=value
        else if (typeof targetobject.style.opacity!="undefined") //Standard opacity syntax
            targetobject.style.opacity=value
    },
    setfocus:function(t){ //Sets focus to the currently active window
        this.zIndexvalue++
        t.style.zIndex=this.zIndexvalue
        t.isClosed=false //tell script this window isn't closed (for detection in t.show())
        this.setopacity(this.lastactivet.handle, 0.5) //unfocus last active window
        this.setopacity(t.handle, 1) //focus currently active window
        this.lastactivet=t //remember last active window
    },
    show:function(t){
        if (t.isClosed){
            alert("DHTML Window has been closed, so nothing to show. Open/Create the window again.")
            return
        }
        if (t.lastx) //If there exists previously stored information such as last x position on window attributes (meaning it's been minimized or closed)
            dhtmlwindow.restore(t.controls.firstChild, t) //restore the window using that info
        else
            t.style.display="block"
        this.setfocus(t)
        t.state="fullview" //indicate the state of the window as being "fullview"
    },
    hide:function(t){
        t.style.display="none"
    },
    ajax_connect:function(url, t){
        //alert(url);
        var page_request;

        try {
            // Try to create object for Firefox, Safari, IE7, etc.
            page_request = new XMLHttpRequest();
        }
        catch (e) {
            try {
                // Try to create object for later versions of IE.
                page_request = new ActiveXObject('MSXML2.XMLHTTP');
            }
            catch (e) {
                try {
                    // Try to create object for early versions of IE.
                    page_request = new ActiveXObject('Microsoft.XMLHTTP');
                }
                catch (e) {
                    // Could not create an XMLHttpRequest object.
                    return false;
                }
            }
        }


        t.contentarea.innerHTML=this.ajaxloadinghtml;
        page_request.onreadystatechange=function(){
            dhtmlwindow.ajax_loadpage(page_request, t)
        }
        page_request.open('GET', url, true)
        page_request.send(null)
    },
    ajax_loadpage:function(page_request, t){
        if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)){
            t.contentarea.innerHTML=page_request.responseText
        }
    },
    stop:function(){
        dhtmlwindow.etarget=null //clean up
        document.onmousemove=null
        document.onmouseup=null
    },
    addEvent:function(target, functionref, tasktype){ //assign a function to execute to an event handler (ie: onunload)
        var tasktype=(window.addEventListener)? tasktype : "on"+tasktype
        if (target.addEventListener)
            target.addEventListener(tasktype, functionref, false)
        else if (target.attachEvent)
            target.attachEvent(tasktype, functionref)
    },
    cleanup:function(){
        for (var i=0; i<dhtmlwindow.tobjects.length; i++){
            dhtmlwindow.tobjects[i].handle._parent=dhtmlwindow.tobjects[i].resizearea._parent=dhtmlwindow.tobjects[i].controls._parent=null
        }
        window.onload=null
    }
}
var dhtmlmodal={
    veilstack: 0,
    open:function(t, contenttype, contentsource, title, attr, recalonload){

        var d=dhtmlwindow //reference dhtmlwindow object
        this.interVeil=document.getElementById("interVeil") //Reference "veil" div
        this.veilstack++ //var to keep track of how many modal windows are open right now
        this.loadveil()
        if (recalonload=="recal" && d.scroll_top==0)
            d.addEvent(window, function(){
                dhtmlmodal.adjustveil()
            }, "load")
        var t=d.open(t, contenttype, contentsource, title, attr, recalonload)
        t.controls.firstChild.style.display="none" //Disable "minimize" button
        t.controls.onclick=function(){
            dhtmlmodal.close(this._parent, true)
        } //OVERWRITE default control action with new one
        t.show=function(){
            dhtmlmodal.show(this)
        } //OVERWRITE default t.show() method with new one
        t.hide=function(){
            dhtmlmodal.close(this)
        } //OVERWRITE default t.hide() method with new one
        return t
    },
    loadveil:function(){
        var d=dhtmlwindow
        d.getviewpoint()
        this.docheightcomplete=(d.standardbody.offsetHeight>d.standardbody.scrollHeight)? d.standardbody.offsetHeight : d.standardbody.scrollHeight
        this.interVeil.style.width=d.docwidth+"px" //set up veil over page
        this.interVeil.style.height=this.docheightcomplete+"px" //set up veil over page
        this.interVeil.style.left=0 //Position veil over page
        this.interVeil.style.top=0 //Position veil over page
        this.interVeil.style.visibility="visible" //Show veil over page
        this.interVeil.style.display="block" //Show veil over page
    },
    adjustveil:function(){ //function to adjust veil when window is resized
        if (this.interVeil && this.interVeil.style.display=="block") //If veil is currently visible on the screen
            this.loadveil() //readjust veil
    },
    closeveil:function(){ //function to close veil
        this.veilstack--
        if (this.veilstack==0) //if this is the only modal window visible on the screen, and being closed
            this.interVeil.style.display="none"
    },
    close:function(t, forceclose){ //DHTML modal close function
        t.contentDoc=(t.contentarea.datatype=="iframe")? window.frames["_iframe-"+t.id].document : t.contentarea //return reference to modal window DIV (or document object in the case of iframe
        if (typeof forceclose!="undefined")
            t.onclose=function(){
                return true
            }
        if (dhtmlwindow.close(t)) //if close() returns true
            this.closeveil()
    },
    show:function(t){
        dhtmlmodal.veilstack++
        dhtmlmodal.loadveil()
        dhtmlwindow.show(t)
    }
} //END object declaration
document.write('<div id="interVeil"></div>')
dhtmlwindow.addEvent(window, function(){
    if (typeof dhtmlmodal!="undefined") dhtmlmodal.adjustveil()
}, "resize")
//dhtmlwindow.addEvent(window, function(){if (typeof dhtmlmodal!="undefined") dhtmlmodal.adjustveil()}, "resize")

;
(function($) {

    /*
	Usage Note:
	-----------
	Do not use both ajaxSubmit and ajaxForm on the same form.  These
	functions are intended to be exclusive.  Use ajaxSubmit if you want
	to bind your own submit handler to the form.  For example,

	$(document).ready(function() {
		$('#myForm').bind('submit', function(e) {
			e.preventDefault(); // <-- important
			$(this).ajaxSubmit({
				target: '#output'
			});
		});
	});

	Use ajaxForm when you want the plugin to manage all the event binding
	for you.  For example,

	$(document).ready(function() {
		$('#myForm').ajaxForm({
			target: '#output'
		});
	});

	When using ajaxForm, the ajaxSubmit function will be invoked for you
	at the appropriate time.
*/

    /**
 * ajaxSubmit() provides a mechanism for immediately submitting
 * an HTML form using AJAX.
 */
    $.fn.ajaxSubmit = function(options) {

        if (!this.length) {
            log('ajaxSubmit: skipping submit process - no element selected');
            return this;
        }

        if (typeof options == 'function') {
            options = {
                success: options
            };
        }

        var url = $.trim(this.attr('action'));
        if (url) {
            // clean url (don't include hash vaue)
            url = (url.match(/^([^#]+)/)||[])[1];
        }
        url = url || window.location.href || '';

        options = $.extend(true, {
            url:  url,
            type: this.attr('method') || 'GET',
            iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank'
        }, options);

        // hook for manipulating the form data before it is extracted;
        // convenient for use with rich editors like tinyMCE or FCKEditor
        var veto = {};
        this.trigger('form-pre-serialize', [this, options, veto]);
        if (veto.veto) {
            log('ajaxSubmit: submit vetoed via form-pre-serialize trigger');
            return this;
        }

        // provide opportunity to alter form data before it is serialized
        if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
            log('ajaxSubmit: submit aborted via beforeSerialize callback');
            return this;
        }

        var n,v,a = this.formToArray(options.semantic);
        if (options.data) {
            options.extraData = options.data;
            for (n in options.data) {
                if(options.data[n] instanceof Array) {
                    for (var k in options.data[n]) {
                        a.push( {
                            name: n,
                            value: options.data[n][k]
                        } );
                    }
                }
                else {
                    v = options.data[n];
                    v = $.isFunction(v) ? v() : v; // if value is fn, invoke it
                    a.push( {
                        name: n,
                        value: v
                    } );
                }
            }
        }

        // give pre-submit callback an opportunity to abort the submit
        if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
            log('ajaxSubmit: submit aborted via beforeSubmit callback');
            return this;
        }

        // fire vetoable 'validate' event
        this.trigger('form-submit-validate', [a, this, options, veto]);
        if (veto.veto) {
            log('ajaxSubmit: submit vetoed via form-submit-validate trigger');
            return this;
        }

        var q = $.param(a);

        if (options.type.toUpperCase() == 'GET') {
            options.url += (options.url.indexOf('?') >= 0 ? '&' : '?') + q;
            options.data = null;  // data is null for 'get'
        }
        else {
            options.data = q; // data is the query string for 'post'
        }

        var $form = this, callbacks = [];
        if (options.resetForm) {
            callbacks.push(function() {
                $form.resetForm();
            });
        }
        if (options.clearForm) {
            callbacks.push(function() {
                $form.clearForm();
            });
        }

        // perform a load on the target only if dataType is not provided
        if (!options.dataType && options.target) {
            var oldSuccess = options.success || function(){};
            callbacks.push(function(data) {
                var fn = options.replaceTarget ? 'replaceWith' : 'html';
                $(options.target)[fn](data).each(oldSuccess, arguments);
            });
        }
        else if (options.success) {
            callbacks.push(options.success);
        }

        options.success = function(data, status, xhr) { // jQuery 1.4+ passes xhr as 3rd arg
            var context = options.context || options;   // jQuery 1.4+ supports scope context
            for (var i=0, max=callbacks.length; i < max; i++) {
                callbacks[i].apply(context, [data, status, xhr || $form, $form]);
            }
        };

        // are there files to upload?
        var fileInputs = $('input:file', this).length > 0;
        var mp = 'multipart/form-data';
        var multipart = ($form.attr('enctype') == mp || $form.attr('encoding') == mp);

        // options.iframe allows user to force iframe mode
        // 06-NOV-09: now defaulting to iframe mode if file input is detected
        if (options.iframe !== false && (fileInputs || options.iframe || multipart)) {
            // hack to fix Safari hang (thanks to Tim Molendijk for this)
            // see:  http://groups.google.com/group/jquery-dev/browse_thread/thread/36395b7ab510dd5d
            if (options.closeKeepAlive) {
                $.get(options.closeKeepAlive, fileUpload);
            }
            else {
                fileUpload();
            }
        }
        else {
            $.ajax(options);
        }

        // fire 'notify' event
        this.trigger('form-submit-notify', [this, options]);
        return this;


        // private function for handling file uploads (hat tip to YAHOO!)
        function fileUpload() {
            var form = $form[0];

            if ($(':input[name=submit],:input[id=submit]', form).length) {
                // if there is an input with a name or id of 'submit' then we won't be
                // able to invoke the submit fn on the form (at least not x-browser)
                alert('Error: Form elements must not have name or id of "submit".');
                return;
            }

            var s = $.extend(true, {}, $.ajaxSettings, options);
            s.context = s.context || s;
            var id = 'jqFormIO' + (new Date().getTime()), fn = '_'+id;
            window[fn] = function() {
                var f = $io.data('form-plugin-onload');
                if (f) {
                    f();
                    window[fn] = undefined;
                    try {
                        delete window[fn];
                    } catch(e){}
                }
            }
            var $io = $('<iframe id="' + id + '" name="' + id + '" src="'+ s.iframeSrc +'" onload="window[\'_\'+this.id]()" />');
            var io = $io[0];

            $io.css({
                position: 'absolute',
                top: '-1000px',
                left: '-1000px'
            });

            var xhr = { // mock object
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: 'n/a',
                getAllResponseHeaders: function() {},
                getResponseHeader: function() {},
                setRequestHeader: function() {},
                abort: function() {
                    this.aborted = 1;
                    $io.attr('src', s.iframeSrc); // abort op in progress
                }
            };

            var g = s.global;
            // trigger ajax global events so that activity/block indicators work like normal
            if (g && ! $.active++) {
                $.event.trigger("ajaxStart");
            }
            if (g) {
                $.event.trigger("ajaxSend", [xhr, s]);
            }

            if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
                if (s.global) {
                    $.active--;
                }
                return;
            }
            if (xhr.aborted) {
                return;
            }

            var cbInvoked = false;
            var timedOut = 0;

            // add submitting element to data if we know it
            var sub = form.clk;
            if (sub) {
                var n = sub.name;
                if (n && !sub.disabled) {
                    s.extraData = s.extraData || {};
                    s.extraData[n] = sub.value;
                    if (sub.type == "image") {
                        s.extraData[n+'.x'] = form.clk_x;
                        s.extraData[n+'.y'] = form.clk_y;
                    }
                }
            }

            // take a breath so that pending repaints get some cpu time before the upload starts
            function doSubmit() {
                // make sure form attrs are set
                var t = $form.attr('target'), a = $form.attr('action');

                // update form attrs in IE friendly way
                form.setAttribute('target',id);
                if (form.getAttribute('method') != 'POST') {
                    form.setAttribute('method', 'POST');
                }
                if (form.getAttribute('action') != s.url) {
                    form.setAttribute('action', s.url);
                }

                // ie borks in some cases when setting encoding
                if (! s.skipEncodingOverride) {
                    $form.attr({
                        encoding: 'multipart/form-data',
                        enctype:  'multipart/form-data'
                    });
                }

                // support timout
                if (s.timeout) {
                    setTimeout(function() {
                        timedOut = true;
                        cb();
                    }, s.timeout);
                }

                // add "extra" data to form if provided in options
                var extraInputs = [];
                try {
                    if (s.extraData) {
                        for (var n in s.extraData) {
                            extraInputs.push(
                                $('<input type="hidden" name="'+n+'" value="'+s.extraData[n]+'" />')
                                .appendTo(form)[0]);
                        }
                    }

                    // add iframe to doc and submit the form
                    $io.appendTo('body');
                    $io.data('form-plugin-onload', cb);
                    form.submit();
                }
                finally {
                    // reset attrs and remove "extra" input elements
                    form.setAttribute('action',a);
                    if(t) {
                        form.setAttribute('target', t);
                    } else {
                        $form.removeAttr('target');
                    }
                    $(extraInputs).remove();
                }
            }

            if (s.forceSync) {
                doSubmit();
            }
            else {
                setTimeout(doSubmit, 10); // this lets dom updates render
            }

            var data, doc, domCheckCount = 50;

            function cb() {
                if (cbInvoked) {
                    return;
                }

                $io.removeData('form-plugin-onload');

                var ok = true;
                try {
                    if (timedOut) {
                        throw 'timeout';
                    }
                    // extract the server response from the iframe
                    doc = io.contentWindow ? io.contentWindow.document : io.contentDocument ? io.contentDocument : io.document;

                    var isXml = s.dataType == 'xml' || doc.XMLDocument || $.isXMLDoc(doc);
                    log('isXml='+isXml);
                    if (!isXml && window.opera && (doc.body == null || doc.body.innerHTML == '')) {
                        if (--domCheckCount) {
                            // in some browsers (Opera) the iframe DOM is not always traversable when
                            // the onload callback fires, so we loop a bit to accommodate
                            log('requeing onLoad callback, DOM not available');
                            setTimeout(cb, 250);
                            return;
                        }
                    // let this fall through because server response could be an empty document
                    //log('Could not access iframe DOM after mutiple tries.');
                    //throw 'DOMException: not available';
                    }

                    //log('response detected');
                    cbInvoked = true;
                    xhr.responseText = doc.documentElement ? doc.documentElement.innerHTML : null;
                    xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                    xhr.getResponseHeader = function(header){
                        var headers = {
                            'content-type': s.dataType
                        };
                        return headers[header];
                    };

                    var scr = /(json|script)/.test(s.dataType);
                    if (scr || s.textarea) {
                        // see if user embedded response in textarea
                        var ta = doc.getElementsByTagName('textarea')[0];
                        if (ta) {
                            xhr.responseText = ta.value;
                        }
                        else if (scr) {
                            // account for browsers injecting pre around json response
                            var pre = doc.getElementsByTagName('pre')[0];
                            var b = doc.getElementsByTagName('body')[0];
                            if (pre) {
                                xhr.responseText = pre.innerHTML;
                            }
                            else if (b) {
                                xhr.responseText = b.innerHTML;
                            }
                        }
                    }
                    else if (s.dataType == 'xml' && !xhr.responseXML && xhr.responseText != null) {
                        xhr.responseXML = toXml(xhr.responseText);
                    }
                    data = $.httpData(xhr, s.dataType);
                }
                catch(e){
                    log('error caught:',e);
                    ok = false;
                    xhr.error = e;
                    $.handleError(s, xhr, 'error', e);
                }

                // ordering of these callbacks/triggers is odd, but that's how $.ajax does it
                if (ok) {
                    s.success.call(s.context, data, 'success', xhr);
                    if (g) {
                        $.event.trigger("ajaxSuccess", [xhr, s]);
                    }
                }
                if (g) {
                    $.event.trigger("ajaxComplete", [xhr, s]);
                }
                if (g && ! --$.active) {
                    $.event.trigger("ajaxStop");
                }
                if (s.complete) {
                    s.complete.call(s.context, xhr, ok ? 'success' : 'error');
                }

                // clean up
                setTimeout(function() {
                    $io.removeData('form-plugin-onload');
                    $io.remove();
                    xhr.responseXML = null;
                }, 100);
            }

            function toXml(s, doc) {
                if (window.ActiveXObject) {
                    doc = new ActiveXObject('Microsoft.XMLDOM');
                    doc.async = 'false';
                    doc.loadXML(s);
                }
                else {
                    doc = (new DOMParser()).parseFromString(s, 'text/xml');
                }
                return (doc && doc.documentElement && doc.documentElement.tagName != 'parsererror') ? doc : null;
            }
        }
    };

    /**
 * ajaxForm() provides a mechanism for fully automating form submission.
 *
 * The advantages of using this method instead of ajaxSubmit() are:
 *
 * 1: This method will include coordinates for <input type="image" /> elements (if the element
 *	is used to submit the form).
 * 2. This method will include the submit element's name/value data (for the element that was
 *	used to submit the form).
 * 3. This method binds the submit() method to the form for you.
 *
 * The options argument for ajaxForm works exactly as it does for ajaxSubmit.  ajaxForm merely
 * passes the options argument along after properly binding events for submit elements and
 * the form itself.
 */
    $.fn.ajaxForm = function(options) {
        alert('inside ajaxform');
        // in jQuery 1.3+ we can fix mistakes with the ready state
        if (this.length === 0) {
            var o = {
                s: this.selector,
                c: this.context
            };
            if (!$.isReady && o.s) {
                log('DOM not ready, queuing ajaxForm');
                $(function() {
                    $(o.s,o.c).ajaxForm(options);
                });
                return this;
            }
            // is your DOM ready?  http://docs.jquery.com/Tutorials:Introducing_$(document).ready()
            log('terminating; zero elements found by selector' + ($.isReady ? '' : ' (DOM not ready)'));
            return this;
        }

        return this.ajaxFormUnbind().bind('submit.form-plugin', function(e) {
            alert('inside ajaxFormUnbind');
            if (!e.isDefaultPrevented()) { // if event has been canceled, don't proceed
                e.preventDefault();
                $(this).ajaxSubmit(options);
            }
        }).bind('click.form-plugin', function(e) {
            var target = e.target;
            alert(target);
            var $el = $(target);
            if (!($el.is(":submit,input:image"))) {
                // is this a child element of the submit el?  (ex: a span within a button)
                var t = $el.closest(':submit');
                if (t.length == 0) {
                    alert('about to return');
                    return;
                }
                target = t[0];
            }
            var form = this;
            form.clk = target;
            if (target.type == 'image') {
                if (e.offsetX != undefined) {
                    form.clk_x = e.offsetX;
                    form.clk_y = e.offsetY;
                } else if (typeof $.fn.offset == 'function') { // try to use dimensions plugin
                    var offset = $el.offset();
                    form.clk_x = e.pageX - offset.left;
                    form.clk_y = e.pageY - offset.top;
                } else {
                    form.clk_x = e.pageX - target.offsetLeft;
                    form.clk_y = e.pageY - target.offsetTop;
                }
            }
            // clear form vars
            setTimeout(function() {
                form.clk = form.clk_x = form.clk_y = null;
            }, 100);
        });
    };

    // ajaxFormUnbind unbinds the event handlers that were bound by ajaxForm
    $.fn.ajaxFormUnbind = function() {
        return this.unbind('submit.form-plugin click.form-plugin');
    };

    /**
 * formToArray() gathers form element data into an array of objects that can
 * be passed to any of the following ajax functions: $.get, $.post, or load.
 * Each object in the array has both a 'name' and 'value' property.  An example of
 * an array for a simple login form might be:
 *
 * [ { name: 'username', value: 'jresig' }, { name: 'password', value: 'secret' } ]
 *
 * It is this array that is passed to pre-submit callback functions provided to the
 * ajaxSubmit() and ajaxForm() methods.
 */
    $.fn.formToArray = function(semantic) {
        var a = [];
        if (this.length === 0) {
            return a;
        }

        var form = this[0];
        var els = semantic ? form.getElementsByTagName('*') : form.elements;
        if (!els) {
            return a;
        }

        var i,j,n,v,el,max,jmax;
        for(i=0, max=els.length; i < max; i++) {
            el = els[i];
            n = el.name;
            if (!n) {
                continue;
            }

            if (semantic && form.clk && el.type == "image") {
                // handle image inputs on the fly when semantic == true
                if(!el.disabled && form.clk == el) {
                    a.push({
                        name: n,
                        value: $(el).val()
                    });
                    a.push({
                        name: n+'.x',
                        value: form.clk_x
                    }, {
                        name: n+'.y',
                        value: form.clk_y
                    });
                }
                continue;
            }

            v = $.fieldValue(el, true);
            if (v && v.constructor == Array) {
                for(j=0, jmax=v.length; j < jmax; j++) {
                    a.push({
                        name: n,
                        value: v[j]
                    });
                }
            }
            else if (v !== null && typeof v != 'undefined') {
                a.push({
                    name: n,
                    value: v
                });
            }
        }

        if (!semantic && form.clk) {
            // input type=='image' are not found in elements array! handle it here
            var $input = $(form.clk), input = $input[0];
            n = input.name;
            if (n && !input.disabled && input.type == 'image') {
                a.push({
                    name: n,
                    value: $input.val()
                });
                a.push({
                    name: n+'.x',
                    value: form.clk_x
                }, {
                    name: n+'.y',
                    value: form.clk_y
                });
            }
        }
        return a;
    };

    /**
 * Serializes form data into a 'submittable' string. This method will return a string
 * in the format: name1=value1&amp;name2=value2
 */
    $.fn.formSerialize = function(semantic) {
        //hand off to jQuery.param for proper encoding
        return $.param(this.formToArray(semantic));
    };

    /**
 * Serializes all field elements in the jQuery object into a query string.
 * This method will return a string in the format: name1=value1&amp;name2=value2
 */
    $.fn.fieldSerialize = function(successful) {
        var a = [];
        this.each(function() {
            var n = this.name;
            if (!n) {
                return;
            }
            var v = $.fieldValue(this, successful);
            if (v && v.constructor == Array) {
                for (var i=0,max=v.length; i < max; i++) {
                    a.push({
                        name: n,
                        value: v[i]
                    });
                }
            }
            else if (v !== null && typeof v != 'undefined') {
                a.push({
                    name: this.name,
                    value: v
                });
            }
        });
        //hand off to jQuery.param for proper encoding
        return $.param(a);
    };

    /**
 * Returns the value(s) of the element in the matched set.  For example, consider the following form:
 *
 *  <form><fieldset>
 *	  <input name="A" type="text" />
 *	  <input name="A" type="text" />
 *	  <input name="B" type="checkbox" value="B1" />
 *	  <input name="B" type="checkbox" value="B2"/>
 *	  <input name="C" type="radio" value="C1" />
 *	  <input name="C" type="radio" value="C2" />
 *  </fieldset></form>
 *
 *  var v = $(':text').fieldValue();
 *  // if no values are entered into the text inputs
 *  v == ['','']
 *  // if values entered into the text inputs are 'foo' and 'bar'
 *  v == ['foo','bar']
 *
 *  var v = $(':checkbox').fieldValue();
 *  // if neither checkbox is checked
 *  v === undefined
 *  // if both checkboxes are checked
 *  v == ['B1', 'B2']
 *
 *  var v = $(':radio').fieldValue();
 *  // if neither radio is checked
 *  v === undefined
 *  // if first radio is checked
 *  v == ['C1']
 *
 * The successful argument controls whether or not the field element must be 'successful'
 * (per http://www.w3.org/TR/html4/interact/forms.html#successful-controls).
 * The default value of the successful argument is true.  If this value is false the value(s)
 * for each element is returned.
 *
 * Note: This method *always* returns an array.  If no valid value can be determined the
 *	   array will be empty, otherwise it will contain one or more values.
 */
    $.fn.fieldValue = function(successful) {
        for (var val=[], i=0, max=this.length; i < max; i++) {
            var el = this[i];
            var v = $.fieldValue(el, successful);
            if (v === null || typeof v == 'undefined' || (v.constructor == Array && !v.length)) {
                continue;
            }
            v.constructor == Array ? $.merge(val, v) : val.push(v);
        }
        return val;
    };

    /**
 * Returns the value of the field element.
 */
    $.fieldValue = function(el, successful) {
        var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
        if (successful === undefined) {
            successful = true;
        }

        if (successful && (!n || el.disabled || t == 'reset' || t == 'button' ||
            (t == 'checkbox' || t == 'radio') && !el.checked ||
            (t == 'submit' || t == 'image') && el.form && el.form.clk != el ||
            tag == 'select' && el.selectedIndex == -1)) {
            return null;
        }

        if (tag == 'select') {
            var index = el.selectedIndex;
            if (index < 0) {
                return null;
            }
            var a = [], ops = el.options;
            var one = (t == 'select-one');
            var max = (one ? index+1 : ops.length);
            for(var i=(one ? index : 0); i < max; i++) {
                var op = ops[i];
                if (op.selected) {
                    var v = op.value;
                    if (!v) { // extra pain for IE...
                        v = (op.attributes && op.attributes['value'] && !(op.attributes['value'].specified)) ? op.text : op.value;
                    }
                    if (one) {
                        return v;
                    }
                    a.push(v);
                }
            }
            return a;
        }
        return $(el).val();
    };

    /**
 * Clears the form data.  Takes the following actions on the form's input fields:
 *  - input text fields will have their 'value' property set to the empty string
 *  - select elements will have their 'selectedIndex' property set to -1
 *  - checkbox and radio inputs will have their 'checked' property set to false
 *  - inputs of type submit, button, reset, and hidden will *not* be effected
 *  - button elements will *not* be effected
 */
    $.fn.clearForm = function() {
        return this.each(function() {
            $('input,select,textarea', this).clearFields();
        });
    };

    /**
 * Clears the selected form elements.
 */
    $.fn.clearFields = $.fn.clearInputs = function() {
        return this.each(function() {
            var t = this.type, tag = this.tagName.toLowerCase();
            if (t == 'text' || t == 'password' || tag == 'textarea') {
                this.value = '';
            }
            else if (t == 'checkbox' || t == 'radio') {
                this.checked = false;
            }
            else if (tag == 'select') {
                this.selectedIndex = -1;
            }
        });
    };

    /**
 * Resets the form data.  Causes all form elements to be reset to their original value.
 */
    $.fn.resetForm = function() {
        return this.each(function() {
            // guard against an input with the name of 'reset'
            // note that IE reports the reset function as an 'object'
            if (typeof this.reset == 'function' || (typeof this.reset == 'object' && !this.reset.nodeType)) {
                this.reset();
            }
        });
    };

    /**
 * Enables or disables any matching elements.
 */
    $.fn.enable = function(b) {
        if (b === undefined) {
            b = true;
        }
        return this.each(function() {
            this.disabled = !b;
        });
    };

    /**
 * Checks/unchecks any matching checkboxes or radio buttons and
 * selects/deselects and matching option elements.
 */
    $.fn.selected = function(select) {
        if (select === undefined) {
            select = true;
        }
        return this.each(function() {
            var t = this.type;
            if (t == 'checkbox' || t == 'radio') {
                this.checked = select;
            }
            else if (this.tagName.toLowerCase() == 'option') {
                var $sel = $(this).parent('select');
                if (select && $sel[0] && $sel[0].type == 'select-one') {
                    // deselect all other options
                    $sel.find('option').selected(false);
                }
                this.selected = select;
            }
        });
    };

    // helper fn for console logging
    // set $.fn.ajaxSubmit.debug to true to enable debug logging
    function log() {
        if ($.fn.ajaxSubmit.debug) {
            var msg = '[jquery.form] ' + Array.prototype.join.call(arguments,'');
            if (window.console && window.console.log) {
                window.console.log(msg);
            }
            else if (window.opera && window.opera.postError) {
                window.opera.postError(msg);
            }
        }
    };

})(jQuery);

