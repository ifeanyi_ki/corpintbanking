/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.vo;

/**
 *
 * @author Admin
 */
public class LoaderVo {
    private String type;
    private String path;
    private String doc_id;
    private String passNo;
    private String surname;
    private String middlename;
    private String firstname;
    private String sex;
    private String dob;
    private String state;
    private String country;
    private String category;
    private String ref_id;
    private String local_govt;
    private String trans_no;
    private String marital_status;
    private String enrolNo;
    private String title;
    private String cre_user_id;
    private String old_ref_id;
    private String new_ref_id;
    private String old_passport_no;
    private String new_passport_no;
    private String approvedStatus;
    private String control;
    //old_ref_id, new_ref_id, old_passport_no, 
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the doc_id
     */
    public String getDoc_id() {
        return doc_id;
    }

    /**
     * @param doc_id the doc_id to set
     */
    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    /**
     * @return the passNo
     */
    public String getPassNo() {
        return passNo;
    }

    /**
     * @param passNo the passNo to set
     */
    public void setPassNo(String passNo) {
        this.passNo = passNo;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }

    /**
     * @param ref_id the ref_id to set
     */
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    /**
     * @return the local_govt
     */
    public String getLocal_govt() {
        return local_govt;
    }

    /**
     * @param local_govt the local_govt to set
     */
    public void setLocal_govt(String local_govt) {
        this.local_govt = local_govt;
    }

    /**
     * @return the trans_no
     */
    public String getTrans_no() {
        return trans_no;
    }

    /**
     * @param trans_no the trans_no to set
     */
    public void setTrans_no(String trans_no) {
        this.trans_no = trans_no;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the marital_status
     */
    public String getMarital_status() {
        return marital_status;
    }

    /**
     * @param marital_status the marital_status to set
     */
    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    /**
     * @return the enrolNo
     */
    public String getEnrolNo() {
        return enrolNo;
    }

    /**
     * @param enrolNo the enrolNo to set
     */
    public void setEnrolNo(String enrolNo) {
        this.enrolNo = enrolNo;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the cre_user_id
     */
    public String getCre_user_id() {
        return cre_user_id;
    }

    /**
     * @param cre_user_id the cre_user_id to set
     */
    public void setCre_user_id(String cre_user_id) {
        this.cre_user_id = cre_user_id;
    }

    /**
     * @return the old_ref_id
     */
    public String getOld_ref_id() {
        return old_ref_id;
    }

    /**
     * @param old_ref_id the old_ref_id to set
     */
    public void setOld_ref_id(String old_ref_id) {
        this.old_ref_id = old_ref_id;
    }

    /**
     * @return the new_ref_id
     */
    public String getNew_ref_id() {
        return new_ref_id;
    }

    /**
     * @param new_ref_id the new_ref_id to set
     */
    public void setNew_ref_id(String new_ref_id) {
        this.new_ref_id = new_ref_id;
    }

    /**
     * @return the old_passport_no
     */
    public String getOld_passport_no() {
        return old_passport_no;
    }

    /**
     * @param old_passport_no the old_passport_no to set
     */
    public void setOld_passport_no(String old_passport_no) {
        this.old_passport_no = old_passport_no;
    }

    /**
     * @return the approvedStatus
     */
    public String getApprovedStatus() {
        return approvedStatus;
    }

    /**
     * @param approvedStatus the approvedStatus to set
     */
    public void setApprovedStatus(String approvedStatus) {
        this.approvedStatus = approvedStatus;
    }

    /**
     * @return the new_passport_no
     */
    public String getNew_passport_no() {
        return new_passport_no;
    }

    /**
     * @param new_passport_no the new_passport_no to set
     */
    public void setNew_passport_no(String new_passport_no) {
        this.new_passport_no = new_passport_no;
    }

    /**
     * @return the control
     */
    public String getControl() {
        return control;
    }

    /**
     * @param control the control to set
     */
    public void setControl(String control) {
        this.control = control;
    }
    
}
