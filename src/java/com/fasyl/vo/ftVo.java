/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.vo;

/**
 *
 * @author REBECCA-PC
 */
public class ftVo {
 
 private String trans_id;
 private String int_id;  
 private double amt;
 private String acc;  
 private String ben_acc; 
 private String init_acc;
 private String narration;  
 private String auth1; 
 private String auth1_action;
 private String operator;  
 private String auth2;

    /**
     * @return the trans_id
     */
    public String getTrans_id() {
        return trans_id;
    }

    /**
     * @param trans_id the trans_id to set
     */
    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    /**
     * @return the int_id
     */
    public String getInt_id() {
        return int_id;
    }

    /**
     * @param int_id the int_id to set
     */
    public void setInt_id(String int_id) {
        this.int_id = int_id;
    }

   
    /**
     * @return the acc
     */
    public String getAcc() {
        return acc;
    }

    /**
     * @param acc the acc to set
     */
    public void setAcc(String acc) {
        this.acc = acc;
    }

    /**
     * @return the ben_acc
     */
    public String getBen_acc() {
        return ben_acc;
    }

    /**
     * @param ben_acc the ben_acc to set
     */
    public void setBen_acc(String ben_acc) {
        this.ben_acc = ben_acc;
    }

    /**
     * @return the narration
     */
    public String getNarration() {
        return narration;
    }

    /**
     * @param narration the narration to set
     */
    public void setNarration(String narration) {
        this.narration = narration;
    }

    /**
     * @return the auth1
     */
    public String getAuth1() {
        return auth1;
    }

    /**
     * @param auth1 the auth1 to set
     */
    public void setAuth1(String auth1) {
        this.auth1 = auth1;
    }

    /**
     * @return the auth1_action
     */
    public String getAuth1_action() {
        return auth1_action;
    }

    /**
     * @param auth1_action the auth1_action to set
     */
    public void setAuth1_action(String auth1_action) {
        this.auth1_action = auth1_action;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the auth2
     */
    public String getAuth2() {
        return auth2;
    }

    /**
     * @param auth2 the auth2 to set
     */
    public void setAuth2(String auth2) {
        this.auth2 = auth2;
    }

    /**
     * @return the amt
     */
    public double getAmt() {
        return amt;
    }

    /**
     * @param amt the amt to set
     */
    public void setAmt(double amt) {
        this.amt = amt;
    }

    /**
     * @return the init_acc
     */
    public String getInit_acc() {
        return init_acc;
    }

    /**
     * @param init_acc the init_acc to set
     */
    public void setInit_acc(String init_acc) {
        this.init_acc = init_acc;
    }

}
