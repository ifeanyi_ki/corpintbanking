/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.dto;

/**
 *
 * @author Administrator
 */
public class FundsDto {
    String txn_ref_no;
    boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
    

    public String getTxn_ref_no() {
        return txn_ref_no;
    }

    public void setTxn_ref_no(String txn_ref_no) {
        this.txn_ref_no = txn_ref_no;
    }
    
}
