/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.dto;

/**
 *
 * @author Administrator
 */
public class FundTransferReceiptDto {
    private String debitAcc;
    private String creditAcc;
    private String benBankName;
    private String benAccName;
    private String amount;
    private String narration;
    private String transactionTime;
    private String customerAccName;
    private String timeStamp;
    private String bene_acc_number;
    private String LFTindicator;

    public String getLFTindicator() {
        return LFTindicator;
    }

    public void setLFTindicator(String LFTindicator) {
        this.LFTindicator = LFTindicator;
    }
    

    public String getBene_acc_number() {
        return bene_acc_number;
    }

    public void setBene_acc_number(String bene_acc_number) {
        this.bene_acc_number = bene_acc_number;
    }
    
    

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    

    public String getCustomerAccName() {
        return customerAccName;
    }

    public void setCustomerAccName(String customerAccName) {
        this.customerAccName = customerAccName;
    }
    
    

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }
    
    

    public String getDebitAcc() {
        return debitAcc;
    }

    public void setDebitAcc(String debitAcc) {
        this.debitAcc = debitAcc;
    }

    public String getCreditAcc() {
        return creditAcc;
    }

    public void setCreditAcc(String creditAcc) {
        this.creditAcc = creditAcc;
    }

    public String getBenBankName() {
        return benBankName;
    }

    public void setBenBankName(String benBankName) {
        this.benBankName = benBankName;
    }

    public String getBenAccName() {
        return benAccName;
    }

    public void setBenAccName(String benAccName) {
        this.benAccName = benAccName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }
    
    
    
}
