
package com.fasyl.ebanking.dto;

/**
 *
 * @author kingsley
 */
public class InterBeneficiaryDto {
    private String beneficiary_id;
    private String beneficiary_name;
    private String customer_no;
    private String destination_acct;
    private String amount;
    private String benBank;
    private String beneficiary_type;

    public String getBeneficiary_type() {
        return beneficiary_type;
    }

    public void setBeneficiary_type(String beneficiary_type) {
        this.beneficiary_type = beneficiary_type;
    }
    
    

    public String getBenBank() {
        return benBank;
    }

    public void setBenBank(String benBank) {
        this.benBank = benBank;
    }
    
    
    public String getBeneficiary_id() {
        return beneficiary_id;
    }

    public void setBeneficiary_id(String beneficiary_id) {
        this.beneficiary_id = beneficiary_id;
    }

    public String getBeneficiary_name() {
        return beneficiary_name;
    }

    public void setBeneficiary_name(String beneficiary_name) {
        this.beneficiary_name = beneficiary_name;
    }

    public String getCustomer_no() {
        return customer_no;
    }

    public void setCustomer_no(String customer_no) {
        this.customer_no = customer_no;
    }

    public String getDestination_acct() {
        return destination_acct;
    }

    public void setDestination_acct(String destination_acct) {
        this.destination_acct = destination_acct;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    
}
