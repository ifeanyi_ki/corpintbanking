
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.db;

//~--- JDK imports ------------------------------------------------------------

//import oracle.jdbc.*;
//import oracle.jdbc.util.*;
//import oracle.jdbc.driver.*;
import java.sql.*;

//import java.lang.*;

/**
 *
 * @author Mcrolly
 * This package is used to gain DB ConnectionClass
 */

//import java.sql.Statement;
//import java.sql.Connection;
//import java.sql.DriverManager;
import java.util.StringTokenizer;

public class ConnectionClass {
    static Connection     conn = null;
    private static String connString;    // = "jdbc:oracle:thin:"+username+"/"+password+"@"+host+":"+port+":"+serviceName;

//  FileName="Connection_jdbc_conn.htm"
//  Type="JDBC" ""
//  DesigntimeType="JDBC"
//  HTTP="false"
//  Catalog=""
//  Schema=""
    private static String driver;         // = "oracle.jdbc.driver.OracleDriver";
    private static String host;           // = "127.0.0.1";
    private static String password;       // = "clpobe";
    private static String port;           // = "1521";
    private static String serviceName;    // = "infinity";
    private static String username;       // = "clobe";

//  public static void main(String[] args) {
//      //  Connection ConnectionClass = null;
//      System.out.println("SQL Test");
//      getConn(null);
//      System.out.println("Connection established");
//      //testRun();
//  }
    public static Connection getConn(String connStr) {

        // Connection  ConnectionClass=null;
        if ((connStr == "") || (connStr == null)) {
            driver      = "oracle.jdbc.driver.OracleDriver";
            username    = "TBMB";
            password    = "TBMB"; //IntBanking123$
            serviceName = "orcl"; //fchomes
            host        = "192.168.235.1"; //172.15.10.50
            port        = "1521";
            connString  = "jdbc:oracle:thin:" + username + "/" + password + "@" + host + ":" + port + ":" + serviceName;
        } else {
            connString = connStr;
        }
//jdbc:oracle:thin:@192.168.235.1:1521:orcl
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (Exception e) {
            System.out.println(e);
            System.exit(0);
        }

        try {
            conn = java.sql.DriverManager.getConnection(connString);
        } catch (Exception e) {
            System.out.println(e);
            System.exit(0);
        }

        System.out.println(conn + " This is the conn ");

        return conn;
    }

    private static void testRun() {
        try {
            Statement          s = conn.createStatement();
            java.sql.ResultSet r = s.executeQuery("Select user_id from brn001.smtb_user_role");

            while (r.next()) {
                System.out.println(r.getString("user_id"));

                StringTokenizer Tok = new StringTokenizer(r.getString("user_id"), "-");
                int             n   = 0;

                while (Tok.hasMoreElements()) {
                    System.out.println("" + ++n + ": " + Tok.nextElement());
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            System.exit(0);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
