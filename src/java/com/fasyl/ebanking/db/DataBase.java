
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.db;

//~--- JDK imports ------------------------------------------------------------
/**
 *
 * @author Nisar
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

public class DataBase {

    protected static Connection con = null;
    protected static Connection con2 = null;
    private static DataSource datasource;
    private static DataSource datasource2;
    static Context initContext;

    static {
        try {

            // initContext  = new InitialContext();
            // envContext  = (Context)initContext;
            //datasource = oracle.jdbc.;
            
            datasource = (DataSource) new InitialContext().lookup("jdbc/OraclePool");
            if(datasource == null){
                //throw new SQLException("****  datasource is null ****");
                System.out.println("****datasource is null****");
            }
            con        = datasource.getConnection();
            
        } catch (Exception e) {
            
            e.printStackTrace(System.out);
        }
    }

    static {
        try {

            // initContext  = new InitialContext();
            // envContext  = (Context)initContext;
            
            datasource2 = (DataSource) new InitialContext().lookup("jdbc/OraclePoolFCC");
            con2 = datasource2.getConnection();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            if (con == null || con.isClosed()) {
                try {
                    con = datasource.getConnection();
                    System.out.println("con is null, getting connection for the second time....");   
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } 
            } else {
                System.out.println("returning con");
                return con;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return con;
    }

    public static Connection getConnectionToFCC() {
        try {
            if (con2 == null || con2.isClosed()) {
                try {
                    System.out.println("inside etconnectionfcc");
                    con2 = datasource2.getConnection(); 
                    System.out.println("about to leave etconnectionfcc");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } 
            } else {
                System.out.println(" I am returning con2");
                return con2;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return con2;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
