
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.util;

//~--- non-JDK imports --------------------------------------------------------

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Nisar
 */
import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLUtilty {
    public static String getDocumentAsXml(Document doc) throws TransformerConfigurationException, TransformerException {
        DOMSource          domSource   = new DOMSource(doc);
        TransformerFactory tf          = TransformerFactory.newInstance();
        Transformer        transformer = tf.newTransformer();

        // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,"yes");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        // we want to pretty format the XML output
        // note : this is broken in jdk1.5 beta!
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        java.io.StringWriter sw = new java.io.StringWriter();
        StreamResult         sr = new StreamResult(sw);

        transformer.transform(domSource, sr);

        return sw.toString();
    }

    public static Document getStringAsdocument(String xml) {
        DocumentBuilder builder  = null;
        Document        document = null;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(XMLUtilty.class.getName()).log(Level.SEVERE, null, ex);
            }

            document = builder.parse(new InputSource(new StringReader(xml)));
        } catch (SAXException ex) {
            Logger.getLogger(XMLUtilty.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLUtilty.class.getName()).log(Level.SEVERE, null, ex);
        }

        return document;
    }

    public static String getTextValue(Element ele, String tagName) {
        String   textVal = null;
        NodeList nl      = ele.getElementsByTagName(tagName);

        if ((nl != null) && (nl.getLength() > 0)) {
            Element el = (Element) nl.item(0);
            System.out.println(el+"hhh");
            if(el.getFirstChild()!=null)
            textVal = el.getFirstChild().getNodeValue();
        }

        return textVal;
    }

    public static ArrayList getOne(String xmldata, String tag) {
        ArrayList<String> list   = new ArrayList<String>();
        Document          doc    = getStringAsdocument(xmldata);
        Element           docEle = doc.getDocumentElement();
        NodeList          nl     = docEle.getElementsByTagName("ROW");

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
                Element el = (Element) nl.item(i);

                list.add(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, tag));
            }
        }

        return list;
    }

    public static String getOneEntry(String xmldata, String tag) {
        String   list   = null;
        Document doc    = getStringAsdocument(xmldata);
        Element  docEle = doc.getDocumentElement();
        NodeList nl     = docEle.getElementsByTagName("ROW");

        if ((nl != null) && (nl.getLength() > 0)) {

            // for(int i = 0 ; i < nl.getLength();i++) {
            Element el = (Element) nl.item(0);

            list = (com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, tag));

            // }
        }

        return list;
    }

    public static ArrayList getStringFromXml(String xmldata, String tag, String tag2) {
        ArrayList<String> list   = new ArrayList<String>();
        ArrayList<String> list2  = new ArrayList<String>();
        Document          doc    = getStringAsdocument(xmldata);
        Element           docEle = doc.getDocumentElement();
        NodeList          nl     = docEle.getElementsByTagName("ROW");

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
                Element el = (Element) nl.item(i);

                list.add(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, tag));
                System.out.println(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, tag));
            }
        }

        return list;
    }

    public static String getOneValue(ArrayList list) {
        String result = "";

        // java.util.ArrayList<String> list = new java.util.ArrayList<String>();

        // list = lists;
        for (int i = 0; i < list.size(); i++) {
            result = (String) list.get(i);
        }

        System.out.println(result + "result--------");

        return result;
    }

    public static Hashtable getPair(String xmldata, String key, String value) {
        Hashtable<String, String> list   = new Hashtable<String, String>();
        Document                  doc    = getStringAsdocument(xmldata);
        Element                   docEle = doc.getDocumentElement();
        NodeList                  nl     = docEle.getElementsByTagName("ROW");

        if ((nl != null) && (nl.getLength() > 0)) {
            for (int i = 0; i < nl.getLength(); i++) {
                Element el = (Element) nl.item(i);

                // list.put(com.fasyl.xnett.util.XMLUtilty.getTextValue(el,tag));
                list.put(com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, key),
                         com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, value));
                System.out.println("Key "+com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, key)+ " value "+com.fasyl.ebanking.util.XMLUtilty.getTextValue(el, value));
            }
        }

        return list;
    }

    
         public static void main(String[] args){
      String xml=     "<ROWSET><ROW><COD_TASK_ID>1400</COD_TASK_ID><NAM_SCREEN>CASA</NAM_SCREEN>" +
            "</ROW><ROW><COD_TASK_ID>1000</COD_TASK_ID><NAM_SCREEN>MAINTENANCE</NAM_SCREEN></ROW><ROW><COD_TASK_ID>9000</COD_TASK_ID>" +
             "<NAM_SCREEN>REGISTRATION</NAM_SCREEN></ROW><ROW><COD_TASK_ID>2000</COD_TASK_ID><NAM_SCREEN>Admin</NAM_SCREEN></ROW>" +
             "</ROWSET>";
      //Hashtable table=getPair(xml,"NAM_SCREEN","COD_TASK_ID");
      java.util.List list  = getStringFromXml(xml,"COD_TASK_ID","NAM_SCREEN");
      
   
      }
     
}


//~ Formatted by Jindent --- http://www.jindent.com
