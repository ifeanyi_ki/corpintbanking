
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.util;

//~--- JDK imports ------------------------------------------------------------
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.MessageFormat;
import java.util.ArrayList;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

/**
 *
 * @author Administrator
 */
public class LoadData {

    private static final String SEL_TXN_PARAM = "select * from eb_process_Parameter";
    private static final String SEL_DOM_IPS = "select ips from ADM_DOM";
    private static final String SEL_BANK_NAME = "select bank_name from bank_list where bank_code=?";
    private static final String SEL_BANK_CODE = "select bank_code from bank_list where bank_name=?";
    private static Hashtable dataHash = null;
    private static ArrayList<String> adminIps = null;
    private static final String[] l_args = new String[0];
    private static final LoadData innerClass = new LoadData();
    private static Connection dbConnection;

    public static Enumeration keys() {
        return dataHash.keys();
    }

    public static synchronized void loadData() {
        ResultSet rs = null;
        Statement sel_txn_param = null;

        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();    // com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:ebanking/ebanking@localhost:1521:orcl");

        String dataKey = null;

        if (dataHash != null) {
            System.out.println("why");
            return;
        }

        try {
            dataHash = new Hashtable();
            sel_txn_param = dbConnection.createStatement();
            rs = sel_txn_param.executeQuery(MessageFormat.format(SEL_TXN_PARAM, l_args));

            while (rs.next()) {
                LoadDataValue loadDataValue = innerClass.new LoadDataValue(rs, dbConnection);

                dataKey = loadDataValue.screenid + loadDataValue.taskId /* +loadDataValue.txnid */;
                System.out.println(dataKey);
                dataHash.put(dataKey, loadDataValue);
                System.out.println(((LoadData.LoadDataValue) dataHash.get(dataKey)).classname + "o gaoooooo");
            }
            System.out.println("before calling loadadminips");
            //loadAdminIps();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            try {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static synchronized ArrayList<String> loadAdminIps() { //added by K.I
        System.out.println("inside loadadminips method");
        ResultSet rs = null;
        PreparedStatement sel_admin_ips = null;

        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();    // com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:ebanking/ebanking@localhost:1521:orcl");



//        if (adminIps != null) {
//            System.out.println("why");
//            return;
//        }

        try {
            adminIps = new ArrayList<String>();
            sel_admin_ips = dbConnection.prepareStatement(SEL_DOM_IPS);
            rs = sel_admin_ips.executeQuery(SEL_DOM_IPS);

            while (rs.next()) {
                adminIps.add(rs.getString("IPS"));

            }
            Iterator<String> iterator = adminIps.iterator();
            while (iterator.hasNext()) {
                System.out.println("ip values loaded form db:" + iterator.next());
            }
            System.out.println("leaving loadadminips");
            return adminIps;
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_admin_ips != null) {
                try {
                    sel_admin_ips.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            try {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return adminIps;
    } //added by K.I

    public static LoadDataValue getDataValue(String screenId, String taskId /* ,     String  txnId */) throws Exception {
        String l_key;
        Object l_value_object;

        System.out.println(screenId + " why" + taskId);
        l_key = getDataKey(screenId, taskId /* , txnId */);
        System.out.println(l_key);

        return (LoadData.LoadDataValue) dataHash.get(l_key.trim());
    }

    public static LoadDataValue getDataValue(String key) throws Exception {
        System.out.println(key);

        return (LoadData.LoadDataValue) dataHash.get(key.trim());
    }

    private static String getDataKey(String screenId, String taskId /* ,String txnId */) {
        return screenId + taskId;
    }

    public class LoadDataValue {

        private String audithandler;
        private String classname;
        private String idrequest;
        private String linecomment;
        private String reqaudit;
        private String reqpreproc;
        private String requireslogin;
        private String screenid;
        private String seqnum;
        private String taskId;
        private String transactiontype;
        private String txnid;
        private String txnmode;

        private LoadDataValue(ResultSet p_rs, Connection con) throws Exception {
            if (p_rs == null) {

                // need to  manage exception
                System.out.println("rs is null");
            } else {
                System.out.println("rs is not null");
                txnid = p_rs.getString("transactiontype").trim();
                seqnum = p_rs.getString("seq_num");
                screenid = p_rs.getString("screen_id".trim());
                taskId = p_rs.getString("task_Id").trim();
                classname = p_rs.getString("classname");
                transactiontype = p_rs.getString("transactiontype");
                requireslogin = p_rs.getString("requireslogin");
                reqaudit = p_rs.getString("req_audit");
                audithandler = p_rs.getString("audithandler");
                linecomment = p_rs.getString("description");
                System.out.println("rs is not null" + linecomment);
            }
        }

        /**
         * @return the txnid
         */
        public String getTxnid() {
            return txnid;
        }

        /**
         * @param txnid the txnid to set
         */
        public void setTxnid(String txnid) {
            this.txnid = txnid;
        }

        /**
         * @return the seqnum
         */
        public String getSeqnum() {
            return seqnum;
        }

        /**
         * @param seqnum the seqnum to set
         */
        public void setSeqnum(String seqnum) {
            this.seqnum = seqnum;
        }

        /**
         * @return the screenid
         */
        public String getScreenid() {
            return screenid;
        }

        /**
         * @param screenid the screenid to set
         */
        public void setScreenid(String screenid) {
            this.screenid = screenid;
        }

        /**
         * @return the taskId
         */
        public String getTaskId() {
            return taskId;
        }

        /**
         * @param taskId the taskId to set
         */
        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        /**
         * @return the classname
         */
        public String getClassname() {
            return classname;
        }

        /**
         * @param classname the classname to set
         */
        public void setClassname(String classname) {
            this.classname = classname;
        }

        /**
         * @return the transactiontype
         */
        public String getTransactiontype() {
            return transactiontype;
        }

        /**
         * @param transactiontype the transactiontype to set
         */
        public void setTransactiontype(String transactiontype) {
            this.transactiontype = transactiontype;
        }

        /**
         * @return the linecomment
         */
        public String getLinecomment() {
            return linecomment;
        }

        /**
         * @param linecomment the linecomment to set
         */
        public void setLinecomment(String linecomment) {
            this.linecomment = linecomment;
        }

        /**
         * @return the idrequest
         */
        public String getIdrequest() {
            return idrequest;
        }

        /**
         * @param idrequest the idrequest to set
         */
        public void setIdrequest(String idrequest) {
            this.idrequest = idrequest;
        }

        /**
         * @return the requireslogin
         */
        public String getRequireslogin() {
            return requireslogin;
        }

        /**
         * @param requireslogin the requireslogin to set
         */
        public void setRequireslogin(String requireslogin) {
            this.requireslogin = requireslogin;
        }

        /**
         * @return the reqaudit
         */
        public String getReqaudit() {
            return reqaudit;
        }

        /**
         * @param reqaudit the reqaudit to set
         */
        public void setReqaudit(String reqaudit) {
            this.reqaudit = reqaudit;
        }

        /**
         * @return the audithandler
         */
        public String getAudithandler() {
            return audithandler;
        }

        /**
         * @param audithandler the audithandler to set
         */
        public void setAudithandler(String audithandler) {
            this.audithandler = audithandler;
        }

        /**
         * @return the reqpreproc
         */
        public String getReqpreproc() {
            return reqpreproc;
        }

        /**
         * @param reqpreproc the reqpreproc to set
         */
        public void setReqpreproc(String reqpreproc) {
            this.reqpreproc = reqpreproc;
        }

        /**
         * @return the txnmode
         */
        public String getTxnmode() {
            return txnmode;
        }

        /**
         * @param txnmode the txnmode to set
         */
        public void setTxnmode(String txnmode) {
            this.txnmode = txnmode;
        }
    }

    public static String getBankName(String bankCode) {

        System.out.println("inside getBAnkName");
        ResultSet rs = null;
        PreparedStatement sel_bank_name = null;
        String bankName = "";
        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();    // com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:ebanking/ebanking@localhost:1521:orcl");

        try {

            sel_bank_name = dbConnection.prepareStatement(SEL_BANK_NAME);
            sel_bank_name.setString(1, bankCode);

            rs = sel_bank_name.executeQuery();

            while (rs.next()) {
                bankName = rs.getString("bank_name");

            }


            System.out.println("leaving get bank name");

        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_bank_name != null) {
                try {
                    sel_bank_name.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            try {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return bankName;

    }
    
    public static String getBankCode(String bankName) {

        System.out.println("inside getBankCode");
        ResultSet rs = null;
        PreparedStatement sel_bank_name = null;
        String bankCode = "";
        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();    // com.fasyl.ebanking.db.ConnectionClass.getConn("jdbc:oracle:thin:ebanking/ebanking@localhost:1521:orcl");

        try {

            sel_bank_name = dbConnection.prepareStatement(SEL_BANK_CODE);
            sel_bank_name.setString(1, bankName);

            rs = sel_bank_name.executeQuery();

            while (rs.next()) {
                bankCode = rs.getString("bank_code");
            }


            System.out.println("leaving get bank code");

        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_bank_name != null) {
                try {
                    sel_bank_name.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            try {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return bankCode;

    }
//  public static void main(String [] args){
//
//      LoadData loadData = new LoadData();
//      loadData.loadData();
//
//              try{
//                  loadData.getDataValue("1", "332");
//      System.out.println(loadData.getDataValue("1", "332").classname+"test");
//      }catch(Exception ex){
//          ex.printStackTrace();
//      }
//
//  }
}


//~ Formatted by Jindent --- http://www.jindent.com
