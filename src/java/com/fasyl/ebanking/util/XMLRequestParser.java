package com.fasyl.ebanking.util;

/*
* Class Name:   XMLRequestParser
*
* Date:         March-2008
*
* � Copyright 2008      Fasyl Nigeria Ltd.
*                       Amazing Grace Plaza
*                       3rd Floor
*                       2e-4e ,Ligali Ayorinde Street
*                       Victoria Island Lagos
*                       Nigeria.
* This source is part of the FASYL FLEXCUBE INTERFACE  Software System and is copyrighted
* by Fasyl Nigeria Ltd. All rights reserved.  No part of this work
* may be reproduced, stored in a retrieval system, adopted or transmitted
* in any form or by any means, electronic, mechanical, photographic, graphic,
* optic recording or otherwise, translated in any language or computer
* language, without the prior written permission of Fasyl Nigeria Ltd.
*
* * ---------------------------------------------------------------------------------------
* The main generic XML parser. It takes XML as ByteArrayInputStream, or physical disk file.
* It returns an HashMap with each xml element name as the key and the xml value as the hashmap
* value.
*
*
* Changes:
* 22-March-2008    Removed unnecessary methods
*
*
*
*
* --------------------------------------------------------------------------------------
 */

//~--- non-JDK imports --------------------------------------------------------

import org.apache.log4j.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.xml.sax.SAXException;

//~--- JDK imports ------------------------------------------------------------

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Vector;

/**
 * Main XML Request parser. Accepts ByteArrayInputStream or physical file as parameter.
 * It returns an HashMap with each xml element name as the key and the xml value as the hashmap
 * value.
 *
 */
public class XMLRequestParser {
    static Logger   logger       = Logger.getLogger(XMLRequestParser.class);
    private HashMap hashMapValue = new HashMap();
    private HashMap hashMapType  = new HashMap();

    /**
     * This constructor takes byte array stream the you want to parse as the parameter.
     * @param xmlValue
     *          XML in byte stream to parse.
     */
    public XMLRequestParser(ByteArrayInputStream xmlValue) {
        javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();

        try {
            javax.xml.parsers.DocumentBuilder db  = dbf.newDocumentBuilder();
            Document                          doc = db.parse(xmlValue);

            for (int i = 0; i <= doc.getChildNodes().getLength() - 1; i++) {
                Node n = doc.getChildNodes().item(i);

                getNodeValue(n);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex);
        }
    }

    public XMLRequestParser(File xmlValue) {
        javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();

        try {
            javax.xml.parsers.DocumentBuilder db  = dbf.newDocumentBuilder();
            Document                          doc = db.parse(xmlValue);

            for (int i = 0; i <= doc.getChildNodes().getLength() - 1; i++) {
                Node n = doc.getChildNodes().item(i);

                getNodeValue(n);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * This constructor is for parsing dynamically the oracle function XML file.
     * Use this constructor to dynamically load function parameters from param element attributes.
     * Not yet fully done.
     * @param inFile
     *          The physical XML file to parse.
     * @param functionName
     *         The function name to parse
     */
    public XMLRequestParser(File inFile, String functionName) {
        javax.xml.parsers.DocumentBuilderFactory dbf = javax.xml.parsers.DocumentBuilderFactory.newInstance();

        try {
            javax.xml.parsers.DocumentBuilder db  = dbf.newDocumentBuilder();
            Document                          doc = db.parse(inFile);
            Node                              n   = doc.getElementsByTagName(functionName).item(0);

            getAttributes(n);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//  public static void main(String[] args) throws SAXException, IOException {
//      File file = new File("f:\\Lnr.xml");
//      XMLRequestParser xrp = new XMLRequestParser(file);
//
//      HashMap hm = xrp.getHashMapValue();
//      System.out.println(hm);
//
//      System.out.println(hm.get("GLOBALINIT"));
//
//      HashMap global = (HashMap)xrp.getHashMapValue();
//        String affDesc = (String)global.get("AFF_DESC");
//        System.out.println(affDesc);
//
//  }

    /**
     * Call when you want to get a node attribute.
     * Not yet fully done.
     * @param node
     *          XML node to get its attribute.
     */
    public HashMap getAttributes(Node node) {
        HashMap hm = new HashMap();

        if (node.hasAttributes()) {
            node.getAttributes().getNamedItem("name");
        }

        return hm;
    }

    /**
     * Call when you want to get a node value.
     * @param node
     *          XML node to get its value.
     */
    private void getNodeValue(Node n) {
        HashMap hm = new HashMap();

        // HashMap<Object, Vector> contentHm = new HashMap<Object, Vector>();
        if (n.hasChildNodes()) {
            for (int i = 0; i <= n.getChildNodes().getLength() - 1; i++) {
                Node node = n.getChildNodes().item(i);

                if (node.hasChildNodes() && (node.getChildNodes().getLength() > 1)) {
                    HashMap h = new HashMap();

                    h = getNodeChildrenValues(node);
                    hm.put(node.getNodeName(), h);
                } else if (node.getNodeName().compareTo("#text") != 0) {
                    hm.put(node.getNodeName(), getNodeValueHM(node));
                }
            }
        } else if ((n.getNodeValue() != null) && (n.getNodeValue().trim().length() > 0)) {
            hm.put(n.getParentNode().getNodeName(), n.getNodeValue());
        } else if ((n.getNodeValue() != null) && (n.getNodeValue().trim().length() == 0)) {
            hm.put(n.getParentNode().getNodeName(), "");
        }

        this.setHashMapValue(hm);
    }

    private String getNodeValueHM(Node n) {
        String hm = "";

        if (n.hasChildNodes() && (n.getChildNodes().getLength() == 1)) {
            Node node = n.getChildNodes().item(0);

            return getNodeValueHM(node);
        } else if ((n.getNodeValue() != null) && (n.getNodeValue().trim().length() > 0)) {
            hm = n.getNodeValue();
        } else if (n.getNodeValue() == null) {
            hm = "";
        } else if ((n.getNodeValue() != null) && (n.getNodeValue().trim().length() == 0)) {
            hm = "";
        }

        return hm;
    }

    private HashMap getNodeChildrenValues(Node n) {
        Vector<HashMap> v          = new Vector<HashMap>();
        HashMap         contentMap = new HashMap();

        for (int i = 0; i <= n.getChildNodes().getLength(); i++) {
            Node node = n.getChildNodes().item(i);

            if (node != null) {
                if (node.hasChildNodes() && (node.getChildNodes().getLength() > 1)) {

                    // v = new Vector<HashMap>();
                    HashMap child = new HashMap();

                    child = getNodeChildrenValues(node);

                    if (contentMap.containsKey(node.getNodeName())) {
                        v.add(child);
                        contentMap.put(node.getNodeName(), v);
                    } else {
                        v = new Vector<HashMap>();
                        v.add(child);
                        contentMap.put(node.getNodeName(), v);
                    }
                } else if (node.hasChildNodes() && (node.getNextSibling() == null)) {
                    contentMap.put(node.getNodeName(), getNodeValueHM(node));
                } else if (node.hasChildNodes() && (node.getNextSibling().getNodeValue() != null)) {
                    contentMap.put(node.getNodeName(), getNodeValueHM(node));

                    // v.add(getNodeValueHM(node));
                } else if (!node.hasChildNodes() && (node.getNodeName().compareToIgnoreCase("#text") > 0)) {

                    // contentMap = new HashMap();
                    contentMap.put(node.getNodeName(), "");

                    // v.add(contentMap);
                } else if (node.hasChildNodes() && (node.getNodeName().compareToIgnoreCase("#text") > 0)
                           && (node.getNextSibling().getNodeValue() != null)
                           && (node.getNextSibling().getNodeValue().trim().length() == 0)) {

                    // contentMap = new HashMap();
                    contentMap.put(node.getNodeName(), "");

                    // v.add(contentMap);
                } else if (node.hasChildNodes() && (node.getNodeName().compareToIgnoreCase("#text") > 0)
                           && (node.getNextSibling().getNodeValue() == null)) {

                    // contentMap = new HashMap();
                    contentMap.put(node.getNodeName(), getNodeValueHM(node));

                    // v.add(contentMap);
                } else if (node.hasChildNodes() && (node.getNextSibling().getNodeValue() != null)
                           && (node.getNextSibling().getNodeValue().trim().length() == 0)) {

                    // contentMap = new HashMap();
                    contentMap.put(node.getNodeName(), "");

                    // v.add(contentMap);
                }
            }
        }

        return contentMap;
    }

    /**
     * Get the class level HashMap that contains the
     * value of the parsed XML.
     */
    public HashMap getHashMapValue() {
        return hashMapValue;
    }

    /**
     * Never need to call this method.
     * @param hashMapValue
     */
    private void setHashMapValue(HashMap hashMapValue) {
        this.hashMapValue = hashMapValue;
    }

    /**
     *
     * @return
     *      Hashmap containing param values read from oracle function configuration file.
     */
    public HashMap getHashMapType() {
        return hashMapType;
    }

    public void setHashMapType(HashMap hashMapType) {
        this.hashMapType = hashMapType;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
