
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.util;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.util.CacheClass._InnerCache;

//~--- JDK imports ------------------------------------------------------------

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

/**
 *
 * @author Administrator
 */
public class CacheClass {

    /**
     * Name of this component.
     */
    private static final String THIS_COMPONENT_NAME = "CacheClass"
    ;

    /**
     * This table to holds the cache instances indexed by their identities.
     */
    private static final Hashtable cacheTable = new Hashtable()
    ;

    /**
     * This default instance of this class.
     */
    private static final CacheClass defaultInstance = new CacheClass()
    ;

    // --------------------------------------------------------------------------

    /**
     * The server's language id.
     */

    // --------------------------------------------------------------------------
    private static Hashtable dataTable
    ;

    // --------------------------------------------------------------------------

    /**
     * This constructor has been deliberately made private so that this class
     * cannot be instantiated from outside.
     */
    private CacheClass() {}

    // --------------------------------------------------------------------------

    /**
     * This method creates a cache identified by a given identity that stores
     * objects of given type.
     *
     * @param  p_identity              The identity that identifies a new cache.
     * @param  p_class                 Class whose instances can be stored in the cache.
     *
     * @throws Exception               If the supplied identity is null or empty
     * @throws Exception               If the supplied class is null.
     * @throws Exception               If the supplied identity identifies an existing
     *                                               cache.
     */
    public static void createCache(String p_identity, Class p_class) throws Exception {
        cacheTable.put(p_identity, defaultInstance.new _InnerCache(p_identity, p_class));
    }

    // --------------------------------------------------------------------------

    /**
     * This method clears and removes a cach identitfied by the supplied identity.
     * The method ignores a null or empty identity or an identity that does not
     * identify a cache.
     *
     * @param  p_identity      Identity, identifying a cahce to be cleared and removed.
     */
    public static void removeCache(String p_identity) {
        _InnerCache l_cache = null;

        if ((p_identity == null) || (p_identity.length() == 0)) {
            return;
        }

        synchronized (cacheTable) {
            if ((l_cache = (_InnerCache) cacheTable.get(p_identity)) != null) {
                cacheTable.remove(p_identity);
                l_cache.objectStack.clear();
                l_cache = null;
            }
        }
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns a chched instance from a chache identified by the given
     * identity.
     *
     * @param  p_identity      Idntity idenifying a cache.
     *
     * @return Cached instance, null if the named cache is empty.
     *
     * @throws Exception       If the supplied identity is null or empty.
     * @throws Exception       If the supplied identity does not identify a cache.
     */
    public static Object getObject(String p_identity) throws Exception {
        CacheClass._InnerCache l_cache = null;

        if ((l_cache = (_InnerCache) cacheTable.get(p_identity)) == null) {
            System.out.println("cache is null ");
        }

        // l_cache = new CacheClass().new _InnerCache();
        System.out.println(" about to return l_cache");

        return l_cache.getObject();
    }

    // --------------------------------------------------------------------------

    /**
     * This method puts a given instance into a cache identified by the given
     * identity.
     *
     * @param  p_identity      An identity identifying a cache that receives an
     *                                       instance.
     * @param  p_object        The object to be put back in the cache.
     *
     * @throws Exception       If the supplied identity is null or empty.
     * @throws Exception       If the supplied identity does not identity a cache.
     * @throws Exception       If the supplied object is null.
     * @throws Exception       If the supplied object is of type other than those
     *                                       cached by the named cache.
     */
    public static void putObject(String p_identity, Object p_object) throws Exception {
        _InnerCache l_cache = null;

        if ((l_cache = (_InnerCache) cacheTable.get(p_identity)) == null) {
            System.out.println(" object is null and can not be put in the cache");
        }

        System.out.println(" this is lobject " + p_object);
        l_cache.putObject(p_object);
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns the number of elements currently residing in the cache
     * identified by the given identity.
     *
     * @throws Exception       If the supplied identity is null or empty.
     * @throws Exception       If the supplied identity does not identify a cache.
     */
    public static int getNbrElements(String p_identity) throws Exception {
        _InnerCache l_cache = null;

        return l_cache.cacheSize;
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns an enumeration over all the registered identities.
     */
    public static Enumeration getRegisteredIdentities() {
        return cacheTable.keys();
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns true if the supplied identity identifies a cache. The
     * method returns false if the supplied identity is null or empty.
     *
     * @param  p_identity      The identity to be checked.
     */
    public static boolean isIdentityRegistered(String p_identity) {
        if ((p_identity == null) || (p_identity.length() == 0)) {
            return false;
        }

        return cacheTable.containsKey(p_identity);
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns returns the class represntation of a class whose
     * instances are cached.
     *
     * @param  p_identity      Name of the cache.
     *
     * @throws Exception       Id the supplied identity is null or empty.
     * @throws Exception       If the supplied identity does not represent cache.
     */
    public static Class getCachedClass(String p_identity) throws Exception {
        _InnerCache l_cache = null;

        return l_cache.cachedClass;
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns array of class representations of all the classes cached
     * by the cache.
     */
    public static Class[] getAllClasses() {
        Class       l_class_array[] = new Class[cacheTable.size()];
        Enumeration l_enum          = cacheTable.elements();

        for (int l_i = 0; l_enum.hasMoreElements(); l_i++) {
            l_class_array[l_i] = ((_InnerCache) l_enum.nextElement()).cachedClass;
        }

        return l_class_array;
    }

    // --------------------------------------------------------------------------
//  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * This inner class models an individual cache that is used to store the objects of
     * a specific type.
     */
    public class _InnerCache {

        /**
         * This variable points to the fully qualified name of the class whose objects
         * may be cached in this cache.
         */
        private final String cacheClassName
        ;

        // --------------------------------------------------------------------------

        /**
         * This variable stores the identity that identifies this cache.
         * cached.
         */
        private final String cacheId
        ;

        /**
         * This variable stores the current number of elements in this cache.
         */
        public int cacheSize
        ;

        /**
         * This variable points to the class cached by this cache.
         */
        private Class cachedClass
        ;

        // --------------------------------------------------------------------------

        /**
         * This variable stores the last time that this cache was accessed.
         */
        public long lastAccessTime
        ;

        /**
         * This stack stores reusable objects to be cached.
         */
        public final Stack objectStack
        ;

        /**
         * This constructor creates this cache.
         *
         * @param  p_cache_id              Identity that identifies this cache.
         * @param  p_class                 Class representation whose objects are stored in
         *                                               this cache.
         */
        public _InnerCache(String p_cache_id, Class p_class) {
            cacheId        = p_cache_id;
            cacheClassName = p_class.getName();
            cachedClass    = p_class;
            objectStack    = new Stack();
        }

        // --------------------------------------------------------------------------

        /**
         * This method returns the instance from this cache. THis method returns null
         * if this cache is empty.
         */
        public Object getObject() {
            System.out.println(" inside inner class get object");
            lastAccessTime = System.currentTimeMillis();

            synchronized (objectStack) {
                if (!objectStack.isEmpty()) {
                    System.out.println(" object stack is not empty");
                    cacheSize--;
                    System.out.println(" This is cache size after removal " + cacheSize);

                    return objectStack.pop();
                }
            }

            System.out.println(" object stack is null");

            return null;
        }

        // --------------------------------------------------------------------------

        /**
         * This methods puts the given object in this cache.
         *
         * @param  p_obj           The object to be cached.
         * @throws Exception       If the object to be stored is not of the type whose
         *                                       objetcs may be stored in this cache.
         */
        private void putObject(Object p_obj) throws Exception {
            System.out.println(" inside innerclass put object");
            lastAccessTime = System.currentTimeMillis();

            synchronized (objectStack) {
                System.out.println(" putting it in the stack class");
                objectStack.push(p_obj);
                cacheSize++;
                System.out.println(" this is cachesize " + cacheSize);
            }
        }

        // --------------------------------------------------------------------------
    }

//  public static void main(String []args){
//     CacheClass test = defaultInstance;
//     try{
//     defaultInstance.getObject("com.fasyl.ebanking.logic.initFT");
//     System.out.println("test");
//     }catch(Exception ec){
//         ec.printStackTrace();
//     }
//  }
}


//~ Formatted by Jindent --- http://www.jindent.com
