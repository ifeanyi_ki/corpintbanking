/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.util;

import java.net.URL;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.Loader;

/**
 *
 * @author Administrator
 */
public class LogManager implements ServletContextListener {

     private static final Logger logger = Logger.getLogger(LogManager.class.getName());
 
      private ServletContext context = null;
 
     
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // throw new UnsupportedOperationException("Not supported yet.");
        
         context = sce.getServletContext();
         
         
         
            String prefix = context.getRealPath("/");
      // String file = "log4j.xml";
        String file = "log4j.properties";
       // URL url = getClass().getClassLoader().getResource(prefix + file);
       // System.out.println(url.toString());
       System.out.println(prefix + file);

       URL url = Loader.getResource(file);

       if (url != null)
       {
          System.out.println(url.toString());
        //  DOMConfigurator.configure(url);
          PropertyConfigurator.configure(url);
          System.out.println(" loaded log4j properties successfully ! ");
       }
       else
       {
           System.out.println(" will use default config ");
           BasicConfigurator.configure();
       }

      //  setLogger(Logger.getLogger("ConfigManager"));
        
         logger.info(" context DESCRIPTION FROM CONFIG MGR: " + context.toString());

        
     //  cfg.setLogger(logger);
        logger.info(" logger initialized !");

        logger.info(" context initialization ... ");
        
        
    }
    
    
    

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
       // throw new UnsupportedOperationException("Not supported yet.");
        
        context = sce.getServletContext();
        
        
    }
    
}
