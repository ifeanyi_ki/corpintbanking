
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.util;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.MessageFormat;

/**
 *
 * @author Administrator
 */
public class AuditLogHandler {
    private static final String INS_AUDIT_TRAIL_STMT =
        "insert into auditlog (" + "       TXN_ID            " + ",      SEQUENCE_NUM      "
        + ",      USER_ID           " + ",      DATTXN            " + ",      TXN_DESC          "
        + ",      RETURNCODE        " + ",      ERRORCODE         " + ",      AUDITREQUEST      "
        + ",      AUDITRESPONSE     " + ",      CUST_ID           " + ",      REMOTE_ADDRESS    "
        + ",      SESSION_ID        " + ",      TRX_AMT           " + ",      RESP_STATUS       "
        + ",      RETRY_COUNT       " + ",      PROCESS_STATUS    " + ",          ADDED_DATE    "
        + ",          TRX_SEQ       " + ",        CUSTOMER_TYPE       "
        + " ) values (?, ?, ?,sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,sysdate,?,?)";

    public static int log(DataofInstance instData) {
        Connection        connect = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt   = null;
        int               result  = -2;

        try {
            pstmt = connect.prepareStatement(INS_AUDIT_TRAIL_STMT);
            pstmt.setString(1, instData.datavalue.getTransactiontype());
            pstmt.setString(2, "");
            pstmt.setString(3, instData.userId);
            pstmt.setString(4, instData.datavalue.getLinecomment());
            pstmt.setString(5, instData.returnCode);
            pstmt.setString(6, instData.errorCode);
            pstmt.setString(7, instData.request);
            pstmt.setString(8, instData.response);
            pstmt.setString(9, instData.custId);
            pstmt.setString(10, instData.remoteAddress);
            pstmt.setString(11, instData.sessionId);
            pstmt.setString(12, instData.trxAmount);
            pstmt.setString(13, instData.respStatus);
            pstmt.setString(14, instData.retryCount);
            pstmt.setString(15, instData.processStatus);
            pstmt.setString(16, instData.requestId);
            pstmt.setString(17, instData.customerType);
            result = pstmt.executeUpdate();

//          pstmt.setString(17, instData);
//          pstmt.setString(18, instData);
            System.out.println(result);
        } catch (Exception ex) {
            result = -1;
            System.out.println(result);
            ex.printStackTrace();
        } finally {
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
            return result;
        
    }

    public static int updateLog(DataofInstance instdata) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        int        result  = -2;
        String     query   =
            "update auditlog set auditresponse=?,resp_status=?,errorcode=?,process_status=?,error_message=? where trx_seq=? and session_id=?";
        PreparedStatement pstmt = null;

        try {

            // String response = (String)instdata.result.get("returResult")==null?(String)instdata.result.get("returResult")
            pstmt = connect.prepareStatement(query);
            pstmt.setString(1, (String) instdata.result.get("returnResult"));
            pstmt.setString(2, instdata.respStatus);
            pstmt.setString(3, instdata.errorCode);
            pstmt.setString(4, instdata.processStatus);
            pstmt.setString(5, instdata.errormessage);
            pstmt.setString(6, instdata.requestId);
            pstmt.setString(7, instdata.sessionId);
            result = pstmt.executeUpdate();
            System.out.println(result + " This is the number of rows updated in the log");
        } catch (Exception ex) {

            // ex.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public static int updateErrorLog(DataofInstance instdata) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        int        result  = -2;
        String     query   =
            "update auditlog set PROCESS_STATUS=?,errorcode=?,error_message=? where trx_seq=? and session_id=?";
        PreparedStatement pstmt = null;

        try {

            // String response = (String)instdata.result.get("returResult")==null?(String)instdata.result.get("returResult")
            pstmt = connect.prepareStatement(query);

            // pstmt.setString(1, (String)instdata.result.get("returnResult"));
            pstmt.setString(1, instdata.processStatus);
            pstmt.setString(2, instdata.errorCode);
            pstmt.setString(3, instdata.errormessage);
            pstmt.setString(4, instdata.requestId);
            pstmt.setString(5, instdata.sessionId);
            result = pstmt.executeUpdate();
            System.out.println(result + " This is the number of rows updated in the log");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
