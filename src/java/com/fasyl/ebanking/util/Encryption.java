package com.fasyl.ebanking.util;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Nisar
 */
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.URLDecoder;
import java.net.URLEncoder;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class Encryption {
    private static final String tope = "temitope";
    private static String       decurl;
    private static String       enurl;
    private static SecretKey    key;
    private static KeySpec      keySpec;

    public Encryption() {
        try {
            keySpec = new DESKeySpec(tope.getBytes());
            key     = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    public static String encrypt(String msg) {
        try {
            keySpec = new DESKeySpec(tope.getBytes());
            key     = SecretKeyFactory.getInstance("DES").generateSecret(keySpec);

            Cipher ecipher = Cipher.getInstance(key.getAlgorithm());

            ecipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] utf8 = msg.getBytes("UTF8");
            byte[] enc  = ecipher.doFinal(utf8);

            enurl = new sun.misc.BASE64Encoder().encode(enc);

            return URLEncoder.encode(enurl);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        // return new sun.misc.BASE64Encoder().encode(enc);
        return enurl;
    }

    @SuppressWarnings("deprecation")
    public static String decrypt(String msg) {
        byte[] decode = null;
        String ss     = URLDecoder.decode(msg);

        try {
            Cipher decipher = Cipher.getInstance(key.getAlgorithm());

            decipher.init(Cipher.DECRYPT_MODE, key);
            decode = new sun.misc.BASE64Decoder().decodeBuffer(ss);

            byte[] utf8 = decipher.doFinal(decode);

            decurl = new String(utf8, "UTF8");

            // System.out.println(msg);
            return decurl;

            // return new String(decode, "UTF8");
        } catch (InvalidKeyException e) {
            e.printStackTrace();

            // } catch (InvalidKeySpecException e) {
            // e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }    // catch (InvalidKeySpecException e) {

        // e.printStackTrace();
        // }
        return decurl;
    }

    public String encodeURL(String url) {
        try {
            return URLEncoder.encode(url, "UTF-8");

            // URLDecoder.decode(s)
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

  public static void main (String args []){
    Encryption encryption = new Encryption();
   System.out.println( encryption.decrypt("GYSeKQCREVqBoqCE9xQ5fg%3D%3D"));
  }
}


//~ Formatted by Jindent --- http://www.jindent.com
