/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.util;

import com.fasyl.ebanking.main.TaskManager;
import java.util.concurrent.ExecutorService;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

/**
 *
 * @author NYEMIKE
 */
public class TaskManagerListener implements ServletContextListener {

     private static final Logger logger = Logger.getLogger(TaskManagerListener.class.getName());
 
    
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //  for now the taskmanager is initialised in Maincontroller no need to add code here. 20150220
        
       // logger.info(" TASK MANAGER context initialised ! ");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        
        logger.info("shutdown initiated. will shutdown task manager threads . . .");
        
        ExecutorService execService = TaskManager.getTaskManager();
        execService.shutdown();
        
        logger.info(" task manager thread pool shutdown ! ");
        
       }
    
}
