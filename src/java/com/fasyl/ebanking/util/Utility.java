
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.util;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.db.ConnectionClass;
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.EbankingConfiguration;

//~--- JDK imports ------------------------------------------------------------
import java.io.IOException;

import java.net.InetAddress;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class Utility implements EbankingConfiguration {    // need to get the actual time and seconds the method was called or use the customer number.

    public static Random sequenceIdRandom = new Random();
    public static Random alphaIdRandom = new Random();

    public static HashMap processError(DataofInstance instData, String errorCode) {
        HashMap error = new HashMap();

        instData.processStatus = ERRORSTATUS;
        if (errorCode.isEmpty()) {
            errorCode = GENERAL_ERROR_MESSAGE;
        }
        error.put("errorcode", errorCode);

        return error;
    }

    public static synchronized String generateSequenceNo() {
        String result = "";

        for (int a = 0; a < 11; a++) {
            int iResult = sequenceIdRandom.nextInt(10);

            result += String.valueOf(iResult);
        }

        return result;
    }

    public static synchronized String genaratePassword() {
        Random passwdIdRandom = new Random();    // we dont care if it generate the same password for the same user
        String result = "";

        for (int a = 0; a < 3; a++) {
            int iResult = passwdIdRandom.nextInt(9);

            result += String.valueOf(iResult);
            result = Utility.padAlpha(result, PASSWD_LENGTH);
        }

        System.out.println(result);

        return result;
    }

    //Ayomide added this for generating Token
    public static synchronized String genaratePin() {
        Random passwdIdRandom = new Random();
        String result = "";

        for (int a = 0; a < 8; a++) {
            int iResult = passwdIdRandom.nextInt(9);

            result += String.valueOf(iResult);
        }

        System.out.println(result);

        return result;
    }

    public static boolean pingServer() {
        String args[] = null;

        // DataOutputStream is =new DataOutputStream();
        boolean result = false;

        try {

            /*
             * Socket t = new Socket("127.0.0.1",7);
             * t.setSoTimeout(5000);
             * // StringBufferInputStream is = new StringBufferInputStream(t.getInputStream());
             * // DataInputStream is = new DataInputStream(t.getInputStream());
             * BufferedReader is =new BufferedReader(new InputStreamReader(t.getInputStream()));
             * PrintStream ps = new PrintStream(t.getOutputStream());
             * ps.println("Hello");
             * ps.flush();
             * String str = is.readLine();
             * is.close();
             * if (str.equals("Hello")){
             * System.out.println("Alive!") ;
             * result=true;
             * }
             * else{
             * System.out.println("Dead or echo port not responding");
             * }
             * t.close();
             */
            String ipaddress = "127.0.0.1";
            InetAddress address = InetAddress.getByAddress(ipaddress.getBytes());

            System.out.println("Name: " + address.getHostName());
            System.out.println("Addr: " + address.getHostAddress());
            System.out.println("Reach: " + address.isReachable(3000));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println(result);

            // return result;
        }

        return result;
    }

    public static String returnHolidayList(int month, int year, String branch) {
        Connection con = null;
        String query = "select holiday_list from sttm_lcl_holiday where month=? and year=? and branch_code=?";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String result = null;

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt = con.prepareStatement(query);
            pstmt.setInt(1, month);
            pstmt.setInt(2, year);
            pstmt.setString(3, "001");
            rs = pstmt.executeQuery();

            if (rs.next()) {
                result = rs.getString(1);
            }
            System.out.println("Holiday List: " + result);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }

    public static synchronized String getReqId() {
        String result = "";
        String sequence = "select reqid.NEXTVAL as sequence from dual ";
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        String[] l_args = new String[0];
        ResultSet rs = null;
        Statement sel_txn_param = null;

        try {
            sel_txn_param = connect.createStatement();
            rs = sel_txn_param.executeQuery(MessageFormat.format(sequence, l_args));

            if (rs.next()) {
                result = rs.getString(1);
                System.out.println("this is the request id : " + result);
            }
        } catch (Exception ex) {
            System.out.println("this is the request id error : " + result);
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }

        result = padAlpha(result, 8);
        System.out.println(result);

        return result;
    }

    public static java.sql.Timestamp getCurrentDate() {
        java.sql.Timestamp result = null;
        String sequence = "select sysdate from dual ";

        // Connection connect=com.fasyl.ebanking.db.DataBase.getConnection();
        Connection connect = null;
        String[] l_args = new String[0];
        ResultSet rs = null;
        Statement sel_txn_param = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            sel_txn_param = connect.createStatement();
            rs = sel_txn_param.executeQuery(MessageFormat.format(sequence, l_args));

            if (rs.next()) {
                result = rs.getTimestamp(1);
                System.out.println("this is the request id : " + result);
            }
        } catch (Exception ex) {
            System.out.println("this is the request id error : " + result);
            ex.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }

    public static java.sql.Date getsqlCurrentDate() {
        java.sql.Date result = null;
        String sequence = "select sysdate from dual ";

        // Connection connect=com.fasyl.ebanking.db.DataBase.getConnection();
        Connection connect = null;
        String[] l_args = new String[0];
        ResultSet rs = null;
        Statement sel_txn_param = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            sel_txn_param = connect.createStatement();
            rs = sel_txn_param.executeQuery(MessageFormat.format(sequence, l_args));

            if (rs.next()) {
                result = rs.getDate(1);
                System.out.println("this is the request id : " + result);
            }
        } catch (Exception ex) {
            System.out.println("this is the request id error : " + result);
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }

        return result;
    }

    public static String getsqlCurrentDate2() {
        String result = null;
        String sequence = "select to_char(to_date(sysdate,'dd-mon-yyyy')) from dual ";

        // Connection connect=com.fasyl.ebanking.db.DataBase.getConnection();
        Connection connect = null;
        String[] l_args = new String[0];
        ResultSet rs = null;
        PreparedStatement sel_txn_param = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            sel_txn_param = connect.prepareStatement(sequence);
            rs = sel_txn_param.executeQuery();

            if (rs.next()) {
                result = rs.getString(1);
                System.out.println("this is the request id : " + result);
            }
        } catch (Exception ex) {
            System.out.println("this is the request id error : " + result);
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }

        return result;
    }

    public static boolean isReqValid(long longs) {
        boolean secs = true;
        long date = new java.sql.Date(longs).getTime();
        long currentDate = getCurrentDate().getTime();

        System.out.println(currentDate + " " + date);

        long sec = currentDate - date;

        System.out.println(sec);

        if ((sec / 1000) >= 300) {
            secs = false;
        }

        return secs;
    }

    public static String padAlpha(String p_str, int p_target_length) {
        int l_length;
        StringBuffer l_buf = null;
        byte l_byte[] = new byte[1];

        if (p_str == null) {
            l_buf = new StringBuffer();
            l_length = 0;
        } else if ((l_length = p_str.length()) == 0) {
            l_buf = new StringBuffer();
            l_length = 0;
        } else if (l_length < p_target_length) {
            l_buf = new StringBuffer(p_str);
        } else {
            return p_str;
        }

        for (; l_length < p_target_length; l_length++) {
            alphaIdRandom.nextBytes(l_byte);
            l_buf.append((char) ((Math.abs(l_byte[0]) % 26) + 'A'));
        }

        return l_buf.toString();
    }

    public static String EndianesDate() {
        StringBuffer buff = new StringBuffer();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(cal.YEAR);
        String years = year + "";

        buff.append(years.substring(2, 4));
        System.out.println(buff.toString());

        int days = cal.get(cal.DAY_OF_YEAR);

        buff.append(days);
        System.out.println(buff.toString());

        return buff.toString();
    }

    public static java.sql.Date convertString2Date(String dates) {
        java.util.Date date = new java.util.Date();
        Date sqlDate = null;
        DateFormat format = new SimpleDateFormat("dd-MMMM-yyyy");
        System.out.println("I am tired" + dates);
        try {
            Calendar cal = Calendar.getInstance();

            format.setLenient(false);
            date = format.parse(dates);
            cal.setTime(date);
            sqlDate = new Date(cal.getTimeInMillis());
            System.out.println(sqlDate);

            // System.out.println(date);
        } catch (Exception ex) {
            System.out.println(ex + " error from convertString2Date");
        }

        return sqlDate;
    }

    public static java.util.Date convertStringToDate(String dates) {
        java.util.Date date = new java.util.Date();
        Date sqlDate = null;
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Calendar cal = Calendar.getInstance();

            format.setLenient(false);
            date = format.parse(dates);
            cal.setTime(date);

            // /sqlDate = new Date(cal.getTimeInMillis());
            System.out.println(sqlDate);

            // System.out.println(date);
        } catch (Exception ex) {
            System.out.println(ex + " error from convertString2Date");
            //Try Added by Ayo to test diff format
            try {
                format = new SimpleDateFormat("dd-MMM-yyyy");
                date = format.parse(dates);
            } catch (ParseException ex1) {
                System.out.println(ex + " error still occured from convertString2Date");
            }
        }

        return date;
    }

    /*
     *  public static void main(String [] args){
     *    Utility util = new Utility();
     *    util.convertString2Date("30/07/2006");
     *
     * }
     */
//  public static void main(String [] args){
//     //boolean result = Utility.isReqValid(1294570000000L);
//      Utility util = new Utility();
//     //boolean results = util.pingServer();
//
//    System.out.println(Utility.addMonthsToDate("15/06/2011", "12"));
//  }
    public static String addMonthsToDate(String date, String tenure) {

        // create Calendar instance
        java.util.Date dates = convertStringToDate(date);

        if (bDebugOn) {
            System.out.println(dates + " dates");
        }

        Calendar now = Calendar.getInstance();

        now.setTime(dates);
        now.add(Calendar.MONTH, Integer.parseInt(tenure));
        System.out.println("date after  months : " + now.get(Calendar.DATE) + "/" + (now.get(Calendar.MONTH) + 1) + "/"
                + now.get(Calendar.YEAR));

        String result = now.get(Calendar.DATE) + "/" + +(now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR);

//      Date result = new Date(now.getTimeInMillis());
        if (bDebugOn) {
            System.out.println(result + " result");
        }

        return result;
    }

    public static String padFigure(String value) {    // this is used to generate date in yyyymmdd
        if (!((value == null) && value.equals(""))) {
            if (value.length() < 2) {
                value = "0" + value;
            }
        }

        return value;
    }

    public static String FormatCurrentDate() {
        Calendar now = Calendar.getInstance();

        now.setTime(getCurrentDate());

        String result = now.get(Calendar.DATE) + "-" + +(now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.YEAR);

        result = String.valueOf(now.get(Calendar.YEAR)) + padFigure(String.valueOf((now.get(Calendar.MONTH)) + 1))
                + padFigure(String.valueOf((now.get(Calendar.DATE))));

        return result;
    }

//  public static void main(String [] args){
//      String result = getsqlCurrentDate().toString();
//      System.out.println(result);
//  }
    public HashMap getAddressBook() {
        Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap map = new HashMap();

        try {
            pstmt = con.prepareStatement("select cod_usr_id,cod_usr_name, email from sm_user_access");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                System.out.println("<<<<" + rs.getString(1) + ">>>>>>>");
                map.put(rs.getString(1), rs.getString(2) + ";" + rs.getString(3));

            }

        } catch (Exception ex) {
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return map;
    }

    public void updateUserMacAddress(String macAddress, String user_id) {
        System.out.println("*** about to update mac address with the following parameters ***");
        System.out.println("macAdrees: " + macAddress);
        System.out.println("user_id: " + user_id.toUpperCase());
        user_id = user_id.toUpperCase();
        
        System.out.println("user id in upper case in updateusermacaddress method: " + user_id);
        Connection con = null;
        String query = "update sm_user_access set machine_id ='" + macAddress + "' where cod_usr_id ='" + user_id + "'";
        System.out.println("query string in updateusermacaddress method: " + query);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int result = 0;

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement(query);
//            pstmt.setString(1, macAddress);
//            pstmt.setString(2, user_id);

            result = pstmt.executeUpdate();
           
            if (result == 1) {
                System.out.println("*** machine id updated successfully ***");
            } else {
                System.err.println("*** machine id not updated ***");
            }

        } catch (SQLException se) {
            System.err.println("error in saving machine id: " + se.getMessage());
            se.printStackTrace(System.out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public String getUserMacAddress(String user_id) {
        System.out.println("*** about to get mac address with the following parameters ***");
        System.out.println("user_id: " + user_id);

        user_id = user_id.toUpperCase();
        
        System.out.println("user id in upper case in getUserMacAddress method: " + user_id);
        Connection con = null;
        String macAddress = "";
        String query = "select machine_id from sm_user_access where cod_usr_id='" + user_id + "'";
        System.out.println("query string in getusermacaddress method: " + query);
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            //con = ConnectionClass.getConn("");//remove
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement(query);
            //pstmt.setString(1, user_id);

            rs = pstmt.executeQuery();

            if (rs.next()) {
                macAddress = rs.getString("machine_id");
                System.out.println("mac address from the db is: " + macAddress);
            }else{
                System.out.println("no mac address found for this user: " + user_id + "in getusermacaddress method");
            }

        } catch (SQLException se) {
            System.err.println("error in retrieving machine id: " + se.getMessage());
            se.printStackTrace(System.out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return macAddress;

    }

    public void saveUserMacAddress(String macAddress, String user_id) {
        System.out.println("*** about to save mac address with the following parameters ***");
        System.out.println("macAdrees: " + macAddress);
        System.out.println("user_id: " + user_id);
        
        user_id = user_id.toUpperCase();
        System.out.println("user id in upper case in utility method saveMacAddress: " + user_id);
        
        if (!isMacAddressEmpty(user_id)) {
            Connection con = null;
            String query = "update sm_user_access set machine_id = ? where cod_usr_id = ?";
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            int result = 0;

            try {
                //con = ConnectionClass.getConn(""); //remove
                con = com.fasyl.ebanking.db.DataBase.getConnection();
                pstmt = con.prepareStatement(query);
                pstmt.setString(1, macAddress);
                pstmt.setString(2, user_id);

                result = pstmt.executeUpdate();

                if (result == 1) {
                    System.out.println("*** machine id saved successfully ***");
                } else {
                    System.err.println("*** machine id not saved ***");
                }

            } catch (SQLException se) {
                System.err.println("error in saving machine id: " + se.getMessage());
                se.printStackTrace(System.out);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (pstmt != null) {
                    try {
                        pstmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (con != null) {
                    try {
                        con.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } else {
            System.out.println("*** machine id already exists ***");
        }

    }

    public boolean isMacAddressEmpty(String user_id) {
        System.out.println("*** about to confirm wether mac address exists with the following parameters ***");
        System.out.println("user_id: " + user_id);
        
        user_id = user_id.toUpperCase();
        
        System.out.println("user id in upper case in isMacAddressEmpty method in utility class: " + user_id);
        
        Connection con = null;
        String query = "select machine_id from sm_user_access where cod_usr_id ='" +  user_id + "'";
        System.out.println("query string for isMacAddressEmpty: " + query);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        boolean result = false;
        String macAddress = "";
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement(query);
            //pstmt.setString(1, user_id);

            rs = pstmt.executeQuery();
            
                      
            if (rs.next()) {
                macAddress = rs.getString("machine_id");
                System.out.println("mac address selected from the db for user id" + user_id + " : " + macAddress);
                if (macAddress != null) {
                    if (!"".equalsIgnoreCase(macAddress)) {
                        System.out.println("*** machine id exists ***");
                        result = true;
                    }

                }

            }else{
                System.out.println("no machine id found for this user id in isMacAddressEmpty method: " + user_id);
            }

        } catch (SQLException se) {
            result = true; 
            System.err.println("error in checking wether mac address exists for this user id: " + user_id + ":" + se.getMessage());
            se.printStackTrace(System.out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        System.out.println("result of mac address verification: " + result);
        return result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
