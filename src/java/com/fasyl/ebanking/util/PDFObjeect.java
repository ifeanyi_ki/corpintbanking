/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.util;

/**
 *
 * @author baby
 */
public class PDFObjeect {
    private String date;
    private String runninbal;
    private String description;
    private String credit;
    private String debit;

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the runninbal
     */
    public String getRunninbal() {
        return runninbal;
    }

    /**
     * @param runninbal the runninbal to set
     */
    public void setRunninbal(String runninbal) {
        this.runninbal = runninbal;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the credit
     */
    public String getCredit() {
        return credit;
    }

    /**
     * @param credit the credit to set
     */
    public void setCredit(String credit) {
        this.credit = credit;
    }

    /**
     * @return the debit
     */
    public String getDebit() {
        return debit;
    }

    /**
     * @param debit the debit to set
     */
    public void setDebit(String debit) {
        this.debit = debit;
    }
    
}
