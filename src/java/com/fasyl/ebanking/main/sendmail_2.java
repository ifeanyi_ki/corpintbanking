
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;
import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.servlet.http.HttpServlet;

public class sendmail_2 extends HttpServlet implements ConnectionListener, TransportListener {

//  implement ConnectionListener interface
    public void opened(ConnectionEvent e) {
        System.out.println(">>> ConnectionListener.opened()");
    }

    public void disconnected(ConnectionEvent e) {}

    public void closed(ConnectionEvent e) {
        System.out.println(">>> ConnectionListener.closed()");
    }

    // implement TransportListener interface
    public void messageDelivered(TransportEvent e) {
        System.out.print(">>> TransportListener.messageDelivered().");
        System.out.println(" Valid Addresses:");

        Address[] valid = e.getValidSentAddresses();

        if (valid != null) {
            for (int i = 0; i < valid.length; i++) {
                System.out.println("    " + valid[i]);
            }
        }
    }

    public void messageNotDelivered(TransportEvent e) {
        System.out.print(">>> TransportListener.messageNotDelivered().");
        System.out.println(" Invalid Addresses:");

        Address[] invalid = e.getInvalidAddresses();

        if (invalid != null) {
            for (int i = 0; i < invalid.length; i++) {
                System.out.println("    " + invalid[i]);
            }
        }
    }

    public void messagePartiallyDelivered(TransportEvent e) {

        // SMTPTransport doesn't partially deliver msgs
    }

    public boolean sendMessage(String tittle, String body, String acct, String type, String fromemail, String toemail) {
        String     username = "ibanking";
        String     password = "Password10@";
        Properties props    = new Properties();

        props.put("mail.smtp.host", "172.15.10.11");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");

        // props.setProperty("mail.smtp.quitwait", "false");
        boolean result  = false;
        Session session = null;

        if ((username != null) && (password != null)) {
            props.put("mail.smtp.auth", "true");
            session = Session.getInstance(props, new MyPasswordAuthenticator(username, password));
        } else {
            session = Session.getDefaultInstance(props, null);
        }

        Message message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(fromemail));

            InternetAddress[] address = InternetAddress.parse(toemail, false);

            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(tittle);
            message.setContent(body, type);

            // message.setSentDate());
            Transport trans = session.getTransport(address[0]);

            trans.addConnectionListener(this);
            trans.addTransportListener(this);
            trans.connect();
            trans.sendMessage(message, address);

            // Transport.send(message);
            result = true;
        } catch (AddressException e) {
            result = false;
            e.printStackTrace();
        } catch (MessagingException e) {
            result = false;
            e.printStackTrace();
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            return result;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
