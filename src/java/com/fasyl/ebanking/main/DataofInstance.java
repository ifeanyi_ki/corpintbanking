
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.logic.LoadBasicParam;
import com.fasyl.ebanking.util.LoadData;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Stack;

/**
 *
 * @author Administrator
 */
public class DataofInstance implements Serializable, EbankingConfiguration {

//  --------------------------------------------------------------------------

    /**
     * This stack holds all the free instances of this class.
     */
    private transient static final Stack instanceStack = new Stack()
    ;

    // --------------------------------------------------------------------------

    public String                        contact;

    /**
     * This variable stores application specific customer id of a user sending
     * this request.
     */
    public String                 custId
    ;
    public String                 customerType;
    public LoadData.LoadDataValue datavalue;
    public String                 email;
    public String                 phone;
    public String                 errorCode;
    public String                 errormessage;
    public String                 ftPrdCode;
    public String                 userName;
    /**
     * Language id for current user request.
     */
    public String langId
    ;

    /* this stores the data from the table EBANKING_PARAMS */
    public HashMap                       paramMap;
    public LoadBasicParam.LoadParamValue paramValue;
    public String                        processStatus;

    /*
     * This variable stores the i/p address from which this request is
     * originated.
     */
    public transient String remoteAddress
    ;
    public String           request;

    /* This variable stores the hashtable generated for the request parsed */
    public Hashtable requestHashTable;
    public String    requestId;
    public String    respStatus;
    public String    response;

    /**
     * This variable stores the current HTTP request string.
     */
    public HashMap result
    ;
    public String  retryCount;
    public String  returnCode;

    /* this store the the output jsp number */
    public String screenId;

    /**
     * Session Id for user session sending this request.
     */
    public String sessionId
    ;
    public String taskId;
    public String trxAmount;
    public String txnDesc;
    public String txnId
    ;

    /**
     * Current user's login id.
     */
    public String userId
    ;

    // --------------------------------------------------------------------------

    /**
     * This constructor creates a new instance of this class. It simply initializes
     * all the local variables and creates new instances of required (reused)
     * variables.
     */
    private DataofInstance() {
        initRequest();

        // any maintainace code can also be written here
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns an instance of this class. The method does following
     *
     * <p><ul>
     * <li>Check if the stack holding instances of InstanceData is empty. If so
     *       create a new instance and return it back to the caller.</li>
     * <li>If the stack is not empty, pop an instance from the stack, initialize
     *       the poped instance and return it back to the caller.</li>
     * </ul></p>
     */
    static DataofInstance getInstance() {
        synchronized (instanceStack) {
            if (instanceStack.isEmpty()) {
                if (bDebugOn) {
                    System.out.println("****** Instance stack is empty ++++++++++");
                }

                return new DataofInstance();
            }

            if (bDebugOn) {
                System.out.println("****** Instance stack is not empty ++++++++++");
            }

            return ((DataofInstance) instanceStack.pop()).initRequest();
        }
    }

    // --------------------------------------------------------------------------

    /**
     * This method puts a given instance back in the pool.
     *
     * @param  p_instancedata  An instance of InstanceData to be put back in
     *                                               the pool.
     */
    static void putBack(DataofInstance p_instancedata) {
        synchronized (instanceStack) {
            instanceStack.push(p_instancedata);
        }
    }

    // --------------------------------------------------------------------------

    /**
     */
    void setResult(HashMap p_result,    // this set the result of a given request
                   DataofInstance p_instancedata) {
        p_instancedata.result = p_result;
    }

    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------

    /**
     * This method initializes this instance of InstanceData. Note that only those
     * instance variables, that are not reused are initialized to null.
     */
    private DataofInstance initRequest() {
        result        = null;
        txnId         = null;
        userId        = null;
        langId        = null;
        sessionId     = null;
        remoteAddress = null;
        custId        = null;
//sms integration phone field added
        // requestHashTable          =new Hashtable();
        screenId              = null;
        taskId                = null;
        this.datavalue        = null;
        this.processStatus    = null;
        this.email            = null;
        this.phone            = null;
        this.paramMap         = null;
        this.trxAmount        = null;
        this.ftPrdCode        = null;
        this.request          = null;
        this.requestHashTable = null;
        this.requestId        = null;
        this.respStatus       = null;
        this.response         = null;
        this.retryCount       = null;
        this.returnCode       = null;
        this.errormessage     = null;
        this.customerType     = null;
        contact               = null;
        this.userName         =null;
        return this;
    }

    // --------------------------------------------------------------------------
    public void setResponse(String p_identity, Object p_response) {}
}


//~ Formatted by Jindent --- http://www.jindent.com
