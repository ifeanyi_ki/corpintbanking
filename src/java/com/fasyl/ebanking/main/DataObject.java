/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.main;


import com.fasyl.vo.LoaderVo;
import com.fasyl.vo.ftVo;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;
//import java.sql.*;
/**
 *
 * @author Admin
 */
public class DataObject {
    
    private Connection conn;
     private static DataSource datasource;
    public DataObject(){
        try {
            //oracle
            datasource = (DataSource) new InitialContext().lookup("jdbc/OraclePool");
            if (datasource == null) {
                //throw new SQLException("****  datasource is null ****");
                System.out.println("****datasource is null****");
            }else{
                conn = datasource.getConnection();
            }
      //    HSQLDB
//         Class.forName("org.hsqldb.jdbcDriver");
//         String url =  "jdbc:hsqldb:C:/CorporateBankingClientSide/CorporateBankingClientSide/Database/db/database";  
//         conn = DriverManager.getConnection(url, "SA", "");
            
            
//        MSSQL  
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            String url = "jdbc:sqlserver://localhost:1433;database=testdb;user=sa;password=password1";
//            this.conn = DriverManager.getConnection(url);
        } catch (Exception ex) {
            Logger.getLogger(DataObject.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public String executeStatement( String query) {
        Statement st = null;
        try {
            
            st = conn.createStatement();
            st.execute(query);
            return "success";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "failure";
        } finally {
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
    }
    
    public int getNumber( String query) {
        Statement st = null;
        int i = 0;
        ResultSet rs = null;
        System.out.println(query);
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) { i = rs.getInt(1);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
             DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
           
        }
        return i;
    }


    
        
    
    
    
    public String getValue( String query) {
        Statement st = null;
        String value = null;
        ResultSet rs = null;
        System.out.println("this is the connection" + conn);
        System.out.println(query);
        
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) { value = rs.getString(1);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
             DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return value;
    }
    
    public List<String> getList( String query) {
        Statement st = null;
        ResultSet rs = null;
        List<String> list = new ArrayList<String>();
        System.out.println(query);
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) {
               String docType = rs.getString(1);
               System.out.println(docType);
               list.add(docType);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return list;
    }
    
    public List<LoaderVo> getLoaderList( String id) {
        Statement st = null;
        String query = "select document_type, document_id from fas_int_doc_detl where ref_id_detl = '" + id + "'";
        List<LoaderVo> list = new ArrayList<LoaderVo>();
        LoaderVo lv = null;
        ResultSet rs = null;
        System.out.println(query);
        try {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                lv = new LoaderVo();
                String type = rs.getString(1);
                String doc_id = rs.getString(2);
                lv.setType(type);
                lv.setDoc_id(doc_id);
                list.add(lv);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
              DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return list;
    }
    
    
    public List<LoaderVo> getImageUpdateList( String id) {
        Statement st = null;
        String query = "select document_type, document_id from fas_int_doc_detl where ref_id_detl = '" + id + "' and approved_status = 'U'";
        List<LoaderVo> list = new ArrayList<LoaderVo>();
        LoaderVo lv = null;
        ResultSet rs = null;
        System.out.println(query);
        try {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                lv = new LoaderVo();
                String type = rs.getString(1);
                String doc_id = rs.getString(2);
                lv.setType(type);
                lv.setDoc_id(doc_id);
                list.add(lv);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
             DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return list;
    }
    
    public void saveImage( File file, String docType, String ref_id, int i){
        PreparedStatement psmnt = null;
        FileInputStream fis;
        
        String doc = null;
        switch (i) {
            case 0:
                doc = "document";
                break;
            case 1:
                doc = "document_update";
                break;
        }
        
        try {
            psmnt = conn.prepareStatement("update fas_int_doc_detl set "+ doc +" = ? where document_type = ? and ref_id_detl = ?");
            fis = new FileInputStream(file);
            psmnt.setBinaryStream(1, (InputStream) fis, (int) (file.length()));
            psmnt.setString(2, docType);
            psmnt.setString(3, ref_id);

            int s = psmnt.executeUpdate();
            if (s > 0) {
                System.out.println("Uploaded successfully !");
            } else {
                System.out.println("unsucessfull to upload image.");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
                DbUtil.closeStatement(psmnt);
            DbUtil.closeConnection(conn);
        }
    }
    
   
    
    public List<String[]> getList3( String query) {
        Statement st = null;
        List<String[]> list = new ArrayList<String[]>();
        String[] l = null;
        ResultSet rs = null;
        System.out.println(query);
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) {
               l = new String[2];
               l[0] = rs.getString(1);
               l[1] = rs.getString(2);
               list.add(l);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
              DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
         
        }
        return list;
    }
    
     public List<String[]> getList4( String query) {
        Statement st = null;
        List<String[]> list = new ArrayList<String[]>();
        String[] l = null;
        ResultSet rs = null;
        System.out.println(query);
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) {
               l = new String[3];
               l[0] = rs.getString(1);
               l[1] = rs.getString(2);
               l[2] = rs.getString(3);
               list.add(l);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
               DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return list;
    }
     
        public List<String[]> getList5( String query) {
        Statement st = null;
        List<String[]> list = new ArrayList<String[]>();
        String[] l = null;
        ResultSet rs = null;
        System.out.println(query);
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) {
               l = new String[7];
               l[0] = rs.getString(1);
               l[1] = rs.getString(2);
               l[2] = rs.getString(3);
               l[3] = rs.getString(4);
               l[4] = rs.getString(5);
               l[5] = rs.getString(6);
               l[6] = rs.getString(7);
               list.add(l);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
               DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return list;
    }
 public boolean isAvailable( String query) {
        Statement st = null;
        boolean result = false;
        ResultSet rs = null;
        System.out.println(query);
        try {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            result = rs.next();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
             DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return result;
    }   
 
 /** Added By Bayo **/
 
 public java.sql.Date chkDate( String query) {
        Statement st = null;
        java.sql.Date result  = null ;
        ResultSet rs = null;
        
         // java.sql.Date  result = new java.sql.Date(new java.util.Date().getTime());
      //  result ;
               System.out.println(query);
        try {
            st = conn.createStatement();
            rs = st.executeQuery(query);
         rs.next();
         result = rs.getDate(1);
             
          System.out.println("This the date returned from chk date "+ result);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
              DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return result;
    }   
 
 /**  Added by Bayo **/
 
 
  public int getId2( String query) {
        Statement st = null;
        int y = 0;
        System.out.println(query);
        try {
           st = conn.createStatement();
             y =  st.executeUpdate(query);
           //ResultSet rs =  st.executeQuery(query);
          // rs.next();
           
               
          //  y = rs.getInt(1);
           
           
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return y;
    }
 


  public int getId( String query) {
        Statement st = null;
        int i = 0;
        System.out.println(query);
        ResultSet rs = null;
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) { i = rs.getInt(1);}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
        }
        return i;
    }
 
  
      public List<String[]> getLists( String query, int k) {
        System.out.println(query);
        Statement st = null;
        List<String[]> list = new ArrayList<String[]>();
        String[] l = null;
        ResultSet rs = null;
        try {
           st = conn.createStatement();
           rs =  st.executeQuery(query);
           while (rs.next()) {
               l = new String[k];
                   for (int i = 1; i <= k ; i++) {
                   l[i-1] = rs.getString(i);
               }
               list.add(l);
               }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
              DbUtil.closeResultSet(rs);
            DbUtil.closeStatement(st);
            DbUtil.closeConnection(conn);
         
        }
        return list;
    }
 
      
    public boolean persistFundTransfer(ftVo vo) {
        Date dd = new Date(System.currentTimeMillis());
         Timestamp tt = new Timestamp(dd.getTime());
         String columns = "trans_id, initiator_id, amount, init_account,  ben_account, narration, auth1, auth1_action, operator, auth2";
        String values = "?, ?, ?, ?, ?, ?, ?,  ?, ?, ?";
        PreparedStatement ps = null;
        boolean result = false;
        try {
            ps = this.conn.prepareStatement("insert into fund_transfer(" + columns + ") values(" + values + ")");
            ps.setString(1, vo.getTrans_id());
            ps.setString(2, vo.getInt_id());
            ps.setDouble(3, vo.getAmt());
            ps.setString(4, vo.getAcc());
           // ps.setTimestamp(5, tt);
            ps.setString(5, vo.getBen_acc());
            ps.setString(6, vo.getNarration());
            ps.setString(7, vo.getAuth1());
            ps.setString(8, vo.getAuth1_action());
            ps.setString(9, vo.getOperator()); 
            ps.setString(10, vo.getAuth2());
            ps.execute();
            result = true;
          } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            DbUtil.closeStatement(ps);
            DbUtil.closeConnection(conn);
         
        }
        return result;
    }     
  
}
