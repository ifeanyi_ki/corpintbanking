
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.util.NoSuchElementException;
import java.util.Vector;

/**
 *
 * @author Administrator
 */
public class StringTokeNizers {

    /**
     *    This variable stores 0 based index of the next token given by a get ()
     *    method.
     */
    private int indexOfLastToken
    ;

    /**
     * This variable stores number tokens found in the given string.
     */
    private int nbrTokens
    ;

    /**
     * This variables stores all the tokens.
     */
    private String tokens[]
    ;

    // --------------------------------------------------------------------------

    /**
     * This constructor creates a new JFStringTokenizer that does not consider
     * delimiter as token.
     *
     * @param  p_src_string    Source string to be tokenized.
     * @param  p_delim_string  String delimiter.
     */
    public StringTokeNizers(String p_src_string, String p_delim_string) {
        this(p_src_string, p_delim_string, false);
    }

    // --------------------------------------------------------------------------

    /**
     * This constructor creates a new StringTokenizer.
     *
     * @param  p_src_string    Source string to be tokenized.
     * @param  p_delim_string  String delimiter.
     * @param  p_delim_token   true, if delimiter should be returned as token,
     *                                               false otherwise.
     */
    public StringTokeNizers(String p_src_string, String p_delim_string, boolean p_delim_token) {
        setData(p_src_string, p_delim_string, p_delim_token);
    }

    // --------------------------------------------------------------------------

    /**
     * This method can be called any time to reset the string tokenizer. It clears
     * existing tokens and builds new tokens using given input. The method does
     * following:
     * <ul>
     * <li>Validate given arguments.
     * <li>Start scanning given string from first character.
     * <li>Look for a delimiter. If none found then append rest of the string
     *       to tokens and break out of the loop.
     * <li>If a delimiter is found and is is the same position as the start of the
     *       search, then append empty token, else extract a token from the source
     *       string and append it to tokens.
     * <li>If a delimiter is considered as token, then append the delimiter to
     *       tokens.
     * <li>Adjust the start position to the end of the delimiter position and
     *       continue scanning.
     * </ul>
     *
     * @param  p_src_string    Source string to be tokenized.
     * @param  p_delim_string  String delimiter.
     * @param  p_delim_token   true, if delimiter should be returned as token,
     *                                               false otherwise.
     */
    public void setData(String p_src_string, String p_delim_string, boolean p_delim_token) {
        Vector l_token_vector = new Vector();
        int    l_start_pos    = 0, l_new_pos, l_src_length, l_delim_length;
        String l_token;

        l_src_length = p_src_string.length();

        if ((p_delim_string == null) || ((l_delim_length = p_delim_string.length()) == 0)) {
            throw new IllegalArgumentException("Delimiter is null or empty");
        }

        indexOfLastToken = 0;

        while (l_start_pos < l_src_length) {
            l_new_pos = p_src_string.indexOf(p_delim_string, l_start_pos);

            if (l_new_pos == -1) {
                l_token = p_src_string.substring(l_start_pos);
                l_token_vector.add(l_token);

                break;
            }

            if (l_start_pos == l_new_pos) {
                l_token = "";
            } else {
                l_token = p_src_string.substring(l_start_pos, l_new_pos);
            }

            l_token_vector.add(l_token);

            if (p_delim_token) {
                l_token_vector.add(p_delim_string);
            }

            l_start_pos = l_new_pos + l_delim_length;
        }

        tokens = new String[nbrTokens = l_token_vector.size()];
        l_token_vector.copyInto(tokens);
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns the number of tokens in the given string.
     */
    public int countTokens() {
        return nbrTokens;
    }

    // --------------------------------------------------------------------------

    /**
     * This method resets the position of the last extracted token so that all the
     * tokens may be re-extracted from begning.
     */
    public void reset() {
        indexOfLastToken = 0;
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns if thre are any more tokens left to be extracted; false
     * otherwise.
     */
    public boolean hasMoreTokens() {
        return indexOfLastToken < nbrTokens;
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns next token.
     *
     * @throws NoSuchElementException  If an attempt is made to extract tokens
     *                                                               after extracting all the tokens.
     */
    public String nextToken() throws NoSuchElementException {
        if (hasMoreTokens()) {
            return tokens[indexOfLastToken++];
        }

        throw new NoSuchElementException("String has only " + nbrTokens + "tokens");
    }

    // --------------------------------------------------------------------------

    /**
     * This token return a token at a given (0 based) index.
     *
     * @throws IllegalArgumentException        If given index is -ve.
     * @throws NoSuchElementException          If given index is more than number of
     *                                                                       tokens.
     */
    public String getToken(int p_token_idx) throws NoSuchElementException {
        if (p_token_idx < 0) {
            throw new IllegalArgumentException("Token index is -ve");
        }

        if (p_token_idx < nbrTokens) {
            return tokens[p_token_idx];
        }

        throw new NoSuchElementException("String has only " + nbrTokens + "tokens");
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns 0 based position of the next token in the source string.
     */
    public int nextTokenPosition() {
        return indexOfLastToken;
    }

    // --------------------------------------------------------------------------

    /**
     * This method returns all the tokens extracted from the given string.
     */
    public String[] getAllTokens() {
        return tokens;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
