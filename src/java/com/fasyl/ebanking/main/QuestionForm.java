/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Administrator
 */
public class QuestionForm extends org.apache.struts.action.ActionForm {
    
    private String answer;
    private String userId;
    private String correctAnswer;
    private String macAddress;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
    
    

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    
 
    public QuestionForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getAnswer() == null || getAnswer().length() < 1) {
            errors.add("answer", new ActionMessage("errors.answer.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        if(!getAnswer().equalsIgnoreCase(getCorrectAnswer())){
            errors.add("wronganswer", new ActionMessage("errors.answer.wrong"));
        }
        return errors;
    }
}
