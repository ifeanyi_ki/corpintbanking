
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.logic.Processes;
import com.fasyl.ebanking.util.AuditLogHandler;
import com.fasyl.ebanking.util.CacheClass;
import com.fasyl.ebanking.util.LoadData;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;

/**
 *
 * @author Administrator
 */
public class MainControllernew extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, DataofInstance instData)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter      out                 = response.getWriter();
        DataofInstance   inst_data           = null;
        Object[]         l_obj               = null;
        HttpSession      session             = request.getSession(false);
        ServletException l_servlet_exception = null;

        try {
            if (instData == null) {
                inst_data = DataofInstance.getInstance();
            } else {
                inst_data = instData;
            }

            inst_data.remoteAddress = request.getRemoteAddr();
            System.out.println(inst_data.remoteAddress);
            analyseRequest(request, inst_data);
            AuditLogHandler.log(inst_data);
            initServletData();

            try {

                // System.out.println(inst_data.requestHashTable.get("screenId").toString()+" testing");
                inst_data.screenId = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                        "screenId", 0, false, "1");
                inst_data.taskId = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "taskId",
                        0, true, "0");
                inst_data.txnId = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "txnId", 0,
                        true, "0");
                inst_data.custId = ((User) session.getAttribute("user")).getCustno();
                inst_data.email  = ((User) session.getAttribute("user")).getCustno();
                System.out.println(inst_data.screenId + inst_data.taskId + inst_data.txnId);

                // inst_data.datavalue=LoadData.getDataValue((String)inst_data.requestHashTable.get("screenId"),(String) inst_data.requestHashTable.get("taskId"),(String) inst_data.requestHashTable.get("txnId"));
                inst_data.datavalue = LoadData.getDataValue(inst_data.screenId.trim(),
                        inst_data.taskId.trim() /* ,inst_data.txnId.trim() */);

                // System.out.println(inst_data.datavalue.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            delegateReqService(inst_data);

            if (inst_data.result != null) {
                BuildResponse(request, response, inst_data);
            }

            DataofInstance.putBack(inst_data);

            if (l_servlet_exception != null) {
                throw l_servlet_exception;
            }
        } finally {
            out.close();
        }
    }

    protected void analyseRequest(HttpServletRequest request, DataofInstance instanceData) {
        byte[]              bytes     = null;
        String              result    = null;
        Hashtable           resultMap = new Hashtable();
        ReadRequestAsStream reqStream = new ReadRequestAsStream();

        try {
            System.out.println(request.getInputStream().available());
            bytes = reqStream.readFromStream(request.getInputStream());    // This get therequest from the stream
            System.out.println(request.getParameter("screenId") + " ScreenId");
            System.out.println(request.getQueryString());

            String test = request.getQueryString();

            instanceData.requestHashTable = HttpUtils.parseQueryString(test);
            System.out.println(instanceData.requestHashTable + "");

            /*
             *  if (bytes == null) {
             *    System.out.println("bytes is null");
             *    //put the exceptio code here
             * } else {
             *    System.out.println("bytes is not null"+bytes.length);
             *    result = reqStream.decode(
             *            new String(bytes, "UTF-8").trim(), "UTF-8");
             *    System.out.println(result+" This is the result");
             *    instanceData.requestHashTable = reqStream.createRequestparamHashTable(new String(bytes, "UTF-8").trim());
             *    System.out.println(result+" another result");
             *    System.out.println(resultMap+ " Resultmap");
             * }
             */
        } catch (Exception ex) {}

        // return resultMap;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, null);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }    // </editor-fold>

    public static void initServletData() {
        LoadData.loadData();
    }

    public void sendRequest(HttpServletRequest req, HttpServletResponse res, String scn) {
        RequestDispatcher rd = req.getRequestDispatcher(scn);

        try {
            rd.forward(req, res);
        } catch (ServletException ex) {
            ex.printStackTrace();
        } catch (IOException ioex) {
            ioex.printStackTrace();
        }
    }

    private void BuildResponse(HttpServletRequest request, HttpServletResponse response, DataofInstance instData) {
        HttpSession session = request.getSession(false);
        String      scn     = null;
        HashMap     result  = instData.result;

        scn = instData.taskId + ".jsp";

        // response.encodeRedirectURL(scn);
        scn = "./screens/" + scn + "?jsessionid=" + session.getId();

        String results = (String) result.get("returnResult");

        System.out.println(results);
        request.setAttribute("results", results);
        response.encodeRedirectURL(scn);
        sendRequest(request, response, scn);
    }

    private void delegateReqService(DataofInstance instData) {
        Processes l_processor  = null;
        Class     l_comp_class = null;
        String    l_class_name = null;

        l_class_name = instData.datavalue.getClassname().trim();
        System.out.println(l_class_name);

        try {
            if (l_class_name.equals("com.fasyl.noclass")) {}
            else if (l_class_name != null) {

                /*
                 * if (((Processes)
                 * CacheClass.getObject (l_class_name)) == null) {
                 */
                l_comp_class = Class.forName(l_class_name);
                l_processor  = (Processes) l_comp_class.newInstance();

                // }
                System.out.println("about to call process ");
                instData.result = l_processor.processRequest(instData);
            } else {
                instData.result = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (l_processor != null) {

                    // CacheClass.putObject (l_class_name, l_processor);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
