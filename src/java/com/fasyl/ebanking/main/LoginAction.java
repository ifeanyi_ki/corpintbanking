
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.db.ConnectionClass;
import com.fasyl.ebanking.db.DataBase;
import com.fasyl.ebanking.logic.LoadBasicParam;
import com.fasyl.ebanking.logic.MnageBulletin;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.LoadData;
import com.fasyl.ebanking.util.Utility;
import com.fasyl.ebanking.util.XMLUtilty;

import oracle.jdbc.OracleTypes;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//~--- JDK imports ------------------------------------------------------------
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Nisar
 */
public class LoginAction extends org.apache.struts.action.Action implements EbankingConfiguration {

    private static final Logger logger = Logger.getLogger(LoginAction.class.getName());

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    static {
        LoadData.loadData();
        LoadBasicParam.loadData();
    }
    private String sessionid = null;
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    User user = null;
    ArrayList<String> ips = LoadData.loadAdminIps();
    // HashMap
    // String bDebugOn = true;
    private String usertype = null;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) {
        HttpSession session = null;

        Connection conn = DataBase.getConnection();
        String macAddress = null;
        Utility utility = new Utility();

        try {
            LoginForm _form = (LoginForm) form;

            _form.validate(mapping, request);

            if (bDebugOn) {
                System.out.println(_form.getUserid());
            }

            if (bDebugOn) {
                System.out.println(_form.getPassword());
            }

            if (bDebugOn) {
                System.out.println(Encryption.encrypt(_form.getPassword()));
            }
            //retrieve the mac address of the user and save in sm_user_access 13/11/2015
            macAddress = _form.getMacAddress();
            String l_userId = _form.getUserid().trim();
            System.out.println("machine id of the user in Login Action: " + macAddress);
            
//            if (!"".equalsIgnoreCase(_form.getUserid()) && macAddress != null && !"".equalsIgnoreCase(macAddress)
//                    && (_form.getUserid() != null)) {
//                //save the mac address of the user
//                utility.saveUserMacAddress(macAddress, l_userId);
//            }

            // if (conn!=null){

            if(conn.isClosed() || conn  == null){
                conn = DataBase.getConnection();
            }
            
            CallableStatement statement = conn.prepareCall("{call Process_Login(?,?,?)}");

            statement.registerOutParameter(3, OracleTypes.VARCHAR);
            statement.setString(1, _form.getUserid());
            statement.setString(2, Encryption.encrypt(_form.getPassword()));

            String result = null;

            statement.executeQuery();
            result = XMLUtilty.getOneEntry(statement.getString(3), "USERNOTFOUND");

            if (bDebugOn) {
                System.out.println(result + " This is the result");
            }

            if (result != null) {
                _form.reset();
                request.setAttribute("msg_text", result);
                request.setAttribute("msg_model_type", "message");
                request.setAttribute("msg_ok_target", "ReLogin.jsp");

                return mapping.findForward("complexerror");
            }

            Document doc = XMLUtilty.getStringAsdocument(statement.getString(3));

            if (bDebugOn) {
                System.out.println(statement.getString(3));
            }

            Element docEle = doc.getDocumentElement();
            NodeList nl = docEle.getElementsByTagName("ROW");

            if ((nl != null) && (nl.getLength() > 0)) {
                for (int i = 0; i < nl.getLength(); i++) {
                    Element el = (Element) nl.item(i);

                    user = getUser(el);
                }
            }

            usertype = user.getUserType();

            System.out.println("user type:" + usertype);
            System.out.println("remote host :" + request.getRemoteHost());
            System.out.println("remote address :" + request.getRemoteAddr());

            if ((ManageLogin.validateUser4login(user.getUserid())) && !(usertype.equals("CORP"))) {    // need to filter corporate customer
                ManageLogin.InnerLogin detail = ManageLogin.getLoginDetail(user.getUserid());

                ManageLogin.invalidateUser(user.getUserid() + " " + detail.getSessionid());
            }

            String idletime = (String) LoadBasicParam.getDataValuenew("IDLETIME");

            if (bDebugOn) {
                System.out.println(idletime);
            }

            session = request.getSession(true);

            if ((idletime == null) || idletime.equals("")) {
                idletime = "300";
            }

            session.setMaxInactiveInterval(Integer.parseInt(idletime));
            sessionid = session.getId();

            /* SMS INTEGRATION PATCH PHONE FIELD ADDED HERE*/
            ManageLogin.InnerLogin login = (new ManageLogin()).new InnerLogin();

            if (!(usertype.equals("CORP"))) {
                synchronized (this) {
                    login.setEmail(user.getEmail());
                    login.setPhone(user.getPhone());
                    login.setUserid(user.getUserid());
                    login.setSessionid(sessionid);
                    ManageLogin.addUser(login);
                }
            }    // corporate user are not added because they can have multiple session

            System.out.println("user role id:" + user.role_id);
            //added by K.I to restrict admin from login in outside permitted domains s requested by TB
            //EXT_USER USER_ADMIN SYS_ADMIN
            if ("USER_ADMIN".equalsIgnoreCase(user.role_id.trim()) || "SYS_ADMIN".equalsIgnoreCase(user.role_id.trim())) {
                System.out.println("checking for loaded ips from DB:" + ips);
                if (ips == null) {
                    System.out.println("ips is null, retrieving ips again: " + ips);
                    ips = LoadData.loadAdminIps();
                } else {
                    System.out.println("ips from db is not null:" + ips);
                }

                if (!ips.contains(request.getRemoteHost())) {
                    System.out.println("*** kick the user out ***");
                    _form.reset();
                    request.setAttribute("msg_text", "Admin are not allowed on this domain");
                    request.setAttribute("msg_model_type", "message");
                    request.setAttribute("msg_ok_target", "ReLogin.jsp");
                    return mapping.findForward("complexerror");
                }

            }

            /*
             * if (!(Utility.isReqValid(session.getLastAccessedTime()))){
             * System.out.println("sessiontimeout in action");
             *
             * }
             */
            if (RELOGIN.equals("N") && !(usertype.equals("CORP"))) {    // need to filter corporate customer for this

                // String isLogin = "Y" ;
                if (bDebugOn) {
                    System.out.println(user.getLoginstate() + "This is the login state in relogin");
                }

                if (user.loginstate.equals("Y")) {
                    if (bDebugOn) {
                        System.out.println("sending message for a user already logged on");
                    }

                    _form.reset();
                    request.setAttribute("msg_text", "The user has already logged somewhere");
                    request.setAttribute("msg_model_type", "message");
                    request.setAttribute("msg_ok_target", "ReLogin.jsp");

                    return mapping.findForward("complexerror");
                }
            }

            session.setAttribute("user", user);

            // this.
            if (user.getUserid() == null) {
                if (bDebugOn) {
                    System.out.println("user == null");
                }

                java.util.ArrayList list = XMLUtilty.getOne(statement.getString(3), "USERNOTFOUND");

                session.setAttribute("failure", "USER NOT FOUND");
                request.setAttribute("failure", "USER NOT FOUND");

                // _form.noUser(mapping, request);
                _form.reset();

                return mapping.findForward("failure");
            } else {
                if (!(user.getUserstatus().equals("A"))) {
                    if ((user.getUserstatus().equals("U"))) {
                        _form.reset();
                        request.setAttribute(
                                "msg_text",
                                " <b>" + user.getUserid()
                                + "</b> , You are a first time user.<br> Kindly change your password by clicking the <b>OK</b> button:");
                        request.setAttribute("msg_model_type", "message");
                        request.setAttribute("actual_msg", "Welcome");
                        request.setAttribute("msg_ok_target", "0001.jsp");

                        return mapping.findForward("complexerror");
                    } else {
                        if (bDebugOn) {
                            System.out.println("The account is not active");
                        }

                        _form.reset();
                        request.setAttribute("msg_text", "The account is not active");
                        request.setAttribute("msg_model_type", "message");
                        request.setAttribute("msg_ok_target", "ReLogin.jsp");

                        return mapping.findForward("complexerror");
                    }
                }

                System.out.println(user.getRemoteaddress() + "tope " + request.getRemoteAddr());

                if (USE_SECRET_PASSWD) {
                    if ((user.getRemoteaddress() != null)) {
                        if (!(user.getRemoteaddress().equals(request.getRemoteAddr()))) {
                            if ((user.getSecret_password().equals("N"))) {
                                int res = -1;
                                String secret_passwd = null;

                                if (Utility.pingServer()) {
                                    secret_passwd = Utility.genaratePassword();
                                    res = this.updateSecretPasswd(secret_passwd, "Y", null);
                                }

                                if (res != 0) {
                                    // write exception catching code
                                }

                                // now send a mail to the client with the secret password;
                                if (user.getEmail() == null) {
                                    // catch email not avaialble exception;
                                }

                                String type = "text/html";
                                String body = "Kindly supply the following information for login  to access your Internet Banking account: "
                                        + secret_passwd;
                                String header = "Secret Password";
                                String toemail = "ibanking@trustbondmortgagebankplc.com";
                                String fromemail = "ibanking@trustbondmortgagebankplc.com";    // a table must be created to provide header,body,from
                                sendmail send = new sendmail();

                                // send.sendMessage(header, body,null, type, fromemail, toemail);
                                boolean results = send.sendMessage(header, body, null, type, fromemail, toemail);

                                if (results) {

                                    // specified the page
                                    // request.setAttribute("secpasswd","You are not connecting from a usual location therefore a new password has been sent to your mail box ");
                                    _form.reset();
                                    request.setAttribute(
                                            "msg_text",
                                            "You are not connecting from a usual location, a new password has been sent to your mail box");
                                    request.setAttribute("msg_model_type", "message");
                                    request.setAttribute("msg_ok_target", "ReLogin.jsp");

                                    return mapping.findForward("complexerror");
                                } else {

                                    // request.setAttribute("secpasswd","You are not connecting from a usual location and mail server is down. ");
                                    // throws exception that it can not send mail through the smtp server
                                    _form.reset();
                                    request.setAttribute(
                                            "msg_text",
                                            "You are not connecting from a usual location and mail server is down.");
                                    request.setAttribute("msg_model_type", "message");
                                    request.setAttribute("msg_ok_target", "ReLogin.jsp");

                                    return mapping.findForward("complexerror");
                                }
                            } else {
                                this.updateSecretPasswd(null, "N", request.getRemoteAddr());
                            }
                        }
                    } else if (user.getRemoteaddress() == null) {
                        this.updateSecretPasswd(null, null, request.getRemoteAddr());
                    }

                    // /}
                    // end of secret password
                }

                if (bDebugOn) {
                    System.out.println("user != null");
                }

                request.setAttribute("user", user);

                // conn.close();
                _form.reset();
                updateUserStatus();
                upadteLogTable();
                insertCustDetails(user.getCustno(), session.getId());

                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
                final String type = "text/html";
                final String body = "Dear Customer,"
                        + "<br /><br />" + "Please be informed that your internet banking account"
                        + " (ID: " + user.getUserid() + ") was accessed on "
                        + ft.format(dNow) + "." + "\n" + "<br /><br />"
                        + "If you did not login to your account at the time stated above,\n"
                        + "please call\n"
                        + "\n"
                        + "\n"
                        + "TrustBond on 01-2771127 or send an email to ibanking@trustbondmortgagebankplc.com immediately.\n"
                        + "\n" + "<br /><br />"
                        + "Thank you for banking with us.\n"
                        + "\n" + "<br /><br />"
                        + "Regards," + "<br />"
                        + "TrustBond Mortgage Bank Internet Banking";

                final String header = "TRUSTBOND LOGIN ALERT";
                final String toemail = user.getEmail();
                final String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");   // a table must be created to provide header,body,from
                final sendmail send = new sendmail();

                logger.info("fromemail" + fromemail);
                logger.info("toemail" + toemail);

                logger.info(" send email in separate thread . . .");
                ExecutorService execService = TaskManager.getTaskManager();

                execService.submit(new Runnable() {
                    public void run() {
                        // Send email.
                        boolean results = send.sendMessage(header, body, null, type, fromemail, toemail);

                        logger.info(" send email result: " + results);
                    }
                });

                //  boolean results = send.sendMessage(header, body, null, type, fromemail, toemail);
//*******************************
                //machine id has changed, prompt security question
//                if (macAddress != null) {
//
//                    if (!macAddress.equalsIgnoreCase(utility.getUserMacAddress(l_userId))) {
//                        //user has logged in on a different device, forward to secrete question page
//                        _form.reset();
//                        System.out.println("user id to be set on session: " + l_userId);
//                        System.out.println("machine id to be set on session: " + macAddress);
//                        utility.updateUserMacAddress(macAddress, l_userId);
//                       
//                        session.setAttribute("l_user_id", l_userId);
//                        session.setAttribute("macAddress", macAddress);
//
//                        return mapping.findForward("secretequestion");
//                    }else{
//                        System.out.println("user mac address has not changed no security question will be asked");
//                    }
//                }

                return mapping.findForward(SUCCESS);
            }

            // _form.reset(mapping, request);/*
//                return mapping.findForward(SUCCESS);
        //}
        } catch (Exception ex) {

            // Logger.getLogger(LoginAction.class.getName()).log(Level.SEVERE, null, ex);
            // session.setAttribute("failure", "USER NOT FOUND");
            ex.printStackTrace();

            return mapping.findForward("failure");
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception e) {
            }
        }
    }

    private int updateUserStatus() {
        if (bDebugOn) {
            System.out.println("inside updateStatus" + user.getUserid().toUpperCase());
        }

        int result = -1;
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        String updateQry = "UPDATE sm_user_access set flg_login_stat='Y', dat_last_signon=sysdate,login_failiure_count='0' where COD_USR_ID='"
                + user.getUserid().toUpperCase() + "'";
        PreparedStatement stmt = null;

        try {
            stmt = connect.prepareStatement(updateQry);
            result = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }

                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
        }

        return result;
    }

    private int updateSecretPasswd(String secPasswd, String secretNeeded, String remoteAddress) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        String remoteaddres = user.getRemoteaddress();
        ;
        String updateQry = null;

        if ((secPasswd != null) && (remoteaddres != null)) {
            updateQry = "update SM_USER_ACCESS set cod_usr_pwd='" + Encryption.encrypt(secPasswd)
                    + "',sec_pass='Y'where cod_usr_id='" + user.getUserid() + "'";
        } else if ((remoteaddres != null) && (secPasswd == null)) {
            updateQry = "update SM_USER_ACCESS set sec_pass='N',default_location='" + remoteAddress
                    + "' where cod_usr_id='" + user.getUserid() + "'";
        } else {
            updateQry = "update SM_USER_ACCESS set default_location='" + remoteAddress + "' where cod_usr_id='"
                    + user.getUserid() + "'";
        }

        PreparedStatement stmt = null;
        int l_rs = -1;

        try {
            stmt = connect.prepareStatement(updateQry);
            l_rs = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }

                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
        }

        return l_rs;
    }

    private void insertCustDetails(String custno, String sessionid) {
        java.sql.Connection connection = null;
        java.sql.CallableStatement callable = null;

        // String data = null;
        try {
            connection = com.fasyl.ebanking.db.DataBase.getConnection();

            // System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
            callable = connection.prepareCall("{call PUT_CUST_ACCT_DET(?,?)}");
            callable.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
            callable.setString(1, custno);
            callable.setString(2, sessionid);

            // /System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
            callable.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                callable.close();
            } catch (Exception e) {
            }
        }
    }

    public String getTimeout() {
        return null;
    }
//ADDED PHONE FIELD FOR SMS INTEGRATION BY REBECCA

    private User getUser(Element elem) {
        user = new User();
        user.setUserid(getTextValue(elem, "COD_USR_ID"));
        user.setUserno(getTextValue(elem, "COD_USR_NO"));
        user.setUsername(getTextValue(elem, "COD_USR_NAME"));
        user.setRole_id(getTextValue(elem, "COD_ROLE_ID"));
        user.setBranch(getTextValue(elem, "COD_USR_BRANCH"));
        user.setAffiliate(getTextValue(elem, "COD_USR_AFFILIATE"));
        user.setUserlang(getTextValue(elem, "COD_LANG"));
        user.setLoginstate(getTextValue(elem, "FLG_LOGIN_STAT"));
        user.setLastlogindate(getTextValue(elem, "DAT_LAST_SIGNON"));
        user.setCustno(getTextValue(elem, "CUST_NO"));
        user.setEmail(getTextValue(elem, "EMAIL"));
        user.setPhone(getTextValue(elem, "PHONE_NO"));
        user.setUserType(getTextValue(elem, "USERTYPE"));
        user.setRemoteaddress(getTextValue(elem, "DEFAULT_LOCATION"));
        user.setSecret_password((getTextValue(elem, "SEC_PASS") == null)
                ? ""
                : getTextValue(elem, "SEC_PASS"));
        user.setUserstatus(getTextValue(elem, "FLG_STATUS"));
        user.setContact(getTextValue(elem, "CONTACT"));

        MnageBulletin msgno = new MnageBulletin();

        user.setNoofmailmsgs(msgno.BulletinCount());

//      System.out.println(user.getRemoteaddress() + "This is the address");
//      System.out.println(user.getSecret_password() + "This is the sec passwd");
//      System.out.println(user.getLoginstate() + "This is the login state");
        return user;
    }

    public String getTextValue(Element ele, String tagName) {
        String textVal = null;
        NodeList nl = ele.getElementsByTagName(tagName);

        if ((nl != null) && (nl.getLength() > 0)) {
            Element el = (Element) nl.item(0);

            textVal = el.getFirstChild().getNodeValue();
        }

        return textVal;
    }

    int upadteLogTable() {
        System.out.println(user.getCustno() + " ---------------");

        String query = "insert into auditlog (txn_id,user_id,dattxn,remote_address,session_id,trx_seq,cust_id,customer_type,txn_desc,resp_status,process_status)"
                + "values(?,?,sysdate,?,?,?,?,?,?,?,?)";
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt = null;
        int result = -1;

        // User user = (User)event.getSession().getAttribute("user");
        try {
            pstmt = connect.prepareStatement(query);
            pstmt.setString(1, "LG");
            pstmt.setString(2, user.getUserid());
            pstmt.setString(3, user.getRemoteaddress());
            pstmt.setString(4, sessionid);
            pstmt.setString(5, Utility.getReqId());
            pstmt.setString(6, user.getCustno());
            pstmt.setString(7, user.getUserType());
            pstmt.setString(8, "Login");
            pstmt.setString(9, "0");
            pstmt.setString(10, "Y");
            result = pstmt.executeUpdate();
        } catch (Exception ex) {

            // instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
            ex.printStackTrace();
        } finally {
            try {
                if (connect != null) {
                    connect.close();
                }
            } catch (Exception ex) {
            }
        }

        return result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
