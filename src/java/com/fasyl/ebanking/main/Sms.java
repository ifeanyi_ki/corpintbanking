/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//import java.io.BufferedReader;
import com.fasyl.ebanking.logic.LoadBasicParam;
import static com.fasyl.ebanking.main.EbankingConfiguration.bDebugOn;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;

//import java.io.DataOutputStream;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
/**
 *
 * @author 
 */
//Created by Rebecca for sms notifications 
public class Sms extends HttpServlet implements EbankingConfiguration {

    private String USER_AGENT;
    
      private static final Logger logger = Logger.getLogger(Sms.class.getName());
 

// 
    public boolean sendSms(String url, String messageBody, String destinationAddress, String sourceAddress) throws IOException {

        boolean result = false;
        
        logger.info(" sms url: " + url);
          


        try {
          
            
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

           
            //con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");


            String urlParameters = "messageBody=" + messageBody
                    + "&destinationAddress=" + destinationAddress
                    + "&sourceAddress=" + sourceAddress;


            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            //print result
            
            result = true;
        } 
        catch (Exception e)
        {
            
            logger.error(" error: " + e.getMessage());
            
           result = false;
        }
        
        
        return result;

        


    }
}
