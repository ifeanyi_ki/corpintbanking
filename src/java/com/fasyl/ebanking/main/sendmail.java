
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.logic.LoadBasicParam;

//~--- JDK imports ------------------------------------------------------------

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.servlet.http.HttpServlet;

public class sendmail extends HttpServlet implements EbankingConfiguration {
    public boolean sendMessage(String tittle, String body, String acct, String type, String fromemail, String toemail) {

//      String username= "febanking";
//      String password = "password@123.";
        boolean result     = false;
        String  Mailserver = null;

 //     String username= "iba@slcb.com";
   //   String password = "int-123IB";
        try {
        
            Mailserver = (String) LoadBasicParam.getDataValuenew("MAILSERVER");

            if (bDebugOn) {
                System.out.println(Mailserver);
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        }

        Properties props = new Properties();

        props.put("mail.smtp.host", Mailserver);    // slcbsec02.slcb.com//swebmail.slcb.com
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.debug", "true");

        // props.put("mail.smtp.port", "465");
        result = false;

        Session session = null;

    //  if (username != null && password != null)
    //  {
           //   props.put("mail.smtp.auth", "true");
          //    session = Session.getInstance(props,
     // new MyPasswordAuthenticator(username, password));
     // }
     // else
    //  {
          session = Session.getDefaultInstance(props, null);

   //   }

        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(fromemail));

            InternetAddress[] address = InternetAddress.parse(toemail, false);

            message.setRecipients(Message.RecipientType.TO, address);
            message.setSubject(tittle);
            System.out.println("body: " + body);
            message.setContent(body, type);

            // message.setSentDate());
            // Transport transport = session.getTransport("smtp");
            
            // 20141209
            Transport.send(message);
            
          //  System.out.println(" not sending email ");
            
            result = true;
        } catch (AddressException e) {
            result = false;
            e.printStackTrace();
        } catch (MessagingException e) {
            result = false;
            e.printStackTrace();
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

//
//  public static  void main(String [] args){
//           String body = "Dear Customer,<br> You have been created as user for SLCB internet"
//                              + " banking and Your default credentials are as follows<br>   "
//                              + " Password is password ,Userid is " + "test" + " and your customer no is " + "test" + ". <br>"
//                              + "Thank You<br> Yours Faithfully <br> IB";
//                      String header = "New Customer";
//                       String type = "text/html";
//                      String toemail = "rolabor@slcb.biz";//rexdido@rexdido.net
//                      String fromemail = "rolabor@slcb.biz";
//                      sendmail send = new sendmail();
//                      send.sendMessage(header, body, "password", type, fromemail, toemail);
//      
//  }
}


//~ Formatted by Jindent --- http://www.jindent.com
