
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------
import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Administrator
 */
public class MySessionListeners implements HttpSessionListener, EbankingConfiguration {

    HttpServletRequest req;
    HttpServletResponse res;
    private UserAccessBo userAccessBo = new UserAccessBoImpl();

    /*
     * public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
     * forwardTo("./screens/logout.jsp", getServletContext(), req, res);
     *
     * }
     */
    public void sessionDestroyed(HttpSessionEvent event) {
        if (bDebugOn) {
            System.out.println("HttpSessionListener : This is the sessionId I am listening to :"
                    + event.getSession().getId());
        }
        System.out.println("About to reset loging state of the user: " + event.getSession().getAttribute("user_id"));
        userAccessBo.updateLoginState((String) event.getSession().getAttribute("user_id"), "N");
        String sessionId = event.getSession().getId();
        String scn = "./screens/logout.jsp";

        doPostOperation(sessionId);

        /*
            * try{
            * doPost(req,res);
            * }catch(Exception ex){
            * ex.printStackTrace();
            * }
            * return;
         */
        // event.getSession()
//            res.sendRedirect("../Login.jsp");
    }

    public void forwardTo(String sTarget, ServletContext context, ServletRequest request, ServletResponse response) {
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(sTarget);

            dispatcher.forward(request, response);

            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doPostOperation(String sessionId) {
        System.out.println(sessionId);

        java.sql.Connection connection = null;

        connection = com.fasyl.ebanking.db.DataBase.getConnection();

        // String custno=((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno();
        String query
                = "select user_id,cust_id,trx_seq,txn_id,txn_desc,auditresponse,dattxn,customer_type from auditlog where session_id='"
                + sessionId + "'order by dattxn desc";
        String delQuery = "delete from pending_response where user_id=? ";
        String insQuery
                = "insert into pending_response (trx_number,trx_id,trx_desc,user_id,response_message,added_date)values(?,?,?,?,?,sysdate)";
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        java.sql.Statement stmt = null;
        java.sql.Statement stmts = null;

        try {

            // connection.setAutoCommit(false);
            stmt = connection.createStatement();
            stmts = connection.createStatement();

            // com.fasyl.ebanking.main.User user=(com.fasyl.ebanking.main.User)session.getAttribute("user");
            rs = stmt.executeQuery(query);

            if (rs.next()) {
                System.out.println("rs.getString(1)*** " + rs.getString(1) + " " + rs.getString(2));

                String userid = rs.getString(1);
                String custid = rs.getString(2);
                String reqid = rs.getString(3);
                String txnid = rs.getString(4);
                String txndesc = rs.getString(5);
                String auditresponse = rs.getString(6);
                String dattxn = rs.getString(7);
                String custtype = rs.getString(8);

                //
                int i = stmt.executeUpdate("update sm_user_access set flg_login_stat='N' where COD_USR_ID='" + userid
                        + "'");

                // int l = stmt.executeUpdate("Delete from CUSTOMER_ACCT_DETAIL where cust_no='" + custid + "'");
                if (bDebugOn) {
                    System.out.println("rs.getString(1)*** " + custid);
                }

                int l = -1;

                if (i >= 0) {
                    System.out.println("rs.getString(2)*** " + custid);

                    if (!((custtype == null) && custtype.equals("CORP"))) {
                        l = stmt.executeUpdate("Delete from CUSTOMER_ACCT_DETAIL where cust_no='" + custid + "'");
                    } // /int l2=stmt.executeUpdate("update sm_user_access set flg_login_stat='N' where COD_USR_ID='"+rs.getString(1)+"'");
                    else {
                        l = stmt.executeUpdate("Delete from CUSTOMER_ACCT_DETAIL where cust_no='" + custid
                                + "' and sessid='" + sessionId + "'");
                    }

                    if (bDebugOn) {
                        System.out.println("rs.getString(2) after*** " + custid);
                    }

                    if (l >= 0) {

                        // l=stmt.executeUpdate("Delete from CUSTOMER_ACCT_DETAIL where cust_no=" + rs.getString(2));
                        pstmt = connection.prepareStatement(delQuery);
                        pstmt.setString(1, userid);
                        pstmt.executeUpdate();
                        pstmt = connection.prepareStatement(insQuery);
                        pstmt.setString(1, reqid);

                        if (bDebugOn) {
                            System.out.println("why1" + reqid);
                        }

                        pstmt.setString(2, txnid);

                        if (bDebugOn) {
                            System.out.println("why2" + txnid);
                        }

                        pstmt.setString(3, txndesc);

                        if (bDebugOn) {
                            System.out.println("why3" + txndesc);
                        }

                        pstmt.setString(4, userid);

                        if (bDebugOn) {
                            System.out.println("why4" + userid);
                        }

                        pstmt.setString(5, auditresponse);

                        if (bDebugOn) {
                            System.out.println("why5" + auditresponse);
                        }

                        // pstmt.setString(6, dattxn);
                        // System.out.println("why6"+dattxn);
                        int s = pstmt.executeUpdate();

                        if (s >= 0) {
                            connection.commit();
                        } else {

                            // /write  exception catching code
                            connection.rollback();
                        }
                    }
                }
            }

            /*
             * if (request.isRequestedSessionIdValid()){
             * session.invalidate();
             * }
             */
            new com.fasyl.ebanking.main.LoginForm().reset();

            //response.sendRedirect("../Login.jsp");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }

                if (pstmt != null) {
                    pstmt.close();
                }

                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
        }
    }

    public void sessionCreated(HttpSessionEvent event) {
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
