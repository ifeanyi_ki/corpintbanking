
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.MessageFormat;

/**
 *
 * @author Administrator
 */
public class SessionManager {

    /**
     * This select statement is used to find out if a requesting user has another
     * session active within ebanking.
     */
    private static final String SEL_IS_USERLOGGED_STR = " select idsession, idapp from usersession "
                                                        + " where iduser = ''{0}'' ";

    /**
     * This statement is used to get the login time from usersession table
     * for the current session (time when the session was created).
     */
    private static final String SEL_SESSION_LOGINTIME = " select iduser, datcreation from usersession "
                                                        + " where idsession = ''{0}'' ";

    /*
     *  This method checks whether the user has an existing connection
     * to the application. It returns null if the user has no conection otherwise it returns the
     * sessionid.
     */

    /*
     *  This update statement updates the user table for last successfull login
     * time, lockflag and number of failed logins.
     */
    private static final String UPD_USER_LOGIN_SUCC = " update  set" + "  nbrfailedlogins = 0"
                                                      + " ,datlastsucclogin      = ?" + " where iduser = ? ";
    private static Connection dbConnection = null;

    public String isLoggedOn(String userId) throws Exception {
        String    result                = null;
        ResultSet l_rs                  = null;
        String    l_session_id          = null;
        Statement l_sel_userlogged_stmt = null;
        String[]  l_args                = new String[1];
        String    l_ret                 = null;

        try {
            dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();
            l_sel_userlogged_stmt = dbConnection.createStatement();
            l_args[0]             = userId;
            l_rs                  = l_sel_userlogged_stmt.executeQuery(MessageFormat.format(SEL_IS_USERLOGGED_STR,
                    l_args));

            if (!l_rs.next()) {
                return null;
            }

            result = l_rs.getString("idsession");
        } finally {
            try {
                l_rs.close();
            } catch (Exception e) {}

            try {
                l_sel_userlogged_stmt.close();
            } catch (Exception e) {}
            
            try {
                dbConnection.close();
            } catch (Exception e) {}
        }

        return result;
    }

    /*
     *    NOTE:Session needs to be terminated before remove session is called.
     *    This method is called from logoff.jsp after session has been invalidated.
     */
    static void removeSession(String p_session_id) {
        Statement         l_stmt     = null;
        String[]          l_args     = new String[1];
        ResultSet         l_rs       = null;
        PreparedStatement l_upd_stmt = null;

        try {
            dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();
            l_stmt    = dbConnection.createStatement();
            l_args[0] = p_session_id;

            try {
                l_rs = l_stmt.executeQuery(MessageFormat.format(SEL_SESSION_LOGINTIME, l_args));

                if (l_rs.next()) {
                    l_upd_stmt = dbConnection.prepareStatement(UPD_USER_LOGIN_SUCC);
                    l_upd_stmt.setTimestamp(1, l_rs.getTimestamp("datcreation"));
                    l_upd_stmt.setString(2, l_rs.getString("iduser"));
                    l_upd_stmt.executeUpdate();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    l_rs.close();
                } catch (Exception e) {}

                try {
                    l_upd_stmt.close();
                } catch (Exception e) {}
                
                try {
                    dbConnection.close();
                } catch (Exception e) {}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
