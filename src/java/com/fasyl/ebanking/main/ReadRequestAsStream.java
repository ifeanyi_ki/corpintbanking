package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Administrator
 */
public class ReadRequestAsStream implements EbankingConfiguration {
    private final int                   MAX_READ_BYTES = 1024 * 4;
    private final ByteArrayOutputStream bop            = new ByteArrayOutputStream(MAX_READ_BYTES);
    private final byte[]                readBuf        = new byte[MAX_READ_BYTES];

    /**
     * This variable stores 0 based index of the next token given by a get ()
     * method.
     */
    private int indexOfLastToken;

    /**
     * This variable stores number tokens found in the given string.
     */
    private int nbrTokens;

    /**
     * This variables stores all the tokens.
     */
    private String tokens[];

    public byte[] readFromStream(InputStream p_stream) throws Exception {
        int l_ret = 0;

        bop.reset();
        System.out.println(p_stream.read(readBuf, 0, MAX_READ_BYTES) + " The read bytes");

        while ((l_ret = p_stream.read(readBuf, 0, MAX_READ_BYTES)) != -1) {
            System.out.println("about to write byte");
            bop.write(readBuf, 0, l_ret);
        }

        return bop.toByteArray();
    }

    public Hashtable createRequestparamHashTable(String p_query_string) throws Exception {
        System.out.println(p_query_string + "p_query_string ");

        StringTokeNizers l_st_amp       = null,
                         l_st_equal     = null;
        String           l_old_values[] = null,
                         l_new_values[] = null,
                         l_key          = null,
                         l_val          = null;
        Hashtable        l_qs_table     = null;
        int              l_i            = 0;

        l_st_amp = new StringTokeNizers(p_query_string, "&");
        System.out.println(l_st_amp + "whyyyyy");
        l_qs_table = new Hashtable();

        while (l_st_amp.hasMoreTokens()) {
            l_st_equal = new StringTokeNizers(l_st_amp.nextToken(), "=");

            if (l_st_equal.countTokens() != 2) {
                continue;
            }

            l_key = l_st_equal.nextToken();
            l_val = new String(decode(l_st_equal.nextToken(), "UTF-8"));

            if (l_qs_table.containsKey(l_key)) {
                l_old_values = (String[]) l_qs_table.get(l_key);
                l_new_values = new String[l_old_values.length + 1];

                for (l_i = 0; l_i < l_old_values.length; l_i++) {
                    l_new_values[l_i] = l_old_values[l_i];
                }

                l_new_values[l_old_values.length] = l_val;
            } else {
                l_new_values    = new String[1];
                l_new_values[0] = l_val;
            }

            l_qs_table.put(l_key, l_new_values);
        }

        System.out.println(l_qs_table + " qstable");

        return l_qs_table;
    }

    public String decode(String p_str, String p_enc) throws Exception {
        String l_str  = null;
        int    l_int  = 0;
        char   l_char = ' ';

        if (p_str == null) {
            return null;
        }

        bop.reset();

        for (int l_i = 0; l_i < p_str.length(); l_i++) {
            l_char = p_str.charAt(l_i);

            switch (l_char) {
            case '+' :
                bop.write(' ');

                break;

            case '%' :
                l_str = p_str.substring(l_i + 1, l_i + 3);
                l_int = Integer.parseInt(p_str.substring(l_i + 1, l_i + 3), 16);
                l_i   += 2;

                // System.out.println(l_int+"hmmmm");
                bop.write(l_int);

                break;

            default :

                // System.out.println(l_char+"gggg!");
                bop.write(l_char);
            }
        }

        if ((p_enc == null) || (p_enc.length() == 0)) {
            return new String(bop.toByteArray());    // .trim ();
        }

        System.out.println(bop.toByteArray());

        return new String(bop.toByteArray(), p_enc);    // .trim ();
    }

    public static void main(String[] args) {
        ReadRequestAsStream test = new ReadRequestAsStream();

        try {
           // String result = test.decode("TOPE & SONS", "UTF-8");
            String result = test.filter("TOPE & SONS");
             //test.JavaEscapeTest();
            System.out.println(result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

     
    public static String getStringArrayValueFromQS(Hashtable p_field_table, String p_field_name, int p_array_position,
            boolean p_optional, String p_default)
            throws Exception {
        Object l_obj;

//      if (bDebugOn)System.out.println("=== This is the filed value==== "+p_field_table.get (p_field_name));
        if ((l_obj = p_field_table.get(p_field_name)) == null || (p_field_table.get(p_field_name) == "")) {
            if (!p_optional) {
                System.out.println("important field is missing");

                // return REQ_FIELD_MISSING; //will uncomment it when appropriate decision has been reached over how to catch exception.
                // throwMissingFieldException (p_lang_id, p_device_id, p_field_name);//need to look for how to catch the exception
            }

            if (p_default != null) {
                return p_default;
            }

            return null;
        }

        if (l_obj instanceof String) {
            if (l_obj == null) {
                if (!p_optional) {

                    // throwMissingFieldException (p_lang_id, p_device_id, p_field_name);
                }

                if (p_default != null) {
                    return p_default;
                }
            }

            return (String) l_obj;
        }

        if ((!p_optional) && ((String[]) l_obj).length <= p_array_position) {

            // throwMissingFieldException (
            // p_lang_id, p_device_id, p_field_name + p_array_position);
        }

        if (((String[]) l_obj).length <= p_array_position || ((String[]) l_obj)[p_array_position] == null) {
            if (p_default != null) {
                return p_default;
            }

            return null;
        }

        return ((String[]) l_obj)[p_array_position];
    }
    
    


//public void JavaEscapeTest() {
//
//	
//                String str = "tope & sons";
//		//String results = StringEscapeUtils.escapeJava(StringUtils.trimToEmpty(str));
//                String results = StringEscapeUtils.ESCAPE_JAVA.translate(str);
//		System.out.println(results);
//

//}


public static Hashtable convertMap(Map map){
    
      System.out.println(map);
      Hashtable ht = new Hashtable();
      Set set = map.entrySet();
      Iterator itr = set.iterator();
      while(itr.hasNext()){
          Map.Entry entry =  (Map.Entry)itr.next();
          Object key = entry.getKey();
          Object val = entry.getValue();
          if(key==null){
              key = ""+null; // Or whatever you want
            }
          if(val==null){
              val = ""+null; // Or whatever you want
            }
          ht.put(key,val);
         
      }
       return ht;
}

public static String filter(String input) {
if (!hasSpecialChars(input)) {
return(input);
}
StringBuffer filtered = new StringBuffer(input.length());
char c;
for(int i=0; i<input.length(); i++) {
c = input.charAt(i);
switch(c) {
case '<': filtered.append("&lt;"); break;
case '>': filtered.append("&gt;"); break;
case '"': filtered.append("&quot;"); break;
case '&': filtered.append("&amp;"); break;
default: filtered.append(c);
}
}
return(filtered.toString());
}

private static boolean hasSpecialChars(String input) {
boolean flag = false;
if ((input != null) && (input.length() > 0)) {
char c;
for(int i=0; i<input.length(); i++) {
c = input.charAt(i);
switch(c) {
case '<': flag = true; break;
case '>': flag = true; break;
case '"': flag = true; break;
case '&': flag = true; break;
}
}
}
return(flag);
}

}



     
  





//~ Formatted by Jindent --- http://www.jindent.com
