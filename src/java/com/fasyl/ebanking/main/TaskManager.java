/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;

/** Manages a pool of threads for executing tasks in separate process.
 *
 * @author NYEMIKE
 */
public class TaskManager {
    
    
      private static final Logger logger = Logger.getLogger(TaskManager.class.getName());

    
    private static ExecutorService execService;

    
    
    public static void initialiaseTaskManager(int threadPoolSize)
    {
        
        logger.info(" about to configure Multitask manager thread pool . . . ");
        
        execService = Executors.newFixedThreadPool(threadPoolSize);
        
        logger.info(" thread pool size: " + threadPoolSize);
        
        logger.info("  Multitask manager thread pool configured! ! ! ");
        
    }
    
    public static ExecutorService getTaskManager()
    {
        
        return execService;
    }
    
    
    public static void shutdownTaskManager()
    {
        
        logger.info(" about to shutdown Task manager thread pool . . . ");
                
        execService.shutdown();
        
        logger.info(" Task manager shutdown !" );
                
    }
    
}
