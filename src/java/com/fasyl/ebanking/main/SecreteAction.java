/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;

import com.fasyl.ebanking.dao.QuestionAndAnswerDao;
import com.fasyl.ebanking.util.Utility;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.Form;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class SecreteAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        Utility utility = new Utility();
        QuestionForm _form = (QuestionForm) form;
        String userAnswer = _form.getAnswer();
        String correctAnswer = _form.getCorrectAnswer();
        String user_id = _form.getUserId();
        String macAddress = _form.getMacAddress();
        //String correctAnswer = (String) request.getAttribute("answer");
        //String macAddress = (String) request.getAttribute("macAddress");

        _form.validate(mapping, request);

        //String user_id = (String) request.getAttribute("user_id");
        System.out.println("user answer in secrete action class: " + userAnswer);
        System.out.println("correct answer in secrete action class: " + correctAnswer);
        System.out.println("user_id in secrete action class: " + user_id);

        if (userAnswer != null && correctAnswer != null) {
            if (!userAnswer.isEmpty() && !correctAnswer.isEmpty()) {
                if (userAnswer.equalsIgnoreCase(correctAnswer)) {
                    //update mac address
                    utility.updateUserMacAddress(macAddress, user_id);
                    return mapping.findForward(SUCCESS);
                } else {
                    return mapping.findForward("secretequestion");
                }
            } else {
                return mapping.findForward("secretequestion");
            }
        }

        return mapping.findForward("secretequestion");
    }
}
