
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

class MyPasswordAuthenticator extends Authenticator {
    String pw;
    String user;

    public MyPasswordAuthenticator(String username, String password) {
        super();
        this.user = username;
        this.pw   = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, pw);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
