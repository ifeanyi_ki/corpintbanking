
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

//~--- JDK imports ------------------------------------------------------------

//import com.fasyl.xnett.util.Encryption;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Nisar
 */
public class LoginForm extends org.apache.struts.action.ActionForm {
    String         password;
    private String secretpasswd;
    String         userid;
    private String macAddress;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    /**
     *
     */
    public LoginForm() {
        super();

        // TODO Auto-generated constructor stub
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        if ((getUserid() == null) || (getUserid().length() < 1)) {

            // System.out.println (Encryption.encrypt(getPassword()));
            errors.add("Userid", new ActionMessage("errors.Userid.required"));

            // TODO: add 'error.name.required' key to your resources
        }

        if ((getPassword() == null) || (getPassword().length() < 1)) {
            errors.add("Password", new ActionMessage("errors.password.required"));
        }

        return errors;
    }

    public ActionErrors noUser(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        // System.out.println (Encryption.encrypt(getPassword()));
        errors.add("Userid", new ActionMessage("errors.Userid.required"));

        // TODO: add 'error.name.required' key to your resources

        return errors;
    }

    public void reset() {
        userid   = null;
        password = null;
    }

    /**
     * @return the secretpasswd
     */
    public String getSecretpasswd() {
        return secretpasswd;
    }

    /**
     * @param secretpasswd the secretpasswd to set
     */
    public void setSecretpasswd(String secretpasswd) {
        this.secretpasswd = secretpasswd;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
