
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

/*sms patch here, phone field added*/
public class User {
    String         affiliate;
    String         branch;
    private String contact;
    private String custno;
    private String email;
    private String phone;
    String         lastlogindate;
    String         loginstate;
    private String noofmailmsgs;
    private String remoteaddress;
    String         role_id;
    private String secret_password;
    private String userType;
    String         userid;
    String         userlang;
    String         username;
    String         userno;
    private String userstatus;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    

    public String getUserlang() {
        return userlang;
    }

    public void setUserlang(String userlang) {
        this.userlang = userlang;
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getAffiliate() {
        return affiliate;
    }

    public void setAffiliate(String affiliate) {
        this.affiliate = affiliate;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(String lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    public String getLoginstate() {
        return loginstate;
    }

    public void setLoginstate(String loginstate) {
        this.loginstate = loginstate;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the custno
     */
    public String getCustno() {
        return custno;
    }

    /**
     * @param custno the custno to set
     */
    public void setCustno(String custno) {
        this.custno = custno;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the remoteaddress
     */
    public String getRemoteaddress() {
        return remoteaddress;
    }

    /**
     * @param remoteaddress the remoteaddress to set
     */
    public void setRemoteaddress(String remoteaddress) {
        this.remoteaddress = remoteaddress;
    }

    /**
     * @return the secret_password
     */
    public String getSecret_password() {
        return secret_password;
    }

    /**
     * @param secret_password the secret_password to set
     */
    public void setSecret_password(String secret_password) {
        this.secret_password = secret_password;
    }

    /**
     * @return the userstatus
     */
    public String getUserstatus() {
        return userstatus;
    }

    /**
     * @param userstatus the userstatus to set
     */
    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    /**
     * @return the noofmailmsgs
     */
    public String getNoofmailmsgs() {
        return noofmailmsgs;
    }

    /**
     * @param noofmailmsgs the noofmailmsgs to set
     */
    public void setNoofmailmsgs(String noofmailmsgs) {
        this.noofmailmsgs = noofmailmsgs;
    }

    /**
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
