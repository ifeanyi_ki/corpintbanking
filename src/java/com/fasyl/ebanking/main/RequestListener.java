/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;


 import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class RequestListener implements Filter{


    @Override
public void destroy(){}
  @Override    
public void doFilter(ServletRequest request,ServletResponse response,FilterChain chain)
throws IOException,ServletException
{
boolean no_splchr_flag = true;
Map inputFields = null;
inputFields = request.getParameterMap();
	if (inputFields != null) {
		Iterator field_iter = inputFields.keySet().iterator();
		while (field_iter.hasNext())
			{
		 String param_name = (String) field_iter.next();
		 String[] param_value = (String[]) inputFields.get(param_name);
		 for (int i = 0; i < param_value.length; i++) {
		   System.out.println("Field="+ param_name + " Entered Value" + param_value[i]);
		  if (containsSplChrs(param_value[i])) {
			 no_splchr_flag = false;
			  break;
			   }
		            }
		if (!no_splchr_flag) {break;	}
				
		           }
		}
		
	if (!no_splchr_flag) {
		try {
		request.getRequestDispatcher( "/splCharacters.jsp").forward( request, response);
		} catch (Exception ex) {
		ex.printStackTrace();
		}
	} 
chain.doFilter(request,response);
}
public void init(FilterConfig filterConfig) throws ServletException{}

//function to check a values contains special characters or not
public static boolean containsSplChrs(String inputStr) { 
			boolean splchr_flag = false;
	 String[] splChrs = { "<",">", 	"script", 	"alert",  "truncate", "delete", "insert" , "drop" , "null", "xp_",  "<>", "!",  "{", "}",  "`", "input" };  // include spl characters as per your requirement
			for (int i = 0; i < splChrs.length; i++) {
				if (inputStr.indexOf(splChrs[i]) >= 0) {
					splchr_flag = true;                          //bad character are available
					break;
				}
			}
			return splchr_flag;
		}



public static String filter(String input) {
if (!hasSpecialChars(input)) {
return(input);
}
StringBuffer filtered = new StringBuffer(input.length());
char c;
for(int i=0; i<input.length(); i++) {
c = input.charAt(i);
switch(c) {
case '<': filtered.append("&lt;"); break;
case '>': filtered.append("&gt;"); break;
case '"': filtered.append("&quot;"); break;
case '&': filtered.append("&amp;"); break;
default: filtered.append(c);
}
}
return(filtered.toString());
}

private static boolean hasSpecialChars(String input) {
boolean flag = false;
if ((input != null) && (input.length() > 0)) {
char c;
for(int i=0; i<input.length(); i++) {
c = input.charAt(i);
switch(c) {
case '<': flag = true; break;
case '>': flag = true; break;
case '"': flag = true; break;
case '&': flag = true; break;
}
}
}
return(flag);
}
}


    

