
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.util.Encryption;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

/**
 *
 * @author Administrator
 */
public class TESTUPLOAD {
    public static void main(String[] args) {
        String     result = null;
        Connection con    = com.fasyl.ebanking.db.ConnectionClass.getConn(null);

        try {
            CallableStatement statement = con.prepareCall("{?=call ebanking_ft_upload.fn_eb_process(?,?,?,?)}");

            statement.registerOutParameter(1, OracleTypes.VARCHAR);
            statement.registerOutParameter(4, OracleTypes.VARCHAR);
            statement.registerOutParameter(5, OracleTypes.VARCHAR);
            statement.setString(2, "DE_UPLOAD");
            statement.setString(3, "008");

            // result=statement.getString(1);
            // System.out.println( statement.getString(1)+" "+ statement.getString(4)+" "+ statement.getString(5));
            statement.executeQuery();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
