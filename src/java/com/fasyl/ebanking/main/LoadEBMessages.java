
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.MessageFormat;

import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public class LoadEBMessages implements EbankingConfiguration {
    private static Hashtable            EBMessagesHash = null;
    private static final String[]       l_args         = new String[0];
    private static final String         selQuery       = "select  * from  EB_MESSAGES";
    private static final LoadEBMessages innerClass     = new LoadEBMessages();
    private static Connection           dbConnection;

    public void LoadEBMessages() {}

    /**
     * This method returns innerclass that represent an object value of the key;
     */
    public static InnerEBMessages getMessageValue(String key) throws Exception {
        if (bdebugOn) {
            System.out.println((LoadEBMessages.InnerEBMessages) EBMessagesHash.get(key.trim()));
        }

        return (LoadEBMessages.InnerEBMessages) EBMessagesHash.get(key.trim());
    }

    public static synchronized void loadData() {
        if (bDebugOn) {
            System.out.println("inside LoadEBMessages __-----------");
        }

        ResultSet rs            = null;
        Statement sel_txn_param = null;

        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();

        String dataKey = null;

        /*
         * if (EBMessagesHash != null ) {
         * System.out.println("loaded");
         * return;
         * }
         */
        try {
            if (bDebugOn) {
                System.out.println("inside LoadEBMessages __----------- try block");
            }

            EBMessagesHash = new Hashtable();
            sel_txn_param  = dbConnection.createStatement();
            rs             = sel_txn_param.executeQuery(MessageFormat.format(selQuery, l_args));

            while (rs.next()) {
                InnerEBMessages innerEBMessages = innerClass.new InnerEBMessages(rs);

                dataKey = rs.getString(1);
                System.out.println(dataKey + " Ebmessages");
                EBMessagesHash.put(dataKey, innerEBMessages);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        finally {
            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public class InnerEBMessages {
        private String messagecode;
        private String messagelang;
        private String messagetype;
        private String messgaedesc;

        public InnerEBMessages(ResultSet p_rs) {
            if (p_rs == null) {

                // need to know how to manage exception
                if (bDebugOn) {
                    System.out.println("rs is null");
                }
            } else {
                if (bDebugOn) {
                    System.out.println("rs is not null");
                }

                try {
                    this.messagecode = p_rs.getString(1);
                    this.messgaedesc = p_rs.getString(2);
                    this.messagelang = p_rs.getString(3);
                    this.messagetype = p_rs.getString(4);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        public String getMessagecode() {
            return messagecode;
        }

        /**
         * @param messagecode the messagecode to set
         */
        public void setMessagecode(String messagecode) {
            this.messagecode = messagecode;
        }

        /**
         * @return the messgaedesc
         */
        public String getMessgaedesc() {
            return messgaedesc;
        }

        /**
         * @param messgaedesc the messgaedesc to set
         */
        public void setMessgaedesc(String messgaedesc) {
            this.messgaedesc = messgaedesc;
        }

        /**
         * @return the messagelang
         */
        public String getMessagelang() {
            return messagelang;
        }

        /**
         * @param messagelang the messagelang to set
         */
        public void setMessagelang(String messagelang) {
            this.messagelang = messagelang;
        }

        /**
         * @return the messagetype
         */
        public String getMessagetype() {
            return messagetype;
        }

        /**
         * @param messagetype the messagetype to set
         */
        public void setMessagetype(String messagetype) {
            this.messagetype = messagetype;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
