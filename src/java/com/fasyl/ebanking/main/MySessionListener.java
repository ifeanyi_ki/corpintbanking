
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 *
 * @author Administrator
 */
public class MySessionListener implements HttpSessionAttributeListener {
    public void attributeReplaced(HttpSessionBindingEvent event) {}

    public void attributeRemoved(HttpSessionBindingEvent event) {
        System.out.println("HttpSessionAttributeListener : This is the session attribute I am listening to :"
                           + event.getName());
    }

    public void attributeAdded(HttpSessionBindingEvent event) {}
}


//~ Formatted by Jindent --- http://www.jindent.com
