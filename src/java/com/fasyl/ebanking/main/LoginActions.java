
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.db.DataBase;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;
import com.fasyl.ebanking.util.XMLUtilty;

import oracle.jdbc.OracleTypes;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class LoginActions extends org.apache.struts.action.Action implements EbankingConfiguration {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    User user = null;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                 HttpServletResponse response) {
        HttpSession session = null;

        Connection conn = null;
        try {
            LoginForm _form = (LoginForm) form;
            _form.validate(mapping, request);
            System.out.println(_form.getUserid());
            System.out.println(_form.getPassword());
            System.out.println(Encryption.encrypt(_form.getPassword()));
            conn = DataBase.getConnection();

            System.out.println("here 1");

            CallableStatement statement = conn.prepareCall("{call Process_Login(?,?,?)}");

            System.out.println("here 2");
            statement.registerOutParameter(3, OracleTypes.VARCHAR);
            System.out.println("here 3");
            statement.setString(1, _form.getUserid());
            System.out.println("here 4");

            // System.err.println(Encryption.encrypt(_form.getPassword()));
            statement.setString(2, Encryption.encrypt(_form.getPassword()));
            statement.executeQuery();

            Document doc = XMLUtilty.getStringAsdocument(statement.getString(3));

            System.out.println(statement.getString(3));

            Element  docEle = doc.getDocumentElement();
            NodeList nl     = docEle.getElementsByTagName("ROW");

            if ((nl != null) && (nl.getLength() > 0)) {
                for (int i = 0; i < nl.getLength(); i++) {
                    Element el = (Element) nl.item(i);

                    user    = getUser(el);
                    session = request.getSession(true);
                }
            }

            session = request.getSession(true);
            session.setMaxInactiveInterval(300);
            session.setAttribute("user", user);
        } catch (Exception ex) {}
        finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (Exception ex) {
            }
        }

        return mapping.findForward(SUCCESS);
    }

    private boolean validateLogin(CallableStatement statement) throws Exception {
        boolean result = false;

        return result;
    }

    private int updateSecretPasswd(String secPasswd, String secretNeeded, String remoteAddress) {
        Connection connect      = com.fasyl.ebanking.db.DataBase.getConnection();
        String     remoteaddres = user.getRemoteaddress();;
        String     updateQry    = null;

        if ((secPasswd != null) && (remoteaddres != null)) {
            updateQry = "update SM_USER_ACCESS set cod_usr_pwd='" + Encryption.encrypt(secPasswd)
                        + "',sec_pass='Y'where cod_usr_id='" + user.getUserid() + "'";
        } else if ((remoteaddres != null) && (secPasswd == null)) {
            updateQry = "update SM_USER_ACCESS set sec_pass='N',default_location='" + remoteAddress
                        + "' where cod_usr_id='" + user.getUserid() + "'";
        } else {
            updateQry = "update SM_USER_ACCESS set default_location='" + remoteAddress + "' where cod_usr_id='"
                        + user.getUserid() + "'";
        }

        PreparedStatement stmt = null;
        int               l_rs = -1;

        try {
            stmt = connect.prepareStatement(updateQry);
            l_rs = stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                connect.close();
                stmt.close();
            } catch (Exception e) {}
        }

        return l_rs;
    }

    private void insertCustDetails(String custno) {
        java.sql.Connection        connection = null;
        java.sql.CallableStatement callable   = null;

        // String data = null;
        try {
            connection = com.fasyl.ebanking.db.DataBase.getConnection();

            // System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
            callable = connection.prepareCall("{call PUT_CUST_ACCT_DET(?)}");
            callable.registerOutParameter(1, oracle.jdbc.OracleTypes.VARCHAR);
            callable.setString(1, custno);

            // /System.out.println(((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno() + "yea");
            callable.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                callable.close();
            } catch (Exception e) {}
        }
    }

    public String getTimeout() {
        return null;
    }

    private User getUser(Element elem) {
        User user = new User();

        user.setUserid(getTextValue(elem, "COD_USR_ID"));
        user.setUserno(getTextValue(elem, "COD_USR_NO"));
        user.setUsername(getTextValue(elem, "COD_USR_NAME"));
        user.setRole_id(getTextValue(elem, "COD_ROLE_ID"));
        user.setBranch(getTextValue(elem, "COD_USR_BRANCH"));
        user.setAffiliate(getTextValue(elem, "COD_USR_AFFILIATE"));
        user.setUserlang(getTextValue(elem, "COD_LANG"));
        user.setLoginstate(getTextValue(elem, "FLG_LOGIN_STAT"));
        user.setLastlogindate(getTextValue(elem, "DAT_LAST_SIGNON"));
        user.setCustno(getTextValue(elem, "CUST_NO"));
        user.setEmail(getTextValue(elem, "EMAIL"));
        user.setRemoteaddress(getTextValue(elem, "DEFAULT_LOCATION"));
        user.setSecret_password((getTextValue(elem, "SEC_PASS").trim() == null)
                                ? ""
                                : getTextValue(elem, "SEC_PASS").trim());
        System.out.println(user.getRemoteaddress() + "This is the address");
        System.out.println(user.getSecret_password() + "This is the sec passwd");

        return user;
    }

    public String getTextValue(Element ele, String tagName) {
        String   textVal = null;
        NodeList nl      = ele.getElementsByTagName(tagName);

        if ((nl != null) && (nl.getLength() > 0)) {
            Element el = (Element) nl.item(0);

            textVal = el.getFirstChild().getNodeValue();
        }

        return textVal;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
