package com.fasyl.ebanking.main;

import com.fasyl.corpIntBankingadmin.daoImpl.PasswordChangeDAOImpl;
import com.fasyl.ebanking.util.Encryption;

import com.fasyl.vo.UserVo;
import com.fasyl.vo.ftVo;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginHandler extends HttpServlet {

    private String userid;
    private String password;
    private String role_id;
    private String organisation;
    private String last_login_date;
    private String login_count;
    private String active;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("logoff") == null) {

            // DataObject dob = new DataObject();
            request.setAttribute("user_id", request.getParameter("user_id"));
            request.setAttribute("email", request.getParameter("email"));
            request.setAttribute("mobile_number", request.getParameter("mobile_number"));
            request.setAttribute("first_name", request.getParameter("first_name"));
            request.setAttribute("last_name", request.getParameter("last_name"));
            request.setAttribute("role_id", request.getParameter("role_id"));
            request.setAttribute("client", request.getParameter("client"));

            HttpSession session = request.getSession(true);
            if (request.getParameter("taskId") == null) {

                session.setAttribute("user_id", request.getParameter("user_id"));

                if ((getValue("select userid from tb_user_access where userid = '" + request.getParameter("user_id") + "'") != null)) {
                    setValues(request.getParameter("user_id"));
                    session.setAttribute("lastLogin", last_login_date);
                    session.setAttribute("client", organisation);
                }

                if ((getValue("select userid from tb_user_access where userid = '" + request.getParameter("user_id") + "'") == null)) {
                    request.setAttribute("message", "You are not a registered user on this platform");
                    dispatch(request, response, "/Login.jsp");
                } else if (login_count.equalsIgnoreCase("0") && Encryption.encrypt(request.getParameter("password")).equalsIgnoreCase(password)) {
                    request.setAttribute("userid", request.getParameter("userid"));
                    dispatch(request, response, "/first_login.jsp");
                } else if (active.equalsIgnoreCase("N")) {
                    request.setAttribute("message", "your account is not active, please contact the system admin");
                    dispatch(request, response, "/Login.jsp");
                } else if (Encryption.encrypt(request.getParameter("password")).equalsIgnoreCase(password)) {
                    session.setAttribute("role_id", role_id);
                    Date date = new Date(System.currentTimeMillis());
                    executeQuery("update tb_user_access set last_login_date = '" + date + "' where  userid = '" + session.getAttribute("user_id") + "'");
                    int update = Integer.parseInt(login_count) + 1;
                    executeQuery("update tb_user_access set login_count = '" + update + "' where userid = '" + session.getAttribute("user_id") + "'");
                    dispatch(request, response, "/Home.jsp");
                } else {
                    request.setAttribute("message", "invalid login parameters");
                    dispatch(request, response, "/Login.jsp");

                }

            } else if (request.getParameter("taskId").equalsIgnoreCase("create_admin_user")) {
                dispatch(request, response, "/confirmAdminUserCreate.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("create_client_user_init")) {
                dispatch(request, response, "/confirmClientUserCreate.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit")) {
                dispatch(request, response, "/CreateClientUser.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_admin_create")) {
                dispatch(request, response, "/CreateAdminUser.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("user_details")) {
                request = loadUser(request);
                dispatch(request, response, "/view_user.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("view_initiators")) {
                request.setAttribute("account", request.getParameter("account"));
                dispatch(request, response, "/initiator_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("view_mandates")) {
                request.setAttribute("account", request.getParameter("account"));
                System.out.println("am here now");
                dispatch(request, response, "/mandate_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("create_client_user")) {
                dispatch(request, response, "/CreateClientUser.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("create_client_admin")) {
                dispatch(request, response, "/CreateAdminUser.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("create_client_user")) {
                dispatch(request, response, "/CreateUser.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("onboard_customer")) {
                dispatch(request, response, "/onboard_corporate_client_1.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("search_details")) {
                request.setAttribute("id_name", request.getParameter("id_name").toUpperCase());
                dispatch(request, response, "/corporate_search_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("search_user_details")) {
                request.setAttribute("id_name", request.getParameter("id_name"));
                dispatch(request, response, "/user_search_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("correct_admin_create")) {
                dispatch(request, response, "/correction_list_admin_create.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("authorize_passwd_change")) {
                dispatch(request, response, "/passwd_change_request_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("change_password")) {
                dispatch(request, response, "/change_password.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("get_ft_details")) {
                request.setAttribute("trans_id", request.getParameter("id"));
                dispatch(request, response, "/trans_detail.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("post")) {
                request.setAttribute("trans_id", request.getParameter("id"));

                String trans_id = request.getParameter("id");
                String query = postTransaction(trans_id);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", trans_id + " has been succesfully posted");
                } else {
                    request.setAttribute("result", "an error occured while processing transaction " + trans_id + " please contact the system admin");
                }
                dispatch(request, response, "/post_transfer.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("back")) {
                String page = request.getParameter("page");

                dispatch(request, response, "/" + page);

            } else if (request.getParameter("taskId").equalsIgnoreCase("authFT")) {
                String trans_id = request.getParameter("trans_id");
                String action = request.getParameter("action");
                String auth1_comment = request.getParameter("auth1_comment");
                String auth2_comment = request.getParameter("auth2_comment");

                role_id = (String) session.getAttribute("role_id");
                String query = null;

                switch (role_id) {
                    case "LOW LEVEL AUTHORIZER":
                        query = "update fund_transfer set auth1_action = '" + action + "', auth1_comment = '" + auth1_comment + "' where trans_id = '" + trans_id + "'";
                        break;
                    case "HIGH LEVEL AUTHORIZER":
                        query = "update fund_transfer set auth2_action = '" + action + "', auth2_comment = '" + auth2_comment + "'  where trans_id = '" + trans_id + "'";
                        break;
                }

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", trans_id + " has been succesfully " + action);
                } else {
                    request.setAttribute("result", "an error occured while processing transaction " + trans_id + " please contact the system admin");
                }

                dispatch(request, response, "/" + "pending_trans.jsp");

                updatePostStatus(trans_id);

            } else if (request.getParameter("taskId").equalsIgnoreCase("movetopage")) {

                String page = request.getParameter("page");
                request.setAttribute("user_id", request.getParameter("user_id"));
                request.setAttribute("email", request.getParameter("email"));
                request.setAttribute("role_id", request.getParameter("role_id"));
                request.setAttribute("mobile_number", request.getParameter("mobile_number"));
                request.setAttribute("first_name", request.getParameter("first_name"));
                request.setAttribute("last_name", request.getParameter("last_name"));
                request.setAttribute("client", request.getParameter("client"));
                request.setAttribute("created_by", request.getParameter("created_by"));
                request.setAttribute("active", request.getParameter("active"));

                dispatch(request, response, "/" + page);

            } else if (request.getParameter("taskId").equalsIgnoreCase("change_pass")) {
                PasswordChangeDAOImpl passwordDaoImpl = new PasswordChangeDAOImpl();
                String oldPassword = request.getParameter("oldpasswd").trim();
                String newPassword = request.getParameter("newpasswd").trim();
                String userid = (String) session.getAttribute("user_id");
                
                newPassword = Encryption.encrypt(newPassword);
                oldPassword = Encryption.encrypt(oldPassword);
                
                String message = passwordDaoImpl.changePassword(userid, newPassword, oldPassword);
                System.out.println("result of change of password: " + message);
//                if (Encryption.encrypt(request.getParameter("oldpasswd")).equalsIgnoreCase(getValue("select password from tb_user_access where userid = '" + session.getAttribute("user_id") + "'"))) {
//
//                    //  if (new DataObject().getValue("select login_count from tb_user_access where userid = '" + request.getParameter("user_id") + "'").equalsIgnoreCase("0")) {   
//                    String query = "insert into password_change_request(userid, new_password, status) values ('" + session.getAttribute("user_id") + "', '" + Encryption.encrypt(request.getParameter("newpasswd")) + "' , " + "'pending')";
//
//                    String check = getValue("select status from password_change_request where userid = '" + session.getAttribute("user_id") + "'");
//
//                    System.out.println("check value for password change request: " + check);
//                    
//                    if (check == null || "APPROVED".equalsIgnoreCase(check) || "DECLINED".equalsIgnoreCase(check)) {
//                        query = executeQuery(query);
//                        if (query.equalsIgnoreCase("success")) {
//                            request.setAttribute("result", "password change request has been queued up for authorization");
//                        } else {
//                            request.setAttribute("result", "error while processing password change request");
//                        }
//                    } else if (check.equalsIgnoreCase("pending")) {
//                        request.setAttribute("result", "You have a request pending for authorisation. Please contact your authorizer to attend to your previous request. Thank you");
//                    } else {
//
//                    }
//
//                } else {
//                    request.setAttribute("result", "current password is not correct.");
//                }
//
                request.setAttribute("result", message);
                dispatch(request, response, "/result.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("get_details")) {

                String client_name = request.getParameter("client_name").trim();
                request.setAttribute("customer_name1", client_name);
                request.setAttribute("customer_no", getValue("select customer_no from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("customer_type", getValue("select customer_type from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("address_line1", getValue("select address_line1 from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("address_line2", getValue("select address_line2 from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("address_line3", getValue("select address_line3 from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("address_line4", getValue("select address_line4 from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("country", getValue("select customer_no from sttm_customer where customer_name1 = '" + client_name + "'"));
                request.setAttribute("nationality", getValue("select nationality from sttm_customer where customer_name1 = '" + client_name + "'"));

                dispatch(request, response, "/corporate_details.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("get_user_details")) {
                request = loadUser(request);
                dispatch(request, response, "/edit_user.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_user")) {
                dispatch(request, response, "/search_user.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("authorize_admin_client")) {
                dispatch(request, response, "/pending_client_admin_create.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("reset_password")) {
                String query = "update tb_user_access set password = '" + Encryption.encrypt("password") + "', login_count = 0 where userid = '" + request.getParameter("user_id") + "'";
                query = executeQuery(query);
                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "password reset successfully effected for user : " + request.getParameter("user_id"));
                } else {
                    request.setAttribute("result", "password reset for " + request.getParameter("user_id") + " cannot be carried out now. please contact system admin");
                }
                dispatch(request, response, "/result.jsp");
            } else if (request.getParameter("taskId").equalsIgnoreCase("approve_pwd_change")) {
                String query = "update tb_user_access set password = (select new_password from password_change_request where userid = '" + request.getParameter("id") + "' and status = 'pending') where userid = '" + request.getParameter("id") + "'";
                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    new DataObject().executeStatement("update password_change_request set status = 'APPROVED', authorised_by ='" + session.getAttribute("user_id") + "' where userid = '" + request.getParameter("id") + "' and status = 'pending'");
                    request.setAttribute("result", "password change successfully effected for user : " + request.getParameter("id"));
                    dispatch(request, response, "/passwd_change_request_list.jsp");

                } else {
                    request.setAttribute("result", "this action cannot be carried out now please try again later");
                    dispatch(request, response, "/passwd_change_request_list.jsp");

                }
            } else if (request.getParameter("taskId").equalsIgnoreCase("decline_pwd_change")) {

                new DataObject().executeStatement("update password_change_request set status = 'DECLINED', authorised_by ='" + session.getAttribute("user_id") + "' where userid = '" + request.getParameter("id") + "'");
                request.setAttribute("result", "password change declined for user : " + request.getParameter("id"));
                dispatch(request, response, "/passwd_change_request_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("authorise")) {

                String query = "update tb_user_access set status = '" + request.getParameter("action")
                        + "', authorise_by = '" + session.getAttribute("user_id") + "' "
                        + ", comment = '" + request.getParameter("comment") + "' "
                        + "where userid = '" + request.getParameter("id") + "'";

                executeQuery(query);
                dispatch(request, response, "/pending_client_admin_create.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("resubmit_admin_create")) {

                String query = "update tb_user_access set status = 'Pending'"
                        + ", userid = '" + request.getParameter("user_id") + "' "
                        + ", organisation = '" + request.getParameter("client") + "' "
                        + ", email = '" + request.getParameter("email") + "' "
                        + ", phone_number = '" + request.getParameter("mobile_number") + "' "
                        + ", firstname = '" + request.getParameter("first_name") + "' "
                        + ", lastname = '" + request.getParameter("last_name") + "' "
                        + "where userid = '" + request.getParameter("userid") + "'";

                executeQuery(query);
                dispatch(request, response, "/correction_list_admin_create.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("submit_edit_user")) {

                String query = "update tb_user_access set active = '" + request.getParameter("active") + "' "
                        + ", email = '" + request.getParameter("email") + "' "
                        + ", phone_number = '" + request.getParameter("mobile_number").trim() + "' "
                        + ", role_id = '" + request.getParameter("role_id") + "' "
                        + "where userid = '" + request.getParameter("user_id") + "'";

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", request.getParameter("user_id") + ": successfully edited");
                    dispatch(request, response, "/result.jsp");

                } else {
                    request.setAttribute("result", request.getParameter("user_id") + ": cannot be edited now, please try again later");
                    dispatch(request, response, "/result.jsp");

                }

            } else if (request.getParameter("taskId").equalsIgnoreCase("get_job_details")) {
                String user_id = request.getParameter("client_name").trim();
                request.setAttribute("userid", user_id);
                request = loadUser(request);

                dispatch(request, response, "/admin_create_details.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("init_mandate")) {

                String query = "insert into acc_init_mandate(account, initiator_id, trans_limit) "
                        + "values('" + request.getParameter("account") + "', '"
                        + request.getParameter("initiator") + "', '" + request.getParameter("trans_limit")
                        + "')";

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "Initiator " + request.getParameter("initiator") + " successfully added");
                } else {
                    request.setAttribute("result", "Initiator " + request.getParameter("initiator") + " cannot be added now, please contact system admin");
                }

                dispatch(request, response, "/manage_account.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_initiator")) {
                System.out.println("initiator_id: " + request.getParameter("initiator_id"));
                System.out.println("trans_limit:" + request.getParameter("trans_limit"));
                System.out.println("account: " + request.getParameter("account"));

                request.setAttribute("initiator_id", request.getParameter("initiator_id"));
                request.setAttribute("trans_limit", request.getParameter("trans_limit"));
                request.setAttribute("account", request.getParameter("account"));

                dispatch(request, response, "/edit_initiator.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_initiator_limit")) {

                String query = "update acc_init_mandate set trans_limit = '" + request.getParameter("trans_limit").trim() + "' where initiator_id = '" + request.getParameter("initiator_id") + "'";

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "Transaction Limit for  " + request.getParameter("initiator_id") + " successfully edited to " + request.getParameter("trans_limit"));
                } else {
                    request.setAttribute("result", "Error Occured while trying to edit transaction limit for Initiator :" + request.getParameter("initiator_id") + ", please contact system admin");
                }
                request.setAttribute("account", request.getParameter("account"));
                dispatch(request, response, "/initiator_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("remove_initiator")) {

                String query = "delete from acc_init_mandate where initiator_id = '" + request.getParameter("initiator_id") + "'";

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", request.getParameter("initiator_id") + " successfully removed from account " + request.getParameter("account"));
                } else {
                    request.setAttribute("result", "Error Occured while trying to remove Initiator :" + request.getParameter("initiator_id") + " from " + request.getParameter("account") + " , please contact system admin");
                }
                request.setAttribute("account", request.getParameter("account"));
                dispatch(request, response, "/initiator_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("setupmandate")) {

                float up_lim = Float.parseFloat(request.getParameter("up_limit").replaceAll(",", "").trim());
                float lo_lim = Float.parseFloat(request.getParameter("lo_limit").replaceAll(",", "").trim());

                String columns = "account, upper_limit, lower_limit, operator";
                String values = "'" + request.getParameter("account") + "', "
                        + "" + up_lim + ", "
                        + "" + lo_lim + ", "
                        + "'" + request.getParameter("operator") + "'";

                String query = "insert into account_mandate(" + columns + ") values(" + values + ")";

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "mandate successfully added");
                } else {
                    request.setAttribute("result", "mandate cannot be added now, please contact system admin");
                }

                dispatch(request, response, "/manage_account.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_mandate")) {
                String account = request.getParameter("account");
                String adj = "account = "
                            + "'" + request.getParameter("account") + "' and "
                            + "operator ='" + request.getParameter("operator") + "'";
              

                String query = "update account_mandate set upper_limit = '"
                        + Float.parseFloat(request.getParameter("up_limit").replaceAll(",", "").trim())
                        + "', lower_limit = '" + Float.parseFloat(request.getParameter("lo_limit").replaceAll(",", "").trim())
                        + "' where " + adj;

                System.out.println("query>>>>>>>>: " + query);

                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "mandate successfully edited");
                } else {
                    request.setAttribute("result", "mandate cannot be edited now, please contact system admin");
                }
                request.setAttribute("account", account);
                dispatch(request, response, "/mandate_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("edit_mandate1")) {

                String account = request.getParameter("account");
                String adj = "account = "
                            + "'" + request.getParameter("account") + "' and "
                            + "operator ='" + request.getParameter("operator") + "'";
                    

                String query = "select upper_limit, lower_limit from account_mandate  where " + adj;

                String[] res = getVals(query, 2);
                String lo_limit = res[1];
                String up_limit = res[0];

                System.out.println("ayam up_limit" + up_limit);

                request.setAttribute("account", account);
                request.setAttribute("up_limit", up_limit);
                request.setAttribute("lo_limit", lo_limit);
                request.setAttribute("operator", request.getParameter("operator"));

                dispatch(request, response, "/edit_mandate.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("remove_mandate")) {
                
                String account = request.getParameter("account");

                String adj = "account = "
                        + "'" + request.getParameter("account") + "' and "
                        + "operator ='" + request.getParameter("operator") + "'";

                String query = "delete from account_mandate where " + adj;
                System.out.println("query: " + query);
                query = executeQuery(query);

                if (query.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "mandate successfully removed");
                } else {
                    request.setAttribute("result", "mandate cannot be removed now, please contact system admin");
                }
                request.setAttribute("account", account);
                dispatch(request, response, "/mandate_list.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("transfer_fund")) {
                request.setAttribute("account", request.getParameter("account"));
                String query = "select trans_limit from acc_init_mandate where account ='" + request.getParameter("account") + "' and initiator_id = '" + (String) session.getAttribute("user_id") + "'";
                request.setAttribute("trans_limit", getValue(query));
                dispatch(request, response, "/transfer_fund.jsp");
            } else if (request.getParameter("taskId").equalsIgnoreCase("submit_transfer")) {

                String acc = request.getParameter("account");
                String ben_acc = request.getParameter("ben_acct");
                String amount = request.getParameter("amount");
                String narration = request.getParameter("narration");
                String token = request.getParameter("token");
                String trans_limit = request.getParameter("trans_limit");

                double limit = Double.parseDouble(trans_limit.replaceAll(",", ""));
                double amt = Double.parseDouble(amount.replaceAll(",", ""));

                if ((amt - limit) > 0) {
                    request.setAttribute("result", "You cannot exceed " + trans_limit);
                    request.setAttribute("trans_limit", trans_limit);
                    request.setAttribute("account", request.getParameter("account"));
                    dispatch(request, response, "/transfer_fund.jsp");
                } else {

                    String query = "select auth1, operator, auth2 from account_mandate where lower_limit <= " + amt + " and upper_limit >= " + amt + " and account ='" + acc + "'";
                    String[] mandate = getVals(query, 3);

                    if (mandate != null) {
                        String auth1 = mandate[0];
                        String operator = mandate[1];
                        String auth2 = mandate[2];

                        String columns = "trans_id, initiator_id, amount, init_account, ben_account, narration, init_time, auth1, auth1_action, operator, auth2";
                        String trans_id = generateId();

                        ftVo tvo = new ftVo();
                        tvo.setAcc(acc);
                        tvo.setAmt(amt);
                        tvo.setAuth1(auth1);
                        tvo.setAuth2(auth2);
                        tvo.setAuth1_action("pending");
                        tvo.setBen_acc(ben_acc);
                        tvo.setInt_id((String) session.getAttribute("user_id"));
                        tvo.setNarration(narration);
                        tvo.setTrans_id(trans_id);
                        tvo.setOperator(operator);
                        tvo.setInit_acc(ben_acc);

                        boolean result = new DataObject().persistFundTransfer(tvo);

                        if (result) {
                            request.setAttribute("result", "transfer has been queued up for authorisation");

                            switch (operator) {
                                case "AND":
                                    executeQuery("update fund_transfer set auth2_action ='pending' where trans_id ='" + trans_id + "'");
                                    break;
                                case "OR":
                                    executeQuery("update fund_transfer set auth2_action ='pending' where trans_id ='" + trans_id + "'");
                                    break;
                            }

                        } else {
                            request.setAttribute("result", "an error occured while processing transfer, please contact system admin");
                        }

                        request.setAttribute("trans_limit", trans_limit);
                        request.setAttribute("account", request.getParameter("account"));
                        dispatch(request, response, "/transfer_fund.jsp");

                    } else {
                        request.setAttribute("result", "A mandate has not been created for this transaction, please ask the admin to create mandate and retry, thanks");
                        request.setAttribute("account", request.getParameter("account"));
                        request.setAttribute("trans_limit", trans_limit);
                        dispatch(request, response, "/transfer_fund.jsp");
                    }

                }
            } else if (request.getParameter("taskId").equalsIgnoreCase("account")) {
                String account = request.getParameter("account");
                request.setAttribute("account", account);

                dispatch(request, response, "/manage_account.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("first_pwd_change")) {
                if (request.getParameter("first_password_change").equals(request.getParameter("first_password_change_confirm"))) {
                    String query = "update tb_user_access set password = '" + Encryption.encrypt(request.getParameter("first_password_change")) + "', login_count = 1 where userid ='" + request.getParameter("user_id") + "'";
                    query = executeQuery(query);
                    if (query.equalsIgnoreCase("success")) {
                        request.setAttribute("message", "Password successfully changed. You may now log in with your new password");
                        dispatch(request, response, "/Login.jsp");

                    } else {
                        request.setAttribute("message", "An error occured while trying to update your password. Please contact system admin");
                        dispatch(request, response, "/Login.jsp");

                    }
                } else {
                    request.setAttribute("message", "passwords do not match");
                    dispatch(request, response, "/Login.jsp");
                }
            } else if (request.getParameter("taskId").equalsIgnoreCase("correct_admin_create_form")) {
                String user_id = request.getParameter("client_name").trim();

                request.setAttribute("userid", user_id);
                request = loadUser(request);

                dispatch(request, response, "/CreateAdminUserForCorrection.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("onboardClient")) {

                String query = "insert into onboarded_clients select * from sttm_customer where customer_no = '" + request.getParameter("customer_no") + "'";
                String result = executeQuery(query);

                if (result.equalsIgnoreCase("success")) {
                    request.setAttribute("result", request.getParameter("customer_name1") + ": successfully onboarded on corporate internet banking");
                } else {
                    request.setAttribute("result", "the system cannot onboard " + request.getParameter("customer_no") + " now, please contact system admin");
                }

                dispatch(request, response, "/result.jsp");

            } else if (request.getParameter("taskId").equalsIgnoreCase("Submit_admin_create")) {
                // before persisting new user do some validation here       

                UserVo uv = loadRequest(request);

                String columns = "userid,  password, firstname,  lastname,  role_id, organisation, created_by, active, email, phone_number, status, login_count";
                String values = "'" + uv.getId() + "', '" + Encryption.encrypt("password") + "','" + uv.getFirstName() + "','" + uv.getLastName()
                        + "', '" + uv.getRoleId() + "', '" + uv.getClient() + "', '" + uv.getCreated_by() + "', 'Y'" + ", '" + uv.getEmail() + "', '"
                        + uv.getPhoneNumber() + "', '" + uv.getStatus() + "', '" + 0 + "'";

                String query = "insert into tb_user_access(" + columns + ")values(" + values + ")";

                String result = executeQuery(query);

                if (result.equalsIgnoreCase("success")) {
                    request.setAttribute("result", "user id : " + uv.getId() + " successfully created");
                    dispatch(request, response, "/result.jsp");
                    //dispatch(request, response, "/admin_create_success.jsp");

                } else {
                    request.setAttribute("result", "user id : " + uv.getId() + " cannot be created now please contact admin");
                    dispatch(request, response, "/result.jsp");
                    //dispatch(request, response, "/admin_create_success.jsp");

                }

            } else {

                dispatch(request, response, "/" + request.getParameter("taskId") + ".jsp");

            }
            //         
        } else {

            dispatch(request, response, "/Login.jsp");

        }

    }

    public UserVo loadRequest(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        UserVo uv = new UserVo();
        uv.setCreated_by((String) session.getAttribute("user_id"));

        uv.setUserName(request.getParameter("user_id"));
        uv.setFirstName(request.getParameter("first_name"));
        uv.setId(request.getParameter("user_id"));
        uv.setLastName(request.getParameter("last_name"));
        uv.setPhoneNumber(request.getParameter("mobile_number"));
        uv.setGender(request.getParameter("gender"));
        uv.setLocationId(request.getParameter("location"));
        uv.setEmail(request.getParameter("email"));
        uv.setClient((String) session.getAttribute("client"));
//      if (((String) session.getAttribute("role_id")).equalsIgnoreCase("super_admin")) 
        uv.setStatus("Approved");
//        } else {
//            uv.setStatus("Pending");
//        }
        uv.setRoleId(request.getParameter("role_id"));
        return uv;
    }

    public HttpServletRequest loadUser(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        String user_id = null;

        if (request.getParameter("user_id") != null) {
            user_id = request.getParameter("user_id").trim();
        }

        if (request.getParameter("client_name") != null) {
            user_id = request.getParameter("client_name").trim();
        }

        request.setAttribute("userid", user_id);
        request.setAttribute("user_id", user_id);
        request.setAttribute("email", getValue("select email from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("role_id", getValue("select role_id from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("mobile_number", getValue("select phone_number from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("first_name", getValue("select firstname from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("last_name", getValue("select lastname from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("client", getValue("select organisation from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("comment", getValue("select comment from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("comment_by", getValue("select authorise_by from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("created_by", getValue("select created_by from tb_user_access where userid = '" + user_id + "'"));
        request.setAttribute("active", getValue("select active from tb_user_access where userid = '" + user_id + "'"));

        return request;
    }

    private String executeQuery(String query) {
        String result = new DataObject().executeStatement(query);
        return result;
    }

    private String getValue(String query) {
        String result = new DataObject().getValue(query);
        return result;
    }

    private String generateId() {
        String result = new DataObject().getValue("select trans_count from trans_id_count");
        int count = Integer.parseInt(result) + 1;
        executeQuery("update trans_id_count set trans_count = '" + count + "'");
        return result;
    }

    private String[] getVals(String query, int k) {
        List<String[]> result = new DataObject().getLists(query, k);

        if (!result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    private void updatePostStatus(String trans_id) {
        String[] mandate = getVals("select auth1_action, operator, auth2_action from fund_transfer where trans_id = '" + trans_id + "'", 3);
        String auth1_action = mandate[0];
        String operator = mandate[1];
        String auth2_action = mandate[2];
        String query = null;
        if (operator.equalsIgnoreCase("AND")) {
            query = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED' and auth2_action = 'APPROVED' and trans_id = '" + trans_id + "'";

        } else if (operator.equalsIgnoreCase("ONLY")) {
            query = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED'  and trans_id = '" + trans_id + "'";

        } else if (operator.equalsIgnoreCase("OR")) {
            query = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED' or auth2_action = 'APPROVED' and trans_id = '" + trans_id + "'";
        }
        executeQuery(query);
    }

    private String postTransaction(String trans_id) {
        String query = "update fund_transfer set post_status = 'POSTED' where trans_id = '" + trans_id + "'";
        query = executeQuery(query);
        return query;
    }

    private void setValues(String user_id) {
        String query = "select userid, password, role_id, organisation, last_login_date, login_count, active from tb_user_access where userid = '" + user_id + "'";
        String[] val = getVals(query, 7);

        userid = val[0];
        password = val[1];
        role_id = val[2];
        organisation = val[3];
        last_login_date = val[4];
        login_count = val[5];
        active = val[6];
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
