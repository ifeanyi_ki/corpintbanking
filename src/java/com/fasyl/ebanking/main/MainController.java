
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.logic.LoadBasicParam;
import com.fasyl.ebanking.logic.LoadEmails;
import com.fasyl.ebanking.logic.Processes;
import com.fasyl.ebanking.util.AuditLogHandler;
import com.fasyl.ebanking.util.CacheClass;
import com.fasyl.ebanking.util.LoadData;
import com.fasyl.ebanking.util.Utility;

//~--- JDK imports ------------------------------------------------------------
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class MainController extends HttpServlet implements EbankingConfiguration {

    private static final Logger logger = Logger.getLogger(MainController.class.getName());

    static {
        initServletData();
    }
    private UserAccessBo userAccessBo = new UserAccessBoImpl();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, DataofInstance instData)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("**** in main controller instData is *** ." + instData);
        PrintWriter outs = response.getWriter();
        DataofInstance inst_data = null;
        Object[] l_obj = null;
        HttpSession session = request.getSession(false);
        ServletException l_servlet_exception = null;
        
        try {
            if (instData == null) {
                if (bDebugOn) {
                    System.out.println("creating a new instance of instance Data.");
                }
                
                inst_data = DataofInstance.getInstance();
                System.out.println("instance data loaded **** inst_data is " + inst_data);
            } else {
                inst_data = instData;
            }

            inst_data.remoteAddress = request.getRemoteAddr();
            System.out.println("remote address gotten *** address is ***" + inst_data.remoteAddress);
            if (bDebugOn) {
                System.out.println(inst_data.remoteAddress);
            }

            analyseRequest(request, inst_data);
            System.out.println("done analysing request ***");

            try {    // the whole logic in this try block will be out into a method for proper error maintainace
                inst_data.sessionId = session.getId();
                inst_data.screenId = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                        "screenId", 0, false, "1");
                inst_data.taskId = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "taskId",
                        0, true, "0");
                inst_data.custId = ((UserVo) session.getAttribute("userVo")).getCustomerNo();
                inst_data.email = ((UserVo) session.getAttribute("userVo")).getEmail();
                inst_data.phone = ((UserVo) session.getAttribute("userVo")).getPhoneNumber();
                inst_data.contact = ((UserVo) session.getAttribute("userVo")).getContact();
                System.out.println(inst_data.screenId + inst_data.taskId + inst_data.txnId);
                inst_data.datavalue = LoadData.getDataValue(inst_data.screenId.trim(),
                        inst_data.taskId.trim() /* ,inst_data.txnId.trim() */);

                if (inst_data.datavalue == null) {
                    if (bDebugOn) {
                        System.out.println("datavalue is null");
                    }

                    inst_data.result = Utility.processError(inst_data, ERR_TASKIDSCREENIDISNULL);
                    BuildResponse(request, response, inst_data);

                    return;
                }

                inst_data.ftPrdCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                        "ftPrdCode", 0, true, "0");
                inst_data.txnId = inst_data.datavalue.getTxnid();
                inst_data.paramValue = LoadBasicParam.getDataValue(inst_data.ftPrdCode);
                inst_data.paramMap = LoadBasicParam.getBasicParamMapValue();
                inst_data.requestId = com.fasyl.ebanking.util.Utility.getReqId();
                inst_data.userId = ( (String)(session.getAttribute("user_id")));
                inst_data.customerType = "C";
                inst_data.userName = ((UserVo) (session.getAttribute("userVo"))).getClient();

                // System.out.println("Linecomment  "+inst_data.datavalue.getLinecomment());
                if (bDebugOn) {
                    System.out.println(inst_data.requestId + "  ===request Id== " + inst_data.customerType);
                }

                AuditLogHandler.log(inst_data);

                // }
                // System.out.println(inst_data.datavalue.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            delegateReqService(inst_data);

            if (inst_data.result != null) {
                BuildResponse(request, response, inst_data);
            }

            if (l_servlet_exception != null) {
                throw l_servlet_exception;
            }
        } finally {
            DataofInstance.putBack(inst_data);

            // outs.close();
        }
    }

    protected void analyseRequest(HttpServletRequest request, DataofInstance instanceData) {
        System.out.println(" ** analysing request **** ");
        byte[] bytes = null;
        String result = null;
        Hashtable resultMap = new Hashtable();
        ReadRequestAsStream reqStream = new ReadRequestAsStream();

        try {
            if (bDebugOn) {
                System.out.println(" this is the method " + request.getMethod());
            }

            if (bDebugOn) {
                System.out.println(" this is " + request.getInputStream() + " This is byte " + bytes);
            }

            bytes = reqStream.readFromStream(request.getInputStream());

            // byte [] bytes2 =  reqStream.readFromStream (request.getReader());
            if (request.getMethod().equalsIgnoreCase("post") && (bytes.length != 0)) {
                if (bytes == null) {
                    if (bDebugOn) {
                        System.out.println("bytes is null");
                    }

                    // put the exceptio code here
                }

                if (bDebugOn) {
                    System.out.println("bytes is not null" + bytes.length);
                }

                result = reqStream.decode(new String(bytes, "UTF-8").trim(), "UTF-8");

                if (bDebugOn) {
                    System.out.println(result + " This is the result");
                }

                instanceData.requestHashTable = reqStream.createRequestparamHashTable(new String(bytes,
                        "UTF-8").trim());

                if (bDebugOn) {
                    System.out.println(instanceData.requestHashTable + "  =====RequestHahTab;e");
                    System.out.println(result + " another result");
                    System.out.println(resultMap + " Resultmap");
                }
            } else {

                // System.out.println(request.getInputStream().available());
                // bytes = reqStream.readFromStream(request.getInputStream());  //This get therequest from the stream
                // System.out.println(request.getParameter("screenId") + " ScreenId");
                if (bDebugOn) {
                    System.out.println(request.getQueryString());
                }

                String reqString = request.getQueryString();

                instanceData.request = reqString;
                System.out.println("This is request string: " + reqString);
                //instanceData.requestHashTable = HttpUtils.parseQueryString(reqString);//getParameterMap()

                Map map = request.getParameterMap();
                //if (bDebugOn) {
                System.out.println("This the map" + map);
                //}
                instanceData.requestHashTable = ReadRequestAsStream.convertMap(map);
                System.out.println(instanceData.requestHashTable + "");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // return resultMap;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);

        User user = null;
        HttpSession session = request.getSession(false);
        String test = request.getParameter("logoff");
        System.out.println("test for loggoff: " + test);

//        if (!(session == null)) {
//            user = (User) session.getAttribute("user");
//        }

        if (request.getParameter("logoff") != null) {
            if (bDebugOn) {
                System.out.println("======= inside logoff ======= ");
            }

            // forwardTo("/Login.jsp", getServletContext(), request, response);
            logout(session, response, request);

            return;
        }

//        if ((session == null) || (session.getAttribute("userVo") == null)) {
//            sessionTimeout(null, response, request);
//
//            return;    // forwardTo("/Login.jsp",getServletContext(),request,response);
//        } 
//        else if ((!ManageLogin.validateUser4Trx(user.getUserid() + " " + session.getId())
//                && !(user.getUserType().equals("CORP")))) {
//
//            // ManageLogin.invalidateUser(session.getId());
//            session.setMaxInactiveInterval(0);
//            sessionTimeout(null, response, request);
//
//            return;
//        }

        if (bDebugOn) {
            System.out.println("======= outside logoff test is :  ======= " + test);
        }

        // System.out.println(sess.getLastAccessedTime());
        // if(sess.)
        processRequest(request, response, null);
    }

    public void forwardTo(String sTarget, ServletContext context, ServletRequest request, ServletResponse response) {
        try {
            RequestDispatcher dispatcher = context.getRequestDispatcher(sTarget);

            dispatcher.forward(request, response);

            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }    // </editor-fold>

    public static void initTaskManager() {

        try {
            logger.info("initialise task manager . . .");
            System.out.println("task manager pool size: " + LoadBasicParam.getDataValuenew("TASKMGRPOOLSIZE"));
            
            Integer taskManagerPoolSize = new Integer((String) LoadBasicParam.getDataValuenew("TASKMGRPOOLSIZE"));

            logger.info(" task manager pool size: " + taskManagerPoolSize);

            TaskManager.initialiaseTaskManager(taskManagerPoolSize.intValue());

            logger.info(" task manager initialise ! ! ! ");

        } catch (Exception ex) {
            logger.error(ex.getStackTrace(), ex);
        } finally {
        }

    }

    public static void initServletData() {
        LoadData.loadData();
        LoadEBMessages.loadData();
        LoadBasicParam.loadData();
        LoadEmails.loadData();

        initTaskManager();

        String screenId = null;
        String taskId = null;
        String valueKey = null;
        String dataValue = null;
        String className = null;
        LoadData.LoadDataValue loaddata = null;
        Enumeration valueKeys = LoadData.keys();

        while (valueKeys.hasMoreElements()) {
            valueKey = (String) valueKeys.nextElement();

            try {
                loaddata = LoadData.getDataValue(valueKey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            className = loaddata.getClassname();

            if (bDebugOn) {
                System.out.println("This is the loaded classname " + className);
            }

            if (bDebugOn) {
                System.out.println("Trx Description " + loaddata.getLinecomment());
            }

            if (!(className.equals("com.fasyl.ebanking.logic.noclass"))) {
                try {
                    if (!CacheClass.isIdentityRegistered(className)) {
                        CacheClass.createCache(className, Class.forName(className));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void sendResponse(HttpServletRequest req, HttpServletResponse res, String scn) {
        RequestDispatcher rd = req.getRequestDispatcher(scn);

        try {
            rd.forward(req, res);
        } catch (ServletException ex) {
            ex.printStackTrace();
        } catch (IOException ioex) {
            ioex.printStackTrace();
        }    /*
         * finally{
         * OutputStream output = null;
         * try{
         * output = res.getOutputStream();
         * System.out.println(output+" This is the solution to9  my problem");
         * }catch(Exception ex){
         * ex.printStackTrace();
         * }
         * }
         */

    }

    private void BuildResponse(HttpServletRequest request, HttpServletResponse response, DataofInstance instData) {
        HttpSession session = request.getSession(false);
        String scn = null;
        HashMap result = instData.result;
        scn = instData.taskId + ".jsp";

        String results = null;
        String otherreq = null;

        // response.encodeRedirectURL(scn);
        scn = "./screens/" + scn + "?jsessionid=" + session.getId();
        System.out.println("session id: " + session.getId());
        if (result.get(ERROR_MAP_KEY) != null) {    // This if will no longer be used if there is an easy way of having popup without sending numeber
            results = (String) result.get(ERROR_MAP_KEY);

            StringWriter writers = new StringWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            try {
                String msg = (String) LoadEBMessages.getMessageValue(results).getMessgaedesc();

                instData.taskId = "usernotfound";
                instData.processStatus = FAIL_PROCESS_STATUS;
                instData.errormessage = msg;
                instData.errorCode = results;

                if (bDebugOn) {
                    System.out.println(instData.processStatus + " " + instData.errormessage + " " + instData.errorCode
                            + " Error Data");
                }

                AuditLogHandler.updateErrorLog(instData);
                response.getWriter().write(results);
                writers.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            results = (String) result.get(RESULT_MAP_KEY);
            otherreq = (String) result.get(OTHER_REQ_RES);

            if (bDebugOn) {
                System.out.println(results + " results " + otherreq + " hmm " + scn);
            }

            request.setAttribute("data", results);
            request.setAttribute("others", otherreq);
            instData.respStatus = String.valueOf(OK_RESP_STATUS);
            instData.processStatus = OK_PROCESS_STATUS;
            AuditLogHandler.updateLog(instData);

            if (instData.taskId.equals("001")) {
                StringWriter writers = new StringWriter();

                response.setContentType("text/xml");
                response.setHeader("Cache-Control", "no-cache");

                try {
                    response.getWriter().write(results);
                    writers.flush();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                response.encodeRedirectURL(scn);
                sendResponse(request, response, scn);
            }
        }
    }

    private void delegateReqService(DataofInstance instData) {

        logger.info("-------------  inside delegateReqService    --------------------- ");

        Processes l_processor = null;
        Class l_comp_class = null;
        String l_class_name = null;

        l_class_name = instData.datavalue.getClassname().trim();
        System.out.println(l_class_name);
        System.out.println(instData.datavalue.getLinecomment().trim() + "This is the line comment");

        try {
            if (l_class_name.trim().equals("com.fasyl.ebanking.logic.noclass")) {
                HashMap noclass = new HashMap();

                noclass.put("returnResult", "noclass");
                instData.result = noclass;
            } else if (l_class_name != null) {
                if (bDebugOn) {
                    System.out.println(" about to check if processes is null ");
                }

                logger.info("log4j: CLASS TO BE USED: " + l_class_name);

                if ((l_processor = (Processes) CacheClass.getObject(l_class_name)) == null) {
                    if (bDebugOn) {
                        System.out.println(" ABOUT TO CREATE NEW INSTANCE");
                    }

                    l_comp_class = Class.forName(l_class_name);
                    l_processor = (Processes) l_comp_class.newInstance();

                    logger.info(l_class_name + " INSTANTIATED ! ");

                }

                if (bDebugOn) {
                    System.out.println("about to call process ");
                }

                logger.info(" invoking process request for " + l_class_name);

                instData.result = l_processor.processRequest(instData);

                logger.info(" after process request for " + l_class_name);

            } else {
                instData.result = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (l_processor != null) {
                    // CacheClass.putObject(l_class_name, l_processor);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sessionTimeout(HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        System.out.println();

        if (session == null) {
            StringWriter writers = new StringWriter();

            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");

            try {
                response.getWriter().write(ENDOFSESSION);
                writers.flush();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            // forwardTo("/Login.jsp",getServletContext(),request,response);
        }
    }

    public void logout(HttpSession session, HttpServletResponse response, HttpServletRequest request) {
        java.sql.Connection connection = null;
        String custno = null;

        try {
            connection = com.fasyl.ebanking.db.DataBase.getConnection();

            if (session != null) {
                userAccessBo.updateLoginState((String) session.getAttribute("user_id"), "N");
//              custno = ((com.fasyl.ebanking.main.User) session.getAttribute("user")).getCustno();
//              java.sql.Statement stmt = connection.createStatement();
//              com.fasyl.ebanking.main.User user = (com.fasyl.ebanking.main.User) session.getAttribute("user");
//               int i=stmt.executeUpdate("update sm_user_access set flg_login_stat='N' where COD_USR_ID='"+user.getUserid()+"'");
//               int l=stmt.executeUpdate("Delete from CUSTOMER_ACCT_DETAIL where cust_no=" + custno);
                session.invalidate();
            }

            //
            new com.fasyl.ebanking.main.LoginForm().reset();
            //forwardTo("/Login.jsp", getServletContext(), request, response);
            forwardTo("/Logout.jsp", getServletContext(), request, response);
        } catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            try {
                connection.close();
            } catch (Exception e) {
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
