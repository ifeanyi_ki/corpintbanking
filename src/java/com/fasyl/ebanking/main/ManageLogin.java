
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author baby
 */
public class ManageLogin implements EbankingConfiguration {

    /**
     * This instance variable stores the only instance of this class allowed
     */
    private static ManageLogin       bInstance = null;
    private static HashMap           loginMap  = new HashMap();
    private static final ManageLogin login     = new ManageLogin();

    /**
     * This Variable store the record of all the users logged in
     */
    private static HashMap Hashmap;

    static {
        Hashmap = new HashMap();
    }

    public ManageLogin() {}

    /**
     * This method is used to create an instance of a class if
     * There is no instance of this class created.
     */
    public static ManageLogin get() {
        if (bInstance == null) {
            bInstance = new ManageLogin();
        }

        return bInstance;
    }

    /**
     * This method add user to list of valid users that logged into the application
     */
    public static void addUser(ManageLogin.InnerLogin innerLogin) {

        // InnerLogin innerLogin = log;
        Hashmap.put(innerLogin.userid + " " + innerLogin.sessionid, innerLogin);
        loginMap.put(innerLogin.userid, innerLogin);
    }

    public static ManageLogin.InnerLogin getLoginDetail(String userid) {
        ManageLogin.InnerLogin result = (ManageLogin.InnerLogin) loginMap.get(userid);

        // if (bDebugOn)System.out.println(" This is the token ==== "+ token);

        return result;
    }

    public static boolean invalidateUser(String key) {
        boolean          result    = false;
        StringTokeNizers tokenizer = new StringTokeNizers(key, " ");
        String           token     = tokenizer.nextToken();

        if (bDebugOn) {
            System.out.println(" This is the token ==== " + token);
        }

        if (validateUser4Trx(key)) {
            Hashmap.remove(key);
            loginMap.remove(token);
        }

        return result;
    }

    public static boolean validateUser4login(String user) {
        boolean result = false;

        result = loginMap.containsKey(user);

        if (bDebugOn) {
            System.out.println(" This is validate for login +===== " + result);
        }

        return result;
    }

    public static boolean validateUser4Trx(String key) {
        boolean                result   = false;
        ManageLogin.InnerLogin validate = (ManageLogin.InnerLogin) Hashmap.get(key);

        if (bDebugOn) {
            System.out.println(" This is validate for trx +===== " + validate);
        }

        if (validate != null) {

            // if (validate.getUserid()!=null||!validate.getUserid().equals("")){
            result = true;

            // }
        }

        return result;
    }

    public class InnerLogin {
        private String email;
        private String phone;
        private String sessionid;
        private String userid;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        /**
         *   @return the sessionid
         */
        
        
        
        
        public String getSessionid() {
            return sessionid;
        }

        /**
         * @param sessionid the sessionid to set
         */
        public void setSessionid(String sessionid) {
            this.sessionid = sessionid;
        }

        /**
         * @return the userid
         */
        public String getUserid() {
            return userid;
        }

        /**
         * @param userid the userid to set
         */
        public void setUserid(String userid) {
            this.userid = userid;
        }

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

      
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
