package com.fasyl.ebanking.main;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

/**
 * msgsendsample creates a very simple text/plain message and sends it.
 * <p>
 * usage: <code>java msgsendsample <i>to from smtphost true|false</i></code>
 * where <i>to</i> and <i>from</i> are the destination and
 * origin email addresses, respectively, and <i>smtphost</i>
 * is the hostname of the machine that has the smtp server
 * running. The last parameter either turns on or turns off
 * debugging during sending.
 *
 * @author Max Spivak
 */
public class sendmessage extends Authenticator {
    static String msgText = "";

    public sendmessage(String text) {
        this.msgText = text;
    }

    public static void sendMsg(String args) {

        /*
         * if (args.length != 4) {
         *   usage();
         *   System.exit(1);
         * }
         */

        /*
         *  private void MyAuthenticator(String Username,String password) {
         *
         * }
         */
        System.out.println();

        String  to          = "ibanking@trustbondmortgagebankplc.com";
        String  from        = "ibanking@trustbondmortgagebankplc.com";
        String  host        = "172.15.10.11";
        boolean debug       = true;    // Boolean.valueOf(args[3]).booleanValue();
        String  SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        String  user        = "ibanking";
        String  pass        = "Password10@";

        // String DEBUG="true";
        String STARTTLS = "true";

        // create some properties and get the default Session
        Properties props = new Properties();

        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        // props.put("mail.smtp.socketFactory.fallback", "false");
        // props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.", "true");
        props.put("mail.transport.protocol.", "smtp");
        props.put("mail.smtp.starttls.enable", STARTTLS);

        // props.put("mail.smtp.debug", DEBUG);
        if (debug) {
            props.put("mail.debug", debug);
        }

        Session session = Session.getDefaultInstance(props, null);    // Instance(props, null);

        session.setDebug(debug);

        try {

            // create a message
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(from));

            InternetAddress[] address = { new InternetAddress(to) };

            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("JavaMail APIs Test");
            msg.setSentDate(new Date());

            // If the desired charset is known, you can use
            // setText(text, charset)
            msg.setText(msgText);

            Transport transport = session.getTransport("smtp");

            transport.connect(host, user, pass);
            Transport.send(msg);
        } catch (MessagingException mex) {
            System.out.println("\n--Exception handling in msgsend.java");
            mex.printStackTrace();
            System.out.println();

            Exception ex = mex;

            do {
                if (ex instanceof SendFailedException) {
                    SendFailedException sfex    = (SendFailedException) ex;
                    Address[]           invalid = sfex.getInvalidAddresses();

                    if (invalid != null) {
                        System.out.println("    ** Invalid Addresses");

                        if (invalid != null) {
                            for (int i = 0; i < invalid.length; i++) {
                                System.out.println("         " + invalid[i]);
                            }
                        }
                    }

                    Address[] validUnsent = sfex.getValidUnsentAddresses();

                    if (validUnsent != null) {
                        System.out.println("    ** ValidUnsent Addresses");

                        if (validUnsent != null) {
                            for (int i = 0; i < validUnsent.length; i++) {
                                System.out.println("         " + validUnsent[i]);
                            }
                        }
                    }

                    Address[] validSent = sfex.getValidSentAddresses();

                    if (validSent != null) {
                        System.out.println("    ** ValidSent Addresses");

                        if (validSent != null) {
                            for (int i = 0; i < validSent.length; i++) {
                                System.out.println("         " + validSent[i]);
                            }
                        }
                    }
                }

                System.out.println();

                if (ex instanceof MessagingException) {
                    ex = ((MessagingException) ex).getNextException();
                } else {
                    ex = null;
                }
            } while (ex != null);
        }
    }

    private static void usage() {
        System.out.println("usage: java msgsendsample <to> <from> <smtp> true|false");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
