
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.main;

/**
 *
 * @author Administrator
 */
public interface EbankingConfiguration {
    
    public static final String  ACCT_EXIST                   = "0026";
    public static final String  ACCT_NO_EXIST                = "0024";
    public static final String  APP_ERROR_SEARCH             = "0010";
    public static final String  EMAILSERVERERROR             = "0003";
    public static final String  SMSSERVERERROR               = "0909";
    public static final String  ENDOFSESSION                 = "0002";
    public static final String  ERRORSTATUS                  = "E";
    public static final String  ERROR_4_BENGRP_NAME          = "0027";
    public static final String  ERROR_MAP_KEY                = "errorcode";
    public static final String  ERR_ACCTNOISNULL             = "0006";
    public static final String  ERR_CHEQUENOISNULL           = "0008";
    public static final String  ERR_DATABASE                 = "0007";
    public static final String  ERR_IN_USER_CREATION         = "0012";
    public static final String  ERR_TASKIDSCREENIDISNULL     = "0009";
    public static final String  FAILED_TO_INSERT_ACCT        = "0011";
    public static final String  FAIL_PROCESS_STATUS          = "N";
    public static final String  FUND_TRANSFER_ERROR_MESSAGE  = "0019";
    public static final String  FUND_TRANSFER_PIN_MESSAGES   = "0017";
    public static final String  FUND_TRANSFER_SUCESS_MESSAGE = "0018";
    public static final String  GENERAL_ERROR_MESSAGE        = "0020";
    public static final String  GENERAL_FT_ERROR             = "0030";
    public static final String  HOST_NOT_AVAILABLE           = "0016";
    public static final String  MISMATCH_TOKEN               = "0032";
    public static final String  MISSING_FIELD                = "0013";
    public static final String  MODIFY_MESSAGE_TASKID        = "";
    public static final String  NOT_A_VALID_ACCT             = "0025";
    public static final String  NOT_VALIDUSER4ROLE           = "023";
    public static final String  NO_ACCT_TO_ADD               = "0021";
    public static final String  NO_TRANSACTION_TO_AUTHRIZE   = "0022";
    public static final String  OK_PROCESS_STATUS            = "Y";
    public static final int     OK_RESP_STATUS               = 0;
    public static final String  OTHER_REQ_RES                = "otherrequests";
    public static final int     PASSWD_LENGTH                = 6;
    public static final String  RELOGIN                      = "Y";
    public static final String  REQUEST4CHEQUETHEADER        = "0005";
    public static final String  REQUEST4CHEQUETMSG           = "0004";
    public static final String  REQ_FIELD_MISSING            = "0015";
    public static final String  RESULT_MAP_KEY               = "returnResult";
    public static final String  SUCCESSFUL                   = "0014";
    public static final String  SUCCESSSTATUS                = "S";
    public static final String  USERCREATIONFAILED           = "0007";
    public static final String  USERCREATIONTEXT             = "0040";
    public static final String  USERCREATIONTEXTHEADER       = "0041";
    public static final String  USERNOTFOUND                 = "0001";
    public static final boolean USE_SECRET_PASSWD            = false;
    public static final boolean bDebugOn                     = true;
    public static final String  g                            = "0033";
    public boolean              bdebugOn                     = true;
    public static final String  DUPBENACCT                    = "0042";
    public static final String  INVALIDTOKEN                 = "0043";
    public static final String  MAXIMUMTOKENCOUNT            = "0044";
    public static final String   Successful_Transaction      ="10";
    public static final String   System_malfunction          ="96";
    public static final String   Completed_Transaction       ="00";
    
    
    // 20141222
   public static final String   HOLDFUNDERROR      = "0050";
   public static final String   RELEASEFUNDERROR      = "0051";
    public static final String  NIPLFTERROR      = "0052";       // NIP LOCAL FUNDS TRANSFER ERROR  i.e. debit casa,  credit NIP GL
   
}


//~ Formatted by Jindent --- http://www.jindent.com
