/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl;
import com.fasyl.corpIntBankingadmin.bo.FundTransferBo;
import com.fasyl.ebanking.dto.FundsDto;
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.LoadData;
import com.fasyl.ebanking.util.Utility;
import com.net.ExternalCall;
import java.sql.Connection;
import java.util.HashMap;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ProcessEcgRequest extends ProcessClass {

    private static final String WS_URL = "http://80.87.77.121/ecgwebserviceTest/BillsPayment.asmx";
    private Element parent = null;
    private String xmlText = null;
    String flag = null;
    Element parent2;
    private Element parentReceipt = null;
    private String token;

    PreparedStatement stmt;
    Connection con;
    Connection conFCC;

    private String payentRef;
    private String errorMsg;
    String customerName;
    private String cusAcctNo;
    private String ben_acc_number;
    private String ben_acc_name;
    private String bankName;
    private String amount;
    private String user_name;
    private String ecgremarks;
    private String nipTxnCode;
    private String nipChTxnCode;

//  public ProcessEcgRequest() {}
    ExternalCall callService = new ExternalCall();

    private String holdRefNo;

    private static final Logger logger = Logger.getLogger(ProcessEcgRequest.class.getName());

    private FundsDAO fundsDAO;

    private String deSourceCode;

    private String sessionId;
    private String branch;

    private String localCcy = "NGN";

    private String narration;

    private String nipCRGL;

    private String nipVAT;
    private String nipIncome;
    private String nipSetmentBankComm; //For Settlement bank commission
    private String bankCode;
    private String bankDesc;
    private String trxId;
    private String roleId;
    private String auth1;
    private String auth2;
    private String action;
    private String auth1Comment;
    private String auth2Comment;
    FundTransferBo fundTransferBo = new FundTransferBoImpl();
    private int outcome = 0;

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        System.out.println("Inside setInstanceVar method");
        cusAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "accttodebt", 0,
                true, null), "UTF-8").trim();

        logger.info(" cusAcctNo: " + cusAcctNo);

        ben_acc_number = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "bene_acc_no", 0, true, null), "UTF-8");

        logger.info(" ben_acc_number: " + ben_acc_number);

        ben_acc_name = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "bene_acc_name", 0, true, null), "UTF-8");
        bankCode = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "bene_bank",
                0, true, null), "UTF-8");
        //bankCode = bankName;
//        bankName = LoadData.getBankName(bankCode); //get bank name using bank code

        ecgremarks = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ecgremarks", 0, true,
                null), "UTF-8");

        amount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amount", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0, true,
                null), "UTF-8");
        trxId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "trxId", 0,
                true, null), "UTF-8");
        auth1 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth1", 0,
                true, null), "UTF-8");
        auth2 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth2", 0,
                true, null), "UTF-8");
        action = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0,
                true, null), "UTF-8");
        auth1Comment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth1Comment", 0,
                true, null), "UTF-8");
        auth2Comment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth2Comment", 0,
                true, null), "UTF-8");
        roleId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "roleId", 0,
                true, null), "UTF-8");
        System.out.println("transaction id: " + trxId);
        payentRef = inst_data.requestId;

        parent = new Element("ROWSET");

        customerName = inst_data.custId;
        user_name = inst_data.userName;

        branch = (String) LoadBasicParam.getDataValuenew("DEBRANCH");

        logger.info(" branch: " + branch);

        deSourceCode = (String) LoadBasicParam.getDataValuenew("DESOURCECODE");

        logger.info(" deSourceCode: " + deSourceCode);

        sessionId = inst_data.sessionId;

        narration = ecgremarks;

        logger.info(narration);

        //Modified by Ayo to support all specified GL for VAT, Income, Settlement Bank etc
        nipCRGL = (String) LoadBasicParam.getDataValuenew("NIP_CR_GL");

        nipVAT = (String) LoadBasicParam.getDataValuenew("NIP_VAT");
        nipIncome = (String) LoadBasicParam.getDataValuenew("NIP_INCOME");
        nipSetmentBankComm = (String) LoadBasicParam.getDataValuenew("NIP_SETMNT_BANK_COMM");
        nipTxnCode = (String) LoadBasicParam.getDataValuenew("NIP_TXN_CODE");
        nipChTxnCode = (String) LoadBasicParam.getDataValuenew("NIP_CH_TXN_CODE");

        logger.info(" NIP CR GL: " + nipCRGL);

        logger.info("Amount:" + amount);
        logger.info(" NIP VAT: " + nipVAT);
        logger.info(" NIP INCOME: " + nipIncome);
        logger.info(" NIP SETTLEMENT BANK COMMISSION: " + nipSetmentBankComm);
        logger.info(" nipTxnCode: " + nipTxnCode);
        logger.info(" nipCHTxnCode: " + nipChTxnCode);

    }

    protected void initializeVar() {
        System.out.println("Inside initializeVar method");
        flag = null;
        cusAcctNo = null;
        ben_acc_number = null;
        ben_acc_name = null;
        bankName = null;
        amount = null;

        ecgremarks = null;
        user_name = null;
        customerName = null;
        token = null;
        payentRef = null;

    }

    @Override
    public HashMap processRequest(DataofInstance instData) {

        //parent = new Element("ROWSET");
        HashMap hashMap = new HashMap();
        boolean results = true;
        FundsDto fundsDto = new FundsDto();
        FundTransferBo fundTransferBo = new FundTransferBoImpl();

        String responseCode = "";

        try {
            initializeVar();
            setInstanceVar(instData);
            results = validatePin(instData.userId, token);

            if (!action.equalsIgnoreCase("DECLINED")) {
                if (results) {
                    if (instData.taskId == "7016" || instData.taskId.equals("7016")) {
                        instData.result = verifyRequest(instData);
                        return instData.result;
                    }
//                setInstanceVar(instData);
                    updateToken(instData.userId); //force the token to expire after each submission of ft
                    //update the users' comment and action. If action is DECLINED, dont process the transaction 
                    switch (roleId) {
                        case "Low Level Authorizer":
                            outcome = fundTransferBo.processFTByAuth1(auth1, action, auth1Comment, trxId);
                            break;
                        case "High Level Authorizer":
                            outcome = fundTransferBo.processFTByAuth2(auth2, action, auth2Comment, trxId);
                            break;
                    }
                    logger.info(" ******Inside process request **** ");

                    //Calculate total sum of money to be debitted, ensuring all amounts are numeric
                    BigDecimal total = new BigDecimal(amount = amount.replace(",", "").trim())
                            .add(new BigDecimal(nipIncome = nipIncome.replace(",", "").trim()))
                            .add(new BigDecimal(nipSetmentBankComm = nipSetmentBankComm.replace(",", "").trim()))
                            .add(new BigDecimal(nipVAT = nipVAT.replace(",", "").trim())
                            );

                    logger.info("Amount Plus Charge: " + total);
                    logger.info("Amount:" + amount);
                    logger.info("nipIncome:" + nipIncome);
                    logger.info("nipSetmentBankComm:" + nipSetmentBankComm);
                    logger.info("nipVAT:" + nipVAT);

                    instData.result = null;

                    conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
                    fundsDAO = new FundsDAO(conFCC);

                    //Step 1: Hold funds
                    results = holdFunds(cusAcctNo, String.valueOf(total));

                    if (!results) {
                        //Modified by Ayo put if stmt
                        if (fundsDAO.getErrorCode().equalsIgnoreCase("EB-202")) {
                            responseCode = g; //Insufficient funds
                        } else {
                            responseCode = HOLDFUNDERROR;
                        }

                        logger.info(" response code: " + responseCode);

                        hashMap.put(ERROR_MAP_KEY, responseCode);
                        instData.result = hashMap;
                        instData.result = Utility.processError(instData, responseCode);
                        logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);
                        updateToken(instData.userId); //force the token to expire after each submission of ft
                        return instData.result;
                    }

                    //Step 2: Release funds
                    results = releaseFunds(holdRefNo, cusAcctNo);

                    if (!results) {
                        responseCode = RELEASEFUNDERROR;     //  "0051";

                        logger.info(" response code: " + responseCode);

                        hashMap.put(ERROR_MAP_KEY, responseCode);
                        instData.result = hashMap;
                        instData.result = Utility.processError(instData, responseCode);
                        logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);
                        updateToken(instData.userId); //force the token to expire after each submission of ft
                        return instData.result;
                    }

                    logger.info("debit customer account, credit NIP CR GL >>>> ");

                    //Step 3: debit customer account, credit NIP CR GL
                    fundsDto = fundsTransfer(deSourceCode, sessionId, branch, nipCRGL, cusAcctNo, instData.custId, localCcy, amount, narration);

                    if (!fundsDto.isResult()) {

                        responseCode = NIPLFTERROR;     //  "0052";

                        logger.info(" response code: " + responseCode);

                        hashMap.put(ERROR_MAP_KEY, responseCode);
                        instData.result = hashMap;
                        instData.result = Utility.processError(instData, responseCode);
                        logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);
                        updateToken(instData.userId); //force the token to expire after each submission of ft
                        return instData.result;

                    }

                    System.out.println("result after debit customer account, credit NIP CR GL >>>> " + results);

                    //Step 4: Remove NIP charges and put in several GLs as maintained
                    results = removeNIPCharges(deSourceCode, sessionId, branch, cusAcctNo, instData.custId, localCcy);
                    if (!results) {

                        responseCode = NIPLFTERROR;     //  "0052";

                        logger.info(" response code for NIP charges: " + responseCode);

                        hashMap.put(ERROR_MAP_KEY, responseCode);
                        instData.result = hashMap;
                        instData.result = Utility.processError(instData, responseCode);
                        logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);
                        updateToken(instData.userId); //force the token to expire after each submission of ft
                        return instData.result;

                    }

                    //bankCode = LoadData.getBankCode(bankName);
                    logger.info(" NIBBS call  >>>>  ");

                    bankName = bankCode;
                    bankCode = LoadData.getBankCode(bankName);
                    System.out.println("bank code: " + bankCode);
                    System.out.println("bank name: " + bankName);
                    //Step 5: Call NIP web service to credit beneficiary 
                    responseCode
                            = callService.doNibbsTransfer(bankCode, "1", ben_acc_name,
                                    ben_acc_number, user_name, ecgremarks, payentRef, amount, cusAcctNo);

                    //Code below for test environment to Simulate delay and response code -- Ayomide
//            try {
//                Thread.sleep(500);
//            }catch(InterruptedException iExc) {
//                iExc.printStackTrace();
//            }
//            responseCode = "01";
                    //Test code ends here
                    logger.info("The response code is >>>>  " + responseCode);

                    if (!(responseCode.equalsIgnoreCase(Completed_Transaction)) && !(responseCode.equalsIgnoreCase(System_malfunction))
                            && !(responseCode.equalsIgnoreCase("System_malfunction"))) {

                        logger.info(" NIP FAILED !");

                        try {
                            //conFCC.rollback();
                            //reverse the debits
                            fundsDto = fundsTransfer_reverse(deSourceCode, sessionId, branch, nipCRGL, cusAcctNo, instData.custId, localCcy, amount, narration);
                            if (!fundsDto.isResult()) {
                                responseCode = NIPLFTERROR;     //  "0052";

                                logger.info(" fund transfer reverse error response code: " + responseCode);

                                hashMap.put(ERROR_MAP_KEY, responseCode);
                                instData.result = hashMap;
                                instData.result = Utility.processError(instData, responseCode);
                                logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);

                                return instData.result;
                            }
                            results = reverse_removeNIPCharges(deSourceCode, sessionId, branch, cusAcctNo, instData.custId, localCcy);
                            if (!results) {
                                responseCode = NIPLFTERROR;     //  "0052";

                                logger.info(" response code for reverse NIP charges: " + responseCode);

                                hashMap.put(ERROR_MAP_KEY, responseCode);
                                instData.result = hashMap;
                                instData.result = Utility.processError(instData, responseCode);
                                logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);

                                return instData.result;
                            }

                        } catch (SQLException sqlExc) {
                            sqlExc.printStackTrace();
                        }
                        hashMap.put(ERROR_MAP_KEY, responseCode);
                        instData.result = hashMap;
                        instData.result = Utility.processError(instData, responseCode);
                        logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, responseCode);
                        updateToken(instData.userId); //force the token to expire after each submission of ft
                        return instData.result;

                    }

                    logger.info(" NIP SUCCESSFUL!");
                    //If successful, commit txn
                    try {
                        conFCC.commit();
                        boolean updatePostStatusByTrxId = fundTransferBo.updatePostStatusByTrxId(trxId);
                        System.out.println("result of updating post status: " + updatePostStatusByTrxId);
                        logger.info("Txn committed");
                    } catch (SQLException sqlExc) {
                        sqlExc.printStackTrace();
                    }

                    logger.info(" generate success response xml ");
                    System.out.println("bank code: " + bankCode);
                    //bankName = LoadData.getBankName(bankCode);
                    System.out.println("bank name " + bankName);
                    Element row = new Element("ROW");

                    Element custAcc = new Element("CUSTACC");
                    Element benACC = new Element("BENACC");
                    Element benBank = new Element("BENBANK");
                    Element benAccName = new Element("BENACCNAME");
                    Element el_amount = new Element("AMOUNT");
                    Element remarks = new Element("REMARKS");
                    Element success = new Element("SUCCESS");
                    Element el_txn_ref_no = new Element("REFNO");

                    custAcc.setText((String) cusAcctNo);
                    benACC.setText((String) ben_acc_number);
                    benBank.setText((String) bankName);
                    benAccName.setText((String) ben_acc_name);
                    el_amount.setText((String) amount);
                    remarks.setText((String) ecgremarks);
                    success.setText((String) LoadEBMessages.getMessageValue(Completed_Transaction).getMessgaedesc());
                    el_txn_ref_no.setText(fundsDto.getTxn_ref_no());

                    row.addContent(custAcc);
                    row.addContent(benBank);
                    row.addContent(benACC);
                    row.addContent(benAccName);
                    //row.addContent(benBank);
                    row.addContent(el_amount);
                    row.addContent(remarks);
//            
                    row.addContent(success);
                    row.addContent(el_txn_ref_no);

                    parent.addContent(row);
                    instData.result = this.generateXML(parent, xmlText); //parent

                    logSucessfullNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef, amount, FundsDAO.txn_ref_no);
                    updateToken(instData.userId); //force the token to expire after each submission of ft
                    System.out.println("xml text before leaving processReq method:" + xmlText);
                } else {
                    instData.result = Utility.processError(instData, MISMATCH_TOKEN);
                    logFailedNipTransactions(cusAcctNo, bankCode, ben_acc_name, ben_acc_number, user_name, ecgremarks, payentRef,
                            amount, responseCode);
                    updateToken(instData.userId); //force the token to expire after each submission of ft
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
            return instData.result;
        } finally {
            if (conFCC != null) {
                try {
                    conFCC.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;

    }

    public void logSucessfullNipTransactions(String ft_debit_acc, String bankname, String ben_acc_name, String ben_acc_number, String user_name, String naration, String payentRef, String amount, String txn_ref_no) {

        con = com.fasyl.ebanking.db.DataBase.getConnection();

        try {
            con.setAutoCommit(false);
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }
        System.out.println("txn ref no in log successfull nip txns: " + txn_ref_no);
        try {
            stmt = con.prepareStatement("insert into nip_successful_transactions(DestinationBankCode,Bene_acc_name,Bene_acc_number,Sender_acc_name,Naration,PaymentRefNo, Amount, Transaction_Time, Txn_Ref_No, ft_debit_acc)values(?,?,?,?,?,?,?,?,?, ?) ");

            stmt.setString(1, bankname);
            stmt.setString(2, ben_acc_name);
            stmt.setString(3, ben_acc_number);
            stmt.setString(4, user_name);
            stmt.setString(5, naration);
            stmt.setString(6, payentRef);
            stmt.setString(7, amount);
            stmt.setString(8, Utility.getsqlCurrentDate2());
            stmt.setString(9, txn_ref_no);
            stmt.setString(10, ft_debit_acc);
            stmt.execute();
            System.out.println("about to commit successful NIP log");
            con.commit();

        } catch (Exception ex) {
            ex.printStackTrace();

            // return instData.result;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        //  return instData.result;
    }

    //
    public void logFailedNipTransactions(String ft_debit_acc, String bankname, String ben_acc_name, String ben_acc_number, String user_name, String naration, String payentRef, String amount, String reason) {

        con = com.fasyl.ebanking.db.DataBase.getConnection();
        try {
            con.setAutoCommit(false);
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }

        try {
            stmt = con.prepareStatement("insert into Nip_failed_transactions(DestinationBankCode,Bene_acc_name,Bene_acc_number,Sender_acc_name,Naration,PaymentRefNo, Amount, Transaction_Time,Reasons, ft_debit_acc)values(?, ?,?,?,?,?,?,?,?,?) ");

            stmt.setString(1, bankname);
            stmt.setString(2, ben_acc_name);
            stmt.setString(3, ben_acc_number);
            stmt.setString(4, user_name);
            stmt.setString(5, naration);
            stmt.setString(6, payentRef);
            stmt.setString(7, amount);
            stmt.setString(8, Utility.getsqlCurrentDate2());
            stmt.setString(9, reason);
            stmt.setString(10, ft_debit_acc);
            stmt.execute();
            System.out.println("about to commit failed NIP log");
            con.commit();

        } catch (Exception ex) {
            ex.printStackTrace();

            // return instData.result;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        //  return instData.result;
    }

    protected HashMap generateXML(Element parent, String xmlText) {
        HashMap hashMap = null;
        org.jdom.Document doc = new org.jdom.Document(parent);
        XMLOutputter outputter = new XMLOutputter();

        outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
        xmlText = outputter.outputString(doc);
        System.out.println(xmlText);
        hashMap = new HashMap();
        hashMap.put(RESULT_MAP_KEY, xmlText);
        //  return xmlText;
        return hashMap;
    }

    HashMap verifyRequest(DataofInstance instData) {

        HashMap result = null;
        try {
            initializeVar();
            setInstanceVar(instData);
            System.out.println("account name is " + this.ben_acc_name);
            System.out.println("account no is " + this.ben_acc_number);
            // parent  = new Element("ROWSET");
            Element row = new Element("ROW");

            Element customeraccount = new Element("ACCOUNT_NUMBER");
            customeraccount.setText(this.cusAcctNo);
            row.addContent(customeraccount);

            Element benaccountNumber = new Element("BENEFICIARY_ACCOUNT");
            benaccountNumber.setText(this.ben_acc_number);
            row.addContent(benaccountNumber);

            Element benaccountName = new Element("BENEFICIARY_ACCOUNT_NAME");
            benaccountName.setText(this.ben_acc_name);
            row.addContent(benaccountName);

            bankName = LoadData.getBankName(bankCode);
            Element benBank = new Element("BENEFICIARY_BANK");
            benBank.setText(this.bankName);
            row.addContent(benBank);

            Element amount = new Element("AMOUNT");
            amount.setText(this.amount);
            row.addContent(amount);

            Element remarks = new Element("REMARKS");
            remarks.setText(this.ecgremarks);
            row.addContent(remarks);

            Element tokens = new Element("TOKEN");
            tokens.setText(this.token);
            row.addContent(tokens);

            parent.addContent(row);

            result = generateXML(parent, this.xmlText);
            System.out.println("xml text after verifying the request: " + xmlText);
        } catch (Exception ex) {
            Logger.getLogger(ExternalCall.class.getName()).log(Level.ERROR, null, ex);
        }
        return result;
    }

    private boolean holdFunds(String accountNo, String amount) throws SQLException {

        logger.info(" ****** holdFunds  **** ");

        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        //fundsDAO = new FundsDAO(conFCC);
        boolean result = fundsDAO.holdFunds(accountNo, new BigDecimal(amount), "HOLD FUND FOR NIP");
        holdRefNo = fundsDAO.getHoldNo();

        //conFCC.close();
        return result;
    }

    private boolean releaseFunds(String holdRefNo, String accountNo) throws SQLException {
        logger.info(" ****** releaseFunds  **** ");

        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        //fundsDAO = new FundsDAO(conFCC);
        boolean result = fundsDAO.releaseFund(holdRefNo, accountNo);

        //conFCC.close();
        return result;
    }

    private FundsDto fundsTransfer(String deSourceCode, String sessionId, String branch, String benacct, String ftdebitacc, String custId, String ccy, String amt, String narration) throws SQLException {
        System.out.println("deSourceCode: " + deSourceCode);
        System.out.println("sessionId: " + sessionId);
        System.out.println("branch: " + branch);
        System.out.println("ftdebitacc: " + ftdebitacc);
        System.out.println("benacct: " + benacct);
        System.out.println("custId: " + custId);
        System.out.println("amt: " + amt);
        System.out.println("ccy: " + ccy);
        System.out.println("narration: " + narration);
        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();

        //fundsDAO = new FundsDAO(conFCC);
        FundsDto fundsDto = new FundsDto();
        fundsDto = fundsDAO.fundTransfer(deSourceCode, sessionId, branch, benacct, ftdebitacc, custId, ccy, amt, narration, nipTxnCode);

        //conFCC.close();
        return fundsDto;
    }

    private FundsDto fundsTransfer_reverse(String deSourceCode, String sessionId, String branch, String benacct, String ftdebitacc, String custId, String ccy, String amt, String narration) throws SQLException {
        System.out.println("deSourceCode: " + deSourceCode);
        System.out.println("sessionId: " + sessionId);
        System.out.println("branch: " + branch);
        System.out.println("ftdebitacc: " + ftdebitacc);
        System.out.println("benacct: " + benacct);
        System.out.println("custId: " + custId);
        System.out.println("amt: " + amt);
        System.out.println("ccy: " + ccy);
        System.out.println("narration: " + narration);
        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();

        //fundsDAO = new FundsDAO(conFCC);
        narration = "REVERSE " + narration;
        amt = "-" + amt;
        FundsDto fundsDto = new FundsDto();
        fundsDto = fundsDAO.fundTransfer(deSourceCode, sessionId, branch, benacct, ftdebitacc, custId, ccy, amt, narration, nipTxnCode);

        //conFCC.close();
        return fundsDto;
    }

    private boolean removeNIPCharges(String deSourceCode, String sessionId, String branch, String ftdebitacc, String custId, String ccy) throws SQLException {

        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        //fundsDAO = new FundsDAO(conFCC);
        boolean result = fundsDAO.debitNIPCharges(deSourceCode, sessionId, branch, ftdebitacc, custId, ccy, nipChTxnCode);

        //conFCC.close();
        return result;
    }

    private boolean reverse_removeNIPCharges(String deSourceCode, String sessionId, String branch, String ftdebitacc, String custId, String ccy) throws SQLException {

        //conFCC = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        //fundsDAO = new FundsDAO(conFCC);
        boolean result = fundsDAO.reverse_debitNIPCharges(deSourceCode, sessionId, branch, ftdebitacc, custId, ccy, nipChTxnCode);

        //conFCC.close();
        return result;
    }

    private void updateToken(String userId) {
        System.out.println("user id for updating token: " + userId);
        Connection con = null;
        PreparedStatement stmt = null;
        String sqlString = "update sm_user_access set pin_to_expire = ? where cod_usr_id = ?";
        boolean result = false;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            stmt = con.prepareStatement(sqlString);

            stmt.setString(1, "0");
            stmt.setString(2, userId);
            result = stmt.execute();

        } catch (Exception e) {
            System.out.println("result after updating token: " + result);
            System.err.println("error updating token >>> " + e.getMessage());
        }
    }

}
