
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class CustomerAccountno extends Processes {
    
     private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CustomerAccountno.class.getName());
 
    
    private static final String ACCOUNT_NUMBER = "FN_GET_ACCT_NO";
    private static final int    NBR_PARAMS     = 1;
    private String              account;
    private String              chequeAmount;
    private String              custNo;
    private String              idSent;
    PreparedStatement stmt;
    Connection con;
     ResultSet rs ;
    // Connection connect;
    ArrayList       list;
    private HashMap preResult;
    private String  returnResult;

    public CustomerAccountno() {}

    private void initializeVar() {
        this.account      = null;
        this.custNo       = null;
        this.chequeAmount = null;
        this.returnResult = null;
        this.idSent       = null;
        this.preResult    = null;
        
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, true, null);
        this.idSent  =ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "id_sent", 0, true, null);
      
        logger.info("TASK ID IS  "+inst_data.taskId);
       
        //Moved inside the if below by Ayo
//        this.idSent = idSent.toUpperCase();
//       
//        logger.info(" idSent: " + this.idSent);
      
       
        if(inst_data.taskId.equals("5002") || inst_data.taskId.equals("5008"))
        {
            this.idSent = idSent.toUpperCase();
       
            logger.info(" idSent: " + this.idSent);
            this.custNo  =  getCustomerId(this.idSent); 
            
            System.out.println("ID SENT IS "+this.custNo);
        }else{
           this.custNo  = inst_data.custId; 
           System.out.println("CUST ID IS  "+this.custNo);
        }
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            //This is to ensure that a customer number is inputted / derived
            if(this.custNo == null || this.custNo.trim().equals("")) {
                instData.result = Utility.processError(instData, USERNOTFOUND);
            } else {
                connect = com.fasyl.ebanking.db.DataBase.getConnection();
                instData.result = ExchangeMap(connect);

                if (instData.result.equals(APP_ERROR_SEARCH)) {
                    instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                }
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {ex.printStackTrace();}
            }
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {ex.printStackTrace();}
            }
        }

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap       result = new HashMap();
        java.sql.Clob datas  = procStmt.getClob(1);
        String        data   = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        result.put(RESULT_MAP_KEY, returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.ACCOUNT_NUMBER, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(2, this.custNo);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
    
    private String getCustomerId(String user_id){
         String customer_id =null;
        try {
           
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            String query = "select cust_no FROM sm_user_access where cod_usr_id=?";
            stmt = con.prepareStatement(query);

            stmt.setString(1, user_id);
             rs = stmt.executeQuery();
             while(rs.next()){
                 
               customer_id = rs.getString(1);
           }

        } catch (SQLException ex) {
            Logger.getLogger(CustomerAccountno.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {ex.printStackTrace();}
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {ex.printStackTrace();}
            }
        }
          return customer_id;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
