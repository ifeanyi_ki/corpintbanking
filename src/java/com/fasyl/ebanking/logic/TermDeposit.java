/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author baby
 */
public class TermDeposit extends ProcessClass {

     private static final Logger logger = Logger.getLogger(TermDeposit.class.getName());
 
    
    public TermDeposit() {
    }
    private Connection connect = null;
    private Element parent;
    private PreparedStatement pstmt = null;
    DataofInstance instdata;
    private String xmlText = null;
    private String refno = null;
    private static String GET_DEPOSIT_DETAIL = "select a.contract_ref_no,b.counterparty,b.tenor,d.rate,"
            + "b.lcy_amount,b.amount,b.value_date,b.maturity_date,c.total_amount_due,b.dflt_settle_ac "
            + "from cstb_contract@fcc a,ldtb_contract_master@fcc b,LDVW_SCHEDULE_SUMMARY@fcc c,cftb_contract_interest@fcc d, cust_acct e "
            + "where b.product_type='D' and a.contract_status='A' and a.contract_ref_no=b.contract_ref_no and a.contract_ref_no=d.contract_reference_no and b.dflt_settle_ac=e.cust_acct "
            + "and c.contract_ref_no=a.contract_ref_no and b.dflt_settle_ac=e.cust_acct and "
            + " a.latest_version_no = b.version_no and b.counterparty=? and b.contract_ref_no=? and b.version_no =(select max(version_no) from ldtb_contract_master@fcc  where counterparty =? and contract_ref_no=?) and settlement_status='N'";
    public static final String GET_TD_CONTRACT = "select a.contract_ref_no,b.counterparty,b.tenor,d.rate,"
            + "b.lcy_amount,b.amount,b.value_date,b.maturity_date,c.total_amount_due,b.dflt_settle_ac "
            + "from cstb_contract@fcc a,ldtb_contract_master@fcc b,LDVW_SCHEDULE_SUMMARY@fcc c,cftb_contract_interest@fcc d, cust_acct e "
            + "where b.product_type='D' and a.contract_status='A' and a.contract_ref_no=b.contract_ref_no and a.contract_ref_no=d.contract_reference_no and b.dflt_settle_ac=e.cust_acct "
            + "and c.contract_ref_no=a.contract_ref_no and b.dflt_settle_ac=e.cust_acct and "
            + " a.latest_version_no = b.version_no and b.counterparty=? and b.version_no =(select max(version_no) from ldtb_contract_master@fcc f where f.contract_ref_no=b.contract_ref_no) and settlement_status='N'";
    public static final String GET_DETAIL_LOAN_INFO = "select c.contract_ref_no,c.branch,"
            + "c.counterparty,c.currency,c.lcy_amount,a.due_date,a.amount_due,a.original_due_date,a.component, "
            + "a.amount_settled,b.principal_outstanding_bal,c.dr_setl_ac "
            + "from cstb_amount_due@fcc a,LDTB_CONTRACT_BALANCE@fcc b,LDTB_CONTRACT_MASTER@fcc c,cust_acct e "
            + "where a.contract_ref_no=b.contract_ref_no and a.contract_ref_no=c.contract_ref_no and c.dr_setl_ac = e.cust_acct"
            + "and c.contract_status='A' and a.due_date=(select max(due_date) from cstb_amount_due@fcc where counterparty=?' and a.contract_ref_no=?) "
            + "and c.counterparty=? and c.module='LD' and a.component='PRINCIPAL' and c.product_type='L' and a.contract_ref_no=? ";

    protected void initializeVar() {
        pstmt = null;
        refno = null;
        parent = null;

    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent = new Element("ROWSET");
        this.refno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false, null);
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;
        ResultSet rs = null;
        boolean result = false;
        this.initializeVar();
        instdata = instData;
        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }
            this.setInstanceVar(instData);

            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            if (instData.taskId.equals("2061")) {
                if(bDebugOn)System.out.println("<<<<<<<<<<<<<<< >>>>>>>>>>>>>" + refno);
                
                logger.info("query: " + GET_DEPOSIT_DETAIL);
                
                pstmt = connect.prepareStatement(GET_DEPOSIT_DETAIL);
                pstmt.setString(1, instData.custId);
                pstmt.setString(2, refno);
                pstmt.setString(3, instData.custId);
                pstmt.setString(4, refno);
            } else if (instData.taskId.equals("206")) {
                
                 logger.info("query: " + GET_TD_CONTRACT);
               
                
                pstmt = connect.prepareStatement(GET_TD_CONTRACT);
                pstmt.setString(1, instData.custId);//instData.custId);
               // pstmt.setString(2, instData.custId);
            }
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);
            //if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                return instData.result;
            } else {
                result = true;
                if (instData.result == null) {

                    instData.result = generateXML(parent, this.xmlText);
                    if (!result || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding=" + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                    }
                }
            }
        } catch (Exception ex) {
              instData.result = Utility.processError(instData, APP_ERROR_SEARCH);            
             ex.printStackTrace();
             return  instData.result;
        } finally {
            
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
        return instData.result;
    }
}
