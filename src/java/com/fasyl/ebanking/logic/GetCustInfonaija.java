
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class GetCustInfonaija extends ProcessClass {
    private static final String ACCT_NO_QUERY   = "select a.cust_ac_no from sttm_cust_account a where a.cust_no=?";
    private static final String CUST_INFO_QUERY =
        "" + "SELECT a.customer_no, a.customer_name1, a.address_line1 ||' '||a.address_line2||' '||a.address_line3||' '||a.address_line4 as ADDRESS,"
        + " a.LANGUAGE, b.description,c.branch_name,c.branch_code "
        + "FROM sttm_customer a, sttm_country b,sttm_branch c "
        + "where a.country=b.country_code and a.local_branch=c.branch_code and a.customer_no=?";

    // private ResultSetMetaData metaData;
    private String            xmlText = null;
    private String            custno;
    private Element           parent;
    private PreparedStatement pstmt;
    private ResultSet         rs;

    public GetCustInfonaija() {}

    protected void initialiseVar() {
        custno = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        this.custno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "customerno", 0, true, null), "UTF-8");;

        if (bDebugOn) {
            System.out.println(custno + " ==== This is the customer number =====");
        }

        parent = new Element("ROWSET");
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        initialiseVar();

        Connection connect = null;

        try {
            if (bDebugOn) {
                System.out.println("========= inside try block ==========");
            }

            setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt = connect.prepareStatement(CUST_INFO_QUERY);
            pstmt.setString(1, custno);
            rs = pstmt.executeQuery();

            Element row = new Element("ROW");

            parent = this.convertToXml(parent, "ROW", rs);

            // if (bDebugOn)System.out.println(" This is secondary Base "+secondaryBase.toString());
            pstmt  = connect.prepareStatement(ACCT_NO_QUERY);
            pstmt.setString(1, custno);
            row    = new Element("ACCOUNTS");
            rs     = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ACCOUNTS", rs);

            if ((parent == null) || (parent.toString().equalsIgnoreCase("<ROWSET\\>"))) {
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            }

            // parent.addContent(secondaryBase);
            // parent.addContent(accountBase);
            instData.result = generateXML(parent, this.xmlText);
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, "Error while retrieving Customer Info");
            ex.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
