
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class FtSetup extends ProcessClass {
    private static final String INS_SETUP = "insert into ft_setup values(" + "?,?,?,?,?,?,?,?,?,?)";
    private static final String UPD_SETUP =
        "update ft_setup set institution_id=?,"
        + "institution_name=?,max_amount=?,min_amount=?,use_beneficiary=?,modified_date=?,modified_by=? where seq_no=?";
    private String            VIEW_SETUP = "select * from ft_setup ";
    private Connection        connect    = null;
    private PreparedStatement pstmt      = null;
    private String            xmlText    = null;
    private String            ftmaxamount;
    private String            ftminimumamount;
    DataofInstance            instancedata;
    DataofInstance            instdata;
    private String            institutionId;
    private String            institutionName;
    private Element           parent;
    private String            refno;
    private String            usebeneficiaryciaryonly;
    private String            userid;

    public FtSetup() {}

    protected void initializeVar() {
        this.ftmaxamount             = null;
        this.ftminimumamount         = null;
        this.usebeneficiaryciaryonly = null;
        this.userid                  = null;
        this.institutionId           = null;
        this.institutionName         = null;
        this.instancedata            = null;
        refno                        = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        this.institutionId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "institutionId", 0, true, null), "UTF-8");
        this.refno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno",
                0, true, null), "UTF-8");
        this.institutionName = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "institution", 0, true, null), "UTF-8");
        this.ftminimumamount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftminimumamount", 0, true, null), "UTF-8");
        this.ftmaxamount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftmaxamount", 0, true, null), "UTF-8");
        this.usebeneficiaryciaryonly =
            decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "usebeneficiaryciaryonly", 0, true, null), "UTF-8");
        parent            = new Element("ROWSET");
        this.instancedata = instdata;
    }

    private void modifySetup() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        int       result = 0;

        try {
            pstmt = connect.prepareStatement(UPD_SETUP);
            pstmt.setString(1, this.institutionId);
            pstmt.setString(2, this.institutionName);
            pstmt.setString(3, this.ftmaxamount);
            pstmt.setString(4, this.ftminimumamount);
            pstmt.setString(5, this.usebeneficiaryciaryonly);
            pstmt.setTimestamp(6, Utility.getCurrentDate());
            pstmt.setString(7, userid);
            pstmt.setString(8, refno);
            System.out.println(refno + "== This is the sequence number");
            result = pstmt.executeUpdate();
            System.out.println(result + " This is d result");

            if (result > 0) {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation was successful");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            } else {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("Error while changing Fund Transfer setup");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            }

            // parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (connect != null) {
                try {
                    connect.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void addSetup() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        int       result = 0;

        try {
            pstmt = connect.prepareStatement(INS_SETUP);
            pstmt.setString(1, this.institutionId);
            pstmt.setString(2, this.institutionName);
            pstmt.setString(3, this.ftmaxamount);
            pstmt.setString(4, this.ftminimumamount);
            pstmt.setString(5, this.usebeneficiaryciaryonly);
            pstmt.setTimestamp(6, Utility.getCurrentDate());
            pstmt.setString(7, userid);
            pstmt.setTimestamp(8, Utility.getCurrentDate());
            pstmt.setString(9, userid);
            pstmt.setLong(10, System.currentTimeMillis());
            result = pstmt.executeUpdate();

            if (result > 0) {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation was successful");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            }

            // parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (connect != null) {
                try {
                    connect.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        // return parent;
    }

    private void ViewSetup() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        boolean   result = false;

        try {
            if (!((refno == null) || refno.equals(""))) {
                VIEW_SETUP = "select * from ft_setup where seq_no='" + refno + "'";
            }

            pstmt = connect.prepareStatement(VIEW_SETUP);
            System.out.println(VIEW_SETUP);

            // pstmt.setString(1, refno);
            rs     = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        // where seq_no like '?" +"%'"
        ResultSet rs     = null;
        boolean   result = false;

        this.initializeVar();
        instdata = instData;

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);

            if (instData.taskId.equals("60111")) {
                this.addSetup();
            } else if (instData.taskId.equals("33112")) {
                this.modifySetup();
            } else if (instData.taskId.equals("3312") || instData.taskId.equals("3311")
                       || instData.taskId.equals("331") || instData.taskId.equals("33111")) {
                this.ViewSetup();
            }

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result          = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            } else {
                result = true;

                if (instData.result == null) {
                    instData.result = generateXML(parent, this.xmlText);

                    if (!result
                            || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                                   + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {

            if (pstmt != null) {
                try {
                    pstmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
