
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class AccountPosition extends ProcessClass {
    private String accountPosition =
        "SELECT a.cust_no, " + "b.customer_name1, " + " b.customer_type, " + "a.cust_ac_no, "
        + "NVL(a.min_reqd_bal,0.00) min_reqd_bal, " + "NVL(d.limit_amount,0.00) limit_amount, " + "a.ccy, "
        + "a.ac_open_date, " + "NVL(a.acy_uncollected,0.00) acy_uncollected, " + "a.branch_code, " + "c.description, "
        + "a.acy_curr_balance " + " ACY_AVILABLE, "
        + "a.acy_avl_bal "
        + " NET_AVILABLE_BAL, " + "a.acy_blocked_amount, " + "a.ac_stat_dormant, " + "a.ac_stat_block "
        + " FROM acvw_all_ac_entries aa, " + "sttm_cust_account a, "
        + " sttm_customer b, " + "  sttm_account_class c, "
        + "( select sum(decode(b.account_type,'U',a.limit_amount))limit_amount,a.liab_id liab_id,b.cust_ac_no cust_ac_no "
        + "from lmtm_limits a, sttm_cust_account b,sttm_account_class c "
        + "where  a.liab_id=b.cust_no " + "and b.account_class = c.account_class " + "and a.availability_flag ='Y' "
        + "and a.line_cd = 'ODFT_LINE'" + "group by a.limit_amount,a.liab_id,b.cust_ac_no ) d "
        + " WHERE aa.ac_no(+) = a.cust_ac_no " + " and a.cust_no(+) = b.customer_no "
        + "AND a.account_class = c.account_class " + " AND d.cust_ac_no (+) = a.cust_ac_no " + "and aa.ac_ccy = a.ccy "
        + "and aa.cust_gl(+)='A' " + "and aa.ac_no= ? " + "group by " + "a.cust_no," + "b.customer_name1, "
        + "b.customer_type," + "a.cust_ac_no, " + "a.min_reqd_bal," + "a.acy_unauth_uncollected, " + "a.ccy, "
        + "a.ac_open_date, " + "a.branch_code," + "c.description," + "a.ac_stat_dormant," + "a.acy_uncollected,"
        + "a.acy_blocked_amount," + "a.acy_curr_balance," + "d.limit_amount," + "a.acy_unauth_cr,"
        + "a.acy_unauth_uncollected," + "a.reg_cc_available_funds," + "a.acy_tank_cr," + "a.acy_tank_dr,"
        + "a.acy_unauth_tank_cr," + "a.acy_tank_uncollected," + "a.acy_unauth_tank_uncollected,"
        + "a.acy_blocked_amount," + "a.receivable_amount," + "a.acy_accrued_dr_ic, " + "a.ac_stat_block,a.acy_avl_bal";
    private String acctSummaryQry =
        "SELECT DISTINCT a.cust_no, b.customer_name1, b.customer_type,ccy, "
        + "a.cust_ac_no, FN_ACCT_BAL2(a.cust_ac_no,a.branch_code) acy_curr_balance,c.description  "
        + "FROM sttm_cust_account a,sttm_account_class c, "
        + "sttm_customer b,cust_acct d "
        + "WHERE a.cust_no = b.customer_no and a.account_class = c.account_class and d.cust_acct=a.cust_ac_no "
        + " and to_char(a.cust_no)=? ";
    private Connection        connect = null;
    private PreparedStatement pstmt   = null;
    private String            xmlText = null;
    private String            account;
    DataofInstance            instdata;
    private Element           parent;

    protected void initializeVar() {
        account = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent = new Element("ROWSET");

        // acct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false, null), "UTF-8");
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false,
                null);
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        this.initializeVar();
        instdata = instData;

        boolean result = false;

        if (bDebugOn) {
            System.out.println(instData.txnId);
        }

        try {
            setInstanceVar(instData);
            parent = getAccPosition(instData);

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result          = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            } else {
                result = true;

                if (instData.result == null) {
                    instData.result = generateXML(parent, this.xmlText);

                    if (!result
                            || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                                   + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    private Element getAccPosition(DataofInstance instdata) throws Exception {

        ResultSet rs     = null;
        boolean   result = false;

        if ((account == null) || account.equals("")) {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt   = connect.prepareStatement(acctSummaryQry);

            if (bDebugOn) {
                System.out.println(acctSummaryQry);
            }

            pstmt.setString(1, instdata.custId);
        } else {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt = connect.prepareStatement(accountPosition);

            if (bDebugOn) {
                System.out.println(account);
            }

            pstmt.setString(1, account);
        }

        rs     = pstmt.executeQuery();
        parent = this.convertToXml(parent, "ROW", rs);

        if (rs != null) {
            rs.close();
        }

        return parent;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
