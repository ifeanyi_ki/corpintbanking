
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class ManageFraud extends ProcessClass {
    
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ManageFraud.class.getName());
 
    
    private String criteria;
    private String enddate;
    private String startdate;

    public ManageFraud() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        criteria = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "criteria",
                0, true, null), "UTF-8");
        enddate  = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "to", 0,
                true, null), "UTF-8");
        startdate = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "from", 0,
                true, null), "UTF-8");
        
        
        logger.info(" criteria: " + criteria);
        logger.info(" startdate: " + startdate);
        logger.info(" enddate: " + enddate);
        
        
    }

    protected void initializeVar() {
        criteria  = null;
        enddate   = null;
        startdate = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection        con    = null;
        CallableStatement stmt   = null;
        String            result = null;

        initializeVar();

        try {
            setInstanceVar(instData);
            
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            
            logger.info("{?=call fn_fraud_report(?,?,?,?,?,?)}");
            
            stmt = con.prepareCall("{?=call fn_fraud_report(?,?,?,?,?,?)}");
            stmt.registerOutParameter(1, OracleTypes.CLOB);
            stmt.registerOutParameter(6, OracleTypes.VARCHAR);
            stmt.registerOutParameter(7, OracleTypes.VARCHAR);
            stmt.setString(2, instData.requestId);
            stmt.setString(3, criteria);
            stmt.setString(4, startdate);
            stmt.setString(5, enddate);
            stmt.execute();
             if (bDebugOn)System.out.println(stmt.getString(6));
            if (stmt.getString(6)!=null){
               
              instData.result =  this.responseMsg(stmt.getString(6)) ;
              return  instData.result;
            }else{
            result = this.ManageClob(stmt.getClob(1));
            }

            if (result != null) {
                HashMap map = new HashMap();

                //System.out.println(stmt.getString(1));
                map.put(RESULT_MAP_KEY,result);
                instData.result = map;
            } else {
                instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

                return instData.result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
