
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class GetExchangerate extends ProcessClass {
    private String GET_EXCHANGE_RATE = "select ccy1,ccy2,rate_type,buy_rate rate,sale_rate rate2 from CYTM_RATES "
                                       + "where branch_code ='099'" + "and rate_type in ('STANDARD') "
                                       + "order by ccy1";
    private Connection        connect = null;
    private PreparedStatement pstmt   = null;
    private String            xmlText = null;

//  public static final String GET_EXCHANGE_RATE = "select ccy1,ccy2,buy_rate,sale_rate "
//          + "from all_rates where  rate_date=(select max(rate_date) from all_rates) "
//          + "and branch_code='001'";

    DataofInstance            instdata;
    private Element           parent;

    public GetExchangerate() {}

    protected void initializeVar() {
        pstmt = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent = new Element("ROWSET");
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        ResultSet rs     = null;
        boolean   result = false;

        this.initializeVar();
        instdata = instData;

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);
            connect  = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt  = connect.prepareStatement(GET_EXCHANGE_RATE);
            rs     = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result          = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            } else {
                result = true;

                if (instData.result == null) {
                    instData.result = generateXML(parent, this.xmlText);

                    if (!result
                            || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                                   + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
