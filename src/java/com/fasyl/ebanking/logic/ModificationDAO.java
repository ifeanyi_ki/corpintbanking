/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 *
 * @author Ese
 * This class was initially created by Ayo for the purpose of User Modification
 * 
 */
public class ModificationDAO {
    
    Connection conn;
    Statement stmt;
    ResultSet rs;
    String queryString;
    
    /**
     * Parameter: Takes in userID and all modifiable values 
     * Action: Compares values with DB values to detect change
     * Return: Returns an HashMap of modified column names and new values
     */
    public HashMap<String, String> buildModifiedColAndValStr(String userID, String name,
            String email, String phone, String branchName, String roleID) {
        
        
        HashMap<String, String> result = new HashMap<String, String>(3);
        StringBuffer columns = new StringBuffer();
        StringBuffer values = new StringBuffer();
        
        conn = com.fasyl.ebanking.db.DataBase.getConnection();
        queryString = "SELECT * FROM SM_USER_ACCESS WHERE cod_usr_id = '"
                + userID.toUpperCase() + "'";
        try {
            if(conn != null) {
                stmt = conn.createStatement();
                rs = stmt.executeQuery(queryString);
                
                //Check if there is a result. There is only one per userID
                if(rs.next()) {
                    System.out.println("Got a record in Modification");
//                    System.out.println("Old Name: \"  "+ rs.getString("cod_usr_name").trim() +"\"");
//                    System.out.println("New Name: \"  "+ name.trim() +"\"");
//                    System.out.println("Old Mail: \"  "+ rs.getString("email").trim() +"\"");
//                    System.out.println("New Mail: \"  "+ email.trim() +"\"");
//                    System.out.println("Old Phone: \"  "+ rs.getString("phone_no").trim() +"\"");
//                    System.out.println("New Phone: \"  "+ phone.trim() +"\"");
//                    System.out.println("Old cod_usr_branch: \"  "+ rs.getString("cod_usr_branch").trim() +"\"");
//                    System.out.println("New cod_usr_branch: \"  "+ branchName +"\"");
//                    System.out.println("Old Role: \"  "+ rs.getString("cod_role_id").trim() +"\"");
//                    System.out.println("New Role: \"  "+ roleID +"\"");
                    //Manually check for changes
                    if((name != null && rs.getString("cod_usr_name") != null) 
                            && !(name.trim().equals(rs.getString("cod_usr_name").trim()))){
                        columns.append("cod_usr_name");
                        values.append(name.trim());
                    }
                    if((email != null && rs.getString("email") != null) 
                            &&!(email.trim().equals(rs.getString("email").trim()))){
                        if(columns.length() == 0) {
                            columns.append("email");
                            values.append(email.trim());
                        } else {
                            columns.append(",email");
                            values.append(",").append(email.trim());
                        }
                        
                    }
                    if((phone != null && rs.getString("phone_no") != null) 
                            &&!(phone.trim().equals(rs.getString("phone_no").trim()))){
                        if(columns.length() == 0) {
                            columns.append("phone_no");
                            values.append(phone.trim());
                        } else {
                            columns.append(",phone_no");
                            values.append(",").append(phone.trim());
                        }
                        
                    }
                    if((branchName != null && rs.getString("cod_usr_branch") != null) 
                            && !(branchName.trim().equals(rs.getString("cod_usr_branch").trim()))){
                        if(columns.length() == 0) {
                            columns.append("cod_usr_branch");
                            values.append(branchName.trim());
                        } else {
                            columns.append(",cod_usr_branch");
                            values.append(",").append(branchName.trim());
                        }
                        
                    }
                    if((roleID != null && rs.getString("cod_role_id") != null) 
                            && !(roleID.trim().equals(rs.getString("cod_role_id").trim()))){
                        if(columns.length() == 0) {
                            columns.append("cod_role_id");
                            values.append(roleID.trim());
                        } else {
                            columns.append(",cod_role_id");
                            values.append(",").append(roleID.trim());
                        }
                        
                    }
                    
                    //Validate if any changes were found
                    if(columns.length() != 0) {
                        result.put("Columns", columns.toString());
                        result.put("Values", values.toString());
                        result.put("Error", "");
                    } else {
                        result.put("Error", "You have not modified any value, Please check again");
                    }
                } else {//Else for if rs.next()
                    
                    //Application should never get here because user must exist
                    result.put("Error", "Requested User not found");
                } 
            } else { //Else for if conn is null
                System.out.println("Connection in ModificationDAO.getModifiedColAndVal is null ");
                result.put("Error", "Error connecting to DB, Contact DB Admin");
            }
        } catch(SQLException sqlE){
           System.out.println("SQLException in ModificationDAO.getModifiedColAndVal: " + sqlE.getMessage());
           result.put("Error", "Database Error, Contact Database Administrator");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        return result;
    }
    
}
