
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.logic.ProcessClass;
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class AuthoriseAcct extends ProcessClass {

    StringBuffer sSql = null;
    private String action;
    Connection con;
    private String isreject;
    private String par;
    private String refno;
    private String rejcomment;
    private String taskId;
    private String uniqueID;
    private String requesterEmail;
    // private static final PasswordGenerator innerClass = new PasswordGenerator();
    CallableStatement callStmt;
    Statement statement;
    ResultSet resultSet;

    public AuthoriseAcct() {
    }

    protected void initializeVar() {
        action = null;
        refno = null;
        isreject = null;
        rejcomment = null;
        taskId = null;
        uniqueID = null;
        requesterEmail = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        action = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0,
                true, null), "UTF-8");
        refno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0,
                true, null), "UTF-8");
        isreject = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "isreject",
                0, true, null), "UTF-8");
        rejcomment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "rejcomment", 0, true, null), "UTF-8");
        taskId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "taskId",
                0, true, null), "UTF-8");
        uniqueID = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "uniqueID", 0, true, null), "UTF-8");
        requesterEmail = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "requesterEmail",
                0, true, null), "UTF-8");

        // par =  decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "par", 0, true, null), "UTF-8");
        System.out.println(action + " ref action " + refno);
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        int result = 0;

        initializeVar();

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            setInstanceVar(instData);
            con.setAutoCommit(false);

            if (!((refno == null) || (refno.equals("")))) {
                if (bDebugOn) {
                    System.out.println("about to authorise === refno === " + refno + " === isreject " + isreject + " ==== rejcomment === rejcomment" + rejcomment);
                }

                //The details gotten from the DB is for email purpose. Not needed if no requesterEmail
                if (requesterEmail != null && !requesterEmail.trim().equals("")) {
                    if (action.trim().startsWith("Remove an account")) {
                        System.out.println("Remove an acct getName");
                        statement = con.createStatement();
                        resultSet = statement.executeQuery("SELECT a.cod_usr_name FROM sm_user_access a,"
                                + "cust_acct b WHERE b.cust_acct = '" + uniqueID + "'"
                                + "AND a.cust_no = b.custno");
                    }
                }


                callStmt = con.prepareCall("{?=call fn_manage_auth(?,?,?,?,?,?,?,?,?)}");
                callStmt.registerOutParameter(1, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(5, OracleTypes.CLOB);
                callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(7, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(8, OracleTypes.VARCHAR);
                callStmt.setString(2, refno);
                callStmt.setString(3, instData.userId);
                callStmt.setString(4, instData.sessionId);
                callStmt.setString(9, isreject);
                callStmt.setString(10, rejcomment);
                callStmt.execute();
                System.out.println("about to commit");
                con.commit();

                //The details gotten from the DB is for email purpose. Not needed if no requesterEmail
                if (requesterEmail != null && !requesterEmail.trim().equals("")) {
                    if (action.trim().startsWith("Add an account")) {
                        System.out.println("Add an acct getName");
                        statement = con.createStatement();
                        //Fetching from sttm_cust_account because if rejected, no record in cust_acct table for the account
//                    resultSet = statement.executeQuery("SELECT ac_desc FROM sttm_cust_account"
//                            + " WHERE cust_ac_no = '" + uniqueID + "'");
                        resultSet = statement.executeQuery("SELECT a.cod_usr_name FROM sm_user_access a,"
                                + "sttm_cust_account b WHERE b.cust_ac_no = '" + uniqueID + "'"
                                + " AND a.cust_no = b.cust_no");
                    }

                    //This is implemented for the sending of email after approval / Rejection by Ayo
                    String msgHeader, msgBody, toEmail, fromEmail, name;
                    toEmail = requesterEmail;
                    fromEmail = LoadEmails.getDataValuenew("AUTH");
                    if (resultSet.next()) {
                        name = resultSet.getString("cod_usr_name");
                    } else {
                        name = "{Could not fetch name}";
                    }
                    System.out.println("From: " + fromEmail);
                    System.out.println("To: " + toEmail);
                    System.out.println("UniqueID: " + uniqueID);
                    System.out.println("Action: " + action);
                    System.out.println("Request Ref No: " + refno);
                    System.out.println("Reject Comment: " + rejcomment);
                    System.out.println("Name: " + name);

                    msgBody = "Dear Admin, <br /> Please find below, details of your authorization request: <br />"
                            + "RequestID: " + refno + "<br />"
                            + "Name: " + name + "<br />"
                            + "Request Description: " + action + "<br />"
                            + "Account Number: " + uniqueID + "<br />";

                    if (isreject.equalsIgnoreCase("Y")) { //If request was rejected
                        msgHeader = "Rejected | " + action + " | " + uniqueID;
                        msgBody += "Action: Rejected <br />"
                                + "Reason: " + rejcomment + "<br />";
                    } else {
                        msgHeader = "Approved | " + action + " | " + uniqueID;
                        msgBody += "Action: Approved <br />";
                    }

                    msgBody += "<br /> Thank you.";
                    sendmail mailSender = new sendmail();
                    boolean res = mailSender.sendMessage(msgHeader, msgBody, "", "text/html", fromEmail, toEmail);
                    if (res) {
                        System.out.println("Email sent sucessfully");
                    } else {
                        System.out.println("Email could not be sent");
                    }
                }

            }

            callStmt = con.prepareCall("{?=call fn_auth_user(?,?,?,?,?,?)}");
            callStmt.registerOutParameter(1, OracleTypes.VARCHAR);
            callStmt.registerOutParameter(4, OracleTypes.CLOB);
            callStmt.registerOutParameter(5, OracleTypes.VARCHAR);
            callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
            callStmt.registerOutParameter(7, OracleTypes.VARCHAR);
            if (taskId.equals("3003")) { //Remove account list
                callStmt.setString(2, "acctRA");
            } else if (taskId.equals("3004")) { //Add account list
                callStmt.setString(2, "acctAA");
            } else { //This should never happen tho by Ayo
                callStmt.setString(2, "acct");
            }

            callStmt.setString(3, instData.sessionId);
            callStmt.execute();
            result = callStmt.getInt(1);

            if (result == 0) {
                HashMap map = new HashMap();
                java.sql.Clob datas = callStmt.getClob(4);
                String data = datas.getSubString(1, (int) datas.length());

                if (bDebugOn) {
                    System.out.println(data);
                }

                map.put(RESULT_MAP_KEY, data);
                instData.result = map;
            } else {
                instData.result = Utility.processError(instData, callStmt.getString(5));

                return instData.result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
