/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.dto.FundTransferReceiptDto;
import com.fasyl.ebanking.dto.FundsDto;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
//import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

/**
 *
 * @author NYEMIKE
 */
public class FundsDAO {

    private static final Logger logger = Logger.getLogger(FundsDAO.class.getName());
    private Connection tbmbFccCon;
    private String holdNo;
    private String errorCode;
    private String errorMsg;
    static String txn_ref_no;
    FundsDto fundsDto = new FundsDto();
    public FundsDAO() {
    }

    public FundsDAO(Connection tbmbFccCon) {
        this.tbmbFccCon = tbmbFccCon;
        try {
            this.tbmbFccCon.setAutoCommit(false);
        } catch (SQLException sqlExc) {
            sqlExc.printStackTrace();
        }

    }

    public boolean holdFunds(String accountNo, BigDecimal amount, String remarks) {

        boolean result = false;

        //  String str = "begin ? := FCRLIVE.GCP_REPLICATE.FN_REP_FCR_ACCOUNTS(?,?,?,?,?,?,?); end;";
        String str = "begin ? := ibft.fn_hold_fund(?,?,?,?,?); end;";

        logger.info(str);

        CallableStatement cstm = null;

        try {

            cstm = tbmbFccCon.prepareCall(str);

            logger.info("Got after the prepare call");

            cstm.registerOutParameter(1, OracleTypes.INTEGER);
            cstm.setString(2, accountNo);
            cstm.setBigDecimal(3, amount);

            cstm.registerOutParameter(4, OracleTypes.VARCHAR);
            cstm.registerOutParameter(5, OracleTypes.VARCHAR);
            cstm.registerOutParameter(6, OracleTypes.VARCHAR);

            cstm.execute();
            logger.info("Hold FUND Executed");

            int outcome = cstm.getInt(1);

            logger.debug("outcome: " + outcome);

            logger.debug(" Outcome of ibft.fn_hold_fund: " + outcome);

            if (outcome == 0) {

                holdNo = cstm.getString(4);

                result = true;

                logger.info(" result: " + result);

                logger.info(" hold reference no: " + holdNo);

                //tbmbFccCon.commit();

                //logger.info(" commited ! ");

            } else {

                result = false;

                //Added by Ayo
                errorCode = cstm.getString(5);
                errorMsg = cstm.getString(6);

                logger.error(" error code: " + errorCode);
                logger.error(" error message: " + errorMsg);

                tbmbFccCon.rollback();

                logger.info(" rollback ! ");

            }

        } catch (SQLException ex) {

            // tbmbFccCon.rollback();
            logger.error(ex.getMessage());
            ex.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();

        } finally {
            if (cstm != null) {
                try {
                    cstm.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }

        }

        return result;
    }

    public boolean releaseFund(String blockNo, String accountNo) {

        boolean result = false;

        String str = "begin ? := ibft.fn_release_fund(?,?,?,?); end;";

        logger.info(str);

        CallableStatement cstm = null;

        try {

            cstm = tbmbFccCon.prepareCall(str);

            cstm.registerOutParameter(1, OracleTypes.INTEGER);
            cstm.setString(2, blockNo);
            cstm.setString(3, accountNo);
            cstm.registerOutParameter(4, OracleTypes.VARCHAR);
            cstm.registerOutParameter(5, OracleTypes.VARCHAR);

            cstm.execute();

            int outcome = cstm.getInt(1);

            logger.info(" outcome: " + outcome);

            logger.info(" Outcome of ibft.fn_release_fund: " + outcome);

            if (outcome == 0) {
                result = true;

                logger.info(" result: " + result);

                logger.info(" hold reference no: " + blockNo);

                //tbmbFccCon.commit();

                //logger.info(" commited ! ");

            } else {

                //Added by Ayo
                errorCode = cstm.getString(4);
                errorMsg = cstm.getString(5);

                logger.error(" error code: " + errorCode);
                logger.error(" error message: " + errorMsg);

                tbmbFccCon.rollback();

                logger.info(" rollback ! ");
            }

        } catch (SQLException ex) {

            logger.error(ex.getMessage());
            ex.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();

        } finally {
            if (cstm != null) {
                try {
                    cstm.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }

        }

        return result;

    }

    public FundsDto fundTransfer(String deSourceCode, String sessionId, String branch, String benacct, String ftdebitacc, String custId, String ccy, String amt, String narration, String txnCode) {

        boolean result = false;
        fundsDto.setResult(result);

        String str = "{?=call ibft.fn_manage_ft3(?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        logger.info(str);

        CallableStatement cstm = null;

        try {

            cstm = tbmbFccCon.prepareCall(str);
            cstm.registerOutParameter(1, OracleTypes.INTEGER);
            cstm.registerOutParameter(12, OracleTypes.VARCHAR);
            cstm.registerOutParameter(13, OracleTypes.VARCHAR);
            cstm.registerOutParameter(14, OracleTypes.VARCHAR);
            cstm.setString(2, deSourceCode);
            cstm.setString(3, sessionId);
            cstm.setString(4, branch);
            cstm.setString(5, benacct);    // bene grp
            cstm.setString(6, ftdebitacc);
            //  statement.setString(7, DestAcctNo);   
            cstm.setString(7, custId);
            cstm.setString(8, ccy);
            cstm.setString(9, amt);
            cstm.setString(10, txnCode);
            cstm.setString(11, narration);


            System.out.println("deSourceCode>>>" + deSourceCode);
            System.out.println("sessionId>>>" + sessionId);
            System.out.println("branch>>>" + branch);
            System.out.println("benacct>>>" + benacct);
            System.out.println("ftdebitacc>>>" + ftdebitacc);
            System.out.println("custId>>>" + custId);
            System.out.println("ccy>>>" + ccy);
            System.out.println("amt>>>" + amt);
            System.out.println("txnCode>>>" + txnCode);
            System.out.println("narration>>>" + narration);
            cstm.execute();

            int outcome = cstm.getInt(1);

            logger.info(" outcome: " + outcome);

            txn_ref_no = cstm.getString(12);
            errorCode = cstm.getString(13);
            errorMsg = cstm.getString(14);

            if (txn_ref_no != null) {
                fundsDto.setTxn_ref_no(txn_ref_no);
                System.out.println("txn_ref_no: " + txn_ref_no);
                //setTxn_ref_no(txn_ref_no);
            } else {
                System.out.println("txn_ref_no: " + txn_ref_no);
            }
            logger.error(" error code: " + errorCode);
            logger.error(" error message: " + errorMsg);


            logger.info(" Outcome of ibft.fn_manage_ft3: " + outcome);

            if (outcome == 0) {
                result = true;
                fundsDto.setResult(result);
                logger.info(" result: " + result);

                tbmbFccCon.commit();

                logger.info(" txn commited ! ");



            } else {

                //Added by Ayo
                errorCode = cstm.getString(12);
                errorMsg = cstm.getString(13);

                logger.error(" error code: " + errorCode);
                logger.error(" error message: " + errorMsg);

                tbmbFccCon.rollback();

                logger.info(" txn rollback ! ");
            }

        } catch (SQLException ex) {

            logger.error(ex.getMessage());
            ex.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();

        } finally {
            if (cstm != null) {
                try {
                    cstm.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }

        }

        return fundsDto;

    }

    public boolean debitNIPCharges(String deSourceCode, String sessionId, String branch, String ftdebitacc, String custId, String ccy, String txnCode) {
        System.out.println("deSourceCode: " + deSourceCode);
        System.out.println("sessionId: " + sessionId);
        System.out.println("branch: " + branch);
        System.out.println("ftdebitacc: " + ftdebitacc);
        System.out.println("custId: " + custId);
        System.out.println("ccy: " + ccy);
        System.out.println("txnCode: " + txnCode);

        boolean result = false;

        String str = "{?=call ibft.fn_debit_NIP_charges(?,?,?,?,?,?,?,?,?)}";

        logger.info(str);

        CallableStatement cstm = null;

        try {

            cstm = tbmbFccCon.prepareCall(str);
            cstm.registerOutParameter(1, OracleTypes.INTEGER);
            cstm.registerOutParameter(9, OracleTypes.VARCHAR);
            cstm.registerOutParameter(10, OracleTypes.VARCHAR);
            cstm.setString(2, deSourceCode);
            cstm.setString(3, sessionId);
            cstm.setString(4, branch);
            cstm.setString(5, ftdebitacc);
            cstm.setString(6, custId);
            cstm.setString(7, ccy);
            cstm.setString(8, txnCode);

            cstm.execute();

            int outcome = cstm.getInt(1);
            System.out.println("outcome after debitNIPCharges: " + outcome);
            logger.info(" outcome: " + outcome);

            logger.info(" Outcome of ibft.fn_debit_NIP_charges: " + outcome);

            if (outcome == 0) {
                result = true;

                logger.info(" result: " + result);

                tbmbFccCon.commit();

                logger.info(" txn commited ! ");

            } else {

                //Added by Ayo
                errorCode = cstm.getString(9);
                errorMsg = cstm.getString(10);

                logger.error(" error code: " + errorCode);
                logger.error(" error message: " + errorMsg);

                tbmbFccCon.rollback();

                logger.info(" txn rollback ! ");
            }

        } catch (SQLException ex) {

            logger.error(ex.getMessage());
            ex.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();

        } finally {
            if (cstm != null) {
                try {
                    cstm.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }

        }

        return result;

    }

    public boolean reverse_debitNIPCharges(String deSourceCode, String sessionId, String branch, String ftdebitacc, String custId, String ccy, String txnCode) {
        System.out.println("deSourceCode: " + deSourceCode);
        System.out.println("sessionId: " + sessionId);
        System.out.println("branch: " + branch);
        System.out.println("ftdebitacc: " + ftdebitacc);
        System.out.println("custId: " + custId);
        System.out.println("ccy: " + ccy);
        System.out.println("txnCode: " + txnCode);

        boolean result = false;

        String str = "{?=call ibft.fn_rev_debit_NIP_charges(?,?,?,?,?,?,?,?,?)}";

        logger.info(str);

        CallableStatement cstm = null;

        try {

            cstm = tbmbFccCon.prepareCall(str);
            cstm.registerOutParameter(1, OracleTypes.INTEGER);
            cstm.registerOutParameter(9, OracleTypes.VARCHAR);
            cstm.registerOutParameter(10, OracleTypes.VARCHAR);
            cstm.setString(2, deSourceCode);
            cstm.setString(3, sessionId);
            cstm.setString(4, branch);
            cstm.setString(5, ftdebitacc);
            cstm.setString(6, custId);
            cstm.setString(7, ccy);
            cstm.setString(8, txnCode);

            cstm.execute();

            int outcome = cstm.getInt(1);
            System.out.println("outcome after debitNIPCharges: " + outcome);
            logger.info(" outcome: " + outcome);

            logger.info(" Outcome of ibft.fn_debit_NIP_charges: " + outcome);

            if (outcome == 0) {
                result = true;

                logger.info(" result: " + result);

                tbmbFccCon.commit();

                logger.info(" txn commited ! ");

            } else {

                //Added by Ayo
                errorCode = cstm.getString(9);
                errorMsg = cstm.getString(10);

                logger.error(" error code: " + errorCode);
                logger.error(" error message: " + errorMsg);

                tbmbFccCon.rollback();

                logger.info(" txn rollback ! ");
            }

        } catch (SQLException ex) {

            logger.error(ex.getMessage());
            ex.printStackTrace();

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();

        } finally {
            if (cstm != null) {
                try {
                    cstm.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }

        }

        return result;

    }
    public FundTransferReceiptDto getFundTransferParticulars(String refNo) {
        FundTransferReceiptDto ftDto = new FundTransferReceiptDto();
        String destBankCode = "";
        Connection connect = null;
        System.out.println("refno in getFundTransferParticulars: " + refNo);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String queryString = "select * from nip_successful_transactions where txn_ref_no=?";
        String bankNameQueryStr = "select * from bank_list where bank_code=?";

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = connect.prepareStatement(queryString);
            pstmt.setString(1, refNo);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                ftDto.setBenAccName(rs.getString("bene_acc_name"));
                destBankCode = rs.getString("destinationbankcode");
                ftDto.setAmount(rs.getString("amount"));
                ftDto.setNarration(rs.getString("naration"));
                
                ftDto.setBene_acc_number(rs.getString("bene_acc_number"));
                ftDto.setTransactionTime(rs.getString("transaction_time"));
                ftDto.setCustomerAccName(rs.getString("sender_acc_name"));
            }

            //get bank name from destination bank code
            pstmt = connect.prepareStatement(bankNameQueryStr);
            pstmt.setString(1, destBankCode);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                ftDto.setBenBankName(rs.getString("bank_name"));
            }

        } catch (Exception e) {
            System.err.println("error in getting fund transfer particulars: " + e.getMessage());
        } finally {
            try {
                if (!connect.isClosed()) {
                    connect.close();
                }
            } catch (SQLException e) {
                System.out.println("error in closing connect: " + e.getMessage());
            }
        }

        return ftDto;
    }

    public FundTransferReceiptDto getThirdPartyFTReceiptParticulars(String refNo) {
        FundTransferReceiptDto ftDto = new FundTransferReceiptDto();
        Connection connect = null;
        System.out.println("refno in getFundTransferParticulars: " + refNo);
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String queryString = "select * from fundtransfer_log where txn_ref_no=?";

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = connect.prepareStatement(queryString);
            pstmt.setString(1, refNo);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                ftDto.setAmount(rs.getString("amount"));
                ftDto.setNarration(rs.getString("narration"));
                ftDto.setDebitAcc(rs.getString("ft_debit_acc"));
                ftDto.setCreditAcc(rs.getString("dest_acct_no"));
                ftDto.setTimeStamp(getFormattedDate(rs.getTimestamp("transaction_time").toString().substring(0, 10)));
                ftDto.setCustomerAccName(rs.getString("account_name"));
                ftDto.setLFTindicator(rs.getString("lft"));
            }
        } catch (Exception e) {
            System.err.println("error in getting fund transfer particulars: " + e.getMessage());
        } finally {
            try {
                if (!connect.isClosed()) {
                    connect.close();
                }
            } catch (SQLException e) {
                System.out.println("error in closing connect: " + e.getMessage());
            }
        }
        return ftDto;
    }

    public String getFormattedDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Date dateStr = null;
        try {
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateStr = dFormat.parse(date);

        } catch (ParseException ex) {
            System.out.println("error in formating date: " + ex.getMessage());
        }
        return sdf.format(dateStr);
    }

    /**
     * @return the holdNo
     */
    public String getHoldNo() {
        return holdNo;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
