
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class ManageAppParameters extends ProcessClass {
    Connection connect;
    String     desc;
    String     failiurecount;
    String     idletime;
    Element    parent;
    String     setup;
    String     xmlText;

    public ManageAppParameters() {}

    protected void initializeVar() {
        parent        = null;
        connect       = null;
        idletime      = null;
        failiurecount = null;
        xmlText       = null;
        setup         = null;
        desc          = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent   = new Element("ROWSET");
        //connect  = null;//com.fasyl.ebanking.db.DataBase.getConnection();
        idletime = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "idletime",
                0, true, null), "UTF-8");
        failiurecount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "failiurecount", 0, true, null), "UTF-8");
        setup = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "setup", 0,
                true, null), "UTF-8");
        desc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "desc", 0,
                true, null), "UTF-8");
        

        if (bDebugOn) {
            System.out.println(idletime + " This is the result " + failiurecount);
        }
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        int result = 0;

        initializeVar();

        try {
            setInstanceVar(instData);

            if (instData.taskId.equals("8041")) {
                result = updateParameter(instData);

                if ((result == 0)) {
                    LoadBasicParam.loadSpecificData("IDLETIME");
                    LoadBasicParam.loadSpecificData("FAILIURE_COUNT");
                    instData.result = this.responseMsg("The application parameter updated successfully");
                } else {
                    instData.result = responseMsg("Error while updating parameter!!");
                }
            }

            if (instData.taskId.equals("802")) {
                instData.result = getAppParameters("SIPRODUCTCODE");
            }

//            if (instData.taskId.equals("801")) {
//                instData.result = getAppParameters();
//            }

            if (instData.taskId.equals("8021")) {
                if(bDebugOn)System.out.println(desc +" ========setup======= "+setup);
                instData.result = updateparam("U",instData.userId,desc,setup);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return instData.result;
    }

    public HashMap createAffilateSetup() {
        String query =
            "insert into eb_affiliate_setup (AFFILIATE_CODE,AFFILIATE_SETUP,COUNTRY_CODE,COUNTRY_NAME,ADDED_BY,ADDED_DATE,MODIFIED_BY,MODIFIED_DATE)values(?,?,?,?,?,sysdate,?,sysdate)";
        PreparedStatement pstmt = null;
        int               rs    = -1;
        HashMap           map   = null;

        connect = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt   = connect.prepareStatement(query);
            rs      = pstmt.executeUpdate();
            if(bDebugOn)System.out.println(" ========rs======= "+rs);
            if ((rs > 0)) {
               map =  responseMsg("The application parameter successfully updated");
            } else {
                map =responseMsg("Error while updating application parameter");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }finally{
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return map;
    }
    public void authorizeParam(){
        
    }
    
    
    public HashMap updateparam(String status,String userid,String key,String value) {
        String            query =
            "update ebanking_params set value =?,modified_by=?,modified_date=sysdate,Status=? where keys=?";
        PreparedStatement pstmt = null;
        int               rs    = -1;
        HashMap           map   = null;

        connect = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt   = connect.prepareStatement(query);
            pstmt.setString(1, value);
            pstmt.setString(2, userid);
            pstmt.setString(3, status);
            pstmt.setString(4, key);
            rs      = pstmt.executeUpdate();
            if(bDebugOn)System.out.println(" ========rs======= "+rs);
            if ((rs > 0)) {
                map = responseMsg("The application parameter successfully updated");
            } else {
              map =   responseMsg("Error while updating application parameter");
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }finally{
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return map;
    }

    public HashMap getAppParameters(String key) {
        String            query = "select * from ebanking_params where keys = ?  ";
        PreparedStatement pstmt = null;
        ResultSet         rs    = null;
        HashMap           map   = null;

        connect = null;

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt   = connect.prepareStatement(query);
            pstmt.setString(1, key);
            rs      = pstmt.executeQuery();
            parent  = this.convertToXml(parent, "ROW", rs);
            map     = this.generateXML(parent, xmlText);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }finally{

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return map;
    }
      
    public int updateParameter(DataofInstance instData) {

        connect =com.fasyl.ebanking.db.DataBase.getConnection();
        java.sql.CallableStatement callable = null;
        int                        result   = -1;
        String                     data     = null;

        try {
            callable = connect.prepareCall("{?=call fn_getparam(?,?,?,?,?,?)}");
            callable.registerOutParameter(1, oracle.jdbc.OracleTypes.INTEGER);
            callable.registerOutParameter(6, oracle.jdbc.OracleTypes.VARCHAR);
            callable.registerOutParameter(7, oracle.jdbc.OracleTypes.VARCHAR);
            callable.setString(2, instData.sessionId);
            callable.setString(3, instData.userId);
            callable.setString(4, failiurecount);
            callable.setString(5, idletime);
            callable.execute();
            result = callable.getInt(1);
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }finally{
            if (callable != null) {
                try {
                    callable.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (bDebugOn) {
            System.out.println(result + " This is the result " + result);
        }

        return result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
