
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class AdminUserInfo extends ProcessClass {
    Connection        con     = null;
    PreparedStatement pstmt   = null;
    ResultSet         rs      = null;
    String            sellang = "select lang_id,lang_desc from lang";
    private String    xmlText = null;
    private Element   parent;
    String            userType;

    public AdminUserInfo() {}

    protected void initializeVar() {
        userType = null;
        xmlText  = null;
        pstmt    = null;
        rs       = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        userType = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "usertype",
                0, true, null), "UTF-8");
        parent = new Element("ROWSET");
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        HashMap map = new HashMap();


        CallableStatement stmt = null;

        initializeVar();

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            setInstanceVar(instData);
            stmt = con.prepareCall("{?=call fn_admin_user_info(?,?,?,?,?,?)}");
            stmt.registerOutParameter(1, OracleTypes.INTEGER);
            stmt.registerOutParameter(4, OracleTypes.CLOB);
            stmt.registerOutParameter(5, OracleTypes.VARCHAR);
            stmt.registerOutParameter(6, OracleTypes.VARCHAR);
            stmt.registerOutParameter(7, OracleTypes.VARCHAR);
            stmt.setString(2, instData.userId);
            stmt.setString(3, instData.sessionId);
            stmt.execute();

            int    result   = stmt.getInt(1);
            String err_code = stmt.getString(6);
            String err_msg  = stmt.getString(5);

            if (result != 0) {
                if (err_code != null) {
                    instData.result = Utility.processError(instData, err_code);

                    return instData.result;
                }

//              instData.result = Utility.processError(instData, ERR_ACCTNOISNULL);   
//               return instData.result;
            } else {
                java.sql.Clob datas   = stmt.getClob(4);
                String        data    = datas.getSubString(1, (int) datas.length());
                HashMap       hashMap = new HashMap();

                hashMap.put(RESULT_MAP_KEY, data);
                instData.result = hashMap;
            }

//          pstmt = con.prepareStatement(sellang);
//          rs = pstmt.executeQuery();
//          parent = this.convertToXml(parent, "ROW", rs);
//          Element row = new Element("USERTYPES");
//                  Element usertype = new Element("USERTYPE");
//                  usertype.setText(this.userType);
//                  row.addContent(usertype);
//                  parent.addContent(row);
//                   instData.result = this.generateXML(parent, xmlText);
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
            ex.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {

                    // instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
                    ex.printStackTrace();

                    // return  instData.result;
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {

                    // instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
                    ex.printStackTrace();

                    // return  instData.result;
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
