
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Date;

/**
 *
 * @author Administrator
 */
public class SiUploadInstVO {
    private char authstatus;
    private Date bookdate;

    // for sitb_upload transaction
    private String branchcode;
    private char   calholexcp;
    private char   convstatus;
    private String counterparty;
    private String errmsg;
    private int    execdays;
    private int    execmths;
    private int    execyrs;
    private Date   firstexecdate;
    private Date   firstvaluedate;
    private char   inststatus;
    private Date   latestcycledate;
    private int    latestcycleno;
    private Date   latestversiondate;
    private char   monthendflag;
    private Date   nextexecdate;
    private Date   nextvaluedate;
    private char   processingtime;
    private String productcode;
    private char   producttype;
    private String ratetype;
    private int    serialno;
    private char   sitype;
    private String sourcecode;
    private String sourceref;
    private String userinstno;

    /**
     * @return the branchcode
     */
    public String getBranchcode() {
        return branchcode;
    }

    /**
     * @param branchcode the branchcode to set
     */
    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    /**
     * @return the sourcecode
     */
    public String getSourcecode() {
        return sourcecode;
    }

    /**
     * @param sourcecode the sourcecode to set
     */
    public void setSourcecode(String sourcecode) {
        this.sourcecode = sourcecode;
    }

    /**
     * @return the sourceref
     */
    public String getSourceref() {
        return sourceref;
    }

    /**
     * @param sourceref the sourceref to set
     */
    public void setSourceref(String sourceref) {
        this.sourceref = sourceref;
    }

    /**
     * @return the productcode
     */
    public String getProductcode() {
        return productcode;
    }

    /**
     * @param productcode the productcode to set
     */
    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    /**
     * @return the producttype
     */
    public char getProducttype() {
        return producttype;
    }

    /**
     * @param producttype the producttype to set
     */
    public void setProducttype(char producttype) {
        this.producttype = producttype;
    }

    /**
     * @return the sitype
     */
    public char getSitype() {
        return sitype;
    }

    /**
     * @param sitype the sitype to set
     */
    public void setSitype(char sitype) {
        this.sitype = sitype;
    }

    /**
     * @return the calholexcp
     */
    public char getCalholexcp() {
        return calholexcp;
    }

    /**
     * @param calholexcp the calholexcp to set
     */
    public void setCalholexcp(char calholexcp) {
        this.calholexcp = calholexcp;
    }

    /**
     * @return the ratetype
     */
    public String getRatetype() {
        return ratetype;
    }

    /**
     * @param ratetype the ratetype to set
     */
    public void setRatetype(String ratetype) {
        this.ratetype = ratetype;
    }

    /**
     * @return the execdays
     */
    public int getExecdays() {
        return execdays;
    }

    /**
     * @param execdays the execdays to set
     */
    public void setExecdays(int execdays) {
        this.execdays = execdays;
    }

    /**
     * @return the execmths
     */
    public int getExecmths() {
        return execmths;
    }

    /**
     * @param execmths the execmths to set
     */
    public void setExecmths(int execmths) {
        this.execmths = execmths;
    }

    /**
     * @return the execyrs
     */
    public int getExecyrs() {
        return execyrs;
    }

    /**
     * @param execyrs the execyrs to set
     */
    public void setExecyrs(int execyrs) {
        this.execyrs = execyrs;
    }

    /**
     * @return the firstexecdate
     */
    public Date getFirstexecdate() {
        return firstexecdate;
    }

    /**
     * @param firstexecdate the firstexecdate to set
     */
    public void setFirstexecdate(Date firstexecdate) {
        this.firstexecdate = firstexecdate;
    }

    /**
     * @return the nextexecdate
     */
    public Date getNextexecdate() {
        return nextexecdate;
    }

    /**
     * @param nextexecdate the nextexecdate to set
     */
    public void setNextexecdate(Date nextexecdate) {
        this.nextexecdate = nextexecdate;
    }

    /**
     * @return the firstvaluedate
     */
    public Date getFirstvaluedate() {
        return firstvaluedate;
    }

    /**
     * @param firstvaluedate the firstvaluedate to set
     */
    public void setFirstvaluedate(Date firstvaluedate) {
        this.firstvaluedate = firstvaluedate;
    }

    /**
     * @return the nextvaluedate
     */
    public Date getNextvaluedate() {
        return nextvaluedate;
    }

    /**
     * @param nextvaluedate the nextvaluedate to set
     */
    public void setNextvaluedate(Date nextvaluedate) {
        this.nextvaluedate = nextvaluedate;
    }

    /**
     * @return the monthendflag
     */
    public char getMonthendflag() {
        return monthendflag;
    }

    /**
     * @param monthendflag the monthendflag to set
     */
    public void setMonthendflag(char monthendflag) {
        this.monthendflag = monthendflag;
    }

    /**
     * @return the processingtime
     */
    public char getProcessingtime() {
        return processingtime;
    }

    /**
     * @param processingtime the processingtime to set
     */
    public void setProcessingtime(char processingtime) {
        this.processingtime = processingtime;
    }

    /**
     * @return the userinstno
     */
    public String getUserinstno() {
        return userinstno;
    }

    /**
     * @param userinstno the userinstno to set
     */
    public void setUserinstno(String userinstno) {
        this.userinstno = userinstno;
    }

    /**
     * @return the inststatus
     */
    public char getInststatus() {
        return inststatus;
    }

    /**
     * @param inststatus the inststatus to set
     */
    public void setInststatus(char inststatus) {
        this.inststatus = inststatus;
    }

    /**
     * @return the authstatus
     */
    public char getAuthstatus() {
        return authstatus;
    }

    /**
     * @param authstatus the authstatus to set
     */
    public void setAuthstatus(char authstatus) {
        this.authstatus = authstatus;
    }

    /**
     * @return the latestversiondate
     */
    public Date getLatestversiondate() {
        return latestversiondate;
    }

    /**
     * @param latestversiondate the latestversiondate to set
     */
    public void setLatestversiondate(Date latestversiondate) {
        this.latestversiondate = latestversiondate;
    }

    /**
     * @return the bookdate
     */
    public Date getBookdate() {
        return bookdate;
    }

    /**
     * @param bookdate the bookdate to set
     */
    public void setBookdate(Date bookdate) {
        this.bookdate = bookdate;
    }

    /**
     * @return the serialno
     */
    public int getSerialno() {
        return serialno;
    }

    /**
     * @param serialno the serialno to set
     */
    public void setSerialno(int serialno) {
        this.serialno = serialno;
    }

    /**
     * @return the counterparty
     */
    public String getCounterparty() {
        return counterparty;
    }

    /**
     * @param counterparty the counterparty to set
     */
    public void setCounterparty(String counterparty) {
        this.counterparty = counterparty;
    }

    /**
     * @return the latestcycleno
     */
    public int getLatestcycleno() {
        return latestcycleno;
    }

    /**
     * @param latestcycleno the latestcycleno to set
     */
    public void setLatestcycleno(int latestcycleno) {
        this.latestcycleno = latestcycleno;
    }

    /**
     * @return the latestcycledate
     */
    public Date getLatestcycledate() {
        return latestcycledate;
    }

    /**
     * @param latestcycledate the latestcycledate to set
     */
    public void setLatestcycledate(Date latestcycledate) {
        this.latestcycledate = latestcycledate;
    }

    /**
     * @return the convstatus
     */
    public char getConvstatus() {
        return convstatus;
    }

    /**
     * @param convstatus the convstatus to set
     */
    public void setConvstatus(char convstatus) {
        this.convstatus = convstatus;
    }

    /**
     * @return the errmsg
     */
    public String getErrmsg() {
        return errmsg;
    }

    /**
     * @param errmsg the errmsg to set
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
