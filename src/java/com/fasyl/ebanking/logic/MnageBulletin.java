
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;
import com.fasyl.ebanking.util.Utility;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Administrator
 */
public class MnageBulletin extends ProcessClass {

    /*
     * This constant defines customer specific message.
     */
    private static final String CUSTOMER_SPECIFIC = "Y";
    /**
     * This constant defines delimiter used while defining customer names for
     * specific messages.
     */
    private static final String DELIMITER = ",";
    private static final String INS_MSG_USER_MAP = " insert into map_user_message " + " ( userid, messageid ) "
            + " values (?, ?) ";
    private static final String MSG_CUST_MAP
            = "select a.cod_usr_id cod_usr_id from SM_USER_ACCESS a, map_user_message b "
            + "where a.cod_usr_id=b.userid and a.cod_usr_id=? and a.flg_mnt_status='A'";
    private String bulletinQry
            = "select messageid, flgupload,body, added_date, tittle,attachment,imgext,attachment2,imgext2,attachment3,imgext3 from bulletin_messages where sysdate between active_date and Expire_date + 1  and idmessage =? and (iscustspecific = 'Y' and idmessage in (select messageid from map_user_message where userid = ?) or iscustspecific = 'N')";
    private HashMap hashMap = null;
    private String insertBulletin = "insert into bulletin_messages values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    

    /*
     * This constant defines sql query for inserting newly created message customers
     * into databse.
     */
    private long messageId = 0L;
    private String viewallnonspecific
            = "select messageid, flgupload,body, to_date(expire_date,'dd-mon-yyyy') expire_date,to_date(active_date,'dd-mon-yyyy') active_date, tittle from bulletin_messages where to_date(Expire_date,'dd-mon-yyyy') >= to_date(sysdate,'dd-mon-yyyy') and iscustspecific = 'N' and to_date(active_date,'dd-mon-yyyy')<= to_date(sysdate,'dd-mon-yyyy')";
    private String viewallnonspecificDetail
            = "select messageid,flgupload,body,attachment,imgext,attachment2,imgext2,attachment3,imgext3,to_date(expire_date,'dd-mon-yyyy') expire_date,to_date(active_date,'dd-mon-yyyy') active_date, tittle from bulletin_messages where to_date(Expire_date,'dd-mon-yyyy') >= to_date(sysdate,'dd-mon-yyyy') and iscustspecific = 'N' and to_date(active_date,'dd-mon-yyyy') <= to_date(sysdate,'dd-mon-yyyy') and messageid=?";
    private String xmlText = null;

    /*
     * This Vector will be used to store the users to which the message is to
     * has been added in the database.
     */
    private final Vector userTable = new Vector();
    private Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

    /*  This String variable contains the customer list.
     **/
    private String custList;
    /**
     * This variable contains the end date of the message.
     */
    private Date datEnd;
    /**
     * This variable contains the from date.
     */
    private Date datFrom;
    private PreparedStatement insCustMsg;
    /**
     * This String variable conntains the message.
     */
    private String message;
    /**
     * This String variable conntains the message subject.
     */
    private String messageSubject;
    private Element parent;
    private boolean queryStatus;
    private String refno;
    /**
     * This String variable contains the value "U" or "C" depending on whether
     * the customer id or user id is entered.
     */
    private String sendToType;
    /**
     * This String variable contains the flag for customer specific message.
     */
    private String specific;
    private String attach;
    private String attach2;
    private String attach3;
    //2015-01- 
    private String sessId;
    private String attachment;
    private byte[] myByteArray;
    private String attachment2;
    private String attachment3;
    

    public MnageBulletin() {
    }

    protected void initializeVar() {

        // /activefrom                   =       null;
        // fromdate                      =       null;
        // todate                                =       null;
        message = "";
        attach = "";
        attachment = "";
        attachment2 = "";
        attachment3 = "";
        sessId = "";

        // oldMessage                    =       "";
        custList = "";
        messageId = 0;
        messageSubject = "";

        // oldMessageSubject     =       "";
        // datCreated                    =       null;
        datFrom = null;
        datEnd = null;
        specific = "";
        userTable.clear();
        this.queryStatus = false;
        refno = null;

        // oldStrDatFrom         =       null;
        // strDatEnd                     =       null;
        // oldStrDatEnd          =       null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();
        messageId = System.currentTimeMillis();
        this.datEnd
                = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                                "msgvalidto", 0, false, null));
        this.datFrom
                = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                                "msgvalidfrom", 0, false, null));
        refno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "valuex", 0, false, null);

        // this.datFrom = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "FirstExecDate", 0, false, null));
        this.message = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "message", 0, false, null), "UTF-8");

        this.attachment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "attach", 0, false, null), "UTF-8");

        this.attachment2 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "attach2", 0, false, null), "UTF-8");

        this.attachment3 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "attach3", 0, false, null), "UTF-8");

        this.sessId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "sessId", 0, false, null), "UTF-8");
      

        // /if (bDebugOn)System.out.println("=========== message value after decode is  "+this.message);
        this.messageSubject = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "subject", 0, false, null), "UTF-8");
        this.specific = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "custspecific", 0,
                false, null);

        // this. = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "detail", 0, false, null);
        parent = new Element("ROWSET");
    }

    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    @Override
    public HashMap processRequest(DataofInstance instData) {
        if (bDebugOn) {
            System.out.println("=========== inside process request ===============");
        }
        System.out.println("session id in instData: " + instData.sessionId);

        initializeVar();

        try {
            if (bDebugOn) {
                System.out.println("=========== inside try block ===============");
            }

            setInstanceVar(instData);

            if (instData.taskId.equals("4022") || instData.taskId.equals("40221") || instData.taskId.equals("513")) {
                if (bDebugOn) {
                    System.out.println("=========== inside view message message ===============");
                }

                viewbulletin(instData);
            } else {
//                Hashtable hash = instData.requestHashTable;
//                Enumeration enums = hash.elements();
//                while(enums.hasMoreElements())
//                {
//                    System.out.println("next element: "+ enums.nextElement());
//                }
                insertMessage();
            }

            if ((!this.queryStatus)
                    && (!(instData.taskId.equals("4022") || instData.taskId.equals("40221")
                    || instData.taskId.equals("513")))) {
                buildResponse();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (insCustMsg != null) {
                try {
                    insCustMsg.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        instData.result = hashMap;

        if (bDebugOn) {
            System.out.println("========= About to return result =======");
        }

        return instData.result;
    }

    private void buildResponse() {
        if (!this.queryStatus) {
            Element result = new Element("ROW");
            Element xtxnseq = new Element("SUCCESS");
System.out.println("insertBulletin" +insertBulletin);
            xtxnseq.setText(String.valueOf("The Operation was successful"));
            result.addContent(xtxnseq);
            parent.addContent(result);
        } else {
            Element result = new Element("ROW");
            Element xtxnseq = new Element("ERROR");

            xtxnseq.setText(String.valueOf("The Operation was not successful"));
            result.addContent(xtxnseq);
            parent.addContent(result);
        }

        hashMap = generateXML(parent, this.xmlText);
    }

    /*
     * This method insers the customer message into the database. It generates the
     *  message id and date of message creation.
     */
 private void insertMessage() throws Exception {
        if (bDebugOn) {
            System.out.println("=========== inside method insert message ===============");
        }

        int l_tmp = 0;

        messageId = System.currentTimeMillis();

        if (specific.equals(CUSTOMER_SPECIFIC)) {
            if (bDebugOn) {
                System.out.println("=========== message is customer specific ===============");
            }

            processCustomers();    // not ye impllemented fully
        }

        // addMessageDetails ();
        if (specific.equals(CUSTOMER_SPECIFIC) && (userTable.size() == 0)) {
            return;
        }

        if (bDebugOn) {
            System.out.println("=========== about to insert message ===============");
        }
        // for attachment on bulletin added by Rebecca
        String ext = (attachment != null) && (!attachment.isEmpty()) ? attachment.substring(attachment.lastIndexOf(".") + 1) :"";
        String ext2 = (attachment2 != null) && (!attachment2.isEmpty()) ? attachment2.substring(attachment2.lastIndexOf(".") + 1) :"";
        String ext3 = (attachment3 != null) && (!attachment3.isEmpty()) ? attachment3.substring(attachment3.lastIndexOf(".") + 1) :"";
        String folderPath = "/IntBanking" + "/attachmentFolder/" + sessId;
        String filePath = folderPath + "/attach." + ext;
        System.out.println("file path  in insert method: " + filePath);
        String filePath2 = folderPath + "/attach2." + ext2;
        System.out.println("file path 2 in insert method: " + filePath2);
        String filePath3 = folderPath + "/attach3." + ext3;
        System.out.println("file path 3 in insert method: " + filePath3);
        byte[] imageByte = null;
        byte[] imageByte2 = null;
        byte[] imageByte3 = null;
        
        try {
            java.awt.image.BufferedImage originalImage;
            java.io.ByteArrayOutputStream 
                    
             baos = null;
            if((attachment != null) && (!attachment.isEmpty()))
            {
                originalImage = javax.imageio.ImageIO.read(new java.io.File(filePath));
                baos = new java.io.ByteArrayOutputStream();
                javax.imageio.ImageIO.write(originalImage, ext, baos);
                baos.flush();
                imageByte = baos.toByteArray();
            }
            
            if((attachment2 != null) && (!attachment2.isEmpty()))
            {
                originalImage = javax.imageio.ImageIO.read(new java.io.File(filePath2));
                baos = new java.io.ByteArrayOutputStream();
                javax.imageio.ImageIO.write(originalImage, ext2, baos);
                baos.flush();
                imageByte2 = baos.toByteArray();
            }
            
            if((attachment3 != null) && (!attachment3.isEmpty()))
            {
                originalImage = javax.imageio.ImageIO.read(new java.io.File(filePath3));
                baos = new java.io.ByteArrayOutputStream();
                javax.imageio.ImageIO.write(originalImage, ext3, baos);
                baos.flush();
                imageByte3 = baos.toByteArray();
            }
            
            if(baos != null)
                baos.close();
            
        } catch (FileNotFoundException fnfEx) {
            System.out.println("fnfEx :" + fnfEx.getMessage());
        }

        insCustMsg = connect.prepareStatement(insertBulletin);
        insCustMsg.setBigDecimal(1, BigDecimal.valueOf(messageId));
        insCustMsg.setTimestamp(2, Utility.getCurrentDate());
        insCustMsg.setTimestamp(3, new Timestamp(datFrom.getTime()));
        insCustMsg.setTimestamp(4, new Timestamp(datEnd.getTime()));
        insCustMsg.setString(5, specific);
        insCustMsg.setString(6, messageSubject);
        insCustMsg.setString(7, "N");
        insCustMsg.setString(8, message);
        insCustMsg.setBytes(9, imageByte);
        insCustMsg.setString(10, ext);
        insCustMsg.setBytes(11, imageByte2);
        insCustMsg.setString(12, ext2);
        insCustMsg.setBytes(13, imageByte3);
        insCustMsg.setString(14, ext3);

        this.queryStatus = insCustMsg.execute();
        if (bDebugOn) {
            System.out.println("=========== This is the query status: " + this.queryStatus + " ===============");
        }
        //delete folders after creating bulletin
       try {
            System.out.println("WANNA DELETE THE FOLDER AFTER CREATING");
            File f = new File(folderPath);
           
            if (f.exists()) {
                FileUtils.forceDelete(f);
            }
        } catch (Exception fnf) {
            System.err.println("ON CREATE; CANNOT DELETE FOLDER @ " + folderPath);
        }
        //delete

        for (l_tmp = 0; l_tmp < userTable.size(); l_tmp++) {
            insertCustomer((String) userTable.get(l_tmp));
        }
    }

    private void viewbulletin(DataofInstance instdata) {
        Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt = null;
        HashMap map = new HashMap();
        ResultSet rs = null;

        //patch updated by Rebecca to delete created folder
        String filePath = "";
          try {
            System.out.println("WANNA DELETE THE FOLDER B4 VIEWING");
            File f = new File(sessId);
            if (f.exists()) {
                FileUtils.forceDelete(f);
            }
        } catch (Exception fnf) {
            System.err.println("ON VIEW; CANNOT DELETE FOLDER @ " + sessId);
        }
        
        try {
            if (instdata.taskId.equals("40221")) {
                pstmt = con.prepareStatement(viewallnonspecificDetail);
                pstmt.setString(1, refno);
                rs = pstmt.executeQuery();
                this.convertToXmlBulletin(parent, "ROW", rs);

            } else {
                pstmt = con.prepareStatement(viewallnonspecific);
                rs = pstmt.executeQuery();
                this.convertToXml(parent, "ROW", rs);

                // pstmt.setTimestamp(1,Utility.getCurrentDate());
            }

//            rs = pstmt.executeQuery();
//            this.convertToXmlBulletin(parent, "ROW", rs);
            hashMap = generateXML(parent, this.xmlText);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    
    

    /**
     * This method insert the record for customer into database for specific
     * message.
     */
    private void insertCustomer(String p_cust) throws Exception {
        PreparedStatement l_insMsgUserMap = null;

        try {
            l_insMsgUserMap = connect.prepareStatement(INS_MSG_USER_MAP);
            l_insMsgUserMap.setString(1, p_cust);
            l_insMsgUserMap.setBigDecimal(2, BigDecimal.valueOf(messageId));
            l_insMsgUserMap.executeUpdate();
        } finally {
            try {
                l_insMsgUserMap.close();
            } catch (Exception e) {
            }

            l_insMsgUserMap = null;
        }
    }

    private void processCustomers() throws Exception {
        String l_cust = null;
        ResultSet l_rs = null;
        PreparedStatement l_valCustMap = null;
        StringTokeNizers l_tokenizer = null;
        Vector l_invalid_users = new Vector();
        String l_fileName = null;
        StringBuffer l_buf = null;
        int l_countusers = 0;

        try {
            l_tokenizer = new StringTokeNizers("", DELIMITER);
            l_tokenizer.setData(custList, DELIMITER, false);
            l_buf = new StringBuffer(MSG_CUST_MAP);

            if (sendToType.equals("U")) {
                while (l_tokenizer.hasMoreTokens()) {
                    l_cust = l_tokenizer.nextToken().trim();

                    if ((!userTable.contains(l_cust)) && (!l_invalid_users.contains(l_cust))) {
                        l_valCustMap = connect.prepareStatement(l_buf.toString());
                        l_valCustMap.setString(1, l_cust);
                        l_rs = l_valCustMap.executeQuery();

                        if (!l_rs.next()) {
                            l_invalid_users.add(l_cust);

                            continue;
                        }
                    }

                    userTable.add(l_cust);
                }
            } else {
                while (l_tokenizer.hasMoreTokens()) {
                    l_cust = l_tokenizer.nextToken().trim();

                    if ((!userTable.contains(l_cust)) && (!l_invalid_users.contains(l_cust))) {
                        l_valCustMap = connect.prepareStatement(l_buf.toString());
                        l_valCustMap.setString(1, l_cust);
                        l_rs = l_valCustMap.executeQuery();
                        l_countusers = 0;

                        while (l_rs.next()) {
                            if (!userTable.contains(l_rs.getString("iduser"))) {
                                userTable.add(l_rs.getString("iduser"));
                            }

                            l_countusers++;
                        }

                        if (l_countusers == 0) {
                            l_invalid_users.add(l_cust);

                            // catch required exception
                        }
                    }
                }
            }
        } finally {
            try {
                l_rs.close();
            } catch (Exception e) {
            }

            try {
                l_valCustMap.close();
            } catch (Exception e) {
            }

            l_valCustMap = null;
        }
    }

    //Patch Updated by Rebecca for View Bulletin
   public Element convertToXmlBulletin(Element parent, String elemname, ResultSet rs) throws Exception {
        if (bDebugOn) {
            System.out.println("========= inside convertToXml ==========");
        }


        ResultSetMetaData metaData = rs.getMetaData();
        Element elem = null;

        if (bDebugOn) {
            System.out.println("==== This is metadata ==== ");
        }

        int numofcolumns = metaData.getColumnCount();

        if (bDebugOn) {
            System.out.println("==== This is metadata count ==== " + numofcolumns);
        }

        Element xmlName = null;

        if (!rs.isBeforeFirst()) {    // this method is not implemented for all jdbc
            if (bDebugOn) {
                System.out.println("==== rs is null=====");
            }

            return null;
        }

        while (rs.next()) {
            elem = new Element(elemname);

            if (bDebugOn) {
                System.out.println(" === inside converttoxml resultset" + numofcolumns);
            }

            for (int i = 1; i <= numofcolumns; i++) {
                if (bDebugOn) {
                    System.out.println(" === inside converttoxml inner " + metaData.getColumnName(i));
                }

             xmlName = new Element(metaData.getColumnName(i));
             
             String folderPath = sessId;
             if(metaData.getColumnName(i).equals("ATTACHMENT"))
                {
                    java.sql.Blob b = rs.getBlob("ATTACHMENT");
                    //byte[] imgBytes = b.getBytes(1,(int)b.length());
                    
                    byte[] imgBytes = null;
                    try{
                          imgBytes = b.getBytes(1,(int)b.length());
                    }catch(NullPointerException npe)
                    {
                        System.out.println("no image for ATTACHMENT1");
                    }
                    
                    File f = new File(folderPath);
                    f.mkdirs();
                    
                    if(imgBytes != null)
                    {
                        try {
                            String filePath = folderPath+"/" + "ATTACHMENT."+rs.getString("imgext");
                            java.io.FileOutputStream fileOuputStream = new java.io.FileOutputStream(filePath);
                            fileOuputStream.write(imgBytes);
                    } catch (Exception ex) {
                            ex.printStackTrace();
                    }
                         
                    }
//                    else
//                        xmlName.setText("no image detected");
                    
                }
             else if(metaData.getColumnName(i).equals("ATTACHMENT2"))
                {
                    java.sql.Blob b = rs.getBlob("ATTACHMENT2");
                    //byte[] imgBytes2 = b.getBytes(1,(int)b.length());
                     byte[] imgBytes2 = null;
                    try{
                          imgBytes2 = b.getBytes(1,(int)b.length());
                    }catch(NullPointerException npe)
                    {
                        System.out.println("no image for ATTACHMENT2");
                    }
                    
                    
                    if(imgBytes2 != null)
                    {
                        try {
                            String filePath = folderPath+"/" + "ATTACHMENT2."+rs.getString("imgext2");
                            java.io.FileOutputStream fileOuputStream = new java.io.FileOutputStream(filePath);
                            fileOuputStream.write(imgBytes2);
                            fileOuputStream.close();
                            
                    } catch (Exception ex) {
                            ex.printStackTrace();
                    }
                         
                    }
//                    else
//                        xmlName.setText("no image detected");
                } 
             else if(metaData.getColumnName(i).equals("ATTACHMENT3"))
                {
                    java.sql.Blob b = rs.getBlob("ATTACHMENT3");
                   // byte[] imgBytes3 = b.getBytes(1,(int)b.length());
                      byte[] imgBytes3 = null;
                    try{
                          imgBytes3 = b.getBytes(1,(int)b.length());
                    }catch(NullPointerException npe)
                    {
                        System.out.println("no image for ATTACHMENT3");
                    }
                    
                    
                    File f = new File(folderPath);
                    f.mkdirs();
                    
                    if(imgBytes3 != null)
                    {
                        try {
                            String filePath = folderPath+"/" + "ATTACHMENT3."+rs.getString("imgext3");
                            java.io.FileOutputStream fileOuputStream = new java.io.FileOutputStream(filePath);
                            fileOuputStream.write(imgBytes3);
                    } catch (Exception ex) {
                            ex.printStackTrace();
                    }
                         
                    }
//                    else
//                        xmlName.setText("no image detected");
                } 
             else {
                    xmlName.setText(rs.getString(metaData.getColumnName(i)));
                }

                // if (bDebugOn)System.out.println(" === inside converttoxml value" +rs.getString(metaData.getColumnName(i)));
                elem.addContent(xmlName);
            }

            parent.addContent(elem);
        }

        if (bDebugOn) {
            System.out.println(" === This is parent in convert to xml ==== " + parent);
        }

        return parent;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
