
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GetCustInfoold extends Processes {
    private static final int    NBR_PARAMS            = 1;
    private static final String PROCEDURE_GETCUSTINFO = "F_GET_CUST_INFO";
    public static final String  SUCCESS               = "false";
    private String              custno;
    private String              customerName;
    private Connection          dbConnection;
    protected CallableStatement procStmt;
    private String              returnResult;

    public GetCustInfoold() {}

    public void initializeVar() {
        this.custno       = null;
        this.procStmt     = null;
        this.returnResult = null;
        this.customerName = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.custno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0, false,
                null);
        System.out.println(custno + " This is the custno");
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        instData.result = ExchangeMap(connect);

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        System.out.println("i am testing oooooooo " + returnResult);
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_GETCUSTINFO, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(2, custno);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
