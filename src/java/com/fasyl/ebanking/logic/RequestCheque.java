
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class RequestCheque extends Processes {
    private static final int    NBR_PARAMS     = 6;
    private static final String REQUEST_CHEQUE = "FN_MAKE_CHEQ_REQUEST";
    private String              account;
    private String              checktype;
    private String              chequeSeq;
    //Connection                  connect;
    private String              custno;
    private String              errorMsg;
    private String              leaves;
    ArrayList                   list;
    private String              noofbooklet;
    private HashMap             preResult;
    private String              returnResult;
    private String              type;
    public RequestCheque() {}

    private void initializeVar() {
        this.account      = null;
        this.leaves       = null;
        this.custno       = null;
        this.returnResult = null;
        this.chequeSeq    = null;
        this.preResult    = null;
        this.errorMsg     = null;
        checktype         = null;
        noofbooklet       = null;
        type              = null;
    }

     @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connects = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            connects = com.fasyl.ebanking.db.DataBase.getConnection();
            preResult = ExchangeMap(connects);

            if (!(preResult.get("returnResult").equals("TRUE"))) {

                // preResult.put("returnResult","Unknown error");
                instData.result = Utility.processError(instData, ERR_DATABASE);

                return instData.result;
            }

            // req.setAttribute("data", datas);
            String type = "text/html";
            String body =
                "Dear Sir/Ma,<br>Kindly process a new cheque book for the user with the following credentials: <br> <B>Account Number : "
                + this.account + ",Number of leaves : " + this.leaves + ",Cheque Type : " + this.checktype
                + ",Number of Booklet : " + this.noofbooklet + "<B><br> Thank You<br> Yours Faithfully <br> IB";
            String   header    = "ChequeBook Request";
            String   toemail   = (String) LoadEmails.getDataValuenew("CHQRQ");;
            String   fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
            sendmail send      = new sendmail();

            // send.sendMessage(header, body, this.account, type, fromemail, toemail);
            boolean results = send.sendMessage(header, body, this.account, type, fromemail, toemail);

            if (results) {
                instData.result = this.preResult;
                instData.result.put("retrunResult", this.chequeSeq);
                errorMsg               = "";
                instData.processStatus = SUCCESSSTATUS;
            } else {

                /*
                 * instData.processStatus=ERRORSTATUS;
                 * instData.result=this.preResult;//in order not to instantiate another class
                 * instData.result.put("errorcode",EMAILSERVERERROR);
                 * //instData.result.put("errorMsg","fatal error");
                 */
                instData.result = Utility.processError(instData, EMAILSERVERERROR);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connects != null) {
                try {
                    connects.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acno", 0, false,
                null);
        this.leaves = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "leaves", 0, false,
                null);
        this.checktype = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "checktype", 0,
                false, null);
        this.noofbooklet = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "noofbooklet", 0,
                false, null);
        System.out.println("<<<<<<<<< This is the Cheque Type >>>>>>>>>> "+checktype);
        this.chequeSeq = Utility.generateSequenceNo();
        this.custno    = inst_data.custId;
        if (checktype==null ||checktype.equals(""))checktype="private";
        type = checktype.equals("corporate") ? "C":"P";
        if (bDebugOn) {
            System.out.println("This is the custno: " + custno);
        }
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.REQUEST_CHEQUE, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }
   
    public String executeProcedure() throws SQLException {
        procStmt.setString(2, this.chequeSeq);
        procStmt.setString(3, this.account);
        procStmt.setString(4, this.custno);
        procStmt.setString(5, this.leaves);
        procStmt.setString(6, "A");
        procStmt.setString(7, type);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
