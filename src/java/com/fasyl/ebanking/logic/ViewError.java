
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author baby
 */
public class ViewError extends ProcessClass {
    private static final String VIEW_ERROR_QRY =
        "select trx_seq,user_id,cust_id,txn_desc,dattxn,errorcode from auditlog "
            + "where dattxn between ? and ?  and "
            + " (ERRORCODE is not null or finaldbres='N' or actual_db_error is "
            + "not null or error_message is not null or process_status='N') and txn_desc is not null order by dattxn desc  ";
    private static final ViewError innerClass = new ViewError();
    private HashMap                hashMap    = null;
    private String                 lowCount   = null;
    private StringBuffer           sSql       = null;
    private int                    size       = 0;
    private String                 xmlText    = null;
    private Connection             con;
    private Date                   from;
    private String                 highCount;
    private Vector                 nextSearchData;
    private Element                parent;
    private PreparedStatement      pstmt;
    private ResultSet              rs;
    private String                 taskId;
    private Date                   to;

    public ViewError() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        nextSearchData = new Vector();
        this.to        =
            Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "to",
                0, false, null));
        this.from =
            Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "from", 0, false, null));
        this.lowCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "lowcount", 0, false,
                null);
        this.highCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "highcount", 0,
                false, null);
        System.out.println("highcount  " + this.highCount);
        this.taskId = inst_data.taskId;
        parent      = new Element("ROWSET");

        // sSql = new StringBuffer(VIEW_ERROR_QRY);
    }

    protected void initializeVar() {
        this.to        = null;
        this.from      = null;
        this.highCount = null;
        this.lowCount  = null;
        sSql           = new StringBuffer("");
        pstmt          = null;
        rs             = null;
        xmlText        = null;
        parent         = null;
        size           = 0;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        xmlText = null;
        con     = com.fasyl.ebanking.db.DataBase.getConnection();
        initializeVar();

        try {
            setInstanceVar(instData);
            pstmt = con.prepareStatement(VIEW_ERROR_QRY);
            pstmt.setDate(1, from);
            pstmt.setDate(2, to);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                nextSearch searchError = innerClass.new nextSearch(rs);

                this.nextSearchData.add(searchError);
            }

            preProcess(instData);
            instData.result = hashMap;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (bDebugOn) {
            System.out.println(" ======This is the instdata.result ====== " + instData.result);
        }

        return instData.result;
    }

    public void preProcess(DataofInstance instData) {
        int highcount = (this.highCount == null)
                        ? 20
                        : Integer.parseInt(this.highCount);
        int lowcount  = (this.lowCount == null)
                        ? 0
                        : Integer.parseInt(this.lowCount);

        size = nextSearchData.size();

        if (size <= 0) {

            // throw exception
            if (bDebugOn) {
                System.out.println(" ====== This is the size:" + size + " ==================");
            }

            hashMap = Utility.processError(instData, APP_ERROR_SEARCH);    // Will change USERNOTFOUND to APP_ERROR_SEARCH

            // return instData.result;
        } else {
            highCount = String.valueOf(highcount);
            lowCount  = String.valueOf(lowcount);

            if (bDebugOn) {
                System.out.println(" This is the size:" + size);
            }

            int countLimit = 0;

            if (highcount > size) {
                countLimit = size - 1;
            } else {
                countLimit = size - 1;    // highcount;
            }

            if (bDebugOn) {
                System.out.println(" This is the countLimit:" + countLimit);
            }

            buildResponse(lowcount, countLimit);
        }
    }

    public void buildResponse(int lowcount, int countLimit) {
        for (int i = lowcount; i <= countLimit; i++) {
            Element result  = new Element("ROW");
            Element xtxnseq = new Element("TXN_SEQ");

            xtxnseq.setText(((ViewError.nextSearch) this.nextSearchData.get(i)).getTrxseq());

            Element xuserid = new Element("USER_ID");

            xuserid.setText(((ViewError.nextSearch) this.nextSearchData.get(i)).getUserId());

            Element xusername = new Element("CUST_ID");

            xusername.setText((((ViewError.nextSearch) this.nextSearchData.get(i)).getCustno()));

            Element userbranch = new Element("TXN_DESC");

            userbranch.setText((((ViewError.nextSearch) this.nextSearchData.get(i)).getTrxdesc()));

            Element lang = new Element("DATTXN");

            lang.setText((((ViewError.nextSearch) this.nextSearchData.get(i)).getDattxn().toString()));

            Element xcustno = new Element("ERRORCODE");

            xcustno.setText((((ViewError.nextSearch) this.nextSearchData.get(i)).getErrocode()));

            Element xhighcount = new Element("HIGHCOUNT");

            xhighcount.setText(String.valueOf(highCount));

            Element xlowcount = new Element("LOWCOUNT");

            xlowcount.setText(String.valueOf(lowCount));

            Element xsize = new Element("SIZE");

            xsize.setText(String.valueOf(size));
            result.addContent(xtxnseq);
            result.addContent(xuserid);
            result.addContent(xusername);
            result.addContent(userbranch);
            result.addContent(lang);
            result.addContent(xcustno);
            result.addContent(xhighcount);
            result.addContent(xlowcount);
            result.addContent(xsize);
            parent.addContent(result);
        }

        hashMap = generateXML(parent, this.xmlText);
    }

    public class nextSearch {
        private String custno;
        private Date   dattxn;
        private String errocode;
        private String trxdesc;
        private String trxseq;
        private String userId;

        public nextSearch(ResultSet rs) {
            try {
                this.trxseq   = rs.getString("trx_seq");
                this.trxdesc  = rs.getString("txn_desc");
                this.custno   = rs.getString("cust_id");
                this.dattxn   = rs.getDate("dattxn");
                this.errocode = rs.getString("errorcode");
                this.userId   = rs.getString("user_id");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        /**
         * @return the trxseq
         */
        public String getTrxseq() {
            return trxseq;
        }

        /**
         * @param trxseq the trxseq to set
         */
        public void setTrxseq(String trxseq) {
            this.trxseq = trxseq;
        }

        /**
         * @return the trxdesc
         */
        public String getTrxdesc() {
            return trxdesc;
        }

        /**
         * @param trxdesc the trxdesc to set
         */
        public void setTrxdesc(String trxdesc) {
            this.trxdesc = trxdesc;
        }

        /**
         * @return the custno
         */
        public String getCustno() {
            return custno;
        }

        /**
         * @param custno the custno to set
         */
        public void setCustno(String custno) {
            this.custno = custno;
        }

        /**
         * @return the dattxn
         */
        public Date getDattxn() {
            return dattxn;
        }

        /**
         * @param dattxn the dattxn to set
         */
        public void setDattxn(Date dattxn) {
            this.dattxn = dattxn;
        }

        /**
         * @return the errocode
         */
        public String getErrocode() {
            return errocode;
        }

        /**
         * @param errocode the errocode to set
         */
        public void setErrocode(String errocode) {
            this.errocode = errocode;
        }

        /**
         * @return the userId
         */
        public String getUserId() {
            return userId;
        }

        /**
         * @param userId the userId to set
         */
        public void setUserId(String userId) {
            this.userId = userId;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
