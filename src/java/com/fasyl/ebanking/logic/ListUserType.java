
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class ListUserType extends Processes {
    private static final String GET_USERTYPEQRY = "fn_GET_USER_TYPE";
    private static final int    NBR_PARAMS      = 0;
    Connection                  connect;
    private String              custNo;
    private String              errorMsg;
    ArrayList                   list;
    private String              returnResult;

    public ListUserType() {}

    private void initializeVar() {
        this.errorMsg = null;
        this.custNo   = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {

        // this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false, null);
        this.custNo = inst_data.custId;
    }

    public HashMap processRequest(DataofInstance instData) {

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            instData.result = ExchangeMap(connect);
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap       result = new HashMap();
        java.sql.Clob datas  = procStmt.getClob(1);
        String        data   = datas.getSubString(1, (int) datas.length());

        returnResult = data;

        // errorMsg = procStmt.getString(3);
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.GET_USERTYPEQRY, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {

        // procStmt.setString(2,this.custNo);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);

        // procStmt.registerOutParameter(3, OracleTypes.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
