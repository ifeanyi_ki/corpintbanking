
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.logic.LoadEmails.LoadParamValue;
import com.fasyl.ebanking.main.EbankingConfiguration;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.text.MessageFormat;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public class LoadEmails implements EbankingConfiguration {
    private static final String     SEL_TXN_PARAM  = "select * from EMAIL_MAINT";
    private static Hashtable        basicParamHash = null;
    private static HashMap          initialParam   = null;
    private static final String[]   l_args         = new String[0];
    private static final LoadEmails innerClass     = new LoadEmails();
    private static Connection       dbConnection;

    public LoadEmails() {}

    public static Enumeration keys() {
        return basicParamHash.keys();
    }

    /* To protect the private map this public method is used to access it from outside the calss */
    public static HashMap getBasicParamMapValue() {
        HashMap parameter = null;

        parameter = (HashMap) initialParam;

        return parameter;
    }

    public static synchronized void loadData() {
        ResultSet rs            = null;
        Statement sel_txn_param = null;

        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();

        String dataKey = null;

        if ((basicParamHash != null) || (initialParam != null)) {
            if (bDebugOn) {
                System.out.println("loaded");
            }

            return;
        }

        try {
            initialParam   = new HashMap();
            basicParamHash = new Hashtable();
            sel_txn_param  = dbConnection.createStatement();
            rs             = sel_txn_param.executeQuery(MessageFormat.format(SEL_TXN_PARAM, l_args));

            while (rs.next()) {
                 if(bDebugOn)System.out.println("<<<<<< Loading emaild data >>>>>>>>>> ");
//              if (rs.getString(1).equals("FTPRODUCTCODE")) {
                LoadParamValue loadParamValue = innerClass.new LoadParamValue(rs, dbConnection);

                dataKey = rs.getString(7);
                basicParamHash.put(dataKey, loadParamValue);
                initialParam.put(rs.getString(7), rs.getString(1));

                // System.out.println(((LoadData.LoadDataValue)basicParamHash.get(dataKey)).classname+"o gaoooooo");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (sel_txn_param != null) {
                try {
                    sel_txn_param.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (dbConnection != null) {
                try {
                    dbConnection.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static LoadParamValue getDataValue(String key) throws Exception {
        if (bDebugOn) {
            System.out.println(key);
        }

        return (LoadEmails.LoadParamValue) initialParam.get(key.trim());
    }

    public static String getDataValuenew(String key) throws Exception {
        if (bDebugOn) {
            System.out.println(key);
        }

        return (String) initialParam.get(key.trim());
    }

    public class LoadParamValue {
        private String keys;
        private String values;

        private LoadParamValue(ResultSet p_rs, Connection con) throws Exception {
            if (p_rs == null) {

                // need to know how to manage exception
                 if (bDebugOn)System.out.println("rs is null");
            } else {
                if (bDebugOn) {
                    System.out.println("rs is not null");
                }

                values = p_rs.getString(1);
                keys   = p_rs.getString(7);
            }
        }

        /**
         * @return the values
         */
        public String getValues() {
            return values;
        }

        /**
         * @param values the values to set
         */
        public void setValues(String values) {
            this.values = values;
        }

        /**
         * @return the keys
         */
        public String getKeys() {
            return keys;
        }

        /**
         * @param keys the keys to set
         */
        public void setKeys(String keys) {
            this.keys = keys;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
