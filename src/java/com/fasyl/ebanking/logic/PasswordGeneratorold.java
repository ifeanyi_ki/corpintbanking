
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class PasswordGeneratorold extends ProcessClass {
    private String olduser         = null;
    StringBuffer   sSql            = null;
    private String search_cust_qry = "select a.cod_usr_id,a.email,a.cod_usr_name,"
                                     + "a.cod_usr_branch,a.cod_lang,a.cust_no from SM_USER_ACCESS a  ";
    private String updQry =
        "update SM_USER_ACCESS set cod_usr_pwd=?,flg_status='A',maint_date=sysdate where cod_usr_id=? ";
    private String    updQry2 = "update SM_USER_ACCESS set email=?,cod_usr_id=?,maint_date=sysdate where cod_usr_id=? ";
    Connection        con;
    private String    email;
    PreparedStatement pstmt;
    private ResultSet rs;
    private String    taskId;
    private String    userid;

    public PasswordGeneratorold() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.email = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0, false,
                null);
        this.userid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customername", 0,
                false, null);

        if ((inst_data.taskId.equals("10111")) || (inst_data.taskId.equals("10311"))
                || (inst_data.taskId.equals("10411"))) {
            this.userid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0,
                    false, null);
            this.email = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "email", 0, false,
                    null);
        }    // this was done as a result of not generalising the document id in jsp pages.

        if (inst_data.taskId.equals("104111")) {
            this.olduser = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "userold", 0,
                    false, null);
        }

        this.taskId = inst_data.taskId;
        sSql        = new StringBuffer(search_cust_qry);

        if ((taskId.equals("1011")) || (taskId.equals("1031")) || (taskId.equals("1041"))) {
            if (email.equals("") &&!(email.equals(""))) {
                System.out.println("inside email is null");
                sSql.append("where a.cod_usr_id like '" + userid + "%'");
            } else if (!(email.equals("")) && userid.equals("")) {
                System.out.println("inside userid is null");
                sSql.append("where a.email like '" + email + "%'");
            } else if (!(email.equals("") && userid.equals(""))) {
                System.out.println("inside both  are not null");
                sSql.append("where a.cod_usr_id like '" + userid + "%' and a.email like '" + email + "%'");
            } else {
                System.out.println("inside both  are null");
            }
        } else {
            sSql.append("where a.cod_usr_id like '" + userid + "%'");
        }

        // }
    }

    protected void initializeVar() {
        this.email   = null;
        this.userid  = null;
        this.olduser = null;
        sSql         = new StringBuffer("");
        pstmt        = null;
        rs           = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        HashMap hashMap = null;
        String  xmlText = null;

        con = com.fasyl.ebanking.db.DataBase.getConnection();
        initializeVar();

        try {
            setInstanceVar(instData);
            System.out.println(sSql + "why");

            Element results = new Element("ROWSET");

            if (!((instData.taskId.equals("101111")) || (instData.taskId.equals("104111")))) {    // /this is not for the pages that will display final result of set of operations in user management
                pstmt = con.prepareStatement(sSql.toString());
                rs    = pstmt.executeQuery();

                // Element results = new Element("ROWSET");

                /*
                 * if (!(rs.next())){
                 * System.out.println("rs is null");
                 * instData.taskId="usernotfound";// this suppose to be caught as exception but cos of the time
                 * }
                 */
                while (rs.next()) {
                    Element result  = new Element("ROW");
                    Element xuserid = new Element("USERID");

                    xuserid.setText(rs.getString("cod_usr_id"));

                    Element xemail = new Element("EMAIL");

                    xemail.setText(rs.getString("email"));

                    Element xusername = new Element("USERNAME");

                    xusername.setText(rs.getString("cod_usr_name"));

                    Element userbranch = new Element("USERBRANCH");

                    userbranch.setText(rs.getString("cod_usr_branch"));

                    Element lang = new Element("LANG");

                    lang.setText(rs.getString("cod_lang"));

                    Element xcustno = new Element("CUSTNO");

                    xcustno.setText(rs.getString("cust_no"));
                    result.addContent(xuserid);
                    result.addContent(xemail);
                    result.addContent(xusername);
                    result.addContent(userbranch);
                    result.addContent(lang);
                    result.addContent(xcustno);
                    results.addContent(result);
                }

                // results.addContent(results);
            } else {

                // Element results = new Element("ROWSET");
                if (true) {    // write code to ping server
                    String password = Utility.genaratePassword();

                    System.out.println("Before encryption " + password);
                    password = Encryption.encrypt(password);

                    if (instData.taskId.equals("104111")) {
                        System.out.println("inside 104111");
                        pstmt = con.prepareStatement(this.updQry2);
                        System.out.println(this.email + this.userid + this.olduser);
                        pstmt.setString(1, this.email);
                        pstmt.setString(2, this.userid.toUpperCase().trim());
                        pstmt.setString(3, this.olduser.toUpperCase().trim());
                    } else {
                        pstmt = con.prepareStatement(this.updQry);
                        pstmt.setString(1, password);
                        pstmt.setString(2, this.userid.toUpperCase().trim());
                    }

                    int upd = pstmt.executeUpdate();

                    System.out.println("This is the number of rows updated " + upd);

                    if (upd > 0) {

                        // writecode to send mail.
                        Element result  = new Element("ROW");
                        Element xuserid = new Element("PASSWORD");

                        if ((instData.taskId.equals("101111"))) {
                            xuserid.setText("The password has been sent to the email for activation");    // this suppose to contain message loaded from the database;
                        } else {
                            xuserid.setText("The operation has been done successfully");
                        }

                        result.addContent(xuserid);
                        results.addContent(result);
                    } else {

                        // write code for the exception thrown
                    }
                }
            }

            org.jdom.Document doc       = new org.jdom.Document(results);
            XMLOutputter      outputter = new XMLOutputter();

            outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
            xmlText = outputter.outputString(doc);
            System.out.println(xmlText);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        hashMap = new HashMap();
        hashMap.put("returnResult", xmlText);
        instData.result = hashMap;

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
