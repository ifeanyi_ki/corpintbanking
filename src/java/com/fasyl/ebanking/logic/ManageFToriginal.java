
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.AuditLogHandler;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class ManageFToriginal extends ProcessClass {
    private Element parent  = null;
    private String  xmlText = null;
    private String  DestAcctNo;
    private String  amt;
    private String  benacct;
    private String  ccy;
    private String  ftdebitacc;
    private String  ftlimt;
    private String  token;

    public ManageFToriginal() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        ftlimt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ftlimt", 0,
                true, null), "UTF-8");
        ftdebitacc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");
        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "DestAcctNo", 0, true, null), "UTF-8");
        benacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benacct",
                0, true, null), "UTF-8");
        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");
        amt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0,
                true, null), "UTF-8");
        parent = new Element("ROWSET");
    }

    protected void initializeVar() {
        ftlimt     = null;
        ftdebitacc = null;
        DestAcctNo = null;
        benacct    = null;
        ccy        = null;
        amt        = null;
        token      = null;
    }

  
    @Override
    public HashMap processRequest(DataofInstance instData) {
        
        try {
            setInstanceVar(instData);
            boolean results = validatePin(instData.userId, token);
            if(results){
           
//            statement = connect.prepareCall("{?=call fn_ibfundtransfer(?,?,?,?,?,?,?,?,?,?,?,?)}");
//            statement.registerOutParameter(1, OracleTypes.INTEGER);
//            statement.registerOutParameter(11, OracleTypes.CLOB);
//            statement.registerOutParameter(12, OracleTypes.VARCHAR);
//            statement.registerOutParameter(13, OracleTypes.CLOB);
//            statement.setString(2, instData.requestId);
//            statement.setString(3, instData.userId);
//            statement.setString(4, instData.custId);
//            statement.setString(5, Utility.getsqlCurrentDate2());
//            statement.setString(6, ftdebitacc);
//            statement.setString(7, DestAcctNo);
//            statement.setString(8, amt);
//            statement.setString(9, Utility.getsqlCurrentDate2());    // Utility.FormatCurrentDate().toString()
//            System.out.println(Utility.FormatCurrentDate().toString() + " ==== hmm ==== "
//                               + Utility.getsqlCurrentDate());
//            statement.setString(10, "Internal Transfer");
//
////      
//            statement.execute();
//            System.out.println("This is the error message " + statement.getString(11) + " hmm "
//                               + statement.getString(13));
//
//            int result = statement.getInt(1);
              HashMap map = ServiceFT(instData.requestId, instData.userId, instData.custId, ftdebitacc, DestAcctNo,
                                           amt);  

            if ((Integer)map.get("result") == 0) {
                
                String success=(String) LoadEBMessages.getMessageValue(FUND_TRANSFER_SUCESS_MESSAGE).getMessgaedesc();
                instData.result = responseMsg(success);
            } else {
                
                String  failiure =(String)map.get("message"); //statement.getString(11);
                   
                if (failiure == null) {
                    failiure = "Error during fund transfer";
                }
                instData.processStatus = FAIL_PROCESS_STATUS;
                instData.errormessage  = failiure;
                //instData.errorCode     = statement.getString(11);
                AuditLogHandler.updateErrorLog(instData);

                instData.result = responseMsg(failiure);

            }
            }else{
                instData.result = responseMsg(tokenfailureres);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        }
//        finally {
//            if ((statement != null) || (connect != null)) {
//                if (statement != null) {
//                    try {
//                        statement.close();
//                    } catch (Exception ex) {
//                        ex.printStackTrace(System.out);
//                    }
//                }
//
//                if (connect != null) {
//                    try {
//                        connect.close();
//                    } catch (Exception ex) {
//                        ex.printStackTrace(System.out);
//                    }
//                }
//            }
//        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
