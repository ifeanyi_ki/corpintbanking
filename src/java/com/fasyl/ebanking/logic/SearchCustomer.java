
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import com.fasyl.ebanking.util.XMLUtilty;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class SearchCustomer extends ProcessClass {
    private static final int    NBR_PARAMS                = 4;
    private static final String PROCEDURE_SEARCH_CUSTOMER = "FN_SEARCH_CUSTOMER";
    public static final String  SUCCESS                   = "false";
    private String              custno;
    private String              customerName;
    private Connection          dbConnection;
    private String              highCount;
    private String              lowCount;
    protected CallableStatement procStmt;
    private String              returnResult;

    public void initializeVar() {
        this.custno       = null;
        this.procStmt     = null;
        this.returnResult = null;
        this.customerName = null;
        this.highCount    = null;
        this.lowCount     = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.customerName = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customername",
                0, false, null);
        this.custno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0, false,
                null);
        this.lowCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "lowcount", 0, false,
                null);
        this.highCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "highcount", 0,
                false, null);
        System.out.println(customerName.toUpperCase());
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            instData.result = ExchangeMap(connect);

            // ArrayList list = null;
            int list = (XMLUtilty.getOne((String) instData.result.get("returnResult"), "CUSTOMER_NO")).size();

            System.out.println("This is getone result " + list);

            if (list <= 0) {
                HashMap errors = new HashMap();
                String  error  = USERNOTFOUND;

                errors.put("errorcode", error);
                instData.result = errors;

                return instData.result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();
        String  data   = ManageClob(procStmt.getClob(1));

        returnResult = data;
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SEARCH_CUSTOMER, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        int highcount = (this.highCount == null)
                        ? 20
                        : Integer.parseInt(this.highCount);
        int lowcount  = (this.lowCount == null)
                        ? 0
                        : Integer.parseInt(this.lowCount);

        procStmt.setString(2, custno);
        procStmt.setString(3, this.customerName.toUpperCase());
        procStmt.setInt(4, highcount);
        procStmt.setInt(5, lowcount);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
