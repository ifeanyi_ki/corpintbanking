
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class TransactionDetail extends ProcessClass {
    private static final String ACCOUNT_POSITION = "FN_GET_TRX_detail";
    private static final int    NBR_PARAMS       = 3;
    private String              account;
    private String              chequeAmount;
    //Connection                  connect;
    String                      dates;
    private String              isUsed;
    ArrayList                   list;
    private HashMap             preResult;
    private String              refNo;
    private String              returnResult;

    public TransactionDetail() {}

    private void initializeVar() {
        this.account      = null;
        this.refNo        = null;
        this.chequeAmount = null;
        this.returnResult = null;
        this.isUsed       = null;
        this.preResult    = null;
        dates             = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acno", 0, false,
                null);
        this.refNo = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false,
                null);;
        this.dates = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "dates", 0, false,
                null);;

        if (bDebugOn) {
            System.out.println(dates);
        }
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        // java.sql.Clob datas = procStmt.getClob(4);
        String data = this.ManageClob(procStmt.getClob(1));    // datas.getSubString(1, (int)datas.length());

        returnResult = data;
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.ACCOUNT_POSITION, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(2, this.account);
        procStmt.setString(3, this.refNo);
        procStmt.setString(4, this.dates);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
