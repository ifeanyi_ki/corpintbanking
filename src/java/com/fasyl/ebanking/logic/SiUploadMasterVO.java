/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import java.util.Date;

/**
 *
 * @author Administrator
 */
public class SiUploadMasterVO {

    public SiUploadMasterVO() {
    }
    private String branchcode;
    private String sourcecode;
    private String sourceref;
    private String detailref;
    private String Userrefno;
    private Date expirydate;
    private Date bookDate;
    private String counterParty;
    private String productType;// This will be sent from the browser
    private int serialNo;//must be generated from the application
    private String ACTIONCODEAMT;
    private char applychgsuxs;
    private String applychgpexc;
    private String applychgrejt;
    private String maxretrycount;
    private String minsweepamt;
    private String draccbr;//This will come from the browser
    private String draccount;//
    private String draccccy;
    private String siamtccy;
    private double siamt;
    private String craccbr;
    private String craccount;//
    private String craccccy;
    private String priority;//
    private String chargewhom;
    private String minbalaftersweep;
    private String internalremarks;
    private String iccfchanged;
    private String taxchanged;
    private String settlechanged;
    private String mischanged;
    private String convstatus;
    private String errmsg;






    /**
     * @return the branchcode
     */
    public String getBranchcode() {
        return branchcode;
    }

    /**
     * @param branchcode the branchcode to set
     */
    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    /**
     * @return the sourcecode
     */
    public String getSorucecode() {
        return sourcecode;
    }

    /**
     * @param sourcecode the sourcecode to set
     */
    public void setSorucecode(String sorucecode) {
        this.sourcecode = sorucecode;
    }

    /**
     * @return the sourceref
     */
    public String getSourceref() {
        return sourceref;
    }

    /**
     * @param sourceref the sourceref to set
     */
    public void setSourceref(String sourceref) {
        this.sourceref = sourceref;
    }

    /**
     * @return the detailref
     */
    public String getDetailref() {
        return detailref;
    }

    /**
     * @param detailref the detailref to set
     */
    public void setDetailref(String detailref) {
        this.detailref = detailref;
    }

    /**
     * @return the Userrefno
     */
    public String getUserrefno() {
        return Userrefno;
    }

    /**
     * @param Userrefno the Userrefno to set
     */
    public void setUserrefno(String Userrefno) {
        this.Userrefno = Userrefno;
    }

    /**
     * @return the expirydate
     */
    public Date getExpirydate() {
        return expirydate;
    }

    /**
     * @param expirydate the expirydate to set
     */
    public void setExpirydate(Date expirydate) {
        this.expirydate = expirydate;
    }

    /**
     * @return the bookDate
     */
    public Date getBookDate() {
        return bookDate;
    }

    /**
     * @param bookDate the bookDate to set
     */
    public void setBookDate(Date bookDate) {
        this.bookDate = bookDate;
    }

    /**
     * @return the counterParty
     */
    public String getCounterParty() {
        return counterParty;
    }

    /**
     * @param counterParty the counterParty to set
     */
    public void setCounterParty(String counterParty) {
        this.counterParty = counterParty;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the serialNo
     */
    public int getSerialNo() {
        return serialNo;
    }

    /**
     * @param serialNo the serialNo to set
     */
    public void setSerialNo(int serialNo) {
        this.serialNo = serialNo;
    }

    /**
     * @return the ACTIONCODEAMT
     */
    public String getACTIONCODEAMT() {
        return ACTIONCODEAMT;
    }

    /**
     * @param ACTIONCODEAMT the ACTIONCODEAMT to set
     */
    public void setACTIONCODEAMT(String ACTIONCODEAMT) {
        this.ACTIONCODEAMT = ACTIONCODEAMT;
    }

    /**
     * @return the applychgsuxs
     */
    public char getApplychgsuxs() {
        return applychgsuxs;
    }

    /**
     * @param applychgsuxs the applychgsuxs to set
     */
    public void setApplychgsuxs(char applychgsuxs) {
        this.applychgsuxs = applychgsuxs;
    }

    /**
     * @return the applychgpexc
     */
    public String getApplychgpexc() {
        return applychgpexc;
    }

    /**
     * @param applychgpexc the applychgpexc to set
     */
    public void setApplychgpexc(String applychgpexc) {
        this.applychgpexc = applychgpexc;
    }

    /**
     * @return the applychgrejt
     */
    public String getApplychgrejt() {
        return applychgrejt;
    }

    /**
     * @param applychgrejt the applychgrejt to set
     */
    public void setApplychgrejt(String applychgrejt) {
        this.applychgrejt = applychgrejt;
    }

    /**
     * @return the maxretrycount
     */
    public String getMaxretrycount() {
        return maxretrycount;
    }

    /**
     * @param maxretrycount the maxretrycount to set
     */
    public void setMaxretrycount(String maxretrycount) {
        this.maxretrycount = maxretrycount;
    }

    /**
     * @return the minsweepamt
     */
    public String getMinsweepamt() {
        return minsweepamt;
    }

    /**
     * @param minsweepamt the minsweepamt to set
     */
    public void setMinsweepamt(String minsweepamt) {
        this.minsweepamt = minsweepamt;
    }

    /**
     * @return the draccbr
     */
    public String getDraccbr() {
        return draccbr;
    }

    /**
     * @param draccbr the draccbr to set
     */
    public void setDraccbr(String draccbr) {
        this.draccbr = draccbr;
    }

    /**
     * @return the draccount
     */
    public String getDraccount() {
        return draccount;
    }

    /**
     * @param draccount the draccount to set
     */
    public void setDraccount(String draccount) {
        this.draccount = draccount;
    }

    /**
     * @return the draccccy
     */
    public String getDraccccy() {
        return draccccy;
    }

    /**
     * @param draccccy the draccccy to set
     */
    public void setDraccccy(String draccccy) {
        this.draccccy = draccccy;
    }

    /**
     * @return the siamtccy
     */
    public String getSiamtccy() {
        return siamtccy;
    }

    /**
     * @param siamtccy the siamtccy to set
     */
    public void setSiamtccy(String siamtccy) {
        this.siamtccy = siamtccy;
    }

    /**
     * @return the siamt
     */
    public double getSiamt() {
        return siamt;
    }

    /**
     * @param siamt the siamt to set
     */
    public void setSiamt(double siamt) {
        this.siamt = siamt;
    }

    /**
     * @return the craccbr
     */
    public String getCraccbr() {
        return craccbr;
    }

    /**
     * @param craccbr the craccbr to set
     */
    public void setCraccbr(String craccbr) {
        this.craccbr = craccbr;
    }

    /**
     * @return the craccount
     */
    public String getCraccount() {
        return craccount;
    }

    /**
     * @param craccount the craccount to set
     */
    public void setCraccount(String craccount) {
        this.craccount = craccount;
    }

    /**
     * @return the craccccy
     */
    public String getCraccccy() {
        return craccccy;
    }

    /**
     * @param craccccy the craccccy to set
     */
    public void setCraccccy(String craccccy) {
        this.craccccy = craccccy;
    }

    /**
     * @return the priority
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * @return the chargewhom
     */
    public String getChargewhom() {
        return chargewhom;
    }

    /**
     * @param chargewhom the chargewhom to set
     */
    public void setChargewhom(String chargewhom) {
        this.chargewhom = chargewhom;
    }

    /**
     * @return the minbalaftersweep
     */
    public String getMinbalaftersweep() {
        return minbalaftersweep;
    }

    /**
     * @param minbalaftersweep the minbalaftersweep to set
     */
    public void setMinbalaftersweep(String minbalaftersweep) {
        this.minbalaftersweep = minbalaftersweep;
    }

    /**
     * @return the internalremarks
     */
    public String getInternalremarks() {
        return internalremarks;
    }

    /**
     * @param internalremarks the internalremarks to set
     */
    public void setInternalremarks(String internalremarks) {
        this.internalremarks = internalremarks;
    }

    /**
     * @return the iccfchanged
     */
    public String getIccfchanged() {
        return iccfchanged;
    }

    /**
     * @param iccfchanged the iccfchanged to set
     */
    public void setIccfchanged(String iccfchanged) {
        this.iccfchanged = iccfchanged;
    }

    /**
     * @return the taxchanged
     */
    public String getTaxchanged() {
        return taxchanged;
    }

    /**
     * @param taxchanged the taxchanged to set
     */
    public void setTaxchanged(String taxchanged) {
        this.taxchanged = taxchanged;
    }

    /**
     * @return the settlechanged
     */
    public String getSettlechanged() {
        return settlechanged;
    }

    /**
     * @param settlechanged the settlechanged to set
     */
    public void setSettlechanged(String settlechanged) {
        this.settlechanged = settlechanged;
    }

    /**
     * @return the mischanged
     */
    public String getMischanged() {
        return mischanged;
    }

    /**
     * @param mischanged the mischanged to set
     */
    public void setMischanged(String mischanged) {
        this.mischanged = mischanged;
    }

    /**
     * @return the convstatus
     */
    public String getConvstatus() {
        return convstatus;
    }

    /**
     * @param convstatus the convstatus to set
     */
    public void setConvstatus(String convstatus) {
        this.convstatus = convstatus;
    }

    /**
     * @return the errmsg
     */
    public String getErrmsg() {
        return errmsg;
    }

    /**
     * @param errmsg the errmsg to set
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}
