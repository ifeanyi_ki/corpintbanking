
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class ManageBulkFT extends ProcessClass {

    private String branch = null;
    private Element parent = null;
    private String xmlText = null;
    private String DestAcctNo;
    private String amt;
    private String benacct;
    private String ccy;
    private String ftdebitacc;
    private String ftlimt;
    private String token;
    private String txnCode;

    // 20141218 narration patch
    private String narration;

    private static final Logger logger = Logger.getLogger(ManageBulkFT.class.getName());

    private String deSourceCode;

    public ManageBulkFT() {
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        ftlimt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ftlimt", 0,
                true, null), "UTF-8");
        ftdebitacc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");
        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");
        benacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benacct",
                0, true, null), "UTF-8");
        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");
        amt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0,
                true, null), "UTF-8");
        branch = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "branch", 0,
                true, null), "UTF-8");

        // 20141218 narration patch
        narration = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "narration", 0,
                true, null), "UTF-8");

        deSourceCode = (String) LoadBasicParam.getDataValuenew("DESOURCECODE");
        
        txnCode = (String) LoadBasicParam.getDataValuenew("IB_TXN_CODE");

        System.out.println("Txn Code for Bulk:" + txnCode);

        logger.info(" deSourceCode: " + deSourceCode);

        logger.info(" ManageBulk FT Params: " + " ftdebitacc: " + ftdebitacc + " DestAcctNo: " + DestAcctNo + " amt: " + amt + " benacct: " + benacct + " narration: " + narration);

        parent = new Element("ROWSET");
    }

    protected void initializeVar() {
        ftlimt = null;
        ftdebitacc = null;
        DestAcctNo = null;
        benacct = null;
        ccy = null;
        amt = null;
        token = null;
        branch = null;

        narration = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        boolean result = false;

        try {
            setInstanceVar(instData);
            result = validatePin(instData.userId, token);

            if (result) {
                if (instData.taskId.equals("70211")) {
                    instData.result = verifyRequest();
                    return instData.result;
                }
           //     HashMap resultz = ServiceBulkFT(instData.requestId,branch,benacct,ftdebitacc,instData.custId,amt,ccy);

                // 20141218 narration patch
                HashMap resultz = ServiceBulkFT(instData.requestId, branch, benacct, ftdebitacc, instData.custId, amt, ccy, narration, deSourceCode, txnCode);

                if ((Integer) resultz.get("result") == 0) {
                    Element row = new Element("ROW");
                    Element success = new Element("SUCCESS");

                    success.setText(
                            (String) LoadEBMessages.getMessageValue(FUND_TRANSFER_SUCESS_MESSAGE).getMessgaedesc());
                    row.addContent(success);
                    parent.addContent(row);
                    instData.result = this.generateXML(parent, xmlText);
                    logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, success.getText());
                } else {
                    Element row = new Element("ROW");
                    Element success = new Element("SUCCESS");
                    String failiure = (String) resultz.get("message");

                    if (failiure == null || failiure == "<ROWSET></ROWSET>") {
                        failiure = "Error during bulk fund transfer";
                    }

                    success.setText(failiure);
                    row.addContent(success);
                    parent.addContent(row);
                    instData.result = this.generateXML(parent, xmlText);
                    logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, failiure);

                    // instData.result = Utility.processError(instData,FUND_TRANSFER_ERROR_MESSAGE );
                }
            } else {
                //instData.result = Utility.processError(instData, MISMATCH_TOKEN);
                //instData.taskId="33210";
//                instData.result = responseMsg(tokenfailureres);
                instData.result = Utility.processError(instData, tokenfailiuercode);
            }
            if (instData.taskId.equals("702110")) {
                clearToken(instData.userId);
            }
        } catch (Exception ex) {
            clearToken(instData.userId);
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        }
//        finally {
//            if (connect != null) {
//                try {
//                    connect.close();
//
//                    // pstmt.close();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//
//            if (statement != null) {
//                try {
//                    statement.close();
//
//                    // pstmt.close();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }

        return instData.result;
    }

    HashMap verifyRequest() {

        //parent  = new Element("ROWSET");
        Element row = new Element("ROW");

        Element benaccts = new Element("DESTACCTNO");
        benaccts.setText(this.benacct);
        row.addContent(benaccts);

        Element amts = new Element("AMOUNT");
        amts.setText(this.amt);
        row.addContent(amts);

        Element ccys = new Element("CCY");
        ccys.setText(this.ccy);
        row.addContent(ccys);

        Element debitacc = new Element("DRACCT");
        debitacc.setText(this.ftdebitacc);
        row.addContent(debitacc);

        Element tokens = new Element("ACTION");
        tokens.setText(this.token);
        row.addContent(tokens);

        // 20141218 narration patch
        Element narrations = new Element("NARRATION");
        narrations.setText(this.narration);
        row.addContent(narrations);

        parent.addContent(row);

        HashMap result = generateXML(parent, this.xmlText);
        return result;
    }

    public String logFtTransactions(String requestId, String userName, String userId, String custId, String ftdebitacc, String DestAcctNo, String amt, String failiure) {
        String query = "insert into fundtransfer_log (Request_id,Account_name,User_Id ,Customer_Id ,Ft_Debit_Acc ,Dest_Acct_No ,Amount ,Message, Transaction_Time,ID)values(?,?,?,?,?,?,?,?,?,ft_sequence.nextval)";
        String failedFtResult = null;
        System.out.println("About to log successful Bulk FT >>>>>> " + requestId);

        // 20150112  report
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            try{
                con.setAutoCommit(false);
            }catch(SQLException sqlExc) {
                sqlExc.printStackTrace();
            }
            stmt = con.prepareStatement(query);

            stmt.setString(1, requestId);
            stmt.setString(2, userName);
            stmt.setString(3, userId);
            stmt.setString(4, custId);
            stmt.setString(5, ftdebitacc);
            stmt.setString(6, DestAcctNo);
            stmt.setString(7, amt);
            stmt.setString(8, failiure);
            stmt.setString(9, Utility.getsqlCurrentDate2());

            stmt.execute();
            System.out.println("about to commit");
            con.commit();

            System.out.print("the date of transaction is >>>>>" + Utility.getsqlCurrentDate2());
            System.out.print("transaction was initiated by >>>>>" + userName);

        } catch (Exception ex) {
            ex.printStackTrace();

            // return instData.result;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        System.out.println("About to log failed Bulk FT >>>>>> " + failiure);
        return failedFtResult;
        //  return instData.result;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
