
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class initFTold extends Processes {
    private static final int    NBR_PARAMS        = 2;
    private static final String PROCEDURE_INIT_FT = "FN_GET_ACCT_DETAIL";
    Connection                  connect           = null;
    private String              custno;
    DataofInstance              instancedata;
    private String              returnResult;

    public initFTold() {}

    public initFTold(String custno) {
        this.custno = custno;

    }

    public void initializeVar() {
        returnResult = null;
        custno       = null;
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connects = com.fasyl.ebanking.db.DataBase.getConnection();

        System.out.println(" ******Inside process request **** ");
        initializeVar();
        custno          = instData.custId;
        instData.result = ExchangeMap(connects);

        if (connects != null) {
            try {
                connect.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return instData.result;
    }

    public void getBeneficiary() {
        String custno = instancedata.custId;
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);

        // procStmt.getString(3);
        result.put("returnResult", returnResult);

        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_INIT_FT, NBR_PARAMS);
    }

    protected void prepareExecProcedure(Connection connnect) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = connnect.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    protected String executeProcedure() throws SQLException {
        System.out.println(" This is the customer no ======= " + custno);
        procStmt.setString(2, "N");
        procStmt.setString(3, custno);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
