
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author baby
 */
public class FundTransfer extends ProcessClass {
    private String  branch  = null;
    private Element parent  = null;
    private String  xmlText = null;
    private String  DestAcctNo;
    private String  amt;
    private String  benacct;
    private String  ccy;
    private String  ftdebitacc;
    private String  ftlimt;
    private String  token;
    
    
     private static final Logger logger = Logger.getLogger(FundTransfer.class.getName());
 

    public FundTransfer() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

       // ftlimt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ftlimt", 0,
          //      true, null), "UTF-8");
        ftdebitacc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");
       
//        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
//                "ftdebitacc", 0, true, null), "UTF-8");
        
         DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "DestAcctNo", 0, true, null), "UTF-8");
        
//        benacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benacct",
//                0, true, null), "UTF-8");
         
         // 20141212 
         benacct = DestAcctNo;
        
        
        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");
        amt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0,
                true, null), "UTF-8");
//        branch = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "branch", 0,
//                true, null), "UTF-8");
        parent = new Element("ROWSET");
        
        branch = "099";
        
        logger.info(" request params: branch " + branch + " amt: " + amt + " ccy: " + ccy + " ftdebitacc: " + ftdebitacc + " DestAcctNo: " + benacct + " customer id: " + inst_data.custId);
        
       // logger.info(" request params: " + inst_data.requestHashTable.toString());
        
        
    }

    protected void initializeVar() {
        ftlimt     = null;
        ftdebitacc = null;
        DestAcctNo = null;
        benacct    = null;
        ccy        = null;
        amt        = null;
        token      = null;
        branch     = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
     //   Connection        connect   = com.fasyl.ebanking.db.DataBase.getConnection();
        
        logger.info(" now using tbmbfcc con");
         Connection  connect = null; 
      
        
        boolean           result    = false;
        CallableStatement statement = null;

        try {
            setInstanceVar(instData);
            result = validatePin(instData.userId, token);

            if (result) {
                
                logger.info("{?=call ebanking_ft_upload.fn_manage_ft(?,?,?,?,?,?,?,?,?,?)}");
                connect  = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
                statement = connect.prepareCall("{?=call ebanking_ft_upload.fn_manage_ft(?,?,?,?,?,?,?,?,?,?)}");
                statement.registerOutParameter(1, OracleTypes.INTEGER);
                statement.registerOutParameter(10, OracleTypes.VARCHAR);
                statement.registerOutParameter(11, OracleTypes.VARCHAR);
                statement.setString(2, "DE_UPLOAD");
                statement.setString(3, instData.sessionId);
                statement.setString(4, branch);
                statement.setString(5, benacct);    // bene grp
                statement.setString(6, ftdebitacc);
              //  statement.setString(7, DestAcctNo);   
                statement.setString(7, instData.custId);     
                statement.setString(8, ccy);
                statement.setString(9, amt);

                // System.out.println( statement.getString(1)+" "+ statement.getString(4)+" "+ statement.getString(5));
                statement.execute();

                int resultz = statement.getInt(1);
                
                logger.info(" result: " + resultz);
                logger.info(" error code: " + statement.getString(10));
                logger.info(" error msg: " + statement.getString(11));
                

                if (resultz == 0) {
                    Element row     = new Element("ROW");
                    Element success = new Element("SUCCESS");

                    success.setText(
                        (String) LoadEBMessages.getMessageValue(FUND_TRANSFER_SUCESS_MESSAGE).getMessgaedesc());
                    row.addContent(success);
                    parent.addContent(row);
                    instData.result = this.generateXML(parent, xmlText);
                } else {
                    instData.result = Utility.processError(instData, FUND_TRANSFER_ERROR_MESSAGE);
                }
            } else {
                instData.result = Utility.processError(instData, MISMATCH_TOKEN);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
