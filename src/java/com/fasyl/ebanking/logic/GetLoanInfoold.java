
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

/**
 *
 * @author baby
 */
public class GetLoanInfoold extends ProcessClass {
    public static final String GET_DETAIL_LOAN_INFO =
        "select component_name,schedule_st_date, schedule_due_date,amount_due,amount_overdue,"
        + "accrued_amount,amount_settled from CLTB_ACCOUNT_SCHEDULES order by schedule_due_date "
        + "where account_number=? ";    // and AMO

//  public static final String GET_DETAIL_LOAN_INFO = "select c.contract_ref_no,c.branch,"+
//"c.counterparty,c.currency,c.lcy_amount,a.due_date,a.amount_due,a.original_due_date,a.component, "+
//"a.amount_settled,b.principal_outstanding_bal,c.dr_setl_ac "+
//"from cstb_amount_due a,LDTB_CONTRACT_BALANCE b,LDTB_CONTRACT_MASTER c  "+
//"where a.contract_ref_no=b.contract_ref_no and a.contract_ref_no=c.contract_ref_no "+
//"and c.contract_status='A' and a.due_date=(select max(due_date) from cstb_amount_due where counterparty=?) "+
//"and c.counterparty=? and c.module='LD' and a.component='PRINCIPAL' and c.product_type='L'  ";

    public static final String GET_LOAN_INFO =
        "select account_number,customer_id,amount_disbursed,dr_prod_ac,"
        + " book_date,value_date,maturity_date,currency from CLTB_ACCOUNT_APPS_MASTER  "    // cust_acct b
        + " where customer_id=?  ";
    private static String GET_LOAN_REPAYMENT =
        "select component_name,schedule_st_date, schedule_due_date,amount_due,amount_overdue,"
        + "accrued_amount,amount_settled from CLTB_ACCOUNT_SCHEDULES  "
        + "where account_number=? order by schedule_due_date";    // and AMOUNT_SETTLED!=0"
    //private Connection        connect = null;
    private PreparedStatement pstmt   = null;
    private String            refno   = null;
    private String            xmlText = null;
    DataofInstance            instdata;
    private Element           parent;

    public GetLoanInfoold() {}

    protected void initializeVar() {
        pstmt   = null;
        xmlText = null;
        refno   = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent     = new Element("ROWSET");
        this.refno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false, null);
        instdata   = inst_data;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connects = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet  rs       = null;
        boolean    result   = false;

        this.initializeVar();

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);

            if (instData.taskId.equals("2051")) {
                pstmt = connects.prepareStatement(GET_DETAIL_LOAN_INFO);
                pstmt.setString(1, instData.custId);
                pstmt.setString(2, instData.custId);
            } else if (instData.taskId.equals("205")) {
                pstmt = connects.prepareStatement(GET_LOAN_INFO);
                pstmt.setString(1, instData.custId);
            } else if (instData.taskId.equals("2052")) {
                System.out.println("About to get loan repayment " + refno);
                pstmt = connects.prepareStatement(GET_LOAN_REPAYMENT);
                pstmt.setString(1, refno);
            }

            rs = pstmt.executeQuery();

            if ((parent == null) || parent.equals("")) {
                System.out.println(" Parent is null");
            }

            parent = this.convertToXml(parent, "ROW", rs);

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result          = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            } else {
                result = true;

                if (instData.result == null) {
                    instData.result = generateXML(parent, this.xmlText);

                    if (!result
                            || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                                   + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                    }
                }
            }
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
            ex.printStackTrace();

            return instData.result;
        } finally {
            
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connects != null) {
                try {
                    connects.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
