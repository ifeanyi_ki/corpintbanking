/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import org.jdom.Element;

/**
 *
 * @author baby
 */
public class ValidateBeneficiaryInfo extends ProcessClass {

    public static final String validateBenAcct = "select 'x' from eb_beneft_details where  "
            + "destination_acct=? and customer_no=? and beneftgrpname=? ";//
    private Element parent = null;
    private String xmlText = null;
    private DataofInstance instancedata;
    private String acct;
    private String benname;
    private String flexvalidation = "SELECT a.branch_code,a.ac_desc, a.cust_ac_no, a.cust_no, a.ccy, a.account_class "
            + " FROM sttm_cust_account a where a.cust_ac_no=?";

    public ValidateBeneficiaryInfo() {
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent = new Element("ROWSET");
        ReadRequestAsStream decodes = new ReadRequestAsStream();
        acct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "acct", 0, true, null), "UTF-8");
        benname = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "benname", 0, true, null), "UTF-8");
      
    }

    protected void initializeVar() {
        xmlText = null;
       acct=null;
        benname=null;
        xmlText=null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        
        this.initializeVar();
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        boolean result = false;

        try {
            this.setInstanceVar(instData);
            pstmt = connect.prepareStatement(validateBenAcct);
            pstmt.setString(1, acct);
            pstmt.setString(2, instData.custId);
            pstmt.setString(3, benname);
            rs = pstmt.executeQuery();
            if (rs.next()) {
               instData.result = Utility.processError(instData, DUPBENACCT);

            } else {
                
                
                 result = true;
                pstmt = connect.prepareStatement(flexvalidation);
                pstmt.setString(1, acct);
                System.out.println("<<<<<<<<<<<<<< About to execute the query >>>>>>>>>>>>>");
                rs = pstmt.executeQuery();
                parent = this.convertToXml(parent, "ROW", rs);
                if ((parent == null)) {
                    System.out.println("<<<<<<<<<<<<<< The query is null >>>>>>>>>>>>>");
                    result = false;
                    instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                    return instData.result;
                } else {
                    
                    instData.result = generateXML(parent, this.xmlText);
                    System.out.println("<<<<<<<<<<<<<< After generating xml >>>>>>>>>>>>>");
                  
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return instData.result;
    }
}
