/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Random;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Administrator
 */
public class AddSI2 extends ProcessClass {

     private static final Logger logger = Logger.getLogger(AddSI2.class.getName());
 
    
    //boolean bDebugOn = true;
    HashMap procResult = null;
    String userRef = "";
    String endianesDate = "";
    private String siType = null;
    private String counterparty = null;
    private String siSourceAcct = null;
    private String siDestAccount = null;
    private String siAmount = null;
    private int siFreqExec = 0;
    private Date siFirstExecDate = null;
    private Date siFinalExecDate = null;
    private String siNarration = null;
    private String siCurrency = null;
    private String siPriority = null;
    private String drBranchCode = null;
    private String crBranchCode = null;
    private final String PROCEDURE_SI_UPLOAD = "EBANKING_SI.UPLOAD_SI";
    protected CallableStatement procStmt;
    private static final int NBR_PARAMS = 9;
    public static Random sequenceIdRandom = new Random();
    public final String SEL_SI_PRD = "SELECT b.product_code, b.product_type, b.exec_days, b.exec_mths, b.exec_yrs,"
            + "b.action_code_amount, b.cal_hol_excp, b.rate_type, b.max_retry_count, "
            + "b.min_sweep_amt, b.processing_time, b.instruction_code, "
            + "b.transfer_type, b.si_type, b.referral_required "
            + "FROM sitm_product_prf b where b.product_code =? ";
    //Connection cons = null;
    Connection con2 = null;
    
    private String headOfficeBranch;
    private String flexUserId;
    private String siSourceCode;
    private String siProductcode;
    

    public void pr_init(Connection con2) throws Exception {

        CallableStatement call = null;

        try {

            logger.info("{call global.pr_init(?,?)}");
            
            logger.info(" headOfficeBranch: " + headOfficeBranch);
            logger.info(" flexUserId: " + flexUserId);
            
            
            call = con2.prepareCall("{call global.pr_init(?,?)}");
            call.setString(1, headOfficeBranch);
            call.setString(2, flexUserId);

            call.execute();

        } catch (SQLException ex) {
            ex.getMessage();
        }
    }
    public static final String INS_INTO_REQD_TABLE = "INSERT INTO SITB_UPLOAD_MASTER "
            + "(                       "
            + "BRANCH_CODE,        "
            + "SOURCE_CODE,        "
            + "SOURCE_REF,         "
            + "DETAIL_REF,         "
            + "USER_REF_NUMBER,    "
            + "SI_EXPIRY_DATE,     "
            + "BOOK_DATE,          "
            + "PRODUCT_CODE,       "
            + "COUNTERPARTY,       "
            + "PRODUCT_TYPE,       "
            + "SERIAL_NO,          "
            + "ACTION_CODE_AMT,    "
            + "APPLY_CHG_SUXS,     "
            + "APPLY_CHG_PEXC,     "
            + "APPLY_CHG_REJT,     "
            + "MAX_RETRY_COUNT,    "
            + "MIN_SWEEP_AMT,      "
            + "DR_ACC_BR,          "
            + "DR_ACCOUNT,         "
            + "DR_ACC_CCY,         "
            + "SI_AMT_CCY,         "
            + "SI_AMT,             "
            + "CR_ACC_BR,          "
            + "CR_ACCOUNT,         "
            + "CR_ACC_CCY,         "
            + "PRIORITY,           "
            + "CHARGE_WHOM,        "
            + "MIN_BAL_AFTER_SWEEP,"
            + "INTERNAL_REMARKS,   "
            + "ICCF_CHANGED,       "
            + "TAX_CHANGED,        "
            + "MIS_CHANGED,        "
            + "CONV_STATUS,        "
            + "SETTLE_CHANGED,     "
            + "ERR_MSG             "
            + 
            " ) VALUES (   ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?    )";
    public static final String INS_INTO_INST_TABLE = "INSERT INTO SITB_UPLOAD_INSTRUCTION( "
            + "BRANCH_CODE,                "
            + "SOURCE_CODE,                "
            + "SOURCE_REF,                 "
            + "PRODUCT_CODE,               "
            + "PRODUCT_TYPE,               "
            + "SI_TYPE,                    "
            + "CAL_HOL_EXCP,               "
            + "RATE_TYPE,                  "
            + "EXEC_DAYS,                  "
            + "EXEC_MTHS,                  "
            + "EXEC_YRS,                   "
            + "FIRST_EXEC_DATE,            "
            + "NEXT_EXEC_DATE,             "
            + "FIRST_VALUE_DATE,           "
            + "NEXT_VALUE_DATE,            "
            + "MONTH_END_FLAG,             "
            + "PROCESSING_TIME,            "
            + "USER_INST_NO,               "
            + "INST_STATUS,                "
            + "AUTH_STATUS,                "
            + "LATEST_VERSION_DATE,        "
            + "BOOK_DATE,                  "
            + "SERIAL_NO,                  "
            + "COUNTERPARTY,               "
            + "LATEST_CYCLE_NO,            "
            + "LATEST_CYCLE_DATE,          "
            + "CONV_STATUS,                "
            + "ERR_MSG                    "
            +
            ") VALUES(?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ? ) ";
    private String serialresult = "";
    private String SourceRef = "";
    private SIObject siobject = null;
    //private CallableStatement pstmt = null;
    private Element parent = null;
    private String xmlText = null;
    

    public AddSI2() {
    }

    public java.sql.Date currentDate() {

        java.util.Date date = new java.util.Date();
        java.sql.Date sqldat = new java.sql.Date(date.getTime());
        return sqldat;

    }

    protected void initializeVar() {
        parent = null;
        xmlText = null;
        this.SourceRef = null;
        this.endianesDate = null;
        this.procResult = null;
        this.userRef = null;
        this.siobject = null;
        this.serialresult = null;
        this.siType = null;
        this.counterparty = null;
        this.siSourceAcct = null;
        this.siDestAccount = null;
        this.siAmount = null;
        this.siFreqExec = 0;
        this.siFirstExecDate = null;
        this.siFinalExecDate = null;
        this.siNarration = null;
        this.siCurrency = null;
        this.siPriority = null;
        this.drBranchCode = null;
        this.crBranchCode = null;

    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.siType = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.counterparty = inst_data.custId;//ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.siSourceAcct = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "dSrcAcctNo", 0, false, null);
        this.siDestAccount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "DestAcctNo", 0, false, null);
        this.siAmount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfAmt", 0, false, null);
        this.siFreqExec = Integer.parseInt(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFreqDesc", 0, false, "1"));
        this.siFirstExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "FirstExecDate", 0, false, null));
        this.siFinalExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFinalDate", 0, false, null));
        this.siNarration = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiNarration", 0, false, null);
        this.siCurrency = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfCurr1", 0, false, null);
        this.siPriority = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiPriority", 0, false, null);
        this.drBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "accountBranch", 0, false, null);
        this.crBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "crbrcode", 0, false, null);
        parent = new Element("ROWSET");

        
       flexUserId = (String) LoadBasicParam.getDataValuenew("USERID");
       headOfficeBranch = (String) LoadBasicParam.getDataValuenew("DEBRANCH");
       siSourceCode = (String) LoadBasicParam.getDataValuenew("SISOURCECODE");
       siProductcode = (String) LoadBasicParam.getDataValuenew("SIPRODUCTCODE");
       
       logger.info(" SI product code: " + siProductcode);
        logger.info(" SI source code: " + siSourceCode);
        logger.info(" branch code: " + headOfficeBranch);
                  
         logger.info(" Flexcube user id: " + flexUserId);
         logger.info(" customer id: " + inst_data.custId);
        
    }

    public HashMap processRequest(DataofInstance instData) {
        
        logger.info(" ******Inside process request **** ");
        
        System.out.println(" ******Inside process request **** ");
        HashMap preResult = null;
        Connection connects = null;
        initializeVar();
        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        synchronized (this) {
            
            boolean loaddata = loadData();
            //loaddata=false;
            if (loaddata) {
                connects = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();

                try {
                    
                     
                    logger.info("{?=call EBANKING_SI.UPLOAD_SI(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                    
                    procStmt = connects.prepareCall("{?=call EBANKING_SI.UPLOAD_SI(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                    procStmt.setString(2, instData.sessionId);
                    procStmt.setString(3, flexUserId);    //(String) LoadBasicParam.getDataValuenew("USERID")
                    procStmt.setString(4, siSourceAcct);
                    procStmt.setString(5, siSourceCode);   // (String) LoadBasicParam.getDataValuenew("SISOURCECODE"))
                    procStmt.setString(6, headOfficeBranch);   
                    procStmt.setString(7, SourceRef);// chnaged from userref to SourceRef
                    procStmt.setString(12, userRef);
                    procStmt.setString(13, siProductcode);
                    procStmt.setString(14, instData.custId);
                    procStmt.setString(15, instData.userId);
                    procStmt.registerOutParameter(1, OracleTypes.INTEGER);
                    procStmt.registerOutParameter(8, OracleTypes.VARCHAR);
                    procStmt.registerOutParameter(9, OracleTypes.VARCHAR);
                    procStmt.registerOutParameter(10, Types.VARCHAR);
                    procStmt.registerOutParameter(11, Types.VARCHAR);
                    procStmt.execute();
                    int result = procStmt.getInt(1);

//            String err_code=pstmt.getString(6);
//            String err_msg=pstmt.getString(5);
//            System.out.println(err_code+ "  error "+err_msg);
                    logger.info(" result: " + result + "error_code" + procStmt.getString(8) +" error msg: " + procStmt.getString(9) + " error msg2: " + procStmt.getString(10));
                    
                    if (result != 0) {
                        
                        
                        Element row = new Element("ROW");
                        Element success = new Element("SUCCESS");
                        
                        success.setText("Error while Initiating Standing Instruction");
                        row.addContent(success);
                        parent.addContent(row);
                        instData.result = this.generateXML(parent, xmlText);

                          logger.error("Error while Initiating Standing Instruction");
                      
                    } else {
//                        String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
//                        String type = "";
//                        String toemail = (String) LoadEmails.getDataValuenew("SI");
//                        String header = "Stop ChequeBook Payment";
//                        String body = "Dear Sir/Ma <br>  The the account with userid " + instData.userId
//                                + "has successfully Initiated the Standing Instruction . <br> <br> Yours Faithfully <br> Internet Banking Service";
//
//                        sendmail send = new sendmail();
//
//                        /* boolean sent = true; */
//                        boolean sent = send.sendMessage(header, body, null, type, fromemail, toemail);
                        Element row = new Element("ROW");
                        Element success = new Element("SUCCESS");
                        success.setText("Standing Instruction Successfully Initiated");
                        row.addContent(success);
                        parent.addContent(row);
                        instData.result = this.generateXML(parent, xmlText);

                          logger.info("Standing Instruction Successfully Initiated");
                      
                    }

                } catch (Exception ex) {
                    instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
                    ex.printStackTrace();
                    return instData.result;
                } finally {
                    if (procStmt != null) {
                        try {
                            procStmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (connects != null) {
                        try {
                            connects.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }


            } else {
                Element row = new Element("ROW");
                Element success = new Element("SUCCESS");
                success.setText("Error while Initiating Standing Instruction");
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);

                 logger.error("Error while Initiating Standing Instruction");
                      
            }
        }
        return instData.result;

    }

    public SiProductVO getProductVO(String productCode, Connection cons) {
        System.out.println("===== Inside getProductVO === ");
        SiProductVO siProductVO = new SiProductVO();
        PreparedStatement pstmts = null;
        ResultSet rs = null;


        try {
            //cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmts = cons.prepareStatement(SEL_SI_PRD);
            System.out.println(SEL_SI_PRD);
            pstmts.setString(1, productCode);
            System.out.println("====This is the product code " + productCode);
            rs = pstmts.executeQuery();
            System.out.println("O GA OOOOOOOOOO");
            //System.out.println("rs : " + rs.getString(1));
            while (rs.next()) {
                siProductVO.setProductcode(rs.getString(1));
                System.out.println(rs.getString(1) + " why");
                siProductVO.setProducttype(rs.getString(2));
                System.out.println(rs.getString(2) + " why1");
                //siProductVO.setExecdays(rs.getInt(3));
                //siProductVO.setExecmths(rs.getInt(4));
                //siProductVO.setExecyrs(rs.getInt(5));
                System.out.println(rs.getString(6) + " This is action code amount");
                siProductVO.setActioncodeamount(rs.getString(6));
                System.out.println("why2");
                siProductVO.setCalholexcp(rs.getString(7));
                System.out.println("why3");
                siProductVO.setRatetype(rs.getString(8));
                System.out.println("why4");
                siProductVO.setMaxretrycount(rs.getInt(9));
                System.out.println("why5");
                siProductVO.setProcessingtime(rs.getString(10));
                System.out.println("why6");
                siProductVO.setMinsweepamt(rs.getString(11));
                System.out.println("why7");
                siProductVO.setInstructioncode(rs.getString(12));
                System.out.println("why8");
                siProductVO.setTransfertype(rs.getString(13));
                System.out.println("why9");
                siProductVO.setSitype(rs.getString(14));
                System.out.println("why10");
                siProductVO.setReferralrequired(rs.getString(15));
                System.out.println("why11");

                System.out.println("====== retrieved product Code =====");
            }


        } catch (SQLException ex) {
            /*if (bDebugOn)*/ {
                System.out.println("sql : " + ex);
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            if (bDebugOn) {
                System.out.println("exception: " + ex);

            }
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }


        return siProductVO;

    }

    private void setInputMasterUpload(SIObject siObject) {
    }

    public String EndianesDate() {
        StringBuffer buff = new StringBuffer();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(cal.YEAR);
        String years = year + "";

        buff.append(years.substring(2, 4));
        System.out.println(buff.toString());
        int days = cal.get(cal.DAY_OF_YEAR);
        buff.append(days);
        if (bDebugOn) {
            System.out.println(buff.toString());
        }

        return buff.toString();
    }

//    public static void main(String[] args) {
//        try{
//            ObjectFactory objFactory = new ObjectFactory();
//          SiProductVO po =
//             objFactory.createSiProductVO();
//       JAXBContext jc = JAXBContext.newInstance( "com.fasyl.ebanking.logic" );
//       //SiProductVO prdvo = new SiProductVO();
//       po.setActioncodeamount("1");
//       po.setMinsweepamt("12");
//       po.setProducttype("sp1");
//       Marshaller m = jc.createMarshaller();
//       m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
//Boolean.TRUE);
//       m.marshal( po, System.out );
//        }catch(Exception ex){
//           ex.printStackTrace();
//        }
//    }
    private String getUserRef(String Serials, String endianesdate) {
        StringBuffer userref = new StringBuffer();
        userref.append(this.drBranchCode);//need to test if they want the user to do more that payment type of SI
        userref.append(this.siType);
        userref.append(endianesdate);
        userref.append(Serials);


        return userref.toString();

    }

    private String getSerial() {
        StringBuffer serial = new StringBuffer();
        int iserial = 0;
        for (int a = 0; a <= 3; a++) {
            iserial = sequenceIdRandom.nextInt(4);
            serial.append(String.valueOf(iserial));
        }
        return serial.toString();
    }

    private String getSourceRef(String serialres, String endianesdate) {

        StringBuffer rets = new StringBuffer();
        String ret = "";
        int iResult = 0;
        rets.append(this.drBranchCode);//need to use the dr account branch

        rets.append(serialres);


        rets.append(endianesdate);
        return rets.toString();

    }

    public boolean loadData() {
        boolean result = false;
        //SIObject siObject = info;
        PreparedStatement pstmt = null;
        int isUpdate = 0;
        String constr = null;
        endianesDate = EndianesDate();
        serialresult = getSerial();
        SourceRef = getSourceRef(serialresult, endianesDate);
        logger.info("This is the sourceref: " + SourceRef);
        String productCode = this.siType;
        logger.info("This is the product code " + productCode);
        userRef = getUserRef(serialresult, endianesDate);
        if (bDebugOn) {
            logger.info(" This is the user ref: " + userRef);
        }
        Connection cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        SiProductVO product = new SiProductVO();
        product = getProductVO(this.siType, cons);
        // String custno = "";
        //String counterParty = custno;// custno will be supplied by the calling servlet


        if (product != null) {

            try {
                if (bDebugOn) {
                    logger.info("====== about to getconnection to fcc======");
                }
                //cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();

                cons.setAutoCommit(false);
                pr_init(cons);
                
                logger.info("Query: " + INS_INTO_REQD_TABLE);
                
                pstmt = cons.prepareStatement(INS_INTO_REQD_TABLE);
                pstmt.setString(1, headOfficeBranch);//I will need to test for transaction type if any other si type is going to be included.
                pstmt.setString(2, siSourceCode);// need to add this to the part of what to return from getsi
                pstmt.setString(3, SourceRef);
                pstmt.setString(4, "SI txn from IBS");
                pstmt.setString(5, userRef);
                pstmt.setDate(6, this.siFinalExecDate);
                pstmt.setDate(7, currentDate());
                pstmt.setString(8, this.siType);
                pstmt.setString(9, this.counterparty);
                pstmt.setString(10, product.getProducttype());
                pstmt.setString(11, serialresult);
                pstmt.setString(12, product.getActioncodeamount());
                pstmt.setString(13, "");
                pstmt.setString(14, "");
                pstmt.setString(15, "");
                pstmt.setInt(16, product.getMaxretrycount());
                pstmt.setString(17, "");
                pstmt.setString(18, this.drBranchCode);
                System.out.println(this.drBranchCode);
                pstmt.setString(19, this.siSourceAcct);//need to be part of what will be returned by getsi;
                pstmt.setString(20, this.siCurrency);
                pstmt.setString(21, this.siCurrency);// this will be changed to the currency accpted by the product
                pstmt.setString(22, this.siAmount);
                pstmt.setString(23, this.crBranchCode);
                pstmt.setString(24, this.siDestAccount);
                pstmt.setString(25, "");// the creditor currency
                pstmt.setString(26, this.siPriority);
                pstmt.setString(27, "");//CHARGE_WHOM
                pstmt.setString(28, "");
                pstmt.setString(29, "");
                pstmt.setString(30, "");
                pstmt.setString(31, "");
                pstmt.setString(32, "");
                pstmt.setString(33, "");
                pstmt.setString(34, "U");
                pstmt.setString(35, "");



                isUpdate = pstmt.executeUpdate();
                if (bDebugOn) {
                    logger.info("=====sitm_upload_master updated" + isUpdate);
                }

                if (isUpdate != 0 && isUpdate > 0) {
                    logger.info("=====About to update sitm_upload_instr======");
                    
                     logger.info("Query: " + INS_INTO_INST_TABLE);
               
                    pstmt = cons.prepareStatement(INS_INTO_INST_TABLE);
                    pstmt.setString(1, headOfficeBranch);//The same thing as above.
                    pstmt.setString(2, siSourceCode);
                    pstmt.setString(3, SourceRef);
                    pstmt.setString(4, this.siType);//siObject.getSiType() returns the product code chosen by the user
                    pstmt.setString(5, product.getProducttype());//type of product e.g payment,collection,sweep in etc
                    pstmt.setString(6, product.getSitype());//product.getsitype() is the si type e.g one to one or one to many
                    pstmt.setString(7, product.getCalholexcp());
                    pstmt.setString(8, product.getRatetype());
                    if (this.siFreqExec == 1 || this.siFreqExec == 2) {
                        if (this.siFreqExec == 1) {
                            pstmt.setInt(9, 1);
                        } else {
                            pstmt.setInt(9, 7);
                        }
                        pstmt.setString(10, "");
                        pstmt.setString(11, "");
                    } else if (this.siFreqExec == 3 || this.siFreqExec == 4 || this.siFreqExec == 5) {
                        pstmt.setString(9, "");
                        if (this.siFreqExec == 3) {
                            pstmt.setInt(10, 1);
                        } else if (this.siFreqExec == 4) {
                            pstmt.setInt(10, 3);
                        } else if (this.siFreqExec == 5) {
                            pstmt.setInt(10, 6);
                        }
                        pstmt.setString(11, "");
                    } else {
                        pstmt.setString(9, String.valueOf(0));
                        pstmt.setString(10, String.valueOf(0));
                        pstmt.setInt(11, 1);
                    }

                    pstmt.setDate(12, this.siFirstExecDate);
                    pstmt.setDate(13, nextexecDate());///get the next execution date
                    pstmt.setDate(14, this.siFirstExecDate);
                    pstmt.setDate(15, nextexecDate());// as in 13
                    pstmt.setString(16, "N");//month end flag
                    pstmt.setString(17, "E");
                    pstmt.setString(18, userRef);
                    pstmt.setString(19, "A");
                    pstmt.setString(20, "A");
                    pstmt.setString(21, "");
                    pstmt.setString(22, "");///book date need to be set as the date the contract was input into the system
                    pstmt.setString(23, serialresult);
                    pstmt.setString(24, this.counterparty);
                    pstmt.setString(25, "");
                    pstmt.setString(26, "");
                    pstmt.setString(27, "U");
                    pstmt.setString(28, "");

                    isUpdate = pstmt.executeUpdate();
                    if (bDebugOn) {
                        logger.info("====sitm_upload_instruction updated=======" + isUpdate);
                    }

                    if (isUpdate != 0 && isUpdate > 0) {
                        //Call upload procedure
                        logger.info("updating log commmited !");
                        cons.commit();
                        result = true;
                        //con2.commit();
                    }


                }

            } catch (SQLException sql) {
                /*if (bDebugOn)*/ {
                    System.out.println("sql ; " + sql);
                   // sql.printStackTrace();
                    try {
                        cons.rollback();
                        if(cons!=null){
                        con2.rollback();
                        }
                    } catch (SQLException ssqle) {
                        /*if (bDebugOn)*/ {
                            System.out.println("sql:" + ssqle);
                          ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex);
                          ex.printStackTrace();
                        }
                    }
                }
            } catch (Exception ex) {
                /*if (bDebugOn)*/ {
                    System.out.println("Exception ; " + ex);
                    ex.printStackTrace();
                    try {
                        cons.rollback();
                        //con2.rollback();
                    } catch (Exception ex2) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex2);
                            ex2.printStackTrace();
                        }
                    }
                }
            } finally {
                if (pstmt != null) {
                    try {
                        pstmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (cons != null) {
                    try {
                        cons.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

        }

        return result;
    }

    protected HashMap extractOutputValues(String type)
            throws SQLException {

        String result = null;

        String status = procStmt.getString(6);
        if (bDebugOn) {
            System.out.println(status);
        }
        String errorCode = procStmt.getString(7);
        if (bDebugOn) {
            System.out.println(errorCode);
        }
        String errorParameter = procStmt.getString(8);
        if (bDebugOn) {
            System.out.println(errorParameter);
        }
        String errorMsg = procStmt.getString(9);
        if (bDebugOn) {
            System.out.println(errorMsg);
        }

        org.jdom.Element results = new org.jdom.Element("RESULT");
        // Element transdetaildr = new Element("Transdetaildr");
        Element statuse = new Element("STATUS");
        statuse.setText(status);
        Element errorcodee = new Element("errorCode");
        errorcodee.setText(errorCode);
        Element errorParametere = new Element("errorParameter");
        errorParametere.setText(errorParameter);
        Element errorMsge = new Element("errorMsg");
        errorMsge.setText(errorMsg);
        results.addContent(statuse);
        results.addContent(errorcodee);
        results.addContent(errorParametere);
        results.addContent(errorMsge);
        org.jdom.Document doc = new org.jdom.Document(results);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        String xmlTexts = outputter.outputString(doc);

        HashMap resultx = new HashMap();
        resultx.put("returnResult", xmlTexts);
        if (bDebugOn) {
            System.out.println(result);
        }
        return resultx;
    }

    public Date nextexecDate() {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
        Calendar cal = Calendar.getInstance();
        String newdate = null;
//java.util.Date dates = Utility.convertStringToDate(siFirstExecDate);
        cal.setTime(siFirstExecDate);
        if (this.siFreqExec == 1 || this.siFreqExec == 2) {
            if (this.siFreqExec == 1) {
                cal.add(Calendar.DATE, 1);
            } else {//Adding 1 day to current date
                cal.add(Calendar.DATE, 7);
            }
        }

            if (this.siFreqExec == 3 || this.siFreqExec == 4 || this.siFreqExec == 5) {
                if (this.siFreqExec == 3) {
                    cal.add(Calendar.MONTH, 1);
                } else if (this.siFreqExec == 4) {
                    cal.add(Calendar.MONTH, 3);
                } else {//Adding 1 day to current date
                    cal.add(Calendar.MONTH, 6);
                }
}

                if (this.siFreqExec == 6) {
                    cal.add(Calendar.YEAR, 1);
                }
            
            newdate = dateformat.format(cal.getTime());
            System.out.println(newdate);
        
        return Utility.convertString2Date(newdate);
    }
//    public static  void main(String[] args){
//    
//       AddSI2 tes = new AddSI2();
//       Calendar cal = Calendar.getInstance();
//       java.util.Date dates = new Date(cal.getTimeInMillis());
//       tes.siFirstExecDate=dates;
//       tes.siFreqExec=2;
//       tes.nextexecDate();
//    }
}