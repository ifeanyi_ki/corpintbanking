
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GetAccountDetail extends Processes {
    private int          NBR_PARAMS         = 1;
    private final String PROCEDURE_ACCT_INQ = "fn_get_acct_info";
    private String       acct;
    private String       returnResult;

    /* this class is used for ajax call that only invoive part of the page e.g account validation with taskid of 001 */
    public GetAccountDetail() {}

    protected String executeProcedure() throws SQLException {
        System.out.println(acct);
        procStmt.setString(2, acct);
        procStmt.execute();

        return procStmt.getString(1);
    }

    public void initializeVar() {
        this.acct         = null;
        this.returnResult = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.acct = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false, null);
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            setInstanceVar(instData);
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        result.put("returnResult", returnResult);

        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_ACCT_INQ, NBR_PARAMS);
    }

    /*
     * public HashMap getSI(){
     * HashMap map = new HashMap();
     * map = ExchangeMap();
     * return map;
     * }
     */
    protected void prepareExecProcedure(Connection con) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = con.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
