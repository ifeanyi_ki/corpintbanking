
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import java.util.HashMap;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class CreateUser extends Processes {

    private static final Logger logger = Logger.getLogger(CreateUser.class.getName());
    private static final String CREATE_USER = "ebanking.fn_create_user";
    public static final String INS_ACCT =
            "insert into cust_acct (custno,cust_acct,status,added_by,added_date)values(?,?,'A',?,sysdate)";
    private static final int NBR_PARAMS = 3;
    public static final String SUCCESS = "false";
    private String mail = null;
    private String phone = null;
//    private String              question      = null;
//    private String              answer         = null;
    private String roletype = null;
    private String user_password = null;
    private String userid = null;
    private String accounts;
    private String custno;
    //private Connection          dbConnection;
    private String parameter;
    private HashMap preresult;
    //protected CallableStatement procStmt;
    private String returnResult;
    private String password;

    public CreateUser() {
    }

    public void initializeVar() {
        this.custno = null;
        this.procStmt = null;
        this.returnResult = null;
        this.parameter = null;
        this.preresult = null;
        this.accounts = null;
        this.roletype = null;
        accounts = null;
        user_password = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        this.parameter = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "txndata", 0, false, null), "UTF-8");
        this.accounts = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "accounts", 0, false, null), "UTF-8");
        this.custno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "custno", 0, false, null), "UTF-8");
        this.roletype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "roletype", 0, false, null), "UTF-8");
        this.mail = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "email",
                0, false, null), "UTF-8");
        this.phone = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "phone",
                0, false, null), "UTF-8");
//          this.question = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "question",
//                0, false, null), "UTF-8");
//            this.answer = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "answer",
//                0, false, null), "UTF-8");
        user_password = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "user_password", 0, false, null), "UTF-8");

        //logger.info(this.parameter + " " + accounts + " " + roletype +   " phone: " + question +"question" + answer +"answer");

        logger.info(" xml: " + parameter);


//      if (!(custno == null || custno.equals(""))) {//This is used temporarily
//          roletype = "CUST";// T
//      }
    }

    private boolean insertAccountNo(DataofInstance instData, Connection connects) {
        //Connection        connects    = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt = null;
        int insert = 0;
        boolean result = false;
        String token = null;
        StringTokeNizers l_tokenizer = new StringTokeNizers(this.accounts, ";");

        if (bDebugOn) {
            System.out.println(" == This is the number of tokens ====" + l_tokenizer.countTokens());
        }

        try {
            while (l_tokenizer.hasMoreTokens()) {
                token = l_tokenizer.nextToken();
                pstmt = connects.prepareStatement(INS_ACCT);
                pstmt.setString(1, custno.trim());
                pstmt.setString(2, token);
                pstmt.setString(3, instData.userId);
                insert = pstmt.executeUpdate();

                if (insert > 0) {
                    result = true;
                } else {
                    result = false;

                    return result;
                }
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {

            if (pstmt != null) {
                try {
                    pstmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connects != null) {
                try {
                    connects.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");

        boolean resultz = false;

        initializeVar();

        try {
            setInstanceVar(instData);

            if ((roletype.equals("CUST") || roletype.equals("CORP")) && ((custno == null) || custno.equals(""))) {
                instData.result = Utility.processError(instData, NOT_VALIDUSER4ROLE);

                return instData.result;
            }
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            connect.setAutoCommit(false);
            if (roletype.equalsIgnoreCase("ADMIN")) { //This ensures Admin users are sent for auth
                System.out.println("ADMIN should be sent for approval");
                AdminCreationHandler adminCreationHandler = new AdminCreationHandler();
                if(adminCreationHandler.sendForAuthorization("SM_USER_ACCESS", parameter, instData)) {
                    System.out.println("Admin User creation successfully sent for authorization");
                    instData.result = new HashMap();
                    instData.result.put(RESULT_MAP_KEY, "Admin User creation successfully sent for authorization");
                    
                    String userID = parameter.substring(parameter.indexOf("<user_id>") + 9,
                            parameter.indexOf("</user_id>"));
                    String userRole = parameter.substring(parameter.indexOf("<cod_role_id>") + 13,
                            parameter.indexOf("</cod_role_id>"));
                    String fullName = parameter.substring(parameter.indexOf("<user_name>") + 11,
                            parameter.indexOf("</user_name>"));
                    String type = "text/html";
                    String header = "Authorization Request: New Admin User Notification";
                    String body = "Dear Admin,<br /><br />"
                            + "Please note that a new Admin user with the following details"
                            + "has been created and pending your approval. <br /> <br />"
                            + "User ID: <b>" + userID + "</b> <br />"
                            + "Name: <b>" + fullName + "</b> <br />"
                            + "Role Type: <b>" + userRole + "</b> <br />"
                            + "Please attend to the request and generate passcode for user after approval."
                            + "<br /><br />Thank you";
                    String toemail = (String) LoadEmails.getDataValuenew("AUTH");
                    //If an email is not registered for this IBUSER, it uses general email as from
                    String fromemail = (instData.email == null || instData.email.trim().equals("")) ?
                            instData.email : (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
                    
                    sendmail send = new sendmail();
                    boolean t = send.sendMessage(header, body, "", type, fromemail, toemail);
                    System.out.println("Mail sent successfully for Admin create? " + t);
                    resultz = true;
                } else {
                    System.out.println("Error sending admin for authorization");
                    instData.result = Utility.processError(instData, ERR_IN_USER_CREATION);
                }
            } //End If (roletype.equalsIgnoreCase("ADMIN"))
            else {
                this.preresult = ExchangeMap(connect);

                if (Integer.parseInt((String) preresult.get(RESULT_MAP_KEY)) == 0) {

                    // out.print("Charges Processed Successfully");
                    // if (roletype.equals("CUST")||roletype.equals("CORP")) {
                    resultz = this.insertAccountNo(instData, connect);    // This is not meant for admin user

                    // } else {
                    resultz = true;

                    // }
                    if (resultz) {

//                  System.out.println(" This is your userid " + userid);
//                  Object [] args = {user_password,userid,custno};
                        String fullName = parameter.substring(parameter.indexOf("<user_name>") + 11,
                            parameter.indexOf("</user_name>"));
                        String type = "text/html";

                        String body = "Dear " + fullName + ",<br><br> Your account has been created on the TrustBond Mortgage Bank internet banking portal."
                                + " Please find your login credentials below:<br><br>"
                                + " <b>User ID</b>: " + userid + " <br>"
                                + " <b>Password</b>: " + user_password + " <br><br>"
                                + "Please check the next email for a passcode which must be used at initial login to change the default password.<br><br> "
                                + "Click <a href='https://ibank.trustbondmortgagebankplc.com/IntBanking'>https://ibank.trustbondmortgagebankplc.com/IntBanking</a> to login.<br><br> "
                                + "Thank You.";

//                  String bodyString = "Dear Customer,<br> You have been created as user for SLCB internet"
//                       + " banking and Your default credentials are as follows<br>   "
//                       + " Password is {0} ,Userid is {1} and your customer no is {2}. <br>"
//                       + "Thank You<br> Yours Faithfully <br> IB";
//                    String bodyString = (String) com.fasyl.ebanking.main.LoadEBMessages.getMessageValue(USERCREATIONTEXT).getMessgaedesc();
//                   String body = MessageFormat.format(bodyString, args);
                        String header = "TRUSTBOND IBANKING USER INFO";    // USERCREATIONTEXTHEADER;
                        String toemail = mail;
                        String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");    // ;
                        sendmail send = new sendmail();

                        // send.sendMessage(header, body, password, type, fromemail, toemail);
                        resultz = send.sendMessage(header, body, "password", type, fromemail, toemail);
                        //notify ib admin when a new user is created
                        type = "text/html";
                        header = "Authorization Request: New User Notification";
                        body = "Dear Admin,<br><br>Please note that a new user with user ID <b>" + userid + "</b> has been created on the internet banking platform. Please generate passcode.<br><br>Thank you";
                        toemail = (String) LoadEmails.getDataValuenew("AUTH");;
                        fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
                        sendmail send2 = new sendmail();
                        resultz = send.sendMessage(header, body, password, type, fromemail, toemail);
                        System.out.println("toemail" + toemail);

                        if (resultz) {
                            this.preresult.remove(RESULT_MAP_KEY);
                            this.preresult.put(
                                    RESULT_MAP_KEY,
                                    "User created successfully. Login details have been sent to his/her e-mail for activation");
                            instData.result = this.preresult;

                            if (!connect.isClosed()) {
                                connect.commit();
                            }
                        } else {
                            if (!connect.isClosed()) {
                                connect.rollback();
                            }
                            resultz = false;
                            deleteInsertedUser();
                            instData.result = Utility.processError(instData, EMAILSERVERERROR);

                            return instData.result;
                        }
                    } else {
                        if (!connect.isClosed()) {
                            connect.rollback();
                        }
                        resultz = false;
                        instData.result = Utility.processError(instData, FAILED_TO_INSERT_ACCT);

                        // instData.result.put("returnResult","User Creation Failed");
                        return instData.result;
                    }
                } else { //For if(Integer.parseInt((String) preresult.get(RESULT_MAP_KEY)) == 0)
                        if (!connect.isClosed()) {
                            connect.rollback();
                        }
                    resultz = false;
                    instData.result = Utility.processError(instData, ERR_IN_USER_CREATION);

                    // instData.result.put("returnResult","User Creation Failed");
                    return instData.result;
                } //End else for (Integer.parseInt((String) preresult.get(RESULT_MAP_KEY)) == 0)
            }

        } catch (Exception ex) {

            try {
                if (!connect.isClosed()) {
                    connect.rollback();
                }
            } catch (SQLException ex1) {
                ex1.printStackTrace();
            }
            resultz = false;
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {

            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch(SQLException sqlExc) {
                    sqlExc.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch(SQLException sqlExc) {
                    sqlExc.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    @Override
    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        this.userid = procStmt.getString(3);
        mail = procStmt.getString(4);
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(CREATE_USER, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();

        logger.info(" procedure: " + l_stmt_string);

        procStmt = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(2, this.parameter);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.VARCHAR);
        procStmt.registerOutParameter(3, OracleTypes.VARCHAR);
        procStmt.registerOutParameter(4, OracleTypes.VARCHAR);
    }

    public boolean deleteInsertedUser() {
        boolean result = false;
        Connection con = null;
        PreparedStatement pstmt = null;

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareStatement("Delete from sm_user_access  where cod_usr_id=?");
            pstmt.setString(1, userid);
            pstmt.executeUpdate();

            if (!(custno == null) || !(custno.equals(""))) {
                pstmt = con.prepareStatement("Delete from cust_acct  where custno=?");
                pstmt.setString(1, userid);
                pstmt.executeUpdate();
            }
        } catch (Exception ex) {
            ex.printStackTrace();;
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch(SQLException sqlExc) {
                    sqlExc.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch(SQLException sqlExc) {
                    sqlExc.printStackTrace();
                }
            }
        }

        return result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
