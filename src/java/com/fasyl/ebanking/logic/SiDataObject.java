
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

/**
 *
 * @author baby
 */
public class SiDataObject {
    private String SourceRef;
    private String counterparty;
    private String crBranchCode;
    private String drBranchCode;
    private String endianesDate;
    private String procResult;
    private String serialresult;
    private String siAmount;
    private String siCurrency;
    private String siDestAccount;
    private String siFinalExecDat;
    private String siFirstExecDat;
    private String siFreqExec;
    private String siNarration;
    private String siPriority;
    private String siSourceAcct;
    private String siType;
    private String siobject;
    private String userRef;

    /**
     * @return the SourceRef
     */
    public String getSourceRef() {
        return SourceRef;
    }

    /**
     * @param SourceRef the SourceRef to set
     */
    public void setSourceRef(String SourceRef) {
        this.SourceRef = SourceRef;
    }

    /**
     * @return the endianesDate
     */
    public String getEndianesDate() {
        return endianesDate;
    }

    /**
     * @param endianesDate the endianesDate to set
     */
    public void setEndianesDate(String endianesDate) {
        this.endianesDate = endianesDate;
    }

    /**
     * @return the procResult
     */
    public String getProcResult() {
        return procResult;
    }

    /**
     * @param procResult the procResult to set
     */
    public void setProcResult(String procResult) {
        this.procResult = procResult;
    }

    /**
     * @return the userRef
     */
    public String getUserRef() {
        return userRef;
    }

    /**
     * @param userRef the userRef to set
     */
    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    /**
     * @return the siobject
     */
    public String getSiobject() {
        return siobject;
    }

    /**
     * @param siobject the siobject to set
     */
    public void setSiobject(String siobject) {
        this.siobject = siobject;
    }

    /**
     * @return the serialresult
     */
    public String getSerialresult() {
        return serialresult;
    }

    /**
     * @param serialresult the serialresult to set
     */
    public void setSerialresult(String serialresult) {
        this.serialresult = serialresult;
    }

    /**
     * @return the siType
     */
    public String getSiType() {
        return siType;
    }

    /**
     * @param siType the siType to set
     */
    public void setSiType(String siType) {
        this.siType = siType;
    }

    /**
     * @return the counterparty
     */
    public String getCounterparty() {
        return counterparty;
    }

    /**
     * @param counterparty the counterparty to set
     */
    public void setCounterparty(String counterparty) {
        this.counterparty = counterparty;
    }

    /**
     * @return the siSourceAcct
     */
    public String getSiSourceAcct() {
        return siSourceAcct;
    }

    /**
     * @param siSourceAcct the siSourceAcct to set
     */
    public void setSiSourceAcct(String siSourceAcct) {
        this.siSourceAcct = siSourceAcct;
    }

    /**
     * @return the siDestAccount
     */
    public String getSiDestAccount() {
        return siDestAccount;
    }

    /**
     * @param siDestAccount the siDestAccount to set
     */
    public void setSiDestAccount(String siDestAccount) {
        this.siDestAccount = siDestAccount;
    }

    /**
     * @return the siAmount
     */
    public String getSiAmount() {
        return siAmount;
    }

    /**
     * @param siAmount the siAmount to set
     */
    public void setSiAmount(String siAmount) {
        this.siAmount = siAmount;
    }

    /**
     * @return the siFreqExec
     */
    public String getSiFreqExec() {
        return siFreqExec;
    }

    /**
     * @param siFreqExec the siFreqExec to set
     */
    public void setSiFreqExec(String siFreqExec) {
        this.siFreqExec = siFreqExec;
    }

    /**
     * @return the siFirstExecDat
     */
    public String getSiFirstExecDat() {
        return siFirstExecDat;
    }

    /**
     * @param siFirstExecDat the siFirstExecDat to set
     */
    public void setSiFirstExecDat(String siFirstExecDat) {
        this.siFirstExecDat = siFirstExecDat;
    }

    /**
     * @return the siFinalExecDat
     */
    public String getSiFinalExecDat() {
        return siFinalExecDat;
    }

    /**
     * @param siFinalExecDat the siFinalExecDat to set
     */
    public void setSiFinalExecDat(String siFinalExecDat) {
        this.siFinalExecDat = siFinalExecDat;
    }

    /**
     * @return the siNarration
     */
    public String getSiNarration() {
        return siNarration;
    }

    /**
     * @param siNarration the siNarration to set
     */
    public void setSiNarration(String siNarration) {
        this.siNarration = siNarration;
    }

    /**
     * @return the siCurrency
     */
    public String getSiCurrency() {
        return siCurrency;
    }

    /**
     * @param siCurrency the siCurrency to set
     */
    public void setSiCurrency(String siCurrency) {
        this.siCurrency = siCurrency;
    }

    /**
     * @return the siPriority
     */
    public String getSiPriority() {
        return siPriority;
    }

    /**
     * @param siPriority the siPriority to set
     */
    public void setSiPriority(String siPriority) {
        this.siPriority = siPriority;
    }

    /**
     * @return the drBranchCode
     */
    public String getDrBranchCode() {
        return drBranchCode;
    }

    /**
     * @param drBranchCode the drBranchCode to set
     */
    public void setDrBranchCode(String drBranchCode) {
        this.drBranchCode = drBranchCode;
    }

    /**
     * @return the crBranchCode
     */
    public String getCrBranchCode() {
        return crBranchCode;
    }

    /**
     * @param crBranchCode the crBranchCode to set
     */
    public void setCrBranchCode(String crBranchCode) {
        this.crBranchCode = crBranchCode;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
