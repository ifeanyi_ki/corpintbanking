
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class ViewSI extends Processes {
    private static final int    NBR_PARAMS       = 7;
    private static final String PROCEDURE_SI_INQ = "ebanking_si.get_si";

    // String acctno;
    public static final String SUCCESS  = "false";
    String                     procName = "";
    private String             account;
    private String             detail;
    protected String           errorCode;
    protected String           errorType;
    protected String           errormsg;
    private String             instno;
    protected int              refOrgStan;
    private String             returnResult;
    private String             rsetSiInq;
    SIObject                   siobject;

    public ViewSI() {}

    public ViewSI(String acctno, String detail, String instno) {
        this.account = acctno;
        this.detail  = detail;
        this.instno  = instno;
    }

    protected void initializeVar() {
        this.account = null;
        this.detail  = null;
        this.instno  = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acctno", 0, false,
                null);
        this.detail = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "detail", 0, false,
                null);
        this.instno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "instno", 0, false,
                null);
        System.out.println(instno + " test  " + detail + "test  " + account);

        if (instno.equals("") || instno.equals(" ")) {
            System.out.println(" when instno is null");
            instno = null;
        }
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        rsetSiInq = procStmt.getString(1);

        java.sql.Clob datas = procStmt.getClob(3);
        String        data  = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        System.out.println("This is what I want " + rsetSiInq);
        System.out.println("This is what I want " + returnResult);

        // }
        errorCode = procStmt.getString(4);
        errorType = procStmt.getString(5);
        errormsg  = procStmt.getString(6);
        result.put("returnResult", returnResult);
        result.put("errorCode", errorCode);
        result.put("errorType", errorType);
        result.put("errormsg", errormsg);

        return result;
    }

    public HashMap processRequest(DataofInstance instData) {

        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SI_INQ, NBR_PARAMS);
    }

    /*
     * public HashMap getSI(){
     * HashMap map = new HashMap();
     * map = ExchangeMap();
     * return map;
     * }
     */
    protected void prepareExecProcedure(Connection con) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = con.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    protected String executeProcedure() throws SQLException {
        System.out.println("Account : " + account);
        procStmt.setString(2, account);
        procStmt.setString(7, detail);
        procStmt.setString(8, instno);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.VARCHAR);
        procStmt.registerOutParameter(3, OracleTypes.CLOB);
        procStmt.registerOutParameter(4, Types.VARCHAR);
        procStmt.registerOutParameter(5, Types.VARCHAR);
        procStmt.registerOutParameter(6, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
