/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author baby
 */
public class GetLoanInfo extends ProcessClass {

      private static final Logger logger = Logger.getLogger(GetLoanInfo.class.getName());
 
    
    public GetLoanInfo(){
        
    }
    private Connection connect = null;
    private Element parent;
    private PreparedStatement pstmt = null;
    DataofInstance instdata;
    private String xmlText = null;
    private String refno = null;
    private static String GET_LOAN_REPAYMENT = "select * from LDVW_SCHEDULE_SUMMARY@fcc where CONTRACT_REF_NO=? order by due_date asc";//and AMOUNT_SETTLED!=0"
    public static final String GET_LOAN_INFO = "select  contract_ref_no,counterparty,amount,lcy_amount,booking_date, "
            + "value_date,maturity_date,dr_setl_ac,dr_setl_ccy from ldtb_contract_master@fcc a ,cust_acct b "
            + "where counterparty=? and module='LD' and product_type='L' and a.version_no =(select max(version_no) from ldtb_contract_master@fcc  where counterparty =? )"
            + " and b.cust_acct=dr_setl_ac and a.settlement_status='N'";
    public static final String GET_DETAIL_LOAN_INFO = "select c.contract_ref_no,c.branch,"+
"c.counterparty,c.currency,c.lcy_amount,a.due_date,a.amount_due,a.original_due_date,a.component, "+
"a.amount_settled,b.principal_outstanding_bal,c.dr_setl_ac "+
"from cstb_amount_due@fcc a,LDTB_CONTRACT_BALANCE@fcc b,LDTB_CONTRACT_MASTER@fcc c  "+
"where a.contract_ref_no=b.contract_ref_no and a.contract_ref_no=c.contract_ref_no "+
"and c.contract_status='A' and a.due_date=(select min(due_date) from cstb_amount_due@fcc where counterparty=? and contract_ref_no =? and due_date > (select today from sttm_dates where branch_code ='099')) "+
"and c.counterparty=? and c.module='LD' and a.component='PRINCIPAL' and c.product_type='L' "
            + " and c.version_no =(select max(version_no) from ldtb_contract_master@fcc  where counterparty =? and contract_ref_no =?) and c.settlement_status='N' and c.contract_ref_no =?  ";

     protected void initializeVar() {
        pstmt = null;
         xmlText = null;
         refno=null;

    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent = new Element("ROWSET");
        this.refno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false, null);
          instdata = inst_data;
    }
  
    @Override
    public HashMap processRequest(DataofInstance instData) {
        ResultSet rs = null;
        boolean result = false;
        this.initializeVar();
       
        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }
            this.setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnection();

            if (instData.taskId.equals("2051")){
                
               logger.info("query: " + GET_DETAIL_LOAN_INFO);  
                
            pstmt = connect.prepareStatement(GET_DETAIL_LOAN_INFO);
            System.out.println(refno+" <<<<<<<<<<<< >>>>>>>> ");
             pstmt.setString(1, instData.custId);
             pstmt.setString(2, refno);
             pstmt.setString(3, instData.custId);
             pstmt.setString(4, instData.custId);
             pstmt.setString(5, refno);
             pstmt.setString(6, refno);
            }else if (instData.taskId.equals("205")){
                
                 logger.info("query: " + GET_LOAN_INFO);  
             
                
             pstmt = connect.prepareStatement(GET_LOAN_INFO);
             pstmt.setString(1, instData.custId);
             pstmt.setString(2, instData.custId);
            }else if (instData.taskId.equals("2052")){
                
              logger.info("query: " + GET_LOAN_REPAYMENT);  
                
                
             pstmt = connect.prepareStatement(GET_LOAN_REPAYMENT);
             pstmt.setString(1, refno);
            }
            rs = pstmt.executeQuery();
            if (parent==null || parent.equals("")){
            System.out.println(" Parent is null");
            }
            
            parent = this.convertToXml(parent, "ROW", rs);
            //if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                return instData.result;
            } else {
                result = true;
                if (instData.result == null) {

                    instData.result = generateXML(parent, this.xmlText);
                    if (!result || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding=" + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                    }
                }
            }
        } catch (Exception ex) {
             instData.result = Utility.processError(instData, APP_ERROR_SEARCH);            
             ex.printStackTrace();
             return  instData.result;
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return instData.result;
    }


}
