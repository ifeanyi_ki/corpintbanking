
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author baby
 */
public class UserManagement extends ProcessClass {

    StringBuffer sSql = null;
    private String action;
//   
    Connection con;
    private String isreject;
    private String par;
    private String refno;
    private String rejcomment;
    private String uniqueID;
    private String requesterEmail;
    // private static final PasswordGenerator innerClass = new PasswordGenerator();
    CallableStatement stmt;
    Statement statement;
    ResultSet resultSet;

    public UserManagement() {
    }

    protected void initializeVar() {
        action = null;
        refno = null;
        isreject = null;
        rejcomment = null;
        uniqueID = null;
        requesterEmail = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        action = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0,
                true, null), "UTF-8");
        refno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0,
                true, null), "UTF-8");
        isreject = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "isreject",
                0, true, null), "UTF-8");
        rejcomment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "rejcomment", 0, true, null), "UTF-8");
        uniqueID = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "uniqueID", 0, true, null), "UTF-8");
        requesterEmail = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "requesterEmail",
                0, true, null), "UTF-8");

        // par =  decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "par", 0, true, null), "UTF-8");
        System.out.println(action + " ref action " + refno);
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        int result = 0;

        con = com.fasyl.ebanking.db.DataBase.getConnection();
        initializeVar();

        try {
            setInstanceVar(instData);
            con.setAutoCommit(false);

            if (!((refno == null) || (refno.equals("")))) {
                System.out.println("about to authorise");
                //If delete, get name before deleting
                if(action.contains("Delete a User")) {
                    statement = con.createStatement();
                    resultSet = statement.executeQuery("SELECT cod_usr_name FROM sm_user_access"
                            + " WHERE cod_usr_id = '" + uniqueID + "'");
                }
                stmt = con.prepareCall("{?=call fn_manage_auth(?,?,?,?,?,?,?,?,?)}");
                stmt.registerOutParameter(1, OracleTypes.VARCHAR);
                stmt.registerOutParameter(5, OracleTypes.CLOB);
                stmt.registerOutParameter(6, OracleTypes.VARCHAR);
                stmt.registerOutParameter(7, OracleTypes.VARCHAR);
                stmt.registerOutParameter(8, OracleTypes.VARCHAR);
                stmt.setString(2, refno);
                stmt.setString(3, instData.userId);
                stmt.setString(4, instData.sessionId);
                stmt.setString(9, isreject);
                stmt.setString(10, rejcomment);
                stmt.execute();
                System.out.println("about to commit");
                con.commit();



                //The details gotten from the DB is for email purpose. Not needed if no requesterEmail
                if (requesterEmail != null && !requesterEmail.trim().equals("")) {
                    //This is implemented for the sending of email after approval / Rejection by Ayo
                    if(!action.contains("Delete a User")) {
                        statement = con.createStatement();
                        resultSet = statement.executeQuery("SELECT cod_usr_name FROM sm_user_access"
                            + " WHERE cod_usr_id = '" + uniqueID + "'");
                    }
                    
                    String msgHeader, msgBody, toEmail, fromEmail, name;
                    toEmail = requesterEmail;
                    fromEmail = LoadEmails.getDataValuenew("AUTH");
                    if (resultSet.next()) {
                        name = resultSet.getString("cod_usr_name");
                    } else {
                        name = "{Could not fetch name}";
                    }

                    System.out.println("From: " + fromEmail);
                    System.out.println("To: " + toEmail);
                    System.out.println("Account No: " + uniqueID);
                    System.out.println("Action: " + action);
                    System.out.println("Request Ref No: " + refno);
                    System.out.println("Reject Comment: " + rejcomment);
                    System.out.println("Name: " + name);

                    msgBody = "Dear Admin, <br /><br /> Please find below, details of your authorization request: <br />"
                            + "RequestID: " + refno + "<br />"
                            + "Name: " + name + "<br />"
                            + "Request Description: " + action + "<br />"
                            + "User ID: " + uniqueID + "<br />";

                    if (isreject.equalsIgnoreCase("Y")) { //If request was rejected
                        msgHeader = "Rejected | " + action + " | " + uniqueID;
                        msgBody += "Action: Rejected <br />"
                                + "Reason: " + rejcomment + "<br />";
                    } else {
                        msgHeader = "Approved | " + action + " | " + uniqueID;
                        msgBody += "Action: Approved <br />";
                    }

                    msgBody += "<br /> Thank you.";
                    sendmail mailSender = new sendmail();
                    boolean res = mailSender.sendMessage(msgHeader, msgBody, "", "text/html", fromEmail, toEmail);
                    if (res) {
                        System.out.println("Email sent sucessfully");
                    } else {
                        System.out.println("Email could not be sent");
                    }
                }

            }

            if(con == null || con.isClosed()) {
                con = com.fasyl.ebanking.db.DataBase.getConnection();
            }
            stmt = con.prepareCall("{?=call fn_auth_user(?,?,?,?,?,?)}");
            stmt.registerOutParameter(1, OracleTypes.VARCHAR);
            stmt.registerOutParameter(4, OracleTypes.CLOB);
            stmt.registerOutParameter(5, OracleTypes.VARCHAR);
            stmt.registerOutParameter(6, OracleTypes.VARCHAR);
            stmt.registerOutParameter(7, OracleTypes.VARCHAR);
            stmt.setString(2, "user");
            stmt.setString(3, instData.sessionId);
            stmt.execute();
            result = stmt.getInt(1);

            if (result == 0) {
                HashMap map = new HashMap();
                java.sql.Clob datas = stmt.getClob(4);
                String data = datas.getSubString(1, (int) datas.length());

                System.out.println(data);
                map.put(RESULT_MAP_KEY, data);
                instData.result = map;
            } else {
                instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

                return instData.result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
