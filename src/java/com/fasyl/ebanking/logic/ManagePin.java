
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.Sms;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;
import static com.sun.xml.ws.security.addressing.impl.policy.Constants.logger;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;
import org.apache.log4j.Logger;
//import java.util.logging.Logger;

/**
 *
 * @author baby
 */
public class ManagePin extends ProcessClass {

//    public static final String UPD_PIN = "update sm_user_access set user_pin=?,pin_to_expire=?,"
//            + "modified_by=?,modified_date=? where cod_usr_id=? ";
    public static final String UPD_PIN = "update tb_user_access set user_pin=?,pin_to_expire=?,"
            + "modified_by=?,modified_date=? where userid=? ";
    private Element parent = null;
    private String xmlText = null;
    private DataofInstance instancedata;
     private static final Logger logger  = Logger.getLogger(ManagePin.class.getName());

    public ManagePin() {
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        parent = new Element("ROWSET");
    }

    protected void initializeVar() {
        xmlText = null;
    }

    private int addPin(String pin) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        int result = 0;

        try {
            pstmt = connect.prepareStatement(UPD_PIN);
            pstmt.setString(1, pin);
            pstmt.setString(2, String.valueOf(System.currentTimeMillis()));
            pstmt.setString(3, instancedata.userId);
            pstmt.setTimestamp(4, Utility.getCurrentDate());
            pstmt.setString(5, instancedata.userId);
            result = pstmt.executeUpdate();

            if (bDebugOn) {
                System.out.println(instancedata.userId + " ==This is my customer id");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        String pin = null;

        this.instancedata = instData;

        //ResultSet rs = null;
        String type = "text/html";
        String body = null;
        String header = "Transaction Token";
        String toemail = instData.email;
        String fromemail = "";
        String pinText = null;


        // SMS PARAMETERS
        String destinationAddress = instData.phone;
        String url = "";
        String messageBody = null;
        String sourceAddress = "";

        this.initializeVar();

        try {
            sourceAddress = (String) LoadBasicParam.getDataValuenew("IBPHONE");
            fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
            url = (String) LoadBasicParam.getDataValuenew("SMSURL");
            pin = Utility.genaratePin();
            pinText = pin;

            int result = this.addPin(Encryption.encrypt(pin));

            if (bDebugOn) {
                System.out.println(result + " This is result and pin is: " + pinText);
            }

            boolean test = true;

            if (result > 0) {
                this.setInstanceVar(instData);

                if (test) {   
                    
                    
                          // THIS WAS DONE TO GIVE ROOM FOR PIN GENERATION EVEN THOUGH IT HAS NOT BEEN SENT
                    body = "Dear Customer, <br><br>  The transaction token (OTP) generated is " + pin
                            + "<br> <br>Kindly note that the token will be valid for 20 minutes only.<br /><br /> Regards,<br />TrustBond Mortgage Bank Internet Banking Service";

                    //message body for sms integration
                    messageBody = "Dear Customer, please use " + pin + " as your one time password (OTP). It expires in 20 minutes";

                     logger.info(" about to send sms . . . ");
                    
                      Sms smsmsg = new Sms();
                      boolean sent2 = smsmsg.sendSms(url, messageBody, destinationAddress, sourceAddress);
                    
                    
                    sendmail send = new sendmail();


                   /* boolean sent = true; 
                      boolean sent = send.sendMessage(header, body, null, type, fromemail, toemail);
                   */
                   
                    if (!sent2) {
                        instData.result = Utility.processError(instData, SMSSERVERERROR);
                      //  instData.result = Utility.processError(instData, EMAILSERVERERROR);
                    }
                    else  {
                        
                        logger.info(" sms sent !");
                        
                        logger.info("about to send email . . . ");
                        
                     //   Sms smsmsg = new Sms();
                     // boolean sent2 = smsmsg.sendSms(url, messageBody, destinationAddress, sourceAddress);
                      
                         /* boolean sent = true; */
                    boolean sent = send.sendMessage(header, body, null, type, fromemail, toemail);

                        
                      
                       if (!sent)
                       {                           
                        instData.result = Utility.processError(instData, EMAILSERVERERROR);
                        // instData.result = Utility.processError(instData, SMSSERVERERROR);                       
                       }
                       else
                           System.out.println(" EMAIL sent successfully");
                                   
                                 }
                    }else {
                    instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                }
            } else {
                instData.result = Utility.processError(instData, "Error while adding token");
            }

            if (instData.result == null) {
                Element row = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText(
                        "The token code has been sent to your registered email/mobile number. Please note that it is valid for 20 minutes only.");
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
