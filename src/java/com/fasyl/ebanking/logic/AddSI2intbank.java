/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.sql.Date;
import java.util.HashMap;
import java.util.Random;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import oracle.jdbc.OracleTypes;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Administrator
 */
public class AddSI2intbank extends Processes {

    boolean bDebugOn = true;
    HashMap procResult = null;
    String userRef = "";
    String endianesDate = "";
    private String siType = null;
    private String counterparty = null;
    private String siSourceAcct = null;
    private String siDestAccount = null;
    private String siAmount = null;
    private int siFreqExec = 0;
    private Date siFirstExecDate = null;
    private Date siFinalExecDate = null;
    private String siNarration = null;
    private String siCurrency = null;
    private String siPriority = null;
    private String drBranchCode = null;
    private String crBranchCode = null;
    private final String PROCEDURE_SI_UPLOAD = "EBANKING_SI.UPLOAD_SI";
    protected CallableStatement procStmt;
    private static final int NBR_PARAMS = 8;
    public static Random sequenceIdRandom = new Random();
    public final String SEL_SI_PRD = "SELECT b.product_code, b.product_type, b.exec_days, b.exec_mths, b.exec_yrs," +
            "b.action_code_amount, b.cal_hol_excp, b.rate_type, b.max_retry_count, " +
            "b.min_sweep_amt, b.processing_time, b.instruction_code, " +
            "b.transfer_type, b.si_type, b.referral_required " +
            "FROM sitm_product_prf b where b.product_code =? ";
    Connection cons = null;
    public static final String INS_INTO_REQD_TABLE = "INSERT INTO SITB_UPLOAD_MASTER " +
            "(                       " +
            "BRANCH_CODE,        " +
            "SOURCE_CODE,        " +
            "SOURCE_REF,         " +
            "DETAIL_REF,         " +
            "USER_REF_NUMBER,    " +
            "SI_EXPIRY_DATE,     " +
            "BOOK_DATE,          " +
            "PRODUCT_CODE,       " +
            "COUNTERPARTY,       " +
            "PRODUCT_TYPE,       " +
            "SERIAL_NO,          " +
            "ACTION_CODE_AMT,    " +
            "APPLY_CHG_SUXS,     " +
            "APPLY_CHG_PEXC,     " +
            "APPLY_CHG_REJT,     " +
            "MAX_RETRY_COUNT,    " +
            "MIN_SWEEP_AMT,      " +
            "DR_ACC_BR,          " +
            "DR_ACCOUNT,         " +
            "DR_ACC_CCY,         " +
            "SI_AMT_CCY,         " +
            "SI_AMT,             " +
            "CR_ACC_BR,          " +
            "CR_ACCOUNT,         " +
            "CR_ACC_CCY,         " +
            "PRIORITY,           " +
            "CHARGE_WHOM,        " +
            "MIN_BAL_AFTER_SWEEP," +
            "INTERNAL_REMARKS,   " +
            "ICCF_CHANGED,       " +
            "TAX_CHANGED,        " +
            "SETTLE_CHANGED,     " +
            "MIS_CHANGED,        " +
            "CONV_STATUS,        " +
            "ERR_MSG             " +
            " ) VALUES (   ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?       )";
    public static final String INS_INTO_INST_TABLE = "INSERT INTO SITB_UPLOAD_INSTRUCTION( " +
            "BRANCH_CODE,                " +
            "SOURCE_CODE,                " +
            "SOURCE_REF,                 " +
            "PRODUCT_CODE,               " +
            "PRODUCT_TYPE,               " +
            "SI_TYPE,                    " +
            "CAL_HOL_EXCP,               " +
            "RATE_TYPE,                  " +
            "EXEC_DAYS,                  " +
            "EXEC_MTHS,                  " +
            "EXEC_YRS,                   " +
            "FIRST_EXEC_DATE,            " +
            "NEXT_EXEC_DATE,             " +
            "FIRST_VALUE_DATE,           " +
            "NEXT_VALUE_DATE,            " +
            "MONTH_END_FLAG,             " +
            "PROCESSING_TIME,            " +
            "USER_INST_NO,               " +
            "INST_STATUS,                " +
            "AUTH_STATUS,                " +
            "LATEST_VERSION_DATE,        " +
            "BOOK_DATE,                  " +
            "SERIAL_NO,                  " +
            "COUNTERPARTY,               " +
            "LATEST_CYCLE_NO,            " +
            "LATEST_CYCLE_DATE,          " +
            "CONV_STATUS,                " +
            "ERR_MSG )                   " +
            "VALUES(?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?) ";
    private String serialresult = "";
    private String SourceRef = "";
    private SIObject siobject = null;

    public AddSI2intbank() {

        

    }

    public java.sql.Date currentDate() {

        java.util.Date date = new java.util.Date();
        java.sql.Date sqldat = new java.sql.Date(date.getTime());
        return sqldat;

    }

    protected void initializeVar() {
        this.SourceRef = null;
        this.endianesDate = null;
        this.procResult = null;
        this.userRef = null;
        this.siobject = null;
        this.serialresult = null;
        this.siType = null;
        this.counterparty = null;
        this.siSourceAcct = null;
        this.siDestAccount = null;
        this.siAmount = null;
        this.siFreqExec = 0;
        this.siFirstExecDate = null;
        this.siFinalExecDate = null;
        this.siNarration = null;
        this.siCurrency = null;
        this.siPriority = null;
        this.drBranchCode = null;
        this.crBranchCode = null;

    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.siType = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.counterparty = inst_data.custId;//ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.siSourceAcct = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "dSrcAcctNo", 0, false, null);
        this.siDestAccount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "DestAcctNo", 0, false, null);
        this.siAmount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfAmt", 0, false, null);
        this.siFreqExec = Integer.parseInt(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFreqDesc", 0, false, "1"));
        this.siFirstExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "FirstExecDate", 0, false, null));
        this.siFinalExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFinalDate", 0, false, null));
        this.siNarration = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiNarration", 0, false, null);
        this.siCurrency = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfCurr1", 0, false, null);
        this.siPriority = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiPriority", 0, false, null);
        this.drBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "accountBranch", 0, false, null);
        this.crBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "crbrcode", 0, false, null);

    }

    public HashMap processRequest(DataofInstance instData) {
        System.out.println(" ******Inside process request **** ");
        HashMap preResult = null;
         Connection connects = null;
        initializeVar();
        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        boolean loaddata = loadData();
        if (loaddata) {
            try{
             connects = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
             instData.result= ExchangeMap(connects);
            }  finally {
                System.out.println("inside finally");
                if (procStmt != null) {
                    try {
                        procStmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (connects != null) {
                    try {
                        connects.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
             
        } else {
        }


        return instData.result;

    }

    public SiProductVO getProductVO(String productCode, Connection cons) {
        System.out.println("===== Inside getProductVO === ");
        SiProductVO siProductVO = new SiProductVO();
        PreparedStatement pstmts = null;
        ResultSet rs = null;


        try {
            pstmts = cons.prepareStatement(SEL_SI_PRD);
            System.out.println(SEL_SI_PRD);
            pstmts.setString(1, productCode);
            System.out.println("====This is the product code " + productCode);
            rs = pstmts.executeQuery();
            System.out.println("O GA OOOOOOOOOO");
            //System.out.println("rs : " + rs.getString(1));
            while (rs.next()) {
                siProductVO.setProductcode(rs.getString(1));
                System.out.println(rs.getString(1) + " why");
                siProductVO.setProducttype(rs.getString(2));
                System.out.println(rs.getString(2) + " why1");
                //siProductVO.setExecdays(rs.getInt(3));
                //siProductVO.setExecmths(rs.getInt(4));
                //siProductVO.setExecyrs(rs.getInt(5));
                System.out.println(rs.getString(6) + " This is action code amount");
                siProductVO.setActioncodeamount(rs.getString(6));
                System.out.println("why2");
                siProductVO.setCalholexcp(rs.getString(7));
                System.out.println("why3");
                siProductVO.setRatetype(rs.getString(8));
                System.out.println("why4");
                siProductVO.setMaxretrycount(rs.getInt(9));
                System.out.println("why5");
                siProductVO.setProcessingtime(rs.getString(10));
                System.out.println("why6");
                siProductVO.setMinsweepamt(rs.getString(11));
                System.out.println("why7");
                siProductVO.setInstructioncode(rs.getString(12));
                System.out.println("why8");
                siProductVO.setTransfertype(rs.getString(13));
                System.out.println("why9");
                siProductVO.setSitype(rs.getString(14));
                System.out.println("why10");
                siProductVO.setReferralrequired(rs.getString(15));
                System.out.println("why11");

                System.out.println("====== retrieved product Code =====");
            }


        } catch (SQLException ex) {
            /*if (bDebugOn)*/ {
                System.out.println("sql : " + ex);
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            /*if (bDebugOn)*/ {
                System.out.println("exception: " + ex);
                ex.printStackTrace();
            }
        }


        return siProductVO;

    }

    private void setInputMasterUpload(SIObject siObject) {
    }

    public String EndianesDate() {
        StringBuffer buff = new StringBuffer();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(cal.YEAR);
        String years = year + "";

        buff.append(years.substring(2, 4));
        System.out.println(buff.toString());
        int days = cal.get(cal.DAY_OF_YEAR);
        buff.append(days);
        System.out.println(buff.toString());

        return buff.toString();
    }

//    public static void main(String[] args) {
//        try{
//            ObjectFactory objFactory = new ObjectFactory();
//          SiProductVO po =
//             objFactory.createSiProductVO();
//       JAXBContext jc = JAXBContext.newInstance( "com.fasyl.ebanking.logic" );
//       //SiProductVO prdvo = new SiProductVO();
//       po.setActioncodeamount("1");
//       po.setMinsweepamt("12");
//       po.setProducttype("sp1");
//       Marshaller m = jc.createMarshaller();
//       m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
//Boolean.TRUE);
//       m.marshal( po, System.out );
//        }catch(Exception ex){
//           ex.printStackTrace();
//        }
//    }

    private String getUserRef(String Serials, String endianesdate) {
        StringBuffer userref = new StringBuffer();
        userref.append(this.drBranchCode);//need to test if they want the user to do more that payment type of SI
        userref.append(this.siType);
        userref.append(endianesdate);
        userref.append(Serials);


        return userref.toString();

    }

    private String getSerial() {
        StringBuffer serial = new StringBuffer();
        int iserial = 0;
        for (int a = 0; a <= 3; a++) {
            iserial = sequenceIdRandom.nextInt(4);
            serial.append(String.valueOf(iserial));
        }
        return serial.toString();
    }

    private String getSourceRef(String serialres, String endianesdate) {

        StringBuffer rets = new StringBuffer();
        String ret = "";
        int iResult = 0;
        rets.append(this.drBranchCode);//need to use the dr account branch

        rets.append(serialres);


        rets.append(endianesdate);
        return rets.toString();

    }

    public boolean loadData() {
        boolean result = false;
        //SIObject siObject = info;
        PreparedStatement pstmt = null;
        int isUpdate = 0;
        String constr = null;
        endianesDate = EndianesDate();
        serialresult = getSerial();
        SourceRef = getSourceRef(serialresult, endianesDate);
        System.out.println("This is the sourceref: " + SourceRef);
        String productCode = this.siType;
        System.out.println("This is the product code " + productCode);
        userRef = getUserRef(serialresult, endianesDate);
        System.out.println(" This is the user ref: " + userRef);
        SiProductVO product = new SiProductVO();
        Connection cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        product = getProductVO(this.siType, cons);
        // String custno = "";
        //String counterParty = custno;// custno will be supplied by the calling servlet


        if (product != null) {

            try {
                System.out.println("====== about to getconnection to fcc======");
                //cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
                cons.setAutoCommit(false);
                pstmt = cons.prepareStatement(INS_INTO_REQD_TABLE);
                pstmt.setString(1, drBranchCode);//I will need to test for transaction type if any other si type is going to be included.
                pstmt.setString(2, "SI_UPLOAD");// need to add this to the part of what to return from getsi
                pstmt.setString(3, SourceRef);
                pstmt.setString(4, "EB");
                pstmt.setString(5, userRef);
                pstmt.setDate(6, this.siFinalExecDate);
                pstmt.setDate(7, currentDate());
                pstmt.setString(8, this.siType);
                pstmt.setString(9, this.counterparty);
                pstmt.setString(10, product.getProducttype());
                pstmt.setString(11, serialresult);
                pstmt.setString(12, product.getActioncodeamount());
                pstmt.setString(13, "");
                pstmt.setString(14, "");
                pstmt.setString(15, "");
                pstmt.setInt(16, product.getMaxretrycount());
                pstmt.setString(17, "");
                pstmt.setString(18, this.drBranchCode);
                System.out.println(this.drBranchCode);
                pstmt.setString(19, this.siSourceAcct);//need to be part of what will be returned by getsi;
                pstmt.setString(20, this.siCurrency);
                pstmt.setString(21, this.siCurrency);// this will be changed to the currency accpted by the product
                pstmt.setString(22, this.siAmount);
                pstmt.setString(23, this.crBranchCode);
                pstmt.setString(24, this.siDestAccount);
                pstmt.setString(25, "");// the creditor currency
                pstmt.setString(26, this.siPriority);
                pstmt.setString(27, "");//CHARGE_WHOM
                pstmt.setString(28, "");
                pstmt.setString(29, "");
                pstmt.setString(30, "");
                pstmt.setString(31, "");
                pstmt.setString(32, "");
                pstmt.setString(33, "");
                pstmt.setString(34, "U");
                pstmt.setString(35, "");



                isUpdate = pstmt.executeUpdate();
                System.out.println("=====sitm_upload_master updated" + isUpdate);

                if (isUpdate != 0 && isUpdate > 0) {
                    System.out.println("=====About to update sitm_upload_instr======");
                    pstmt = cons.prepareStatement(INS_INTO_INST_TABLE);
                    pstmt.setString(1, this.drBranchCode);//The same thing as above.
                    pstmt.setString(2, "SI_UPLOAD");
                    pstmt.setString(3, SourceRef);
                    pstmt.setString(4, this.siType);//siObject.getSiType() returns the product code chosen by the user
                    pstmt.setString(5, product.getProducttype());//type of product e.g payment,collection,sweep in etc
                    pstmt.setString(6, product.getSitype());//product.getsitype() is the si type e.g one to one or one to many
                    pstmt.setString(7, product.getCalholexcp());
                    pstmt.setString(8, product.getRatetype());
                    if (this.siFreqExec == 1 || this.siFreqExec == 2) {
                        if (this.siFreqExec == 1) {
                            pstmt.setInt(9, 1);
                        } else {
                            pstmt.setInt(9, 7);
                        }
                        pstmt.setString(10, "");
                        pstmt.setString(11, "");
                    } else if (this.siFreqExec == 3 || this.siFreqExec == 4 || this.siFreqExec == 5) {
                        pstmt.setString(9, "");
                        if (this.siFreqExec == 3) {
                            pstmt.setInt(10, 1);
                        } else if (this.siFreqExec == 4) {
                            pstmt.setInt(10, 3);
                        } else if (this.siFreqExec == 5) {
                            pstmt.setInt(10, 6);
                        }
                        pstmt.setString(11, "");
                    } else {
                        pstmt.setString(9, String.valueOf(0));
                        pstmt.setString(10, String.valueOf(0));
                        pstmt.setInt(11, 1);
                    }

                    pstmt.setDate(12, this.siFirstExecDate);
                    pstmt.setString(13, "");///get the next execution date
                    pstmt.setDate(14, this.siFirstExecDate);
                    pstmt.setString(15, "");// as in 13
                    pstmt.setString(16, "");//month end flag
                    pstmt.setString(17, "");
                    pstmt.setString(18, userRef);
                    pstmt.setString(19, "");
                    pstmt.setString(20, "");
                    pstmt.setString(21, "");
                    pstmt.setString(22, "");///book date need to be set as the date the contract was input into the system
                    pstmt.setString(23, serialresult);
                    pstmt.setString(24, this.counterparty);
                    pstmt.setString(25, "");
                    pstmt.setString(26, "");
                    pstmt.setString(27, "U");
                    pstmt.setString(28, "");

                    isUpdate = pstmt.executeUpdate();
                    System.out.println("====sitm_upload_instruction updated=======");

                    if (isUpdate != 0 && isUpdate > 0) {
                        //Call upload procedure
                        System.out.println("updating log");
                        cons.commit();
                        result = true;
                        //con2.commit();
                    }


                }

            } catch (SQLException sql) {
                /*if (bDebugOn)*/ {
                    System.out.println("sql ; " + sql);
                    sql.printStackTrace();
                    try {
                        cons.rollback();
                        //con2.rollback();
                    } catch (SQLException ssqle) {
                        /*if (bDebugOn)*/ {
                            System.out.println("sql:" + ssqle);
                            ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex);
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (Exception ex) {
                /*if (bDebugOn)*/ {
                    System.out.println("Exception ; " + ex);
                    ex.printStackTrace();
                    try {
                        cons.rollback();
                        //con2.rollback();
                    } catch (Exception ex2) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex2);
                            ex2.printStackTrace();
                        }
                    }
                }
            } finally {
                System.out.println("inside finally");
                if (pstmt != null) {
                    try {
                        pstmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (cons != null) {
                    try {
                        cons.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

        }

        return result;
    }

    public String executeProcedure()
            throws SQLException {

        procStmt.setString(2, "SYSTEM");
        procStmt.setString(3, "SI_UPLOAD");//TO BE PICKED FROM THEDB
        procStmt.setString(4, this.drBranchCode);
        procStmt.setString(5, userRef);
        //procStmt.setString(6,null );
        //procStmt.setString(7, null);
        //procStmt.setString(8, null);
        //procStmt.setString(9, null);


        procStmt.execute();

        return procStmt.getString(1);

    }

    protected void prepareExecProcedure(Connection connects)
            throws Exception {



        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt = connects.prepareCall(l_stmt_string);
        registerOutParameters();

    }

    protected String getProcedureParamString() {

        return buildProcedureStatement(
                PROCEDURE_SI_UPLOAD, NBR_PARAMS);

    }

    protected void registerOutParameters()
            throws SQLException {

        //if (!isMerantJdbcDriver) {
        procStmt.registerOutParameter(1, OracleTypes.VARCHAR);
        procStmt.registerOutParameter(6, OracleTypes.VARCHAR);
        // }
        procStmt.registerOutParameter(7, Types.VARCHAR);
        //procStmt.registerOutParameter (PARAM_IDX_CUST_ID, Types.INTEGER);
        procStmt.registerOutParameter(8, Types.VARCHAR);
        procStmt.registerOutParameter(9, Types.VARCHAR);

    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;

    }

    protected HashMap extractOutputValues(String type)
            throws SQLException {

        String result = null;

        String status = procStmt.getString(6);
        if (bDebugOn) {
            System.out.println(status);
        }
        String errorCode = procStmt.getString(7);
        if (bDebugOn) {
            System.out.println(errorCode);
        }
        String errorParameter = procStmt.getString(8);
        if (bDebugOn) {
            System.out.println(errorParameter);
        }
        String errorMsg = procStmt.getString(9);
        if (bDebugOn) {
            System.out.println(errorMsg);
        }

        org.jdom.Element results = new org.jdom.Element("RESULT");
        // Element transdetaildr = new Element("Transdetaildr");
        Element statuse = new Element("STATUS");
        statuse.setText(status);
        Element errorcodee = new Element("errorCode");
        errorcodee.setText(errorCode);
        Element errorParametere = new Element("errorParameter");
        errorParametere.setText(errorParameter);
        Element errorMsge = new Element("errorMsg");
        errorMsge.setText(errorMsg);
        results.addContent(statuse);
        results.addContent(errorcodee);
        results.addContent(errorParametere);
        results.addContent(errorMsge);
        org.jdom.Document doc = new org.jdom.Document(results);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        String xmlText = outputter.outputString(doc);
        /*try{
        DocumentBuilderFactory docBuilder = DocumentBuilderFactory.newInstance();
        DocumentBuilder  Builder = docBuilder.newDocumentBuilder();
        Document doc = Builder.newDocument();
        Element  el  = doc.createElement("RESULT");
        Element statuse = doc.createElement("STATUS");
        el.appendChild(doc.createTextNode(status));
        Element errorcodee = doc.createElement("ERRORCODE");
        el.appendChild(doc.createTextNode(errorCode));
        Element errorParametere = doc.createElement("ERRORPARAMETER");
        el.appendChild(doc.createTextNode(errorParameter));
        Element errorMsge = doc.createElement("ERRORMSG");
        el.appendChild(doc.createTextNode(errorMsg));

        XMLUtilty.getDocumentAsXml(doc);
        }catch(TransformerConfigurationException tr){

        }catch(TransformerException ex){

        }catch(ParserConfigurationException ps){

        }*/
        //result = status;
        HashMap resultx =new HashMap();
        resultx.put("returnResult", xmlText);
        if (bDebugOn) {
            System.out.println(result);
        }
        return resultx;
    }

    public String Exchange() {
        cons = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        String l_txn_proc_rc = "";

        try {

            prepareExecProcedure(cons);
            l_txn_proc_rc = executeProcedure();
            String result = extractOutputValues();
            System.out.println(l_txn_proc_rc);

            //if (SUCCESS.equals(l_txn_proc_rc)) {
            //setTransactionError (errorCode);
            //setBeanValues ();
            return result;
            //} else {

            //System.out.println("error in adding the si");
            // }

            //buildResponseMessage ();


        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getErrorCode() == 20000) {
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            /*try {
            rsetSiInq.close ();
            } catch (Exception e) {
            }*/
            try {
                procStmt.close();
            } catch (Exception e1) {
            }
            try {
                if (cons != null) {
                    cons.close();
                }
            } catch (Exception ex) {
                //instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);   

                ex.printStackTrace();
                //return  instData.result;
            }
        }

        return null;

    }
}
