/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;

import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;


/**
 *
 * @author Administrator
 */
public class PasswordGenerator extends ProcessClass {
    
     private static final Logger logger = Logger.getLogger(PasswordGenerator.class.getName());
    private String cod_usr_name;
    private String cod_usr_branch;
    private String phone_no;
    private String cod_usr_pwd;
    private String modified_by;
    private String cod_usr_id;
    //private String user_name;
    private String username;

    public PasswordGenerator() {
    }
    private String email;
    private String phone;
    private String userid;
    private String taskId;
    private String highCount;
    private String lowCount = null;
    private ResultSet rs;
    private Vector nextSearchData;
    Connection con;
    StringBuffer sSql = null;
    private static final PasswordGenerator innerClass = new PasswordGenerator();
    private String search_cust_qry = "select a.cod_usr_id,a.email,a.cod_usr_name,cod_role_id,"
            + "a.cod_usr_branch,a.cod_lang,a.cust_no,a.dat_last_signon,a.phone_no, a.flg_status, "
            + "(SELECT wmsys.wm_concat(cust_acct) from cust_acct b where b.custno = a.cust_no) acct_no from SM_USER_ACCESS a  ";
    private String genratePasswordQuery = "update SM_USER_ACCESS set cod_usr_pwd=?,login_failiure_count=0,"
            + "token_failiure_count=0,modified_date=sysdate,modified_by=?  where cod_usr_id=? ";
    private String updQry = "{?=call fn_change_password(?,?,?,?,?)}";
     //System.out.println("updQry" + updQry);
    private String updPasswd = "update SM_USER_ACCESS set cod_usr_pwd=?,login_failiure_count=0,token_failiure_count=0,maint_date=sysdate where cod_usr_id=? ";
    //private String updQry2 = "update SM_USER_ACCESS set email=?,cod_usr_id=?,maint_date=sysdate where cod_usr_id=? ";
    private String insertauthqry = "insert into auth_request_log (dat_txn,cod_user_id,session_id,"
            + "cod_retrieval_ref,cod_assgn_authorizer,request_status,cod_authorizer_sysip,"
            + "request_desc,request_par,action,action_table,action_column,action_value,cod_requester_email)"
            + " values(sysdate,?,?,auth_seq.nextval,?,?,?,?,?,?,?,?,?,?)";
    private String updQrypasscode = "update SM_USER_ACCESS set passcode=?,login_failiure_count=0,token_failiure_count=0,modified_date=sysdate,modified_by=? where cod_usr_id=? ";
    PreparedStatement pstmt;
    CallableStatement callStmt;
    private String olduser;
    private String oldpassword;
    private String newpassword;
    private String ispasscode;
    private String action;
    private String actiondesc;
    private String tablename;
    private String tablecolumn;
    private String roleid;
    private String mails;//added in order to continue the flow of using email with customer no parameter
    private String branchname;

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        nextSearchData = new Vector();
        this.phone = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "phone", 0, false, null);
        this.username = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "username", 0, false, null);
        this.email = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0, false, null);
        this.mails = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "mails", 0, false, null);
        this.userid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customername", 0, false, null);
        this.cod_usr_id = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customername", 0, false, null);
        this.olduser = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "userold", 0, false, null);
        this.lowCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "lowcount", 0, false, null);
        oldpassword = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "oldpasswd", 0, false, null);
        newpassword = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "newpasswd", 0, false, null);
        this.highCount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "highcount", 0, false, null);
        ispasscode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ispasscode", 0, false, null);
        this.action = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0, false, null);
        actiondesc = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "actiondesc", 0, false, null);
        tablecolumn = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "tablecolumn", 0, false, null);
        tablename = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "tablename", 0, false, null);
        roleid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "roleid", 0, false, null);
        branchname = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "branchname", 0, false, null);
        
        if ((inst_data.taskId.equals("10111")) || (inst_data.taskId.equals("10311")) || (inst_data.taskId.equals("10411"))) {
            this.userid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customerno", 0, false, null);


            this.email = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "email", 0, false, null);
        }//this was done as a result of not generalising the document id in jsp pages.
        System.out.println(" This userid " + userid + " \n email: " + email + " \n Branch:  " + olduser);

        /*if (!(olduser.equals("") ||olduser==null ))
         olduser.toUpperCase();*/
        this.taskId = inst_data.taskId;
        sSql = new StringBuffer(search_cust_qry);
        getRequiredSql();
        System.out.println(sSql + "modified");
        //}
    }

    private String getRequiredSql() {
        /**
         * * List of screen ids for search result*
         */
        ArrayList searchUserResult = new ArrayList();
        /**
         * * List of screen ids to return user form*
         */
        ArrayList getUserForm = new ArrayList();
        searchUserResult.add("1011");
        searchUserResult.add("1031");
        searchUserResult.add("1041");
        searchUserResult.add("1061");
        getUserForm.add("10111");
        getUserForm.add("10311");
        getUserForm.add("10411");
        //getUserForm.add("0002");
        if (searchUserResult.contains(taskId)) {
            if (email.equals("") && !(email.equals(""))) {
                System.out.println("inside email is null");
                sSql.append("where a.cod_usr_id like upper( '" + userid + "%')");
            } else if (!(email.equals("")) && userid.equals("")) {
                System.out.println("inside userid is null");
                sSql.append("where a.email like lower( '" + email + "%')");
            } else if (!(email.equals("") && userid.equals(""))) {
                System.out.println("inside both  are not null");
                sSql.append("where a.cod_usr_id like upper( '" + userid + "%') and a.email like lower( '" + email + "%')");
            }
        } else if (getUserForm.contains(taskId)) {
            sSql.append("where a.cod_usr_id = upper( '" + userid + "')");
        }

        return sSql.toString();
    }

    protected void initializeVar() {
        this.email = null;
        this.phone = null;
        this.username =null;
        this.userid = null;
        this.olduser = null;
        this.highCount = null;
        this.lowCount = null;
        this.ispasscode = null;
        this.action = null;
        this.actiondesc = null;
        sSql = new StringBuffer("");
        pstmt = null;
        rs = null;
        this.tablecolumn = null;
        this.tablecolumn = null;
        roleid = null;
        mails = null;
        branchname = null;
        callStmt = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        System.out.println("in process request method");
        HashMap hashMap = null;
        String xmlText = null;
        initializeVar();
        con = com.fasyl.ebanking.db.DataBase.getConnection();



        try {
            con.setAutoCommit(false);
            setInstanceVar(instData);
            Element results = new Element("ROWSET");
            if (!((instData.taskId.equals("101111")) || (instData.taskId.equals("104111")) || (instData.taskId.equals("0002")))) {///this is not for the pages that will display final result of set of operations in user management
                System.out.println("in if statement 1>>>>>>");
                pstmt = con.prepareStatement(sSql.toString());
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    nextSearch pwdGen = innerClass.new nextSearch(rs);
                    this.nextSearchData.add(pwdGen);
                }

                int highcount = this.highCount == null ? 20 : Integer.parseInt(this.highCount);
                int lowcount = this.lowCount == null ? 0 : Integer.parseInt(this.lowCount);
                int size = nextSearchData.size();
                if (size <= 0) {
                    //throw exception
                    instData.result = Utility.processError(instData, USERNOTFOUND);
                    return instData.result;

                }
                highCount = String.valueOf(highcount);
                lowCount = String.valueOf(lowcount);
                System.out.println(" This is the size:" + size);
                int countLimit = 0;
                if (highcount > size) {
                    countLimit = size - 1;
                } else {
                    countLimit = highcount;
                }
                System.out.println("This is the countLimit:" + countLimit);
                for (int i = lowcount; i <= countLimit; i++) {
                    Element result = new Element("ROW");
                    Element xuserid = new Element("USERID");
                    xuserid.setText(((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_userid());
                    Element xemail = new Element("EMAIL");
                    xemail.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_email()));
                    Element xusername = new Element("USERNAME");
                    xusername.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_username()));
                    Element userbranch = new Element("USERBRANCH");
                    userbranch.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_brachCode()));
                    Element roleid = new Element("COD_ROLE_ID");
                    roleid.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_roleid()));
                    Element lang = new Element("LANG");
                    lang.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_lang()));
                    Element xcustno = new Element("CUSTNO");
                    xcustno.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_custno()));
                    Element xlastlogindate = new Element("LASTLOGINDATE");
                    xlastlogindate.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_lastlogindate()));
                    Element xphone = new Element("PHONE");
                    xphone.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_phone_no()));
                    Element xhighcount = new Element("HIGHCOUNT");
                    xhighcount.setText(String.valueOf(highcount));
                    Element xlowcount = new Element("LOWCOUNT");
                    xlowcount.setText(String.valueOf(lowcount));
                    Element xsize = new Element("SIZE");
                    xsize.setText(String.valueOf(size));
                    Element xcustomername = new Element("SEARCHNAME");
                    xcustomername.setText(this.userid);
                    Element xcustomerno = new Element("SEARCHEMAIL");
                    xcustomerno.setText(this.email);
                    Element xacctno = new Element("ACCT_NO");
                    xacctno.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_acct_no()));
                    Element xflgstatus = new Element("FLG_STATUS");
                    xflgstatus.setText((((PasswordGenerator.nextSearch) this.nextSearchData.get(i)).getInner_flg_status()));
                    
                   
                    result.addContent(xuserid);
                    result.addContent(xemail);
                    result.addContent(xphone);
                    result.addContent(xusername);
                    result.addContent(userbranch);
                    result.addContent(roleid);
                    result.addContent(lang);
                    result.addContent(xcustno);
                    result.addContent(xlastlogindate);
                    result.addContent(xhighcount);
                    result.addContent(xlowcount);
                    result.addContent(xsize);
                    result.addContent(xacctno);
                    result.addContent(xflgstatus);
                    results.addContent(result);

                }


                //results.addContent(results);

            }
            else {
                System.out.println("in else statetement 1>>>>>>");
                //Element results = new Element("ROWSET");
                if (true) {//write code to ping server
                    String password = Utility.genaratePassword();
                    String paswordText = password;
                    System.out.println("Before encryption ");
                    password = Encryption.encrypt(password);
                    System.out.println("after encryption " + password);

                    //Declaration moved up here for the sake of generating ErrorMsg in else
                    String errorMsg = "";
                    if (instData.taskId.equals("104111")) {

                        ModificationDAO modDAO;
                        HashMap<String, String> modResult;
                        String action_column;
                        String action_value;
                                    
                        //This happens if it is modify user
                        if(action.equalsIgnoreCase("M")) {
                            
                            //This is assumed by Ayo to be for modified user. Contact me or Debug app if otherwise
                            modDAO = new ModificationDAO();
                            modResult = modDAO.buildModifiedColAndValStr(userid, username, email, phone, branchname, roleid);
                            errorMsg = modResult.get("Error");
                            action_column = modResult.get("Columns");
                            action_value = modResult.get("Values");
                        } else { //Else is for other approval requests
                            action_column = tablecolumn; //Table column was set from session.js for some
                            action_value = roleid; //Ayo says RoleID from session.js has been messed up
                        }
                        
                        if(errorMsg == null) {
                            System.out.println("Error msg is null");
                        }
                        else {
                            System.out.println("Error Message length: " + errorMsg.length());
                            System.out.println("Error msg: " + errorMsg);
                        }
                        
                        //If error message doesnt have a value
                        if(errorMsg.isEmpty()) {
                            if(con != null && con.isClosed()) {
                                con = com.fasyl.ebanking.db.DataBase.getConnection();
                            }
                            pstmt = con.prepareStatement(this.insertauthqry);
                            //System.out.println(this.email + this.userid + this.olduser);
                            pstmt.setString(1, userid.toUpperCase().trim());
                            pstmt.setString(2, instData.sessionId);
                            pstmt.setString(3, "");
                            pstmt.setString(4, "W");
                            pstmt.setString(5, instData.remoteAddress);
                            pstmt.setString(6, actiondesc);
                            pstmt.setString(7, instData.request);
                            pstmt.setString(8, action);
                            pstmt.setString(9, tablename);
                            pstmt.setString(10, action_column);
                            pstmt.setString(11, action_value);
                            pstmt.setString(12, instData.email); //Added to know auth_requester admin
                        }
                    } //End if (instData.taskId.equals("104111")) 
                    else {
                        
                        //pstmt = con.prepareStatement(this.updQry);
                         
                        if (!(ispasscode == null || ispasscode.equals("")) && ispasscode.equals("true")) {
                            pstmt = con.prepareStatement(this.updQrypasscode);
                            pstmt.setString(1, password);
                            logger.info("password :" + password);
                            pstmt.setString(2, instData.userId);
                            logger.info("userid :" + instData.userId);
                            pstmt.setString(3, this.userid.toUpperCase().trim());
                            System.out.println("actual userid :" + this.userid.toUpperCase().trim());
                        }
                        if(ispasscode == null || ispasscode.equals("")){ //This should not happen. Just to prevent NullPOinter
                            ispasscode = "false";
                        }
                        if (this.newpassword == null && !ispasscode.equals("true")) {

                            pstmt = con.prepareStatement(this.genratePasswordQuery);
                            pstmt.setString(1, password);
                            logger.info("password :" + password);
                            pstmt.setString(2, instData.userId);
                            logger.info("userid :" + instData.userId);
                            pstmt.setString(3, this.userid.toUpperCase().trim());
                            System.out.println("actual userid :" + this.userid.toUpperCase().trim());
                        } else if (!ispasscode.equals("true")) {
                            //Validate User inout before call to Database: Ayo
                            if(this.oldpassword.trim().equals("") ||
                                    this.newpassword.trim().equals("")) {
                                errorMsg = "You need to enter value in the password field";
                            } else {
                                System.out.println("About to change pssword: " + updQry);
                                callStmt = con.prepareCall(updQry);
                                callStmt.registerOutParameter(1, OracleTypes.VARCHAR);
                                callStmt.registerOutParameter(5, OracleTypes.VARCHAR);
                                callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
                                callStmt.setString(2, Encryption.encrypt(this.oldpassword));
                                callStmt.setString(3, Encryption.encrypt(this.newpassword));
                                callStmt.setString(4, instData.userId);
                                callStmt.execute();
                                System.out.println("Result of " + updQry + " is " + callStmt.getString(1));
                                System.out.println("Err_Code: " + callStmt.getString(5));
                                System.out.println("Err_Reason " + callStmt.getString(6));
                            }
//                            pstmt.setString(1, Encryption.encrypt(this.newpassword));
//                            pstmt.setString(2, instData.userId);
//                            pstmt.setString(3, instData.userId);
                        }
                    }
                    //if below added by Ayo to prevent Null pointer exception in case line 314 is not executed
                    int upd = 0;
                    boolean updated = false;
                    if(errorMsg.isEmpty() && callStmt == null) { 
                        System.out.println("About to call update :");
                        upd = pstmt.executeUpdate();
                        if(upd > 0) {
                            updated = true;
                        }
                        System.out.println("This is the number of rows updated " + upd);
                    } else if(callStmt != null) {
                        upd = Integer.parseInt(callStmt.getString(1)); //This is for change password
                        if(upd == 0) {
                            updated = true;
                        } else if(upd == 201) { 
                            errorMsg = "New password has recently been used by you, enter a different password";
                            updated = false;
                        } else if(upd == 202) {
                            errorMsg = "Old password entered is incorrect";
                            updated = false;
                        } else{
                            errorMsg = "Error changing password, Try again later";
                            updated = false;
                        }
                    }
                    
                    boolean resultz = false;
                    String header = "Passcode Notification";
                    if (updated) {
                        String body = null;
                        if (!(instData.taskId.equals("104111") || instData.taskId.equals("0002"))) {
                            String type = "text/html";
                            if (!(ispasscode == null || ispasscode.equals("")) && ispasscode.equals("true")) {
                                header = "Passcode";
                                body = "Dear Customer,<br><br>Your passcode is " + paswordText + "<br><br>"
                                        + "Click <a href='https://ibank.trustbondmortgagebankplc.com/IntBanking'>https://ibank.trustbondmortgagebankplc.com/IntBanking</a> to login.<br><br> ";
                            } else {
                                header = "Change Password";
                                body = "Dear Customer,<br><br>Your new password is   " + paswordText + " <br><br>";

                            }

                            String toemail = mails;
                            String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");


                            sendmail send = new sendmail();

                            //send.sendMessage(header, body, password, type, fromemail, toemail);
                            resultz = send.sendMessage(header, body, password, type, fromemail, toemail);
                            if (!resultz) {
                                try {
                                    con.rollback();
                                } catch (Exception e) {
                                    instData.result = Utility.processError(instData, EMAILSERVERERROR);
                                    e.getMessage();
                                }
                                instData.result = Utility.processError(instData, EMAILSERVERERROR);

                                return instData.result;

                            } else {
                                if(con.isClosed()) {
                                    con = com.fasyl.ebanking.db.DataBase.getConnection();
                                }// End if(!con.isClosed())
                                con.commit(); 
                            } //End else for if (!resultz)
                        }  else { //Else for if (!(instData.taskId.equals("104111") || instData.taskId.equals("0002")))
                            
                            //Ib admin gets notified when a user is Modified   
                            String type = "text/html";
                            header    = "Authorization Alert (User Modification)";
                            body ="Dear Admin,<br><br> Please note that there is an item <b>(User ID:" +userid+")</b> pending authorization";
                            String   toemail   = (String) LoadEmails.getDataValuenew("AUTH");
                            String   fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
                            sendmail send = new sendmail();
                            resultz = send.sendMessage(header, body, password, type, fromemail, toemail);
                            System.out.println("toemail" +toemail);
                
                        }

                        Element result = new Element("ROW");
                        Element xuserid = new Element("PASSWORD");
                        if ((instData.taskId.equals("101111"))) {
                            if (ispasscode.equals("false")) {
                                xuserid.setText("The password has been sent to the email for activation");//this suppose to contain message loaded from the database;
                            } else {
                                xuserid.setText("Your passcode has been sent to the email for activation");//this suppose to contain message loaded from the database;   
                            }
                        } else {
                            /**
                             * This is assumed for successful operation on (Modify User by ib user)
                             * taskID 104111 as at the point of this writing by Ayo
                             */
                            xuserid.setText("The operation has been done successfully");
                        }
                        result.addContent(xuserid);
                        results.addContent(result);
                    } else {
                        //write code for the exception thrown
                        Element result = new Element("ROW");
                        Element xuserid = new Element("PASSWORD");
                        //Ayo put the next line for error during modify user
                        xuserid.setText(errorMsg);
                        result.addContent(xuserid);
                        results.addContent(result);
                    }
                }
            }
            org.jdom.Document doc = new org.jdom.Document(results);
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
            xmlText = outputter.outputString(doc);
            if (bDebugOn) {
                System.out.println(xmlText);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            try {
                if(con.isClosed()) {
                    con = com.fasyl.ebanking.db.DataBase.getConnection();
                }// End if(!con.isClosed())
                con.rollback();
            } catch (Exception e) {
                e.getMessage();
            }
            return instData.result;
        } finally {
            
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (callStmt != null) {
                try {
                    callStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        String usertypes = null;
        if (instData.taskId.equals("10411")) {
            usertypes = getDistinctUserTypes();
            if (usertypes == null || usertypes.equals("")) {
                instData.result = Utility.processError(instData, "Error while retrieving usertypes,try again later");
            }
        }
        hashMap = new HashMap();
        hashMap.put(RESULT_MAP_KEY, xmlText);
        hashMap.put(OTHER_REQ_RES, usertypes);
        instData.result = hashMap;
        return instData.result;
    }

    public class nextSearch {

        private String inner_userid;
        private String inner_email;
        private String inner_username;
        private String inner_brachCode;
        private String inner_lowCount;
        private String inner_custno;
        private String inner_highCount;
        private String inner_lang;
        private String inner_lastlogindate;
        private String inner_roleid;
        private String inner_phone_no;
        private String inner_acct_no;
        private String inner_flg_status;

        public nextSearch(ResultSet rs) {
            try {
                this.inner_userid = rs.getString("cod_usr_id");
                this.inner_email = rs.getString("email");
                this.inner_highCount = highCount;
                this.inner_username = rs.getString("cod_usr_name");
                this.inner_brachCode = rs.getString("cod_usr_branch");
                this.inner_lowCount = lowCount;
                this.inner_custno = rs.getString("cust_no");
                this.inner_lang = rs.getString("cod_lang");
                this.inner_lastlogindate = rs.getString("dat_last_signon");
                this.inner_phone_no = rs.getString("phone_no");
                this.inner_roleid = rs.getString("cod_role_id");
                this.inner_acct_no = rs.getString("acct_no");
                this.inner_flg_status = rs.getString("flg_status");

                if(this.inner_acct_no != null) {
                    this.inner_acct_no = this.inner_acct_no.replace(",", " ");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        public String getInner_phone_no() {
            return inner_phone_no;
        }

        public void setInner_phone_no(String inner_phone_no) {
            this.inner_phone_no = inner_phone_no;
        }

        /**
         * @return the inner_userid
         */
        public String getInner_userid() {
            return inner_userid;
        }

        /**
         * @param inner_userid the inner_userid to set
         */
        public void setInner_userid(String inner_userid) {
            this.inner_userid = inner_userid;
        }

        /**
         * @return the inner_email
         */
        public String getInner_email() {
            return inner_email;
        }

        /**
         * @param inner_email the inner_email to set
         */
        public void setInner_email(String inner_email) {
            this.inner_email = inner_email;
        }

        /**
         * @return the inner_username
         */
        public String getInner_username() {
            return inner_username;
        }

        /**
         * @param inner_username the inner_username to set
         */
        public void setInner_username(String inner_username) {
            this.inner_username = inner_username;
        }

        /**
         * @return the inner_brachCode
         */
        public String getInner_brachCode() {
            return inner_brachCode;
        }

        /**
         * @param inner_brachCode the inner_brachCode to set
         */
        public void setInner_brachCode(String inner_brachCode) {
            this.inner_brachCode = inner_brachCode;
        }

        /**
         * @return the inner_lowCount
         */
        public String getInner_lowCount() {
            return inner_lowCount;
        }

        /**
         * @param inner_lowCount the inner_lowCount to set
         */
        public void setInner_lowCount(String inner_lowCount) {
            this.inner_lowCount = inner_lowCount;
        }

        /**
         * @return the inner_custno
         */
        public String getInner_custno() {
            return inner_custno;
        }

        /**
         * @param inner_custno the inner_custno to set
         */
        public void setInner_custno(String inner_custno) {
            this.inner_custno = inner_custno;
        }

        /**
         * @return the inner_highCount
         */
        public String getInner_highCount() {
            return inner_highCount;
        }

        /**
         * @param inner_highCount the inner_highCount to set
         */
        public void setInner_highCount(String inner_highCount) {
            this.inner_highCount = inner_highCount;
        }

        /**
         * @return the inner_lang
         */
        public String getInner_lang() {
            return inner_lang;
        }

        /**
         * @param inner_lang the inner_lang to set
         */
        public void setInner_lang(String inner_lang) {
            this.inner_lang = inner_lang;
        }

        /**
         * @return the inner_lastlogindate
         */
        public String getInner_lastlogindate() {
            return inner_lastlogindate;
        }

        /**
         * @param inner_lastlogindate the inner_lastlogindate to set
         */
        public void setInner_lastlogindate(String inner_lastlogindate) {
            this.inner_lastlogindate = inner_lastlogindate;
        }

        /**
         * @return the inner_roleid
         */
        public String getInner_roleid() {
            return inner_roleid;
        }

        /**
         * @param inner_roleid the inner_roleid to set
         */
        public void setInner_roleid(String inner_roleid) {
            this.inner_roleid = inner_roleid;
        }

        /**
         * @return the inner_acct_no
         */
        public String getInner_acct_no() {
            return inner_acct_no;
        }

        /**
         * @param inner_acct_no the inner_acct_no to set
         */
        public void setInner_acct_no(String inner_acct_no) {
            this.inner_acct_no = inner_acct_no;
        }

        /**
         * @return the inner_flg_status
         */
        public String getInner_flg_status() {
            return inner_flg_status;
        }

        /**
         * @param inner_acct_no the inner_acct_no to set
         */
        public void setInner_flg_status(String inner_flg_status) {
            this.inner_flg_status = inner_flg_status;
        }
        
        
    }
}
