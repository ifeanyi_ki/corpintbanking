
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

//import com.fasyl.ebanking.main.sendmail;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class AccountPositionold extends Processes {
    private static final int    NBR_PARAMS         = 2;
    private static final String TRANSACTION_DETAIL = "FN_GETACCTPOSITION";
    private String              account;
    private String              custNo;
    private String              isUsed;

    // Connection connect;
    ArrayList       list;
    private HashMap preResult;
    private String  refNo;
    private String  returnResult;

    public AccountPositionold() {}

    private void initializeVar() {
        this.account      = null;
        this.custNo       = null;
        this.refNo        = null;
        this.returnResult = null;
        this.isUsed       = null;
        this.preResult    = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false,
                null);
        this.refNo  = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false,
                null);
        this.custNo = inst_data.custId;
        System.out.println(account + " this is the account");

//      if (account.compareTo("")==0)account=null;
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        instData.result = ExchangeMap(connect);

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.TRANSACTION_DETAIL, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        System.out.println(this.custNo);

        /*
         * procStmt.setString(
         * 2, this.account);
         */
        procStmt.setString(2, this.custNo);
        procStmt.setString(3, this.account);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
