
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GeneralReport extends ProcessClass {
    private Connection connect = null;

    // private Connection connectToFcc = null;
    //private CallableStatement pstmt   = null;
    private String            trxtype = null;
    private String            xmlText = null;
    String                    LoginUserid;
    private String            account;
    private String            bulkamtxml;
    private String            customerno;
    private String            endDate;
    private String            highCount;
    DataofInstance            instdata;
    private String            isAddAccount;
    private String            lowCount;
    private Element           parent;
    private String            startDate;

    public GeneralReport() {}

    protected void initializeVar() {
        trxtype          = null;
        this.customerno  = null;
        this.account     = null;
        this.LoginUserid = null;
        endDate          = null;
        startDate        = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent  = new Element("ROWSET");
        endDate = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "to", 0,
                true, null), "UTF-8");
        startDate = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "from", 0,
                true, null), "UTF-8");
        trxtype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "trxtype",
                0, true, null), "UTF-8");

        if (bDebugOn) {
            System.out.println("startdate " + startDate + " enddate " + endDate + " trx type " + trxtype);
        }

        if ((startDate == null) || startDate.equals("")) {
            startDate = "N";
        }

        if ((endDate == null) || endDate.equals("")) {
            endDate = "N";
        }

        this.LoginUserid = inst_data.userId;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        this.initializeVar();
        instdata = instData;

        boolean           result    = false;
        CallableStatement statement = null;

        if (bDebugOn) {
            System.out.println(instData.txnId);
        }

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            statement = connect.prepareCall("{?=call fn_report(?,?,?,?,?,?)}");
            statement.registerOutParameter(1, OracleTypes.CLOB);
            statement.registerOutParameter(6, OracleTypes.CLOB);
            statement.registerOutParameter(7, OracleTypes.VARCHAR);
            statement.setString(2, instData.requestId);
            statement.setString(3, trxtype);
            statement.setString(4, startDate);
            statement.setString(5, endDate);
            statement.execute();
            instData.result = new HashMap();

           // java.sql.Clob datas   = procStmt.getClob(1);
            String        data    = this.ManageClob(statement.getClob(1));
            String        results = data;

            if (bDebugOn) {
                System.out.println(results);
            }

            if ((results == null) || results.equals("")) {
                results = "<ROWSET></ROWSET>";
            }

            setReportTaskID(instData, trxtype);
            instData.result.put(RESULT_MAP_KEY, results);
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    private void setReportTaskID(DataofInstance instdata, String trxtype) {
        if (trxtype.equals("AAU")) {
            instdata.taskId = "9012";
        } else if (trxtype.equals("AUTHAUM")) {
            instdata.taskId = "9013";
        } else if (trxtype.equals("AUTHAAA")) {
            instdata.taskId = "9014";
        } else if (trxtype.equals("AUTHAAR")) {
            instdata.taskId = "9015";
        }else if (trxtype.equals("CFLOG")) {
            instdata.taskId = "9016";
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
