
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GetChequeStatus extends Processes {

    // protected CallableStatement procStmt;
    private static final int    NBR_PARAMS                = 2;
    private static final String PROCEDURE_SEARCH_CUSTOMER = "F_CHEQUE_STATUS";    // I will need to rewrite this procedure
    public static final String  SUCCESS                   = "false";
    private String              acct;
    private String              chequeno;
    private String              customerName;
    private Connection          dbConnection;
    private String              returnResult;

    public GetChequeStatus() {}

    public void initializeVar() {
        this.chequeno     = null;
        this.procStmt     = null;
        this.returnResult = null;
        this.customerName = null;
        this.acct         = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.acct     = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acno", 0, false,
                null);
        this.chequeno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "checkno", 0, false,
                null);
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);

            if (this.acct == null) {
                instData.result = Utility.processError(instData, ERR_ACCTNOISNULL);

                return instData.result;
            } else if (this.chequeno == null) {
                instData.result = Utility.processError(instData, ERR_CHEQUENOISNULL);

                return instData.result;
            }
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap       result = new HashMap();
        java.sql.Clob datas  = procStmt.getClob(1);
        String        data   = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SEARCH_CUSTOMER, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(2, this.acct);
        procStmt.setString(3, this.chequeno);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
