
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;

/**
 *
 * @author Administrator
 *
 */
public class ManagePasscode extends ProcessClass {
    private Connection con            = null;
    private String     conf_account   = null;
    private String     conf_email     = null;
    private String     conf_image     = null;
    private String     updQrypasscode =
        "update SM_USER_ACCESS set passcode=?,flg_status='A',login_failiure_count=0,maint_date=sysdate where cod_usr_id=? ";
    private String            custuserid;
    private String            ispasscode;
    private CallableStatement pstmt;

    public ManagePasscode() {}

    protected void initializeVar() {
        conf_image   = null;
        conf_email   = null;
        conf_account = null;
        custuserid   = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        conf_image = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "conf_image", 0, true, null), "UTF-8");
        conf_email = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "conf_email", 0, true, null), "UTF-8");
        conf_account = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "conf_account", 0, true, null), "UTF-8");
        custuserid = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "userid",
                0, true, null), "UTF-8");
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {

        int    result      = -1;
        String body        = null;
        String type        = "text/html";
        String passcode    = Utility.genaratePassword();
        String pascodetext = passcode;

        System.out.println("Before encryption " + passcode);
        passcode = Encryption.encrypt(passcode);

        boolean mailResult = false;

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = con.prepareCall("{?=call generate_passcode(?,?,?,?,?,?,?,?)}");
            pstmt.registerOutParameter(1, OracleTypes.INTEGER);
            pstmt.registerOutParameter(9, OracleTypes.CLOB);
            pstmt.setString(2, custuserid);
            pstmt.setString(3, instData.userId);
            pstmt.setString(4, conf_image);
            pstmt.setString(5, conf_account);
            pstmt.setString(6, conf_email);
            pstmt.setString(7, passcode);
            pstmt.setString(8, instData.sessionId);
            pstmt.execute();
            result = pstmt.getInt(1);

            if (result == 0) {
                body = "Dear Customer,<br><br>Your passcode is " + pascodetext + "<br>Please use this for first time login only.<br /> Thank You.<br /><br /> Regards,<br/>TrustBond Mortgage Bank Internet Banking Service";

                String   header    = "Change Password";
                String   toemail   = instData.email;
                String   fromemail = "ibanking@trustbondmortgagebankplc.com";
                sendmail send      = new sendmail();

                mailResult = send.sendMessage(header, body, passcode, type, fromemail, toemail);

                if (mailResult) {
                    HashMap       hashMap = new HashMap();
                    java.sql.Clob datas   = procStmt.getClob(3);
                    String        data    = datas.getSubString(1, (int) datas.length());

                    hashMap.put("returnResult", data);
                    instData.result = hashMap;
                } else {
                    instData.result = Utility.processError(instData,
                            "There is problem with mail server, Kndly try again later");

                    return instData.result;
                }
            } else {
                instData.result = Utility.processError(instData, "System error,Try again later");
            }
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, "system Error while generating passcode");
            ex.printStackTrace();
        } finally {
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
