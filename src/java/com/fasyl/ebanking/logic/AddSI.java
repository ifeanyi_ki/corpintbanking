/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import oracle.jdbc.OracleTypes;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 *
 * @author Administrator
 */
public class AddSI extends Processes {

    boolean bDebugOn = true;
    HashMap procResult = null;
    String userRef = "";
    String endianesDate = "";
    private final String PROCEDURE_SI_UPLOAD = "EBANKING_SI.UPLOAD_SI";
    protected CallableStatement procStmt;
    private static final int NBR_PARAMS = 8;
    public static Random sequenceIdRandom = new Random();
    public  final String SEL_SI_PRD = "SELECT b.product_code, b.product_type, b.exec_days, b.exec_mths, b.exec_yrs," +
            "b.action_code_amount, b.cal_hol_excp, b.rate_type, b.max_retry_count, " +
            "b.min_sweep_amt, b.processing_time, b.instruction_code, " +
            "b.transfer_type, b.si_type, b.referral_required " +
            "FROM sitm_product_prf b where b.product_code =? ";
    Connection con = null;
    Connection con2 = null;
    public static final String INS_INTO_REQD_TABLE = "INSERT INTO SITB_UPLOAD_MASTER " +
            "(                       " +
            "BRANCH_CODE,        " +
            "SOURCE_CODE,        " +
            "SOURCE_REF,         " +
            "DETAIL_REF,         " +
            "USER_REF_NUMBER,    " +
            "SI_EXPIRY_DATE,     " +
            "BOOK_DATE,          " +
            "PRODUCT_CODE,       " +
            "COUNTERPARTY,       " +
            "PRODUCT_TYPE,       " +
            "SERIAL_NO,          " +
            "ACTION_CODE_AMT,    " +
            "APPLY_CHG_SUXS,     " +
            "APPLY_CHG_PEXC,     " +
            "APPLY_CHG_REJT,     " +
            "MAX_RETRY_COUNT,    " +
            "MIN_SWEEP_AMT,      " +
            "DR_ACC_BR,          " +
            "DR_ACCOUNT,         " +
            "DR_ACC_CCY,         " +
            "SI_AMT_CCY,         " +
            "SI_AMT,             " +
            "CR_ACC_BR,          " +
            "CR_ACCOUNT,         " +
            "CR_ACC_CCY,         " +
            "PRIORITY,           " +
            "CHARGE_WHOM,        " +
            "MIN_BAL_AFTER_SWEEP," +
            "INTERNAL_REMARKS,   " +
            "ICCF_CHANGED,       " +
            "TAX_CHANGED,        " +
            "SETTLE_CHANGED,     " +
            "MIS_CHANGED,        " +
            "CONV_STATUS,        " +
            "ERR_MSG             " +
            " ) VALUES (   ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?,    ?       )";
    public static final String INS_INTO_INST_TABLE = "INSERT INTO SITB_UPLOAD_INSTRUCTION( " +
            "BRANCH_CODE,                " +
            "SOURCE_CODE,                " +
            "SOURCE_REF,                 " +
            "PRODUCT_CODE,               " +
            "PRODUCT_TYPE,               " +
            "SI_TYPE,                    " +
            "CAL_HOL_EXCP,               " +
            "RATE_TYPE,                  " +
            "EXEC_DAYS,                  " +
            "EXEC_MTHS,                  " +
            "EXEC_YRS,                   " +
            "FIRST_EXEC_DATE,            " +
            "NEXT_EXEC_DATE,             " +
            "FIRST_VALUE_DATE,           " +
            "NEXT_VALUE_DATE,            " +
            "MONTH_END_FLAG,             " +
            "PROCESSING_TIME,            " +
            "USER_INST_NO,               " +
            "INST_STATUS,                " +
            "AUTH_STATUS,                " +
            "LATEST_VERSION_DATE,        " +
            "BOOK_DATE,                  " +
            "SERIAL_NO,                  " +
            "COUNTERPARTY,               " +
            "LATEST_CYCLE_NO,            " +
            "LATEST_CYCLE_DATE,          " +
            "CONV_STATUS,                " +
            "ERR_MSG )                   " +
            "VALUES(?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?,                                 ?) ";
    private String serialresult = "";
    private String SourceRef = "";
    private SIObject siobject=null;

    public AddSI(SIObject siobject){

        this.siobject=siobject;
        System.out.println("Inside Constructor addsi drbranchcode: "+siobject.getDrBranchCode());

    }

    public java.sql.Date currentDate() {

        Date date = new Date();
        java.sql.Date sqldat = new java.sql.Date(date.getTime());
        return sqldat;

    }

    public SiProductVO getProductVO(String productCode,Connection cons) {
 System.out.println("===== Inside getProductVO === ");
        SiProductVO siProductVO = new SiProductVO() ;
        PreparedStatement pstmts = null;
        ResultSet rs = null;


        try {
            pstmts = cons.prepareStatement(SEL_SI_PRD);
            System.out.println(SEL_SI_PRD);
            pstmts.setString(1, productCode);
            System.out.println("====This is the product code "+productCode);
            rs=pstmts.executeQuery();
            System.out.println("O GA OOOOOOOOOO");
            //System.out.println("rs : " + rs.getString(1));
            while (rs.next()) {
                siProductVO.setProductcode(rs.getString(1));
                System.out.println(rs.getString(1)+" why");
                siProductVO.setProducttype(rs.getString(2));
                System.out.println(rs.getString(2)+" why1");
                //siProductVO.setExecdays(rs.getInt(3));
                //siProductVO.setExecmths(rs.getInt(4));
                //siProductVO.setExecyrs(rs.getInt(5));
                System.out.println(rs.getString(6)+" This is action code amount");
                siProductVO.setActioncodeamount(rs.getString(6));
                System.out.println("why2");
                siProductVO.setCalholexcp(rs.getString(7));
                System.out.println("why3");
                siProductVO.setRatetype(rs.getString(8));
                System.out.println("why4");
                siProductVO.setMaxretrycount(rs.getInt(9));
                System.out.println("why5");
                siProductVO.setProcessingtime(rs.getString(10));
                System.out.println("why6");
                siProductVO.setMinsweepamt(rs.getString(11));
                System.out.println("why7");
                siProductVO.setInstructioncode(rs.getString(12));
                System.out.println("why8");
                siProductVO.setTransfertype(rs.getString(13));
                System.out.println("why9");
                siProductVO.setSitype(rs.getString(14));
                System.out.println("why10");
                siProductVO.setReferralrequired(rs.getString(15));
                System.out.println("why11");

    System.out.println("====== retrieved product Code =====");
            }


        } catch (SQLException ex) {
            /*if (bDebugOn)*/{
                System.out.println("sql : " + ex);
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            /*if (bDebugOn)*/ {
                System.out.println("exception: " + ex);
                ex.printStackTrace();
            }
        } 


        return siProductVO;

    }
     public HashMap processRequest(DataofInstance instData){
             System.out.println(" ******Inside process request **** ");
             //initializeVar();
              //custno = instData.custId;
          instData.result=ExchangeMap(this.con);


        return instData.result;

    }
    private void setInputMasterUpload(SIObject siObject) {
    }

    public String EndianesDate() {
        StringBuffer buff = new StringBuffer();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(cal.YEAR);
        String years = year + "";

        buff.append(years.substring(2, 4));
        System.out.println(buff.toString());
        int days = cal.get(cal.DAY_OF_YEAR);
        buff.append(days);
        System.out.println(buff.toString());

        return buff.toString();
    }

//     public static void main(String[] args) {
//   // AddSI addSi = new AddSI();
//   // addSi.getProductVO("SIP1", con3);
//   /* org.jdom.Element results = new org.jdom.Element("RESULT");
//       // Element transdetaildr = new Element("Transdetaildr");
//                    Element statuse = new Element("STATUS");
//                    statuse.setText("status");
//                    Element errorcodee = new Element("errorCode");
//                    errorcodee.setText("errorCode");
//                    Element errorParametere = new Element("errorParameter");
//                    errorParametere.setText("errorParameter");
//                    Element errorMsge = new Element("errorMsg");
//                    errorMsge.setText("errorMsg");
//                    results.addContent(statuse);
//                    results.addContent(errorcodee);
//                    results.addContent(errorParametere);
//                    results.addContent(errorMsge);
//                    org.jdom.Document doc = new org.jdom.Document(results);
//            XMLOutputter outputter = new XMLOutputter();
//            outputter.setFormat(Format.getPrettyFormat());
//          //  outputter.toString();
//            String xmlText = outputter.outputString(doc);
//            System.out.println(xmlText);
//    //String test = addSi.EndianesDate();
//    //System.out.println(test);
//    //System.out.println(addSi.getSourceRef());*/
//
//     /*try{
//                    DocumentBuilderFactory docBuilder = DocumentBuilderFactory.newInstance();
//                    DocumentBuilder  Builder = docBuilder.newDocumentBuilder();
//                    Document doc = Builder.newDocument();
//                    Element  el  = doc.createElement("RESULT");
//                    Element statuse = doc.createElement("STATUS");
//                    statuse.appendChild(doc.createTextNode(String.valueOf("WHY")));
//                    el.appendChild(statuse);
//                    Element errorcodee = doc.createElement("ERRORCODE");
//                    errorcodee.appendChild(doc.createTextNode(String.valueOf("ERRORCODE")));
//                    el.appendChild(errorcodee);
//                    Element errorParametere = doc.createElement("ERRORPARAMETER");
//                    errorParametere.appendChild(doc.createTextNode(String.valueOf("ERRORPARAMETER")));
//                    el.appendChild(errorParametere);
//                    Element errorMsge = doc.createElement("ERRORMSG");
//                    errorMsge.appendChild(doc.createTextNode(String.valueOf("ERRORMAESSAGE")));
//                    el.appendChild(errorMsge);
//
//                    String test = XMLUtilty.getDocumentAsXml(doc);
//                    System.out.println(test);
//        }catch(TransformerConfigurationException tr){
//
//        }catch(TransformerException ex){
//
//        }catch(ParserConfigurationException ps){
//
//        }
//        //return result;*/
//    }
    private String getUserRef(String Serials, SIObject siobject, String endianesdate) {
        StringBuffer userref = new StringBuffer();
        userref.append(siobject.getDrBranchCode());//need to test if they want the user to do more that payment type of SI
        userref.append(siobject.getSiType());
        userref.append(endianesdate);
        userref.append(Serials);


        return userref.toString();

    }

    private String getSerial() {
        StringBuffer serial = new StringBuffer();
        int iserial = 0;
        for (int a = 0; a <= 3; a++) {
            iserial = sequenceIdRandom.nextInt(4);
            serial.append(String.valueOf(iserial));
        }
        return serial.toString();
    }

    private String getSourceRef(String serialres, String endianesdate) {

        StringBuffer rets = new StringBuffer();
        String ret = "";
        int iResult = 0;
        rets.append(siobject.getDrBranchCode());//need to use the dr account branch

        rets.append(serialres);


        rets.append(endianesdate);
        return rets.toString();

    }

    public boolean loadData(SIObject info) {
        boolean result = false;
        SIObject siObject = info;
        PreparedStatement pstmt = null;
        int isUpdate = 0;
        String constr = null;
        endianesDate = EndianesDate();
        serialresult = getSerial();
        SourceRef = getSourceRef(serialresult, endianesDate);
        System.out.println("This is the sourceref: "+SourceRef);
        String productCode = info.getSiType();
        System.out.println("This is the product code " +productCode);
        userRef = getUserRef(serialresult, siObject, endianesDate);
        System.out.println(" This is the user ref: " + userRef);
        SiProductVO product = new SiProductVO();
        con = com.fasyl.ebanking.db.DataBase.getConnection();
        product = getProductVO(siObject.getSiType(),con);
       // String custno = "";
        //String counterParty = custno;// custno will be supplied by the calling servlet


        if (product != null) {

            try {
                System.out.println("====== about to getconnection to fcc======");
                con.setAutoCommit(false);
                pstmt = con.prepareStatement(INS_INTO_REQD_TABLE);
                pstmt.setString(1, siObject.getDrBranchCode());//I will need to test for transaction type if any other si type is going to be included.
                pstmt.setString(2, "SI_UPLOAD");// need to add this to the part of what to return from getsi
                pstmt.setString(3, SourceRef);
                pstmt.setString(4, "EB");
                pstmt.setString(5, userRef);
                pstmt.setDate(6, siObject.getSiFinalExecDate());
                pstmt.setDate(7,currentDate() );
                pstmt.setString(8, siObject.getSiType());
                pstmt.setString(9, siObject.getCounterparty());
                pstmt.setString(10, product.getProducttype());
                pstmt.setString(11, serialresult);
                pstmt.setString(12, product.getActioncodeamount());
                pstmt.setString(13, "");
                pstmt.setString(14, "");
                pstmt.setString(15, "");
                pstmt.setInt(16, product.getMaxretrycount());
                pstmt.setString(17, "");
                pstmt.setString(18,siObject.getDrBranchCode());
                System.out.println(siObject.getDrBranchCode());
                pstmt.setString(19,siObject.getSiSourceAcct() );//need to be part of what will be returned by getsi;
                pstmt.setString(20, siObject.getSiCurrency());
                pstmt.setString(21, siObject.getSiCurrency());// this will be changed to the currency accpted by the product
                pstmt.setString(22, siObject.getSiAmount());
                pstmt.setString(23, siObject.getCrBranchCode());
                pstmt.setString(24, siObject.getSiDestAccount());
                pstmt.setString(25, "");// the creditor currency
                pstmt.setString(26, siObject.getSiPriority());
                pstmt.setString(27, "");//CHARGE_WHOM
                pstmt.setString(28, "");
                pstmt.setString(29, "");
                pstmt.setString(30, "");
                pstmt.setString(31, "");
                pstmt.setString(32, "");
                pstmt.setString(33, "");
                pstmt.setString(34, "U");
                pstmt.setString(35, "");



                isUpdate = pstmt.executeUpdate();
                System.out.println("=====sitm_upload_master updated" + isUpdate);

                if (isUpdate != 0 && isUpdate > 0) {
                     System.out.println("=====About to update sitm_upload_instr======");
                    pstmt = con.prepareStatement(INS_INTO_INST_TABLE);
                    pstmt.setString(1, siObject.getDrBranchCode());//The same thing as above.
                    pstmt.setString(2, "SI_UPLOAD");
                    pstmt.setString(3, SourceRef);
                    pstmt.setString(4, siObject.getSiType());//siObject.getSiType() returns the product code chosen by the user
                    pstmt.setString(5, product.getProducttype());//type of product e.g payment,collection,sweep in etc
                    pstmt.setString(6, product.getSitype());//product.getsitype() is the si type e.g one to one or one to many
                    pstmt.setString(7, product.getCalholexcp());
                    pstmt.setString(8, product.getRatetype());
                    if (siObject.getSiFreqExec()==1||siObject.getSiFreqExec()==2) {
                        if(siObject.getSiFreqExec()==1){
                        pstmt.setInt(9, 1);
                        }else{
                            pstmt.setInt(9, 7);
                        }
                        pstmt.setString(10, "");
                        pstmt.setString(11, "");
                    } else if (siObject.getSiFreqExec()==3||siObject.getSiFreqExec()==4||siObject.getSiFreqExec()==5) {
                        pstmt.setString(9, "");
                        if(siObject.getSiFreqExec()==3){
                        pstmt.setInt(10,1 );
                        }else if(siObject.getSiFreqExec()==4){
                        pstmt.setInt(10,3 );
                        }else if(siObject.getSiFreqExec()==5){
                        pstmt.setInt(10,6 );
                        }
                        pstmt.setString(11,"");
                    } else {
                        pstmt.setString(9, String.valueOf(0));
                        pstmt.setString(10, String.valueOf(0));
                        pstmt.setInt(11,1);
                    }

                    pstmt.setDate(12, siObject.getSiFirstExecDate());
                    pstmt.setString(13, "");///get the next execution date
                    pstmt.setDate(14, siObject.getSiFirstExecDate());
                    pstmt.setString(15, "");// as in 13
                    pstmt.setString(16, "");//month end flag
                    pstmt.setString(17, "");
                    pstmt.setString(18, userRef);
                    pstmt.setString(19, "");
                    pstmt.setString(20, "");
                    pstmt.setString(21, "");
                    pstmt.setString(22, "");///book date need to be set as the date the contract was input into the system
                    pstmt.setString(23, serialresult);
                    pstmt.setString(24, siObject.getCounterparty());
                    pstmt.setString(25, "");
                    pstmt.setString(26, "");
                    pstmt.setString(27, "U");
                    pstmt.setString(28, "");

                    isUpdate = pstmt.executeUpdate();
                    System.out.println("====sitm_upload_instruction updated=======");

                    if (isUpdate != 0 && isUpdate > 0) {
                        //Call upload procedure
                        System.out.println("updating log");
                        con.commit();
                        result=true;
                        //con2.commit();
                    }


                }

            } catch (SQLException sql) {
                /*if (bDebugOn)*/ {
                    System.out.println("sql ; " + sql);
                    sql.printStackTrace();
                    try {
                        con.rollback();
                        con2.rollback();
                    } catch (SQLException ssqle) {
                        /*if (bDebugOn)*/ {
                            System.out.println("sql:" + ssqle);
                            ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex);
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (Exception ex) {
                /*if (bDebugOn)*/ {
                    System.out.println("Exception ; " + ex);
                    ex.printStackTrace();
                    try {
                        con.rollback();
                        //con2.rollback();
                    } catch (Exception ex2) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex2);
                            ex2.printStackTrace();
                        }
                    }
                }
            } finally {
                if (con != null || con2 != null) {
                    try {
                        con.close();
                        //con2.close();
                    } catch (SQLException ssqle) {
                        if (bDebugOn) {
                            System.out.println("sql:" + ssqle);
                            ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        if (bDebugOn) {
                            System.out.println("exception: " + ex);
                            ex.printStackTrace();
                        }
                    }
                }
            }

        }

        return result;
    }

    public String executeProcedure()
            throws SQLException {

        procStmt.setString(2, "SYSTEM");
        procStmt.setString(3, "SI_UPLOAD");//TO BE PICKED FROM THEDB
        procStmt.setString(4, siobject.getDrBranchCode());
        procStmt.setString(5, userRef);
        //procStmt.setString(6,null );
        //procStmt.setString(7, null);
        //procStmt.setString(8, null);
        //procStmt.setString(9, null);


        procStmt.execute();

        return procStmt.getString(1);

    }

     protected void prepareExecProcedure(Connection con)
            throws Exception {



        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt = con.prepareCall(l_stmt_string);
        registerOutParameters();

    }

    protected String getProcedureParamString() {

        return buildProcedureStatement(
                PROCEDURE_SI_UPLOAD, NBR_PARAMS);

    }

    protected void registerOutParameters()
            throws SQLException {

        //if (!isMerantJdbcDriver) {
        procStmt.registerOutParameter(1, OracleTypes.VARCHAR);
        procStmt.registerOutParameter(6, OracleTypes.VARCHAR);
        // }
        procStmt.registerOutParameter(7, Types.VARCHAR);
        //procStmt.registerOutParameter (PARAM_IDX_CUST_ID, Types.INTEGER);
        procStmt.registerOutParameter(8, Types.VARCHAR);
        procStmt.registerOutParameter(9, Types.VARCHAR);

    }
  protected HashMap extractOutputValues(String type)throws SQLException{
      HashMap map = null;

      return map;

  }
    protected String extractOutputValues()
            throws SQLException {

        String result = null;

        String status = procStmt.getString(6);
        if(bDebugOn)System.out.println(status);
        String errorCode = procStmt.getString(7);
        if(bDebugOn)System.out.println(errorCode);
        String errorParameter = procStmt.getString(8);
        if(bDebugOn)System.out.println(errorParameter);
        String errorMsg = procStmt.getString(9);
        if(bDebugOn)System.out.println(errorMsg);
        
        org.jdom.Element results = new org.jdom.Element("RESULT");
       // Element transdetaildr = new Element("Transdetaildr");
                    Element statuse = new Element("STATUS");
                    statuse.setText(status);
                    Element errorcodee = new Element("errorCode");
                    errorcodee.setText(errorCode);
                    Element errorParametere = new Element("errorParameter");
                    errorParametere.setText(errorParameter);
                    Element errorMsge = new Element("errorMsg");
                    errorMsge.setText(errorMsg);
                    results.addContent(statuse);
                    results.addContent(errorcodee);
                    results.addContent(errorParametere);
                    results.addContent(errorMsge);
                    org.jdom.Document doc = new org.jdom.Document(results);
            XMLOutputter outputter = new XMLOutputter();
          outputter.setFormat(Format.getPrettyFormat());
            String xmlText = outputter.outputString(doc);
        /*try{
                    DocumentBuilderFactory docBuilder = DocumentBuilderFactory.newInstance();
                    DocumentBuilder  Builder = docBuilder.newDocumentBuilder();
                    Document doc = Builder.newDocument();
                    Element  el  = doc.createElement("RESULT");
                    Element statuse = doc.createElement("STATUS");
                    el.appendChild(doc.createTextNode(status));
                    Element errorcodee = doc.createElement("ERRORCODE");
                    el.appendChild(doc.createTextNode(errorCode));
                     Element errorParametere = doc.createElement("ERRORPARAMETER");
                    el.appendChild(doc.createTextNode(errorParameter));
                    Element errorMsge = doc.createElement("ERRORMSG");
                    el.appendChild(doc.createTextNode(errorMsg));

                    XMLUtilty.getDocumentAsXml(doc);
        }catch(TransformerConfigurationException tr){

        }catch(TransformerException ex){

        }catch(ParserConfigurationException ps){

        }*/
            result=status;
         if(bDebugOn)System.out.println(result);
        return result;
    }

    public String Exchange() {
        con = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        String l_txn_proc_rc = "";

        try {

            prepareExecProcedure(con);
            l_txn_proc_rc = executeProcedure();
            String result = extractOutputValues();
            System.out.println(l_txn_proc_rc);

            //if (SUCCESS.equals(l_txn_proc_rc)) {
            //setTransactionError (errorCode);
            //setBeanValues ();
            return result;
            //} else {

            //System.out.println("error in adding the si");
            // }

            //buildResponseMessage ();


        } catch (SQLException e) {
            e.printStackTrace();
            if (e.getErrorCode() == 20000) {
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            /*try {
            rsetSiInq.close ();
            } catch (Exception e) {
            }*/
            try {
                procStmt.close();
            } catch (Exception e1) {
            }
            if (con != null) {
                   try {
                       con.close();
                       //con2.close();
                   } catch (SQLException ssqle) {
                       if (bDebugOn) {
                           System.out.println("sql:" + ssqle);
                           ssqle.printStackTrace();
                       }
                   } catch (Exception ex) {
                       if (bDebugOn) {
                           System.out.println("exception: " + ex);
                           ex.printStackTrace();
                       }
                   }
            }
        }

        return null;

    }
}
