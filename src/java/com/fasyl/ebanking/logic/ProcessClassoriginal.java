
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
* This class must be extended by by any non procedure executing logic to
*be able to follow the template and at the same time remove the boredom
* of implementing unused method
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import java.util.Calendar;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class ProcessClassoriginal extends Processes {

    // protected CallableStatement procStmt;
    private static final int   NBR_PARAMS    = 1;
    public static final String SUCCESS       = "false";
    protected String           insertauthqry =
        "insert into auth_request_log values(sysdate,?,?,auth_seq.nextval,?,?,?,?,?,?,?,?,?,?)";
    private String     custno;
    private String     customerName;
    private Connection dbConnection;
    private String     returnResult;
    public String      tokenfailureres;

    public ProcessClassoriginal() {}

    protected boolean addRequest4Auth(DataofInstance instData, String userid, String actiondesc, String action,
                                      String tablename, String tablecolumn, String datavalue) {
        Connection        con       = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement pstmt     = null;
        int               result    = 0;
        boolean           insResult = false;

        try {
            pstmt = con.prepareStatement(this.insertauthqry);

            // System.out.println(this.email + this.userid + this.olduser);
            pstmt.setString(1, userid.toUpperCase().trim());
            pstmt.setString(2, instData.sessionId);
            pstmt.setString(3, "");
            pstmt.setString(4, "W");
            pstmt.setString(5, instData.remoteAddress);
            pstmt.setString(6, actiondesc);
            pstmt.setString(7, instData.request);
            pstmt.setString(8, action);
            pstmt.setString(9, tablename);
            pstmt.setString(10, tablecolumn);
            pstmt.setString(11, datavalue);
            pstmt.setString(12, null);
            result = pstmt.executeUpdate();

            if (result <= 0) {
                insResult = false;
            } else {
                insResult = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
            try{
               if (con!=null){
                  con.close(); 
               } 
                if (pstmt!=null){
                  pstmt.close(); 
               } 
            }catch(Exception ex){
                ex.getMessage();
            }
        }

        return insResult;
    }

    public HashMap processRequest(DataofInstance instData) {
        HashMap hashMap = null;

        return hashMap;
    }

    @Override
    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement("", 0);
    }

    public void prepareExecProcedure(Connection dbConnections) throws Exception {}

    public String executeProcedure() throws SQLException {
        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {}

    public String BulletinCount() {
        Connection        connects  = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement l_stmt    = null;
        ResultSet         l_rs      = null;
        int               l_nbr_msg = 0,
                          l_count   = 0;

        try {
            l_stmt = connects.prepareStatement(
                "select count(8) from bulletin_messages where to_date(Expire_date,'dd-mon-yyyy')>=to_date(sysdate,'dd-mon-yyyy') and iscustspecific = 'N' and to_date(active_date,'dd-mon-yyyy') <= to_date(sysdate,'dd-mon-yyyy')");

            // l_stmt.setString(1, userid);
            l_rs = l_stmt.executeQuery();

            if (l_rs.next()) {
                l_nbr_msg = l_rs.getInt(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (connects != null) {
                    connects.close();
                }

                if (l_rs != null) {
                    l_rs.close();
                }

                if (l_stmt != null) {
                    l_stmt.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return String.valueOf(l_nbr_msg);
    }

    public String getUserTypes() {
        String            list = null;
        Connection        con  = com.fasyl.ebanking.db.DataBase.getConnection();
        CallableStatement stmt = null;

        try {

            // stmt.registerOutParameter(1, OracleTypes.CLOB);
            stmt = con.prepareCall("{ ? = call fn_GET_USER_TYPE()}");
            stmt.registerOutParameter(1, OracleTypes.CLOB);
            stmt.execute();
            list = stmt.getString(1);

            if (bDebugOn) {
                System.out.println(list);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
            try{
               if (con!=null){
                  con.close(); 
               } 
                if (stmt!=null){
                  stmt.close(); 
               } 
            }catch(Exception ex){
                ex.getMessage();
            }
        }

        return list;
    }

    public String getDistinctUserTypes() {
        String            list = null;
        Connection        con  = com.fasyl.ebanking.db.DataBase.getConnection();
        CallableStatement stmt = null;

        try {

            // stmt.registerOutParameter(1, OracleTypes.CLOB);
            stmt = con.prepareCall("{ ? = call FN_GET_DISTINCT_USERTYPE()}");
            stmt.registerOutParameter(1, OracleTypes.CLOB);
            stmt.execute();
            list = this.ManageClob(stmt.getClob(1));

            if (bDebugOn) {
                System.out.println(list);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }    //
        finally{
            try{
               if (con!=null){
                  con.close(); 
               } 
                if (stmt!=null){
                  stmt.close(); 
               } 
            }catch(Exception ex){
                ex.getMessage();
            }
        }

        return list;
    }

    public Element convertToXml(Element parent, String elemname, ResultSet rs) throws Exception {
        if (bDebugOn) {
            System.out.println("========= inside convertToXml ==========");
        }

        ResultSetMetaData metaData = rs.getMetaData();
        Element           elem     = null;

        if (bDebugOn) {
            System.out.println("==== This is metadata ==== ");
        }

        int numofcolumns = metaData.getColumnCount();

        if (bDebugOn) {
            System.out.println("==== This is metadata count ==== " + numofcolumns);
        }

        Element xmlName = null;

        if (!rs.isBeforeFirst()) {    // this method is not implemented for all jdbc
            if (bDebugOn) {
                System.out.println("==== rs is null=====");
            }

            return null;
        }

        while (rs.next()) {
            elem = new Element(elemname);

            if (bDebugOn) {
                System.out.println(" === inside converttoxml resultset" + numofcolumns);
            }

            for (int i = 1; i <= numofcolumns; i++) {
                if (bDebugOn) {
                    System.out.println(" === inside converttoxml inner" + metaData.getColumnName(i));
                }

                xmlName = new Element(metaData.getColumnName(i));
                xmlName.setText(rs.getString(metaData.getColumnName(i)));

                // if (bDebugOn)System.out.println(" === inside converttoxml value" +rs.getString(metaData.getColumnName(i)));
                elem.addContent(xmlName);
            }

            parent.addContent(elem);
        }

        if (bDebugOn) {
            System.out.println(" === This is parent in convert to xml ==== " + parent);
        }

        return parent;
    }

    protected HashMap generateXML(Element parent, String xmlText) {
        HashMap           hashMap   = null;
        org.jdom.Document doc       = new org.jdom.Document(parent);
        XMLOutputter      outputter = new XMLOutputter();

        outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
        xmlText = outputter.outputString(doc);
        System.out.println(xmlText);
        hashMap = new HashMap();
        hashMap.put(RESULT_MAP_KEY, xmlText);

        return hashMap;
    }

    public boolean validatePin(String userid, String inputpin) {
        boolean           result          = false;
        PreparedStatement pstmt           = null;
        ResultSet         rs              = null;
        String            pin             = null;
        String            pin_expiry_time = null;
        Connection        con             = com.fasyl.ebanking.db.DataBase.getConnection();
        String            tokenfailcount = null;
        int               failcount = 0;
        String    type      = "text/html";
        String    body      = null;
        String    header    = "Consecutive failiure attempt of token";
        String    toemail   = "";
        String    fromemail = "";

        try {
            pstmt = con.prepareStatement("select pin_to_expire,user_pin,TOKEN_FAILIURE_COUNT from tb_user_access where userid=?");
            pstmt.setString(1, userid);
            rs = pstmt.executeQuery();
            toemail = (String) LoadEmails.getDataValuenew("SC");
            fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
            if (rs.next()) {
                pin             = rs.getString(2);
                pin_expiry_time = rs.getString(1);
                tokenfailcount  = rs.getString(3);
            }
                if (tokenfailcount==null || tokenfailcount.equals("")){
                    failcount = 0; 
                }else{
                failcount = Integer.parseInt(tokenfailcount);
            }
                
                System.out.println("========= This is pin =================="+Encryption.decrypt(pin)  + "inputpin===== "+inputpin);
                if(failcount<3 ){
            if (!(pin == null) && Encryption.decrypt(pin).equals(inputpin)) {
                
                Calendar calendar1 = Calendar.getInstance();
                long     now       = calendar1.getTimeInMillis();
                long     timediff  = now - Long.parseLong(pin_expiry_time);
             
                    
                
                if (timediff / (1000 * 60 * 60) < 1) {
                    result = true;
                } else {
                    result = false;
                    tokenfailureres = " You are using invalid token";
            pstmt = con.prepareStatement("update sms_user_access set TOKEN_FAILIURE_COUNT=? where cod_usr_id=? ");
            pstmt.setInt(1, failcount+1);
                   
            pstmt.setString(2, userid);
            pstmt.executeUpdate();
                }
            } else {
                result = false;
                tokenfailureres="You are using invalid tokens";
                pstmt = con.prepareStatement("update sms_user_access set TOKEN_FAILIURE_COUNT=? where cod_usr_id=? ");
            pstmt.setInt(1, failcount+1);
                   
            pstmt.setString(2, userid);
            pstmt.executeUpdate();
            }
            }else{
              result = false; 
             this.tokenfailureres ="You have exceeded the number consecdutive failed token usage attempt ";
            pstmt = con.prepareStatement("update sms_users_access set TOKEN_FAILIURE_COUNT=? where cod_usr_id=? ");
            pstmt.setInt(1, failcount+1);
                   
            pstmt.setString(2, userid);
            pstmt.executeUpdate();
             body = "Dear Sir/Ma <br>  The the account with userid " + userid
                           + "has tried to log into the application more than the number of required consecutive login with wrong token. <br> <br> Yours Faithfully <br> Internet Banking Service";

                    sendmail send = new sendmail();

                    /* boolean sent = true; */
                    boolean sent = send.sendMessage(header, body, null, type, fromemail, toemail);
              
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        }finally{
            try{
               if (con!=null){
                  con.close(); 
               } 
                if (pstmt!=null){
                  pstmt.close(); 
               } 
            }catch(Exception ex){
                ex.getMessage();
            }
        }

        return result;
    }

    public String ManageClob(java.sql.Clob datas) {
        String result = null;

        try {
            if(datas==null){
             result="<ROWSET></ROWSET>";  
            }else{
            result = datas.getSubString(1, (int) datas.length());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return result;
    }

    public HashMap responseMsg(String results) {
        HashMap map     = null;
        Element parent  = new Element("ROWSET");
        String  xmlText = null;
        Element row     = new Element("ROW");
        Element success = new Element("SUCCESS");

        success.setText((results));
        row.addContent(success);
        parent.addContent(row);
        map = this.generateXML(parent, xmlText);

        return map;
    }
    
    synchronized public HashMap ServiceFT(String requestid, String loginuserid, String custno, String ftdebitacct, String DestAcctNo,
                         String amt) {
        int               result    = 0;
        Connection        connect   = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        CallableStatement statement = null;
        HashMap map = new HashMap();

        try {
            statement = connect.prepareCall("{?=call fn_ibfundtransfer(?,?,?,?,?,?,?,?,?,?,?,?)}");
            statement.registerOutParameter(1, OracleTypes.INTEGER);
            statement.registerOutParameter(11, OracleTypes.CLOB);
            statement.registerOutParameter(12, OracleTypes.VARCHAR);
            statement.registerOutParameter(13, OracleTypes.CLOB);
            statement.setString(2, requestid);
            statement.setString(3, loginuserid);
            statement.setString(4, custno);
            statement.setString(5, Utility.getsqlCurrentDate2());
            statement.setString(6, ftdebitacct);
            statement.setString(7, DestAcctNo);
            statement.setString(8, amt);
            statement.setString(9, Utility.getsqlCurrentDate2());    // Utility.FormatCurrentDate().toString()
            System.out.println(Utility.FormatCurrentDate().toString() + " ==== hmm ==== "
                               + Utility.getsqlCurrentDate());
            statement.setString(10, "Internal Transfer");

//      
            statement.execute();
//            System.out.println("This is the error message " + statement.getString(11) + " hmm "
//                               + statement.getString(13));

           // int results = statement.getInt(1);
            map.put("result", statement.getInt(1));
            map.put("message", statement.getInt(11));
            map.put("errorcode", statement.getInt(13));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        } finally {
            if ((statement != null) || (connect != null)) {
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }

                if (connect != null) {
                    try {
                        connect.close();
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }
            }
        }

        return map;
    }
    
    
    
    synchronized public HashMap ServiceBulkFT(String requestid, String branch, String benacct, String ftdebitacc, String custId,
                         String amt,String ccy) {
        int               result    = 0;
        Connection        connect   = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
        CallableStatement statement = null;
        HashMap map = new HashMap();

        try {
         statement = connect.prepareCall("{?=call bulk_ft.fn_manage_ft(?,?,?,?,?,?,?,?,?,?)}");
                statement.registerOutParameter(1, OracleTypes.INTEGER);
                statement.registerOutParameter(10, OracleTypes.VARCHAR);
                statement.registerOutParameter(11, OracleTypes.VARCHAR);
                statement.setString(2, "DE_UPLOAD");
                statement.setString(3, requestid);
                statement.setString(4, branch);
                statement.setString(5, benacct);
                statement.setString(6, ftdebitacc);
                statement.setString(7, custId);
                statement.setString(8, ccy);
                statement.setString(9, amt);


//      
            statement.execute();
            

            map.put("result", statement.getInt(1));
            map.put("message", statement.getInt(10));
            map.put("errorcode", statement.getInt(11));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        } finally {
            if ((statement != null) || (connect != null)) {
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }

                if (connect != null) {
                    try {
                        connect.close();
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }
            }
        }

        return map;
    }
    
}


//~ Formatted by Jindent --- http://www.jindent.com
