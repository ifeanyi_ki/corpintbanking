/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;
import com.fasyl.ebanking.util.Utility;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.jdom.Element;

/**
 *
 * @author baby
 */
public class ViewMail extends ProcessClass {


 private HashMap hashMap = null;

        	/**
	This date formatter is used to parse string date and to format a date.
	**/
	public SimpleDateFormat
		dateFormatter
	;
    private Element parent;
    private String xmlText = null;
    private String loginUserId;
    private DataofInstance instancedata;
    private boolean customermail=false;
    
    	/**
	To check whether the request which has been generated is
	from the draft box or a fresh message.
	**/
	private String
		flg_box
	;
	Connection connect;
        /**
	This date formatter is used to build as of date display string.
	**/
        public SimpleDateFormat
		asOfDateFormatter
	;
   private static final String
		SEL_MAIMESSAGE_STR				=
			" select * from mail_messages where msg_id = ?"
	;
	private static final String
		UPD_MAILMSGUSERMAP_STR =
			" update msg_user_map_mail "
		+	" set read_date = ?"
		+	" where msg_id = ? "
		+	" and user_id = ? "
		+	" and box_flag in ('T', 'C')"
		+	" and read_date is null"
	;
	//--------------------------------------------------------------------------

            /**
    This string constant stores the statement that checks whether the given user
    has access to the message id
    **/
    private static final String
    	SEL_MSG_USER_MAP				=
        	"select 1 from msg_user_map_mail where user_id = ? and msg_id = ? ";
	private static final String
		INBOX						= "I"
	;
	private static final String
		DRAFT						= "D"
	;
	private static final String
		SENT						= "S"
	;
	private static final String
		OPERATION_TYPE_FORWARD		= "FWD"
	;
	private static final String
		OPERATION_TYPE_REPLY 		= "RPY"
	;
	private static final String
		OPERATION_TYPE_COMPOSE 		= "COM"
	;
	//--------------------------------------------------------------------------
	/**
	This constant points to the dataname used to retrieve the CSS names from the
	datamap.
	**/
	private static final String
		DATANAME_MAIL_SUBJECT			= "DATANAME_MAIL_SUBJECT"
	;
	/**
	This constant points to the dataname used to retrieve the CSS names from the
	datamap.
	**/
	private static final String
		DATANAME_MAIL_ID			= "DATANAME_MAIL_ID"
	;
	/**
	This string constant denotes the HTTP request field name for the messageid
	**/
	private static final String
		QPARAM_NAME_MESSAGEID		= "fldIdMsg"
	;
	/**
	This string constant denotes the HTTP request field name for the Flag to
	see if the request for message was made from drafts or inbox or sent items.
	**/
	private static final String
		QPARAM_NAME_FLAGBOX			= "fldFlgBox"
	;
	/**
	This string constant denotes the HTTP request field name for the request
	type.
	**/
	private static final String
		QPARAM_NAME_REQTYPE			= "fldReqType"
	;
	//--------------------------------------------------------------------------
	private PreparedStatement
		updStmt
	;
	/**
	String to store value of Message Id.
	**/
	private long
		messageId
	;
	/**
	String to store value of New Message Id .
	**/
	private long
		newMessageId
	;
	/**
	String to store value of Flag denoting either Inbox or Drafts or Sent Items.
	**/
	private String
		flgBox
	;
	/**
	String to store value of request type.
	**/
	private String
		reqType
	;
	//--------------------------------------------------------------------------
        public String formattedSystemDate () {

		return formatDate (new Date ());

	}
        /**
	This method formats a given date using the site standard date format. Input
	date can be java.util.Date or any date derived from it (like java.sql.Date).

	@param	p_date	Date to be formatted

	@return	String date formatted to standard format
	**/
	public String formatDate (
		Date		p_date
	,	DateFormat	p_dateFormat
	) {

		if (p_date == null) {
			return null;
		}

		return (p_dateFormat==null)?
					asOfDateFormatter.format (p_date)
				:	p_dateFormat.format (p_date);

	}
	/**
	This constructor is used to initialise the JFSequence Generator Class.
	**/
	public ViewMail () {

		super();

		try{
			//JFSequenceGenerator.initialize();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}
	//--------------------------------------------------------------------------
	/**
	This method opens data statements for SQL queries.

	@throws		Exception	If any JDBC call fails.
	**/
	protected void openAdminStatements ()
	throws Exception {
		updStmt				= connect.prepareStatement (UPD_MAILMSGUSERMAP_STR);

	}
	//--------------------------------------------------------------------------
	/**
	This method closes statement opened to select Message Details.
	**/
	protected void closeAdminStatements ()
	{
		try {
			updStmt.close ();
		} catch (Exception e) {
		}

		updStmt				= null;

	}
	//--------------------------------------------------------------------------
	/**
	This method initializes the variables required for a transaction.
	**/
	protected void initializeVar ()
	{
		messageId	=	0;
		flgBox		=	"";
		reqType		=	"";
                instancedata    =       null;
	}
	//--------------------------------------------------------------------------
	/**
	
	**/
        @Override
	public HashMap processRequest (
		DataofInstance instData
	)  {
                this.initializeVar();
		String l_result = null;
		Element				l_elem	= null;

		

		try {
                    this.setInstanceVar(instData);
                            if (reqType.equals (OPERATION_TYPE_COMPOSE)) {
				getNewMessageId ();
                                Element msgid = new Element("MSG_ID");
                                msgid.setText(String.valueOf(newMessageId));
                                parent.addContent(msgid);
				

			} else if ((reqType.equals (OPERATION_TYPE_REPLY))
						|| (reqType.equals (OPERATION_TYPE_FORWARD))) {
				getNewMessageId ();
				putMessageInXml (newMessageId);
				if (l_elem != null) {
                                    //need  to check whethere mssageid is alredy in the xmle.g xmlParser.getElement("messageid",,	null)	0)


			                  Element lelem = new Element("L_ELEM");
                                          lelem.setText(String.valueOf(newMessageId));
                                          parent.addContent(lelem);

					
				} else {
					  Element msgid = new Element("MSG_ID");
                                          msgid.setText(String.valueOf(newMessageId));
				}

			} else {
            	if (reqType != null && !reqType.equals("D") ) {
                          if(bDebugOn)System.out.println("about to call checkPrivilegeOnMessage ");
	            	if ( (l_result = checkPrivilegeOnMessage(messageId)) == null ) {
		                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                                 return instData.result;
	                }
                }
				putMessageInXml (messageId);
				updateReadDate ();
			}
		} catch (Exception e) {
			e.printStackTrace ();
			//need to retur some exception to User
		} finally {
                    try {
                        if (connect != null) {
                            connect.close();
                        }
                    }catch(Exception exc){}
                }
                  Element reqtype = new Element("reqType");
                  reqtype.setText(reqType);
                  Element flgbox = new Element("flgbox");
                  flgbox.setText(flgBox);
                  Element date = new Element("date");
                  date.setText(formattedSystemDate ());
                  Element userid = new Element("userid");
                  userid.setText(loginUserId);
                  parent.addContent(reqtype);
                  parent.addContent(flgbox);
                  parent.addContent(date);
                  parent.addContent(userid);
                  if (instData.result == null){
                      instData.result = generateXML(parent, this.xmlText);
                      //if(bDebugOn)System.out.println(instData.result);
                  }
     

		return instData.result;
	}
	//--------------------------------------------------------------------------
	/**
	This method parses the query string given by the front end and extracts
	various fields from it.

	@param	p_request_message	HTTP query string sent by the front end.

	@return	null	If parsing query string and extracting fields from it is
					succesful, error result otherwise.
	**/
	protected void setInstanceVar  (
		DataofInstance inst_data
	)throws Exception {
                 ReadRequestAsStream decodes = new ReadRequestAsStream();
                 this.flgBox=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "flg_box", 0, true, null),"UTF-8");
		this.loginUserId=inst_data.userId;
                this.messageId=Long.parseLong(decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "messageId", 0, true, null),"UTF-8"));
                parent = new Element("ROWSET");
                this.dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
                asOfDateFormatter=new SimpleDateFormat("dd/MM/yyyy");
                this.reqType=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "reqType", 0, true, null),"UTF-8");
	        instancedata = inst_data;
        }
	//--------------------------------------------------------------------------

        /**
	This method formats a given date using the site standard date format. Input
	date can be java.util.Date or any date derived from it (like java.sql.Date).

	@param	p_date	Date to be formatted

	@return	String date formatted to standard format
	**/
	public String formatDate (
		Date	p_date
	) {

		return formatDate (p_date, dateFormatter);
	}
	private void putMessageInXml (
		long	p_display_message_id
	) throws Exception {
            //connect =com.fasyl.ebanking.db.DataBase.getConnection();

		String 				fromId 		= 	null,
							l_touser	=	null,
							l_ccuser	=	null,
							l_userList	=	null;
        PreparedStatement	l_stmt		=	null;
		ResultSet 			l_rs		=	null;
        StringBuffer		l_buf 		=	null;
        boolean				l_pass		= 	false;
		StringTokeNizers		l_st		= 	null		;

		try {

        	l_buf = new StringBuffer (SEL_MAIMESSAGE_STR);

        	l_stmt = connect.prepareStatement (l_buf.toString());


        	l_stmt.setBigDecimal (1, BigDecimal.valueOf (messageId));
                if (messageId!=0){
                         Element msgid = new Element("MSG_ID");
                         msgid.setText(String.valueOf(messageId));
                         parent.addContent(msgid);
                }

        	l_rs	=	l_stmt.executeQuery ();

			if (l_rs.next ()) {
				if (flgBox.equals(INBOX)) {
                                       if(bDebugOn)System.out.println("<<<<<<Inside getting inbo mail>>>>>>>>>>>");
					l_touser	= l_rs.getString("list_to_user");
					l_ccuser	= l_rs.getString("cc_user_list");
                                         
                                         
					l_userList	= l_touser+";"+l_ccuser;
					l_st 	= new StringTokeNizers(l_userList,";");
					while (l_st.hasMoreTokens()) {
						if (loginUserId.equals (l_st.nextToken())) {
							l_pass	= true;
                                                        System.out.println("==This is the pass"+l_pass);
							break;
						}
					}
                                       if(instancedata.customerType.equals("ADMIN")&& customermail){
                                           l_pass = true;
                                          }
				} else if (flgBox.equals(SENT) || flgBox.equals(DRAFT)) {
					if (l_rs.getString("from_id").equals(loginUserId)) {
						l_pass	= true;
					}
				}

				if (l_pass) {
                                           Element touserlist = new Element("TO_USER_LIST");
                                         touserlist.setText(l_rs.getString ("list_to_user"));
                                         Element ccuserlist = new Element("CC_USER_LIST");
                                         ccuserlist.setText(l_rs.getString ("cc_user_list"));
                                         if (touserlist!=null) parent.addContent(touserlist);

                                          if (ccuserlist!=null)parent.addContent(ccuserlist);
					  l_userList	= l_touser+";"+l_ccuser;
                                          Element from_id = new Element("FROM_ID");
                                          from_id.setText(l_rs.getString("from_id"));
                                          Element from_id_name = new Element("FROM_ID_NAME");
                                          from_id_name.setText(this.getUsernames(l_rs.getString("from_id")));
                                          Element subject = new Element("TEXT_SUBJECT");
                                          subject.setText(l_rs.getString("text_subject"));
                                          Element tousername = new Element("LIST_TO_USERNAME");

                                          tousername.setText(l_rs.getString("LIST_TO_USERNAME"));
                
                                          Element ccusername = new Element("CC_USERNAME_LIST");

                                          ccusername.setText(l_rs.getString("CC_USERNAME_LIST"));
                                          parent.addContent(subject);
                                          System.out.println(l_rs.getString("text_subject")+" This subject");
                                          parent.addContent(from_id);
                                          parent.addContent(from_id_name);
                                          Element textmessage = new Element("MESSAGE_TEXT");
                                          textmessage.setText(l_rs.getString ("msg_text"));// i can store text message as email
                                          parent.addContent(textmessage);
                                          parent.addContent(ccusername);
                                          parent.addContent(tousername);
			   		

					if(loginUserId.equalsIgnoreCase(fromId)){
                                                 Element flgval = new Element("flgval");
                                                 flgval.setText("Y");
                                                 parent.addContent(flgval);
						
					} else {
                                                 Element flgval = new Element("flgval");
                                                 flgval.setText("N");
                                                 parent.addContent(flgval);
						
					}
				}
			}
		} finally {
			try {
				l_stmt.close ();
			} catch (Exception e) {
			}
			try {
				l_rs.close ();
			} catch (Exception e) {
			}
			l_rs	= null;
   			l_stmt	= null;
		}
	}

        protected String checkPrivilegeOnMessage (
    	long p_messageId
    ) throws Exception {
          //connect =com.fasyl.ebanking.db.DataBase.getConnection();

    	PreparedStatement 	l_stmt 	= null;
        ResultSet 			l_rs 	= null;

        try
        {       if (instancedata.customerType.equals("ADMIN")){
                     l_stmt = connect.prepareStatement("SELECT 1 from mail_messages where list_to_user='IBADMIN' and  msg_status='S' AND msg_id=?");
	             //l_stmt.setString (1, loginUserId);
	             l_stmt.setBigDecimal (1, BigDecimal.valueOf (p_messageId));

	             l_rs = l_stmt.executeQuery();
                       if (l_rs.next()) {
                          if(bDebugOn)System.out.println("checkPrivilegeOnMessage is not returning null for messages sent to Administrator "+ l_rs.getString(1));
	        	 customermail=true;
                          return l_rs.getString(1);
	        }
            
                  }
                
	        l_stmt = connect.prepareStatement(SEL_MSG_USER_MAP);
	        l_stmt.setString (1, loginUserId);
	        l_stmt.setBigDecimal (2, BigDecimal.valueOf (p_messageId));

	        l_rs = l_stmt.executeQuery();

	        if (l_rs.next()) {
                          if(bDebugOn)System.out.println("checkPrivilegeOnMessage is not returning null "+ l_rs.getString(1));
	        	return l_rs.getString(1);
	        }
                if(bDebugOn)System.out.println("checkPrivilegeOnMessage is  returning null");
	        return null;//this will be looked into later to generate error to tell user that they can not found mapping message


		} finally {
        	try
        	{
        		l_rs.close();
        	} catch (Exception e){
        	}
            try
            {
            	l_stmt.close();
            } catch (Exception e){
            }
        }
    }
	//-------------------------------------------------------------------------
	private void getNewMessageId ()
	throws Exception {

		newMessageId	= System.currentTimeMillis();	//look for a means of genrating sequence number
	}
	//-------------------------------------------------------------------------
	private void updateReadDate ()
	 {
             Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
             PreparedStatement updStmts = null;
            try{
                 updStmts				= con.prepareStatement (UPD_MAILMSGUSERMAP_STR);
		updStmts.setTimestamp (1, new java.sql.Timestamp (new java.util.Date ().getTime ()));
		updStmts.setBigDecimal (2, BigDecimal.valueOf (messageId));
		updStmts.setString (3, loginUserId);
		updStmts.executeUpdate ();
            }catch(Exception ex){
                ex.getMessage();
            }finally{
                try{
                   if (con!=null){
                      con.close(); 
                   }
                   
                     if (updStmts!=null){
                      updStmts.close(); 
                   } 
                }catch(Exception ex){
                    ex.getMessage();
                }
            }

	}

//        public static void main(String [] args){
//            System.out.println(new java.sql.Timestamp (new java.util.Date ().getTime ()));
//        }
        
        
        /**This method is used to get a list of corresponding usernames
         * to a userid in the cc and to filed.**/
        private String getUsernames(String userid){
            Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            String result = null;
            try{
              pstmt = con.prepareStatement("select cod_usr_name from sm_user_access where cod_usr_id=?");  
              pstmt.setString(1, userid);
              rs = pstmt.executeQuery();
              if(rs.next()){
                 result = rs.getString("cod_usr_name"); 
              }
            }catch(Exception ex){
                ex.printStackTrace();
            }finally{
                try{
                    if (con!=null){
                       con.close(); 
                    }
                    if (rs!=null){
                       rs.close(); 
                    }
                    if (pstmt!=null){
                       pstmt.close(); 
                    }
                    
                }catch(Exception ex){
                    
                }
            }
            return result;
        }
}
