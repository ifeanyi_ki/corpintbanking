
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.AuditLogHandler;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author baby
 */
public class ManageFT extends ProcessClass {
    private Element parent  = null;
    private String  xmlText = null;
    private String  DestAcctNo;
    private String  amt;
    private String  benacct;
    private String  ccy;
    private String  ftdebitacc;
    private String  ftlimt;
    private String  token;
    
    
    // 20141218 narration patch
    private String narration;
     private static final Logger logger = Logger.getLogger(ManageFT.class.getName());
 
    
     // 20150112  report
     Connection con;
    PreparedStatement stmt;
     
    //private PreparedStatement pstmt = null;

    public ManageFT() {}
    
    

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();
        System.out.println("Inside setInstanceVar method");
        ftlimt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ftlimt", 0,
                true, null), "UTF-8");
        ftdebitacc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");
        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "DestAcctNo", 0, true, null), "UTF-8");
        benacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benacct",
                0, true, null), "UTF-8");
        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");
        amt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0,
                true, null), "UTF-8");
        
        
         // 20141218 narration patch
        narration = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "narration", 0,
                true, null), "UTF-8");
        
      
         logger.info(" ManageFT Params: " + " ftdebitacc: " + ftdebitacc + " DestAcctNo: " + DestAcctNo + " amt: " + amt + " narration: " + narration);
        
        
        parent = new Element("ROWSET");
    }

    protected void initializeVar() {
        System.out.println("Inside initializeVar method");
        ftlimt     = null;
        ftdebitacc = null;
        DestAcctNo = null;
        benacct    = null;
        ccy        = null;
        amt        = null;
        token      = null;        
        narration = null;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        boolean results=true;
        try {
            System.out.println("Inside processRequest method - about to set");
            setInstanceVar(instData);
           
            results = validatePin(instData.userId, token);
            
            if(results){
                  if (instData.taskId.equals("3321")||instData.taskId.equals("3331")){
                      instData.result = verifyRequest();
                      return instData.result;
                  }
               
              HashMap map = ServiceFT(instData.requestId, instData.userId, instData.custId, ftdebitacc, DestAcctNo,
                                           amt);  
            System.out.println(map.get("result")+ " This is the return result");
            if ((Integer)map.get("result") == 0) {
                
                String success=(String) LoadEBMessages.getMessageValue(FUND_TRANSFER_SUCESS_MESSAGE).getMessgaedesc();
                instData.result = responseMsg(success);
                
                logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, success);
                
            } else {
                
                String  failiure =(String)map.get("message"); //statement.getString(11);
                
                   
                if (failiure == null||failiure == "<ROWSET></ROWSET>") {
                    failiure = "Error during fund transfer";
                    updateLogFailedFt(instData.requestId, failiure);
                    
                }
                instData.processStatus = FAIL_PROCESS_STATUS;
                instData.errormessage  = failiure;
                instData.errorCode     = (String)map.get("errorcode");
                AuditLogHandler.updateErrorLog(instData);

                instData.result = responseMsg(failiure);
                logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, failiure);

            }
            }else{
                 System.out.println("i am here "+tokenfailiuercode);
//                instData.taskId="33210";
//                instData.result = responseMsg(tokenfailureres);
                instData.result = Utility.processError(instData, tokenfailiuercode);
            }
            
            if (instData.taskId.equals("33210") || instData.taskId.equals("33310"))
                clearToken(instData.userId);
        } catch (Exception ex) {
            clearToken(instData.userId);
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        }
//        finally {
//            if ((statement != null) || (connect != null)) {
//                if (statement != null) {
//                    try {
//                        statement.close();
//                    } catch (Exception ex) {
//                        ex.printStackTrace(System.out);
//                    }
//                }
//
//                if (connect != null) {
//                    try {
//                        connect.close();
//                    } catch (Exception ex) {
//                        ex.printStackTrace(System.out);
//                    }
//                }
//            }
//        }

        return instData.result;
    }
    
    HashMap verifyRequest(){
        
         //parent  = new Element("ROWSET");
         Element row     = new Element("ROW");
         
         Element destacctno = new Element("DESTACCTNO");
         destacctno.setText(this.DestAcctNo);
         row.addContent(destacctno);
          
         Element amts = new Element("AMOUNT");
         amts.setText(this.amt);
         row.addContent(amts);
         
         Element ccys = new Element("CCY");
         ccys.setText(this.ccy);
         row.addContent(ccys);
         
         Element debitacc = new Element("DRACCT");
         debitacc.setText(this.ftdebitacc);
         row.addContent(debitacc);
                 
         Element tokens = new Element("ACTION");
         tokens.setText(this.token);
         row.addContent(tokens);
         
         
          // 20141218 narration patch
          Element narrations = new Element("NARRATION");
         narrations.setText(this.narration);
         row.addContent(narrations);
         
         
         parent.addContent(row);
         
       HashMap result = generateXML(parent, this.xmlText);
        return result;
    }
    
    
    
    
    public String logSuccessfulFt(String requestId, String userId, String custId, String ftdebitacc, String DestAcctNo, String amt, String failiure) {
        String successfulFtResult = null;

        System.out.println("About to log successful FT >>>>>> ");
        return successfulFtResult;
    }

    public String logFtTransactions(String requestId, String userName, String userId, String custId, String ftdebitacc, String DestAcctNo, String amt, String failiure) {
        String query = "insert into fundtransfer_log (Request_id,Account_name,User_Id ,Customer_Id ,Ft_Debit_Acc ,Dest_Acct_No ,Amount ,Message, Transaction_Time,ID)values(?,?,?,?,?,?,?,?,?,ft_sequence.nextval)";
        String failedFtResult = null;
        System.out.println("About to log successful FT >>>>>> " + requestId);
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            stmt = con.prepareStatement(query);

            stmt.setString(1, requestId);
            stmt.setString(2, userName);
            stmt.setString(3, userId);
            stmt.setString(4, custId);
            stmt.setString(5, ftdebitacc);
            stmt.setString(6, DestAcctNo);
            stmt.setString(7, amt);
            stmt.setString(8, failiure);
            stmt.setString(9, Utility.getsqlCurrentDate2());

            stmt.execute();
            System.out.println("about to commit");
            con.commit();

            System.out.print("the date of transaction is >>>>>" + Utility.getsqlCurrentDate2());
            System.out.print("transaction was initiated by >>>>>" + userName);

        } catch (Exception ex) {
            ex.printStackTrace();

            // return instData.result;
        } finally {
            
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        System.out.println("About to log failed  FT >>>>>> " + failiure);
        return failedFtResult;
        //  return instData.result;
    }

    public boolean updateLogFailedFt(String requestId, String failiure) {

        boolean failedFtResult = false;
        System.out.println("About to update failed  FT >>>>>> ");
        return failedFtResult;
    }
    
}


//~ Formatted by Jindent --- http://www.jindent.com
