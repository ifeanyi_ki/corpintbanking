
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author Administrator
 */
public class CustomerTrx extends ProcessClass {

    private static final String ACCOUNT_POSITION = "FN_GET_TRX";
    private int NBR_PARAMS = 6;
    private String StarDate;
    private String account;
    private String custNo;
    private String endDate;
    private String from;
    private String isCheck;
    // private final String schema = "ihsluat";
    private String schema = "ihsllive";
    private static final Logger logger = Logger.getLogger(CustomerTrx.class.getName());
    // Connection connect;
    private ArrayList list;
    private HashMap preResult;
    private String returnResult;
    private String to;
    private String ordering;

    public CustomerTrx() {
    }

    private void initializeVar() {
        this.account = null;
        this.custNo = null;
        this.from = null;
        this.returnResult = null;
        this.to = null;
        this.preResult = null;
        this.StarDate = null;
        this.endDate = null;
        this.isCheck = null;
        this.ordering = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acctno", 0, false,
                "N");
        this.custNo = inst_data.custId;
        this.StarDate = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "startDate", 0,
                false, "N");
        this.endDate = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "endDate", 0, false,
                "N");
        this.from = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "from", 0, false, "0");
        this.to = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "to", 0, false, "0");
        this.isCheck = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "check", 0, false,
                "N");
        this.ordering = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "order", 0,
                false, "desc");
        if (ordering == null) {
            ordering = "desc";
        }
        if (ordering.isEmpty() || ordering.length() == 0) {
            ordering = "desc";
        }

        System.out.println("start date:" + StarDate);
        System.out.println(endDate);
        System.out.println(from);
        System.out.println(to);
        System.out.println(isCheck);


        schema = (String) LoadBasicParam.getDataValuenew("FCCSCHEMA");

        logger.info(" fcc db schema name: " + schema);

    }

    private void getNumParam(String startdate, String enddate) {
        if (startdate.equals("N")) {
            NBR_PARAMS = NBR_PARAMS - 1;
        }

        if (enddate.equals("N")) {
            NBR_PARAMS = NBR_PARAMS - 1;
        }
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        //Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            instData.result = getTrxHist();//ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        finally {
//            if (connect != null) {
//                try {
//                    connect.close();
//                } catch (Exception ex) {}
//            }
//        }

        return instData.result;
    }

    public HashMap getTrxHist() {
        Connection connect = null;
        System.out.println("This is the hist account " + account);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Element parent = new Element("ROWSET");
        HashMap map = null;
        String xmlText = null;
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            System.out.println("This is the hist query " + chooseQuery());
            pstmt = connect.prepareStatement(chooseQuery());
            setQueryParam(pstmt);
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);
            map = this.generateXML(parent, xmlText);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return map;
    }

    public void setQueryParam(PreparedStatement pstmt) throws Exception {

        if (!account.equals("N") && !StarDate.equals("N") && !endDate.equals("N") && !to.equals("0")) {
            pstmt.setString(1, this.account);
            pstmt.setString(2, this.StarDate);
            pstmt.setString(3, this.endDate);
            pstmt.setString(4, this.from);
            pstmt.setString(5, this.to);
        } else if (!account.equals("N") && !StarDate.equals("N") && !endDate.equals("N")) {
            pstmt.setString(1, this.account);
            pstmt.setString(2, this.StarDate);
            pstmt.setString(3, this.endDate);
        } else if (!account.equals("N") && !to.equals("0")) {
            if (isCheck.equals("B")) {
                pstmt.setString(1, this.account);
                pstmt.setString(2, this.from);
                pstmt.setString(3, this.to);
            } else if (isCheck.equals("D")) {
                pstmt.setString(1, this.account);
                pstmt.setString(2, this.from);
                pstmt.setString(3, this.to);
                //pstmt.setString(4, this.isCheck); 
            } else if (isCheck.equals("C")) {
                pstmt.setString(1, this.account);
                pstmt.setString(2, this.from);
                pstmt.setString(3, this.to);
                //pstmt.setString(4, this.isCheck); 
            }
        } else {
            if (isCheck.equals("S")) {
                pstmt.setString(1, this.account);
            } else {
                pstmt.setString(1, this.account);
            }
        }
    }

    public String chooseQuery() {
        String query = null;
        if (!account.equals("N") && !StarDate.equals("N") && !endDate.equals("N") && !to.equals("0")) {
            //Ayomide added this if when client requested amount and period should be selectable together
            query = "select refno, ac_entry_sr_no, ac_no, ac_ccy, fcy_amount, lcy_amount,"
                    + " auth_stat, drcr_ind, trn_code, value_dt, " + schema + ".get_trn_desc(TO_CHAR(ac_no), TO_CHAR(refno), TO_DATE(value_dt), ac_entry_sr_no) trn_desc,trx_amount,Running_Balance,"
                    + "txn_init_date from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.ac_ccy, a.fcy_amount, a.lcy_amount,"
                    + " a.auth_stat, a.drcr_ind, a.trn_code, value_dt,a.trx_amount,Running_Balance,"
                    + "txn_init_date FROM get_transactions a"
                    + " WHERE to_char(a.ac_no) =to_char(?) and to_date(a.txn_init_date) between to_date(?,'dd-mon-yyyy')"
                    + " and to_date(?,'dd-mon-yyyy') and a.lcy_amount between ? and ? and auth_stat='A'and event!='REVL'";

            if (isCheck.equals("C")) {
                query += " and a.drcr_ind='C'";
            } else if (isCheck.equals("D")) {
                query += " and a.drcr_ind='D'";
            }

            query += "  order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ") order by ac_entry_sr_no " + ordering;

        } else if (!account.equals("N") && !StarDate.equals("N") && !endDate.equals("N")) {
            query = "select refno, ac_entry_sr_no, ac_no, ac_ccy, fcy_amount, lcy_amount,"
                    + " auth_stat, drcr_ind, trn_code, value_dt, " + schema + ".get_trn_desc(TO_CHAR(ac_no), TO_CHAR(refno), TO_DATE(value_dt), ac_entry_sr_no) trn_desc,trx_amount,Running_Balance,"
                    + "txn_init_date from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.ac_ccy, a.fcy_amount, a.lcy_amount,"
                    + " a.auth_stat, a.drcr_ind, a.trn_code, value_dt,a.trx_amount,Running_Balance,"
                    + "txn_init_date FROM get_transactions a"
                    + " WHERE to_char(a.ac_no) =to_char(?) and to_date(a.txn_init_date) between to_date(?,'dd-mon-yyyy')"
                    + " and to_date(?,'dd-mon-yyyy') and auth_stat='A'and event!='REVL'  order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ") order by ac_entry_sr_no " + ordering;
        } else if (!account.equals("N") && !to.equals("0")) {
            if (isCheck.equals("B")) {
                query = " select * from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.ac_ccy, a.fcy_amount, a.lcy_amount,"
                        + " a.auth_stat, a.drcr_ind, a.trn_code, value_dt, a.trn_dt,nvl(trxn_nrrxn(trn_ref_no,event_sr_no,module,ac_entry_sr_no,ac_no),'TrustBond Mortgage Bank Transaction') trn_desc,a.trx_amount,Running_Balance,"
                        + " txn_init_date FROM get_transactions a"
                        + " WHERE to_char(a.ac_no) =to_char(?)and a.lcy_amount between ? and ? "
                        + " and auth_stat='A' and event!='REVL' order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ") order by ac_entry_sr_no " + ordering;
            } else if (isCheck.equals("D")) {
                query = "select * from (select * from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.ac_ccy, a.fcy_amount, a.lcy_amount,"
                        + " a.auth_stat, a.drcr_ind, a.trn_code, value_dt, a.trn_dt,nvl(trxn_nrrxn(trn_ref_no,event_sr_no,module,ac_entry_sr_no,ac_no),'TrustBond Mortgage Bank Transaction') trn_desc,a.trx_amount,Running_Balance,"
                        + "txn_init_date FROM get_transactions a"
                        + " WHERE to_char(a.ac_no) =to_char(?)and a.lcy_amount between ? and ? "
                        + " and auth_stat='A' and a.drcr_ind='D' and event!='REVL' order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ") order by ac_entry_sr_no " + ordering + ") order by ac_entry_sr_no " + ordering;
            } else if (isCheck.equals("C")) {
                query = "  select * from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.ac_ccy, a.fcy_amount, a.lcy_amount,"
                        + " a.auth_stat, a.drcr_ind, a.trn_code,value_dt, a.trn_dt,nvl(trxn_nrrxn(trn_ref_no,event_sr_no,module,ac_entry_sr_no,ac_no),'TrustBond Mortgage Bank Transaction') trn_desc,a.trx_amount,Running_Balance,"
                        + "txn_init_date FROM get_transactions a"
                        + " WHERE to_char(a.ac_no) =to_char(?)and a.lcy_amount between ? and ? "
                        + " and auth_stat='A' and a.drcr_ind='C' and event!='REVL' order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ")order by ac_entry_sr_no " + ordering;
            }
        } else {
            if (isCheck.equals("S")) {
                query = "select refno, ac_entry_sr_no, ac_no, ac_ccy, fcy_amount, lcy_amount,"
                        + " auth_stat, drcr_ind, trn_code, value_dt, " + schema + ".get_trn_desc(TO_CHAR(ac_no), TO_CHAR(refno), TO_DATE(value_dt), ac_entry_sr_no) trn_desc,trx_amount,Running_Balance,"
                        + "txn_init_date from (select * from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.fcy_amount, a.lcy_amount,a.ac_ccy,"
                        + " a.auth_stat, a.drcr_ind, a.trn_code, value_dt,a.trx_amount,Running_Balance,"
                        + "txn_init_date FROM get_transactions a"
                        + " WHERE to_char(a.ac_no) =to_char(?) and auth_stat='A' and event!='REVL' order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + ") where rownum <=10) order by ac_entry_sr_no " + ordering;

            } else {
                query = "select refno, ac_entry_sr_no, ac_no, ac_ccy, fcy_amount, lcy_amount,"
                        + " auth_stat, drcr_ind, trn_code, value_dt, " + schema + ".get_trn_desc(TO_CHAR(ac_no), TO_CHAR(refno), TO_DATE(value_dt), ac_entry_sr_no) trn_desc,trx_amount,Running_Balance,"
                        + "txn_init_date from (select * from (SELECT a.trn_ref_no as refno, a.ac_entry_sr_no, a.ac_no, a.fcy_amount, a.lcy_amount,a.ac_ccy,"
                        + " a.auth_stat, a.drcr_ind, a.trn_code,value_dt,a.trx_amount,Running_Balance,"
                        + "txn_init_date FROM get_transactions a"
                        + " WHERE to_char(a.ac_no) =to_char(?) and auth_stat='A' and event!='REVL' order by txn_init_date " + ordering + ",a.ac_entry_sr_no " + ordering + " ) where txn_init_date >= trunc(sysdate-3)) order by ac_entry_sr_no " + ordering;

            }

        }
        System.out.println("query in customerTrx: " + query);
        return query;
    }

    public String Managenull(String variab, String what) {
        if (variab == null || variab.isEmpty()) {
            variab = what;
        }
        return variab;
    }

    public String getTrxXml(String acct, String startdate, String enddate, String froms, String tos, String drcrs, String order) {
        System.out.println(account + " StarDate " + startdate + "enddate: " + enddate + "froms; " + froms + "tos: " + tos);
        if (order == null) {
            ordering = "desc";
        } else {
            ordering = order;
        }
        account = acct;
        StarDate = Managenull(startdate, "N");//startdate == null ? "N" : startdate;
        endDate = Managenull(enddate, "N");//enddate == null ? "N" : enddate;
        from = Managenull(froms, "0");//froms == null ? "0" : froms;  
        to = Managenull(tos, "0");//tos == null ? "0" : tos;
        this.isCheck = Managenull(drcrs, "N"); //drcrs == null ? "N" : drcrs;

        System.out.println(account + " " + StarDate + " " + endDate);
        HashMap map = null;


        map = getTrxHist();//ExchangeMap(connect);

        return (String) map.get(RESULT_MAP_KEY);
    }

    @Override
    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();
        java.sql.Clob datas = procStmt.getClob(1);
        String data = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        result.put("returnResult", returnResult);

        // result.put("errorType", errorType);
        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(this.ACCOUNT_POSITION, NBR_PARAMS);
    }

    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        int counter = 2;    // This is used to help in allocating index appropriately

        procStmt.setString(counter, this.account);

        if (StarDate.equals("N")) {
            procStmt.setString(3, StarDate);
        } else {
            procStmt.setString(3, StarDate);
        }

        if (endDate.equals("N")) {
            procStmt.setString(4, endDate);
        } else {
            procStmt.setString(4, endDate);
        }

        procStmt.setString(5, this.from);
        procStmt.setString(6, this.to);
        procStmt.setString(7, this.isCheck);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, OracleTypes.CLOB);
    }

    //added by K.I to retrieve the customer's acc desc for receipt generation during transfers
    public String getAccName(String accNo) {
        String acc_name="";
        Connection connect = null;
        System.out.println("This is the cust account " + accNo);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String queryString = "select ac_desc from sttm_cust_account where cust_ac_no=?";

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt = connect.prepareStatement(queryString);
            pstmt.setString(1, accNo);
            rs = pstmt.executeQuery();
            
            if(rs.next()){
                acc_name = rs.getString(1);
                System.out.println("acc name "+ acc_name);
            }

        } catch (Exception e) {
            System.err.println("error in retrieving cust acc description during fund transfer receipt generation: " + e.getMessage());
        }finally {
            try {
                if(!connect.isClosed()){
                    connect.close();
                }                
            } catch (SQLException e) {
                System.out.println("error in closing connect: " + e.getMessage());
            }
        }


        return acc_name;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
