//~--- non-JDK imports --------------------------------------------------------
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.logic.ProcessClass;
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;

import java.util.HashMap;

public class ViewAccountSummary extends ProcessClass {
    private static String ACCT_SUMMARY_QUERY =
        "SELECT DISTINCT a.cust_no, b.customer_name1, b.customer_type, a.acc_status, "
        + "a.cust_ac_no, a.min_reqd_bal, a.acy_curr_balance, "
        + "a.acy_unauth_uncollected, a.acy_avl_bal, a.account_class,"
        + "d.limit_amount, a.ccy, a.ac_open_date, a.branch_code, " + "e.ac_class_type,a.acy_accrued_dr_ic, "
        + "TO_NUMBER (acy_avl_bal + NVL (limit_amount, 0)) drawing_power, " + "TO_NUMBER (  acy_avl_bal"
        + "+ NVL (limit_amount, 0) " + " - (  NVL (a.min_reqd_bal, 0) " + "+ NVL (a.acy_accrued_dr_ic, 0) " + ")"
        + ") Net_Available " + "FROM sttm_cust_account a, " + "sttm_customer b, "
        + "sttm_accls_ccy_balances c, " + " lmtm_limits d, " + "sttm_account_class e "
        + "WHERE a.cust_no = b.customer_no " + "AND c.account_class = a.account_class "
        + "AND d.liab_id(+) = a.cust_no" + " AND e.account_class(+) = a.account_class and a.cust_no=?";
    private String  account;
    //Connection      connect;
    private String  custNo;
    private String  isUsed;
    private HashMap preResult;
    private String  refNo;
    private String  returnResult;

    public ViewAccountSummary() {}

    private void initializeVar() {
        this.account      = null;
        this.custNo       = null;
        this.refNo        = null;
        this.returnResult = null;
        this.isUsed       = null;
        this.preResult    = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct", 0, false,
                null);
        this.refNo  = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0, false,
                null);
        this.custNo = inst_data.custId;
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        

        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
