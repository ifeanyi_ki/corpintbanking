
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.EbankingConfiguration;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public abstract class Processes implements EbankingConfiguration {
    protected Connection        dbConnection;
    protected CallableStatement procStmt;

    protected abstract void prepareExecProcedure(Connection con) throws Exception;

    protected abstract String getProcedureParamString();

    protected abstract String extractOutputValues() throws SQLException;

    protected abstract HashMap extractOutputValues(String input) throws SQLException;    // this input is usually null ,this is done to reduce code duplication

    protected abstract String executeProcedure() throws SQLException;

    // protected abstract void initializeVar();// same as in setInstanceVar()
    public abstract HashMap processRequest(DataofInstance instData);

    // protected abstract void setInstanceVar(DataofInstance instData) throws Exception; This must be uncommented so as to follow true template design

    protected String getProcedureString() {
        return "";
    }

    protected final String buildProcedureStatement(String p_proc_name, int p_nbr_params) {
        StringBuffer l_buf = null;

        l_buf = new StringBuffer(200);
        l_buf.append("{? = ");
        l_buf.append("call ").append(p_proc_name);
        l_buf.append(" (");

        for (int l_i = 0; l_i < p_nbr_params; l_i++) {
            if (l_i > 0) {
                l_buf.append(", ");
            }

            l_buf.append("?");
        }

        l_buf.append(")");
        l_buf.append("}");
        System.out.println(l_buf.toString());

        return l_buf.toString();
    }

    public HashMap ExchangeMap(Connection dbConnections) {

        // dbConnection=com.fasyl.ebanking.db.DataBase.getConnection();
        String l_txn_proc_rc = "";

        try {
            prepareExecProcedure(dbConnections);
            l_txn_proc_rc = executeProcedure();

            HashMap result = extractOutputValues(null);

            System.out.println(l_txn_proc_rc + "exchangemap");

            return result;
        } catch (SQLException e) {
            e.printStackTrace();

            if (e.getErrorCode() == 20000) {}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            /*
             * try {
             * rsetSiInq.close ();
             * } catch (Exception e) {
             * }
             */
            try {
                procStmt.close();
            } catch (Exception e1) {}
        }

        return null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
