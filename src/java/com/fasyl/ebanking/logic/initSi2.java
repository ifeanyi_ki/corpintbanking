
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class initSi2 extends Processes {

      private static final Logger logger = Logger.getLogger(initSi2.class.getName());
 
    
    
    // protected CallableStatement procStmt;
    private static final int NBR_PARAMS        = 3;
    private static final int PARAM_IDX_CUR     = 3;
    private static final int PARAM_IDX_CUST_ID = 2;

    // private static final int PARAM_IDX_ERROR_CODE = 5;
    private static final int    PARAM_IDX_ERROR_TYPE = 4;
    private static final int    PARAM_IDX_RET_VALUE  = 1;
    private static final String PROCEDURE_SI_INQ     = "f_si_initiation";
    ;
    public static final String  SUCCESS              = "false";
    String                      procName             = "";
    private String              custno;
    protected Connection        dbConnection;
    protected long              errorCode;
    protected String            errorType;
    protected int               refOrgStan;
    private String              returnResult;
    SIObject                    siobject;

    public initSi2() {}

    public void initializeVar() {
        this.custno       = null;
        this.errorCode    = 0;
        this.errorType    = null;
        this.procStmt     = null;
        this.returnResult = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.custno = inst_data.custId;
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();

        logger.info(" ******Inside process request **** ");
        
        initializeVar();

        try {
            setInstanceVar(instData);
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String getProcedureString() {
        return "";
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap       result = new HashMap();
        java.sql.Clob datas  = procStmt.getClob(3);
        String        data   = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        
        logger.info(" returnResult: " + returnResult);
        
        logger.info(" errorType: " + errorType);

        // }
        // errorCode             = procStmt.getInt       (PARAM_IDX_ERROR_CODE);
        errorType = procStmt.getString(PARAM_IDX_ERROR_TYPE);
        result.put("returnResult", returnResult);

        // result.put("errorCode", errorCode);
        result.put("errorType", errorType);

        return result;
    }

    /*
     * protected final String buildProcedureStatement(
     * String p_proc_name, int p_nbr_params) {
     *
     * StringBuffer l_buf = null;
     *
     * l_buf = new StringBuffer(200);
     *
     * l_buf.append("{? = ");
     * l_buf.append("call ").append(p_proc_name);
     * l_buf.append(" (");
     *
     * for (int l_i = 0; l_i < p_nbr_params; l_i++) {
     * if (l_i > 0) {
     * l_buf.append(", ");
     * }
     * l_buf.append("?");
     * }
     * l_buf.append(")");
     *
     * l_buf.append("}");
     *
     * System.out.println(l_buf.toString());
     *
     * return l_buf.toString();
     *
     * }
     */
    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SI_INQ, NBR_PARAMS);
    }

    /*
     * public HashMap initiateSI() {
     * HashMap result = null;
     * result = Exchange();
     * return result;
     *
     * }
     */
    public void prepareExecProcedure(Connection dbConnection) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        
        
        logger.info(" query: " + l_stmt_string);
        
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        
        logger.info(" custno: " + custno);
        
        procStmt.setString(PARAM_IDX_CUST_ID, custno);
        procStmt.execute();

        return procStmt.getString(PARAM_IDX_RET_VALUE);
    }

    protected void registerOutParameters() throws SQLException {

        // if (!isMerantJdbcDriver) {
        procStmt.registerOutParameter(PARAM_IDX_CUR, OracleTypes.CLOB);

        // }
        procStmt.registerOutParameter(PARAM_IDX_RET_VALUE, Types.VARCHAR);

        // procStmt.registerOutParameter (PARAM_IDX_CUST_ID, Types.INTEGER);
        // procStmt.registerOutParameter (PARAM_IDX_ERROR_CODE, Types.INTEGER);
        procStmt.registerOutParameter(PARAM_IDX_ERROR_TYPE, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
