/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import oracle.jdbc.OracleTypes;
import org.jdom.Element;

/**
 *
 * @author 
 */
public class ManageAppEmail extends ProcessClass{


    public ManageAppEmail()  {

    }

    Connection connection = com.fasyl.ebanking.db.DataBase.getConnection();

    /**
	This is the constant which represents the query for inserting a record in the
	mail message table.
	**/
       private HashMap hashMap = null;
       private boolean queryStatus;
       private Element parent;
       private String xmlText = null;
       private boolean draftResult=false;
       
       //Created by Ayo and Becky to list emails
       //private String toEmails;
       private String frmMsg;
       private String frmMsg1;
    /**
	This char constant indicates user type for ordinary users.
	**/
	public static final char
		USER_TYPE_ORDINARY					= 'O'
	;
	/**
	This char constant indicates user type for application admininstrator.
	**/
	public static final char
		USER_TYPE_APP_ADMIN					= 'C'
	;
	/**
	This char constant indicates user type for framework admininstrator.
	**/
	public static final char
		USER_TYPE_EB_ADMIN				= 'A'
	;
	private static final String
		INS_STMT_MAILMESSAGE		=
						"insert into mail_messages ("
					+	" msg_id"
					+	",from_id"
					+	",text_subject"
					+	",msg_date"
					+	",msg_text"
					+	",list_to_user"
					+	",cc_user_list"
					+	",msg_status"
                                        +       ",list_to_username"
                                        +        ",cc_username_list"
					+	") values ("
					+	" ?"
					+	",?"
					+	",?"
					+	",?"
					+	",?"
					+	",?"
					+	",?"
					+	",?"
                                        +	",?"
                                        +	",?"
					+	")"
	;
	/**
	This is the constant which represents the query for inserting a record in the
	mail message usermap table.
	**/
	private static final String
		INS_STMT_MAILMSGUSERMAP			=
						"insert into msg_user_map_mail ("
					+	" msg_id"
					+	",user_id"
					+	",box_flag"
					+	",number_seq"
					+	") values ("
					+	" ?"
					+	",?"
					+	",?"
					+	",?"
					+	")"
	;
	/**
	This is the constant which represents the query for validating a user.
	**/
        /**
	This variable holds the user id of the logged in user.
	**/
	protected String
		loginUserId
	;
	private static final String
		SEL_STMT_MSTUSER				=
				"select 1 from sm_user_access where cod_usr_id = ? "
			+	"and flg_status = 'A'"
	;
	/**
	This is the constant which represents the query for selecting the usertype of
	any user.
	**/
	private static final String
		SEL_STMT_CUSTMAP				=
			"select cod_usr_no, cod_role_id from sm_user_access where cod_usr_id = ?"
	;
	/**
	This is the constant which represents the query for selecting the usertype of
	and checking if the user selected is an admin of any of the apps that the
	logged in user is assigned.
	**/
	private static final String
		SEL_STMT_ADMIN_SAME_APP			=
					""
	;
	/**
	This is the constant which deletes the selected message from the mail message table
	**/
	private static final String
		DEL_STMT_MAILMESSAGE			=
			"delete from mail_messages where msg_id = ?"
	;
	/**
         * 
	This is the constant which deletes the selected message entries from the mail message
	usermap table for the specified id message.
	**/
	private static final String
		DEL_STMT_MSGMAP					=
			"delete from msg_user_map_mail where msg_id = ?"
	;
        /**
	This is the constant which deletes the selected message from the mail message table
	**/
	private static final String
		DEL_IBADMIN_MAILMESSAGE			=
			"delete from mail_messages where msg_id = ?"
	;
	/**
	This string contains the query to get the customer description for a userid
	***/
	private static final String
		SEL_USER_DESC	= 	"select cod_usr_id, cod_usr_name,cod_role_id "
						+	"from sm_user_access where cod_usr_id = ? "
	;
	//--------------------------------------------------------------------------
	/**
	This constant defines the delimiter which separates the user names in the TO
	list or the CC list
	**/
	private static final String
		DELIMITER 				= 	";"
	;
	/**
	This constant defines the Mode it can be Send/Save
	Send = 1
	Save = 2
	**/
	private static final int
		MODE_SEND 						= 	1
	;
	private static final int
		MODE_DRAFT						=	2
	;
	/**
	This string constant stores the value of flag denoting that the type of the
	request made by the user is compose.
	**/
	public static final String
		VAL_REQ_TYPE_COM 				= "COM"
	;
	/**
	This constant represents the status of the message.
	**/
	private static final String
		STATUS_SEND						= "S"
	;
	/**
	This constant represents the status of the message.
	**/
	private static final String
		STATUS_DRAFT					= "D"
	;

	/**
	This constant holds the prefix of the template id used for the different mail
	forms
	**/
	private static final String
		MAIL_FORM						= "MAILFORM_"
	;
	//--------------------------------------------------------------------------
	
	
	/**
	String to store value of Message Id.
	**/
	private long
		messageId
	;
        	/**
	String to store value of Message Id.
	**/
	private String
		messageId2
	;
	/**
	String to store value of users in To
	**/
	private String
		toMsg
	;
	/**
	String to store value of users in CC
	**/
	private String
		ccMsg
	;
	/**
	String to store value of Subject
	**/
	private String
		subject
	;
	/**
	String to store value of Text
	**/
	private String
		text
	;
	/**
	To store value of Mode it can be (Send(1)/Save(2))
	**/
	private int
		mode
	;
	/**
	To check whether the request which has been generated is
	to verify the message or to send the message.
	**/
	private String
		verify
	;
	/**
	To check whether the request which has been generated is
	from the draft box or a fresh message.
	**/
	private String
		flg_box
	;
	/**
	Indicates the no of records affected when the insert or delete statements
	are called.
	**/
	private int
		nbrRecords
	;

	/**
	Stores the request message
	**/
//	private String
//		requestMessage
//	;

	/**
	This constant points to the dataname used to retrieve the CSS names from the
	datamap.
	**/
	private static final String
		DATANAME_MAIL_SUBJECT			= ""
	;
	/**
	This constant points to the dataname used to retrieve the CSS names from the
	datamap.
	**/
	private static final String
		DATANAME_MAIL_ID				= ""
	;
	//--------------------------------------------------------------------------
	/**
	The variable is used to tokenize to and cc user list into individual users
	**/
	private StringTokeNizers
		userTokenizer				= new StringTokeNizers ("", ";")
	;
	//--------------------------------------------------------------------------
	/**
	This vector maintains a list of users so that no duplicate mails are sent, even
	if the user specifies the same user multiple times in the to and cc list.
	Each unique user gets the mail once only.
	**/
	private Vector
		toUserTable	=	new Vector ()
	;
	//--------------------------------------------------------------------------
	
        	/**
	This vector maintains a list of usernames.

	**/
	private String
		toUserNameTable	=	null
	;
	//--------------------------------------------------------------------------

        
        	/**
	This vector maintains a list of usernames.

	**/
	private String
		ccUserNameTable	=	null
	;
        
        	/****/
	private String
		delete	=	null
	;
	//--------------------------------------------------------------------------
        
	//--------------------------------------------------------------------------
	/**
	This method inititalizes the transaction setting all the instance variables.
	**/
        private String toMsg1 = null;
        private String ccMsg1 = null;
	protected void initializeVar()
	{
		toUserTable.clear ();
		nbrRecords = 0;
		flg_box	= "";
                this.messageId=0;
                this.messageId2=null;
                this.subject=null;
                this.text=null;
                this.toMsg=null;
                this.verify=null;
                this.text = null;
                this.nbrRecords=0;
                this.ccMsg=null;
                this.loginUserId=null;
                this.ccMsg1=null;
                this.toMsg1=null;
                toUserNameTable=null;
                ccUserNameTable=null;
                delete         =null;
                this.xmlText   =null;
                this.parent    =null;
                frmMsg1 = null;
                frmMsg = null;
                
	}
	//--------------------------------------------------------------------------
 protected void setInstanceVar(DataofInstance inst_data) throws Exception{
     ReadRequestAsStream decodes = new ReadRequestAsStream();
     this.verify=  decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "verify", 0, true, null),"UTF-8");
     this.toMsg=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldToMsg", 0, true, null),"UTF-8");
     this.subject=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSubject", 0, false, null),"UTF-8");
     this.ccMsg=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldCcMsg", 0, true, null),"UTF-8");
     this.flg_box=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "flg_box", 0, true, null),"UTF-8");
     this.text = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldTxt", 0, true, null),"UTF-8");
     this.verify = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "verify", 0, true, null),"UTF-8");
     String modes = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "mode", 0, true, null);
     this.messageId2=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "messageId", 0, true, null),"UTF-8");
     this.messageId=System.currentTimeMillis();
     //this.ccMsg1=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldCcMsg1", 0, true, null),"UTF-8");
     this.toMsg1=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldToMsg1", 0, true, null),"UTF-8");
     this.delete=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "delete", 0, true, null),"UTF-8");
     
     
     this.frmMsg=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldfromMsg", 0, true, null),"UTF-8");
     this.frmMsg1=decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldfromMsg1", 0, true, null),"UTF-8");
     
     parent = new Element("ROWSET");
     if (modes!=null || !modes.equals("")){
         this.mode=Integer.parseInt(modes);
     }
     this.loginUserId=inst_data.userId;
     if( flg_box.equals("")){
         if (bDebugOn)System.out.println("=== inside flg_box empty");
         flg_box=null;
     }
     if (bDebugOn)System.out.println("====This is flg_box =="+flg_box);

 }
 @Override
	public HashMap processRequest (DataofInstance instData

	)  {
                //this.toEmails = "";
                 if (bDebugOn)System.out.println("====inside processrequest ==");

		int 				l_to_success_count = 0,
							l_cc_success_count = 0,
							l_mode	    	   = MODE_SEND;

//		requestMessage		= p_request_message;

		try {
                     if (bDebugOn)System.out.println("====about to call setInstancevar ==");
                          setInstanceVar(instData);


			//buildMessageNode (l_mode);
                          if((delete!=null )){
                              instData.result=deleteMail(instData.userId,instData.customerType);
                              return instData.result;
                          }

			if (mode == MODE_SEND && flg_box == null) {
                            if (bDebugOn)System.out.println("====about to send mail that is not a draft=====");
				if(toMsg!=null) {
					l_to_success_count = splitIntoUserTokens (toMsg, "T");
                                        if (bDebugOn)System.out.println("This is number of to users "+l_to_success_count);
                                        //updatemailmsg(this.toUserNameTable,"to");
                                        //toUserNameTable=null;
				}
				if(ccMsg!=null) {
					l_cc_success_count = splitIntoUserTokens (ccMsg, "C");
                                         if (bDebugOn)System.out.println("This is number of cc users "+l_cc_success_count);
                                        // updatemailmsg(this.toUserNameTable,"cc");
                                        // toUserNameTable=null;
				}
				if((l_to_success_count+l_cc_success_count)==0 && verify.equals ("S") ) {

					mode = MODE_DRAFT;
					deleteMessage ();
					insertMailMessage ();
					mode = MODE_SEND;
				}
				//addSuccessfulNode (l_to_success_count + l_cc_success_count);
                            if ((l_to_success_count+l_cc_success_count)>0||this.draftResult)
                             this.queryStatus=false;
                            
                            //Code to send email comes here!!!
                            
                            System.out.println("Email To: " + toMsg);
                            System.out.println("Email To 1: " + toMsg1);
                            System.out.println("Email From: " + frmMsg);
                            System.out.println("Email From 1: " + frmMsg1);
                            System.out.println("To User Table length: " + toUserTable.size());
                            System.out.println("To User Table value: " + toUserTable.get(0));
                            
                            String type = "text/html";
                            String body = text;

                            String header = subject;
                            //String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");   // a table must be created to provide header,body,from
                            String fromemail = frmMsg;
                            sendmail send = new sendmail();
                            

                            System.out.println("fromemail: " + fromemail);
                            
                            for(int x = 0; x < toUserTable.size(); x++) {
                                boolean results = send.sendMessage(header, body, null, type, fromemail, toUserTable.get(x).toString());
                                
                            }
                            //String [] toEmailsArr = toEmails.split(";");
//                            
//                            for(int x = 0; x < toEmailsArr.length; x++) {
//                                boolean results = send.sendMessage(header, body, null, type, fromemail, toEmailsArr[x]);
//                                System.out.println(toUserNameTable.split(";")[x] + " : " + toEmailsArr[x]);
//                            }
                            
                            
                            
			} else if (mode == MODE_SEND && flg_box.equals ("D")) {
                            if (bDebugOn)System.out.println("===inside mode send and flgbox is D");
				deleteMessage ();
				if(toMsg !=	null) {
					l_to_success_count = splitIntoUserTokens (toMsg, "T");
                                        //updatemailmsg(this.toUserNameTable,"to");
                                       
                                        //this.ccUserNameTable=toUserNameTable;   /**Using ccUserNameTable to hold the data **/
                                         //toUserNameTable=null;
				}
				if(ccMsg !=	null) {
					l_cc_success_count = splitIntoUserTokens (ccMsg, "C");
                                        //updatemailmsg(this.toUserNameTable,"cc");
                                        //toUserNameTable=null;
				}
				//addSuccessfulNode (l_to_success_count + l_cc_success_count);
				if (nbrRecords == 0) {
						mode = MODE_DRAFT;
						insertMailMessage ();
				}

			} else if (mode == MODE_DRAFT ) {
                             if (bDebugOn)System.out.println("===inside mode  draft alone");
				l_mode = MODE_DRAFT;
				deleteMessage ();
				if (nbrRecords == 0) {
					insertMailMessage ();
				}

			}
			//buildMessageNode (l_mode);

		} catch (Exception e) {
			e.printStackTrace ();

		}finally{
                    if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
                }
              this.buildResponse();
              instData.result=hashMap;
             return instData.result;
	}
	//--------------------------------------------------------------------------
	/**
	This method deletes the entries for a given message from the msg_map_user_mail
	table and then from its parent table mail_messages
	**/
	private void deleteMessage ()
	throws Exception {

		PreparedStatement	l_map_stmt	=	null
						,	l_stmt		=	null;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {

			l_map_stmt	=
				con.prepareStatement (DEL_STMT_MSGMAP);
			l_stmt		=
				con.prepareStatement (DEL_STMT_MAILMESSAGE);

			l_map_stmt.setBigDecimal (1, BigDecimal.valueOf (messageId));
			l_map_stmt.executeUpdate ();
			l_stmt.setBigDecimal (1, BigDecimal.valueOf (messageId));
			l_stmt.executeUpdate ();
		} finally {
			try {
				l_map_stmt.close ();
			} catch (Exception e) {
			}
			try {
				l_stmt.close ();
			} catch (Exception e) {
			}
                        try {
                            if(con!=null){
				con.close ();
                            }
			} catch (Exception e) {
			}
			l_stmt	=	null;
			l_map_stmt	=	null;
		}

	}
	//--------------------------------------------------------------------------
	/**
	This method parses the query string given by the front end and extracts
	various fields from it.

	@param	p_request_message	HTTP query string sent by the front end.

	@return	null	If parsing query string and extracting fields from it is
					succesful, error result otherwise.
	**/

	//--------------------------------------------------------------------------
	/**
	This message gets the description for a given user
	@param	p_userid
	**/
	private String getUserDescription(
		String p_userid
	)throws Exception{

		StringTokeNizers 	l_tokenizer	= null;
                PreparedStatement        pstmt           = null;
		ArrayList			l_arr_list	= null;
		String				l_token		= null;
		ResultSet		l_userdesc	= null;
		StringBuffer		l_buf		= new StringBuffer (1024);
		int					l_count		= 0;
                
		l_tokenizer	= 	new StringTokeNizers (p_userid, DELIMITER);
                //Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		while (l_tokenizer.hasMoreTokens ()) {
			l_token	=	l_tokenizer.nextToken ();

			l_arr_list	= 	new ArrayList();
			l_arr_list.add(l_token);
                        pstmt =  connection.prepareStatement(SEL_USER_DESC);
			l_userdesc	=	pstmt.executeQuery();
			if(l_count>0){
				l_buf.append (";");
			}
			if (l_userdesc.next()){
				l_buf.append (
					l_userdesc.getString ("salution"))
					.append (" ")
					.append (l_userdesc.getString ("firstname"))
					.append (" ")
					.append (l_userdesc.getString("lastname")
				);
			}
			l_count++;
		}
		l_count	=	0;

		return l_buf.toString ();

	}
	//--------------------------------------------------------------------------
	/**
	This message splits the to and cc user list using JFStringTokenizer.
	The logic for validation has already been mentioned in the calling method.

	@param	p_userlist	the delimiter separated user list
	@param	p_type	whether To or Cc

	@return	int		No of valid users.
	**/
	private int splitIntoUserTokens (
		String p_userlist,
		String p_type
	) throws Exception {

		String		l_to_cc_user	= 	null;
		int 		l_seqnbr		=	0;
		boolean		l_ordinary_user	= (isThisUserOrdinary (loginUserId, "Z")),
//		boolean		l_ordinary_user = true,
					l_send_mode		= verify.equals ("S"),
					l_inserted		= false;

		userTokenizer.setData (p_userlist, DELIMITER, false);

		while (userTokenizer.hasMoreTokens ()) {
			l_to_cc_user = userTokenizer.nextToken ();
			if ((l_to_cc_user != null)  && (l_to_cc_user.length () != 0)) {
                            if(bDebugOn)System.out.println("=====Copied user is/are not null=======");
				l_to_cc_user	= l_to_cc_user.trim ();
			}

			if (toUserTable.contains (l_to_cc_user)) {
				continue;
			}

			if (loginUserId.equals(l_to_cc_user)) {
				continue;
			}

			toUserTable.add (l_to_cc_user);
                        if(toUserNameTable==null){
                        toUserNameTable=this.getUsernames(l_to_cc_user);
                        }else{
                           toUserNameTable=toUserNameTable+";"+ this.getUsernames(l_to_cc_user);
                        }
			if (!existsUser (l_to_cc_user)) {
				//addUserNode (l_to_cc_user, "M", p_type);
				continue;
			}
			/*if (l_ordinary_user) {
//				if (isThisUserOrdinary (l_to_cc_user, p_type)) {
//					addUserNode (l_to_cc_user, "A", p_type);
//					continue;
//			} else
				if (!isThisUserAdminOfSameApp (l_to_cc_user)) {
					//addUserNode (l_to_cc_user, "A", p_type);
					continue;
				}
			} */

			if (l_send_mode) {
                            if(bDebugOn)System.out.println("===send mode is true===");
				if (toUserTable.size() > 0) {
					if ( ! l_inserted) {
						if (nbrRecords == 0) {
							insertMailMessage ();
							l_inserted = true;
						}
					}
                                        if(bDebugOn)System.out.println("===send mode is true===");
					insertMailMessageMap (l_to_cc_user, p_type,++l_seqnbr);
				}
			}

			//addUserNode (l_to_cc_user, "S", p_type);
		}
                
		userTokenizer.reset ();
		return l_seqnbr;

	}
	//--------------------------------------------------------------------------
      
        
     private String updatemailmsg(String listofusername,String tocc){
            Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            String result = null;
            String query = "update mail_messages set LIST_TO_USERNAME=? where msg_id=?";
            if (tocc.equals("to")){
                query = "update mail_messages set LIST_TO_USERNAME=? where msg_id=?";
            }else{
                query = "update mail_messages set CC_USERNAME_LIST=? where msg_id=?";
            }
            try{
              pstmt = con.prepareStatement(query);  
              pstmt.setString(1,listofusername);
              pstmt.setBigDecimal(2, BigDecimal.valueOf (this.messageId));
              
             pstmt.executeUpdate();
            
            }catch(Exception ex){
                ex.printStackTrace();
            }finally{
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (pstmt != null) {
                    try {
                        pstmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (con != null) {
                    try {
                        con.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            return result;
        }

	/**
	This method returns if the user is an ordinary user other than Ordinary
	Corporate user with same token.

	@param	p_type	User in the to or cc list which needs to be validated.

	@return	boolean	true if the user is ordinary
	**/
	private boolean isThisUserOrdinary (
		String	p_user_id
	,	String	p_type
	) throws Exception {
                if(bDebugOn)System.out.println("=====inside isThisUserOrdinary======");
		PreparedStatement	l_stmt		= null;
		ResultSet			l_rs		= null;
		boolean				l_rc		= false;
		String				l_token		= null;
		String				l_app_id	= null;
		String				l_usertype	= null;
                 Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {
			l_stmt	= con.prepareStatement (SEL_STMT_CUSTMAP);
			l_stmt.setString (1, p_user_id);
			l_rs = l_stmt.executeQuery ();
			if (l_rs.next ()) {
				l_rc = (l_rs.getString ("cod_role_id").charAt (0) == USER_TYPE_ORDINARY);

				/*if (p_type.equals ("Z")) {
					return l_rc;
				}*/

				
				l_usertype	=	l_rs.getString ("cod_role_id");

			
			}

		} finally {
                if (l_rs != null) {
                    try {
                        l_rs.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (l_stmt != null) {
                    try {
                        l_stmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                if (con != null) {
                    try {
                        con.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
		}
                if(bDebugOn)System.out.println("=====inside isThisUserOrdinary======"+l_rc);
		return l_rc;

	}
	//--------------------------------------------------------------------------
	/**
	This method returns if the user is an admin of any one of the apps that the
	logged in user has been assigned.
	It is called only if the logged in user is an ordinary user.

	@param	p_type	User in the to or cc list which needs to be validated.

	@return	boolean	true if the user is admin of any of the apps as that of the
						 logged in user false otherwise
	**/
	private boolean isThisUserAdminOfSameApp(
		String	p_user_id
	) throws Exception {

		PreparedStatement	l_stmt		= null;
		ResultSet			l_rs		= null;
		boolean				l_rc		= false;
		char				l_type		= ' ';
		String				l_token		= "";
		String				l_app_id	= null;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {

			l_stmt	= con.prepareStatement (SEL_STMT_ADMIN_SAME_APP);
			l_stmt.setString (1, p_user_id);
			l_stmt.setString (2, p_user_id);
			l_rs = l_stmt.executeQuery ();

			if (l_rs.next ()) {
				l_rc = ((l_type = l_rs.getString ("cod_role_id").charAt (0)) == USER_TYPE_APP_ADMIN);
			}

			l_token		= l_rs.getString ("token");
			l_app_id	= l_rs.getString ("idapp");

			if ((!l_rc) && (l_type == USER_TYPE_ORDINARY)
					) {
				return true;
			}

			if (! l_rc) {
				return (l_type == USER_TYPE_EB_ADMIN);
			} else {
				
			}
		} finally {
			
                    if (l_rs != null) {
                        try {
                            l_rs.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (l_stmt != null) {
                        try {
                            l_stmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
		return l_rc;

	}
	//--------------------------------------------------------------------------
	/**
	This method returns if the user is an application admin.

	@param	p_type	User in the to or cc list which needs to be validated.

	@return	boolean	true if the user is an admin of any app on the system
	**/
	private boolean isThisUserAppAdmin(
		String	p_user_id
	) throws Exception {

		PreparedStatement	l_stmt	= null;
		ResultSet			l_rs	= null;
		boolean				l_rc	= false;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {
			l_stmt	= con.prepareStatement (SEL_STMT_CUSTMAP);
			l_stmt.setString (1, p_user_id);
			l_rs = l_stmt.executeQuery ();
			if (l_rs.next ()) {
				l_rc = (l_rs.getString ("cod_role_id").charAt (0) == USER_TYPE_APP_ADMIN);
			}
		} finally {
			
                    if (l_rs != null) {
                        try {
                            l_rs.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (l_stmt != null) {
                        try {
                            l_stmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
		}
		l_rs	=	null;
		l_stmt	=	null;
		return l_rc;

	}
	
	//--------------------------------------------------------------------------
	/**
	This method is called to verify if the user is an active valid user without
	the user being locked.

	@param	p_type	User in the to or cc list which needs to be validated.

	@return	boolean	true if the user is a valid active unlocked user.

	**/
	private boolean existsUser (
		String	p_user_id
	) throws Exception {
           if(bDebugOn)System.out.println("======inside method exist user======");
		ResultSet			l_rs = null;
		boolean				l_rc = false;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		PreparedStatement l_stmt	=	null;

		try {
			l_stmt = con.prepareStatement (SEL_STMT_MSTUSER);
			l_stmt.setString (1, p_user_id.toUpperCase());
			l_rs = l_stmt.executeQuery ();
			if (l_rs.next ()) {
				l_rc = true;
			}
		} finally {
			
                    if (l_rs != null) {
                        try {
                            l_rs.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (l_stmt != null) {
                        try {
                            l_stmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                        
		}
		l_stmt	=	null;
		l_rs	=	null;
		return l_rc;

	}
	//--------------------------------------------------------------------------
        private void buildResponse(){
            if (!this.queryStatus){
               Element result = new Element("ROW");
               Element xtxnseq = new Element("SUCCESS");
              xtxnseq.setText(String.valueOf("The Operation was successful"));
              result.addContent(xtxnseq);
              parent.addContent(result);
            }
             else{

            Element result = new Element("ROW");
            Element xtxnseq = new Element("ERROR");
             xtxnseq.setText(String.valueOf("The Operation was not successful"));
            result.addContent(xtxnseq);
            parent.addContent(result);

             }

          hashMap = generateXML(parent,this.xmlText);

}
	/**
	This is the method which is called to make the insertion into the mail_messages
	table.
	**/
	private void insertMailMessage ()
	throws Exception {

		PreparedStatement	l_stmt = null;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {
			l_stmt	=	con.prepareStatement (INS_STMT_MAILMESSAGE);
			l_stmt.setBigDecimal (1, BigDecimal.valueOf (messageId));
			l_stmt.setString (2, loginUserId);
			l_stmt.setString (3, subject);
			l_stmt.setTimestamp (4,
						Utility.getCurrentDate());
			l_stmt.setString (5, text);
			l_stmt.setString (6, toMsg);
			l_stmt.setString (7, ccMsg);
			l_stmt.setString (8,	(mode == MODE_SEND
										? STATUS_SEND
										: STATUS_DRAFT
									)
								);
			l_stmt.setString (9,this.toMsg1);//screennumber/taskid
			l_stmt.setString (10, this.ccMsg1);

			nbrRecords = l_stmt.executeUpdate ();

			if (mode == MODE_SEND) {
				insertMailMessageMap (loginUserId, "F",1);
			}
		} finally {
                    if (l_stmt != null) {
                        try {
                            l_stmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
		}

	}
	//-------------------------------------------------------------------------
	/**
	This method is called to insert a record into the msg_map_user_mail table

	@param	p_userid	User in the to or cc list which needs to be inserted in
						the table.
	@param	p_box_flag	Indicates whether the message is a To or a Cc.
	@param	p_seq_nbr	Is Incremented so that multiple users can get the same
						mail.
	**/
	private void insertMailMessageMap (
		String	p_userid
	,	String	p_box_flag
	,	int		p_seq_nbr
	) throws Exception {
                if(bdebugOn)System.out.println("====inside insertMailmessageMap=====");
		PreparedStatement	l_stmt = null;
                Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
		try {
			l_stmt	= con.prepareStatement (INS_STMT_MAILMSGUSERMAP);
			l_stmt.setBigDecimal (1, BigDecimal.valueOf (messageId));
			l_stmt.setString (2, p_userid);
			l_stmt.setString (3, p_box_flag);
		        l_stmt.setInt (4, p_seq_nbr);;
			l_stmt.executeUpdate ();
		} finally {

                    if (l_stmt != null) {
                        try {
                            l_stmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
		}

	}
	//--------------------------------------------------------------------------
	
	private void addHTTPNode (
		StringBuffer 	p_HTTPStr
	,	String		p_nodeName
	,	String		p_nodeValue
	){
		p_HTTPStr.append(p_nodeName).append("=").append(p_nodeValue).append("&");
	}
	//--------------------------------------------------------------------------
        
        /**This method is used to get a list of corresponding usernames
         * to a userid in the cc and to filed.**/
        private String getUsernames(String userid){
            Connection con = com.fasyl.ebanking.db.DataBase.getConnection();
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            String result = null;
            try{
              pstmt = con.prepareStatement("select cod_usr_name,email from sm_user_access where cod_usr_id=?");  
              
              pstmt.setString(1, userid);
              rs = pstmt.executeQuery();
              if(rs.next()){
                 result = rs.getString("cod_usr_name"); 
                 
//                 if("".equals(toEmails)) {
//                     toEmails = rs.getString("email");
//                 } else {
//                     toEmails +=  ";" + rs.getString("email");
//                 }
                 
                  //System.out.println("User: " + result +", Email: " + rs.getString("email"));
              }
            }catch(Exception ex){
                ex.printStackTrace();
            }finally{
                    if (rs != null) {
                        try {
                            rs.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (pstmt != null) {
                        try {
                            pstmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
            }
            return result;
        }
        
        private HashMap deleteMail(String userid,String userType){
            //String result = null;
            System.out.println(userid+" msgid "+this.messageId2+" usertype "+userType);
            String res = "";
            String msg = null;
            String err = null;
            CallableStatement pstmt = null;
            HashMap map  = new HashMap();
            Connection con          = com.fasyl.ebanking.db.DataBase.getConnection();
            try{
                pstmt = con.prepareCall("{? =call deleteMail(?,?,?,?,?)}");
                pstmt.setString(2,userid );
                pstmt.setString(4,userType );
                pstmt.setString(3,messageId2);
                pstmt.registerOutParameter(1, OracleTypes.VARCHAR);
                pstmt.registerOutParameter(5, OracleTypes.VARCHAR);
                pstmt.registerOutParameter(6, OracleTypes.VARCHAR);
                pstmt.execute();
                res = pstmt.getString(1);
               Element result = new Element("ROW");
               Element xtxnseq = new Element("SUCCESS");
               if (res.equals(null))
                   res="Error while deleting mail";
              xtxnseq.setText(String.valueOf(res));
              result.addContent(xtxnseq);
              parent.addContent(result);
              map = generateXML(parent,this.xmlText);

                
            }catch(Exception ex){
                ex.printStackTrace();
            }finally{
                    if (pstmt != null) {
                        try {
                            pstmt.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }

                    if (con != null) {
                        try {
                            con.close();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
            }
            return map;
        }

}
