/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ese
 */
public class AdminCreationProcess extends ProcessClass{
    
    private AdminCreationHandler adminCreationHandler;
    private Connection conn;
    private String isreject;
    private String par;
    private String refno;
    private String rejcomment;
    private String uniqueID;
    private String userID;
    private String userPassword;
    private String requesterEmail;
    
    
    protected void initializeVar() {
        adminCreationHandler = null;
        conn = null;
        isreject = null;
        par = null;
        refno = null;
        rejcomment = null;
        uniqueID = null;
        requesterEmail = null;
    }
    
    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();
        adminCreationHandler = new AdminCreationHandler();

        refno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "refno", 0,
                true, null), "UTF-8");
        isreject = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "isreject",
                0, true, null), "UTF-8");
        rejcomment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "rejcomment", 0, true, null), "UTF-8");
        uniqueID = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "uniqueID", 0, true, null), "UTF-8");
        requesterEmail = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "requesterEmail",
                0, true, null), "UTF-8");
    }
    
    @Override
    public HashMap processRequest(DataofInstance instData) {
        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            System.out.println("Error occured setting instance Var");
        }
        //Ref Number only gets accross if Approve or Reject is clicked
        if(!(refno == null || refno.equals(""))) {
            try {
                adminCreationHandler.manageApproval(instData, refno, isreject, rejcomment, requesterEmail);
            } catch (Exception ex) {
                System.out.println("Exception from getting IBMail in adminCreationHandler.manageApproval: " + ex.getMessage());
            }
        }
        
        //This would always list the pendings whether after approval or on pageload
        instData.result = adminCreationHandler.getAllPendings(instData);
        
        return instData.result;
    }
}
