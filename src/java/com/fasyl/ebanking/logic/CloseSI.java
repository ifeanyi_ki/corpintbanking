
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class CloseSI extends Processes {
    private static final int    NBR_PARAMS         = 8;
    private static final String PROCEDURE_SI_CLOSE = "ebanking_si.closesi";

    // String acctno;
    public static final String SUCCESS  = "false";
    String                     procName = "";
    private String             account;
    private String             branchCode;
    protected String           errorCode;
    protected String           errorType;
    protected String           errormsg;
    private String             instno;
    private String             userID;
    private String             returnResult;
    private String             rsetSiInq;
    SIObject                   siobject;

    public CloseSI(String branchCode, String instno) {
        this.branchCode = branchCode;
        this.instno     = instno;
    }

    @Override
    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;
        
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            this.userID = instData.userId;
            System.out.println(" ******Inside process request **** ");
            instData.result = ExchangeMap(connect);
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        


        return instData.result;
    }

    @Override
    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);

        java.sql.Clob datas = procStmt.getClob(5);
        String        data  = datas.getSubString(1, (int) datas.length());

        rsetSiInq = data;
        System.out.println("This is what I want " + rsetSiInq);

        // }
        errorCode = procStmt.getString(6);
        errorType = procStmt.getString(7);
        errormsg  = procStmt.getString(8);
        result.put("rsetSiInq", rsetSiInq);
        result.put("errorCode", errorCode);
        result.put("errorType", errorType);
        result.put("errormsg", errormsg);

        return result;
    }

    @Override
    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SI_CLOSE, NBR_PARAMS);
    }

    /*
     * public HashMap getSI(){
     * HashMap map = new HashMap();
     * map = ExchangeMap();
     * return map;
     * }
     */
    @Override
    protected void prepareExecProcedure(Connection con) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = con.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    @Override
    protected String executeProcedure() throws SQLException {
        System.out.println("Account : " + account);
        procStmt.setString(2, "SYSTEM");
        procStmt.setString(3, branchCode);
        procStmt.setString(4, instno);
        procStmt.setString(9, userID);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.VARCHAR);
        procStmt.registerOutParameter(5, OracleTypes.CLOB);
        procStmt.registerOutParameter(6, Types.VARCHAR);
        procStmt.registerOutParameter(7, Types.VARCHAR);
        procStmt.registerOutParameter(8, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
