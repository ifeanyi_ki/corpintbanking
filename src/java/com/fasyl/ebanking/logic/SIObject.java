
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Date;

/**
 *
 * @author Administrator
 */
public class SIObject {
    private String counterparty    = null;
    private String crBranchCode    = null;
    private String drBranchCode    = null;
    private String siAmount        = null;
    private String siCurrency      = null;
    private String siDestAccount   = null;
    private Date   siFinalExecDate = null;
    private Date   siFirstExecDate = null;
    private int    siFreqExec      = 0;
    private String siNarration     = null;
    private String siPriority      = null;
    private String siSourceAcct    = null;
    private String siType          = null;

    /**
     * @return the siType
     */
    public String getSiType() {
        return siType;
    }

    /**
     * @param siType the siType to set
     */
    public void setSiType(String siType) {
        this.siType = siType;
    }

    /**
     * @return the siSourceAcct
     */
    public String getSiSourceAcct() {
        return siSourceAcct;
    }

    /**
     * @param siSourceAcct the siSourceAcct to set
     */
    public void setSiSourceAcct(String siSourceAcct) {
        this.siSourceAcct = siSourceAcct;
    }

    /**
     * @return the siDestAccount
     */
    public String getSiDestAccount() {
        return siDestAccount;
    }

    /**
     * @param siDestAccount the siDestAccount to set
     */
    public void setSiDestAccount(String siDestAccount) {
        this.siDestAccount = siDestAccount;
    }

    /**
     * @return the siAmount
     */
    public String getSiAmount() {
        return siAmount;
    }

    /**
     * @param siAmount the siAmount to set
     */
    public void setSiAmount(String siAmount) {
        this.siAmount = siAmount;
    }

    /**
     * @return the siFreqExec
     */
    public int getSiFreqExec() {
        return siFreqExec;
    }

    /**
     * @param siFreqExec the siFreqExec to set
     */
    public void setSiFreqExec(int siFreqExec) {
        this.siFreqExec = siFreqExec;
    }

    /**
     * @return the siFirstExecDate
     */
    public Date getSiFirstExecDate() {
        return siFirstExecDate;
    }

    /**
     * @param siFirstExecDate the siFirstExecDate to set
     */
    public void setSiFirstExecDate(Date siFirstExecDate) {
        this.siFirstExecDate = siFirstExecDate;
    }

    /**
     * @return the siFinalExecDate
     */
    public Date getSiFinalExecDate() {
        return siFinalExecDate;
    }

    /**
     * @param siFinalExecDate the siFinalExecDate to set
     */
    public void setSiFinalExecDate(Date siFinalExecDate) {
        this.siFinalExecDate = siFinalExecDate;
    }

    /**
     * @return the siNarration
     */
    public String getSiNarration() {
        return siNarration;
    }

    /**
     * @param siNarration the siNarration to set
     */
    public void setSiNarration(String siNarration) {
        this.siNarration = siNarration;
    }

    /**
     * @return the siCurrency
     */
    public String getSiCurrency() {
        return siCurrency;
    }

    /**
     * @param siCurrency the siCurrency to set
     */
    public void setSiCurrency(String siCurrency) {
        this.siCurrency = siCurrency;
    }

    /**
     * @return the siPriority
     */
    public String getSiPriority() {
        return siPriority;
    }

    /**
     * @param siPriority the siPriority to set
     */
    public void setSiPriority(String siPriority) {
        this.siPriority = siPriority;
    }

    /**
     * @return the counterparty
     */
    public String getCounterparty() {
        return counterparty;
    }

    /**
     * @param counterparty the counterparty to set
     */
    public void setCounterparty(String counterparty) {
        this.counterparty = counterparty;
    }

    /**
     * @return the drBranchCode
     */
    public String getDrBranchCode() {
        return drBranchCode;
    }

    /**
     * @param drBranchCode the drBranchCode to set
     */
    public void setDrBranchCode(String drBranchCode) {
        this.drBranchCode = drBranchCode;
    }

    /**
     * @return the crBranchCode
     */
    public String getCrBranchCode() {
        return crBranchCode;
    }

    /**
     * @param crBranchCode the crBranchCode to set
     */
    public void setCrBranchCode(String crBranchCode) {
        this.crBranchCode = crBranchCode;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
