
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author baby
 */
public class ListMailMessages extends ProcessClass {
    public static final String BOX_FLAG_CC = "C";
    public static final String BOX_FLAG_TO = "T";

    /**
     * This string constant determines the record fetched is even numbered.
     */
    public static final String EVEN_STR = "E";

    /**
     * This constant identifies the Sequence name which is stored in the database
     */
    public static final String ID_SEQUENCE    = "SEQ_MAIL";
    public static final String MSG_STAT_DRAFT = "D";
    public static final String MSG_STAT_SEND  = "S";

    /**
     * This string constant determines the record fetched is odd numbered.
     */
    public static final String ODD_STR = "O";

    /**
     * This string constant names the HTTP query field for value of a flag denoting
     * whether the message belongs to inbox, sent messages box, drafts box etc..
     */
    protected static final String QPARAM_FLG_BOX = "fldFlgBox";
    private static final String   SEL_STMT_DRAFT =
        "select a.MSG_ID, a.FROM_ID, a.TEXT_SUBJECT, a.MSG_DATE, a.LIST_TO_USER,a.LIST_TO_USERNAME,a.CC_USERNAME_LIST,a.CC_USER_LIST " + "from mail_messages a "
        + "where a.FROM_ID = ? " + "and a.MSG_STATUS = 'D' order by a.MSG_DATE desc";
    private static final String DEL_STMT_MAIL= "delete from msg_user_map_mail where USER_ID = ? and msg_id=? ";
    private static final String SEL_STMT_INBOX =
        " select m.MSG_ID, m.FROM_ID, m.TEXT_SUBJECT, m.MSG_DATE, m.LIST_TO_USER,m.LIST_TO_USERNAME,m.CC_USERNAME_LIST,m.CC_USER_LIST," + "u.READ_DATE, 'a' touser "
        + "from mail_messages m, msg_user_map_mail u " + "where m.MSG_ID = u.MSG_ID " + "and m.MSG_STATUS = 'S' "
        + "and u.BOX_FLAG in ('T', 'C') " + "and u.USER_ID = ?  order by m.MSG_DATE desc";
    private static final String ADMIN_SEL_STMT_INBOX =
        " select m.MSG_ID, m.FROM_ID, m.TEXT_SUBJECT, m.MSG_DATE, m.LIST_TO_USER,m.LIST_TO_USERNAME,m.CC_USERNAME_LIST,m.CC_USER_LIST," + "u.READ_DATE, 'a' touser "
        + "from mail_messages m, msg_user_map_mail u " + "where m.MSG_ID = u.MSG_ID " + "and m.MSG_STATUS = 'S' "
        + "and u.BOX_FLAG in ('T', 'C') " + "and (u.USER_ID = 'IBADMIN' or u.USER_ID =?) AND m.MSG_ID not in(select msg_id from ibadmin_Mail_exclusion where user_id=?)  order by m.MSG_DATE desc";//m.MSG_ID not in(select msg_id from ibadmin_Mail_exclusion where user_id=?)
    private static final String SEL_STMT_SEND =
        " select m.MSG_ID, m.FROM_ID, m.TEXT_SUBJECT, m.MSG_DATE, m.LIST_TO_USER,m.LIST_TO_USERNAME,m.CC_USERNAME_LIST,m.CC_USER_LIST," + "u.READ_DATE, u.USER_ID touser "
        + "from mail_messages m, msg_user_map_mail u " + "where m.FROM_ID = ? " + "and m.MSG_STATUS = 'S' "
        + "and u.BOX_FLAG = 'F' " + "and m.MSG_ID = u.MSG_ID " + "order by m.MSG_DATE desc";

    /**
     * This string contains the query to get the customer description for a userid
     */
    private static final String SEL_USER_DESC = "select cod_usr_id, cod_usr_name "
                                                + "from sm_user_access a where cod_usr_id = ? ";

    // --------------------------------------------------------------------------

    /**
     * Constant holding semi colon delimiter.
     */
    public static final String SEMICOLON_DELIMITER = ";";

    // --------------------------------------------------------------------------
    private String inboxCount = "select count(1) from mail_messages m, msg_user_map_mail u "
                                + "where m.MSG_ID = u.MSG_ID and m.MSG_STATUS = 'S' and u.BOX_FLAG in ('T', 'C') "
                                + "and u.USER_ID = ? and u.read_date is null order by m.MSG_DATE desc ";
    private String  xmlText     = null;
    private boolean draftResult = false;
    Connection      connect
    ;

    /**
     * This date formatter is used to parse string date and to format a date.
     */
    public SimpleDateFormat dateFormatter;

    /**
     * This variable holds the mode of the inbox.
     */
    protected String flgBox;

    // --------------------------------------------------------------------------

    private String   fromname;
    private HashMap  hashMap;

    /**
     * This variable holds the user id of the logged in user.
     */
    protected String loginUserId;
    private Element  parent;
    private String   usertype;

    public ListMailMessages() {}

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent           = new Element("ROWSET");
        this.loginUserId = inst_data.userId;
        this.flgBox      = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "flg_box", 0, true, null), "UTF-8");

        if (bDebugOn) {
            System.out.println(flgBox + "====this is the flgbox");
        }

        if (inst_data.taskId.equals("514")) {
            this.flgBox = "I";
        }
        usertype = inst_data.customerType;
    }

    /**
     *   This method inititalizes the transaction setting all the instance variables.
     */
    protected void initializeVar() {
        this.hashMap       = null;
        this.parent        = null;
        this.fromname      = null;
        this.usertype      =null;
        this.dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        if (bDebugOn) {
            System.out.println("====inside processrequest ==");
        }

        instData.result = null;
        initializeVar();

        try {
            setInstanceVar(instData);
            searchMessages();
        } catch (Exception ex) {
            ex.printStackTrace();

            // I will apply good exception catching by returning exception inside catch bracket
        } 
        finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        instData.result = hashMap;

        return instData.result;
    }

    // --------------------------------------------------------------------------
    public String numberMsg(String userid) {
        Connection        connects  = com.fasyl.ebanking.db.DataBase.getConnection();
        PreparedStatement l_stmt    = null;
        ResultSet         l_rs      = null;
        int               l_nbr_msg = 0,
                          l_count   = 0;

        try {
            l_stmt = connects.prepareStatement(inboxCount);
            l_stmt.setString(1, userid);
            l_rs = l_stmt.executeQuery();

            while (l_rs.next()) {
                ++l_nbr_msg;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
//        finally {
//            if (l_rs != null) {
//                try {
//                    l_rs.close();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//            if (l_stmt != null) {
//                try {
//                    l_stmt.close();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//            
//            if (connects != null) {
//                try {
//                    connects.close();
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }

        return String.valueOf(l_nbr_msg);
    }

    // --------------------------------------------------------------------------
    protected void searchMessages() throws Exception {
        //connect = com.fasyl.ebanking.db.DataBase.getConnection();

        PreparedStatement l_stmt      = null;
        ResultSet         l_rs        = null;
        int               l_nbr_msg   = 0,
                          l_count     = 0;
        Element           l_element   = null;    // it was not null
        Timestamp         l_time      = null;
        ResultSet         l_userdesc  = null;
        String            l_fromid    = null,
                          l_token     = null,
                          l_temp      = null,
                          l_touser    = null;
        ArrayList         l_arr_list  = null;
        StringBuffer      l_buf       = new StringBuffer(1024);
        StringTokeNizers  l_tokenizer = null;
    
        //if (connect == null) {
//        if (bDebugOn) {
//            System.out.println("====connect is null===");
//        }

        connect = com.fasyl.ebanking.db.DataBase.getConnection();
        //}

        if (flgBox.equals(MSG_STAT_DRAFT)) {
            l_stmt = connect.prepareStatement(SEL_STMT_DRAFT);
        } else if (flgBox.equals(MSG_STAT_SEND)) {
            l_stmt = connect.prepareStatement(SEL_STMT_SEND);
        } else {
            String query = null;
            System.out.println(usertype+ " This is the usertype");
            if(usertype.equals("ADMIN")){
              query =   ADMIN_SEL_STMT_INBOX;
              
            }else{
                query =   SEL_STMT_INBOX;
   
            }
            l_stmt = connect.prepareStatement(query);
        }
         
        try {
             
            l_stmt.setString(1, loginUserId);
            if(usertype.equals("ADMIN" )&& flgBox.equals("I")){
                l_stmt.setString(2,loginUserId);
            }
            l_rs = l_stmt.executeQuery();

            while (l_rs.next()) {
                ++l_nbr_msg;

                Element result = new Element("ROW");
                Element toname = null;
                Element msgid  = new Element("MSG_ID");

                msgid.setText(String.valueOf(l_rs.getBigDecimal("msg_id").longValue()));
                l_fromid = l_rs.getString("FROM_ID");
                l_touser = l_rs.getString("LIST_TO_USER");

                Element fromid = new Element("FROM_ID");

                fromid.setText(l_fromid);

                Element to_user = new Element("TO_USER");

                to_user.setText(l_touser);

                Element fromnames = null;

                if (flgBox.equals("I")) {
                    l_temp = l_fromid;
                } else {
                    l_temp = l_touser;
                }
                if (l_temp!=null){
                l_tokenizer = new StringTokeNizers(l_temp, SEMICOLON_DELIMITER);
             
                while (l_tokenizer.hasMoreTokens()) {
                    l_token    = l_tokenizer.nextToken();
                    l_arr_list = new ArrayList();
                    l_arr_list.add(l_token);
                    if(connect  == null || connect.isClosed()) {
                        connect = com.fasyl.ebanking.db.DataBase.getConnection();
                    }
                    l_stmt = connect.prepareStatement(SEL_USER_DESC);
                    l_stmt.setString(1, l_token);
                    l_userdesc = l_stmt.executeQuery();

                    if (l_count > 0) {
                        l_buf.append(SEMICOLON_DELIMITER);
                    }

                    if (l_userdesc.next()) {
                        l_buf.append((l_userdesc.getString("cod_usr_name")));
                    }

                    l_count++;
                }
            }
                l_count = 0;

                if (flgBox.equals("I")) {
                    fromnames = new Element("FROM_NAME");
                    fromnames.setText(l_buf.toString());
                } else {
                    toname = new Element("TO_NAME");
                    toname.setText(l_buf.toString());
                }

                l_buf.setLength(0);

                Element subject = new Element("TEXT_SUBJECT");

                subject.setText(l_rs.getString("TEXT_SUBJECT"));

                Element datmessage = new Element("MSG_DATE");

                if (bDebugOn) {
                    System.out.println(l_rs.getTimestamp("MSG_DATE") + " ====This is the msg_date");
                }
                Element tousername = new Element("LIST_TO_USERNAME");

                tousername.setText(l_rs.getString("LIST_TO_USERNAME"));
                
                 Element ccusername = new Element("CC_USERNAME_LIST");

                ccusername.setText(l_rs.getString("CC_USERNAME_LIST"));
                datmessage.setText(dateFormatter.format(new Date(l_rs.getTimestamp("MSG_DATE").getTime())));

                Element touserlist = new Element("LIST_TO_USER");

                touserlist.setText(l_rs.getString("LIST_TO_USER"));
                
                 Element ccuserlist = new Element("CC_USER_LIST");

                ccuserlist.setText(l_rs.getString("CC_USER_LIST"));
            

                Element datread = null;

                if (!flgBox.equals(MSG_STAT_DRAFT)) {
                    l_time = l_rs.getTimestamp("READ_DATE");

                    if (l_time != null) {
                        datread = new Element("READ_DATE");
                        datread.setText(dateFormatter.format(new Date(l_time.getTime())));

                        // l_element.setAttribute(
                        // "datread", dateFormatter.format(new Date(l_time.getTime())));
                    }
                }

                result.addContent(msgid);
                result.addContent(fromid);
                result.addContent(to_user);

                if (fromnames != null) {
                    result.addContent(fromnames);
                }

                if (toname != null) {
                    result.addContent(toname);
                }

                result.addContent(subject);
                result.addContent(datmessage);
                result.addContent(touserlist);
                result.addContent(ccuserlist);
                result.addContent(tousername);
                result.addContent(ccusername);

                if (datread != null) {
                    result.addContent(datread);
                }

                parent.addContent(result);
            }
        } catch(Exception e){
            System.out.println("error: " + e.getMessage());
        }
//        finally {
//            try {
//                l_rs.close();
//            } catch (Exception e) {e.printStackTrace();}
//
//            try {
//                l_stmt.close();
//            } catch (Exception e) {e.printStackTrace();}
//        }

        Element flgbox = new Element("FLGBOX");

        flgbox.setText(flgBox);

        Element nbrmsg = new Element("NBRMSG");

        nbrmsg.setText(String.valueOf(l_nbr_msg));
        parent.addContent(flgbox);
        parent.addContent(nbrmsg);
        hashMap = generateXML(parent, this.xmlText);
    }

    // -------------------------------------------------------------------------
}


//~ Formatted by Jindent --- http://www.jindent.com
