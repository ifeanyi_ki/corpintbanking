
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.StringTokeNizers;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public class Sundry extends ProcessClass {

    // public static final String REMOVE_ACCOUNT = "update cust_acct set status=?,modified_date=?,modified_by=? where custno=? and cust_acct=?";
    // public static final String REMOVE_ACCOUNT = "Delete from cust_acct where custno=? and cust_acct=?";
    // public static final String REMOVE_ACCOUNT_REQ = "";
    public static final String ADD_ACCOUNT = "insert into cust_acct"
            + "(custno,cust_acct,status,added_date,added_by) values " + "(?,?,?,?,?)";
    public static final String RETRIEVE_ACCOUNT =
            "select a.cust_no, a.cust_ac_no from sttm_cust_account a where a.cust_no=? and a.record_stat in ('O') and a.cust_ac_no not in (select cust_acct from cust_acct where custno=? and status in('A','U')) ";
    public static final String RETRIEVE_ACCOUNT_TO_REMOVE =
            "select custno,cust_acct,b.cod_usr_id from cust_acct a,sm_user_access b where custno=? and status in('A') and a.custno=b.cust_no";

    /* This variable is used to store coneection instance for this class */
    private Connection connect = null;
    private String holidayList = null;
    char[] charArray = null;
    // private Connection connectToFcc = null;
    private PreparedStatement pstmt = null;
    private String xmlText = null;
    String LoginUserid;
    private String account;
    private String action;
    private String actiondesc;
    private String beneficiarygrpname;
    private String branch;
    private String bulkamtxml;
    private String customername;
    private String customerno;
    private String datavalue;
    private String date;
    private String highCount;
    DataofInstance instdata;
    private String isAddAccount;
    private String lowCount;
    private Element parent;
    private String tablecolumn;
    private String tablename;
    private String tenure;
    private String userid;
    private String header;
    private String body;
    private String password;
    private boolean resultz;

    public Sundry() {
    }

    protected void initializeVar() {
        pstmt = null;
        this.customerno = null;
        this.account = null;
        this.LoginUserid = null;
        this.isAddAccount = null;
        this.branch = null;
        this.date = null;
        this.tenure = null;
        this.tablecolumn = null;
        this.tablecolumn = null;
        datavalue = null;
        action = null;
        userid = null;
        actiondesc = null;
        customername = null;
        this.beneficiarygrpname = null;
        holidayList = null;
        charArray = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent = new Element("ROWSET");
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        customerno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "customerno", 0, true, null), "UTF-8");
        customername = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "customername", 0, true, null), "UTF-8");
        account = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct_no",
                0, true, null), "UTF-8");
        isAddAccount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "isAddAccount", 0, true, null), "UTF-8");
        branch = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "branch", 0,
                true, null), "UTF-8");
        date = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "startDates",
                0, true, null), "UTF-8");
        tenure = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "tenure", 0,
                true, null), "UTF-8");
        this.action = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0, false,
                null);
        actiondesc = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "actiondesc", 0, false,
                null);
        tablecolumn = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "tablecolumn", 0,
                false, null);
        tablename = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "tablename", 0, false,
                null);
        beneficiarygrpname = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "beneficiarygrpname", 0, false, null);
        datavalue = account;    // ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "roleid", 0, false, null);
        userid = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "userid", 0, false, null);

        if (bDebugOn) {
            System.out.println(" ===== This is the account number ======= " + account);
        }

        this.LoginUserid = inst_data.userId;
    }

    /* This method check whether a user has been created in the application */
    public boolean validateCustomerNo() {
        boolean result = false;
        ResultSet rs = null;

        try {
            pstmt = connect.prepareStatement("select 1 from sm_user_access where cust_no = ?");
            pstmt.setString(1, this.customerno);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                result = true;
            }
        } catch (Exception ex) {
            result = false;
            ex.printStackTrace();
        } finally {
            if ((pstmt != null) || (rs != null)) {
                try {
                    if (pstmt != null) {
                    }

                    pstmt.close();

                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return result;
    }

    public boolean retrieveAccountToRemove() {
        boolean result = false;
        ResultSet rs = null;

        try {
            pstmt = connect.prepareStatement(RETRIEVE_ACCOUNT_TO_REMOVE);
            pstmt.setString(1, customerno);
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        } finally {
            if ((pstmt != null) || (rs != null)) {
                try {
                    if (pstmt != null) {
                    }

                    pstmt.close();

                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            return result;
        }
    }

    public boolean retrieveAccountToCustno() {
        boolean result = false;
        ResultSet rs = null;

        try {
            pstmt = connect.prepareStatement(RETRIEVE_ACCOUNT);
            pstmt.setString(1, customerno);
            pstmt.setString(2, customerno);
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result = false;
            } else {
                result = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        } finally {
            if ((pstmt != null) || (rs != null)) {
                try {
                    if (pstmt != null) {
                    }

                    pstmt.close();

                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            return result;
        }
    }

    public String validateRequest(Hashtable instData, String reqParName, int arrayPosition, boolean isMandatory,
            String defaultRet)
            throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();
        String result = null;

        result = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(instData, reqParName, arrayPosition,
                isMandatory, defaultRet), "UTF-8");

        return result;
    }

    public void removeAccount() {
    }

    public boolean addAccountToCustno() {
        boolean result = false;
        int ins = 0;

        result = this.validateCustomerNo();

        try {
            if (result) {
                if (isAddAccount.equalsIgnoreCase("true")) {
                    pstmt = connect.prepareStatement(ADD_ACCOUNT);
                    pstmt.setString(1, customerno);
                    pstmt.setString(2, this.account);
                    pstmt.setString(3, "A");
                    pstmt.setTimestamp(4, Utility.getCurrentDate());
                    pstmt.setString(5, this.LoginUserid);
                    ins = pstmt.executeUpdate();
                } else {
                    pstmt = connect.prepareStatement(insertauthqry);
                    pstmt.setString(1, this.customerno);
                    pstmt.setString(2, this.account);
                    ins = pstmt.executeUpdate();
                }
            } else {
                result = false;
            }

            if (ins > 0) {
                result = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = false;
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    result = false;
                    ex.printStackTrace();
                }
            }

            return result;
        }
    }

//  public static void main(String[] args){
//     String tst = "test";
//     char [] ans = tst.toCharArray();
//     for (int i=0;i<ans.length; i++){
//     System.out.println(ans[i]);
//      }
//  }
    @Override
    public HashMap processRequest(DataofInstance instData) {
        this.initializeVar();
        instdata = instData;

        boolean result = false;

        if (bDebugOn) {
            System.out.println(instData.txnId);
        }

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);

            if ((instData.taskId.equals("105111")) || (instData.taskId.equals("106111"))) {

                // result = this.addAccountToCustno();
                result = addRequest4Auth(instData, userid, actiondesc, action, tablename, tablecolumn, datavalue);

                if (result) {
                    Element row = new Element("ROW");
                    Element success = new Element("SUCCESS");

                    //notify ib admin when a user is added or removed
                    String type = "text/html";
                    header = "Authorization Alert (Add / Remove account)";
                    body = "Dear admin,<br><br> Please note that there is an authorization for user with Account number: "
                            +account+" awaiting your action";
                    String toemail = (String) LoadEmails.getDataValuenew("AUTH");;
                    String fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
                    sendmail send = new sendmail();
                    resultz = send.sendMessage(header, body, password, type, fromemail, toemail);

                    success.setText("The operation was successful");
                    row.addContent(success);
                    parent.addContent(row);
                } else {
                    instData.result = Utility.processError(instData, NO_ACCT_TO_ADD);

                    return instData.result;
                }
            } else if (instData.taskId.equals("10511")) {
                result = retrieveAccountToCustno();

                if (!result) {
                    instData.result = Utility.processError(instData, NO_ACCT_TO_ADD);

                    return instData.result;
                }
            } else if (instData.taskId.equals("10611")) {
                if (bDebugOn) {
                    System.out.println("=== inside account to remove in processrequest=====");
                }

                result = retrieveAccountToRemove();

                if (!result) {
                    instData.result = Utility.processError(instData, NO_ACCT_TO_ADD);

                    return instData.result;
                }
            } else if (instData.txnId.equals("NTRVSFE")) {
                String[] dateArray = {null, null, null};

                // String date = "15/01/2011";//the date string must be in d format dd/mm/yyyy
                // String branch = null;
                System.out.println("Date: " + date);
                String FinalDate = Utility.addMonthsToDate(date, tenure);
                int day = 0;
                int month = 0;
                int year = 0;
                StringTokeNizers tokens = new StringTokeNizers(FinalDate, "/");

                if (tokens.countTokens() == 3) {
                    int counter = 0;

                    dateArray = tokens.getAllTokens();
                    day = Integer.parseInt(dateArray[0]);
                    month = Integer.parseInt(dateArray[1]);
                    year = Integer.parseInt(dateArray[2]);

                    if (bDebugOn) {
                        System.out.println(day + " month: " + month + " year: " + year);
                    }
                }

                holidayList = Utility.returnHolidayList(month, year, branch);

                if (holidayList != null) {
                    charArray = holidayList.toCharArray();

                    instData.result = new HashMap();
                    System.out.println("Test Day:" + day +":"+ testDay(day));
                    while (!testDay(day)) {
                        System.out.println("<<<<<< Testing date >>>>>>>> " + day);
                        day = day + 1;
                    }

                    if (charArray[day - 1] != 'H') {
                        FinalDate = day + "/" + month + "/" + year;
                        System.out.println("Final Date >>>>>>>>>>> " + FinalDate);

                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        DateFormat formats = new SimpleDateFormat("dd-MMM-yyyy");
                        java.util.Date dates = format.parse(FinalDate);

                        FinalDate = formats.format(dates);
                        System.out.println(FinalDate);
                        instData.result.put(RESULT_MAP_KEY, FinalDate);

                        // instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                        return instData.result;
                    }
                }
            } else if (instData.taskId.equals("1051")) {
                instData.result = new HashMap();

                String searchresult = searchCustomer4acctAddition();

                instData.result.put(RESULT_MAP_KEY, searchresult);
            } else if (instData.taskId.equals("007")) {
                result = getBeneficiaryTotal(instData);

                if (result) {
                    HashMap map = new HashMap();

                    map.put(RESULT_MAP_KEY, this.bulkamtxml);
                    instData.result = map;
                    instData.taskId = "001";
                } else {
                    instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

                    return instData.result;
                }
            } else {
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            }

            if (instData.result == null) {
                instData.result = generateXML(parent, this.xmlText);

                if (!result
                        || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding=" + "UTF-8"
                        + "?> <ROWSET />")) {
                    instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                    return instData.result;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    private boolean testDay(int day) {
        boolean result = false;
        if (holidayList != null) {
            if (charArray[day - 1] == 'H') {
                result = false;
            } else {
                result = true;
            }
        }
        return result;
    }

    private String searchCustomer4acctAddition() throws Exception {
        String result = "";


        int highcount = (this.highCount == null)
                ? 20
                : Integer.parseInt(this.highCount);
        int lowcount = (this.lowCount == null)
                ? 0
                : Integer.parseInt(this.lowCount);
        CallableStatement stmt = connect.prepareCall("{?=call fn_searchacc4addition(?, ?, ?, ?)}");

        stmt.registerOutParameter(1, OracleTypes.CLOB);
        stmt.setString(2, customerno);
        stmt.setString(3, this.customername.toUpperCase());
        stmt.setInt(4, highcount);
        stmt.setInt(5, lowcount);
        stmt.execute();

        java.sql.Clob datas = stmt.getClob(1);
        String data = datas.getSubString(1, (int) datas.length());

        return data;
    }

    public boolean getBeneficiaryTotal(DataofInstance instData) {
        boolean finalresult = false;

        try {
            System.out.println(beneficiarygrpname);

            CallableStatement statement = connect.prepareCall("{?=call fn_get_bulktotal(?,?,?,?,?)}");

            statement.registerOutParameter(1, OracleTypes.INTEGER);
            statement.registerOutParameter(4, OracleTypes.CLOB);
            statement.registerOutParameter(5, OracleTypes.VARCHAR);
            statement.registerOutParameter(6, OracleTypes.VARCHAR);
            statement.setString(2, instData.sessionId);
            statement.setString(3, beneficiarygrpname);
            statement.execute();
            System.out.println("hmmmmmm=========");

            int result = statement.getInt(1);

            if (result == 0) {
                finalresult = true;
                bulkamtxml = this.ManageClob(statement.getClob(4));
            } else {
                finalresult = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return finalresult;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
