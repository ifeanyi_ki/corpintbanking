
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class StopChequePayment extends ProcessClass {
    private static final int    NBR_PARAMS               = 5;
    private static final String PROCEDURE_IS_CHEQUE_USED = "chk_stop_payment";
    private Element             parent                   = null;
    private String              xmlText                  = null;
    private String              account;
    private String              chequeAmount;
    private String              chequeNo;
    private String              chequeNo2;
    //Connection                  connect;
    private String              isUsed;
    ArrayList                   list;
    private HashMap             preResult;
    private String              returnResult;

    public StopChequePayment() {}

    private void initializeVar() {
        this.account      = null;
        this.chequeNo     = null;
        this.chequeNo2     = null;
        this.chequeAmount = null;
        this.returnResult = null;
        this.isUsed       = null;
        this.preResult    = null;
        parent            = null;
        xmlText           = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.account = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acno", 0, false,
                null);
        this.chequeNo = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "checkno", 0, false,
                null);
         this.chequeNo2 = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "checkno2", 0, false,
                null);
        this.chequeAmount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "chequeAmount",
                0, false, null);
        parent = new Element("ROWSET");
    }
     
    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connects = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        int result = -2;

//      try {
//          setInstanceVar(instData);
//      } catch (Exception ex) {
//          ex.printStackTrace();
//      }
          String   header    = "";
            String   toemail   = "";
            String   fromemail = "";
        try {
           setInstanceVar(instData);
           header    = "Stop ChequeBook Payment";
           toemail   = (String) LoadEmails.getDataValuenew("CHQST");
           fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
           connects = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            procStmt = connects.prepareCall("{?=call chk_stop_payment(?,?,?,?,?,?,?)}");
            procStmt.setString(4, this.account);
            procStmt.setString(2, this.chequeNo);
            procStmt.setString(3, this.chequeNo2);
            procStmt.setString(5, null /* this.chequeAmount */);    // this was chaned because if the cheque has not being paid in amount will be null in flexcube
            procStmt.registerOutParameter(1, OracleTypes.INTEGER);
            procStmt.setString(8, instData.requestId);
            procStmt.registerOutParameter(6, OracleTypes.VARCHAR);
            procStmt.registerOutParameter(7, OracleTypes.VARCHAR);
            procStmt.execute();
            System.out.println(instData.requestId + " == instData.requestId== audit==" + procStmt.getInt(1) + "  "
                               + chequeNo + " " + account);
            result = procStmt.getInt(1);

//          String err_code=pstmt.getString(6);
//          String err_msg=pstmt.getString(5);
//          System.out.println(err_code+ "  error "+err_msg);
            System.out.println("Ei ni result==== " + result);

            if (result != 0) {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText(procStmt.getString(7));
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.responseMsg(procStmt.getString(7));//this.generateXML(parent, xmlText);
            } else {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("Payment has been stopped on the cheque successfully");
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);
            }
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
            ex.printStackTrace();

            return instData.result;
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connects != null) {
                try {
                    connects.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if ((instData.taskId.equals("3231")) && (result == 0)) {
            System.out.println("====== for 3231 =====");

            String type = "text/html";
            String body =
                "Dear Sir/Ma,<br>Payment has been stopped on the cheque  with the following credentials: <br> <B>Account Number : "
                + this.account + "<br> Cheque Number: <br>" + this.chequeNo
                + "<B><br> Thank You<br> Yours Faithfully <br> Internet Banking Service";
          
            sendmail send      = new sendmail();
            boolean  results   = send.sendMessage(header, body, this.account, type, fromemail, toemail);

//          if (results) {
//              preResult.clear();
//               instData.result=preResult;
//              instData.result.put("returnResult", " The cheque with cheque no " + this.chequeNo + "has been successfully requested to be stopped.");
//          } else {
//               preResult.clear();
//               instData.result=preResult;
//              instData.result.put("returnResult", "There is problem with mail server");
//          }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
