
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author baby
 */
public class ManageBeneficiary extends ProcessClass {
    private static final String GET_BENEFICIARY_TYPE = "SELECT * FROM BEN_TYPE";
    private static final String GET_BENE_GROUP_INFO  =
        "select distinct beneficiary_id,institution_id,description,beneftgrpname from  eb_beneft_details  where beneftgrpname=? and customer_no=?";
    private static final String INS_BENEFICIARY = "insert into eb_beneft_details values("
                                                  + "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String MODIFY_GRP_BENEFICIARY =
        "update eb_beneft_details set beneficiary_id=?,beneficiary_name=?,"
        + "customer_no=?,source_acct=?,destination_acct=?,institution_id=?,description=?,modified_date=?,"
        + "modified_by=?,beneftgrpname=?,amount=?"
        + "where beneficiary_id=? and destination_acct=? and beneftgrpname=? ";
    private static final String VIEW_BENEFIARY_DETAIL =
        "select * from  eb_beneft_details where beneficiary_id=? and destination_acct=? ";
    private static final String VIEW_GRP_BENEFICIARY =
        "select * from  eb_beneft_details where customer_no=? and beneftgrpname=? ";    // and beneficiary_type=?
    private static final String VIEW_GRP_BENEFICIARY_NAME =
        "select distinct beneftgrpname from  eb_beneft_details where customer_no=? and beneftgrpname is not null ";    // and beneficiary_type='GRP'
    
    private static final String VIEW_GRP_BENEFICIARY_NAME_NOIND =
        "select distinct beneftgrpname from  eb_beneft_details where customer_no=? and beneftgrpname is not null and beneficiary_type='GRP'";
    private static final String VIEW_IND_BENEFIARY =
        "select * from  eb_beneft_details where customer_no=? and beneficiary_type='IND' ";
    private String     amount          = null;
    private String     beneficiaryName = null;
    private String     beneficiaryacct = null;
    private String     bentype         = null;
    private Connection connect         = null;
    private String     custno          = null;
    private String     delqry          =
        "Delete from eb_beneft_details where beneficiary_id=? and destination_acct=? and beneftgrpname=? and customer_no=?";
    private String            description = null;
    private String            institution = null;
    private PreparedStatement pstmt       = null;
    private String            userid      = null;
    private String            xmlText     = null;
    long                      serial      = System.currentTimeMillis();
    private String            SourceAccount;
    private String            beneficiarygrp;
    private String            beneficiaryid;
    private String            bengrpname;
    private String            ccy;
    DataofInstance            instancedata;
    DataofInstance            instdata;
    private String            oldbeneficiaryacct;
    private String            oldbeneficiarygrp;
    private Element           parent;

    public ManageBeneficiary() {}

    protected void initializeVar() {
        pstmt                   = null;
        xmlText                 = null;
        beneficiaryName         = null;
        beneficiaryacct         = null;
        description             = null;
        institution             = null;
        custno                  = null;
        bentype                 = null;
        beneficiarygrp          = null;
        this.instancedata       = null;
        beneficiaryid           = null;
        bengrpname              = null;
        this.oldbeneficiaryacct = null;
        amount                  = null;
        ccy                     = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        this.beneficiaryName = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "beneficiaryname", 0, true, null), "UTF-8");
        this.beneficiaryacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "beneficiaryacct", 0, true, null), "UTF-8");
        this.SourceAccount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "SourceAccount", 0, true, null), "UTF-8");
        this.institution = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "institution", 0, true, null), "UTF-8");
        this.description = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "description", 0, true, null), "UTF-8");
        this.beneficiarygrp = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "beneficiarygrp", 0, true, null), "UTF-8");
        this.bentype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "bentype", 0, true, null), "UTF-8");
        this.beneficiaryid = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "beneficiaryid", 0, true, null), "UTF-8");
        this.bengrpname = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "selectbengrp", 0, true, null), "UTF-8");
        this.oldbeneficiaryacct =
            decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "oldbeneficiaryacct", 0, true, null), "UTF-8");
        oldbeneficiarygrp = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "oldbeneficiarygrp", 0, true, null), "UTF-8");
        amount = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "txnamount",
                0, true, null), "UTF-8");
        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");

        if (bDebugOn) {
            System.out.println(oldbeneficiarygrp + " ==== amount ++= " + amount + " ==ccy== " + ccy);
        }

        if ((amount == null) || amount.equals("")) {
            System.out.println("amount is null");
            amount = null;
        }

        this.instancedata = inst_data;
        parent            = new Element("ROWSET");

        if (this.instancedata.taskId.equals("60111")) {
            this.beneficiaryid = null;
        }
    }

    private Element getBeneficiaryType() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        boolean   result = false;

        try {
            if (instancedata.taskId.equals("602") || instancedata.taskId.equals("60211")
                    || instancedata.taskId.equals("604") || instancedata.taskId.equals("60411")
                    || instancedata.taskId.equals("60120") || instancedata.taskId.equals("604")
                    || instancedata.taskId.equals("60411") || instancedata.taskId.equals("603")) {
                String query = VIEW_GRP_BENEFICIARY_NAME;//30-may-2012
                if (instancedata.taskId.equals("60120")){
                    query = VIEW_GRP_BENEFICIARY_NAME_NOIND;
                }
                pstmt = connect.prepareStatement(query);
                pstmt.setString(1, instancedata.custId);
                System.out.println(instancedata.custId + " customer no");
            } else {
                pstmt = connect.prepareStatement(GET_BENEFICIARY_TYPE);
            }

            rs     = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }finally{
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (parent == null) {
            parent = parent = new Element("ROWSET");
        }

        return parent;
    }

    private Element ListBeneficiary(String type) {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        boolean   result = false;

        try {
            if (instancedata.taskId.equals("6021") || instancedata.taskId.equals("6041")
                    || instancedata.taskId.equals("6031")) {
                pstmt = connect.prepareStatement(VIEW_GRP_BENEFICIARY);
                pstmt.setString(1, this.instancedata.custId);

                // pstmt.setString(2, this.bentype);
                pstmt.setString(2, this.beneficiarygrp);
            } else if (instancedata.taskId.equals("60211") || instancedata.taskId.equals("60411")
                       || instancedata.taskId.equals("60311")) {
                pstmt = connect.prepareStatement(VIEW_BENEFIARY_DETAIL);
                System.out.println(beneficiaryid + " ====== beneficiaryid "
                                   + BigDecimal.valueOf(Long.parseLong(beneficiaryid.trim())) + " acct "
                                   + beneficiaryacct);
                pstmt.setBigDecimal(1, BigDecimal.valueOf(Long.parseLong(beneficiaryid.trim())));
                pstmt.setString(2, this.beneficiaryacct);
            } else if (instancedata.taskId.equals("60122")) {
                pstmt = connect.prepareStatement(GET_BENE_GROUP_INFO);

                // System.out.println(beneficiaryid + " ====== beneficiaryid " + BigDecimal.valueOf(Long.parseLong(beneficiaryid.trim())) + " acct " + beneficiaryacct);
                pstmt.setString(1, this.beneficiarygrp);
                pstmt.setString(2, instancedata.custId);
            }

            rs     = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (parent == null) {
            parent = parent = new Element("ROWSET");
        }

        return parent;
    }

    private Element deleteBeneneficiary() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        int       result = -1;

        try {
            if (bDebugOn) {
                System.out.println("beneficiaryid : " + beneficiaryid + " beneficiaryacct: " + beneficiaryacct
                                   + " beneficiarygrp: " + beneficiarygrp);
            }

            pstmt = connect.prepareStatement(delqry);
            pstmt.setString(1, beneficiaryid);
            pstmt.setString(2, beneficiaryacct);
            pstmt.setString(3, beneficiarygrp);
            pstmt.setString(4, instancedata.custId);
            result = pstmt.executeUpdate();

            if (result > 0) {
                if (bDebugOn) {
                    System.out.println("Thank God");
                }

                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation was successful");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            } else {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation failed, try again later");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (parent == null) {
            parent = parent = new Element("ROWSET");
        }

        return parent;
    }

    private Element updateBeneficiary() {
        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        int       result = 0;

        try {
            if (this.instancedata.taskId.equals("602111")) {
                pstmt = connect.prepareStatement(MODIFY_GRP_BENEFICIARY);
                pstmt.setBigDecimal(1, BigDecimal.valueOf(Long.parseLong(beneficiaryid)));
                pstmt.setString(2, this.beneficiaryName);
                pstmt.setString(3, instancedata.custId);
                pstmt.setString(4, "");
                pstmt.setString(5, this.beneficiaryacct);
                pstmt.setString(6, this.institution);
                pstmt.setString(7, this.description);

                // pstmt.setString(8, this.bentype);
                pstmt.setTimestamp(8, Utility.getCurrentDate());
                pstmt.setString(9, userid);
                pstmt.setString(10, this.beneficiarygrp);
                pstmt.setBigDecimal(11, validateAmt(amount));

                if (bDebugOn) {
                    System.out.println(oldbeneficiarygrp + " == " + beneficiaryid + " === " + oldbeneficiaryacct + " "
                                       + amount);
                }

                pstmt.setString(12, beneficiaryid);
                pstmt.setString(13, this.oldbeneficiaryacct);
                pstmt.setString(14, this.oldbeneficiarygrp);

                // result = pstmt.executeUpdate();
            } else {
                System.out.println(" === inside insert");
                pstmt = connect.prepareStatement(INS_BENEFICIARY);

                if (beneficiaryid != null) {
                    pstmt.setBigDecimal(1, BigDecimal.valueOf(Long.parseLong(beneficiaryid)));

                    if (bDebugOn) {
                        System.out.println(beneficiaryid + " ===beneficiaryid");
                    }
                } else {
                    System.out.println(BigDecimal.valueOf(serial) + " as bigdecimal serial");
                    pstmt.setBigDecimal(1, BigDecimal.valueOf(serial));
                }

                pstmt.setString(2, this.beneficiaryName);
                pstmt.setString(3, instancedata.custId);
                pstmt.setString(4, "");
                pstmt.setString(5, this.beneficiaryacct);
                pstmt.setString(6, this.institution);
                pstmt.setString(7, this.description);
                pstmt.setString(8, this.bentype);
                pstmt.setTimestamp(9, Utility.getCurrentDate());
                pstmt.setString(10, userid);
                pstmt.setTimestamp(11, Utility.getCurrentDate());
                pstmt.setString(12, userid);
                pstmt.setString(13, "1");
                pstmt.setString(14, this.beneficiarygrp);

                 pstmt.setBigDecimal(15, BigDecimal.valueOf(0));
//                pstmt.setBigDecimal(15, validateAmt(amount));
                pstmt.setString(16, ccy);
                pstmt.setString(17, "");

                // result = pstmt.executeUpdate();
            }

            result = pstmt.executeUpdate();

            if (bDebugOn) {
                System.out.println(result + " result");
            }

            if (result > 0) {
                if (bDebugOn) {
                    System.out.println("Thank God");
                }

                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation was successful");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            } else {
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText("The operation failed, try again later");
                row.addContent(success);
                parent.addContent(row);
                instancedata.result = this.generateXML(parent, xmlText);
            }

            // parent = this.convertToXml(parent, "ROW", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (parent == null) {
            parent = parent = new Element("ROWSET");
        }

        return parent;
    }

    public BigDecimal validateAmt(String amount) {
        BigDecimal amt = new BigDecimal(amount);

        if (amount.equals("")) {

            // System.out.println("amount is null hmmm");
            amount = null;
        }

//        System.out.println(amt.valueOf((amount == null)
//                                       ? 0
//                                       : Double.parseDouble(amount)) + " =========");
        System.out.println(new BigDecimal(amount) + " =========");

        return amt ;
                                  
                                  
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        //connect = com.fasyl.ebanking.db.DataBase.getConnection();

        ResultSet rs     = null;
        boolean   result = false;

        this.initializeVar();
        instdata = instData;

        try {
            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);
            this.chooseprocessAction();

            // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
            if ((parent == null)) {
                result          = false;
                instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                return instData.result;
            } else {
                result = true;

                if (instData.result == null) {
                    instData.result = generateXML(parent, this.xmlText);

                    if (!result
                            || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                                   + "UTF-8" + "?> <ROWSET />")) {
                        instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                    }
                }
            }
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
            ex.printStackTrace();
        } finally {
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (parent == null) {
            parent = parent = new Element("ROWSET");
        }

        return instData.result;
    }

    private void chooseprocessAction() {
        ArrayList updatebeneficiary = new ArrayList();
        ArrayList deletebeneficiary = new ArrayList();
        ArrayList getbeneficiary    = new ArrayList();
        ArrayList listbeneficiary   = new ArrayList();

        updatebeneficiary.add("60111");    // Insert new  Individual beneficiary screen
        updatebeneficiary.add("602111");
        updatebeneficiary.add("601221");
        deletebeneficiary.add("603111");
        getbeneficiary.add("601");
        getbeneficiary.add("602");
        getbeneficiary.add("603");
        getbeneficiary.add("604");
        getbeneficiary.add("60120");
        listbeneficiary.add("6021");
        getbeneficiary.add("60120");
        listbeneficiary.add("60122");
        listbeneficiary.add("60211");
        listbeneficiary.add("6041");
        listbeneficiary.add("6031");
        listbeneficiary.add("60411");
        listbeneficiary.add("60311");

        if (updatebeneficiary.contains(this.instancedata.taskId)) {
            this.updateBeneficiary();
        }

        if (deletebeneficiary.contains(this.instancedata.taskId)) {
            this.deleteBeneneficiary();
        }

        if (listbeneficiary.contains(this.instancedata.taskId)) {
            this.ListBeneficiary(bentype);
        }

        if (getbeneficiary.contains(this.instancedata.taskId)) {
            this.getBeneficiaryType();
        }
    }

    private void choosePrepareStatement() throws Exception {
        ArrayList listbeneficiary = new ArrayList();
    }

//  public static void main(String [] args){
//      String amount = "";
//       if( amount.equals("")){
//      System.out.println("amount is null hmmm");
//          amount=null;
//      }
//        System.out.println(BigDecimal.valueOf(amount==null?0:Long.parseLong(amount))+ " =========");
//      }
}


//~ Formatted by Jindent --- http://www.jindent.com
