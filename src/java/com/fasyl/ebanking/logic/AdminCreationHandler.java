/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Ayo
 *
 * Class created by Ayo to handle Admin creation authorization request nd
 * approval
 */
public class AdminCreationHandler {

    private PreparedStatement pstmt;
    private Connection conn;
    private CallableStatement callStmt;
    private Statement statement;
    private ResultSet resultSet;
    private String insertauthqry = "insert into auth_request_log (dat_txn,cod_user_id,session_id,"
            + "cod_retrieval_ref,cod_assgn_authorizer,request_status,cod_authorizer_sysip,"
            + "request_desc,request_par,action,action_table,action_column,action_value, cod_requester_email)"
            + " values(sysdate,?,?,auth_seq.nextval,?,?,?,?,?,?,?,?,?,?)";

    public boolean sendForAuthorization(String tableName, String adminUserInfo, DataofInstance instData) {
        boolean result = false;
        String userID = adminUserInfo.substring(adminUserInfo.indexOf("<user_id>") + 9,
                adminUserInfo.indexOf("</user_id>"));
        try {
            conn = com.fasyl.ebanking.db.DataBase.getConnection();
            if (conn != null) {
                conn.setAutoCommit(false);
                pstmt = conn.prepareStatement(this.insertauthqry);
                //System.out.println(this.email + this.userid + this.olduser);
                pstmt.setString(1, userID.toUpperCase().trim()); //Supposed to be UserID, already in adminUserInfo
                pstmt.setString(2, instData.sessionId);
                pstmt.setString(3, ""); //No authorizer yet
                pstmt.setString(4, "W"); //Status W signifies pending. What a status!!!
                pstmt.setString(5, instData.remoteAddress);
                pstmt.setString(6, "Create an Admin"); //Request Description
                pstmt.setString(7, instData.request);//Request URL with parameter
                pstmt.setString(8, "CA"); //CA signifies Create Admin
                pstmt.setString(9, tableName); //SM_USER_ACCESS for now
                pstmt.setString(10, "all_columns"); //This value would  not be used
                pstmt.setString(11, adminUserInfo);//This is the Almighty string with all info 
                pstmt.setString(12, instData.email);//This is the requester's email   

                System.out.println("About to execute Statement");
                System.out.println(pstmt.toString());
                pstmt.execute();
                result = pstmt.getUpdateCount() > 0;
                System.out.println("After execute Statement, Result is :" + result);

                if (result) {
                    conn.commit();
                } else {
                    conn.rollback();
                }


            } else {
                System.out.println("Connection is null in " + AdminCreationHandler.class.getName());
                //Code for conn == null
            }

        } catch (SQLException sqlExc) {
            System.out.println("SQLException in " + AdminCreationHandler.class.getName());
            sqlExc.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
                if (pstmt != null && !pstmt.isClosed()) {
                    pstmt.close();
                }
            } catch (SQLException ex) {
                System.out.println("SQLException closing resource in " + AdminCreationHandler.class.getName());
                ex.printStackTrace();
            }
        }
        return result;
    }

    //This function gets all the Authorization request for Admin User Creation 
    public HashMap getAllPendings(DataofInstance instData) {
        int result;
        try {
            conn = com.fasyl.ebanking.db.DataBase.getConnection();
            if (conn != null) {
                System.out.println("About to list users");
                callStmt = conn.prepareCall("{?=call fn_auth_user(?,?,?,?,?,?)}");
                callStmt.registerOutParameter(1, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(4, OracleTypes.CLOB);
                callStmt.registerOutParameter(5, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(7, OracleTypes.VARCHAR);
                callStmt.setString(2, "adminUser");
                callStmt.setString(3, instData.sessionId);
                callStmt.execute();
                result = callStmt.getInt(1);

                System.out.println("List user function returned: " + result);
                if (result == 0) {
                    HashMap map = new HashMap();
                    java.sql.Clob datas = callStmt.getClob(4);
                    String data = datas.getSubString(1, (int) datas.length());

                    data = data.replace("&quot;", "\"");
                    System.out.println(data);
                    map.put("returnResult", data);
                    instData.result = map;
                } else {
                    instData.result = Utility.processError(instData, "0020");

                    return instData.result;
                }
            } else { //Else for if(conn != null)
                System.out.println("connection is null at " + AdminCreationHandler.class.getName());
                instData.result = Utility.processError(instData, "0020");
                return instData.result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, "0020");

            return instData.result;
        } finally {
            if (conn != null) {
                try {
                    conn.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (callStmt != null) {
                try {
                    callStmt.close();

                    // pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    //This function manages the Approve and Reject for Admin User Creation
    public void manageApproval(DataofInstance instData, String refno,
            String isreject, String rejcomment, String requesterEmail) throws Exception {

        int result;

        try {

            conn = com.fasyl.ebanking.db.DataBase.getConnection();
            if (conn != null) {
                conn.setAutoCommit(false);
                System.out.println("about to authorise");
                callStmt = conn.prepareCall("{?=call fn_manage_auth(?,?,?,?,?,?,?,?,?)}");
                callStmt.registerOutParameter(1, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(5, OracleTypes.CLOB);
                callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(7, OracleTypes.VARCHAR);
                callStmt.registerOutParameter(8, OracleTypes.VARCHAR);
                callStmt.setString(2, refno);
                callStmt.setString(3, instData.userId);
                callStmt.setString(4, instData.sessionId);
                callStmt.setString(9, isreject);
                callStmt.setString(10, rejcomment);
                callStmt.execute();
                System.out.println("about to commit");
                conn.commit();
                result = callStmt.getInt(1);

                System.out.println("Manage Auth user function returned: " + result);
                if (result == 0) {
                    //HashMap map = new HashMap();
                    String header, body, fromemail, toemail, userID = "", userRole = "";
                    //If creation request not rejected, send details to new user
                    if (isreject.equals("N")) {
                        java.sql.Clob datas = callStmt.getClob(5);
                        String data = datas.getSubString(1, (int) datas.length());

                        //data = data.replace("&quot;", "\"");
                        System.out.println(data);
                        //map.put("returnResult", data);
                        //instData.result = map;
                        String[] dataArr = data.split(";");
                        userID = dataArr[0];
                        toemail = dataArr[1].trim();
                        String password = dataArr[2];
                        userRole = dataArr[3];


                        //Send email to the New Administrator created
                        String type = "text/html";
                        header = "New Admin User Notification";
                        body = "Dear New User,<br /><br />"
                                + "You have been created as an administrator on the"
                                + "Internet Banking platform with the following details. <br /> <br />"
                                + "User ID: <b>" + userID + "</b> <br />"
                                + "Password: <b>" + password + "</b> <br />"
                                + "Role Type: <b>" + userRole + "</b> <br /><br />"
                                + "You should receive a passcode for first time login soon."
                                + "<br /><br />Thank you";
                        //If an email is not registered for this IBUSER, it uses general email as from
                        fromemail = (instData.email == null || instData.email.trim().equals(""))
                                ? instData.email : (String) LoadBasicParam.getDataValuenew("IBAEMAIL");

                        sendmail send = new sendmail();
                        boolean t = send.sendMessage(header, body, "", type, fromemail, toemail);
                        System.out.println("Mail sent successfully for Admin created? " + t);
                    }

                    //Send email to the requesting Admin
                    if (requesterEmail != null && !requesterEmail.trim().equals("")) {

                        //This is implemented for the sending of email after approval / Rejection by Ayo
                        //String msgHeader, msgBody, toEmail, fromEmail, name;
                        toemail = requesterEmail;
                        fromemail = LoadEmails.getDataValuenew("AUTH");

                        System.out.println("From: " + fromemail);
                        System.out.println("To: " + toemail);
                        System.out.println("Request Ref No: " + refno);
                        System.out.println("Reject Comment: " + rejcomment);

                        body = "Dear Admin, <br /> Please find below, details of your authorization request: <br />"
                                + "RequestID: " + refno + "<br />"
                                + "Request Description: Add a New Admin User <br />";
                        
                        //Only add these if approved
                        if(isreject.equals("N")) {
                            body += "UserID: " + userID + "<br />"
                                + "User Role: " + userRole + "<br />";
                        }
                                

                        if (isreject.equalsIgnoreCase("Y")) { //If request was rejected
                            header = "Rejected | Add a New Admin User" + " | " + userID;
                            body += "Action: Rejected <br />"
                                    + "Reason: " + rejcomment + "<br />";
                        } else {
                            header = "Approved | Add a New Admin User" + " | " + userID;
                            body += "Action: Approved <br />";
                        }

                        body += "<br /> Thank you.";
                        sendmail mailSender = new sendmail();
                        boolean res = mailSender.sendMessage(header, body, "", "text/html", fromemail, toemail);
                        if (res) {
                            System.out.println("Email sent sucessfully");
                        } else {
                            System.out.println("Email could not be sent");
                        }
                    } // Ends if (requesterEmail != null && !requesterEmail.trim().equals(""))
                } else {
                    //instData.result = Utility.processError(instData, "0020");
                }
            } else {
                System.out.println("Connection is null in " + AdminCreationHandler.class.getName());
            }

        } catch (SQLException ex) {
            System.out.println("SQLException in " + AdminCreationHandler.class.getName());
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null && !conn.isClosed()) {
                    conn.close();
                }
                if (callStmt != null && !callStmt.isClosed()) {
                    callStmt.close();
                }
            } catch (SQLException ex) {
                System.out.println("SQLException closing resource in " + AdminCreationHandler.class.getName());
                ex.printStackTrace();
            }
        }
    }
}
