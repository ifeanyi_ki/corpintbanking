
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class InitSi {
    private static final int NBR_PARAMS        = 3;
    private static final int PARAM_IDX_CUR     = 3;
    private static final int PARAM_IDX_CUST_ID = 2;

    // private static final int PARAM_IDX_ERROR_CODE = 5;
    private static final int    PARAM_IDX_ERROR_TYPE = 4;
    private static final int    PARAM_IDX_RET_VALUE  = 1;
    private static final String PROCEDURE_SI_INQ     = "f_si_initiation";
    ;
    public static final String  SUCCESS              = "false";
    String                      procName             = "";
    private String              custno;
    protected Connection        dbConnection;
    protected long              errorCode;
    protected String            errorType;
    protected CallableStatement procStmt;
    protected int               refOrgStan;
    private String              rsetSiInq;
    SIObject                    siobject;

    public InitSi(String custno) {
        this.custno = custno;
    }

    protected String getProcedureString() {
        return "";
    }

    protected HashMap extractOutputValues() throws SQLException {
        HashMap result = new HashMap();

        /*
         * if (isMerantJdbcDriver) {
         * rsetSiInq               = procStmt.getResultSet ();
         * } else {
         */
        java.sql.Clob datas = procStmt.getClob(3);
        String        data  = datas.getSubString(1, (int) datas.length());

        rsetSiInq = data;

        // }
        // errorCode             = procStmt.getInt       (PARAM_IDX_ERROR_CODE);
        errorType = procStmt.getString(PARAM_IDX_ERROR_TYPE);
        result.put("rsetSiInq", rsetSiInq);

        // result.put("errorCode", errorCode);
        result.put("errorType", errorType);

        return result;
    }

    protected final String buildProcedureStatement(String p_proc_name, int p_nbr_params) {
        StringBuffer l_buf = null;

        l_buf = new StringBuffer(200);
        l_buf.append("{? = ");
        l_buf.append("call ").append(p_proc_name);
        l_buf.append(" (");

        for (int l_i = 0; l_i < p_nbr_params; l_i++) {
            if (l_i > 0) {
                l_buf.append(", ");
            }

            l_buf.append("?");
        }

        l_buf.append(")");
        l_buf.append("}");
        System.out.println(l_buf.toString());

        return l_buf.toString();
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SI_INQ, NBR_PARAMS);
    }

    public HashMap initiateSI() {
        HashMap result = null;

        result = Exchange();

        return result;
    }

    public void prepareExecProcedure() throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = dbConnection.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    public String executeProcedure() throws SQLException {
        procStmt.setString(PARAM_IDX_CUST_ID, custno);
        procStmt.execute();

        return procStmt.getString(PARAM_IDX_RET_VALUE);
    }

    protected void registerOutParameters() throws SQLException {

        // if (!isMerantJdbcDriver) {
        procStmt.registerOutParameter(PARAM_IDX_CUR, OracleTypes.CLOB);

        // }
        procStmt.registerOutParameter(PARAM_IDX_RET_VALUE, Types.VARCHAR);

        // procStmt.registerOutParameter (PARAM_IDX_CUST_ID, Types.INTEGER);
        // procStmt.registerOutParameter (PARAM_IDX_ERROR_CODE, Types.INTEGER);
        procStmt.registerOutParameter(PARAM_IDX_ERROR_TYPE, Types.VARCHAR);
    }

    protected HashMap Exchange() {
        dbConnection = com.fasyl.ebanking.db.DataBase.getConnection();

        String l_txn_proc_rc = "";

        try {
            prepareExecProcedure();
            l_txn_proc_rc = executeProcedure();

            HashMap result = extractOutputValues();

            System.out.println(l_txn_proc_rc);

            // if (SUCCESS.equals(l_txn_proc_rc)) {
            // setTransactionError (errorCode);
            // setBeanValues ();
            return result;

            // } else {

            // System.out.println("error in adding the si");
            // }
            // buildResponseMessage ();
        } catch (SQLException e) {
            e.printStackTrace();

            if (e.getErrorCode() == 20000) {}
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

            /*
             * try {
             * rsetSiInq.close ();
             * } catch (Exception e) {
             * }
             */
            try {
                procStmt.close();
            } catch (Exception e1) {e1.addSuppressed(e1);}
            try {
                dbConnection.close();
            } catch (Exception e1) {e1.printStackTrace();}
        }

        return null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
