
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GetUserDetail extends Processes {
    private int          NBR_PARAMS         = 1;
    private final String PROCEDURE_ACCT_INQ = "FN_GET_USER_DETAIL";
    private String       returnResult;
    private String       user;

    /* this class is used for ajax call that only invoive part of the page e.g account validation with taskid of 001 */
    public GetUserDetail() {}

    protected String executeProcedure() throws SQLException {
        System.out.println(user);
        procStmt.setString(2, user);
        procStmt.execute();

        return procStmt.getString(1);
    }

    public void initializeVar() {
        this.user         = null;
        this.returnResult = null;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.user = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "user", 0, false, null);
    }

    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        returnResult = procStmt.getString(1);
        result.put("returnResult", returnResult);

        return result;
    }

    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_ACCT_INQ, NBR_PARAMS);
    }

    /*
     * public HashMap getSI(){
     * HashMap map = new HashMap();
     * map = ExchangeMap();
     * return map;
     * }
     */
    protected void prepareExecProcedure(Connection con) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = con.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
