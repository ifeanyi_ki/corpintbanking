
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl;
import com.fasyl.corpIntBankingadmin.bo.FundTransferBo;
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.LoadEBMessages;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author baby
 */
public class LocalFundTransfer extends ProcessClass {

    private String branch = null;
    private Element parent = null;
    private String xmlText = null;
    private String DestAcctNo;
    private String amt;
    private String benacct;
    private String ccy;
    private String ftdebitacc;
    private String ftlimt;
    private String token;
    private String txnCode;
    private String trxId;
    private static final Logger logger = Logger.getLogger(LocalFundTransfer.class.getName());
    // 20141218 narration patch
    private String narration;
    private String deSourceCode;
    private String roleId;
    private String auth1;
    private String auth2;
    private String action;
    private String auth1Comment;
    private String auth2Comment;
    FundTransferBo fundTransferBo = new FundTransferBoImpl();
    private int outcome = 0;

    public LocalFundTransfer() {
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        // ftlimt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ftlimt", 0,
        //      true, null), "UTF-8");
        ftdebitacc = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ftdebitacc", 0, true, null), "UTF-8");

//        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
//                "ftdebitacc", 0, true, null), "UTF-8");
        DestAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "DestAcctNo", 0, true, null), "UTF-8");

//        benacct = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benacct",
//                0, true, null), "UTF-8");
        // 20141212 
        benacct = DestAcctNo;

        ccy = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ccy", 0, true,
                null), "UTF-8");
        amt = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, true,
                null), "UTF-8");
        token = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "token", 0,
                true, null), "UTF-8");

        trxId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "trxId", 0,
                true, null), "UTF-8");
        auth1 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth1", 0,
                true, null), "UTF-8");
        auth2 = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth2", 0,
                true, null), "UTF-8");
        action = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "action", 0,
                true, null), "UTF-8");
        auth1Comment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth1Comment", 0,
                true, null), "UTF-8");
        auth2Comment = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "auth2Comment", 0,
                true, null), "UTF-8");
        roleId = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "roleId", 0,
                true, null), "UTF-8");
        System.out.println("transaction id: " + trxId);
//        branch = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "branch", 0,
//                true, null), "UTF-8");
        parent = new Element("ROWSET");

        // branch = "099";
        // 20141218 narration patch
        branch = (String) LoadBasicParam.getDataValuenew("DEBRANCH");

        // logger.info(" request params: " + inst_data.requestHashTable.toString());
        narration = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "narration", 0,
                true, null), "UTF-8");

        deSourceCode = (String) LoadBasicParam.getDataValuenew("DESOURCECODE");

        txnCode = (String) LoadBasicParam.getDataValuenew("IB_TXN_CODE");

        System.out.println("Txn Code for Local:" + txnCode);
        logger.info(" deSourceCode: " + deSourceCode);
        logger.info(" request params: branch " + branch + " amt: " + amt + " ccy: " + ccy + " ftdebitacc: " + ftdebitacc + " DestAcctNo: " + benacct + " customer id: " + inst_data.custId + " narration: " + narration);

    }

    protected void initializeVar() {
        ftlimt = null;
        ftdebitacc = null;
        DestAcctNo = null;
        benacct = null;
        ccy = null;
        amt = null;
        token = null;
        branch = null;
        narration = null;  // 20141218 narration patch
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        //   Connection        connect   = com.fasyl.ebanking.db.DataBase.getConnection();

        logger.info(" now using tbmbfcc con");
        FundTransferBo fundTransferBo = new FundTransferBoImpl();
        Connection connect = null;
        String errCode = "", errReason = "";

        boolean result = false;
        CallableStatement statement = null;

        try {
            setInstanceVar(instData);
            result = validatePin(instData.userId, token);

            //update the users' comment and action. If action is DECLINED, dont process the transaction 
            switch (roleId) {
                case "Low Level Authorizer":
                    outcome = fundTransferBo.processFTByAuth1(auth1, action, auth1Comment, trxId);
                    break;
                case "High Level Authorizer":
                    outcome = fundTransferBo.processFTByAuth2(auth2, action, auth2Comment, trxId);
                    break;
            }

            if (!action.equalsIgnoreCase("DECLINED")) {
                if (result) {
                updateToken(instData.userId); //force the token to expire after each submission of ft
                    // 20141218 narration patch
                    logger.info("{?=call ibft.fn_manage_ft3(?,?,?,?,?,?,?,?,?,?,?)}");
                    
                    connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
                    try {
                        connect.setAutoCommit(false);
                    } catch (SQLException sqlExc) {
                        sqlExc.printStackTrace();
                    }
                    
                    statement = connect.prepareCall("{?=call ibft.fn_manage_ft3(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                    statement.registerOutParameter(1, OracleTypes.INTEGER);
                    statement.registerOutParameter(12, OracleTypes.VARCHAR);
                    statement.registerOutParameter(13, OracleTypes.VARCHAR);
                    statement.registerOutParameter(14, OracleTypes.VARCHAR);
                    statement.setString(2, deSourceCode);
                    statement.setString(3, instData.sessionId);
                    statement.setString(4, branch);
                    statement.setString(5, benacct);    // bene grp
                    statement.setString(6, ftdebitacc);
                    //  statement.setString(7, DestAcctNo);   
                    statement.setString(7, instData.custId);
                    statement.setString(8, ccy);
                    statement.setString(9, amt);
                    statement.setString(10, txnCode);
                    statement.setString(11, narration);

                    // System.out.println( statement.getString(1)+" "+ statement.getString(4)+" "+ statement.getString(5));
                    statement.execute();
                    
                    int resultz = statement.getInt(1);
                    
                    logger.info(" result: " + resultz);
                    String txn_ref_no = statement.getString(12);
                    errCode = statement.getString(13);
                    errReason = statement.getString(14);
                    if (txn_ref_no != null) {
                        System.out.println("txn_ref_no: " + txn_ref_no);
                        //setTxn_ref_no(txn_ref_no);
                    } else {
                        System.out.println("txn_ref_no: " + txn_ref_no);
                    }
                    logger.info(" error code: " + statement.getString(12));
                    logger.info(" error msg: " + statement.getString(13));
                    
                    if (resultz == 0) {
                        
                        try {
                            connect.commit();
                        } catch (SQLException sqlExc) {
                            sqlExc.printStackTrace();
                        }
                        
                        Element row = new Element("ROW");
                        Element custacc = new Element("FTDEBITACC");
                        Element benacc = new Element("BENACC");
                        Element el_amount = new Element("AMOUNT");
                        Element el_narration = new Element("NARRATION");
                        Element success = new Element("SUCCESS");
                        Element el_txn_ref_no = new Element("REFNO");
                        
                        custacc.setText((String) ftdebitacc);
                        benacc.setText((String) benacct);
                        el_amount.setText((String) amt);
                        el_narration.setText((String) narration);
                        success.setText(
                                (String) LoadEBMessages.getMessageValue(FUND_TRANSFER_SUCESS_MESSAGE).getMessgaedesc());
                        el_txn_ref_no.setText(txn_ref_no);
                        
                        row.addContent(custacc);
                        row.addContent(benacc);
                        row.addContent(el_amount);
                        row.addContent(el_narration);
                        row.addContent(success);
                        row.addContent(el_txn_ref_no);
                        parent.addContent(row);
                        instData.result = this.generateXML(parent, xmlText);
                        boolean updatePostStatusByTrxId = fundTransferBo.updatePostStatusByTrxId(trxId);
                        System.out.println("result of updating post status: " + updatePostStatusByTrxId);
                        logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, success.getText(), txn_ref_no, narration, "Y"); //Y indicates its a Local FT

                    } else {
                        
                        try {
                            connect.rollback();
                        } catch (SQLException sqlExc) {
                            sqlExc.printStackTrace();
                        }
                        if (errCode != null && errCode.equals("EB-202")) {
                            instData.result = Utility.processError(instData, g);
                        } else {
                            instData.result = Utility.processError(instData, FUND_TRANSFER_ERROR_MESSAGE);
                        }
                        
                        logFtTransactions(instData.requestId, instData.userName, instData.userId, instData.custId, ftdebitacc, DestAcctNo, amt, "Local FT Failed", txn_ref_no, narration, "Y"); //Y indicates is Local FT while N indicates is 3rd Party

                    }
                } else {
                    updateToken(instData.userId); //force the token to expire after each submission of ft
                    instData.result = Utility.processError(instData, MISMATCH_TOKEN);
                }
            } else {
                System.out.println("Action on Local Ft : " + action);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);

            return instData.result;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    public String logFtTransactions(String requestId, String userName, String userId, String custId, String ftdebitacc, String DestAcctNo, String amt, String failiure, String Txn_ref_no, String narration, String LFTflag) {
        String query = "insert into fundtransfer_log (Request_id,Account_name,User_Id ,Customer_Id ,Ft_Debit_Acc ,Dest_Acct_No ,Amount ,Message, Transaction_Time,txn_ref_no,narration,lft,ID)values(?,?,?,?,?,?,?,?,?,?,?,?,ft_sequence.nextval)";
        String failedFtResult = null;
        System.out.println("About to log successful Local FT >>>>>> " + requestId);

        // 20150112  report
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            con.setAutoCommit(false);
            stmt = con.prepareStatement(query);

            stmt.setString(1, requestId);
            stmt.setString(2, userName);
            stmt.setString(3, userId);
            stmt.setString(4, custId);
            stmt.setString(5, ftdebitacc);
            stmt.setString(6, DestAcctNo);
            stmt.setString(7, amt);
            stmt.setString(8, failiure);
            stmt.setString(9, Utility.getsqlCurrentDate2());
            stmt.setString(10, Txn_ref_no);
            stmt.setString(11, narration);
            stmt.setString(12, LFTflag);
            stmt.execute();
            System.out.println("about to commit");
            con.commit();

            System.out.print("the date of transaction is >>>>>" + Utility.getsqlCurrentDate2());
            System.out.print("transaction was initiated by >>>>>" + userName);

        } catch (Exception ex) {
            ex.printStackTrace();

            // return instData.result;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        System.out.println("About to log failed Local FT >>>>>> " + failiure);
        return failedFtResult;
        //  return instData.result;
    }

    private void updateToken(String userId) {
        System.out.println("user id for updating token: " + userId);
        Connection con = null;
        PreparedStatement stmt = null;
        String sqlString = "update sm_user_access set pin_to_expire = ? where cod_usr_id = ?";
        boolean result = false;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            stmt = con.prepareStatement(sqlString);

            stmt.setString(1, "0");
            stmt.setString(2, userId);
            result = stmt.execute();

        } catch (Exception e) {
            System.out.println("result after updating token: " + result);
            System.err.println("error updating token >>> " + e.getMessage());
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
