
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;

import oracle.jdbc.OracleTypes;

import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class closeSI2 extends Processes {
    private static final int    NBR_PARAMS         = 8;
    private static final String PROCEDURE_SI_CLOSE = "ebanking_si.closesi";

    // String acctno;
    public static final String SUCCESS  = "false";
    String                     procName = "";
    private String             account;
    private String             branchCode;
    private String             corebankinguserid;
    protected String           errorCode;
    protected String           errorType;
    protected String           errormsg;
    private String             instno;
    private String             userID;
    private String             returnResult;
    private String             rsetSiInq;
    SIObject                   siobject;

    public closeSI2() {}

    protected String extractOutputValues() throws SQLException {
        String map = null;

        return map;
    }

    public void initializeVar() {
        this.errorType    = null;
        this.errorCode    = null;
        this.errormsg     = null;
        this.rsetSiInq    = null;
        this.returnResult = null;
        this.account      = null;
        this.branchCode   = null;
        this.instno       = null;
        corebankinguserid = null;    //
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();

        try {
            corebankinguserid = (String) LoadBasicParam.getDataValuenew("USERID");
            this.userID = instData.userId;
            setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            instData.result = ExchangeMap(connect);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (procStmt != null) {
                try {
                    procStmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.instno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "instno", 0, false,
                null);
        this.branchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acct_branch", 0,
                false, null);
    }

    @Override
    protected HashMap extractOutputValues(String input) throws SQLException {
        HashMap result = new HashMap();

        rsetSiInq = procStmt.getString(1);

        java.sql.Clob datas = procStmt.getClob(5);
        String        data  = datas.getSubString(1, (int) datas.length());

        returnResult = data;
        System.out.println("This is what I want " + returnResult);

        // }
        errorCode = procStmt.getString(6);
        errorType = procStmt.getString(7);
        errormsg  = procStmt.getString(8);

        org.jdom.Element results = new org.jdom.Element("RESULT");

        // Element transdetaildr = new Element("Transdetaildr");
        Element statuse = new Element("RETURNRESULT");

        statuse.setText(returnResult);

        Element errorcodee = new Element("ERROCODE");

        errorcodee.setText(errorCode);

        Element errorParametere = new Element("ERRORTYPE");

        errorParametere.setText(errorType);

        Element errorMsge = new Element("ERRORMESSAGE");

        errorMsge.setText(errormsg);
        results.addContent(statuse);
        results.addContent(errorcodee);
        results.addContent(errorParametere);
        results.addContent(errorMsge);

        org.jdom.Document doc       = new org.jdom.Document(results);
        XMLOutputter      outputter = new XMLOutputter();

        outputter.setFormat(Format.getPrettyFormat());

        String  xmlText = outputter.outputString(doc);
        HashMap resultx = new HashMap();

        resultx.put("returnResult", xmlText);

        if (bDebugOn) {
            System.out.println(result);
        }

        return resultx;
    }

    @Override
    protected String getProcedureParamString() {
        return buildProcedureStatement(PROCEDURE_SI_CLOSE, NBR_PARAMS);
    }

    /*
     * public HashMap getSI(){
     * HashMap map = new HashMap();
     * map = ExchangeMap();
     * return map;
     * }
     */
    @Override
    protected void prepareExecProcedure(Connection con) throws Exception {
        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt      = con.prepareCall(l_stmt_string);
        registerOutParameters();
    }

    @Override
    protected String executeProcedure() throws SQLException {
        System.out.println("Account : " + account);
        procStmt.setString(2, corebankinguserid);
        procStmt.setString(3, branchCode);
        procStmt.setString(4, instno);
        procStmt.setString(9, userID);
        procStmt.execute();

        return procStmt.getString(1);
    }

    protected void registerOutParameters() throws SQLException {
        procStmt.registerOutParameter(1, Types.VARCHAR);
        procStmt.registerOutParameter(5, OracleTypes.CLOB);
        procStmt.registerOutParameter(6, Types.VARCHAR);
        procStmt.registerOutParameter(7, Types.VARCHAR);
        procStmt.registerOutParameter(8, Types.VARCHAR);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
