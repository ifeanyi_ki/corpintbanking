
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import oracle.jdbc.OracleTypes;

//~--- JDK imports ------------------------------------------------------------

import java.sql.CallableStatement;
import java.sql.Connection;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class GetCustInfo extends ProcessClass {
    private Connection        connect;
    private String            custno;
    private String            custtype;
    private CallableStatement pstmt;

    public GetCustInfo() {}

    protected void initialiseVar() {
        custno   = null;
        custtype = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        this.custno = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "customerno", 0, true, null), "UTF-8");
        custtype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "customerno", 0, true, null), "UTF-8");

        if (bDebugOn) {
            System.out.println(custno + " ==== This is the customer number =====");
        }
    }

    @Override
    public HashMap processRequest(DataofInstance instData) {
        initialiseVar();

        try {
            this.setInstanceVar(instData);
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            pstmt = connect.prepareCall("{?=call getcustinfo(?,?,?,?,?,?)}");
            pstmt.registerOutParameter(1, OracleTypes.INTEGER);
            pstmt.registerOutParameter(5, OracleTypes.VARCHAR);
            pstmt.registerOutParameter(6, OracleTypes.VARCHAR);
            pstmt.registerOutParameter(7, OracleTypes.CLOB);
            System.out.println(custno + " This is the custno");
            pstmt.setString(2, custno);
            pstmt.setString(3, instData.userId);
            pstmt.setString(4, instData.sessionId);

            // pstmt.setString(5, custtype);
            pstmt.execute();

            int    result   = pstmt.getInt(1);
            String err_code = pstmt.getString(6);
            String err_msg  = pstmt.getString(5);

            System.out.println(err_code + "  error " + err_msg);

            if (result != 0) {
                if (err_code != null) {
                    instData.result = Utility.processError(instData, err_code);

                    return instData.result;
                }

//              instData.result = Utility.processError(instData, ERR_ACCTNOISNULL);   
//               return instData.result;
            } else {
                HashMap hashMap = new HashMap();
                String  data    = ManageClob(pstmt.getClob(7));    // datas.getSubString(1, (int)datas.length());

                hashMap.put(RESULT_MAP_KEY, data);
                instData.result = hashMap;
            }
        } catch (Exception ex) {
            instData.result = Utility.processError(instData, GENERAL_ERROR_MESSAGE);
            ex.printStackTrace();

            return instData.result;
        } finally {
            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
