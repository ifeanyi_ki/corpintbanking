
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- JDK imports ------------------------------------------------------------

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@XmlRootElement(name = "QUERY_ACCSTAT")
public class SiProductVO {
    private String actioncodeamount;
    private String calholexcp;
    private int    execdays;
    private int    execmths;
    private int    execyrs;
    private String instructioncode;
    private int    maxretrycount;
    private String minsweepamt;
    private String processingtime;

    // for product definition
    private String productcode;
    private String producttype;
    private String ratetype;
    private String referralrequired;
    private String sitype;
    private String transfertype;

    /**
     * @return the productcode
     */
    public String getProductcode() {
        return productcode;
    }

    /**
     * @param productcode the productcode to set
     */
    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    /**
     * @return the producttype
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * @param producttype the producttype to set
     */
    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    /**
     * @return the ratetype
     */
    public String getRatetype() {
        return ratetype;
    }

    /**
     * @param ratetype the ratetype to set
     */
    public void setRatetype(String ratetype) {
        this.ratetype = ratetype;
    }

    /**
     * @return the execdays
     */
    public int getExecdays() {
        return execdays;
    }

    /**
     * @param execdays the execdays to set
     */
    public void setExecdays(int execdays) {
        this.execdays = execdays;
    }

    /**
     * @return the execmths
     */
    public int getExecmths() {
        return execmths;
    }

    /**
     * @param execmths the execmths to set
     */
    public void setExecmths(int execmths) {
        this.execmths = execmths;
    }

    /**
     * @return the execyrs
     */
    public int getExecyrs() {
        return execyrs;
    }

    /**
     * @param execyrs the execyrs to set
     */
    public void setExecyrs(int execyrs) {
        this.execyrs = execyrs;
    }

    /**
     * @return the calholexcp
     */
    public String getCalholexcp() {
        return calholexcp;
    }

    /**
     * @param calholexcp the calholexcp to set
     */
    public void setCalholexcp(String calholexcp) {
        this.calholexcp = calholexcp;
    }

    /**
     * @return the maxretrycount
     */
    public int getMaxretrycount() {
        return maxretrycount;
    }

    /**
     * @param maxretrycount the maxretrycount to set
     */
    public void setMaxretrycount(int maxretrycount) {
        this.maxretrycount = maxretrycount;
    }

    /**
     * @return the minsweepamt
     */
    public String getMinsweepamt() {
        return minsweepamt;
    }

    /**
     * @param minsweepamt the minsweepamt to set
     */
    public void setMinsweepamt(String minsweepamt) {
        this.minsweepamt = minsweepamt;
    }

    /**
     * @return the processingtime
     */
    public String getProcessingtime() {
        return processingtime;
    }

    /**
     * @param processingtime the processingtime to set
     */
    public void setProcessingtime(String processingtime) {
        this.processingtime = processingtime;
    }

    /**
     * @return the instructioncode
     */
    public String getInstructioncode() {
        return instructioncode;
    }

    /**
     * @param instructioncode the instructioncode to set
     */
    public void setInstructioncode(String instructioncode) {
        this.instructioncode = instructioncode;
    }

    /**
     * @return the sitype
     */
    public String getSitype() {
        return sitype;
    }

    /**
     * @param sitype the sitype to set
     */
    public void setSitype(String sitype) {
        this.sitype = sitype;
    }

    /**
     * @return the transfertype
     */
    public String getTransfertype() {
        return transfertype;
    }

    /**
     * @param transfertype the transfertype to set
     */
    public void setTransfertype(String transfertype) {
        this.transfertype = transfertype;
    }

    /**
     * @return the referralrequired
     */
    public String getReferralrequired() {
        return referralrequired;
    }

    /**
     * @param referralrequired the referralrequired to set
     */
    public void setReferralrequired(String referralrequired) {
        this.referralrequired = referralrequired;
    }

    /**
     * @return the actioncodeamount
     */
    public String getActioncodeamount() {
        return actioncodeamount;
    }

    /**
     * @param actioncodeamount the actioncodeamount to set
     */
    public void setActioncodeamount(String actioncodeamount) {
        this.actioncodeamount = actioncodeamount;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
