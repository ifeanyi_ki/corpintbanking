/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author Administrator
 */
public class AddFT extends Processes {

    boolean bDebugOn = true;
    private Connection con;
    private String productcode = null;
    private String counterparty = null;
    private String siSourceAcct = null;
    private String siDestAccount = null;
    private String siAmount = null;
    private int siFreqExec = 0;
    private Date siFirstExecDate = null;
    private Date siFinalExecDate = null;
    private String siNarration = null;
    private String siCurrency = null;
    private String siPriority = null;
    private String drBranchCode = null;
    private String crBranchCode = null;
    private String drCcy = null;
    private String crCcy = null;
    private final String PROCEDURE_SI_UPLOAD = "ebanking_ft.ft_ebupload";
    protected CallableStatement procStmt;
    private static final int NBR_PARAMS = 8;
    private static final String CSTBS_EXT_CONTRACT_STAT_QRY="insert into cstbs_ext_contract_stat"+
            "(" +
               "BRANCH_CODE          "+
   "SOURCE               "+
   "PRODUCT_CODE         "+
   "COUNTERPARTY         "+
   "EXTERNAL_INIT_DATE   "+
   "MODULE               "+
   "EXTERNAL_REF_NO      "+
   "IMPORT_STATUS        "+
   "CITICUBE_REF_NO      "+
   "POST_IMPORT_STATUS   "+
   "EXPORT_STATUS        "+
   "USER_ID              "+
   "JOBNO                "+
   "CONTRACT_REF_NO      "+
   "ERR_CODE             "+
   "ERR_MESSAGE          "+
   "ACTION_CODE          "+
    ") values(?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

    private static final String FTTB_UPLOAD_MASTER_QRY = " insert into fttb_upload_master " +
            "(" +
            "SOURCE_CODE        " +
            "SOURCE_REF         " +
            "FT_CONTRACT_REF    " +
            "BRANCH_CODE        " +
            "USER_REF_NO        " +
            "FT_TYPE            " +
            "MSG_COVER          " +
            "MSG_AS_OF          " +
            "RATE_AS_OF         " +
            "AFTER_RATE_CHANGE  " +
            "RATE_TYPE          " +
            "SPREAD_CODE        " +
            "EXCHANGE_RATE      " +
            "DR_BRANCH          " +
            "DR_ACCOUNT         " +
            "DR_CCY             " +
            "DR_AMOUNT          " +
            "DR_VALUE_DATE      " +
            "CR_BRANCH          " +
            "CR_ACCOUNT         " +
            "CR_CCY             " +
            "CR_AMOUNT          " +
            "CR_VALUE_DATE      " +
            "MCK_NUMBER         " +
            "CHECK_NUMBER       " +
            "BY_ORDER_OF1       " +
            "BY_ORDER_OF2       " +
            "BY_ORDER_OF3       " +
            "BY_ORDER_OF4       " +
            "ULTIMATE_BEN1      " +
            "ULTIMATE_BEN2      " +
            "ULTIMATE_BEN3      " +
            "ULTIMATE_BEN4      " +
            "INT_REIM_INST1     " +
            "INT_REIM_INST2     " +
            "INT_REIM_INST3     " +
            "INT_REIM_INST4     " +
            "INTERMEDIARY1      " +
            "INTERMEDIARY2      " +
            "INTERMEDIARY3      " +
            "INTERMEDIARY4      " +
            "RECVR_CORRES1      " +
            "RECVR_CORRES2      " +
            "RECVR_CORRES3      " +
            "RECVR_CORRES4      " +
            "ACC_WITH_INST1     " +
            "ACC_WITH_INST2     " +
            "ACC_WITH_INST3     " +
            "ACC_WITH_INST4     " +
            "SNDR_TO_RECVR_INFO1" +
            "SNDR_TO_RECVR_INFO2" +
            "SNDR_TO_RECVR_INFO3" +
            "SNDR_TO_RECVR_INFO4" +
            "SNDR_TO_RECVR_INFO5" +
            "SNDR_TO_RECVR_INFO6" +
            "PAYMENT_DETAILS1   " +
            "PAYMENT_DETAILS2   " +
            "PAYMENT_DETAILS3   " +
            "PAYMENT_DETAILS4   " +
            "ORDERING_INST1     " +
            "ORDERING_INST2     " +
            "ORDERING_INST3     " +
            "ORDERING_INST4     " +
            "BENEFICIARY_INST1  " +
            "BENEFICIARY_INST2  " +
            "BENEFICIARY_INST3  " +
            "BENEFICIARY_INST4  " +
            "UPLOAD_STATUS      " +
            "APPLY_ICCF         " +
            "CHARGE_WHOM        " +
            "APPLY_TAX          " +
            "APPLY_SETTLEMENTS  " +
            "PASS_ACC_ENTRIES   " +
            "INTERNAL_REMARKS   " +
            "PRODUCT_CODE       " +
            "LCY_EQUIVALENT     " +
            "ERI_CCY            " +
            "ERI_AMOUNT         " +
            "RECEIVER           " +
            "BENEFICIARY_INST5  " +
            "ORDERING_INST5     " +
            "UPLOAD_MSG_TYPE    " +
            "TIME_RECEIVED      " +
            "DCN                " +
            "ULTIMATE_BEN5      " +
            "INT_REIM_INST5     " +
            "INTERMEDIARY5      " +
            "RECVR_CORRES5      " +
            "ACC_WITH_INST5     " +
            "AMOUNT_BLOCK_NO    " +
            "ACCTING_AS_OF      " +
            "ACCOUNTING_DATE    " +
            "EX_RATE_DATE       " +
            "EX_RATE_SERIAL     " +
            "FX_CONTRACT_REF_NO " +
            "AUTO_AUTH_FT       " +
            ")VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    public AddFT() {
    }

    public boolean loadData(DataofInstance instdata) {
        boolean result = false;
        PreparedStatement pstmt = null;
        int isUpdate = 0;
        String constr = null;
        String serial = this.getSerial();
        String sourceRef=this.getSourceRefNo(serial);

        try {
            con = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            con.setAutoCommit(false);
            pstmt = con.prepareStatement(FTTB_UPLOAD_MASTER_QRY);
            pstmt.setString(1, (String) instdata.paramMap.get("FTSOURCECODE"));
            pstmt.setString(2,sourceRef );
            pstmt.setString(3, "");
            pstmt.setString(4, drBranchCode);
            pstmt.setString(5, "");
//            pstmt.setString(6, instdata.paramValue.getProducttype());
//            pstmt.setString(7, instdata.paramValue.getCoverrequired());
//            pstmt.setString(8, instdata.paramValue.getMessagecode());
            pstmt.setString(9, "");
            pstmt.setString(10, "N");
//            pstmt.setString(11, "");//should be null for non cross currenct rate
//            pstmt.setString(12, instdata.paramValue.getSpreadcode());
            pstmt.setString(13, "");
            pstmt.setString(14, this.drBranchCode);
            pstmt.setString(15, this.siSourceAcct);
            pstmt.setString(16, this.drCcy);
            pstmt.setString(17, "");//debit amount can not be null for non cros currwncy
            pstmt.setString(18, "");//debut value date not yet clear
            pstmt.setString(19, this.crBranchCode);
            pstmt.setString(20, this.siDestAccount);
            pstmt.setString(21, this.crCcy);
            pstmt.setString(22, this.siAmount);//this is the credit amount
            pstmt.setString(23, "");//crvaluedtae not yet clear
            pstmt.setString(24, "");
            pstmt.setString(25, "");//need to understand better order of
            pstmt.setString(26, "");
            pstmt.setString(27, "");
            pstmt.setString(28, "");
            pstmt.setString(29, "");
            pstmt.setString(30, "100095");//need to get the custid for account we are crediting
            pstmt.setString(31, "");
            pstmt.setString(32, "");
            pstmt.setString(33, "");
            pstmt.setString(34, "");
            pstmt.setString(35, "");
            pstmt.setString(36, "");
            pstmt.setString(37, "");
            pstmt.setString(38, "");
            pstmt.setString(39, "");
            pstmt.setString(40, "");
            pstmt.setString(41, "");
            pstmt.setString(42, "");
            pstmt.setString(43, "");
            pstmt.setString(44, "");
            pstmt.setString(45, "");
            pstmt.setString(46, "");
            pstmt.setString(47, "");
            pstmt.setString(48, "");
            pstmt.setString(49, "");
            pstmt.setString(50, "");
            pstmt.setString(51, "");
            pstmt.setString(52, "");
            pstmt.setString(53, "");
            pstmt.setString(54, "");
            pstmt.setString(55, "");
            pstmt.setString(56, "");
            pstmt.setString(57, "");
            pstmt.setString(58, "");
            pstmt.setString(59, "");
            pstmt.setString(60, "");
            pstmt.setString(61, "");
            pstmt.setString(62, "");
            pstmt.setString(63, "");
            pstmt.setString(64, "");
            pstmt.setString(65, "");
            pstmt.setString(66, "");
            pstmt.setString(67, "");
            pstmt.setString(68, "A");
            pstmt.setString(69, "Y");
            pstmt.setString(70, "Y");
            pstmt.setString(71, "Y");
            pstmt.setString(72, "Y");
            pstmt.setString(73, "Y");
            pstmt.setString(74, "");
            pstmt.setString(75, "");
            pstmt.setString(76, "");
            pstmt.setString(77, "");
            pstmt.setString(78, "");
            pstmt.setString(79, "");
            pstmt.setString(80, "");
            pstmt.setString(81, "");
            pstmt.setString(82, "");
            pstmt.setString(83, "");
            pstmt.setString(84, "");
            pstmt.setString(85, "");
            pstmt.setString(86, "");
            pstmt.setString(87, "");
            pstmt.setString(88, "");
            pstmt.setString(89, "");
            pstmt.setString(90, "");
            pstmt.setString(91, "");
            pstmt.setString(92, "");
            pstmt.setString(93, "");
            pstmt.setString(94, "");
            pstmt.setString(95, "");
            pstmt.setString(96, "");
            isUpdate = pstmt.executeUpdate();
                System.out.println("=====FTTB_upload_master updated" + isUpdate);

                if (isUpdate != 0 && isUpdate > 0){
            pstmt = con.prepareStatement(CSTBS_EXT_CONTRACT_STAT_QRY);
            pstmt.setString(1, this.drBranchCode);
//            pstmt.setString(2, (String)instdata.paramMap.get("FTSOURCECODE"));
//            pstmt.setString(3,instdata.paramValue.getProducttype());
            pstmt.setString(4, this.counterparty);
            pstmt.setString(5, "FT");
            pstmt.setString(6, "");
            pstmt.setString(7, sourceRef);
            pstmt.setString(8, "U");
            pstmt.setString(9, "");
            pstmt.setString(10, "");
            pstmt.setString(11, "");//should be null for non cross currenct rate
            pstmt.setString(12, "SYSTEM");
            pstmt.setString(13, serial);
            pstmt.setString(14, "");
            pstmt.setString(15, "");
            pstmt.setString(16,"");
            pstmt.setString(17, "");
            isUpdate = pstmt.executeUpdate();
                }


        } catch (SQLException sql) {
                /*if (bDebugOn)*/ {
                    System.out.println("sql ; " + sql);
                    sql.printStackTrace();
                    try {
                        con.rollback();
                       // con2.rollback();
                    } catch (SQLException ssqle) {
                        /*if (bDebugOn)*/ {
                            System.out.println("sql:" + ssqle);
                            ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex);
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (Exception ex) {
                /*if (bDebugOn)*/ {
                    System.out.println("Exception ; " + ex);
                    ex.printStackTrace();
                    try {
                        con.rollback();
                        //con2.rollback();
                    } catch (Exception ex2) {
                        /*if (bDebugOn)*/ {
                            System.out.println("exception: " + ex2);
                            ex2.printStackTrace();
                        }
                    }
                }
            } finally {
                if (con != null /*|| con2 != null*/) {
                    try {
                        con.close();
                        //con2.close();
                    } catch (SQLException ssqle) {
                        if (bDebugOn) {
                            System.out.println("sql:" + ssqle);
                            ssqle.printStackTrace();
                        }
                    } catch (Exception ex) {
                        if (bDebugOn) {
                            System.out.println("exception: " + ex);
                            ex.printStackTrace();
                        }
                    }
                }
            }
        return result;

    }

    public void initializeVar() {
        /*this.SourceRef = null;
        this.endianesDate = null;
        this.procResult = null;
        this.userRef = null;
        this.siobject = null;
        this.serialresult = null;*/
        this.productcode = null;
        this.counterparty = null;
        this.siSourceAcct = null;
        this.siDestAccount = null;
        this.siAmount = null;
        this.siFreqExec = 0;
        this.siFirstExecDate = null;
        this.siFinalExecDate = null;
        this.siNarration = null;
        this.siCurrency = null;
        this.siPriority = null;
        this.drBranchCode = null;
        this.crBranchCode = null;
        this.crCcy = null;
        this.drCcy = null;

    }

    private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.productcode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.counterparty = inst_data.custId;//ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "fldSiType", 0, false, null);
        this.siSourceAcct = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "dSrcAcctNo", 0, false, null);
        this.siDestAccount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "DestAcctNo", 0, false, null);
        this.siAmount = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfAmt", 0, false, null);
        this.siFreqExec = Integer.parseInt(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFreqDesc", 0, false, "1"));
        this.siFirstExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "FirstExecDate", 0, false, null));
        this.siFinalExecDate = Utility.convertString2Date(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiFinalDate", 0, false, null));
        this.siNarration = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiNarration", 0, false, null);
        this.siCurrency = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiTrfCurr1", 0, false, null);
        this.siPriority = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "SiPriority", 0, false, null);
        this.drBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "accountBranch", 0, false, null);
        this.crBranchCode = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "crbrcode", 0, false, null);

    }

      private static synchronized String getSerial(){
         String result = "";
        String sequence = "select ft_extrefno.NEXTVAL from dual ";
        //Connection connect=com.fasyl.ebanking.db.DataBase.getConnection();
        String[] l_args = new String[0];
        ResultSet rs = null;
        Statement sel_txn_param = null;
        try{
        rs = sel_txn_param.executeQuery(
                    MessageFormat.format(
                    sequence, l_args));
        if (rs.next()){
           result=rs.getString(1);
        }

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
      }

     private String getSourceRefNo(String serialno){
         StringBuffer rets = new StringBuffer();
        String ret = "";
        int iResult = 0;
        rets.append(this.drBranchCode);//need to use the dr account branch

        rets.append(serialno);


        rets.append(Utility.EndianesDate());
        return rets.toString();

     }
    public HashMap processRequest(DataofInstance instData) {
        System.out.println(" ******Inside process request **** ");
        initializeVar();
        try {
            setInstanceVar(instData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        boolean loaddata = loadData(instData);
        
        if (loaddata) {
            try {
                con = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
                instData.result = ExchangeMap(con);
            }
            finally {
                
                if (procStmt != null) {
                    try {
                        procStmt.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                
               if (con != null) {
                   try {
                       con.close();
                       //con2.close();
                   } catch (SQLException ssqle) {
                       if (bDebugOn) {
                           System.out.println("sql:" + ssqle);
                           ssqle.printStackTrace();
                       }
                   } catch (Exception ex) {
                       if (bDebugOn) {
                           System.out.println("exception: " + ex);
                           ex.printStackTrace();
                       }
                   }
               }
            }
        } else {
        }


        return instData.result;

    }

    public String executeProcedure()
            throws SQLException {

        procStmt.setString(2, "SYSTEM");
        procStmt.setString(3, "SI_UPLOAD");//TO BE PICKED FROM THEDB
        procStmt.setString(4, "");
        procStmt.setString(5, "");
        //procStmt.setString(6,null );
        //procStmt.setString(7, null);
        //procStmt.setString(8, null);
        //procStmt.setString(9, null);


        procStmt.execute();

        return procStmt.getString(1);

    }

    protected void prepareExecProcedure(Connection con)
            throws Exception {



        String l_stmt_string = "";

        l_stmt_string = getProcedureString() + getProcedureParamString();
        procStmt = con.prepareCall(l_stmt_string);
        registerOutParameters();

    }

    protected String getProcedureParamString() {

        return buildProcedureStatement(
                PROCEDURE_SI_UPLOAD, NBR_PARAMS);

    }

    protected void registerOutParameters()
            throws SQLException {

        //if (!isMerantJdbcDriver) {
        procStmt.registerOutParameter(1, OracleTypes.VARCHAR);
        procStmt.registerOutParameter(6, OracleTypes.VARCHAR);
        // }
        procStmt.registerOutParameter(7, Types.VARCHAR);
        //procStmt.registerOutParameter (PARAM_IDX_CUST_ID, Types.INTEGER);
        procStmt.registerOutParameter(8, Types.VARCHAR);
        procStmt.registerOutParameter(9, Types.VARCHAR);

    }

    protected HashMap extractOutputValues(String type) throws SQLException {
        HashMap map = null;

        return map;

    }

    protected String extractOutputValues()
            throws SQLException {

        String result = null;

        String status = procStmt.getString(6);
        if (bDebugOn) {
            System.out.println(status);
        }
        String errorCode = procStmt.getString(7);
        if (bDebugOn) {
            System.out.println(errorCode);
        }
        String errorParameter = procStmt.getString(8);
        if (bDebugOn) {
            System.out.println(errorParameter);
        }
        String errorMsg = procStmt.getString(9);
        if (bDebugOn) {
            System.out.println(errorMsg);
        }

        org.jdom.Element results = new org.jdom.Element("RESULT");
        // Element transdetaildr = new Element("Transdetaildr");
        Element statuse = new Element("STATUS");
        statuse.setText(status);
        Element errorcodee = new Element("errorCode");
        errorcodee.setText(errorCode);
        Element errorParametere = new Element("errorParameter");
        errorParametere.setText(errorParameter);
        Element errorMsge = new Element("errorMsg");
        errorMsge.setText(errorMsg);
        results.addContent(statuse);
        results.addContent(errorcodee);
        results.addContent(errorParametere);
        results.addContent(errorMsge);
        org.jdom.Document doc = new org.jdom.Document(results);
        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(org.jdom.output.Format.getPrettyFormat());
        String xmlText = outputter.outputString(doc);
        /*try{
        DocumentBuilderFactory docBuilder = DocumentBuilderFactory.newInstance();
        DocumentBuilder  Builder = docBuilder.newDocumentBuilder();
        Document doc = Builder.newDocument();
        Element  el  = doc.createElement("RESULT");
        Element statuse = doc.createElement("STATUS");
        el.appendChild(doc.createTextNode(status));
        Element errorcodee = doc.createElement("ERRORCODE");
        el.appendChild(doc.createTextNode(errorCode));
        Element errorParametere = doc.createElement("ERRORPARAMETER");
        el.appendChild(doc.createTextNode(errorParameter));
        Element errorMsge = doc.createElement("ERRORMSG");
        el.appendChild(doc.createTextNode(errorMsg));

        XMLUtilty.getDocumentAsXml(doc);
        }catch(TransformerConfigurationException tr){

        }catch(TransformerException ex){

        }catch(ParserConfigurationException ps){

        }*/
        result = status;
        if (bDebugOn) {
            System.out.println(result);
        }
        return result;
    }
}
