
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

//~--- non-JDK imports --------------------------------------------------------
import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.util.Utility;

import org.jdom.Element;

//~--- JDK imports ------------------------------------------------------------
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.net.ExternalCall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baby
 */
public class initFT extends ProcessClass {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(initFT.class.getName());

    ExternalCall call = new ExternalCall();
    private static final String GET_ACCT_DETAIL = "select a.ac_desc,a.branch_code,c.cust_acct cust_ac_no,a.cust_no,a.ccy,fn_acct_bal2(a.cust_ac_no,a.branch_code) Balance,b.max_amount "
            + "from sttm_cust_account a,ft_setup b,cust_acct c where a.cust_no=? and a.ccy=b.ccy and a.cust_ac_no=c.cust_acct";
    private static final String GET_BENEFICIARY = "select distinct beneficiary_name,destination_acct,CCY from eb_beneft_details where customer_no=? and beneficiary_type=?";
    private static final String GET_BENEFICIARYGRP = "select distinct beneftgrpname from eb_beneft_details where customer_no=? and beneficiary_type=?";
    private static final String GET_BENEFICIARYGRP_THIRD_PARTY_FT = "select distinct beneficiary_name,destination_acct,CCY from eb_beneft_details where customer_no=? and (beneficiary_type=? or beneficiary_type=?)";
    private static final String GET_BANKS = "select bank_code, bank_name from BANK_LIST";
    private PreparedStatement pstmt = null;
    private String xmlText = null;
    DataofInstance instdata;
    private Element parent;
    private String flag;
    private String ben_acc_number;
    private String cusAcctNo;
    private String bankName;

    //  static NIPServices nip = null;
    // 20150112 reports
    private String startDate;
    private String endDate;
    private String trxtype = null;
    private String trftype;
    private String customerid;

    public initFT() {
    }

    protected void initializeVar() {
        pstmt = null;
        xmlText = null;
        flag = null;
        ben_acc_number = null;
        cusAcctNo = null;
        bankName = null;

        // 20150112 reports
        endDate = null;
        startDate = null;
        trxtype = null;
        trftype = null;
        customerid = null;
    }

    protected void setInstanceVar(DataofInstance inst_data) throws Exception {
        ReadRequestAsStream decodes = new ReadRequestAsStream();

        parent = new Element("ROWSET");

        flag = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "flag", 0, true, null), "UTF-8");

        System.out.println("flag " + ben_acc_number);

        ben_acc_number = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable,
                "ben_acc_no", 0, true, null), "UTF-8");

        System.out.println("ben_acc_number " + ben_acc_number);
        cusAcctNo = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "accttodebt", 0,
                true, null), "UTF-8");
        System.out.println("cusAcctNo " + cusAcctNo);
        bankName = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "ben_bank",
                0, true, null), "UTF-8");
        System.out.println("bankName " + bankName);

        // 20150112 reports
        endDate = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "to", 0,
                true, null), "UTF-8");
        startDate = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "from", 0,
                true, null), "UTF-8");
        trxtype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "trxtype",
                0, true, null), "UTF-8");
        trftype = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "trftype",
                0, true, null), "UTF-8");
        customerid = decodes.decode(ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "customer_id",
                0, true, null), "UTF-8");

        logger.info(" customer id: " + customerid);

        if (customerid != null) {
            customerid = customerid.toUpperCase();
            logger.info(" customer id: " + customerid);
        } else {
            logger.info(" customer id is null ");
        }

        System.out.println("fund transaction type is " + trftype);

    }

    @Override
    public HashMap processRequest(DataofInstance instData) {

        try {
            ResultSet rs = null;
            boolean result = false;

            this.initializeVar();
            setInstanceVar(instData);
            instdata = instData;

            if (bDebugOn) {
                System.out.println("==== inside process request try block ========");
            }

            this.setInstanceVar(instData);

            ArrayList list = new ArrayList();

            list.add("332");
            list.add("7021");
            list.add("70002");
            list.add("333");
            list.add("334");
            list.add("7014");

            list.add("7015");
            if (flag == null) {

                if (list.contains(instData.taskId)) {
                    if (instData.taskId.equals("7021") || instData.taskId.equals("70002")) {
                        this.getBeneficiary("GRP");
                    } else if (instData.taskId.equals("332")) {
                        System.out.println("*** about to get beneficiaries for 3rd party ft ***");
                        System.out.println("instData.taskId.equals(\"332\")" + instData.taskId.equals("332"));
                        this.getBeneficiaryForThirdPartFT(); //added by K.I to take care of FT to Corporate accts
                    } else {
                        this.getBeneficiary("IND");
                    }

                    this.getAcctDetail();
                    this.getSetupdetail();

                    // 20150112 report
                    //if (instData.taskId.equals("5002")) {
                    //Next line modified by Ayo after report patch
                    if (instData.taskId.equals("5002") || instData.taskId.equals("7015")) {
                        this.getBankDetail();

                    }
                } else if (instData.taskId.equals("7014")) { //Readded by Ayo after report patch
                    this.getAcctDetail();

                } else if (instData.taskId.equals("33112")) {
                    // this.ViewSetup();
                } else if (instData.taskId.equals("5003") && trftype.equals("LOCFT")) {
                    //  this.getAcctDetail();

                    this.getFundtransactionDetail();

                } else if (instData.taskId.equals("5003") && trftype.equals("INTFT")) {

                    this.getFundtransactionDetailForInterBank();

                } else if (instData.taskId.equals("5002")) {

                    this.getAlltransactions();

                } else if (instData.taskId.equals("50081")) {
                    getReportGeneral();

                } //else if added by becky for Si report patch
                else if (instData.taskId.equals("5013")) {
                    getReportGeneralForSi();
                }

                // if (bDebugOn)System.out.println(" ===== This is the parent tag ======= "+parent.toString());
                if ((parent == null)) {
                    result = false;
                    instData.result = Utility.processError(instData, APP_ERROR_SEARCH);

                    return instData.result;
                } else {
                    result = true;

                    if (instData.result == null) {
                        instData.result = generateXML(parent, this.xmlText);

                        if (!result
                                || instData.result.get(RESULT_MAP_KEY).equals("<?xml version=" + "1.0" + " encoding="
                                + "UTF-8" + "?> <ROWSET />")) {
                            instData.result = Utility.processError(instData, APP_ERROR_SEARCH);
                        }
                    }
                }
            } else {

                String flag = "flag";
                this.getAcctDetail();
                this.getSetupdetail();
                this.getBankDetail();

                System.out.println("about to call NIPS for beneficiary name >>>>");
                System.out.println("cusAcctNo: " + cusAcctNo);
                System.out.println("ben_acc_number: " + ben_acc_number);
                System.out.println("bankName: " + bankName);
                String getbeneficiaryName
                        = call.getBeneficiaryName(cusAcctNo, ben_acc_number, bankName);

                System.out.println("beneficiary account name is >>>>>>" + getbeneficiaryName);
                if (getbeneficiaryName == null || "".equals(getbeneficiaryName)) {
                    System.out.println("ERROR ESSAGE IS " + getbeneficiaryName);
                    getbeneficiaryName = "NotFound";
                    flag = "test";
                }
                if (getbeneficiaryName.equals("failed") || "failed".equals(getbeneficiaryName)) {
                    System.out.println("ERROR ESSAGE IS " + getbeneficiaryName);
                    getbeneficiaryName = "NotFound2";
                    flag = "test";
                }
                System.out.println("beneficiary account name is >>>>>>" + getbeneficiaryName);
                Element row = new Element("ROW");

                Element test = new Element("FLAG");
                test.setText(flag);
                row.addContent(test);

                Element tokens = new Element("ACCNAME");
                tokens.setText(getbeneficiaryName);
                row.addContent(tokens);

                Element customeraccount = new Element("ACCOUNT_NUMBER");
                customeraccount.setText(this.cusAcctNo);
                row.addContent(customeraccount);

                Element beneaccount = new Element("BENE_ACCOUNT_NUMBER");
                beneaccount.setText(this.ben_acc_number);
                row.addContent(beneaccount);

                Element benaccountBank = new Element("BENEFICIARY_BANK");
                benaccountBank.setText(getTheBankName(this.bankName));
                row.addContent(benaccountBank);

                Element benaccountBankcode = new Element("BENEFICIARY_BANK_CODE");
                benaccountBankcode.setText(this.bankName);
                row.addContent(benaccountBankcode);

                parent.addContent(row);

                instData.result = generateXML(parent, this.xmlText);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return instData.result;

    }
    
    private void getBeneficiaryForThirdPartFT() {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            System.out.println("instdata.custId.trim(): " + instdata.custId.trim());
            pstmt = connect.prepareStatement(GET_BENEFICIARYGRP_THIRD_PARTY_FT);
            pstmt.setString(1, instdata.custId.trim());
            pstmt.setString(2, "IND");
            pstmt.setString(3, "GRP");
            
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "BENEFICIARY", rs);

            if (parent == null) {
                parent = new Element("ROWSET");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    private void getBeneficiary(String type) {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            if (type.equals("GRP")) {
                pstmt = connect.prepareStatement(GET_BENEFICIARYGRP);
                pstmt.setString(1, instdata.custId.trim());
                pstmt.setString(2, type.trim());
            } else {
                pstmt = connect.prepareStatement(GET_BENEFICIARY);
                pstmt.setString(1, instdata.custId.trim());
                pstmt.setString(2, type.trim());
            }

            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "BENEFICIARY", rs);

            if (parent == null) {
                parent = new Element("ROWSET");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void getAcctDetail() {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            pstmt = connect.prepareStatement(GET_ACCT_DETAIL);
            pstmt.setString(1, instdata.custId);
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "ROW", rs);

            if (parent == null) {
                parent = new Element("ROWSET");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void getSetupdetail() {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            pstmt = connect.prepareStatement("select * from ft_setup");
            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "CCY_SETUP", rs);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void getBankDetail() {
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            pstmt = connect.prepareStatement(GET_BANKS);

            rs = pstmt.executeQuery();
            parent = this.convertToXml(parent, "BANKS", rs);

            if (parent == null) {
                parent = new Element("ROWSET");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private String getTheBankName(String theCode) {

        List namecode_pair = new ArrayList();;
        String bankCode = null;
        String bankName = null;
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        boolean result = false;

        try {
            pstmt = connect.prepareStatement("select bank_name from BANK_LIST where bank_code=?");
            pstmt.setString(1, theCode);
            rs = pstmt.executeQuery();

            while (rs.next()) {

                bankName = rs.getString("bank_name");

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return bankName;

    }

    public void getFundtransactionDetail() {

        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        String query = null;

        boolean result = false;
        if (trxtype.equals("FLDFT")) {
            query = "select request_id,account_name,user_id,ft_debit_acc,dest_acct_no,"
                    + "amount,message,transaction_time from fundtransfer_log a "
                    + "where a.Transaction_Time between to_date( '" + startDate + "','dd-mon-yyyy') "
                    + " and  to_date('" + endDate + "','dd-mon-yyyy') + 1 and message!='Your Transfer was successfully done' and ft_debit_acc='" + cusAcctNo + "'";

        } else {
            query = "select request_id,account_name,user_id,ft_debit_acc,dest_acct_no,"
                    + "amount,message,transaction_time from fundtransfer_log a "
                    + "where a.Transaction_Time between to_date( '" + startDate + "','dd-mon-yyyy')  "
                    + "and  to_date('" + endDate + "','dd-mon-yyyy') + 1 and message = 'Your Transfer was successfully done'and ft_debit_acc='" + cusAcctNo + "'";
        }
        System.out.println("query is " + query);
        try {
            pstmt = connect.prepareStatement(query);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }
            System.out.println("The parents is " + parent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void getFundtransactionDetailForInterBank() {

        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        String query = null;

        boolean result = false;
        if (trxtype.equals("FLDFT")) {
            query = "select * from Nip_failed_transactions a where a.Transaction_Time between to_date( '" + startDate + "','dd-mon-yyyy')  and  to_date('" + endDate + "','dd-mon-yyyy') + 1 ";

        } else {

            query = "select * from nip_successful_transactions a where a.Transaction_Time between to_date( '" + startDate + "','dd-mon-yyyy')  and  to_date('" + endDate + "','dd-mon-yyyy') + 1 ";

        }
        System.out.println("query is " + query);
        try {
            pstmt = connect.prepareStatement(query);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }
            System.out.println("The parents is " + parent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void getReportGeneral() {

        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        String query = null;
        boolean result = false;
        query = "select txn_id , user_id , dattxn ,txn_desc,remote_address,process_status, customer_type from auditlog a where a.dattxn between to_date( '" + startDate + "','dd-mon-yyyy')  and  to_date('" + endDate + "','dd-mon-yyyy') + 1 and user_id='" + customerid + "'";

        System.out.println("query is " + query);
        try {
            pstmt = connect.prepareStatement(query);
            // pstmt.setString(1, instdata.custId);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "ROW", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }
            System.out.println("The parents is " + parent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    //added by becky for Si report patch
    public void getReportGeneralForSi() {

        Connection connect = null;
        ResultSet rs = null;
        String query = null;

        query = "select * from eb_si_log a where (a.ADDED_DATE between to_date( '" + startDate + "','dd-mon-yyyy')"
                + "  and  to_date('" + endDate + "','dd-mon-yyyy') + 1 ) AND a.USER_ID ='" + customerid.toUpperCase() + "'";

        System.out.println("query is XXX" + query);
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            pstmt = connect.prepareStatement(query);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "ROW", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }
            System.out.println("The parents is " + parent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void getAlltransactions() {

        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;
        String query = null;
        boolean result = false;
        query = "select * from auditlog a where a.dattxn between to_date( '" + startDate + "','dd-mon-yyyy')  and  to_date('" + endDate + "','dd-mon-yyyy') + 1 ";
        System.out.println("query is " + query);
        try {
            pstmt = connect.prepareStatement(query);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }
            System.out.println("The parents is " + parent);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public HashMap getFundtransactionDetailForInterbankExport(String from, String to, String txtp) {
        parent = new Element("ROWSET");
        String query = null;
        Connection connect = com.fasyl.ebanking.db.DataBase.getConnection();
        ResultSet rs = null;

        boolean result = false;
        System.out.println("THE TRANSACTION TYPE IS " + txtp);
        if (txtp.equals("FLDFT")) {
            query = "select * from Nip_failed_transactions a where a.Transaction_Time between to_date( '" + from + "','dd-mon-yyyy')  and  to_date('" + to + "','dd-mon-yyyy') + 1 ";

        } else {

            query = "select * from nip_successful_transactions a where a.Transaction_Time between to_date( '" + from + "','dd-mon-yyyy')  and  to_date('" + to + "','dd-mon-yyyy') + 1 ";

        }
        try {
            System.out.println("QUERY  IS " + query);
            pstmt = connect.prepareStatement(query);

            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }

            HashMap map = generateXML(parent, this.xmlText);
            if (map != null) {
                System.out.println("The parents is " + parent);

                return map;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }

    public HashMap getFundtransactionDetailForExport(String from, String to, String txtp) {
        parent = new Element("ROWSET");
        String query = null;
        Connection connect = null;
        ResultSet rs = null;

        boolean result = false;
        System.out.println("THE TRANSACTION TYPE IS " + txtp);
        if (txtp.equals("FLDFT")) {
            query = "select request_id,account_name,user_id,ft_debit_acc,dest_acct_no,amount,message,transaction_time from fundtransfer_log a where a.Transaction_Time between to_date( '" + from + "','dd-mon-yyyy')  and  to_date('" + to + "','dd-mon-yyyy') + 1 and message!='Your Transfer was successfully done'";

        } else {
            query = "select request_id,account_name,user_id,ft_debit_acc,dest_acct_no,amount,message,transaction_time from fundtransfer_log a where a.Transaction_Time between to_date( '" + from + "','dd-mon-yyyy')  and  to_date('" + to + "','dd-mon-yyyy') + 1 and message = 'Your Transfer was successfully done'";
        }
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            System.out.println("QUERY  IS " + query);
            pstmt = connect.prepareStatement(query);

            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }

            HashMap map = generateXML(parent, this.xmlText);
            if (map != null) {
                System.out.println("The parents is " + parent);

                return map;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }

    //added by becky for Si report patch
    public HashMap getSIForExport(String user_id, String from, String to, String txtp) {
        parent = new Element("ROWSET");
        String query = null;
        Connection connect = null;
        ResultSet rs = null;
        //String startDateW ="5-Oct-2014";
        // String endDateW="18-Dec-2014";
        boolean result = false;
        System.out.println("THE TRANSACTION TYPE IS " + txtp);
        // if(txtp.equals("FLDFT")){
        query = "select * from eb_si_log a where (a.ADDED_DATE between to_date( '" + from + "','dd-mon-yyyy')"
                + "  and  to_date('" + to + "','dd-mon-yyyy') + 1) AND a.USER_ID ='" + user_id.toUpperCase() + "'";

        //}else{
        //  query ="select request_id,account_name,user_id,ft_debit_acc,dest_acct_no,amount,message,transaction_time from fundtransfer_log a where a.Transaction_Time between to_date( '"+from+"','dd-mon-yyyy')  and  to_date('"+to+"','dd-mon-yyyy') + 1 and message = 'Your Transfer was successfully done'";
        // }  
        try {
            connect = com.fasyl.ebanking.db.DataBase.getConnectionToFCC();
            System.out.println("QUERY  IS " + query);
            pstmt = connect.prepareStatement(query);
            // pstmt.setString(1, instdata.custId);
            rs = pstmt.executeQuery();

            parent = this.convertToXml(parent, "FUND", rs);

            if (parent == null) {
                System.out.println("parent is null");
                parent = new Element("ROWSET");
            }

            HashMap map = generateXML(parent, this.xmlText);
            if (map != null) {
                System.out.println("The parents is " + parent);

                return map;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
