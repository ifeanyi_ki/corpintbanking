/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import static com.fasyl.ebanking.main.EbankingConfiguration.RESULT_MAP_KEY;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author ibanking
 */
//Created by Rebecca for SecurityQuestion when location changes
public class SecurityQuestion extends ProcessClass {

    private Element parent = null;
    private String xmlText = null;

    private static final Logger logger = Logger.getLogger(SecurityQuestion.class.getName());

    private String errorMsg;

    private Connection connect = null;

    private String securityQuestionXml = null;
    private Object rst;

    public String getSecurityQuestions() {

        try {

            connect = com.fasyl.ebanking.db.DataBase.getConnection();

            logger.info(" get questions from db . . .  ");

            getQuestionsFromDB();

            connect.close();

            logger.info(" connection closed ! ");

        } catch (Exception ex) {

            errorMsg = ex.getMessage();

            logger.error(errorMsg);

            //   ex.printStackTrace();
        } finally {
            logger.info(" response xml: " + securityQuestionXml);
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        }

        return securityQuestionXml;
    }

    private void getQuestionsFromDB() throws Exception {

        Statement rset = null;

        rset = connect.createStatement();

        String sql;
        sql = "SELECT QUESTION_ID, QUESTION FROM SM_QUESTION";

        logger.info(" query: " + sql);

        ResultSet rs = rset.executeQuery(sql);

        logger.info(" fetch size: " + rs.getFetchSize());

        HashMap resultMap = generateSecurityXml(rs);

        rs.close();

        rset.close();

        logger.info(" statement and result set closed ! ");

        securityQuestionXml = (String) resultMap.get(RESULT_MAP_KEY);

        logger.info(" security xml: " + securityQuestionXml);

    }

    private HashMap generateSecurityXml(ResultSet rset) throws Exception {

        logger.info(" ------- generateSecurityXml ------------- ");

        parent = new Element("ROWSET");

        Element row = new Element("ROW");

        Element questionId = new Element("QUESTION_ID");
        questionId.setText(rset.getString("QUESTION_ID"));
        row.addContent(questionId);

        Element question = new Element("QUESTION");
        question.setText(rset.getString("QUESTION"));
        row.addContent(question);

        parent.addContent(row);

        HashMap result = generateXML(parent, this.xmlText);

        return result;
    }

    public java.util.ArrayList<String[]> getSecurityQuestion() throws SQLException {
        java.util.ArrayList<String[]> questionList = new java.util.ArrayList<String[]>();

        connect = com.fasyl.ebanking.db.DataBase.getConnection();

        Statement statement = connect.createStatement();

        String sql = "SELECT QUESTION_ID, QUESTION FROM SM_QUESTION";

        logger.info(" query: " + sql);

        ResultSet rs = statement.executeQuery(sql);

        //iterate through the result set
        while (rs.next()) {
            String[] question = new String[]{rs.getString("QUESTION_ID"), rs.getString("QUESTION")};
            questionList.add(question);
        }
        
        //Close all shits
        rs.close();
        statement.close();
        connect.close();

        return questionList;
    }

    public static void main(String args[]) {
    }
}
