/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.logic;

import com.fasyl.ebanking.main.DataofInstance;
import com.fasyl.ebanking.main.ReadRequestAsStream;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.HashMap;
import oracle.jdbc.OracleTypes;
import org.jdom.Element;

/**
 *
 * @author Administrator
 */
public class ConfirmCheque extends ProcessClass {
    
    private String acct;
    private String chequeno;
    private String amt;
    private String  beneficiaryname;
    private Element        parent  = null;
    private String         xmlText = null;
    
    public ConfirmCheque(){
        
    }
    
        public void initializeVar() {
        this.chequeno     = null;
        this.procStmt     = null;
        this.acct         = null;
        this.beneficiaryname=  null;
    }
         private void setInstanceVar(DataofInstance inst_data) throws Exception {
        this.acct     = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "acno", 0, false,
                null);
        this.chequeno = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "checkno", 0, false,
                null);
         this.amt = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "amt", 0, false,
                null);
         this.beneficiaryname = ReadRequestAsStream.getStringArrayValueFromQS(inst_data.requestHashTable, "benname", 0, false,
                null);
         xmlText = null;
         parent = new Element("ROWSET");
    }

         
         @Override
    public HashMap processRequest(DataofInstance instData) {
        Connection connect = null;
        CallableStatement statement = null;

        System.out.println(" ******Inside process request **** ");
        initializeVar();
        
        String   header;
        String   toemail;
        String   fromemail;

        try {
            setInstanceVar(instData);

            if (this.acct == null) {
                instData.result = Utility.processError(instData, ERR_ACCTNOISNULL);

                return instData.result;
            } else if (this.chequeno == null) {
                instData.result = Utility.processError(instData, ERR_CHEQUENOISNULL);

                return instData.result;
            }
            connect = com.fasyl.ebanking.db.DataBase.getConnection();
            statement = connect.prepareCall("{?=call CHK_STATUS_CONF(?,?,?,?,?,?,?)}");

            statement.registerOutParameter(1, OracleTypes.INTEGER);
            statement.registerOutParameter(6, OracleTypes.VARCHAR);
            statement.registerOutParameter(7, OracleTypes.VARCHAR);
            statement.setString(2,this.chequeno);
            statement.setString(3,this.chequeno);
            statement.setString(4,this.acct);
            statement.setString(5,this.amt);                
            statement.setString(8, instData.requestId);
            statement.execute();

            int result = statement.getInt(1);

            if (result==0){
           header    = "Confirmation of Cheque";
           toemail   = (String) LoadEmails.getDataValuenew("CHQST");
           fromemail = (String) LoadBasicParam.getDataValuenew("IBAEMAIL");
           String type = "text/html";
           String body =
                "Dear Sir/Ma,<br>The customer has sent a confirmation of the cheque  with the following credentials: <br> <B>Account Number : "
                + this.acct + "<br> Cheque Number: <br>" + this.chequeno + "<br> Amount: <br>"+ this.amt + "<br> Beneficiary Name: <br>"+ this.beneficiaryname 
                + "<B><br> Thank You<br> Yours Faithfully <br> Internet Banking Service";
          
            sendmail send      = new sendmail();
            boolean  results   = send.sendMessage(header, body, this.acct, type, fromemail, toemail);
            
            if (results){
                 Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");

                success.setText(
                    "You have succesfully sent a confirmatory message to the bank.");
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);
                
            }else{
                Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");
                
                success.setText(
                    "Error with  mail server while sending the message");
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);
                
            }

                }else{
                    Element row     = new Element("ROW");
                Element success = new Element("SUCCESS");
                String texts = statement.getString(7);
                if(texts==null)
                    texts = "Error while procesing your request";
                success.setText(texts);
                row.addContent(success);
                parent.addContent(row);
                instData.result = this.generateXML(parent, xmlText);
                
                }
            
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            
            
            if (connect != null) {
                try {
                    connect.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return instData.result;
    }

    
    
}
