/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.ebanking.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class QuestionAndAnswerDao {

    public HashMap getQuestionAndAnswer(String userId) {
        System.out.println("*** about to get question and answer with the following parameters ***");
        System.out.println("userId: " + userId);
        
        HashMap queAndAnsMap = new HashMap();
        String queryString = "select s1.question as question, s2.answer as answer from TBMB.SM_QUESTION s1 join TBMB.SM_USER_ACCESS s2 "
                + "on s1.question_id = s2.question_id where s2.cod_usr_id ='" + userId.toUpperCase() + "'";
        System.out.println("query string: " + queryString);
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String question = "";
        String answer = "";
        try {
//            con = ConnectionClass.getConn(""); //remove
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            if (con.isClosed() || con == null) {
                con = com.fasyl.ebanking.db.DataBase.getConnection();
//                con = ConnectionClass.getConn("");
                System.out.println("con is closed getting con again...");
            } else {
                System.out.println("con is not null or empty either: " + con);
            }
            pstmt = con.prepareStatement(queryString);
            //pstmt.setString(1, userId);

            rs = pstmt.executeQuery();

            if (rs == null) {
                System.err.println("*** result set is null ***");
            }
            
            if (rs.next()) {
                System.out.println("am in while loop");
                question = rs.getString("question");
                queAndAnsMap.put("question", question);

                answer = rs.getString("answer");
                queAndAnsMap.put("answer", answer);
            }
            System.out.println("question from getQuestionandAnswer: " + question);
            System.out.println("answer from getQuestionAndAnswer: " + answer);
        } catch (Throwable se) {
            System.err.println("error in get question and answer: " + se.getMessage());
            se.printStackTrace(System.out);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return queAndAnsMap;
    }
}
