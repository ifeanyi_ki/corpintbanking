/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.utility;

import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author kingsley
 */
public class AuditLog {

    final static Logger logger = Logger.getLogger(AuditLog.class);
    public DataSource ds;
    public JdbcTemplate jt;

    public AuditLog() {
        try {
            ds = ConnectionManager.getInstance().getTBMBDatasource();
            jt = new JdbcTemplate(ds);
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.error("error occured here trying to initialize ds and jt: " + ex.getMessage());
        }
    }

    public boolean log(AuditLogVo alVo) {
        String insertQuery = "insert into auditlog (txn_id,user_id,dattxn,remote_address,session_id,"
                + "trx_seq,customer_type,txn_desc,resp_status,process_status, error_message)"
                + "values(?,?,sysdate,?,?,?,?,?,?,?,?)";
        Object[] param = new Object[]{alVo.getTxnId(), alVo.getUserId(),alVo.getRemoteAddress(),
                                        alVo.getSessionId(), alVo.getTrxSeq(), alVo.getCustomerType(),
                                        alVo.getTrxDesc(), alVo.getResponseStatus(), alVo.getProcessStatus(), 
                                        alVo.getErrorMessage()};
 
        int result = jt.update(insertQuery, param);
        
        return (result == 1) ? true : false;

    }

}
