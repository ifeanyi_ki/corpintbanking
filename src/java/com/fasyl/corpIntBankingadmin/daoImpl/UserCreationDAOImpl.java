package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.dao.UserCreationDAO;
import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.util.Encryption;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.hsqldb.types.Types;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author kingsley
 */
public class UserCreationDAOImpl implements UserCreationDAO {

    final static Logger logger = Logger.getLogger(UserCreationDAOImpl.class);
    public DataSource ds;
    private JdbcTemplate jt;
    List<String> pendingUseridList;

    public UserCreationDAOImpl() throws IOException, SQLException, PropertyVetoException {
        ds = ConnectionManager.getInstance().getTBMBDatasource();
        pendingUseridList = new ArrayList<>();
        jt = new JdbcTemplate(ds);
    }

    @Override
    public int createUser(UserVo userVo) {
        String dateCreated = "SYSDATE";
        String insertSql = "insert into tb_user_access ("
                + "userid,"
                + "password,"
                + "firstname,"
                + "lastname,"
                + "role_id,"
                + "organisation,"
                + "created_by,"
                + "active,"
                + "email,"
                + "phone_number,"
                + "status,"
                + "password_reset,"
                + "login_state,"
               
                + "login_count)" + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        Object[] params = new Object[]{userVo.getId(), Encryption.encrypt("password"), userVo.getFirstName(),
            userVo.getLastName(), userVo.getRoleId(), userVo.getClient(), userVo.getCreated_by(),
            "Y", userVo.getEmail(), userVo.getPhoneNumber(), userVo.getStatus(),"N", "N", 0};

//        int[] types = new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
//            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,Types.VARCHAR,
//            Types.INTEGER};

        int rowCount = 0;
        try {
            rowCount = jt.update(insertSql, params /*, types*/);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("number of rows inserted in UserCreationDAO createUser method: " + rowCount);
        return rowCount;
    }

    public int createUserX(UserVo userVo, String userid, String user_id) {

        String updateQuery = "update tb_user_access set status = ? "
                + ", userid = ? "
                + ", organisation = ? "
                + ", email = ? "
                + ", phone_number = ? "
                + ", firstname = ? "
                + ", lastname = ? "
                + " where userid = ? ";

        Object[] params = new Object[]{userVo.getStatus(), userid, userVo.getClient(),
            userVo.getEmail(), userVo.getPhoneNumber(), userVo.getFirstName(), userVo.getLastName(),
            user_id};

        int[] types = new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
            Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};

        int rowCount = 0;
        try {
            rowCount = jt.update(updateQuery, params, types);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("number of rows inserted in UserCreationDAO createUserX method: " + rowCount);
        return rowCount;
    }

    @Override
    public int updateUser(UserVo userVo) {
        String sqlCmd = "update tb_user_access set active = ? "
                + ", email = ? "
                + ", phone_number = ? "
                + ", role_id = ? "
                + "where userid = ?";

        Object[] params = new Object[]{userVo.getActive(), userVo.getEmail(), userVo.getPhoneNumber(), userVo.getRoleId(), userVo.getId()};

        int affectedRows = 0;
        try {
            affectedRows = jt.update(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        if (affectedRows != 1) {
            return -1;
        } else {
            return affectedRows;
        }

    }

    @Override
    public List<String> getUserByStatus() {

        String sqlCmd = "select userid from tb_user_access where status = ? ";

        List<String> queryForList = null;
        try {
            queryForList = jt.queryForList(sqlCmd, new Object[]{"Pending"}, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
//        pendingUseridList.add(queryForRowSet.getString("userid"));
        System.out.println("pending user ids >>>");
//        queryForList.stream().forEach(System.out::println);
        return queryForList;
    }

    @Override
    public int authoriseAdminCreation(UserVo userVo, String authId) {
        logger.debug("*** values in uservo in authoriseAdminCreation method in usercreationctrller ***");
        logger.debug("status: " + userVo.getStatus());
        logger.debug("comment: " + userVo.getComment());
        logger.debug("id: " + userVo.getId());
        logger.debug("authId: " + authId);

        String sqlCmd = "update tb_user_access set status = ?"
                + ", authorise_by = ?"
                + ", comments = ? "
                + " where userid = ?";

        Object[] params = new Object[]{userVo.getStatus(), authId, userVo.getComment(), userVo.getId()};

        int affectedRows = 0;
        try {
            affectedRows = jt.update(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return affectedRows;

    }

}
