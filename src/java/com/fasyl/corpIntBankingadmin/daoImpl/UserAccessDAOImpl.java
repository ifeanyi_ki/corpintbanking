package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.dao.UserAccessDAO;
import com.fasyl.corpIntBankingadmin.dao.mapper.UserRowMapper;
import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.QuestionData;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 *
 * @author kingsley
 */
public class UserAccessDAOImpl implements UserAccessDAO {

    final static Logger logger = Logger.getLogger(UserAccessDAOImpl.class);
    public DataSource ds;
    public JdbcTemplate jt;

    private boolean flag = false;

    public UserAccessDAOImpl() {
        try {
            ds = ConnectionManager.getInstance().getTBMBDatasource();
            jt = new JdbcTemplate(ds);
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.error("error occured here trying to initialize ds and jt: " + ex.getMessage());
        }
    }

    @Override
    public boolean doesUserExist(String userId) {
        Object[] parameters = new Object[]{userId};
        Object o = null;
        try {
            o = jt.queryForInt("select count(*) from tb_user_access where userid = ?",
                    parameters);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        if ((int) o > 0) {
            flag = true;
        }

        logger.debug("user id count: " + o);
        return flag;
    }

    @Override
    public int getLoggingCount(String userId) {
        Object[] parameters = new Object[]{userId};
        Object o = null;
        try {
            o = jt.queryForInt("select login_count from tb_user_access where userid = ?",
                    parameters);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("login count in getLoggingCount in useraccessdaoimpl: " + (int) o);
        return (int) o;
    }

    @Override
    public boolean isUserActive(String userId) {
        Object[] parameters = new Object[]{userId, "Y"};
        Object o = null;
        try {
            o = jt.queryForObject("select password from tb_user_access where userid = ? and active = ?",
                    parameters, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        if (o != null) {
            flag = true;
        }
        logger.debug("password in isuseractive in useraccessdaoimpl: " + o);
        return flag;
    }

    @Override
    public String getRoleId(String userId) {
        Object[] parameters = new Object[]{userId};
        Object o = null;
        try {
            o = jt.queryForObject("select role_id from tb_user_access where userid = ? ",
                    parameters, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("role id in getRoleId in useraccessdaoimpl: " + o);

        return (String) o;

    }

    @Override
    public int updateLastLoggingDate(String userId, Date loggingDate) {
        Object[] parameters = new Object[]{loggingDate, userId};
        int[] types = {Types.DATE, Types.VARCHAR};

        int affectedRows = 0;
        try {
            affectedRows = jt.update("update tb_user_access set last_login_date = ? where userid = ?",
                    parameters, types);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("affected rows after updating last login date in updateLastLoggingDate in useraccessdaoimpl: " + affectedRows);

        return affectedRows;

    }

    @Override
    public int updateLoggingCount(String userId, int loggingCount) {

        Object[] parameters = new Object[]{loggingCount, userId};
        int[] types = {Types.INTEGER, Types.VARCHAR};
        System.out.println(userId + ", " + loggingCount);
        int affectedRows = 0;
        try {
            affectedRows = jt.update("update tb_user_access set login_count = ? where userid = ?",
                    parameters, types);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        logger.debug("affected rows after updating login count in updateLoggingCount in useraccessdaoimpl: " + affectedRows);

        return affectedRows;
    }

    @Override
    public String getLastLoginDate(String userId) {
        Object[] parameters = new Object[]{userId};
        Object o = null;
        try {
            o = jt.queryForObject("select last_login_date from tb_user_access where userid = ? ",
                    parameters, Date.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("last login date in getLastLoginDate in useraccessdaoimpl: " + o);

        return getFormattedDate(o.toString());

    }

    @Override
    public String getPassword(String userId) {
        logger.debug("user id in get password menthod of useraccessdaoimpl: " + userId);
        Object[] parameters = new Object[]{userId};
        Object o = null;
        try {
            o = jt.queryForObject("select password from tb_user_access where userid = ?",
                    parameters, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("password in getPassword in useraccessdaoimpl: " + o);
        return o.toString();
    }

    @Override
    public int updatePasswordOnFirstTimeLogin(String userId, String password, String questionId, String answer) {
        Date firstLoginDate = new Date();
        Object[] parameters = new Object[]{password, 1, firstLoginDate, questionId, answer, userId};
        int affectedRows = 0;
        try {
            affectedRows = jt.update("update tb_user_access set password = ? , login_count = ?, last_login_date = ?,"
                    + " question_id = ?, answer = ? where userid = ?",
                    parameters);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("affected rows after updating password in updatePasswordOnFirstTimeLogin in useraccessdaoimpl: "
                + affectedRows);

        return affectedRows;
    }

    @Override
    public UserVo getUser(String userId) {
        logger.debug("user id in get User method in useraccessdaoimp:" + userId);
        UserVo userVo = null;
        String sqlCmd = "select * from tb_user_access where userid = ?";
        Object[] parameters = new Object[]{userId};

        try {
            userVo = (UserVo) jt.queryForObject(sqlCmd, parameters, new UserRowMapper());
            userVo.setCustomerNo(getCustNo(userId));
            return userVo;
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        return userVo;
    }

    public String getCustNo(String userId) {
        logger.debug("user id in getCustNo method in useraccessdaoimp:" + userId);
        String sqlCmd = "select onboarded_clients.customer_no from tb_user_access tb inner join onboarded_clients"
                + " on tb.organisation=onboarded_clients.customer_name1"
                + " where tb.userid= ?";
        SqlRowSet queryForRowSet = jt.queryForRowSet(sqlCmd, new Object[]{userId});
        if (queryForRowSet.next()) {
            System.out.println("customer no from getCustNo method is: " + queryForRowSet.getString("customer_no"));
            return queryForRowSet.getString("customer_no");
        }
        return queryForRowSet.getString("customer_no");
    }

    @Override
    public List<String> getUserByIdorBylastOrFirstName(String userId, String clientName) {
        logger.debug("user id in getUserByIdorBylastOrFirstName method in userAccessDaoImpl class: " + userId);
        logger.debug("clientName in getUserByIdorBylastOrFirstName method in userAccessDaoImpl class: " + clientName);
        String sqlCmd = "select userid from tb_user_access where ( userid like '%" + userId + "%' or firstname like '%"
                + userId + "%' "
                + "or "
                + "lastname like '%" + userId + "%') "
                + " and (organisation ='" + clientName + "'" + " ) and role_id != 'client_admin'";

        jt = new JdbcTemplate(ds);

        List<String> userList = null;
        try {
            userList = jt.queryForList(sqlCmd, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        if (!userList.isEmpty()) {
            logger.debug("*** user found ***");
            return userList;
        } else {
            return null;
        }

    }

    @Override
    public List<String> getListOfDeclinedUsers(String userId) {
        logger.debug("user id in getListOfDeclinedUsers method in userAccessDaoImpl class: " + userId);

        String sqlCmd = "select userid from tb_user_access where status = ? and created_by = ? ";

        Object[] params = new Object[]{"Declined", userId};
        List<String> declinedUsersList = null;
        try {
            declinedUsersList = jt.queryForList(sqlCmd, params, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return declinedUsersList;
    }

    @Override
    public boolean isLoggedIn(String userId) {
        logger.debug("user id in isLoggedIn method in userAccessDaoImpl class: " + userId);
        String login_state = "";
        String sqlCmd = "select login_state from tb_user_access where userid = ? ";
        Object[] params = new Object[]{userId};

        SqlRowSet queryForRowSet = null;
        try {
            queryForRowSet = jt.queryForRowSet(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        try {
            if (queryForRowSet.next()) {
                login_state = queryForRowSet.getString("login_state");
                logger.debug("login state of " + userId + " is " + login_state);
            }
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        return (login_state.equalsIgnoreCase("Y")) ? true : false;
    }

    @Override
    public int updateLoginState(String userId, String status) {
        logger.debug("user id in updateLoginState method in userAccessDaoImpl class: " + userId);
        String sqlCmd = "update tb_user_access set login_state = ? where userid = ?";
        Object[] params = new Object[]{status, userId};
        int affectedRows = 0;
        try {
            affectedRows = jt.update(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        logger.debug("affected rows count after update: " + affectedRows);
        return affectedRows;
    }

    public String getFormattedDate(String dateString) {
        logger.debug("date string for formatting: " + dateString);
        String formattedDateString = "";
        formattedDateString = dateString.substring(0, 9);

        StringTokenizer strTokenizer = new StringTokenizer(dateString, "-");
        String year = "", month = "", day = "";

        year = (String) strTokenizer.nextToken();
        month = (String) strTokenizer.nextToken();
        day = (String) strTokenizer.nextToken();
        day = day.substring(0, 2);
        formattedDateString = day + "-" + month + "-" + year;

        return formattedDateString;
    }

    @Override
    public boolean isFieldAvailable(String field) {
        logger.debug("field in isFieldAvailable method in userAccessDaoImpl class: " + field);
        String sqlCmd = "select count(*) as count from tb_user_access where phone_number = ?  or email = ? or userid = ?";
        Object[] params = new Object[]{field, field, field};
        SqlRowSet fieldCountRowSet = jt.queryForRowSet(sqlCmd, params);

        if (fieldCountRowSet.next()) {
            int count = Integer.parseInt(fieldCountRowSet.getString("count"));

            if (count > 0) {
                System.out.println("count: " + count);
                return true;
            } else {
                System.out.println("count: " + count);
                return false;
            }

        }
        return true;
    }

    @Override
    public boolean isFieldAvailable2(String field, String userid) {
        logger.debug("field in isFieldAvailable2 method in userAccessDaoImpl class: "
                + field + " and userid: " + userid);
        String sqlCmd = "select count(*) as count from tb_user_access where "
                + "(phone_number = ? or email = ? ) and (userid != ? )";
        Object[] params = new Object[]{field, field, userid};
        SqlRowSet fieldCountRowSet = jt.queryForRowSet(sqlCmd, params);

        if (fieldCountRowSet.next()) {
            int count = Integer.parseInt(fieldCountRowSet.getString("count"));

            if (count > 0) {
                System.out.println("count: " + count);
                return true;
            } else {
                System.out.println("count: " + count);
                return false;
            }

        }
        return true;
    }

    @Override
    public ArrayList getQuestions() {
        //select * from sm_question;
        logger.debug("About to retrieve questions from db");
        ArrayList questionList = new ArrayList();
        String sqlCmd = "select * from sm_question";

        questionList = (ArrayList) jt.query(sqlCmd, new BeanPropertyRowMapper(QuestionData.class));

        return questionList;
    }

    @Override
    public String getPasswordReset(String userId) {
        logger.debug("user id in getPasswordReset method of useraccessdaoimpl: " + userId);
        Object[] parameters = new Object[]{userId};
        Object o = jt.queryForObject("select password_reset from tb_user_access where userid = ?",
                parameters, String.class);

        logger.debug("password_reset in getPasswordReset in useraccessdaoimpl: " + o);
        return o.toString();
    }

    public String getAnswer(String userId) {
        logger.debug("user id in getAnswer method of useraccessdaoimpl: " + userId);
        Object[] parameters = new Object[]{userId};

        Object o = jt.queryForObject("select answer from tb_user_access where userid = ?",
                parameters, String.class);

        logger.debug("answer in getPasswordReset in useraccessdaoimpl: " + o);
        return o.toString();
    }

    public String getQuestion(String userId) {
        logger.debug("user id in getPasswordReset method of useraccessdaoimpl: " + userId);
        Object[] parameters = new Object[]{userId};

        Object o = jt.queryForObject("select sq.question from sm_question sq inner join tb_user_access tb"
                + " on tb.question_id = sq.question_id where userid = ?",
                parameters, String.class);

        logger.debug("password_reset in getPasswordReset in useraccessdaoimpl: " + o);
        return o.toString();
    }

    @Override
    public int updatePasswordReset(String userId, String password) {
        System.out.println("userid: " + userId + " , password: " + password);
        Object[] parameters = new Object[]{password, userId};
        int affectedRows = jt.update("update tb_user_access set password = ?, password_reset = 'N' where userid = ?",
                parameters);

        System.out.println("affected rows after password reset: " + affectedRows);
        logger.debug("affected rows after updating password in updatePasswordOnFirstTimeLogin in useraccessdaoimpl: "
                + affectedRows);

        return affectedRows;
    }
}
