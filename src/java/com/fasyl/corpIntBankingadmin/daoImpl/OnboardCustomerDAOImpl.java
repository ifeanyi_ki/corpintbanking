package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.dao.OnboardCustomerDAO;
import com.fasyl.corpIntBankingadmin.dao.mapper.CustomerRowMapper;
import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.CustomerVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 *
 * @author kingsley
 */
public class OnboardCustomerDAOImpl implements OnboardCustomerDAO {

    final static Logger logger = Logger.getLogger(UserCreationDAOImpl.class);
    private DataSource ds;
    private JdbcTemplate jt;

    public OnboardCustomerDAOImpl() throws IOException, SQLException, PropertyVetoException {
        ds = ConnectionManager.getInstance().getTBMBDatasource();
        jt = new JdbcTemplate(ds);
    }

    @Override
    public List<String> searchUnBoardedCustomerNameList(String customername) {
        List<String> customerList = new ArrayList<>();
        try {
            logger.debug("customer name in get customer method in onboard dao: " + customername);
            
            
            String sqlString = "select customer_name1 from sttm_customer where customer_no = ? and customer_type = ? "
                    + "and customer_name1 not in "
                    + "(select customer_name1 from onboarded_clients)";
            
            String cmdSql = "select customer_name1 from sttm_customer where customer_name1 like '%" + customername + "%' "
                    + "and customer_name1 not in "
                    + "(select customer_name1 from onboarded_clients)";
            
            SqlRowSet queryForRowSet = jt.queryForRowSet(sqlString, new Object[]{customername, "C"});
            
            while (queryForRowSet.next()) {
                customerList.add(queryForRowSet.getString("customer_name1"));
            }
            
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return customerList;
    }

    @Override
    public CustomerVo getCustomerDetails(String client_name) {
        logger.debug("customer name in get getCustomerDetails in onboard dao: " + client_name);
        String sqlCmd = "select * from sttm_customer where customer_name1 = ?";

        try {
            return (CustomerVo) jt.queryForObject(sqlCmd, new Object[]{client_name}, new CustomerRowMapper());
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public int onboardClient(String customerNo) {
        logger.debug("customerNo in get onboardClient in onboard dao: " + customerNo);
        String cmdSql = " insert into onboarded_clients"
                + "(select customer_no,CUSTOMER_TYPE,customer_name1,address_line1, "
                + "address_line2,address_line3,address_line4,country,short_name, "
                + "nationality,CRM_CUSTOMER,ISSUER_CUSTOMER,TREASURY_CUSTOMER, SYSDATE "
                + "from sttm_customer where customer_no = ? )";

        int affectedRows = 0;
        try {
            affectedRows = jt.update(cmdSql, customerNo);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return affectedRows;
    }

    @Override
    public List<String> getOnboardedClientList() {
        String sqlCmd = "select customer_name1 from onboarded_clients";

        List<String> clientList = null;
        try {
            clientList = jt.queryForList(sqlCmd, String.class);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return clientList;
    }

}
