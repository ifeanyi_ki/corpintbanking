package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.dao.PasswordChangeDAO;
import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.ebanking.util.Encryption;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class PasswordChangeDAOImpl implements PasswordChangeDAO {

    final static Logger logger = Logger.getLogger(UserAccessDAOImpl.class);
    private DataSource ds;
    private JdbcTemplate jt;

    public PasswordChangeDAOImpl() {
        try {
            ds = ConnectionManager.getInstance().getTBMBDatasource();
            jt = new JdbcTemplate(ds);
        } catch (IOException | SQLException | PropertyVetoException iOException) {
            System.out.println("error in getting datasource: " + iOException.getMessage());
        }
    }

    @Override
    public List<String> getListofPendingPasswordChangeRequest(String userid) {
        System.out.println("user id in updatePassword method in passwordchangedaoimpl: " + userid);
        List<String> useridStringList = new ArrayList<>();
        
       String sqlCmd =  "select pcr.userid from password_change_request pcr " + 
                        " inner join tb_user_access tua " +
                        " on (pcr.userid = tua.userid) " +
                        " where pcr.status = ? and tua.created_by = ? ";

//        String sqlCmd = "select userid from password_change_request where status = ? ";

        Object[] param = new Object[]{"pending", userid};
        jt = new JdbcTemplate(ds);

        try {
            useridStringList = jt.queryForList(sqlCmd, param, String.class);
        } catch (Exception ex) {
            System.err.println("error in getListofPendingPasswordChangeRequest: " + ex.getMessage());
        }

        if (useridStringList.isEmpty()) {
            System.out.println("*** pending request list is empty ***");
        }
        for (String useridVal : useridStringList) {
            System.out.println("user id: " + useridVal);
        }

//        useridStringList.stream().forEach((u) -> {
//            logger.debug("userid in getListofPendingPasswordChangeRequest: " + u);
//        });

        return useridStringList;
    }

    @Override
    public int updatePasswordwithNewPassword(String userid) {
        logger.debug("user id in updatePassword method in passwordchangedaoimpl: " + userid);
        int result = 0;
        String newPassword = getNewPassword(userid);
        String cmdSql = "update tb_user_access set password = ? where userid = ? ";

        if (!"".equalsIgnoreCase(newPassword) || null != newPassword) {
            //go ahead and update the user's password with the new password.
            Object[] params = new Object[]{newPassword, userid};
            jt = new JdbcTemplate(ds);

            try {
                result = jt.update(cmdSql, params); //result == affected rows == success
            } catch (Exception ex) {
                System.err.println("error: " + ex.getMessage());
            }

        } else {
            result = -1;//no pending password change request for the user
        }
        return result;

    }

    public String getNewPassword(String userid) {
        logger.debug("user id in getNewPassword method in passwordchangedaoimpl: " + userid);

        String sqlCmd = "select new_password from password_change_request where userid = ? and status = ? ";
        Object[] params = new Object[]{userid, "pending"};

        jt = new JdbcTemplate(ds);
        SqlRowSet queryForRowSet = null;
        try {
            queryForRowSet = jt.queryForRowSet(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        try {
            if(queryForRowSet.next()){
                return queryForRowSet.getString("new_password");
            }
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        return "";
    }

    @Override
    public int approvePasswordChangeRequest(String userid, String authId) {
        logger.debug("userid in approvePasswordChangeRequest method in passwordchangedaoimpl class: " + userid);
        logger.debug("authId in approvePasswordChangeRequest method in passwordchangedaoimpl class: " + authId);

        String cmdSql = "update password_change_request set status = ?,  authorised_by = ? where userid = ? and status = ?";
        Object[] params = new Object[]{"APPROVED", authId, userid, "pending"};

        jt = new JdbcTemplate(ds);
        int affectedRows = 0;
        try {
            affectedRows = jt.update(cmdSql, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return affectedRows;
    }

    @Override
    public int declinePasswordChangeRequest(String userid, String authId) {
        logger.debug("userid in approvePasswordChangeRequest method in passwordchangedaoimpl class: " + userid);
        logger.debug("authId in approvePasswordChangeRequest method in passwordchangedaoimpl class: " + authId);

        String cmdSql = "update password_change_request set status = ?,  authorised_by = ? where userid = ? and status = ?";
        Object[] params = new Object[]{"DECLINED", authId, userid, "pending"};

        jt = new JdbcTemplate(ds);
        int affectedRows = 0;
        try {
            affectedRows = jt.update(cmdSql, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return affectedRows;

    }

    @Override
    public int resetPassword(String userId) {
        logger.debug("userid in v method in passwordchangedaoimpl class: " + userId);
        String cmdSql = "update tb_user_access set password = ? , password_reset = ? where userid = ? ";

        int affectedRows = 0;
        try {
            affectedRows = jt.update(cmdSql, Encryption.encrypt("password"), "Y", userId);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        return affectedRows;

    }

    @Override
    public int logChangePasswordRequest(String userId, String newPassword, String status) {
        logger.debug("parameters in changepasswordrequest in passwordchangedaoimpl: "
                + userId + ", " + newPassword + ", " + status);
        String sqlCmd = "insert into password_change_request(userid, new_password, status)"
                + " values(?,?,?)";
        Object[] params = new Object[]{userId, newPassword, status};
        int affectedRows = 0;
        try {
            affectedRows = jt.update(sqlCmd, params);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("no of affected rows after inserting password change request: " + affectedRows);

        if (affectedRows == 1) {
            return affectedRows;
        } else {
            return -1;
        }

    }

    @Override
    public String getchangePasswordRequestStatus(String userId) {
        logger.debug("userid in getchangePasswordRequestStatus method in passwordchangedaoimpl: " + userId);
        String status = "";

        String sqlCmd = "select status from password_change_request where userid = ?";
        SqlRowSet queryForRowSet = null;
        try {
            queryForRowSet = jt.queryForRowSet(sqlCmd, userId);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        
        try {
            if(queryForRowSet.next()){
                status = queryForRowSet.getString("status");
            }
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        

        if (status != null || !"".equals(status)) {
            logger.debug("status of password chnge request: " + status);
            return status;
        } else {
            return status;
        }

    }
    
    public String getUserEailForPassAuthNotification(String userid){
        System.out.println("userid: " + userid);
        String cmdQuery = "select email from tb_user_access where userid= ?";
        String email = jt.queryForObject(cmdQuery, new Object[]{userid}, String.class);
        
        System.out.println("email from getUserEailForPassAuthNotification: " + email);
        
        return email;
    }
    
    public String changePassword(String userId, String newPassword, String oldPassword) {
        String errCode;
        String errMsg = "";
        try {
            CallableStatement callStmt;
            Connection con = ds.getConnection();
            con.setAutoCommit(false);
            callStmt = con.prepareCall("{?=call fn_change_password_corp(?,?,?,?,?)}");
            callStmt.registerOutParameter(1, OracleTypes.INTEGER);
            callStmt.registerOutParameter(5, OracleTypes.VARCHAR);
            callStmt.registerOutParameter(6, OracleTypes.VARCHAR);
            callStmt.setString(2, oldPassword);
            callStmt.setString(3, newPassword);
            callStmt.setString(4, userId);
            
            callStmt.execute();
            int responseCode = callStmt.getInt(1);
            System.out.println("response code: " + responseCode);
            errCode = callStmt.getString(5);
            errMsg = callStmt.getString(6);
            System.out.println("err code: " + errCode);
            System.out.println("err message: " + errMsg);
            if(responseCode == 0){
                con.commit();
                return "Password change was successful.";
            }else if(responseCode == 201){
                con.rollback();
                return errMsg;
            }else if(responseCode == 202){
                con.rollback();
                return "Old Password is not correct.";
            }else{
                con.rollback();
                errMsg = "Error try again later.";
            }
//            logger.debug("userid in changePassword method in passwordchangedaoimpl class: " + userId + ", new password: " + newPassword);
//            String cmdSql = "update tb_user_access set password = ? where userid = ? ";

//            int affectedRows = jt.update(cmdSql, Encryption.encrypt(newPassword), userId.toUpperCase());
//            return affectedRows;
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(PasswordChangeDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return errMsg;
    }

}
