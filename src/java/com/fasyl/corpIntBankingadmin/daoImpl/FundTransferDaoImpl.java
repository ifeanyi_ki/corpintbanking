/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.dao.FundTransferDAO;
import com.fasyl.corpIntBankingadmin.dao.mapper.FtVoRowMapper;
import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.FundTransferVo;
import com.fasyl.corpIntBankingadmin.vo.MandateVo;
import com.fasyl.corpIntBankingadmin.vo.ftVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 *
 * @author XNETTIntBanking
 */
public class FundTransferDaoImpl implements FundTransferDAO {

    private DataSource ds;
    private JdbcTemplate jt;

    public FundTransferDaoImpl() {
        try {
            ds = ConnectionManager.getInstance().getTBMBDatasource();
            jt = new JdbcTemplate(ds);
        } catch (IOException ex) {
            Logger.getLogger(FundTransferDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(FundTransferDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(FundTransferDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getPendingFundTransferCount(String userId) {
        System.out.println("userid for getPendingTransferFundCount is: " + userId);
        int count = 0;
        String cmdSql = "select count(*) from fund_transfer where post_status = ? and initiator_id = ? ";
        Object[] prams = new Object[]{"pending", userId};
        try {
            count = jt.queryForInt(cmdSql, prams);
            System.out.println("number of pending fund transfers: " + count);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }
        return count;
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfers(String userId) {
        System.out.println("user id in getPendingFundTransfers method in FundTransferDaoImpl:" + userId);
        List<FundTransferVo> queryForList = new ArrayList<>();
        
        String alias = new String(userId);
        int atIndex = alias.indexOf("@");
        alias = alias.substring(atIndex, alias.length());
        alias = alias.replace("@", "");
        System.out.println("alias ==> " + alias);
        
        String cmdQuery
                = "select ft.initiator_id, ft.init_account, ft.ben_account, ft.amount, ft.trans_id from ft.fund_transfer"
                + " where ft.auth2_action = 'pending' and ft.auth1_action = 'pending'"
                + " and ft.operator = 'OR' and SUBSTR(ft.initiator_id, (INSTR(ft.initiator_id, '@') + 1), LENGTH(ft.initiator_id)) = '" + alias + "'";

        try {
            SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery);
            while (queryForRowSet.next()) {
                FundTransferVo fundTransferVo = new FundTransferVo();
                fundTransferVo.setInitiatorId(queryForRowSet.getString("initiator_id"));
                fundTransferVo.setInitAccount(queryForRowSet.getString("init_account"));
                fundTransferVo.setBenAccount(queryForRowSet.getString("ben_account"));
                fundTransferVo.setAmount(queryForRowSet.getString("amount"));
                fundTransferVo.setTransactionId(queryForRowSet.getString("trans_id"));
                queryForList.add(fundTransferVo);
            }

//            queryForList.stream().forEach(c -> System.out.println(">>>>" + c.getInitiatorId()
//                    + ", " + c.getInitAccount() + ", " + c.getBenAccount() + ", " + c.getAmount() + ", " + c.getTransactionId()));
        } catch (Exception ex) {
            System.err.println("Error in gettin pending fund transfer : " + ex.getMessage());
        }

        return queryForList;
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfersByAuth1(String userId) {
        System.out.println("user id in getPendingFundTransfers method in getPendingFundTransfersByAuth1:" + userId);
        List<FundTransferVo> queryForList = new ArrayList<>();
        String alias = new String(userId);
        int atIndex = alias.indexOf("@");
        alias = alias.substring(atIndex, alias.length());
        alias = alias.replace("@", "");
        System.out.println("alias ==> " + alias);
        
        String cmdQuery
                = "select ft.initiator_id, ft.init_account, ft.ben_account, ft.amount, ft.trans_id from fund_transfer ft "
                + "where ft.auth1_action = 'pending' and SUBSTR(ft.initiator_id, (INSTR(ft.initiator_id, '@') + 1), LENGTH(ft.initiator_id)) = '" + alias + "' ";

        try {
            SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery);
            while (queryForRowSet.next()) {
                FundTransferVo fundTransferVo = new FundTransferVo();
                fundTransferVo.setInitiatorId(queryForRowSet.getString("initiator_id"));
                fundTransferVo.setInitAccount(queryForRowSet.getString("init_account"));
                fundTransferVo.setBenAccount(queryForRowSet.getString("ben_account"));
                fundTransferVo.setAmount(queryForRowSet.getString("amount"));
                fundTransferVo.setTransactionId(queryForRowSet.getString("trans_id"));
                queryForList.add(fundTransferVo);
            }

//            queryForList.stream().forEach(c -> System.out.println("initiator id: " + c.getInitiatorId() 
//                    + ", bene acc: " + c.getBenAccount() + ", init acc: " + c.getInitAccount() + ", amount: " 
//                    + c.getAmount() + ", trx id: " + c.getTransactionId()));
        } catch (Exception ex) {
            System.err.println("Error in gettin pending fund transfer : " + ex.getMessage());
        }

        return queryForList;
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfersByAuth2(String userId) {
        System.out.println("user id in getPendingFundTransfers method in getPendingFundTransfersByAuth2:" + userId);
        List<FundTransferVo> queryForList = new ArrayList<>();
        String alias = new String(userId);
        int atIndex = alias.indexOf("@");
        alias = alias.substring(atIndex, alias.length());
        alias = alias.replace("@", "");
        System.out.println("alias ==> " + alias);
        
        String cmdQuery
                = "select ft.initiator_id, ft.init_account, ft.ben_account, ft.amount, ft.trans_id from fund_transfer ft where "
                + "ft.auth2_action = 'pending' and ft.auth1_action = 'APPROVED' and ft.operator = 'AND' and "
                + " SUBSTR(ft.initiator_id, (INSTR(ft.initiator_id, '@') + 1), LENGTH(ft.initiator_id)) = '" + alias + "'";

        try {
            SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery);
            while (queryForRowSet.next()) {
                FundTransferVo fundTransferVo = new FundTransferVo();
                fundTransferVo.setInitiatorId(queryForRowSet.getString("initiator_id"));
                fundTransferVo.setInitAccount(queryForRowSet.getString("init_account"));
                fundTransferVo.setBenAccount(queryForRowSet.getString("ben_account"));
                fundTransferVo.setAmount(queryForRowSet.getString("amount"));
                fundTransferVo.setTransactionId(queryForRowSet.getString("trans_id"));
                queryForList.add(fundTransferVo);
            }

//            queryForList.stream().forEach(c -> System.out.println(">>>>" + c.getInitiatorId()
//                    + ", " + c.getInitAccount() + ", " + c.getBenAccount() + ", " + c.getAmount() + ", " + c.getTransactionId()));
        } catch (Exception ex) {
            System.err.println("Error in gettin pending fund transfer : " + ex.getMessage());
        }

        return queryForList;
    }

    @Override
    public List<String> getAccountListForAnInitiator(String userId) {
        System.out.println("user id in getAccountListForAnInitiator method in FundTransferDaoImpl:" + userId);
        String cmdQuery = "select account from acc_init_mandate where initiator_id = ? ";
        List<String> accList = new ArrayList<>();
        try {
            SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, new Object[]{userId});

            while (queryForRowSet.next()) {
                accList.add(queryForRowSet.getString("account"));
            }
            System.out.println(">>>>>" + " accounts for userid: " + userId);

            //  accList.stream().forEach(System.out::println);
            System.out.println("************************************");
        } catch (Exception ex) {
            System.err.println("Error in gettin pending fund transfer : " + ex.getMessage());
        }
        return accList;
    }

    @Override
    public String getTransactionLimit(String accountNo, String userId) {
        try {
            System.out.println("Account No and userId in getTransactionLimit: " + accountNo + ", " + userId);
//        List<String> transactionLimit = new ArrayList<>();
//        String cmdQuery = "select trans_limit from acc_init_mandate where"
//                + " account = ? and initiator_id = ? ";
//        Object[] param = new Object[]{accountNo, userId};
//        try {
//            transactionLimit = jt.queryForList(cmdQuery, param, String.class); 
//
//            if(transactionLimit.isEmpty()){
//                System.out.println("trx limit list is empty");
//                return null;
//            }else{
//                System.out.println("transaction limit for userid: " + userId + " is; " + transactionLimit.get(0));
//                return transactionLimit.get(0);
//            }
//            //while (queryForRowSet.next()) {
//            //transactionLimit = queryForRowSet.getObject("trans_limit");
//            
//            //}
//        } catch (Exception ex) {
//            System.err.println("error in getTransaction limit: " + ex.getMessage());
//        }
//        return transactionLimit.get(0);

            Statement st = ds.getConnection().createStatement();
            String cmdQuery = "select trans_limit from acc_init_mandate where"
                    + " account ='" + accountNo.trim() + "' and initiator_id ='" + userId + "'";
            System.out.println("query: " + cmdQuery);
            ResultSet rs = st.executeQuery(cmdQuery);
            String limit = "";
            while (rs.next()) {
                limit = rs.getString(1);
                System.out.println("limit: " + limit);
            }
            return limit;
        } catch (SQLException ex) {
            Logger.getLogger(FundTransferDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    @Override
    public MandateVo getMandate(String amount, String accountNumber) {
        System.out.println("Amount and accountNumber for fetching mandate in get Mandate method is: "
                + amount + " and " + accountNumber);
        MandateVo mandateVo = null;
        String cmdQuery = "select auth1, operator, auth2 from account_mandate where lower_limit <= ? and"
                + " upper_limit >= ? and account = ? ";
        Object[] params = new Object[]{amount, amount, accountNumber.trim()};
        SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, params);

        while (queryForRowSet.next()) {
            mandateVo = new MandateVo();
            mandateVo.setAuth1(queryForRowSet.getString("auth1"));
            mandateVo.setOperator(queryForRowSet.getString("operator"));
            mandateVo.setAuth2(queryForRowSet.getString("auth2"));
        }
        System.out.println("mandate: " + mandateVo);
        return mandateVo;

    }

    @Override
    public int getTrxId() {
        String cmdQuery = "select trans_count from trans_id_count";
        int count = jt.queryForInt(cmdQuery);
        count++;
        String updateQuery = "update trans_id_count set trans_count = '" + count + "'";
        int result = jt.update(updateQuery);
        System.out.println("Was transId successfully renewed? 1 is successfull, otherwise something went wrong: " + result);

        return --count;

    }

    public String getDailyTransactionLimit() {
        String corpDailyLimit = "";
        String selectSqlCommand = "select value from ebanking_params where keys = 'CORP_DAILY_LIMIT'";

        SqlRowSet queryForRowSet = jt.queryForRowSet(selectSqlCommand);
        while (queryForRowSet.next()) {
            corpDailyLimit = queryForRowSet.getString("value");
        }

        System.out.println("corp daily limit retrieved from FundTransferCtrller updateDailyTransactionLimit method: " + corpDailyLimit);
        return corpDailyLimit;
    }

    @Override
    public int logFundTransfer(ftVo ftVo) {
        String init_time = "SYSDATE";
        String columns = "trans_id, initiator_id, amount, init_account,  ben_account, "
                + "narration, auth1, auth1_action, operator, auth2,auth2_action, trx_type, bene_bank, "
                + "bene_acc_name, "
                + "init_time, auth1_comment, auth2_comment, post_status";
        String insertCmd = "insert into fund_transfer (" + columns + ") values('"
                + ftVo.getTrans_id() + "','"
                + ftVo.getInt_id() + "','"
                + String.valueOf(ftVo.getAmt()) + "','"
                + ftVo.getInit_acc() + "','"
                + ftVo.getBen_acc() + "','"
                + ftVo.getNarration() + "','"
                + ftVo.getAuth1() + "','"
                + ftVo.getAuth1_action() + "','"
                + ftVo.getOperator() + "','"
                + ftVo.getAuth2() + "','"
                + ftVo.getAuth2_action() + "','"
                + ftVo.getTrxType() + "','"
                + ftVo.getBene_bank() + "','"
                + ftVo.getBene_acc_name() + "',"
                + init_time + ",'"
                + "" + "','"
                + "" + "','"
                + ""
                + "')";

        System.out.println("query for logging fund transfer: " + insertCmd);
        int result = jt.update(insertCmd);

        return result;
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfONLYOperator(String userId) {
        System.out.println("user id for fetching authorized ONLY fund transfers in getAuthorizedFundTrxOfONLYOperator method: "
                + userId);
        List<ftVo> fTVoList = new ArrayList<>();
        String cmdQuery = "select init_account, ben_account, amount, trans_id, trx_type, narration, bene_bank from fund_transfer where"
                + " (post_status = ?) and (initiator_id = ? ) and (AUTH1_ACTION=?) and operator=?";
        Object[] params = new Object[]{"pending", userId, "APPROVED", "ONLY"};

        SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, params);

        while (queryForRowSet.next()) {
            ftVo fTVo = new ftVo();
            fTVo.setAcc(queryForRowSet.getString("init_account"));
            fTVo.setBen_acc(queryForRowSet.getString("ben_account"));
            fTVo.setAmt(Double.parseDouble(queryForRowSet.getString("amount")));
            fTVo.setTrans_id(queryForRowSet.getString("trans_id"));
            fTVo.setTrxType(queryForRowSet.getString("trx_type"));
            fTVo.setNarration(queryForRowSet.getString("narration"));
            fTVo.setBene_bank(queryForRowSet.getString("bene_bank"));
            fTVo.setBene_bank_code(fTVo.getBene_bank());
            fTVoList.add(fTVo);
        }

        return fTVoList;
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfOROperator(String userId) {
        System.out.println("user id for fetching authorized ONLY fund transfers in getAuthorizedFundTrxOfOROperator method: "
                + userId);
        List<ftVo> fTVoList = new ArrayList<>();
        String cmdQuery = "select  init_account, ben_account, amount, trans_id, trx_type, narration, bene_bank from fund_transfer"
                + " where (operator = ?) and  (auth1_action = ?) and (post_status = ?) and"
                + " (initiator_id = ?)";
        Object[] params = new Object[]{"OR", "APPROVED", "pending", userId};

        SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, params);

        while (queryForRowSet.next()) {
            ftVo fTVo = new ftVo();
            fTVo.setAcc(queryForRowSet.getString("init_account"));
            fTVo.setBen_acc(queryForRowSet.getString("ben_account"));
            fTVo.setAmt(Double.parseDouble(queryForRowSet.getString("amount")));
            fTVo.setTrans_id(queryForRowSet.getString("trans_id"));
            fTVo.setTrxType(queryForRowSet.getString("trx_type"));
            fTVo.setNarration(queryForRowSet.getString("narration"));
            fTVo.setBene_bank(queryForRowSet.getString("bene_bank"));
            fTVo.setBene_bank_code(fTVo.getBene_bank());
            fTVoList.add(fTVo);
        }

        return fTVoList;
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfAndOperator(String userId) {
        System.out.println("user id in getAuthorizedFundTrxOfAndOperator method: " + userId);
        List<ftVo> fTVoList = new ArrayList<>();
        String cmdQuery = "select  init_account, ben_account, amount, trans_id, trx_type, narration, bene_bank"
                + " from fund_transfer"
                + " where (operator = ?) and  (auth1_action = ?) and (AUTH2_ACTION = ?)"
                + " and (post_status = ?) and (initiator_id = ?) ";
        Object[] params = new Object[]{"AND", "APPROVED", "APPROVED", "pending", userId};

        SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, params);

        while (queryForRowSet.next()) {
            ftVo fTVo = new ftVo();
            fTVo.setAcc(queryForRowSet.getString("init_account"));
            fTVo.setBen_acc(queryForRowSet.getString("ben_account"));
            fTVo.setAmt(Double.parseDouble(queryForRowSet.getString("amount")));
            fTVo.setTrans_id(queryForRowSet.getString("trans_id"));
            fTVo.setTrxType(queryForRowSet.getString("trx_type"));
            fTVo.setNarration(queryForRowSet.getString("narration"));
            fTVo.setBene_bank(queryForRowSet.getString("bene_bank"));
            fTVo.setBene_bank_code(fTVo.getBene_bank());
            fTVoList.add(fTVo);
        }

        return fTVoList;
    }

    public String getTransactionThreshold(String customerNo) {
        String cmdQuery = "select transaction_threshold from onboarded_clients where customer_no=?";
        Object[] params = new Object[]{customerNo};

        return jt.queryForObject(cmdQuery, params, String.class);

    }

    public String getBankCode(String benbank) {
        System.out.println("ban bank in get bank code method: " + benbank);
        String cmdQuery = "select  bank_code from bank_list where bank_name=?";
        Object[] params = new Object[]{benbank};

        return jt.queryForObject(cmdQuery, String.class);
    }

    @Override
    public ftVo getFundTransferByTrxId(String transId) {
        System.out.println("transId in getFundTransferByTrxId: " + transId);
        String cmdQuery = "select initiator_id, init_account, ben_account, amount, narration, auth1, auth1_action, auth2, auth2_action,"
                + " auth1_comment, auth2_comment, operator, trx_type, "
                + " bene_acc_name, bene_bank  from fund_transfer where trans_id = ?";
        ftVo ftVoObj = (ftVo) jt.queryForObject(cmdQuery, new Object[]{transId}, new FtVoRowMapper());

        return ftVoObj;
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth1(String transId) {
        System.out.println("transId in getFundTransferByTrxIdAndAuth1: " + transId);
        String cmdQuery = "select initiator_id, init_account, ben_account, amount, narration, auth1, auth1_action, auth2, auth2_action,"
                + " auth1_comment, auth2_comment, operator, trx_type, "
                + " bene_acc_name, bene_bank  from fund_transfer where trans_id = ? and auth1_action = 'pending'";
        ftVo ftVoObj = (ftVo) jt.queryForObject(cmdQuery, new Object[]{transId}, new FtVoRowMapper());

        return ftVoObj;
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth2(String transId) {
        System.out.println("transId in getFundTransferByTrxIdAndAuth2: " + transId);
        String cmdQuery = "select initiator_id, init_account, ben_account, amount, narration, auth1, auth1_action, auth2, auth2_action,"
                + " auth1_comment, auth2_comment, operator, trx_type, "
                + " bene_acc_name, bene_bank from fund_transfer where trans_id = ?"
                + " and auth2_action = 'pending' and (auth1_action = 'APPROVED' or auth1_action = 'pending')";
        ftVo ftVoObj = (ftVo) jt.queryForObject(cmdQuery, new Object[]{transId}, new FtVoRowMapper());

        return ftVoObj;
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth2withOR(String transId) {
        System.out.println("transId in getFundTransferByTrxIdAndAuth2: " + transId);
        String cmdQuery = "select initiator_id, init_account, ben_account, amount, narration, auth1, auth1_action, auth2, "
                + " auth2_action, auth1_comment, auth2_comment, operator, trx_type, bene_acc_name, bene_bank "
                + " from fund_transfer where trans_id = ?"
                + " and auth2_action = 'pending' and auth1_action = 'pending' and operator = 'OR'";
        ftVo ftVoObj = (ftVo) jt.queryForObject(cmdQuery, new Object[]{transId}, new FtVoRowMapper());

        return ftVoObj;
    }

    @Override
    public int processFTByAuth1(String auth1, String action, String auth1Comment, String trxId) {
        System.out.println("param in processFTByAuth1: " + action + ", " + auth1Comment + ", " + trxId);
        String auth1_time = "SYSDATE";
        String cmdQuery = "update fund_transfer set auth1 = ?, auth1_action = ? , auth1_comment = ?, auth1_time = " + auth1_time
                + " where trans_id = ?";
        Object[] params = new Object[]{auth1, action, auth1Comment, trxId};

        int result = jt.update(cmdQuery, params);
        return result;
    }

    @Override
    public int processFTByAuth2(String auth2, String action, String auth2Comment, String trxId) {
        System.out.println("param in processFTByAuth2: " + action + ", " + auth2Comment + ", " + trxId);
        String auth2_time = "SYSDATE";
        String cmdQuery = "update fund_transfer set auth2 = ?, auth2_action = ? , auth2_comment = ?, auth2_time =" + auth2_time
                + " where trans_id = ?";
        Object[] params = new Object[]{auth2, action, auth2Comment, trxId};

        int result = jt.update(cmdQuery, params);

        return result;
    }

    @Override
    public int updatePostStatus(String trxId) {
        System.out.println("param in updatePostStatus: " + trxId);
        String operator = "";
        String cmdQuery = "select operator from fund_transfer where trans_id = ?";
        SqlRowSet queryForRowSet = jt.queryForRowSet(cmdQuery, new Object[]{trxId.trim()});

        while (queryForRowSet.next()) {
            operator = queryForRowSet.getString("operator");
        }

        String buildQuery = "";
        if (operator.equalsIgnoreCase("AND")) {
            buildQuery = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED' and auth2_action = 'APPROVED' and trans_id = ?";

        } else if (operator.equalsIgnoreCase("ONLY")) {
            buildQuery = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED'  and trans_id = ?";

        } else if (operator.equalsIgnoreCase("OR")) {
            buildQuery = "update fund_transfer set post_status = 'pending' where auth1_action = 'APPROVED' or auth2_action = 'APPROVED' and trans_id = ?";
        }
        return jt.update(buildQuery, new Object[]{trxId});

    }

    @Override
    public boolean updatePostStatusByTrxId(String trxId) {
        System.out.println("param in updatePostStatusByTrxId: " + trxId);
        String cmdQuery = "update fund_transfer set post_status=? where trans_id = ?";
        Object[] param = new Object[]{"processed", trxId};

        int result = jt.update(cmdQuery, param);
        System.out.println("result of updating post status in fund transfer table after succcessful transfer transaction: " + result);
        return (result == 1) ? true : false;
    }

    public String getThirdPartySum(String accNo) {
        System.out.println("*** about to get total amount for third party ft with the following parameters ***");
        System.out.println("accNo: " + accNo);
        String totalAmount = "";
        String queryString = "select sum(amount) as sum_amount, cast (transaction_time as date)  from fundtransfer_log where "
                + " (cast (transaction_time as date) = TO_DATE(TO_CHAR(sysdate, 'MM/DD/YYYY'), 'MM/DD/YYYY')) "
                + " and (ft_debit_acc = ?) "
                + " and LFT = 'N' "
                + " group by amount, transaction_time ";

        System.out.println("query string: " + queryString);
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        Connection con = null;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            if (con.isClosed() || con == null) {
                con = com.fasyl.ebanking.db.DataBase.getConnection();
                System.out.println("con is closed getting con again...");
            } else {
                System.out.println("con is not null or empty either: " + con);
            }
            pstmt = con.prepareStatement(queryString);
            pstmt.setString(1, accNo);

            rs = pstmt.executeQuery();

            if (rs == null) {
                System.err.println("*** result set is null ***");
            }

            if (rs.next()) {
                totalAmount = rs.getString("sum_amount");
                System.out.println("3rd party total sum: " + totalAmount);
            } else {
                totalAmount = "0";
            }
            if (!rs.isBeforeFirst()) {
                System.out.println("No data");
                totalAmount = "0";
            }

        } catch (Throwable se) {
            System.err.println("error in getting total sum for third party transfer: " + se.getMessage());
            se.printStackTrace(System.out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return totalAmount;
    }

    public String getInterBankSum(String accNo) {
        System.out.println("*** about to get total amount for inter bank ft with the following parameters ***");
        System.out.println("accNo: " + accNo);
        String totalAmount = "";
        String queryString = "select  sum(amount) as sum_amount, cast (transaction_time as date) from nip_successful_transactions "
                + " where (cast (transaction_time as date) = TO_DATE(TO_CHAR(sysdate, 'MM/DD/YYYY'), 'MM/DD/YYYY')) "
                + " and (ft_debit_acc = ? ) "
                + " group by amount, transaction_time";

        System.out.println("query string: " + queryString);
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            if (con.isClosed() || con == null) {
                con = com.fasyl.ebanking.db.DataBase.getConnection();
                System.out.println("con is closed getting con again...");
            } else {
                System.out.println("con is not null or empty either: " + con);
            }
            pstmt = con.prepareStatement(queryString);
            pstmt.setString(1, accNo);

            rs = pstmt.executeQuery();

            if (rs == null) {
                System.err.println("*** result set is null ***");
            }

            if (rs.next()) {
                totalAmount = rs.getString("sum_amount");
                System.out.println("inter bank total sum: " + totalAmount);
            } else {
                totalAmount = "0";
            }
            if (!rs.isBeforeFirst()) {
                System.out.println("No data");
                totalAmount = "0";
            }

        } catch (Throwable se) {
            System.err.println("error in getting total sum for inter-bank transfer: " + se.getMessage());
            se.printStackTrace(System.out);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return totalAmount;
    }

}
