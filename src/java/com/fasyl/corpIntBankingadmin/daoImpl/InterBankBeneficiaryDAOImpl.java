package com.fasyl.corpIntBankingadmin.daoImpl;

import com.fasyl.corpIntBankingadmin.infrastructure.ConnectionManager;
import com.fasyl.corpIntBankingadmin.vo.BankVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.fasyl.corpIntBankingadmin.dao.InterBankBeneficiaryDAO;
import com.fasyl.ebanking.dto.InterBeneficiaryDto;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 *
 * @author kingsley
 */
public class InterBankBeneficiaryDAOImpl implements InterBankBeneficiaryDAO {

    final static Logger logger = Logger.getLogger(UserAccessDAOImpl.class);
    public DataSource ds;
    public JdbcTemplate jt;

    public InterBankBeneficiaryDAOImpl() {
        try {
            ds = ConnectionManager.getInstance().getTBMBDatasource();
            jt = new JdbcTemplate(ds);
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.error("error occured here trying to initialize ds and jt: " + ex.getMessage());
        }
    }

    @Override
    public List<BankVo> getBankList() {
        logger.debug("About to fetch bank list from db");
        List bankList = new ArrayList();
        String sqlCmd = "select * from bank_list";

        bankList = jt.query(sqlCmd, new BeanPropertyRowMapper(BankVo.class));

        return bankList;
    }

    @Override
    public String getDebitAccNo(String custNo) {
        logger.debug("About to get debit acc no in getDebitAccNo method ");
        String sqlCmd = "select cust_acct from cust_acct where custno=? and rownum < 2";
        String debitAccNo = "";
        Object[] param = new Object[]{custNo};

        SqlRowSet queryForRowSet = jt.queryForRowSet(sqlCmd, param);
        if (queryForRowSet.next()) {
            debitAccNo = queryForRowSet.getString("cust_acct");
        }
        return debitAccNo;

    }

    @Override
    public String getCustNo(String customerName) {
        logger.debug("About to get cust no in getCustNo method : " + customerName);
        String sqlCmd = "select customer_no from sttm_customer where customer_name1 = ? and rownum < 2";
        String debitAccNo = "";
        Object[] param = new Object[]{customerName};

        SqlRowSet queryForRowSet = jt.queryForRowSet(sqlCmd, param);
        if (queryForRowSet.next()) {
            debitAccNo = queryForRowSet.getString("customer_no");
        }
        return debitAccNo;

    }

    @Override
    public int createInterBankBeneficairy(InterBeneficiaryDto iBenDto) {
        String addedDate = "SYSDATE";
        String insertSql = "insert into eb_beneft_details ("
                + "beneficiary_id,"
                + "beneficiary_name,"
                + "customer_no,"
                + "destination_acct,"
                + "beneficiary_type,"
                + "added_date,"
                + "status,"
                + "beneftgrpname,"
                + "benbank,"
                + "ccy,"
                + "amount)"
                + " values( " + System.currentTimeMillis() + ",'" + iBenDto.getBeneficiary_name() + "','"
                + iBenDto.getCustomer_no() + "','" + iBenDto.getDestination_acct() + "','" + "IBB" + "',"
                + addedDate + ",'" + "1" + "','" + "Individual" + "','" + iBenDto.getBenBank() + "','"
                + "NGN" + "','" + iBenDto.getAmount() + "')";

        System.out.println("query : " + insertSql);
        Object[] params = new Object[]{System.currentTimeMillis(), iBenDto.getBeneficiary_name(),
            iBenDto.getCustomer_no(), iBenDto.getDestination_acct(), "IBB", addedDate, "1", "Individual",
            iBenDto.getBenBank(), "NGN", iBenDto.getAmount()};

        int rowCount = 0;
        try {
            rowCount = jt.update(insertSql /*, params*/);
        } catch (Exception ex) {
            System.err.println("error: " + ex.getMessage());
        }

        logger.debug("number of rows inserted in createInterBankBeneficairy method: " + rowCount);
        return rowCount;
    }

    @Override
    public List<InterBeneficiaryDto> getInterBenList(String clientName) {
        logger.debug("About to fetch inter bank bene list from db");
        String custNo = getCustNo(clientName);
        List bankList = new ArrayList();
        String sqlCmd = "select eb.beneficiary_id,eb.beneficiary_name,eb.customer_no,eb.destination_acct,eb.beneficiary_type,"
                + "eb.amount,bl.bank_name as benbank"
                + " from eb_beneft_details eb"
                + " inner join bank_list bl "
                + " on bl.bank_code = eb.benbank "
                + " where eb.beneficiary_type=? and eb.customer_no = ?";

        Object[] params = new Object[]{"IBB", custNo};
        bankList = jt.query(sqlCmd, params, new BeanPropertyRowMapper(InterBeneficiaryDto.class));

        return bankList;
    }

    @Override
    public int deleteInterBankBen(String benId) {
        logger.debug("About to delete ben in deleteInterBankBen method: " + benId);
        String sqlCmd = "delete from eb_beneft_details where beneficiary_id = ?";

        int result = jt.update(sqlCmd, new Object[]{benId});

        return result;
    }

    @Override
    public int updateInterBankBen(String benId, String amt) {
        logger.debug("About to update ben in updateInterBankBen method: " + benId);
        String modDate = "SYSDATE";
        String sqlCmd = "update eb_beneft_details set amount ='" + amt + "', modified_date = " + modDate +  " where beneficiary_id = '" + benId + "'";

        int result = jt.update(sqlCmd /*, new Object[]{amt, modDate, benId}*/);

        return result;
    }

}
