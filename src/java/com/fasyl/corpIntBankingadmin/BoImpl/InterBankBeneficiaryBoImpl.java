/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.InterBankBeneficiaryBo;
import com.fasyl.corpIntBankingadmin.dao.InterBankBeneficiaryDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.InterBankBeneficiaryDAOImpl;
import com.fasyl.corpIntBankingadmin.vo.BankVo;
import com.fasyl.ebanking.dto.InterBeneficiaryDto;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class InterBankBeneficiaryBoImpl implements InterBankBeneficiaryBo {

    final static Logger logger = Logger.getLogger(PasswordChangeBoImpl.class);
    private InterBankBeneficiaryDAO interBankBeneficiaryDAO;

    public InterBankBeneficiaryBoImpl() {
        interBankBeneficiaryDAO = new InterBankBeneficiaryDAOImpl();
    }

    
    @Override
    public List<BankVo> getBankList() {
        return interBankBeneficiaryDAO.getBankList();
    }

    @Override
    public String getDebitAccNo(String userId) {
        return interBankBeneficiaryDAO.getDebitAccNo(userId);
    }

    @Override
    public int createInterBankBeneficairy(InterBeneficiaryDto interBeneficiaryDto) {
        return interBankBeneficiaryDAO.createInterBankBeneficairy(interBeneficiaryDto);
    }

    @Override
    public String getCustNo(String userId) {
        return interBankBeneficiaryDAO.getCustNo(userId);
    }

    @Override
    public List<InterBeneficiaryDto> getInterBenList(String userId) {
        return interBankBeneficiaryDAO.getInterBenList(userId);
    }

    @Override
    public int deleteInterBankBen(String benId) {
        return interBankBeneficiaryDAO.deleteInterBankBen(benId);
    }

    @Override
    public int updateInterBankBen(String benId, String amt) {
        return interBankBeneficiaryDAO.updateInterBankBen(benId, amt);
    }

}
