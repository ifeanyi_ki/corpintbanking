/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.OnboardCustomerBo;
import com.fasyl.corpIntBankingadmin.dao.OnboardCustomerDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.OnboardCustomerDAOImpl;
import com.fasyl.corpIntBankingadmin.vo.CustomerVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class OnboardCustomerBoImpl implements OnboardCustomerBo{

    final static Logger logger = Logger.getLogger(UserAccessBoImpl.class);
    private OnboardCustomerDAO onboardCustomerDAO;

    public OnboardCustomerBoImpl() {
        try {
            onboardCustomerDAO = new OnboardCustomerDAOImpl();
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.error("error creating usercreationdao in usercreationboimpl: " + ex.getMessage());
        }
    }
    
    @Override
    public List<String> searchUnboardedCustomerNameList(String customerName) {
        return onboardCustomerDAO.searchUnBoardedCustomerNameList(customerName);
    }

    @Override
    public CustomerVo getCustomerDetails(String client_name) {
        return onboardCustomerDAO.getCustomerDetails(client_name);
    }

    @Override
    public int onboardClient(String customerNo) {
        return onboardCustomerDAO.onboardClient(customerNo);
    }

    @Override
    public List<String> getOnboardedClientList() {
        return onboardCustomerDAO.getOnboardedClientList();
    }
    
}
