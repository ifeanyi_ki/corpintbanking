package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.UserCreationBo;
import com.fasyl.corpIntBankingadmin.dao.UserCreationDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.UserCreationDAOImpl;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class UserCreationBoImpl implements UserCreationBo {

    final static Logger logger = Logger.getLogger(UserAccessBoImpl.class);
    private UserCreationDAO userCreationDAO;

    public UserCreationBoImpl() {
        try {
            userCreationDAO = new UserCreationDAOImpl();
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.error("error creating usercreationdao in usercreationboimpl: " + ex.getMessage());
        }
    }

    @Override
    public int createUser(UserVo userVo) {
        int rowCount = userCreationDAO.createUser(userVo);
        return rowCount;
    }

    @Override
    public int updateUser(UserVo userVo) {
        int rowCount = userCreationDAO.updateUser(userVo);
        return rowCount;
    }

    @Override
    public List<String> getUserByStatus() {
        return userCreationDAO.getUserByStatus();
    }

    @Override
    public int authoriseAdminCreation(UserVo userVo, String authId) {
        return userCreationDAO.authoriseAdminCreation(userVo, authId);
    }

    @Override
    public int createUserX(UserVo userVo, String userid, String user_id) {
        logger.debug("*** param values in createUserX ***");
        logger.debug("userid : " + userid);
        logger.debug("user_id : " + user_id);
        int affectedRows = 0;
        try {
            affectedRows = new UserCreationDAOImpl().createUserX(userVo, userid, user_id);
            return affectedRows;
        } catch (IOException | SQLException | PropertyVetoException ex) {
            logger.debug("error trying to createUserX in usercreationboimpl: " + ex.getMessage());
        }
        return affectedRows;
    }

}
