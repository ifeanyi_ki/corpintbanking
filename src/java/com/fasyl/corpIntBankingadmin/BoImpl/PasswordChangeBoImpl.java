package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.PasswordChangeBo;
import com.fasyl.corpIntBankingadmin.dao.PasswordChangeDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.PasswordChangeDAOImpl;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class PasswordChangeBoImpl implements PasswordChangeBo {

    final static Logger logger = Logger.getLogger(PasswordChangeBoImpl.class);
    private PasswordChangeDAO passwordChangeDao;

    public PasswordChangeBoImpl() {
        passwordChangeDao = new PasswordChangeDAOImpl();
    }

    @Override
    public List<String> getListofPendingPasswordChangeRequest(String userid) {
        return passwordChangeDao.getListofPendingPasswordChangeRequest(userid);
    }

    @Override
    public int authorisePasswordChange(String userid, String authId) {
        int result;

        result = passwordChangeDao.updatePasswordwithNewPassword(userid);
        logger.debug("result of updatePasswordwithNewPassword in passwordchangeboimpl is: " + result);

        if (result == 1) { //1 equals success
            result = passwordChangeDao.approvePasswordChangeRequest(userid, authId);
        }

        return result;
    }

    @Override
    public int declinePasswordChange(String userid, String authId) {
        int result;

        result = passwordChangeDao.declinePasswordChangeRequest(userid, authId);
        logger.debug("result of declinePasswordChange in passwordchangeboimpl is: " + result);

        return result;

    }

    @Override
    public int resetPassword(String userId) {
        int result;
        result = passwordChangeDao.resetPassword(userId);
        logger.debug("result of resetPassword in passwordchangeboimpl is: " + result);
        return result;
    }

    @Override
    public int changePasswordRequest(String userId, String newPassword, String status) {
        int result = 0;
        //log password change request
        result = passwordChangeDao.logChangePasswordRequest(userId, newPassword, status);
        return result;
    }

    @Override
    public String getPasswordRequestChangeStatus(String userId) {
        return passwordChangeDao.getchangePasswordRequestStatus(userId);
    }

}
