package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.dao.UserAccessDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.UserAccessDAOImpl;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class UserAccessBoImpl implements UserAccessBo {

    final static Logger logger = Logger.getLogger(UserAccessBoImpl.class);
    public UserAccessDAO userAccessDao;

    public UserAccessBoImpl() {

        userAccessDao = new UserAccessDAOImpl();
    }

    @Override
    public boolean doesUserExist(String userId) {
        return userAccessDao.doesUserExist(userId);
    }

    @Override
    public int getLoggingCount(String userId) {
        return userAccessDao.getLoggingCount(userId);
    }

    @Override
    public boolean isUserActive(String userId) {
        return userAccessDao.isUserActive(userId);
    }

    @Override
    public String getRoleId(String userId) {
        return userAccessDao.getRoleId(userId);
    }

    @Override
    public int updateLastLoggingDate(String userId) {
        Date date = new Date(System.currentTimeMillis());
        return userAccessDao.updateLastLoggingDate(userId, date);
    }

    @Override
    public int updateLoggingCount(String userId, int loggingCount) {
        return userAccessDao.updateLoggingCount(userId, loggingCount);
    }

    @Override
    public String getLastLoginDate(String userId) {
        return userAccessDao.getLastLoginDate(userId);
    }

    @Override
    public String getPassword(String userId) {
        return userAccessDao.getPassword(userId);
    }

    @Override
    public int updatePasswordOnFirstTimeLogin(String userId, String password, String questionId, String answer) {
        return userAccessDao.updatePasswordOnFirstTimeLogin(userId, password, questionId, answer);
    }

    @Override
    public UserVo getUser(String userId) {
        return userAccessDao.getUser(userId);

    }

    @Override
    public List<String> getUserByIdorBylastOrFirstName(String userId, String clientName) {
        return userAccessDao.getUserByIdorBylastOrFirstName(userId, clientName);
    }

    @Override
    public List<String> getListOfDeclinedUsers(String userId) {
        return userAccessDao.getListOfDeclinedUsers(userId);
    }

    @Override
    public boolean isLoggedIn(String userId) {
        return userAccessDao.isLoggedIn(userId);
    }

    @Override
    public int updateLoginState(String userId, String status) {
        return userAccessDao.updateLoginState(userId, status);
    }

    @Override
    public boolean isFieldAvailable(String field) {
        return userAccessDao.isFieldAvailable(field);
    }

    @Override
    public boolean isFieldAvailable2(String field, String userid) {
        return userAccessDao.isFieldAvailable2(field, userid);
    }

    @Override
    public ArrayList getQuestions() {
        return userAccessDao.getQuestions();
    }

    @Override
    public String getPasswordReset(String userid) {
        return userAccessDao.getPasswordReset(userid);
    }

    @Override
    public int updatePasswordReset(String userId, String password) {
        return userAccessDao.updatePasswordReset(userId, password);
    }
}
