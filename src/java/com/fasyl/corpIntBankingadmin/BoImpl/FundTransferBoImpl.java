/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.BoImpl;

import com.fasyl.corpIntBankingadmin.bo.FundTransferBo;
import com.fasyl.corpIntBankingadmin.dao.FundTransferDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.FundTransferDaoImpl;
import com.fasyl.corpIntBankingadmin.vo.FundTransferVo;
import com.fasyl.corpIntBankingadmin.vo.MandateVo;
import com.fasyl.corpIntBankingadmin.vo.ftVo;
import java.util.List;

/**
 *
 * @author XNETTIntBanking
 */
public class FundTransferBoImpl implements FundTransferBo{
private FundTransferDAO fundTransferDao;


    public FundTransferBoImpl() {
        fundTransferDao = new FundTransferDaoImpl();
    }

    @Override
    public int getPendingFundTransferCount(String userId) {
        return fundTransferDao.getPendingFundTransferCount(userId);
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfers(String userId) {
        return fundTransferDao.getPendingFundTransfers(userId);
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfersByAuth1(String userId) {
        return fundTransferDao.getPendingFundTransfersByAuth1(userId);
    }

    @Override
    public List<FundTransferVo> getPendingFundTransfersByAuth2(String userId) {
        return fundTransferDao.getPendingFundTransfersByAuth2(userId);
    }

    @Override
    public List<String> getAccountListForAnInitiator(String userId) {
        return fundTransferDao.getAccountListForAnInitiator(userId);
    }

    @Override
    public String getTransactionLimit(String accountNo, String userId) {
        return fundTransferDao.getTransactionLimit(accountNo, userId);
    }

    @Override
    public MandateVo getMandate(String amount, String accountNumber) {
        return fundTransferDao.getMandate(amount, accountNumber);
    }

    @Override
    public int logFundTransfer(ftVo ftVo) {
        return fundTransferDao.logFundTransfer(ftVo);
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfONLYOperator(String userId) {
        return fundTransferDao.getAuthorizedFundTrxOfONLYOperator(userId);
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfOROperator(String userId) {
        return fundTransferDao.getAuthorizedFundTrxOfOROperator(userId);
    }

    @Override
    public List<ftVo> getAuthorizedFundTrxOfAndOperator(String userId) {
        return fundTransferDao.getAuthorizedFundTrxOfAndOperator(userId);
    }

    @Override
    public ftVo getFundTransferByTrxId(String transId) {
        return fundTransferDao.getFundTransferByTrxId(transId);
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth1(String transId) {
         return fundTransferDao.getFundTransferByTrxIdAndAuth1(transId);
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth2(String transId) {
        return fundTransferDao.getFundTransferByTrxIdAndAuth2(transId);
    }

    @Override
    public ftVo getFundTransferByTrxIdAndAuth2withOR(String transId) {
        return fundTransferDao.getFundTransferByTrxIdAndAuth2withOR(transId);
    }

    @Override
    public int processFTByAuth1(String auth1, String action, String auth1Comment, String trxId) {
        return fundTransferDao.processFTByAuth1(auth1, action, auth1Comment, trxId);
    }

    @Override
    public int processFTByAuth2(String auth2, String action, String auth1Comment, String trxId) {
        return fundTransferDao.processFTByAuth2(auth2, action, auth1Comment, trxId);
    }

    @Override
    public boolean updatePostStatusByTrxId(String trxId) {
        return fundTransferDao.updatePostStatusByTrxId(trxId);
    }
    
}
