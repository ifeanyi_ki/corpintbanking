package com.fasyl.corpIntBankingadmin.infrastructure;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

/**
 *
 * @author kingsley
 */
public class ConnectionManager {

    private static ConnectionManager conManager;
    public BasicDataSource bds;
    public BasicDataSource bdsTBMB;
    public BasicDataSource bdsTBMBFCC;
    private static DataSource datasource;
    private static DataSource datasource2;
    static Context initContext;

    static {
        try {

            // initContext  = new InitialContext();
            // envContext  = (Context)initContext;
            //datasource = oracle.jdbc.;
            datasource = (DataSource) new InitialContext().lookup("jdbc/OraclePool");
            if (datasource == null) {
                //throw new SQLException("****  datasource is null ****");
                System.out.println("****datasource is null****");
            }

        } catch (Exception e) {

            e.printStackTrace(System.out);
        }
    }

    static {
        try {

            // initContext  = new InitialContext();
            // envContext  = (Context)initContext;
            datasource2 = (DataSource) new InitialContext().lookup("jdbc/OraclePoolFCC");
//            con2 = datasource2.getConnection();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ConnectionManager() throws IOException, SQLException, PropertyVetoException {

//        if (Boolean.parseBoolean(PropertiesManager.getInstance().getProperty("UAT"))) {
//            System.out.println("hello");
//            bds = new BasicDataSource();
//            bds.setDriverClassName(PropertiesManager.getInstance().getProperty("jdbc.driver"));
//            bds.setUrl(PropertiesManager.getInstance().getProperty("jdbc.url"));
//            bds.setUsername(PropertiesManager.getInstance().getProperty("jdbc.user"));
//            bds.setPassword(PropertiesManager.getInstance().getProperty("jdbc.password"));
//
//            bds.setInitialSize(5);
//            bds.setMaxActive(5);
//            bds.setMaxIdle(20);
//            bds.setMinIdle(5);
//
//        } else {
        System.out.println("hello1");
        bdsTBMB = new BasicDataSource();
//            bdsTBMBFCC = new BasicDataSource();

        bdsTBMB.setDriverClassName(PropertiesManager.getInstance().getProperty("tbmb.driver"));
        bdsTBMB.setUrl(PropertiesManager.getInstance().getProperty("tbmb.url"));
        bdsTBMB.setUsername(PropertiesManager.getInstance().getProperty("tbmb.user"));
        bdsTBMB.setPassword(PropertiesManager.getInstance().getProperty("tbmb.password"));

//            bdsTBMBFCC.setDriverClassName(PropertiesManager.getInstance().getProperty("tbmbfcc.driver"));
//            bdsTBMBFCC.setUrl(PropertiesManager.getInstance().getProperty("tbmbfcc.url"));
//            bdsTBMBFCC.setUsername(PropertiesManager.getInstance().getProperty("tbmbfcc.user"));
//            bdsTBMBFCC.setPassword(PropertiesManager.getInstance().getProperty("tbmbfcc.password"));
        bdsTBMB.setInitialSize(5);
        bdsTBMB.setMaxActive(20);
        bdsTBMB.setMaxIdle(20);
        bdsTBMB.setMinIdle(5);

//            bdsTBMBFCC.setInitialSize(5);
//            bdsTBMBFCC.setMaxActive(5);
//            bdsTBMBFCC.setMaxIdle(20);
//            bdsTBMBFCC.setMinIdle(5);
//        }
        // the settings below are optional -- c3p0 can work with defaults
        //bds.setDefaultAutoCommit(true);
    }

    public static ConnectionManager getInstance() throws IOException, SQLException, PropertyVetoException {
        if (conManager == null) {
            conManager = new ConnectionManager();
            return conManager;
        } else {
            return conManager;
        }
    }

    public BasicDataSource getDatasource() throws SQLException {
        return this.bds;
    }

    public DataSource getTBMBDatasource() throws SQLException {
//        return this.bds;
//        return this.bdsTBMB
        return datasource;
    }

    public BasicDataSource getTBMBFCCDatasource() throws SQLException {
        return this.bdsTBMBFCC;
    }

}
