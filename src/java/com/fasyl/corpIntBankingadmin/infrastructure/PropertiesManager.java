package com.fasyl.corpIntBankingadmin.infrastructure;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class PropertiesManager {

    final static Logger logger = Logger.getLogger(PropertiesManager.class);
    private final Properties configProp = new Properties();

    private PropertiesManager() {
        //Private constructor to restrict new instances
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            configProp.load(classLoader.getResourceAsStream("app.properties"));
//            InputStream inStream = this.getClass().getResourceAsStream("/app.properties");
//            configProp.load(inStream);
        } catch (IOException | NullPointerException e) {
            System.out.println("Error while loading properties file: " + e.getMessage());
        }
    }

    //Bill Pugh Solution for singleton pattern
    private static class LazyHolder {

        private static final PropertiesManager INSTANCE = new PropertiesManager();
    }

    public static PropertiesManager getInstance() {
        return LazyHolder.INSTANCE;
    }

    public String getProperty(String key) {
        return configProp.getProperty(key);
    }

}
