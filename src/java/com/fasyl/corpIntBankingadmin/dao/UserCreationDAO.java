package com.fasyl.corpIntBankingadmin.dao;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.util.List;

/**
 *
 * @author kingsley
 */
public interface UserCreationDAO {
    
    public int createUser(UserVo userVo);
    
    public int updateUser(UserVo userVo);
    
    public List<String> getUserByStatus();
    
    public int authoriseAdminCreation(UserVo userVo, String authId);
}
