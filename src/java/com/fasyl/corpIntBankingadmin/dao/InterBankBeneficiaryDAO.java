/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.dao;

import com.fasyl.corpIntBankingadmin.vo.BankVo;
import com.fasyl.ebanking.dto.InterBeneficiaryDto;
import java.util.List;

/**
 *
 * @author kingsley
 */
public interface InterBankBeneficiaryDAO {

    public List<BankVo> getBankList();

    public String getDebitAccNo(String custNo);

    public int createInterBankBeneficairy(InterBeneficiaryDto interBeneficiaryDto);

    public String getCustNo(String customerName);

    public List<InterBeneficiaryDto> getInterBenList(String userId);

    public int deleteInterBankBen(String benId);
    
    public int updateInterBankBen(String benId, String amt);
}
