
package com.fasyl.corpIntBankingadmin.dao;

import com.fasyl.corpIntBankingadmin.vo.CustomerVo;
import java.util.List;

/**
 *
 * @author kingsley
 */
public interface OnboardCustomerDAO {
    public List<String> searchUnBoardedCustomerNameList(String customername);
    
    public CustomerVo getCustomerDetails(String client_name);
    
    public int onboardClient(String customerNo);
    
    public List<String> getOnboardedClientList();
    
}
