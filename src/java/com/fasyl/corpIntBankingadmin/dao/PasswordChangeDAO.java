package com.fasyl.corpIntBankingadmin.dao;

import java.util.List;

/**
 *
 * @author kingsley
 */
public interface PasswordChangeDAO {

    public List<String> getListofPendingPasswordChangeRequest(String userid);

    public int updatePasswordwithNewPassword(String userid);

    public int approvePasswordChangeRequest(String userid, String authId);

    public int declinePasswordChangeRequest(String userid, String authId);
    
    public int resetPassword(String userId);
    
    public int logChangePasswordRequest(String userId, String newPassword, String status);
    
    public String getchangePasswordRequestStatus(String userId);
}
