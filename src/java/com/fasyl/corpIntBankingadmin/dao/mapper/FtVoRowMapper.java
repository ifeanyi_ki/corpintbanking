/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.dao.mapper;

import com.fasyl.corpIntBankingadmin.vo.ftVo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kingsley
 */
public class FtVoRowMapper implements RowMapper{

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        ftVo fTVo = new ftVo();
        fTVo.setInt_id(rs.getString("initiator_id"));
        fTVo.setInit_acc(rs.getString("init_account"));
        fTVo.setBen_acc(rs.getString("ben_account"));
        fTVo.setAmt(Double.parseDouble(rs.getString("amount")));
        fTVo.setNarration(rs.getString("narration"));
        fTVo.setAuth1(rs.getString("auth1"));
        fTVo.setAuth1_action(rs.getString("auth1_action"));
        fTVo.setAuth2(rs.getString("auth2"));
        fTVo.setAuth2_action(rs.getString("auth2_action"));
        fTVo.setAuth1_comment(rs.getString("auth1_comment"));
        fTVo.setAuth2_comment(rs.getString("auth2_comment"));
        fTVo.setOperator(rs.getString("operator"));
        fTVo.setTrxType(rs.getString("trx_type"));
        fTVo.setBene_acc_name(rs.getString("bene_acc_name"));
        fTVo.setBene_bank(rs.getString("bene_bank"));
       
        return fTVo;
    }
    
}
