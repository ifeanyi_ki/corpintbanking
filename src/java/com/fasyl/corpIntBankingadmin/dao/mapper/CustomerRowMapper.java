/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.dao.mapper;

import com.fasyl.corpIntBankingadmin.vo.CustomerVo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kingsley
 */
public class CustomerRowMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        CustomerVo customerVo = new CustomerVo();
        customerVo.setCustomer_no(rs.getString("customer_no"));
        customerVo.setCustomerName1(rs.getString("customer_name1"));
        customerVo.setCustomer_type(rs.getString("customer_type"));
        customerVo.setAddress_line1(rs.getString("address_line1"));
        customerVo.setAddress_line2(rs.getString("address_line2"));
        customerVo.setAddress_line3(rs.getString("address_line3"));
        customerVo.setAddress_line4(rs.getString("address_line4"));
        customerVo.setCountry(rs.getString("country"));
        customerVo.setNationality(rs.getString("nationality"));

        return customerVo;
    }

}
