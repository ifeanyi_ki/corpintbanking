package com.fasyl.corpIntBankingadmin.dao.mapper;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author kingsley
 */
public class UserRowMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet rs, int i) throws SQLException {
        UserVo userVo = new UserVo();
        userVo.setUserName(rs.getString("userid"));
        userVo.setId(userVo.getUserName());
        userVo.setActive(rs.getString("active"));
        userVo.setAuthorizer(rs.getString("authorise_by"));
        userVo.setClient(rs.getString("organisation"));
        userVo.setComment(rs.getString("comments")); //change comment to comments
        userVo.setCreated_by(rs.getString("created_by"));
        //userVo.setDateCreated(rs.getDate("date_created"));
        userVo.setEmail(rs.getString("email"));
        userVo.setContact(rs.getString("email"));
        userVo.setFirstName(rs.getString("firstname"));
        userVo.setLastName(rs.getString("lastname"));
        userVo.setPassword(rs.getString("password"));
        userVo.setPhoneNumber(rs.getString("phone_number"));
        userVo.setRoleId(rs.getString("role_id"));
      
        return userVo;

    }

}
