package com.fasyl.corpIntBankingadmin.dao.mapper;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 *
 * @author kingsley
 */
public class UserResultSetExtractor implements ResultSetExtractor<Object>{

    @Override
    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
        UserVo userVo = new UserVo();
        
        userVo.setEmail(rs.getString("email"));
        userVo.setRole(rs.getString("role_id"));
        userVo.setPhoneNumber(rs.getString("phone_number"));
        userVo.setFirstName(rs.getString("firstname"));
        userVo.setLastName(rs.getString("lastname"));
        userVo.setClient(rs.getString("organisation"));
        userVo.setComment(rs.getString("comment"));
        userVo.setComment_by(rs.getString("authoriser_by"));
        userVo.setCreated_by(rs.getString("created_by"));
        userVo.setActive(rs.getString("active"));
        
        System.out.println("*** user vo properties in userresultsetextractor ***");
        System.out.println("email: " + userVo.getEmail());
        System.out.println("role_id: " + userVo.getRoleId());
        System.out.println("phone_number: " + userVo.getPhoneNumber());
        System.out.println("firstname: " + userVo.getFirstName());
        System.out.println("lastname: " + userVo.getLastName());
        System.out.println("organisation: " + userVo.getClient());
        System.out.println("comment: " + userVo.getComment());
        System.out.println("authoriser_by: " + userVo.getComment_by());
        System.out.println("created_by: " + userVo.getCreated_by());
        System.out.println("active: " + userVo.getActive());
        System.out.println("*****************************************************");
        
        return userVo;
    }
    
}
