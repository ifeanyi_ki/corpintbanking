/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl;
import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.FundTransferBo;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.ebanking.util.Utility;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
public class AccountController extends HttpServlet {

    final static Logger logger = Logger.getLogger(AccountController.class);
    AuditLogVo alVo;
    private UserAccessBo userAccessBo = new UserAccessBoImpl();
    AuditLog auditLog = new AuditLog();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String navigation = request.getParameter("taskId");
        logger.debug("navigation command in AccountController controller: " + navigation);
        HttpSession session = request.getSession(false);
        
        switch (navigation) {
            case "accounts_list":
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("ACCL");
                alVo.setTrxDesc("Accounts List");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                dispatch(request, response, "/" + request.getParameter("taskId") + ".jsp");
                break;
            case "back":
                String accNo = request.getParameter("accNo").trim();
                System.out.println("account no: " + accNo);
                request.setAttribute("account", accNo);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("BACCM");
                alVo.setTrxDesc("Back to account management page");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=manage_account.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/manage_account.jsp");
                break;
            case "init_accounts":
                String trxType = request.getParameter("trxType").trim();
                System.out.println("transaction type in Account Controller: " + trxType);

                FundTransferBo fundTransferBo = new FundTransferBoImpl();
                String user_id = (String) session.getAttribute("user_id");

                List<String> accList = fundTransferBo.getAccountListForAnInitiator(user_id);
                request.setAttribute("accList", accList);
                if (trxType.equalsIgnoreCase("thirdParty")) {
                    dispatch(request, response, "/332.jsp");
                } else if (trxType.equalsIgnoreCase("interBank")) {
                    dispatch(request, response, "/7015.jsp");
                } else {
                    System.out.println("unknown trx type in acc controller: " + trxType);
                }
                break;
            default:
                dispatch(request, response, "/" + request.getParameter("taskId") + ".jsp");
                logger.debug("*** unrecognized navigation command in UserCreation controller ***");
                break;
        }
    }
    
    
    
    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session){
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr());
        alVo.setUserId((String)session.getAttribute("user_id"));
        
        return alVo;
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
