package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.BoImpl.UserCreationBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.bo.UserCreationBo;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.util.Utility;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
@WebServlet(name = "UserAccessController", urlPatterns = {"/UserAccessController"})
public class UserAccessController extends HttpServlet {

    final static Logger logger = Logger.getLogger(UserAccessController.class);
    private UserAccessBo userAccessBo = new UserAccessBoImpl();
    private UserCreationBo userCreationBo = new UserCreationBoImpl();
    private List<String> userListByIdorForLName;
    AuditLogVo alVo;
    AuditLog auditLog = new AuditLog();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String navigation = request.getParameter("taskId");
        HttpSession session = request.getSession(false);

        switch (navigation) {
            case "edit_user":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("EU");
                alVo.setTrxDesc("edit user");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/search_user.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/search_user.jsp");
                break;
            case "search_user_details":

                userListByIdorForLName = userAccessBo.getUserByIdorBylastOrFirstName(request.getParameter("id_name").trim(),
                        ((UserVo) session.getAttribute("userVo")).getClient());
                request.setAttribute("userList", userListByIdorForLName);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("SUD");
                alVo.setTrxDesc("search user details");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/user_search_list.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/user_search_list.jsp");
                break;
            case "get_user_details":
                request = loadUser(request);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("GUD");
                alVo.setTrxDesc("get user details");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/edit_user.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/edit_user.jsp");
                break;
            case "movetopage":
                UserVo userReload = new UserVo();
                String page = request.getParameter("page").trim();
                userReload.setId(request.getParameter("user_id").trim());
                userReload.setEmail(request.getParameter("email").trim());
                userReload.setRoleId(request.getParameter("role_id").trim());
                userReload.setPhoneNumber(request.getParameter("mobile_number").trim());
                userReload.setFirstName(request.getParameter("first_name").trim());
                userReload.setLastName(request.getParameter("last_name").trim());
                userReload.setClient(request.getParameter("client").trim());
                userReload.setCreated_by(request.getParameter("created_by").trim());
                userReload.setActive(request.getParameter("active").trim());

                request.setAttribute("userVo", userReload);
                dispatch(request, response, "/" + page);
                break;
            case "submit_edit_user":
                UserVo userVo = new UserVo();
                userVo.setActive(request.getParameter("active").trim());
                userVo.setEmail(request.getParameter("email").trim());
                userVo.setPhoneNumber(request.getParameter("mobile_number").trim());
                userVo.setRoleId(request.getParameter("role_id").trim());
                userVo.setId(request.getParameter("user_id").trim());

                if (userAccessBo.isFieldAvailable2(userVo.getEmail(), userVo.getId())) {
                    request.setAttribute("result", "email supplied is already registered");
                    dispatch(request, response, "/result.jsp");
                    
                } else if (userAccessBo.isFieldAvailable2(userVo.getPhoneNumber(), userVo.getId())) {
                    request.setAttribute("result", "phone number supplied is already registered");
                    dispatch(request, response, "/result.jsp");
                    
                } else {
                    int result = userCreationBo.updateUser(userVo);

                    if (result == 1) { //1 equals success
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, session);
                        alVo.setTxnId("SEU");
                        alVo.setTrxDesc("submit edit user");
                        alVo.setProcessStatus("Y");
                        alVo.setResponseStatus("0");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + "/edit_user.jsp");
                        auditLog.log(alVo);
                        //*****************************************
                        request.setAttribute("result", request.getParameter("user_id") + ": successfully edited");
                        dispatch(request, response, "/result.jsp");

                    } else {
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, session);
                        alVo.setTxnId("SEU");
                        alVo.setTrxDesc("get user details");
                        alVo.setProcessStatus("N");
                        alVo.setResponseStatus("-1");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + "/edit_user.jsp");
                        alVo.setErrorMessage(request.getParameter("user_id") + ": cannot be edited now, please try again later");
                        auditLog.log(alVo);
                        //*****************************************
                        request.setAttribute("result", request.getParameter("user_id") + ": cannot be edited now, please try again later");
                        dispatch(request, response, "/result.jsp");

                    }
                }
                break;
            default:
                logger.debug("*** unrecognized navigation found in User Access controller ***");
                break;
        }

    }

    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session) {
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr());
        alVo.setUserId((String) session.getAttribute("user_id"));

        return alVo;
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    private HttpServletRequest loadUser(HttpServletRequest request) {
        UserVo userVo = new UserVo();
        String user_id = "";

        logger.debug("user_id in loaduser method in useraccessctller: " + request.getParameter("user_id"));
        logger.debug("client_name in loaduser method in useraccessctller: " + request.getParameter("client_name"));

        if (request.getParameter("user_id") != null) {
            user_id = request.getParameter("user_id").trim();
        }

        if (request.getParameter("client_name") != null) {
            user_id = request.getParameter("client_name").trim();
        }

        userVo = userAccessBo.getUser(user_id);
        userVo.setId(user_id);
        request.setAttribute("userid", user_id);
        request.setAttribute("user_id", user_id);

        request.setAttribute("userVo", userVo);

        return request;
    }

//      case "get_user_details":
//                request = loadUser(request);
//                dispatch(request, response, "/edit_user.jsp");
//                break;
//                
//            case "movetopage":
//                UserVo userReload = new UserVo();
//                String page = request.getParameter("page");
//                userReload.setId(request.getParameter("user_id"));
//                userReload.setEmail(request.getParameter("email"));
//                userReload.setRoleId(request.getParameter("role_id"));
//                userReload.setPhoneNumber(request.getParameter("mobile_number"));
//                userReload.setFirstName(request.getParameter("first_name"));
//                userReload.setLastName(request.getParameter("last_name"));
//                userReload.setClient(request.getParameter("client"));
//                userReload.setCreated_by(request.getParameter("created_by"));
//                userReload.setActive(request.getParameter("active"));
//                
//                request.setAttribute("userVo", userReload);
//                dispatch(request, response, "/" + page);
//                break;
//            case "submit_edit_user":
//                UserVo userVo = new UserVo();
//                userVo.setActive(request.getParameter("active").trim());
//                userVo.setEmail(request.getParameter("email").trim());
//                userVo.setPhoneNumber(request.getParameter("mobile_number").trim());
//                userVo.setRoleId(request.getParameter("role_id").trim());
//                userVo.setId(request.getParameter("user_id").trim());
//                
//                int result = userCreationBo.updateUser(userVo);
//                
//
//
//                if (result == 1) { //1 equals success
//                    request.setAttribute("result", request.getParameter("user_id") + ": successfully edited");
//                    dispatch(request, response, "/result.jsp");
//
//                } else {
//                    request.setAttribute("result", request.getParameter("user_id") + ": cannot be edited now, please try again later");
//                    dispatch(request, response, "/result.jsp");
//
//                }
//                break;
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
