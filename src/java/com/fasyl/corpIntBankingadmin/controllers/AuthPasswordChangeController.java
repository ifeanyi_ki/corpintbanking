package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.PasswordChangeBoImpl;
import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.PasswordChangeBo;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.daoImpl.PasswordChangeDAOImpl;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.Utility;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

@WebServlet(name = "AuthPasswordChangeController", urlPatterns = {"/AuthPasswordChangeController"})
public class AuthPasswordChangeController extends HttpServlet {

    final static Logger logger = Logger.getLogger(AuthPasswordChangeController.class);
    private UserAccessBo userAccessBo = new UserAccessBoImpl();
    private PasswordChangeBo passwordBo = new PasswordChangeBoImpl();
    private PasswordChangeDAOImpl passwordChangeDao = new PasswordChangeDAOImpl();
    AuditLogVo alVo;
    AuditLog auditLog = new AuditLog();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession(false);
        String navigation = request.getParameter("taskId");

      
        
        String userid = request.getParameter("id");
        String authId = (String) session.getAttribute("user_id");

        logger.debug("navigation command in AuthPasswordChange controller: " + navigation);

        switch (navigation) {
            //"authorize_passwd_change"
            case "change_pass":
                String confirmpasswd = request.getParameter("confirmpasswd").trim();
                String newPassword = request.getParameter("newpasswd").trim();
                String oldPassword = request.getParameter("oldpasswd").trim();

                System.out.println("newPassword: " + newPassword);
                System.out.println("confirm password: " + confirmpasswd);
                System.out.println("oldPassword: " + oldPassword);

                if (Encryption.encrypt(request.getParameter("oldpasswd").trim())
                        .equalsIgnoreCase(userAccessBo.getPassword((String) session.getAttribute("user_id")))) {

                    String check = passwordBo.getPasswordRequestChangeStatus((String) session.getAttribute("user_id"));

                    if ("".equals(check) || "Declined".equalsIgnoreCase(check)) {
                        //log change of password request
                        int result = passwordBo.changePasswordRequest((String) session.getAttribute("user_id"), Encryption.encrypt((String) request.getParameter("newpasswd")), "pending");

                        if (result == 1) {
                            //*****************************************
                            alVo = new AuditLogVo();
                            alVo = loadAuditLog(request, session);
                            alVo.setTxnId("PCHR");
                            alVo.setTrxDesc("Password change request");
                            alVo.setProcessStatus("Y");
                            alVo.setResponseStatus("0");
                            alVo.setTrxSeq(Utility.getReqId());
                            alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                            auditLog.log(alVo);
                            //*****************************************
                            request.setAttribute("result", "password change request has been queued up for authorization");
                        } else {
                            //*****************************************
                            alVo = new AuditLogVo();
                            alVo = loadAuditLog(request, session);
                            alVo.setTxnId("PCHR");
                            alVo.setTrxDesc("Password change request");
                            alVo.setProcessStatus("N");
                            alVo.setResponseStatus("-1");
                            alVo.setTrxSeq(Utility.getReqId());
                            alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                            alVo.setErrorMessage("error while processing password change request");
                            auditLog.log(alVo);
                            //*****************************************
                            request.setAttribute("result", "error while processing password change request");
                        }
                    } else if (check.equalsIgnoreCase("pending")) {
                        //*********************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, session);
                        alVo.setTxnId("PCHR");
                        alVo.setTrxDesc("Password change request");
                        alVo.setProcessStatus("N");
                        alVo.setResponseStatus("-1");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                        alVo.setErrorMessage("pending request already exists");
                        auditLog.log(alVo);
                        //*****************************************
                        request.setAttribute("result", "You have a request pending for authorisation. Please contact your authorizer to attend to your previous request. Thank you");
                    } else {
                        logger.debug("status of password change request for this userId " + session.getAttribute("user_id") + " is neither pending or empty");
                    }
                } else {
                    //*********************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("PCHR");
                    alVo.setTrxDesc("Password change request");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    alVo.setErrorMessage("Old password is not correct");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "Old password is not correct");
                }

                dispatch(request, response, "/result.jsp");

                break;

            case "authorize_passwd_change": //used
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("APCHR");
                alVo.setTrxDesc("Authorize password change request");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=/passwd_change_request_list.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/passwd_change_request_list.jsp");
                break;

            case "user_details":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("VUSRD");
                alVo.setTrxDesc("View user details");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=/view_user.jsp");
                auditLog.log(alVo);
                //*****************************************
                request = loadUser(request);
                dispatch(request, response, "/view_user.jsp");
                break;
            case "back":
                String page = request.getParameter("page");
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("BPCHRL");
                alVo.setTrxDesc("back to password change request list");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/" + page);
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/" + page);
                break;
            case "approve_pwd_change":
                logger.debug("userid in case approve_pwd_change in authpasswordchange ctrller: " + userid);
                logger.debug("authid in case approve_pwd_change in authpasswordchange ctrller: " + authId);

                int result = passwordBo.authorisePasswordChange(userid, authId);
                logger.debug("result of  passwordBo.authorisePasswordChange in authpasswordchangecontroller: " + result);

                switch (result) {
                    case 1:
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, session);
                        alVo.setTxnId("APCHRL");
                        alVo.setTrxDesc("password change successfully effected for user : " + userid);
                        alVo.setProcessStatus("Y");
                        alVo.setResponseStatus("0");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                        auditLog.log(alVo);
                        //*****************************************

                        String header = "Notification of Password change request on TrustBond Corporate Banking Platform";
                        String messageBody = "Hi, " + userid + " <br/>";
                        messageBody += "Your password change request has been approved on TrustBond Corporate Banking Platform." + " <br/>";
                        messageBody += " <br/>";
                        messageBody += "Thank you!" + " <br/>";
                        messageBody += " <br/>";
                        messageBody += " <br/>";
                        messageBody += "TrustBond Mortgage Bank plc";
                        String type = "text/html";
                        String fromMail = "ibanking@trustbondmortgagebankplc.com";
                        String toMail = passwordChangeDao.getUserEailForPassAuthNotification(userid);
                        sendmail mailSender = new sendmail();
                        System.out.println("******************************************");
                        System.out.println("message body: " + messageBody);
                        System.out.println("toemail: " + toMail);
                        System.out.println("fromMail: " + fromMail);
                        System.out.println("******************************************");
//                        boolean resultMail = mailSender.sendMessage(header, messageBody, "", type, fromMail, toMail);
//                        System.out.println("result of mail sending: " + resultMail); comment in
                        request.setAttribute("result", "password change successfully effected for user : " + userid);
                        dispatch(request, response, "/result.jsp");
                        break;
                    case -1:
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, session);
                        alVo.setTxnId("APCHRL");
                        alVo.setTrxDesc("authorisation of password change request");
                        alVo.setProcessStatus("N");
                        alVo.setResponseStatus("-1");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                        alVo.setErrorMessage("authorisation of password change failed for user : " + userid);
                        auditLog.log(alVo);
                        //*****************************************
                        request.setAttribute("result", "no pending password change request found for this user: " + userid);
                        dispatch(request, response, "/result.jsp");
                        break;
                    default:
                        request.setAttribute("result", "this action cannot be carried out now please try again later");
                        dispatch(request, response, "/result.jsp");
                        break;
                }
                break;

            case "decline_pwd_change":
                result = passwordBo.declinePasswordChange(userid, authId);
                if (result == 1) {//successfully declineds
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("DPCHR");
                    alVo.setTrxDesc("password change declined successfully effected for user : " + userid);
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************
                    String header = "Notification of Password change request on TrustBond Corporate Banking Platform";
                    String messageBody = "Hi, " + userid + " <br/>";
                    messageBody += "Your password change request was declined on TrustBond Corporate Banking Platform." + " <br/>";
                    messageBody += " <br/>";
                    messageBody += "Thank you!" + " <br/>";
                    messageBody += " <br/>";
                    messageBody += " <br/>";
                    messageBody += "TrustBond Mortgage Bank plc";
                    String type = "text/html";
                    String fromMail = "ibanking@trustbondmortgagebankplc.com";
                    String toMail = passwordChangeDao.getUserEailForPassAuthNotification(userid);
                    sendmail mailSender = new sendmail();
                    System.out.println("******************************************");
                    System.out.println("message body: " + messageBody);
                    System.out.println("toemail: " + toMail);
                    System.out.println("fromMail: " + fromMail);
                    System.out.println("******************************************");
//                    boolean resultMail = mailSender.sendMessage(header, messageBody, "", type, fromMail, toMail);
//                    System.out.println("result for sending mail : " + resultMail);
                    request.setAttribute("result", "password change declined for user : " + userid);
                    dispatch(request, response, "/result.jsp");
                } else { //failed to update password change request table
                    request.setAttribute("result", "this action cannot be carried out now please try again later");
                    dispatch(request, response, "/result.jsp");
                }
                break;

            case "reset_password": //used
                result = passwordBo.resetPassword(request.getParameter("user_id").trim());

                if (result == 1) {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("RPCHR");
                    alVo.setTrxDesc("password reset successfully effected for user : " + userid);
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************
                    String header = "Notification of Password change request on TrustBond Corporate Banking Platform";
                    String messageBody = "Hi, " + userid + " <br/>";
                    messageBody += "Your password has been reset on TrustBond Corporate Banking Platform." + " <br/>";
                    messageBody += "Please, log in and change your password on first login. ";
                    messageBody += " <br/>";
                    messageBody += "Thank you!" + " <br/>";
                    messageBody += " <br/>";
                    messageBody += " <br/>";
                    messageBody += "TrustBond Mortgage Bank plc";
                    String type = "text/html";
                    String fromMail = "ibanking@trustbondmortgagebankplc.com";
                    String toMail = request.getParameter("email");
                    sendmail mailSender = new sendmail();
                    System.out.println("******************************************");
                    System.out.println("message body: " + messageBody);
                    System.out.println("toemail: " + toMail);
                    System.out.println("fromMail: " + fromMail);
                    System.out.println("******************************************");
//                    boolean resultMail = mailSender.sendMessage(header, messageBody, "", type, fromMail, toMail);
//                    System.out.println("result of mail sending: " + resultMail); cooment in 
                    request.setAttribute("result", "password reset successfully effected for user : " + request.getParameter("user_id"));
                } else {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("DPCHR");
                    alVo.setTrxDesc("Decline password change request");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    alVo.setErrorMessage("password reset for " + request.getParameter("user_id").trim() + " cannot be carried out now. please contact system admin");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "password reset for " + request.getParameter("user_id") + " cannot be carried out now. please contact system admin");
                }
                dispatch(request, response, "/result.jsp");
                break;

            case "change_password":
                //*******************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("CHP");
                    alVo.setTrxDesc("Change password");
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************
                dispatch(request, response, "/change_password.jsp");
                break;

            default:
                logger.debug("*** unrecognized navigation command in authorisepasswordchang controller ***");
                break;
        }
    }

    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session) {
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr().trim());
        alVo.setUserId((String) session.getAttribute("user_id"));

        return alVo;
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private HttpServletRequest loadUser(HttpServletRequest request) {
        UserVo userVo = new UserVo();
        String user_id = "";

        logger.debug("user_id in loaduser method in authpasswordchangectrller: " + request.getParameter("user_id"));
        logger.debug("client_name in loaduser method in authpasswordchangectrller: " + request.getParameter("client_name"));

        if (request.getParameter("user_id") != null) {
            user_id = request.getParameter("user_id").trim();
        }

        if (request.getParameter("client_name") != null) {
            user_id = request.getParameter("client_name").trim();
        }

        userVo = userAccessBo.getUser(user_id);

        request.setAttribute("userid", user_id);
        request.setAttribute("user_id", user_id);

        request.setAttribute("userVo", userVo);

        return request;
    }
    
    

}
