package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.OnboardCustomerBoImpl;
import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.BoImpl.UserCreationBoImpl;
import com.fasyl.corpIntBankingadmin.bo.OnboardCustomerBo;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.bo.UserCreationBo;
import com.fasyl.corpIntBankingadmin.dao.UserAccessDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.UserAccessDAOImpl;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.main.sendmail;
import com.fasyl.ebanking.util.Utility;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author kingsley
 */
@WebServlet(name = "UserCreationController", urlPatterns = {"/UserCreationController"})
public class UserCreationController extends HttpServlet {

    final static Logger logger = Logger.getLogger(UserCreationController.class);
    private UserCreationBo userCreationBo = new UserCreationBoImpl();
    private UserAccessBo userAccessBo = new UserAccessBoImpl();
    private UserVo userVo = new UserVo();
    private List<String> userListByIdorForLName;
    sendmail mailSender;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        OnboardCustomerBo onboardCustomerBo = new OnboardCustomerBoImpl();
        UserAccessBo userAccessBo = new UserAccessBoImpl();
        HttpSession session = request.getSession(false);

        AuditLogVo alVo;
        AuditLog auditLog = new AuditLog();

        String navigation = request.getParameter("taskId");
        logger.debug("navigation command in UserCreation controller: " + navigation);

        switch (navigation) {
            case "create_client_user":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, request.getSession(false));
                alVo.setTxnId("CCU");
                alVo.setTrxDesc("create client user");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/CreateUser.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/CreateUser.jsp");
                break;
            case "create_admin_user":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, request.getSession(false));
                alVo.setTxnId("CCCU");
                alVo.setTrxDesc("confirm create client admin user creation");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/confirmAdminUserCreate.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/confirmAdminUserCreate.jsp");
                break;
            case "edit_admin_create":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, request.getSession(false));
                alVo.setTxnId("EU");
                alVo.setTrxDesc("Edit user");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + "/CreateUser.jsp");
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/CreateUser.jsp");
                break;
            case "Submit_admin_create":
                userVo = loadRequest(request);
                System.out.println("is userid already registered: " + userAccessBo.isFieldAvailable(userVo.getUserName()));
                if (userAccessBo.isFieldAvailable(userVo.getUserName())) {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, request.getSession(false));
                    alVo.setTxnId("SAUC");
                    alVo.setTrxDesc("submit admin user creation");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + "/CreateUser.jsp");
                    alVo.setErrorMessage("userid supplied is already registered");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "userid supplied is already registered");
                    dispatch(request, response, "/result.jsp");
                } //check integrity of phone number and email filed in the system
                //                System.out.println("is field available: " + userAccessBo.isFieldAvailable(userVo.getEmail()));
                else if (userAccessBo.isFieldAvailable(userVo.getEmail())) {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, request.getSession(false));
                    alVo.setTxnId("SAUC");
                    alVo.setTrxDesc("submit admin user creation");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + "/CreateUser.jsp");
                    alVo.setErrorMessage("email supplied is already registered");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "email supplied is already registered");
                    dispatch(request, response, "/result.jsp");
                } else if (userAccessBo.isFieldAvailable(userVo.getPhoneNumber())) {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, request.getSession(false));
                    alVo.setTxnId("SAUC");
                    alVo.setTrxDesc("submit admin user creation");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + "/CreateUser.jsp");
                    alVo.setErrorMessage("email supplied is already registered");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "phone number supplied is already registered");
                    dispatch(request, response, "/result.jsp");
                } else {
                    int result = userCreationBo.createUser(userVo);
                    if (result == 1) {
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, request.getSession(false));
                        alVo.setTxnId("SAUC");
                        alVo.setTrxDesc("submit admin user creation");
                        alVo.setProcessStatus("Y");
                        alVo.setResponseStatus("0");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                        auditLog.log(alVo);
                        //*****************************************
                        //***********notify the user ****************
                        String header = "Notification of Registration on TrustBond Corporate Banking Platform";
                        String messageBody = "Hi, " + request.getParameter("user_id") + " <br/>";
                        messageBody += "You have been created on TrustBond Corporate Banking Platform." + " <br/>";
                        messageBody += "user id :     " + request.getParameter("user_id") + " <br/>";
                        messageBody += "Please contact your admin for the default password" + " <br/>";
                        messageBody += " <br/>";
                        messageBody += "Thank you!" + " <br/>";
                        messageBody += " <br/>";
                        messageBody += " <br/>";
                        messageBody += "TrustBond Mortgage Bank plc";
                        String type = "text/html";
                        String fromMail = "ibanking@trustbondmortgagebankplc.com";
                        String toMail = request.getParameter("email");
                        mailSender = new sendmail();
                        System.out.println("******************************************");
                        System.out.println("message body: " + messageBody);
                        System.out.println("toemail: " + toMail);
                        System.out.println("fromMail: " + fromMail);
                        System.out.println("******************************************");
//                    boolean resultMail = mailSender.sendMessage(header, messageBody, "", type, fromMail, toMail); comment in
//                    boolean res = mailSender.sendMessage(msgHeader, msgBody, "", "text/html", fromEmail, toEmail);
                        //*******************************************

                        request.setAttribute("result", "user id : " + userVo.getId() + " successfully created");
                        dispatch(request, response, "/result.jsp");

                    } else {
                        //*****************************************
                        alVo = new AuditLogVo();
                        alVo = loadAuditLog(request, request.getSession(false));
                        alVo.setTxnId("SAUC");
                        alVo.setTrxDesc("submit admin user creation");
                        alVo.setProcessStatus("N");
                        alVo.setResponseStatus("-1");
                        alVo.setTrxSeq(Utility.getReqId());
                        alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                        alVo.setErrorMessage("user id : " + userVo.getId() + " cannot be created now please contact admin");
                        auditLog.log(alVo);
                        //*****************************************
                        request.setAttribute("result", "user id : " + userVo.getId() + " cannot be created now please contact admin");
                        dispatch(request, response, "/result.jsp");
                    }
                    break;
                }
            default:
                logger.debug("*** unrecognized navigation command in UserCreation controller ***");
                break;
        }

    }

    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session) {
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr());
        alVo.setUserId((String) session.getAttribute("user_id"));

        return alVo;
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    public UserVo loadRequest(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        logger.debug("*** request parameter in loadrequest method in UserCreation controller ***");
        logger.debug("user id: " + session.getAttribute("user_id"));
        logger.debug("first_name: " + request.getParameter("first_name"));
        logger.debug("last_name: " + request.getParameter("last_name"));
        logger.debug("mobile_number: " + request.getParameter("mobile_number"));
        logger.debug("email: " + request.getParameter("email"));
        logger.debug("**************************************************************");
        UserVo uv = new UserVo();
        uv.setCreated_by((String) session.getAttribute("user_id"));

        uv.setUserName(request.getParameter("user_id").trim());
        uv.setFirstName(request.getParameter("first_name").trim());
        uv.setId(request.getParameter("user_id").trim());
        uv.setLastName(request.getParameter("last_name").trim());
        uv.setPhoneNumber(request.getParameter("mobile_number").trim());
        uv.setEmail(request.getParameter("email").trim());
        uv.setClient(((UserVo) session.getAttribute("userVo")).getClient().trim());
        uv.setStatus("Approved");
        uv.setRoleId(request.getParameter("role_id").trim());
        return uv;
    }

    private HttpServletRequest loadUser(HttpServletRequest request) {
        UserVo userVo = new UserVo();
        String user_id = "";

        logger.debug("user_id in loaduser method in useraccessctller: " + request.getParameter("user_id"));
        logger.debug("client_name in loaduser method in useraccessctller: " + request.getParameter("client_name"));

        if (request.getParameter("user_id") != null) {
            user_id = request.getParameter("user_id").trim();
        }

        if (request.getParameter("client_name") != null) {
            user_id = request.getParameter("client_name").trim();
        }

        userVo = userAccessBo.getUser(user_id);

        request.setAttribute("userid", user_id);
        request.setAttribute("user_id", user_id);

        request.setAttribute("userVo", userVo);

        return request;
    }

//    case "create_bank_user_init":
//                dispatch(request, response, "/confirmBankUserCreate.jsp");
//                break;
//
//            case "edit":
//                dispatch(request, response, "/CreateBankUser.jsp");
//                break;
//
//            case "edit_user":
//                dispatch(request, response, "/search_user.jsp");
//                break;
//
//            case "search_user_details":
//                userListByIdorForLName = userAccessBo.getUserByIdorBylastOrFirstName(request.getParameter("id_name"));
//                request.setAttribute("userList", userListByIdorForLName);
//                dispatch(request, response, "/user_search_list.jsp");
//                break;
//
//            case "authorize_admin_client":
//                dispatch(request, response, "/pending_client_admin_create.jsp");
//                break;
//
//            case "create_client_admin":
//                request.setAttribute("clientList", onboardCustomerBo.getOnboardedClientList());
//                dispatch(request, response, "/CreateAdminUser.jsp");
//                break;
//
//            case "create_admin_user":
//                dispatch(request, response, "/confirmAdminUserCreate.jsp");
//                break;
//
//            case "edit_admin_create":
//                request.setAttribute("clientList", onboardCustomerBo.getOnboardedClientList());
//                dispatch(request, response, "/CreateAdminUser.jsp");
//                break;
//
//            case "authorise":
//                UserVo userVo = new UserVo();
//
//                userVo.setComment(request.getParameter("comment"));
//                userVo.setId(request.getParameter("id"));
//                userVo.setStatus(request.getParameter("action"));
//
//                HttpSession session = request.getSession(false);
//                int result = userCreationBo.authoriseAdminCreation(userVo, (String) session.getAttribute("user_id"));
//
//                if (result == 1) {//success
//                    if (request.getParameter("action").equalsIgnoreCase("Declined")) {
//                        
//                        request.setAttribute("result", "Client admin creation request declined.");
//                        dispatch(request, response, "/result.jsp");
//                    } else {
//                        
//                        request.setAttribute("result", "Client admin creation request authorized successfully..");
//                        dispatch(request, response, "/result.jsp");
//                    }
//                    request.setAttribute("result", "User creation authorisation failed, please contact admin.");
//                    dispatch(request, response, "/result.jsp");
//
//                } else {//error 
//                    request.setAttribute("result", "User creation authorisation failed, please contact admin.");
//                    dispatch(request, response, "/result.jsp");
//                }
//
//                break;
//
//            case "get_job_details":
//                String user_id = request.getParameter("client_name").trim();
//                request.setAttribute("userid", user_id);
//                request = loadUser(request);
//                System.err.println("client_name: " + user_id);
//                dispatch(request, response, "/admin_create_details.jsp");
//                break;
//
//            case "Submit_admin_create":
//
//                UserVo uv = loadRequest(request);
//
//                int rowCount = userCreationBo.createUser(uv);
//
//                if (rowCount > 0) {
//                    request.setAttribute("result", "user id : " + uv.getId() + " successfully created");
//                    dispatch(request, response, "/result.jsp");
//
//                } else {
//                    request.setAttribute("result", "user id : " + uv.getId() + " cannot be created now please contact admin");
//                    dispatch(request, response, "/result.jsp");
//                }
//
//                if (!navigation.equalsIgnoreCase("Submit_admin_create")) {
//                    dispatch(request, response, "/CreateAdminUser.jsp");
//                }
//                break;
//
//            case "correct_admin_create":
//
//                session = request.getSession(false);
//                userAccessDao = new UserAccessDAOImpl();
//                List<String> declinedUsersList = userAccessDao.getListOfDeclinedUsers((String) session.getAttribute("user_id"));
//                request.setAttribute("declinedUsersList", declinedUsersList);
//                dispatch(request, response, "/correction_list_admin_create.jsp");
//                break;
//
//            case "correct_admin_create_form":
//                user_id = request.getParameter("client_name").trim();
//                request.setAttribute("userid", user_id);
//                request = loadUser(request);
//                dispatch(request, response, "/CreateAdminUserForCorrection.jsp");
//
//                break;
//
//            case "resubmit_admin_create":
//                userVo = loadRequest(request);
//                userVo.setStatus("Pending");
//                result = userCreationBo.createUserX(userVo, request.getParameter("user_id"), request.getParameter("userid"));
//                logger.debug("result after creatUserX: " + result);
//
//                dispatch(request, response, "/correction_list_admin_create.jsp");
//                break;
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
