package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.FundTransferBoImpl;
import com.fasyl.corpIntBankingadmin.bo.FundTransferBo;
import com.fasyl.corpIntBankingadmin.dao.FundTransferDAO;
import com.fasyl.corpIntBankingadmin.daoImpl.FundTransferDaoImpl;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.corpIntBankingadmin.vo.MandateVo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.corpIntBankingadmin.vo.ftVo;
import com.fasyl.ebanking.logic.ProcessClass;
import com.fasyl.ebanking.util.Utility;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kingsley
 */
public class FUndTransferController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String navigation = request.getParameter("taskId");
        System.out.println("navigation command in FUndTransferController: " + navigation);
        HttpSession session = request.getSession(false);
        FundTransferBo fundTransferBo = new FundTransferBoImpl();

        FundTransferDAO fundTransferDao = new FundTransferDaoImpl();
        ProcessClass processClass = new ProcessClass();

        AuditLogVo alVo;
        AuditLog auditLog = new AuditLog();

        switch (navigation) {
            case "back":
                dispatch(request, response, "/declined_transfer.jsp");
                break;
            case "localFT":
                System.out.println("token: " + request.getParameter("token"));
                if (processClass.validatePin((String) session.getAttribute("user_id"), request.getParameter("token").trim())) {
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft                    
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("ILFT");
                    alVo.setTrxDesc("initiate local fund transfer");
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************

                    logFundTransfer(request, response, fundTransferBo, fundTransferDao, navigation);

                } else {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("ILFT");
                    alVo.setTrxDesc("initiate local fund transfer");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    alVo.setErrorMessage("Token has Expired. Kindly regenerate");
                    auditLog.log(alVo);
                    //*****************************************
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    request.setAttribute("result", " Token has Expired. Kindly regenerate ");
                    dispatch(request, response, "/result.jsp");
                }
                break;
            case "thirdPartyFT":
                System.out.println("token: " + request.getParameter("token"));
                if (processClass.validatePin((String) session.getAttribute("user_id"), request.getParameter("token"))) {
                    logFundTransfer(request, response, fundTransferBo, fundTransferDao, navigation);
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("ITPFT");
                    alVo.setTrxDesc("initiate third party fund transfer");
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************
                } else {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("ITPFT");
                    alVo.setTrxDesc("initiate third party fund transfer");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    alVo.setErrorMessage("Token has Expired. Kindly regenerate");
                    auditLog.log(alVo);
                    //*****************************************
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    request.setAttribute("result", " Token has Expired. Kindly regenerate ");
                    dispatch(request, response, "/result.jsp");
                }
                break;
            case "IBFT":
                System.out.println("token: " + request.getParameter("token").trim());
                if (processClass.validatePin((String) session.getAttribute("user_id"), request.getParameter("token").trim())) {
                    logFundTransfer(request, response, fundTransferBo, fundTransferDao, navigation);
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("INBFT");
                    alVo.setTrxDesc("initiate inter bank fund transfer");
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    auditLog.log(alVo);
                    //*****************************************
                } else {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("INBFT");
                    alVo.setTrxDesc("initiate inter bank fund transfer");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                    alVo.setErrorMessage("Token has Expired. Kindly regenerate");
                    auditLog.log(alVo);
                    //*****************************************
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    request.setAttribute("result", " Token has Expired. Kindly regenerate ");
                    dispatch(request, response, "/result.jsp");
                }
                break;
            case "post_transfer":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("POFT");
                alVo.setTrxDesc("submit approved fund transfer");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/post_transfer.jsp");
                break;
            case "get_ft_details":
                request.setAttribute("trans_id", request.getParameter("id"));
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("GFTD");
                alVo.setTrxDesc("get fund transfer details");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/trans_detail.jsp");
                break;
            case "authFT":
                int result = 0;
                String auth1 = request.getParameter("auth1");
                String auth2 = request.getParameter("auth2");

                String trans_id = request.getParameter("trans_id");
                String action = request.getParameter("action");
                String auth1_comment = request.getParameter("auth1_comment");
                String auth2_comment = request.getParameter("auth2_comment");
                String pin = request.getParameter("pin"); //validate pin for LLA during AND operation

                String role_id = ((UserVo) session.getAttribute("userVo")).getRoleId();

                switch (role_id) {
                    case "Low Level Authorizer":
                        if (processClass.validatePin((String) session.getAttribute("user_id"), pin.trim())) {
                            result = fundTransferBo.processFTByAuth1(auth1, action, auth1_comment, trans_id);
                            updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                        } else {
                            updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                            request.setAttribute("result", " Token has Expired. Kindly regenerate ");
                            dispatch(request, response, "/result.jsp");
                        }
                        break;
                    case "High Level Authorizer":
                        result = fundTransferBo.processFTByAuth2(auth2, action, auth2_comment, trans_id);
                        break;
                }

                if (result == 1) {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("HLFTA");
                    alVo.setTrxDesc("approve fund transfer request");
                    alVo.setProcessStatus("Y");
                    alVo.setResponseStatus("0");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + "/" + "pending_trans.jsp");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "Transaction has been succesfully " + action);
                } else {
                    //*****************************************
                    alVo = new AuditLogVo();
                    alVo = loadAuditLog(request, session);
                    alVo.setTxnId("HLFTA");
                    alVo.setTrxDesc("approve fund transfer request");
                    alVo.setProcessStatus("N");
                    alVo.setResponseStatus("-1");
                    alVo.setTrxSeq(Utility.getReqId());
                    alVo.setAuditRequest("screenId=1&taskId=" + "/" + "pending_trans.jsp");
                    alVo.setErrorMessage("An error occured while processing transaction " + trans_id + " please contact the system admin");
                    auditLog.log(alVo);
                    //*****************************************
                    request.setAttribute("result", "An error occured while processing transaction " + trans_id + " please contact the system admin");
                }

                int flag = fundTransferDao.updatePostStatus(trans_id);
                System.out.println("result of updating post_status is : " + flag);

                dispatch(request, response, "/" + "pending_trans.jsp");

                break;

            case "post":
                String trxId = request.getParameter("id");
                ftVo ftDetailsByTrxId = fundTransferBo.getFundTransferByTrxId(trxId);
                if (ftDetailsByTrxId.getTrxType().equalsIgnoreCase("Third Party Transfer")) {

                } else if (ftDetailsByTrxId.getTrxType().equalsIgnoreCase("Inter Bank Fund Transfer")) {

                } else {
                    System.err.println("**** Unknown trx type ****");
                }
                break;

            case "declined_transfer":

                break;
            default:
                System.out.println("*** unrecognized navigation command in FundTransferController ***");
                break;
        }

    }

    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session) {
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr());
        alVo.setUserId((String) session.getAttribute("user_id"));

        return alVo;
    }

    public void logFundTransfer(HttpServletRequest request, HttpServletResponse response,
            FundTransferBo fundTransferBo, FundTransferDAO fundTransferDao,
            String navigation) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        FundTransferDaoImpl ftDaoImpl = new FundTransferDaoImpl();
        MandateVo mandateVo = null;
        String acc = request.getParameter("ftdebitacc").trim();
        String ben_acc = request.getParameter("DestAcctNo").trim();
        String amount = request.getParameter("amt").trim();
        System.out.println("amount: " + amount);
        String ccy = request.getParameter("ccy");
        String narration = request.getParameter("narration").trim();

//        String trxLimit = fundTransferBo.getTransactionLimit(acc, (String) request.getSession().getAttribute("user_id"));
//        System.out.println("trx limit: " + trxLimit);
//        double accLimitDouble = Double.parseDouble(trxLimit.replaceAll(",", ""));
        double dailyLimit = Double.parseDouble(ftDaoImpl.getDailyTransactionLimit());
        String trxThreshold = ftDaoImpl.getTransactionThreshold(((UserVo) session.getAttribute("userVo")).getCustomerNo());
        System.out.println("transaction threshold: " + trxThreshold);
//        System.out.println("accLimitDouble: " + accLimitDouble);

        System.out.println("dailyLimit: " + dailyLimit);
        double amt = Double.parseDouble(amount.replaceAll(",", ""));
        System.out.println("amt: " + amt);

        if (navigation.equalsIgnoreCase("localFT")) {
            dailyLimit = 0; //disable daily limit for local ft
            trxThreshold = "0"; //disable trx threshold
        }

        if (!trxThreshold.equals("0")) {
            if (amt > Double.parseDouble(trxThreshold)) {
                request.setAttribute("result", "You have exceeded the transaction limit.");
                dispatch(request, response, "/result.jsp");
            }
        }
        if (((amt) > dailyLimit)) {
            request.setAttribute("result", "You have exceeded your daily limit.");
            dispatch(request, response, "/result.jsp");
        } else {
            mandateVo = fundTransferBo.getMandate(String.valueOf(amt), acc);
            if (mandateVo != null) {
                System.out.println("mandate: " + mandateVo.getAuth1() + ", "
                        + mandateVo.getOperator() + ", " + mandateVo.getAuth2());
                String auth1 = mandateVo.getAuth1();
                String operator = mandateVo.getOperator();
                System.out.println("operatior: " + operator);
                String auth2 = mandateVo.getAuth2();

                int trans_id = fundTransferDao.getTrxId();

                ftVo tvo = new ftVo();
                tvo.setAcc(acc);
                tvo.setAmt(amt);
                tvo.setAuth1(auth1);
                tvo.setAuth2(auth2);
                tvo.setAuth1_action("pending");
                tvo.setBen_acc(ben_acc);
                tvo.setInt_id((String) request.getSession().getAttribute("user_id"));
                tvo.setNarration(narration);
                tvo.setTrans_id(String.valueOf(trans_id));
                tvo.setOperator(operator);
                tvo.setInit_acc(acc);
                tvo.setCcy(ccy);

                if (navigation.equalsIgnoreCase("thirdPartyFT")) {
                    tvo.setTrxType("Third Party Transfer");
                } else if (navigation.equalsIgnoreCase("localFT")) {
                    tvo.setTrxType("Local Transfer");
                } else if (navigation.equalsIgnoreCase("IBFT")) {
                    String bene_bank = request.getParameter("bene_bank").trim();
                    tvo.setBene_bank(bene_bank);
                    String bene_acc_name = request.getParameter("bene_acc_name").trim();
                    tvo.setBene_acc_name(bene_acc_name);
                    tvo.setTrxType("Inter Bank Fund Transfer");
                }

                switch (operator) {
                    case "AND":
                        tvo.setAuth2_action("pending");
                        break;
                    case "OR":
                        tvo.setAuth2_action("pending");
                        break;
                    default:
                        System.out.println("Operator : " + operator);
                        break;
                }
                //log third party fund transfer
                int result = fundTransferBo.logFundTransfer(tvo);
                if (result == 1) {
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    request.setAttribute("result", "Transfer has been queued up for authorisation");
                    dispatch(request, response, "/result.jsp");
                } else {
                    updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                    request.setAttribute("result", "An error occured while processing transfer, please contact system admin");
//                          request.setAttribute("trans_limit", trans_limit);
//                          request.setAttribute("account", request.getParameter("account"));
                    dispatch(request, response, "/result.jsp");
                }
            } else {
                System.out.println("**** mandate is null *****");
                updateToken((String) request.getSession().getAttribute("user_id")); //force the token to expire after each submission of ft
                request.setAttribute("result", "A mandate has not been created for this transaction, please ask the admin to create mandate and retry, thanks!");
//                        request.setAttribute("account", request.getParameter("account"));
//                        request.setAttribute("trans_limit", trans_limit);
                dispatch(request, response, "/result.jsp");
            }

        }
    }

    private void updateToken(String userId) {
        System.out.println("user id for updating token: " + userId);
        Connection con = null;
        PreparedStatement stmt = null;
        String sqlString = "update sm_user_access set pin_to_expire = ? where cod_usr_id = ?";
        boolean result = false;
        try {
            con = com.fasyl.ebanking.db.DataBase.getConnection();
            stmt = con.prepareStatement(sqlString);

            stmt.setString(1, "0");
            stmt.setString(2, userId);
            result = stmt.execute();

        } catch (Exception e) {
            System.out.println("result after updating token: " + result);
            System.err.println("error updating token >>> " + e.getMessage());
        }
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
