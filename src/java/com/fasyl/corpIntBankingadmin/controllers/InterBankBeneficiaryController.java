package com.fasyl.corpIntBankingadmin.controllers;

import com.fasyl.corpIntBankingadmin.BoImpl.InterBankBeneficiaryBoImpl;
import com.fasyl.corpIntBankingadmin.bo.InterBankBeneficiaryBo;
import com.fasyl.corpIntBankingadmin.utility.AuditLog;
import com.fasyl.corpIntBankingadmin.vo.AuditLogVo;
import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.dto.InterBeneficiaryDto;
import com.fasyl.ebanking.util.Utility;
import com.net.ExternalCall;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author kingsley
 */
public class InterBankBeneficiaryController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int result = 0;
        String benName = "";
        String benAccNo = "";
        String benBank = "";
        String amt = "";
        String userId = "";
        String debitAcc = "";
        String benId = "";
        String navigation = request.getParameter("taskId");
        System.out.println("navigation command in InterBankBeneficiaryController: " + navigation);
        HttpSession session = request.getSession(false);
        InterBankBeneficiaryBo interBankBeneficiaryBo = new InterBankBeneficiaryBoImpl();
        InterBeneficiaryDto iBenDto;
        AuditLogVo alVo;
        AuditLog auditLog = new AuditLog();
        ExternalCall externalCall = new ExternalCall();
        StringWriter writers = new StringWriter();
        switch (navigation) {
            case "addBen":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("ABEN");
                alVo.setTrxDesc("Add inter bank beneficiary");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                request.setAttribute("benName", "first"); //set a value to denote first time loading of addBen, this value changes when user performs nameEnquiry
                dispatch(request, response, "/addBen.jsp");
                break;
            case "doNameEnquiry":
                userId = request.getParameter("userId");
                String custNo = interBankBeneficiaryBo.getCustNo(((UserVo) session.getAttribute("userVo")).getClient());
                debitAcc = interBankBeneficiaryBo.getDebitAccNo(custNo);
                benBank = request.getParameter("bankName");
                benAccNo = request.getParameter("benAccNo");
                System.out.println("cust no: " + custNo + "userId: " + userId + ", debitAcc: " + debitAcc + ", benBank: " + benBank + ", benAccNo: " + benAccNo);
//                benName = externalCall.getBeneficiaryName(debitAcc, benAccNo, benBank);
                benName = "John Doe";
                System.out.println("ben name retrived from creating inter bank beneficiary: " + benName);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("DNEQ");
                alVo.setTrxDesc("Add inter bank beneficiary");
                alVo.setProcessStatus("Y");
                if (benName != null || !benName.equalsIgnoreCase("")) {
                    alVo.setResponseStatus("0");
                } else {
                    alVo.setResponseStatus("-1");
                }

                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                request.setAttribute("benBank", benBank);
                request.setAttribute("benAccNo", benAccNo);
                request.setAttribute("benName", benName);
                dispatch(request, response, "/addBen.jsp");
                break;
            case "createBen":
                benAccNo = request.getParameter("benAccNo");
                benName = request.getParameter("interbankAccName");
                benBank = request.getParameter("bankName");
                amt = request.getParameter("interbankAmt");
                userId = request.getParameter("userId");
                custNo = interBankBeneficiaryBo.getCustNo(((UserVo) session.getAttribute("userVo")).getClient());
                debitAcc = interBankBeneficiaryBo.getDebitAccNo(custNo);

                iBenDto = new InterBeneficiaryDto();
                iBenDto.setBeneficiary_name(benName);
                iBenDto.setBenBank(benBank);
                iBenDto.setAmount(amt);
                iBenDto.setCustomer_no(custNo);
                iBenDto.setDestination_acct(benAccNo);
                result = interBankBeneficiaryBo.createInterBankBeneficairy(iBenDto);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("DNEQ");
                alVo.setTrxDesc("Create inter bank beneficiary");
                alVo.setProcessStatus("Y");
                if (result > 0) {
                    alVo.setResponseStatus("0");
                } else {
                    alVo.setResponseStatus("-1");
                }

                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************

                if (result > 0) {
                    request.setAttribute("result", "Inter bank beneficiary was added successfully.");
                    dispatch(request, response, "/result.jsp");
                } else {
                    request.setAttribute("result", "Operation was not successful, please try again.");
                    dispatch(request, response, "/result.jsp");
                }

                break;
            case "viewBen":
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("VBEN");
                alVo.setTrxDesc("View inter bank beneficiary");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/viewBen.jsp");
                break;
            case "modBen":
                benBank = request.getParameter("benBank");
                benName = request.getParameter("benName");
                benAccNo = request.getParameter("benAcct");
                benId = request.getParameter("benId");
                amt = request.getParameter("amt");
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("MBEN");
                alVo.setTrxDesc("Modify inter bank beneficiary");
                alVo.setProcessStatus("Y");
                alVo.setResponseStatus("0");
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                dispatch(request, response, "/modBen.jsp");
                break;

            case "updateBen":
                benBank = request.getParameter("benBank");
                benName = request.getParameter("benName");
                benAccNo = request.getParameter("benAcct");
                benId = request.getParameter("benId");
                amt = request.getParameter("amt");
                result = interBankBeneficiaryBo.updateInterBankBen(benId, amt);
                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("UBEN");
                alVo.setTrxDesc("Modify inter bank beneficiary");
                alVo.setProcessStatus("Y");
                if (result > 0) {
                    alVo.setResponseStatus("0");
                } else {
                    alVo.setResponseStatus("-1");
                }
                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                if (result > 0) {
                    request.setAttribute("result", "Inter bank beneficiary was modified successfully.");
                    dispatch(request, response, "/viewBen.jsp");
                } else {
                    request.setAttribute("result", "Operation was not successful, please try again.");
                    dispatch(request, response, "/viewBen.jsp");
                }
                break;
            case "delBen":
                benId = request.getParameter("benId");
                result = interBankBeneficiaryBo.deleteInterBankBen(benId);

                //*****************************************
                alVo = new AuditLogVo();
                alVo = loadAuditLog(request, session);
                alVo.setTxnId("DIBB");
                alVo.setTrxDesc("Delete inter bank beneficiary");
                alVo.setProcessStatus("Y");
                if (result > 0) {
                    alVo.setResponseStatus("0");
                } else {
                    alVo.setResponseStatus("-1");
                }

                alVo.setTrxSeq(Utility.getReqId());
                alVo.setAuditRequest("screenId=1&taskId=" + request.getParameter("taskId"));
                auditLog.log(alVo);
                //*****************************************
                if (result > 0) {
                    request.setAttribute("result", "Inter bank beneficiary was deleted successfully.");
                    dispatch(request, response, "/viewBen.jsp");
                } else {
                    request.setAttribute("result", "Operation was not successful, please try again.");
                    dispatch(request, response, "/viewBen.jsp");
                }
                break;
            default:
                System.out.println("*** unrecognized navigation command in InterBankBeneficiaryController ***");
                break;
        }
    }

    public AuditLogVo loadAuditLog(HttpServletRequest request, HttpSession session) {
        AuditLogVo alVo = new AuditLogVo();
        alVo.setSessionId(session.getId());
        alVo.setRemoteAddress(request.getRemoteAddr());
        alVo.setUserId((String) session.getAttribute("user_id"));

        return alVo;
    }

    private void dispatch(HttpServletRequest request, HttpServletResponse response, String destination) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
