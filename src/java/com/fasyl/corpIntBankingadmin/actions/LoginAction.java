package com.fasyl.corpIntBankingadmin.actions;

import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.forms.LoginForm;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import com.fasyl.ebanking.logic.LoadBasicParam;
import com.fasyl.ebanking.util.Encryption;
import com.fasyl.ebanking.util.LoadData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class LoginAction extends org.apache.struts.action.Action {

    final static Logger logger = Logger.getLogger(LoginAction.class);
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String FIRSTLOGIN = "firstlogin";
    private static final String PASSWORDRESETLOGIN = "passwordresetlogin";
    private String encryptedPassword;
    public UserAccessBo userAccessBo;
    UserVo userVo;
    private int loggingCount = 0;

    public LoginAction() {
        userAccessBo = new UserAccessBoImpl();
    }
    
    static {
        LoadData.loadData();
        LoadBasicParam.loadData();
    }

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        try {
            LoginForm _form = (LoginForm) form;
            logger.debug("User id: " + _form.getUserid());
            logger.debug("Password: " + _form.getPassword());

            String userid = _form.getUserid().toUpperCase();
            String password = _form.getPassword();
            
            _form.validate(mapping, request);

            encryptedPassword = Encryption.encrypt(password);
            
            System.out.println("user id in caps: " + userid);
            System.out.println("password from login: " + encryptedPassword);
            HttpSession session = request.getSession(true);
            if (userAccessBo.getLoggingCount(userid) == 0 && userAccessBo.isUserActive(userid)
                    && userAccessBo.doesUserExist(userid) 
                    && encryptedPassword.equalsIgnoreCase(userAccessBo.getPassword(userid))) {
                session.setAttribute("user_id", userid);
//                session.setAttribute("lastLogin", userAccessBo.getLastLoginDate(_form.getUserid()));
                return mapping.findForward(FIRSTLOGIN);

            }else if ((userAccessBo.getPasswordReset(userid)).equalsIgnoreCase("Y") 
                    &&  userAccessBo.isUserActive(userid)
                    && userAccessBo.doesUserExist(userid) && encryptedPassword.equalsIgnoreCase(userAccessBo.getPassword(userid))) {

                session.setAttribute("user_id", userid);
                return mapping.findForward(PASSWORDRESETLOGIN);

            }else if (userAccessBo.isLoggedIn(userid)) {
                //user logged in already dont allow second time logging in
                _form.reset();
                request.setAttribute("msg_text", "The user has already logged somewhere");
                request.setAttribute("msg_model_type", "message");
                request.setAttribute("msg_ok_target", "ReLogin.jsp");

                return mapping.findForward("complexerror");
                
            } else if(!userAccessBo.isUserActive(userid)){
                request.setAttribute("message", "your account is not active, please contact the system admin");
                return mapping.findForward(FAILURE);
                
            } else if (userAccessBo.isUserActive(userid) && 
                    encryptedPassword.equalsIgnoreCase(userAccessBo.getPassword(userid))) {
                session.setAttribute("user_id", userid);
                session.setAttribute("lastLogin", userAccessBo.getLastLoginDate(userid));
                
                userVo = userAccessBo.getUser(userid);
                session.setAttribute("userVo", userVo);
                
                userAccessBo.updateLastLoggingDate(userid);
                loggingCount = userAccessBo.getLoggingCount(userid);
                userAccessBo.updateLoggingCount(userid, ++loggingCount);
                userAccessBo.updateLastLoggingDate(userid);
                userAccessBo.updateLoginState(userid, "Y");

                return mapping.findForward(SUCCESS);

            } else {
                request.setAttribute("message", "Invalid login credentials");
                return mapping.findForward(FAILURE);
            }

        } catch (Exception e) {
            logger.error("Error occured while handling user, " + e.getMessage());
            e.printStackTrace(System.out);
        }
        return mapping.findForward(SUCCESS);
    }
}
