package com.fasyl.corpIntBankingadmin.actions;

import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.corpIntBankingadmin.daoImpl.UserAccessDAOImpl;
import com.fasyl.corpIntBankingadmin.forms.FirstLoginForm;
import com.fasyl.ebanking.util.Encryption;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class FirstLoginAction extends org.apache.struts.action.Action {

    final static Logger logger = Logger.getLogger(FirstLoginAction.class);
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    private static final String FAIRLURE = "failure";
    private String encryptedPassword;

    private UserAccessBo userAccessBo;
    private UserAccessDAOImpl userAccessDaoImpl = new UserAccessDAOImpl();

    public FirstLoginAction() {
        userAccessBo = new UserAccessBoImpl();
    }

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        try {
            FirstLoginForm _form = (FirstLoginForm) form;

            _form.validate(mapping, request);

            encryptedPassword = Encryption.encrypt(_form.getNewPassword());

            logger.debug("encrypted password: " + encryptedPassword);

            HttpSession session = request.getSession(false);
            
//             if (!_form.getAnswer().equalsIgnoreCase(userAccessDaoImpl.getAnswer((String) session.getAttribute("user_id")))) {
//                request.setAttribute("message", "Wrong answer, try again!");
//                return mapping.findForward(FAIRLURE);
//            }
             
            int affectedRowCount = userAccessBo.updatePasswordOnFirstTimeLogin((String) session.getAttribute("user_id"), encryptedPassword,
                    _form.getQuestionId(), _form.getAnswer());

            if (affectedRowCount == 1) {
                request.setAttribute("message", "Password successfully changed. <br/>You may now log in with your new password");
                return mapping.findForward(SUCCESS);
            } else {
                request.setAttribute("message", "An error occured while trying to update your password. <br/>Please contact system admin");
                return mapping.findForward(FAIRLURE);
            }

        } catch (Exception e) {
            System.out.println("error in processing first login action : " + e.getMessage());
        }

        return mapping.findForward(SUCCESS);
    }
}
