package com.fasyl.corpIntBankingadmin.vo;

/**
 *
 * @author kingsley
 */
public class FundTransferVo {

    private String transactionId;
    private String initiatorId;
    private String initAccount;
    private String benAccount;
    private String narration;
    private String initTime;
    private String auth1;
    private String auth1Action;
    private String auth1Time;
    private String auth1Comment;
    private String auth2;
    private String auth2Action;
    private String auth2Time;
    private String auth2Comment;
    private String operator;
    private String postStatus;
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(String initiatorId) {
        this.initiatorId = initiatorId;
    }

    public String getInitAccount() {
        return initAccount;
    }

    public void setInitAccount(String initAccount) {
        this.initAccount = initAccount;
    }

    public String getBenAccount() {
        return benAccount;
    }

    public void setBenAccount(String benAccount) {
        this.benAccount = benAccount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getInitTime() {
        return initTime;
    }

    public void setInitTime(String initTime) {
        this.initTime = initTime;
    }

    public String getAuth1() {
        return auth1;
    }

    public void setAuth1(String auth1) {
        this.auth1 = auth1;
    }

    public String getAuth1Action() {
        return auth1Action;
    }

    public void setAuth1Action(String auth1Action) {
        this.auth1Action = auth1Action;
    }

    public String getAuth1Time() {
        return auth1Time;
    }

    public void setAuth1Time(String auth1Time) {
        this.auth1Time = auth1Time;
    }

    public String getAuth1Comment() {
        return auth1Comment;
    }

    public void setAuth1Comment(String auth1Comment) {
        this.auth1Comment = auth1Comment;
    }

    public String getAuth2() {
        return auth2;
    }

    public void setAuth2(String auth2) {
        this.auth2 = auth2;
    }

    public String getAuth2Action() {
        return auth2Action;
    }

    public void setAuth2Action(String auth2Action) {
        this.auth2Action = auth2Action;
    }

    public String getAuth2Time() {
        return auth2Time;
    }

    public void setAuth2Time(String auth2Time) {
        this.auth2Time = auth2Time;
    }

    public String getAuth2Comment() {
        return auth2Comment;
    }

    public void setAuth2Comment(String auth2Comment) {
        this.auth2Comment = auth2Comment;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }
    
    
}
