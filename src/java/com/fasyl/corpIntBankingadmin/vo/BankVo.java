
package com.fasyl.corpIntBankingadmin.vo;

/**
 *
 * @author kingsley
 */
public class BankVo {
    private String bank_code;
    private String bank_name;

    public String getBankCode() {
        return bank_code;
    }

    public void setBankCode(String bankCode) {
        this.bank_code = bankCode;
    }

    public String getBankName() {
        return bank_name;
    }

    public void setBankName(String bankName) {
        this.bank_name = bankName;
    }
    
    
}
