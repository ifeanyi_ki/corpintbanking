/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.vo;

/**
 *
 * @author kingsley
 */
public class AuditLogVo {
//    (txn_id,user_id,dattxn,remote_address,session_id,"
//                + "trx_seq,cust_id,customer_type,txn_desc,resp_status,process_status)"
    private String txnId;
    private String userId;
    private String txnDate;
    private String remoteAddress;
    private String sessionId;
    private String trxSeq;
    private String custId;
    private String customerType;
    private String trxDesc;
    private String responseStatus;
    private String processStatus;
    private String auditRequest;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    

    public String getAuditRequest() {
        return auditRequest;
    }

    public void setAuditRequest(String auditRequest) {
        this.auditRequest = auditRequest;
    }
    
    

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setTrxSeq(String trxSeq) {
        this.trxSeq = trxSeq;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public void setTrxDesc(String trxDesc) {
        this.trxDesc = trxDesc;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    
    public String getTxnId() {
        return txnId;
    }

    public String getUserId() {
        return userId;
    }

    public String getTxnDate() {
        return "SYSDATE";
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getTrxSeq() {
        return trxSeq;
    }

    public String getCustId() {
        return custId;
    }

    public String getCustomerType() {
        return "C";
    }

    public String getTrxDesc() {
        return trxDesc;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public String getProcessStatus() {
        return processStatus;
    }
    
    
    
    
}
