
package com.fasyl.corpIntBankingadmin.vo;

/**
 *
 * @author kingsley
 */
public class QuestionData {
    private String question_id;
    private String question;

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
    
    
}
