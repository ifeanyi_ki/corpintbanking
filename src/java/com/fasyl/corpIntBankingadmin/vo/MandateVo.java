package com.fasyl.corpIntBankingadmin.vo;

/**
 *
 * @author kingsley
 */
public class MandateVo {
    String auth1;
    String auth2;
    String operator;
    String lowerLimit;
    String higherLimit;

    public String getAuth1() {
        return auth1;
    }

    public void setAuth1(String auth1) {
        this.auth1 = auth1;
    }

    public String getAuth2() {
        return auth2;
    }

    public void setAuth2(String auth2) {
        this.auth2 = auth2;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(String lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getHigherLimit() {
        return higherLimit;
    }

    public void setHigherLimit(String higherLimit) {
        this.higherLimit = higherLimit;
    }
    
    
    
}
