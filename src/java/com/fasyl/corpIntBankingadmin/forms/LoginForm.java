
package com.fasyl.corpIntBankingadmin.forms;

import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;


public class LoginForm extends org.apache.struts.action.ActionForm {
    private String userid;
    private String password;
    private String macAddress;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
    
    public String getUserid() {
        return userid;
    }

    public void setUserid(String username) {
        this.userid = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginForm() throws IOException, SQLException, PropertyVetoException {
        super();
    }

   
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        UserAccessBo userAccessBo;
        userAccessBo = new UserAccessBoImpl();
        String userid = getUserid().toUpperCase();
        
        System.out.println("user id in caps: " + userid);
        
        if ((userid == null) || (userid.length() < 1)) {
            errors.add("userid", new ActionMessage("errors.Userid.required"));
        }

        if ((getPassword() == null) || (getPassword().length() < 1)) {
            errors.add("password", new ActionMessage("errors.password.required"));
        }
        
        if(!userAccessBo.doesUserExist(userid)){
            errors.add("userid", new ActionMessage("errors.user.absent"));
            
        }
        
        return errors;
    }
    
    public void reset() {
        userid   = null;
        password = null;
    }
}
