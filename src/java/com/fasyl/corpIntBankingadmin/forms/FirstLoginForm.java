package com.fasyl.corpIntBankingadmin.forms;

import com.fasyl.corpIntBankingadmin.BoImpl.UserAccessBoImpl;
import com.fasyl.corpIntBankingadmin.bo.UserAccessBo;
import com.fasyl.ebanking.util.Encryption;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class FirstLoginForm extends org.apache.struts.action.ActionForm {

    private String newPassword;
    private String oldPassword;
    private String confirmPassword;
    private UserAccessBo userAccessBo = new UserAccessBoImpl();
    private String questionId;
    private String question;
    private String answer;
    private ArrayList questionList;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ArrayList getQuestionList() {
        questionList = userAccessBo.getQuestions();
        return questionList;
    }

    public void setQuestionList(ArrayList questionList) {
        this.questionList = questionList;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        if (!validatePassword(getNewPassword())) {
            //errors.password.strength
            errors.add("passStrength", new ActionMessage("errors.password.strength"));
            errors.add("passStrength", new ActionMessage("errors.password.strength1"));
            errors.add("passStrength", new ActionMessage("errors.password.strength2"));
            errors.add("passStrength", new ActionMessage("errors.password.strength3"));
            errors.add("passStrength", new ActionMessage("errors.password.strength4"));
            errors.add("passStrength", new ActionMessage("errors.password.strength5"));
        }

        if (getAnswer() == null || getAnswer().length() < 1) {
            errors.add("answer", new ActionMessage("errors.answer.required"));
        }

        if (getQuestionId().equals("0") || getQuestionId() == "0" || getQuestionId().length() < 1 || getQuestionId() == null) {
            errors.add("question", new ActionMessage("errors.question.required"));
        }

        if (getOldPassword() == null || getOldPassword().length() < 1) {
            errors.add("oldPassword", new ActionMessage("error.old.password"));
        }

        if (getNewPassword() == null || getNewPassword().length() < 1) {
            errors.add("newPassword", new ActionMessage("error.new.password"));
        }

        if (getConfirmPassword() == null || getConfirmPassword().length() < 1) {
            errors.add("confirmPassword", new ActionMessage("error.confirm.password"));
        }

        if (getOldPassword().equalsIgnoreCase(getNewPassword())) {
            errors.add("newPassword", new ActionMessage("error.password.oldeqnew"));
        }

        if (!Encryption.encrypt(getOldPassword()).equalsIgnoreCase(userAccessBo.getPassword(
                (String) request.getSession().getAttribute("user_id")))) {
            errors.add("oldPassword", new ActionMessage("error.password.oldntexist"));
        }

        if (!getConfirmPassword().equalsIgnoreCase(getNewPassword())) {
            errors.add("newPassword", new ActionMessage("error.password.ntequal"));
        }

        if (!(getNewPassword() == null)) {
            if (getNewPassword().equalsIgnoreCase("password")) {
                errors.add("newPassword", new ActionMessage("error.cannot.usedefaultPassword"));
            }
        }
        return errors;
    }

    private boolean validatePassword(String newPassword) {
        String regex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(newPassword);

        if (matcher.find()) {
            return true;
        } else if (newPassword.length() < 8 || newPassword.length() > 35) {
            return false;
        } else {
            return false;
        }
    }
}
