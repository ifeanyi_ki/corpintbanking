
package com.fasyl.corpIntBankingadmin.bo;

import java.util.List;

/**
 *
 * @author kingsley
 */
public interface PasswordChangeBo {
    public List<String> getListofPendingPasswordChangeRequest(String userid);
    
    public int authorisePasswordChange(String userid, String authId);
    
    public int declinePasswordChange(String userid, String authId);
    
    public int resetPassword(String userId);
    
    public int changePasswordRequest(String userId, String newPassword, String status);
    
    public String getPasswordRequestChangeStatus(String userId);
    
}
