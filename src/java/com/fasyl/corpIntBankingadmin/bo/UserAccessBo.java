package com.fasyl.corpIntBankingadmin.bo;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.util.ArrayList;
import java.util.List;


public interface UserAccessBo {
    
    public boolean doesUserExist(String userId);
    
    public int getLoggingCount(String userId);
    
    public boolean isUserActive(String userId);
    
    public String getRoleId(String userId);
    
    public int updateLastLoggingDate(String userId);
    
    public int updateLoggingCount(String userId, int loggingCount);
    
    public String getLastLoginDate(String userId);
    
    public String getPassword(String userId);
    
    public int updatePasswordOnFirstTimeLogin(String userId, String password, String questionId, String answer);
    
    public UserVo getUser(String userId);
    
    public List<String> getUserByIdorBylastOrFirstName(String userId, String clientName);
    
    public List<String> getListOfDeclinedUsers(String userId);
    
    public boolean isLoggedIn(String userId);
    
    public int updateLoginState(String userId, String status);
    
    public boolean isFieldAvailable(String field);
    
    public boolean isFieldAvailable2(String field, String userid);
    
    public ArrayList getQuestions();
    
    public String getPasswordReset(String userid);
    
    public int updatePasswordReset(String userId, String password);
    
}
