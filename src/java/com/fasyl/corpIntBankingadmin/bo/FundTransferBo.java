/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fasyl.corpIntBankingadmin.bo;

import com.fasyl.corpIntBankingadmin.vo.FundTransferVo;
import com.fasyl.corpIntBankingadmin.vo.MandateVo;
import com.fasyl.corpIntBankingadmin.vo.ftVo;
import java.util.List;

/**
 *
 * @author XNETTIntBanking
 */
public interface FundTransferBo {

    public int getPendingFundTransferCount(String userId);

    public List<FundTransferVo> getPendingFundTransfers(String userId);

    public List<FundTransferVo> getPendingFundTransfersByAuth1(String userId);

    public List<FundTransferVo> getPendingFundTransfersByAuth2(String userId);

    public List<String> getAccountListForAnInitiator(String userId);

    public String getTransactionLimit(String accountNo, String userId);

    public MandateVo getMandate(String amount, String accountNumber);

    public int logFundTransfer(ftVo ftVo);

    public List<ftVo> getAuthorizedFundTrxOfONLYOperator(String userId);

    public List<ftVo> getAuthorizedFundTrxOfOROperator(String userId);

    public List<ftVo> getAuthorizedFundTrxOfAndOperator(String userId);

    public ftVo getFundTransferByTrxId(String transId);
    
    public ftVo getFundTransferByTrxIdAndAuth1(String transId);
    
    public ftVo getFundTransferByTrxIdAndAuth2(String transId);
    
    public ftVo getFundTransferByTrxIdAndAuth2withOR(String transId);
    
    public int processFTByAuth1(String auth1, String action, String auth1Comment, String trxId);
    
    public int processFTByAuth2(String auth2, String action, String auth1Comment, String trxId);
    
    public boolean updatePostStatusByTrxId(String trxId);

}
