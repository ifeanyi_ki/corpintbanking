package com.fasyl.corpIntBankingadmin.bo;

import com.fasyl.corpIntBankingadmin.vo.CustomerVo;
import java.util.List;

/**
 *
 * @author kingsley
 */
public interface OnboardCustomerBo {

    public List<String> searchUnboardedCustomerNameList(String customerName);

    public CustomerVo getCustomerDetails(String client_name);

    public int onboardClient(String customerNo);

    public List<String> getOnboardedClientList();
}
