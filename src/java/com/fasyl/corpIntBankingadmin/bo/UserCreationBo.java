package com.fasyl.corpIntBankingadmin.bo;

import com.fasyl.corpIntBankingadmin.vo.UserVo;
import java.util.List;

/**
 *
 * @author kingsley
 */
public interface UserCreationBo {
    public int createUser(UserVo userVo);
    
    public int updateUser(UserVo userVo);
    
    public List<String> getUserByStatus();
    
    public int authoriseAdminCreation(UserVo userVo, String authId);
    
    public int createUserX(UserVo userVo, String userid, String user_id);
}
